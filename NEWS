=====================================================================
Multi precision toolbox for Scilab
Copyright (C) 2011 - Jonathan Blanchard

This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at    
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
=====================================================================
Changes from version 0.2 to version 0.3:

MPScilab gateway:

  * Bug fixes
    - Fixed possible segfault mps_linspace().


Changes from version 0.1 to version 0.2:

MPScilab gateway:
  * General
    - Most functions now supports an implicit result operand as well as an explicit one.
      For example mps_sin can be called with an explicit operand as before like this :
      mps_sin( rop, op )
      or an implicit one that will be created and initialized accordingly :
      rop = mps_sin( op )

    - Better and more consistent arguments validation for most functions.

  * Bug fixes
    - Fixed mix-up in the size of the copied matrix in mps_copy().
    - Tweaked a few tests that were erroneously failing on some platforms.

  * New functions
    - mpx_getdebuginfo() : Returns informations about the toolbox.
    - mps_sum() : Sum of matrix elements.
    - mps_factorial() : Factorial function.
    - mps_gamma() : Gamma function.
    - mps_erf() and mps_erfc() : Error function and complementary error function.
    - mps_diag() : Diagonal inclusion or extraction.
    - mps_trace() : Evaluate the trace of a matrix.
    - mps_prod() : Product of matrix elements.
    - mps_det() : Determinant of a matrix.
    - mps_inc() : Matrix inverse.
    - mps_mean() : Mean value of the elements of a matrix.
    - mps_variance() : Variance of of the elements of a matrix.
    - mps_stdev() : Standard deviation of the elements of a matrix.
    - mps_linspace() : Set or create a linearly space vector.
    - mps_int(), mps_ceil(), mps_floor() and mps_round() : Rounding to integer functions.
    - mps_sqr() : Squaring function.
    - mps_log1p() : Natural logarithm of its argument plus one.

mplib:
  * Enhancements
    - Faster matrix initialization, up to 30% faster for large matrices.
    - More accurate but slightly slower matrix multiplication in mps_mat_mul().
