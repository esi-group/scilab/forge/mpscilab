/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* gives the cumsum of op in rop by column */
int mps_col_cumsum( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop, prev;
    unsigned int i, j;
    const unsigned int numrow = MPS_NUMROW(op);
    const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Result operand of unequal size in mps_col_cumsum()\n");

    for ( i = 1; i <= numrow; ++i )
    {
		mpfr_set(mps_get_ele(rop, i, 1), mps_get_ele(op, i, 1), rnd);
		for ( j = 2; j <= numcol; ++j)
		{
			subrop = mps_get_ele(rop, i, j );
			subop = mps_get_ele(op, i, j );
			prev = mps_get_ele(rop, i, j - 1 );

			mpfr_add(subrop, subop, prev, rnd);
		}
    }

    return 0;
}

/* gives the cumsum by the rows of op in rop */
int mps_row_cumsum( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop, prev;
    unsigned int i, j;
    const unsigned int numcol = MPS_NUMCOL(op);
    const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Result operand of unequal size in mps_row_cumsum()\n");

    for ( i = 1; i <= numcol; ++i )
    {
		mpfr_set(mps_get_ele(rop, 1, i), mps_get_ele(op, 1, i), rnd);
		for ( j = 2; j <= numrow; ++j)
		{
			subrop = mps_get_ele(rop, j, i );
			subop = mps_get_ele(op, j, i );
			prev = mps_get_ele(rop, j - 1, i );

			mpfr_add(subrop, subop, prev, rnd);
		}
    }

    return 0;
}

