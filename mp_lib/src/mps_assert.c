/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdio.h>
#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * Assertion failure function. Somewhat blatantly "inspired" by the
 * mpfr-gmp.c file of the mpfr source code.
 */
void mps_assert_fail(const char *file, int line, const char *expr)
{
    if (file != NULL && file[0] != '\0')
    {
        fprintf (stderr, "%s:", file);
        if (line != -1)
            fprintf (stderr, "%d: ", line);
    }
    fprintf (stderr, " assertion failed: %s\n", expr);

    abort();
}

/* This version also print a custom message to stderr. */
void mps_assert_fail_msg(const char *file, int line, const char *expr, const char *msg)
{
    if (file != NULL && file[0] != '\0')
    {
        fprintf (stderr, "%s:", file);
        if (line != -1)
            fprintf (stderr, "%d: ", line);
    }

    fprintf (stderr, " assertion failed: %s --- ", expr);
	fputs(msg, stderr);

    abort();
}

/* Print an error to stderr but do not abort. */
void mps_warning_msg(const char *file, int line, const char *msg)
{
    if (file != NULL && file[0] != '\0')
    {
        fprintf (stderr, "%s:", file);
        if (line != -1)
            fprintf (stderr, "%d: ", line);
    }

    fprintf (stderr, " warning: --- ");
	fputs(msg, stderr);

    abort();
}

