/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Set each element of rop to the function func applied to op */
int mps_map( const mps_ptr rop, const mps_ptr op, const MPFunc func, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_map()\n" );

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
        GET_NUMTHR(num_thr, op)
        #pragma omp parallel for private(i, subrop, subop) \
            schedule(static) num_threads(num_thr)
    #endif
    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );
		func(subrop, subop, rnd);
    }

    return 0;
}

/* version of map which takes 2 arguments */
int mps_map2( const mps_ptr rop,
              const mps_ptr op1,
              const mps_ptr op2,
              const MPFunc2 func,
              const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_map2()\n" );
    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_map2()\n" );

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
        GET_NUMTHR(num_thr, op1)
        #pragma omp parallel for private(i, subrop, subop1, subop2) \
            schedule(static) num_threads(num_thr)
    #endif
    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );
		func(subrop, subop1, subop2, rnd);
    }

    return 0;
}

