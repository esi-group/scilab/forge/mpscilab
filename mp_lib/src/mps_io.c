/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mps_priv.h"
#include "mps_assert.h"

#include "crc32.h"

/* Save and mps matrix to a file. */
int mps_save( FILE* file, const mps_ptr op )
{
    int ret;

    ret = mps_write_header( file, op );

    if( ret != 0 )
        return 1;

    ret = mps_write_flat( file, op );

    if( ret != 0 )
        return 1;
    
    return 0;
}

/* Load an mps matrix from a file. */
int mps_load( const mps_ptr rop, FILE* file )
{
    int ret;
    mps_file_struct header_info;

    ret = mps_read_header( file, &header_info );

    if( ret != 0 )
        return 1;

    ret = mps_read_flat( rop, file, &header_info );

    if( ret != 0 )
        return 1;

    return 0;
}

/*
 * Write the custom mps file header to the given file from op. Writing starts 
 * at the current file position.
 *
 * Header format :
 *
 * 8 bytes signature; 0x 89 4d 50 53 0D 0A 1A 0A
 * 8 bytes endianness and limb size;
 * 8 bytes header version;
 * 8 bytes format; Format of the stored data.
 * 8 bytes numrow; Number of rows.
 * 8 bytes numcol; Number of columns.
 * 8 bytes precision;
 * 8 bytes checksum;
 */
int mps_write_header( FILE* file,  const mps_ptr op )
{
    mps_uint32_t crc32;
    mps_endian_t endian = mps_get_endianness();
    
    /* Byte array containing the file signature. */
    unsigned char sig[8] = { MPS_FS0, MPS_FS1, MPS_FS2, MPS_FS3, MPS_FS4, 
                                MPS_FS5, MPS_FS6, MPS_FS7};

    /* Array for all the remaining header data. */
    mps_uint64_t header[7];

    if( endian == MPS_BIG_ENDIAN )
    {
        if( GMP_LIMB_BITS == 32 )
            header[0] = MPS_BIG_32;
        else
            header[0] = MPS_BIG_64;
    }
    else
    {
        if( GMP_LIMB_BITS == 32 )
            header[0] = MPS_LIT_32;
        else
            header[0] = MPS_LIT_64;
    }

    /* Header version, not really used at the moment. */
    header[1] = 1;

    /* Format, hard coded to 1 for now. */
    header[2] = 1;

    /* Number of rows. */
    header[3] = MPS_NUMROW(op);

    /* Number of columns. */
    header[4] = MPS_NUMCOL(op);

    /* Precision in bits */
    header[5] = MPS_PREC(op);

    /* Evaluate the checksum. */
    crc32 = mps_crc32( 0, sig, 8 );
    crc32 = mps_crc32( crc32, (unsigned char *)header, 6*8 );

    /*
     * The checksum is stored twice so that big and little endian platforms can
     * read the checksum just by casting to a 32 bit int.
     */
    header[6] = crc32;
    crc32 = MPS_SWAP_32(crc32);

    header[6] += (mps_uint64_t)crc32 << 32;

    /* We assume that everything has been taken care of by the caller. */
    clearerr( file );

    fwrite( (void*)sig, sizeof(unsigned char), 8, file );

    if( ferror(file) != 0 )
        return 1;

    fwrite( (void*)header, sizeof(mps_uint64_t), 7, file );

    if( ferror(file) != 0 )
        return 1;

    /* Flush to help in debugging if something crash soon after returning. */
    fflush( file );

    return 0;
}

int mps_read_header( FILE* file, mps_file_struct* header_info )
{
    /* Byte array containing the file signature. */
    unsigned char sig[8] = { MPS_FS0, MPS_FS1, MPS_FS2, MPS_FS3, MPS_FS4,
                                MPS_FS5, MPS_FS6, MPS_FS7};

    mps_uint32_t crc32;
    mps_uint64_t header[8];
    unsigned int i;
    mps_endian_t endian = mps_get_endianness();

    /* We assume that everything has been taken care of by the caller. */
    clearerr( file );

    fread( (void*)header, sizeof(mps_uint64_t), 8, file  );

    if( ferror(file) != 0 )
        return 1;

    for( i=0; i < 8; i++ )
    {
        if( sig[i] != ((unsigned char*)header)[i] )
            return 1;
    }

    crc32 = mps_crc32( 0, (unsigned char*)header, 7*8 );

    printf("%x\n",crc32);
    printf("%x\n",(mps_uint32_t)header[7]);

    if( crc32 != (mps_uint32_t)header[7] )
        return 1;

    if( header[1] == MPS_BIG_32 )
    {
        header_info->_mps_file_endian = MPS_BIG_ENDIAN;
        header_info->_mps_file_word = 32;
    }
    else if( header[1] == MPS_BIG_64 )
    {
        header_info->_mps_file_endian = MPS_BIG_ENDIAN;
        header_info->_mps_file_word = 64;
    }
    else if( header[1] == MPS_LIT_32 )
    {
        header_info->_mps_file_endian = MPS_LITTLE_ENDIAN;
        header_info->_mps_file_word = 32;
    }
    else if( header[1] == MPS_LIT_64 )
    {
        header_info->_mps_file_endian = MPS_LITTLE_ENDIAN;
        header_info->_mps_file_word = 64;
    }
    else
    {
        return 1;
    }

    if( endian == header_info->_mps_file_endian )
    {
        header_info->_mps_file_version = header[2];
        header_info->_mps_file_format = header[3];
        header_info->_mps_file_numrow = header[4];
        header_info->_mps_file_numcol = header[5];
        header_info->_mps_file_precision = header[6];
    }
    else
    {
        header_info->_mps_file_version = MPS_SWAP_64(header[2]);
        header_info->_mps_file_format = MPS_SWAP_64(header[3]);
        header_info->_mps_file_numrow = MPS_SWAP_64(header[4]);
        header_info->_mps_file_numcol = MPS_SWAP_64(header[5]);
        header_info->_mps_file_precision = MPS_SWAP_64(header[6]);
    }

    return 0;
}

/* 
 * Write an mps matrix in a flat format without any compression whatsoever. The
 * function starts writing at the current position of the file given as an
 * argument.
 *
 * This is at the moment very crude(slow) but functional.
 */
int mps_write_flat( FILE* file,  const mps_ptr op )
{
    unsigned int matsize = MPS_SIZE(op);
    unsigned int i;
    mpfr_ptr subop;
    mps_int64_t val;
    size_t w = 0;
    size_t limbsize = GMP_LIMB_BITS / 8;

    size_t limb_length = mpfr_custom_get_size(MPS_PREC(op));

    /* We assume that everything has been taken care of by the caller. */
    clearerr( file );

    /* First write all the relevant section of the mpfr structure. */
    for( i = 1; i<=matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );
        
        val = (mps_int64_t)(subop->_mpfr_sign);
        w = fwrite( (void*)&val, sizeof(mps_int64_t), 1, file );

        if( w != 1 )
            break;

        val = (mps_int64_t)(subop->_mpfr_exp);
        w = fwrite( (void*)&val, sizeof(mps_int64_t), 1, file );
        
        if( w != 1 )
            break;
    }

    if( ferror(file) != 0 )
        return 1;

    /* Then write all the significands. */
    for( i = 1; i<=matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );

        w = fwrite( (void*)(subop->_mpfr_d), limbsize, limb_length/limbsize, file );

        if( w != limb_length/limbsize )
            break;
    }

    if( ferror(file) != 0 )
        return 1;

    /* Flush to help in debugging if something crash soon after returning. */
    fflush( file );

    return 0;
}

/* Read an mps matrix from a flat format.  */
int mps_read_flat( const mps_ptr rop, FILE* file, mps_file_struct* header_info )
{
    unsigned int matsize = header_info->_mps_file_numrow * header_info->_mps_file_numcol;
    unsigned int i;
    unsigned int limb_count;
    mps_uint64_t tmp;
    mps_uint32_t * tmp_ptr = (mps_uint32_t*)&tmp;
    mps_int64_t tmpsign, tmpexp;
     /* mps_uint32_t zero = 0; */
    mpfr_ptr subrop;
    mps_uint64_t* limb_ptr;
    mp_limb_t *nat_limb_ptr;
    mp_limb_t nat_tmp;

    size_t nat_limbsize = GMP_LIMB_BITS;
    size_t limbsize = header_info->_mps_file_word;
    mps_endian_t nat_endian = mps_get_endianness();
    mps_endian_t endian = header_info->_mps_file_endian;

    size_t length = mpfr_custom_get_size( header_info->_mps_file_precision );
    length = length / (GMP_LIMB_BITS/8);

    MPS_ASSERT_MSG( MPS_NUMROW(rop) == header_info->_mps_file_numrow,
        "Wrong number of rows for the result operand in mps_read_flat().\n" );

    MPS_ASSERT_MSG( MPS_NUMCOL(rop) == header_info->_mps_file_numcol,
        "Wrong number of columns for the result operand in mps_read_flat().\n" );

    MPS_ASSERT_MSG( MPS_PREC(rop) == header_info->_mps_file_precision,
        "Wrong number of columns for the result operand in mps_read_flat().\n" );

    MPS_ASSERT_MSG( MPS_IS_COL_ORDER(rop),
        "Wrong ordering for the result operand in mps_read_flat().\n" );

    /* We assume that everything has been taken care of by the caller. */
    clearerr( file );

    for( i = 1; i <= matsize; i++ )
    {
        subrop = mps_get_ele_col( rop, i );
        
        fread( (void*)&tmpsign, sizeof(mps_int64_t), 1, file );
        fread( (void*)&tmpexp, sizeof(mps_int64_t), 1, file );

        if( ferror(file) != 0 )
            return 1;

        if( nat_endian != endian )
        {
            tmpsign = MPS_SWAP_64(tmpsign);
            tmpexp = MPS_SWAP_64(tmpexp);
        }

        subrop->_mpfr_prec = (mpfr_prec_t)(header_info->_mps_file_precision);
        subrop->_mpfr_sign = (mpfr_sign_t)tmpsign;
        subrop->_mpfr_exp = (mp_exp_t)tmpexp;
        subrop->_mpfr_d = (void*)(((unsigned char*)MPS_LIMBS_ARRAY( rop ) +
                        sizeof(___mps_alloc_struct) + (nat_limbsize/8)*(i-1)));

    }

    /* If the limbsize is the same we can use a faster reading method. */
    if( limbsize == nat_limbsize )
    {
        for( i = 1; i <= matsize; i++ )
        {
            subrop = mps_get_ele_col( rop, i );
            nat_limb_ptr = subrop->_mpfr_d;

            for( limb_count = 0; limb_count < length; limb_count++ );
            {
                fread( (void*)&nat_tmp, GMP_LIMB_BITS/8, 1, file );

                if( ferror(file) != 0 )
                    return 1;

                if( nat_endian == endian )
                    nat_limb_ptr[limb_count] = nat_tmp;
                else if( GMP_LIMB_BITS == 64 )
                    nat_limb_ptr[limb_count] = MPS_SWAP_64( nat_tmp );
                else
                    nat_limb_ptr[limb_count] = MPS_SWAP_32( nat_tmp );
            }

        }
    }
    else
    {
        /* Not implemented for now. */
        return 1;
        
        for( i = 1; i <= matsize; i++ )
        {
            subrop = mps_get_ele_col( rop, i );
            limb_ptr = (mps_uint64_t*)(subrop->_mpfr_d);

            limb_count = limbsize / 32;

            if( header_info->_mps_file_precision % 64 < 32 )
            {
                /*
                 * When changing the limbsize we may have to add or remove an extra
                 * 32 bit of zeros.
                 */
                if( nat_limbsize == 32 )
                {
                    fread( (void*)&tmp, sizeof(mps_uint32_t), 2, file  );
                    mps_swap_to_native( &limb_ptr[0], &tmp, endian, limbsize );
                    ((mps_uint32_t*)limb_ptr)[0] = ((mps_uint32_t*)limb_ptr)[1];
                    limb_ptr = (mps_uint64_t*)((unsigned char *)limb_ptr + 32);
                    limb_count = limb_count - 1;
                }
                else
                {
                    tmp = 0;
                    fread( (void*)(&tmp_ptr[1]), sizeof(mps_uint32_t), 1, file  );
                    mps_swap_to_native( &limb_ptr[0], &tmp, endian, limbsize );
                    limb_ptr = (mps_uint64_t*)((unsigned char *)limb_ptr + 32);
                    limb_count = limb_count - 1;
                }

                if( ferror(file) != 0 )
                    return 1;

/* Removed temporarily. */
/*              for( limb_count; limb_count >= 0; limb_count -= 2 ) */
                {
                    fread( (void*)(&tmp_ptr[1]), sizeof(mps_uint32_t), 2, file );
                }

            }

        }
    }
    
    return 0;
}

/* Find at run time if the platform is big or little endian. */
mps_endian_t mps_get_endianness()
{
    unsigned int i = 1;
    char* ptr = (char*)&i;

    if (ptr[0] == 0)
        return MPS_BIG_ENDIAN;
    else
        return MPS_LITTLE_ENDIAN;
    
    return 0;
}

/* 
 * Evaluate byte-per-byte a crc32 checksum. It's using the precomputed table
 * in crc.h. The generator polynomial is the same as zlib :
 * x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 +
 * x^4 + x^2 + x + 1
 */
mps_uint32_t mps_crc32( mps_uint32_t crcin, unsigned char *op, size_t length )
{
    mps_uint32_t crcout;
    size_t i;

    crcout = crcin ^ 0xFFFFFFFF;
    
    for (i=0; i < length; i++)
    {
        crcout = (crcout >> 8) ^ crctable[ (crcout ^ op[i]) & 0xFF ];
    }
    
    return crcout ^ 0xFFFFFFFF ;
}

/*
 * Swap a 64 bit value from it's original ordering to the native ordering.
 * This is "necessary" because mpfr can store the significand both as an array
 * of 32 or 64 bit integers.
 *
 * To prevent unaligned access all copy are done in chunk of 32 bit except when
 * the limsize of the source and target match.
 *
 * NOTE : There is a shorter way of writing this, but a good compiler should
 * optimize it to pretty much the same code.
 */
void mps_swap_to_native( mps_uint64_t* rop,
                         mps_uint64_t* op,
                         mps_endian_t endian,
                         unsigned int limbsize )
{
    mps_endian_t nat_endian = mps_get_endianness();
    size_t nat_limbsize = GMP_LIMB_BITS;

    if( nat_endian == endian && nat_limbsize == limbsize )
    {
        /* Native format so we can copy as-is. */
        *rop = *op;
    }
    else if( nat_endian != endian && nat_limbsize == limbsize )
    {
        /* Only the endianness is different so we only need to swap each word. */
        if( nat_limbsize == 64 )
            *rop = MPS_SWAP_64( *op );
        else
        {
            ((mps_uint32_t*)rop)[0] = MPS_SWAP_32( ((mps_uint32_t*)op)[0] );
            ((mps_uint32_t*)rop)[1] = MPS_SWAP_32( ((mps_uint32_t*)op)[1] );
        }
    }
    else if( nat_endian == MPS_BIG_ENDIAN && nat_limbsize < limbsize )
    {
        if( endian == MPS_BIG_ENDIAN )
        {
            /* BIG <- BIG & 32 <- 64 */
            ((mps_uint32_t*)rop)[0] = ((mps_uint32_t*)op)[1];
            ((mps_uint32_t*)rop)[1] = ((mps_uint32_t*)op)[0];
        }
        else
        {
            /* BIG <- LIT & 32 <- 64 */
            ((mps_uint32_t*)rop)[0] = MPS_SWAP_32( ((mps_uint32_t*)op)[0] );
            ((mps_uint32_t*)rop)[1] = MPS_SWAP_32( ((mps_uint32_t*)op)[1] );
        }
    }
    else if( nat_endian == MPS_LITTLE_ENDIAN && nat_limbsize < limbsize )
    {
        if( endian == MPS_BIG_ENDIAN )
        {
            /* LIT <- BIG & 32 <- 64 */
            ((mps_uint32_t*)rop)[0] = MPS_SWAP_32( ((mps_uint32_t*)op)[1] );
            ((mps_uint32_t*)rop)[1] = MPS_SWAP_32( ((mps_uint32_t*)op)[0] );
        }
        else
        {
            /* LIT <- LIT & 32 <- 64 */
            ((mps_uint32_t*)rop)[0] = ((mps_uint32_t*)op)[0];
            ((mps_uint32_t*)rop)[1] = ((mps_uint32_t*)op)[1];
        }
        
    }
    else if( nat_endian == MPS_BIG_ENDIAN && nat_limbsize > limbsize )
    {
        if( endian == MPS_BIG_ENDIAN )
        {
            /* BIG <- BIG & 64 <- 32 */
            ((mps_uint32_t*)rop)[0] = ((mps_uint32_t*)op)[1];
            ((mps_uint32_t*)rop)[1] = ((mps_uint32_t*)op)[0];
        }
        else
        {
            /* BIG <- LIT & 64 <- 32 */
            ((mps_uint32_t*)rop)[0] = MPS_SWAP_32( ((mps_uint32_t*)op)[0] );
            ((mps_uint32_t*)rop)[1] = MPS_SWAP_32( ((mps_uint32_t*)op)[1] );
        }
    }
    else if( nat_endian == MPS_LITTLE_ENDIAN && nat_limbsize > limbsize )
    {
        if( endian == MPS_BIG_ENDIAN )
        {
            /* LIT <- BIG & 64 <- 32 */
            ((mps_uint32_t*)rop)[0] = MPS_SWAP_32( ((mps_uint32_t*)op)[1] );
            ((mps_uint32_t*)rop)[1] = MPS_SWAP_32( ((mps_uint32_t*)op)[0] );
        }
        else
        {
            /* LIT <- LIT & 64 <- 32 */
            ((mps_uint32_t*)rop)[0] = ((mps_uint32_t*)op)[0];
            ((mps_uint32_t*)rop)[1] = ((mps_uint32_t*)op)[1];
        }
    }
    
    return;
}
