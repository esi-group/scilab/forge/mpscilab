/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"


/* Set each element of rop to sqrt(op) */
int mps_sqrt( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_sqrt()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_sqrt( subrop, subop, rnd );
    }

    return 0;
}


int mps_sqrt_double( const mps_ptr rop, const double op[], const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_sqrt( subrop, subrop, rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_sqrt( subrop, subrop, rnd );
        }
    }

    return 0;
}


/* Set each element of rop to kth root of op */
int mps_root( const mps_ptr rop, const mps_ptr op, const unsigned long int k, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_root()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_root( subrop, subop, k, rnd );
    }

    return 0;
}


/* Set each element of rop to cube root op */
int mps_cbrt( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_cbrt()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_cbrt( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to 1 / sqrt(op) */
int mps_rec_sqrt( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rec_sqrt()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rec_sqrt( subrop, subop, rnd );
    }

    return 0;
}

