/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* Fills every element of op with 0's */
int mps_zero( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_set_ui( subop, 0, rnd );
    }

    return 0;
}

/* Fills every element of op with 1's */
int mps_ones( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_set_ui( subop, 1, rnd );
    }

    return 0;
}

/* Fills every element of op with pi */
int mps_const_pi( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_const_pi( subop, rnd );
    }

    return 0;
}

/* Fills every element of op with e */
int mps_const_e( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);
    mpfr_t one;

    mpfr_init2( one, 2 );
    mpfr_set_ui( one, 1, GMP_RNDN );

    for( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_exp( subop, one, rnd );
    }

    mpfr_clear( one );

    return 0;
}

/* Fills every element of op with euler constant */
int mps_const_euler( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_const_euler( subop, rnd );
    }

    return 0;
}

/* Fills every element of op with log 2 */
int mps_const_log2( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_const_log2( subop, rnd );
    }

    return 0;
}

/* Fills every element of op with catalan's constant */
int mps_const_catalan( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_const_catalan( subop, rnd );
    }

    return 0;
}

/* Fills op with the identity matrix */
int mps_identity( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    unsigned int sizeside = MPS_NUMROW(op);
    const unsigned int matsize = sizeside * sizeside;

	++sizeside;		/* use this to fill diagonal */

	MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
					"Result operand is not square in mps_identity()\n");

	/* fill it with 0's with 1's in diagonal */
    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
		if (i % sizeside != 1)
			mpfr_set_ui( subop, 0, rnd );
		else
			mpfr_set_ui( subop, 1, rnd );
    }

    return 0;
}

/* gives the rotation matrix for angle in rop.
   TODO: for a matrix not 2x2? */
int mps_rotation_2d( const mps_ptr rop, const mpfr_ptr angle, const mpfr_rnd_t rnd )
{
	mpfr_ptr subrop;

	MPS_ASSERT_MSG(MPS_IS_SQUARE(rop) && MPS_NUMROW(rop) == 2,
					"Expected 2x2 matrix in mps_rotation_2d()\n");

	subrop = mps_get_ele(rop, 1, 1);
	mpfr_cos(subrop, angle, rnd);

	subrop = mps_get_ele(rop, 1, 2);
	mpfr_sin(subrop, angle, rnd);
	mpfr_neg(subrop, subrop, rnd);

	subrop = mps_get_ele(rop, 2, 1);
	mpfr_sin(subrop, angle, rnd);

	subrop = mps_get_ele(rop, 2, 2);
	mpfr_cos(subrop, angle, rnd);

	return 0;
}

/* fill matrix sequentially from start in increments of step. */
int mps_fill_seq(const mps_ptr op,
                 const mpfr_ptr start,
                 const mpfr_ptr step,
                 const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	for ( i = 0; i < matsize; ++i )
	{
		subop = mps_get_ele_seq(op, i + 1);
		mpfr_mul_si(subop, step, i, rnd);
		mpfr_add(subop, subop, start, rnd);
	}

	return 0;
}

int mps_fill_seq_row(mps_ptr op, const mpfr_ptr start, const mpfr_ptr step, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	for ( i = 0; i < matsize; ++i )
	{
		subop = mps_get_ele_row(op, i + 1);
		mpfr_mul_si(subop, step, i, rnd);
		mpfr_add(subop, subop, start, rnd);
	}

	return 0;
}

int mps_fill_seq_si(const mps_ptr op, const int start, const int step, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_seq(op, i);
		mpfr_set_si(subop, start + ((int) i - 1) * step, rnd);
	}

	return 0;
}

int mps_fill_seq_row_si(const mps_ptr op, const int start, const int step, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_row(op, i);
		mpfr_set_si(subop, start + ((int) i - 1) * step, rnd);
	}

	return 0;
}

/* fill a matrix by some scale, e.g 1, 2, 4, 8, 16...*/
int mps_fill_seq_scale(const mps_ptr op, const int start, const int scale, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop1, subop2;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	subop1 = mps_get_ele_seq(op, 1);
	mpfr_set_si(subop1, start, rnd);

	for ( i = 2; i <= matsize; ++i )
	{
		subop1 = mps_get_ele_seq(op, i - 1);
		subop2 = mps_get_ele_seq(op, i);
		mpfr_mul_si(subop2, subop1, scale, rnd);
	}

	return 0;
}

/* Replace every NaN of op with 0's */
int mps_throw_nan( const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
        if ( mpfr_nan_p(subop) )
            mpfr_set_ui( subop, 0, rnd );
    }

    return 0;
}

