/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "mps_priv.h"
#include "mps_assert.h"

#include <stdarg.h>

/* global and static stuff. */
static mps_alloc_ptr ListHead = NULL;

unsigned int g_num_threads;


/*
 * Update of the global thread number. The variable is then flushed to propagate
 * the change to shared memory. This function should not be called from multiple
 * threads at the same time, in such a case the ordering of the updates is
 * undefined.
 */
void mps_set_num_threads( unsigned int num )
{
    g_num_threads = num;
    #ifdef WITH_OPENMP
        #pragma omp flush(g_num_threads)
    #endif
    return;
}


/*
 * Access function to get the number of threads desired globally. Semantically
 * a function should not depend on the return value of this function with
 * respect to a very recent update of the variable.
 * !Note the flush directive to propagate the thread value to everyone calling
 * this function.
 */
unsigned int mps_get_num_threads()
{
    #ifdef WITH_OPENMP
        #pragma omp flush(g_num_threads)
    #endif
    return g_num_threads;
}


/* Simple wrapper around omp_get_num_procs(). */
int mps_get_num_procs()
{
    #ifdef WITH_OPENMP
        return omp_get_num_procs();
    #else
        return 1;
    #endif
}


/* Return 1 if mplib was built with OpenMP support, 0 otherwise. */
int mps_openmp_p()
{
    #ifdef WITH_OPENMP
        return 1;
    #else
        return 0;
    #endif
}


/* Get various info from omp. */
mps_omp_struct mps_omp_status()
{
    mps_omp_struct omp;

    #ifdef WITH_OPENMP
        omp.num_threads = omp_get_num_threads();
        omp.max_threads = omp_get_max_threads();
        omp.thread_num = omp_get_thread_num();
        omp.num_procs = omp_get_num_procs();
        omp.in_parallel = omp_in_parallel();
        omp.dynamic = omp_get_dynamic();
        omp.nested = omp_get_nested();
        omp.thread_limit = omp_get_thread_limit();
        omp.max_active_levels = omp_get_max_active_levels();
        omp.level = omp_get_level();
        omp.ancestor_thread_num = omp_get_ancestor_thread_num( omp.level );
        omp.team_size = omp_get_team_size( omp.level );
        omp.active_level = omp_get_active_level();
    #else
        omp.num_threads = 0;
        omp.max_threads = 1;
        omp.thread_num = 0;
        omp.num_procs = 1;
        omp.in_parallel = 0;
        omp.dynamic = 0;
        omp.nested = 0;
        omp.thread_limit = 1;
        omp.max_active_levels = 0;
        omp.level = 0;
        omp.ancestor_thread_num = 0;
        omp.team_size = 1;
        omp.active_level = 0;
    #endif

    return omp;
}

/*
 *Allocate and initialize the space for a multiprecision matrix of mpfr_t.
 */
void * mps_alloc( const unsigned int n, const unsigned int m, const mpfr_prec_t precision )
{
    /* Declare and evaluate all the storage sizes required */
    unsigned int i;
    size_t mpsalloclistsize = sizeof(___mps_alloc_struct);
    size_t mpfrsize = sizeof(mpfr_t);
    size_t mpfrarraysize;

    size_t significandsize = mpfr_custom_get_size(precision);
    size_t limbsarraysize;

    void *mpfrarray_ptr;
    void *limbsarray_ptr;
    void *ptr;

    /* Calculate the space required. */
    mpfrarraysize = mpfrsize*n*m;
    limbsarraysize = significandsize*n*m + mpsalloclistsize;

    mpfrarray_ptr = malloc( mpfrarraysize );
    limbsarray_ptr = malloc( limbsarraysize );
    ptr = limbsarray_ptr;

    if( mpfrarray_ptr == NULL || limbsarray_ptr == NULL )
        return (void*)NULL;

    /* Protect the list of allocated matrices from concurrent access. */
    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    /* Setup all of the fields */
    MPS_NEXT( (mps_alloc_ptr)limbsarray_ptr ) = ListHead;
    MPS_PREV( (mps_alloc_ptr)limbsarray_ptr ) = NULL;
    MPS_MPFR_ARRAY( (mps_alloc_ptr)limbsarray_ptr ) = mpfrarray_ptr;
    MPS_LIMBS_ARRAY( (mps_alloc_ptr)limbsarray_ptr ) = limbsarray_ptr;
    MPS_ALLOC_SIZE( (mps_alloc_ptr)limbsarray_ptr ) = mpfrarraysize;
    MPS_LIMBS_ALLOC_SIZE( (mps_alloc_ptr)limbsarray_ptr ) = limbsarraysize;

    /* Insert the new matrix at the top of the list. */
    if ( ListHead != NULL )
        MPS_PREV( ListHead ) = limbsarray_ptr;

    /* Update the list head. */
    ListHead = (mps_alloc_ptr)limbsarray_ptr;

    #ifdef WITH_OPENMP
        }
    #endif

    /* Init the whole matrix to 0. */
    limbsarray_ptr = (unsigned char*)limbsarray_ptr + mpsalloclistsize;

    for ( i = 0; i < n*m; i++ )
    {
        mpfr_custom_init_set( mpfrarray_ptr, MPFR_ZERO_KIND, 0, precision, limbsarray_ptr );
        /* mpfr_set_d( mpfrarray_ptr, 0, GMP_RNDN ); */
        mpfrarray_ptr = (unsigned char*)mpfrarray_ptr + mpfrsize;
        limbsarray_ptr = (unsigned char*)limbsarray_ptr + significandsize;
    }

    return  ptr;
}


int mps_init( mps_ptr mps,
               const unsigned int n,
               const unsigned int m,
               const mpfr_prec_t precision,
               const mps_order_t order )
{
    mps_alloc_ptr ptr = mps_alloc( n, m, precision );

    if( ptr == NULL )
        return 1;

    /* Setup all of the fields */
    MPS_MPFR_ARRAY( mps ) = MPS_MPFR_ARRAY( ptr );
    MPS_LIMBS_ARRAY( mps ) = ptr;
    MPS_ALLOC_SIZE( mps ) = MPS_ALLOC_SIZE( ptr );
    MPS_LIMBS_ALLOC_SIZE( mps ) = MPS_LIMBS_ALLOC_SIZE( ptr );
    MPS_TYPE( mps ) = 0;
    MPS_PREC( mps ) = precision;
    MPS_NUMROW( mps ) = n;
    MPS_NUMCOL( mps ) = m;
	mps_set_order( mps, order );

    return 0;
}


/* Variable argument matrix initializtion. Last arg must be mps_ptr type and null */
void mps_inits(const unsigned int n,
               const unsigned int m,
               const mpfr_prec_t prec,
               const mps_order_t order, mps_ptr x, ... )
{
	va_list arg;

	va_start(arg, x);

	while ( x != NULL )
	{
		mps_init(x, n, m, prec, order);
		x = (mps_ptr) va_arg(arg, mps_ptr);
	}
	va_end(arg);
}


/*
 * Free the memory used by an mps matrix.
 */
int mps_free( const mps_ptr mpsptr )
{
    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr limbs_array = MPS_LIMBS_ARRAY( mpsptr );
    void * mpfr_array = MPS_MPFR_ARRAY( limbs_array );
    mps_alloc_ptr nextptr = MPS_NEXT( limbs_array );
    mps_alloc_ptr prevptr = MPS_PREV( limbs_array );

    /*
     * Update the previous element in the list if it exist
     * if not than update the head pointer.
     */
    if ( prevptr == NULL )
        ListHead = nextptr;
    else
        MPS_NEXT( prevptr ) = nextptr;

    /* Update the next element if it exist.*/
    if ( nextptr != NULL )
        MPS_PREV( nextptr ) = prevptr;

    /* Free the allocated memory. */

    /* Free the mpfr array. */
    if ( mpfr_array != NULL )
        free( mpfr_array );

    /* Free the limbs array. */
    if ( limbs_array != NULL )
        free( limbs_array );

    MPS_MPFR_ARRAY( mpsptr ) = NULL;
    MPS_LIMBS_ARRAY( mpsptr ) = NULL;

    #ifdef WITH_OPENMP
        }
    #endif

    return 0;
}


/*
 * Free the memory used by an mps matrix from its alloc struct.
 * Mostly for the usage of the garbage collector.
 */
int mps_free_alloc( const mps_alloc_ptr limbs_array )
{
    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    void * mpfr_array = MPS_MPFR_ARRAY( limbs_array );
    mps_alloc_ptr nextptr = MPS_NEXT( limbs_array );
    mps_alloc_ptr prevptr = MPS_PREV( limbs_array );

    /*
     * Update the previous element in the list if it exist
     * if not than update the head pointer.
     */
    if ( prevptr == NULL )
        ListHead = nextptr;
    else
        MPS_NEXT( prevptr ) = nextptr;

    /* Update the next element if it exist.*/
    if ( nextptr != NULL )
        MPS_PREV( nextptr ) = prevptr;

    /* Free the allocated memory. */

    /* Free the mpfr array. */
    if ( mpfr_array != NULL )
        free( mpfr_array );

    /* Free the limbs array. */
    if ( limbs_array != NULL )
        free( limbs_array );

    #ifdef WITH_OPENMP
        }
    #endif    
    return 0;
}


/* modified from mpfr_clears. Last arg must be mps_ptr type and null */
void mps_frees(mps_ptr x, ...)
{
	va_list arg;
	va_start(arg, x);

	while (x != NULL)
	{
		mps_free(x);
		x = (mps_ptr) va_arg(arg, mps_ptr);
	}
	va_end(arg);
}


/*
 * Free the memory used by all currently allocated matrices.
 */
int mps_free_all()
{
    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while ( ptr != NULL )
    {
        ListHead = MPS_NEXT( ptr );
        /*Free the allocated memory. */

        /* Free the mpfr array. */
        if ( MPS_MPFR_ARRAY( ptr ) != NULL )
            free( MPS_MPFR_ARRAY( ptr ) );

        /* Free the limbs array. */
        free( MPS_LIMBS_ARRAY( ptr ) );

        ptr = ListHead;
    }

    ListHead = NULL;

    #ifdef WITH_OPENMP
        }
    #endif

    return 0;
}


/*
 * Search the list of allocated matrices. Return non-zero if op is in the list.
 * If 0 is returned it most probably mean the the mps_t was never allocated.
 * Warning : Extensive comparison is done to make sure that the matrix exist,
 * as such calling this function on long list might be slow.
 */
int mps_exist( const mps_ptr op )
{
    int found = 0;

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while ( ptr != NULL )
    {
        if ( MPS_MPFR_ARRAY(op) == MPS_MPFR_ARRAY(ptr) &&
            MPS_LIMBS_ARRAY(op) == MPS_LIMBS_ARRAY(ptr) &&
            MPS_ALLOC_SIZE(op) == MPS_ALLOC_SIZE(ptr) &&
            MPS_LIMBS_ALLOC_SIZE(op) == MPS_LIMBS_ALLOC_SIZE(ptr) )
        {
            found = 1;
            break;
        }

        ptr = MPS_NEXT( ptr );
    }

    #ifdef WITH_OPENMP
        }
    #endif

    return found;
}

int mps_position( const mps_ptr op )
{
    int count = -1;
    int position = -1;

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while ( ptr != NULL )
    {
        count++;
        if ( MPS_MPFR_ARRAY(op) == MPS_MPFR_ARRAY(ptr) &&
            MPS_LIMBS_ARRAY(op) == MPS_LIMBS_ARRAY(ptr) &&
            MPS_ALLOC_SIZE(op) == MPS_ALLOC_SIZE(ptr) &&
            MPS_LIMBS_ALLOC_SIZE(op) == MPS_LIMBS_ALLOC_SIZE(ptr) )
        {
            position = count;
            break;
        }

        ptr = MPS_NEXT( ptr );
    }

    #ifdef WITH_OPENMP
        }
    #endif

    return position;
}


mps_alloc_ptr mps_return_alloc_pos( const int pos )
{
    int count = -1;
    mps_alloc_ptr res = NULL;

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while ( ptr != NULL )
    {
        count++;
        if ( count == pos )
        {
            res = ptr;
            break;
        }

        ptr = MPS_NEXT( ptr );
    }

    #ifdef WITH_OPENMP
        }
    #endif

    return res;
}


mps_alloc_ptr mps_return_alloc_list_head()
{
    return ListHead;
}


/* Copy an mps matrix into another one. Must be exactly of the same size and precision. */
int mps_copy( mps_ptr rop, const mps_ptr op )
{
    size_t list_s = sizeof(___mps_alloc_struct);
    size_t offset;
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Operands differ in dimension in mps_copy()\n" );

    MPS_ASSERT_MSG( MPS_SAME_PREC(rop,op),
        "Operands differ in precision in mps_copy()\n" );

    MPS_ASSERT_MSG( MPS_SAME_ALLOC(rop,op),
        "Operands differ in allocated size in mps_copy()\n" );

    MPS_ASSERT_MSG( MPS_NOT_ALIAS(rop,op),
        "Result operand alias the argument in mps_copy()\n" );

    /* Memcpy the mpfr_array. */
    memcpy( MPS_MPFR_ARRAY( rop ), MPS_MPFR_ARRAY( op ), MPS_ALLOC_SIZE( op ) );

    /* Memcpy the limbs array. */
    memcpy( (unsigned char*)MPS_LIMBS_ARRAY( rop ) + list_s, (unsigned char*)MPS_LIMBS_ARRAY( op ) + list_s,
        MPS_LIMBS_ALLOC_SIZE( op ) - list_s );

    /* Update the mpfr_t limbs pointer. (type-cast party.) */
    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele_seq( op, i );

        /* Calculate the offset between the element limbs position and the top of the array of limbs */
        offset = (unsigned char*)subop->_mpfr_d - (unsigned char*)MPS_LIMBS_ARRAY( op );

        /* Update the coresponding result element with the same offset. */
        subrop->_mpfr_d = (mp_limb_t*)((unsigned char*)MPS_LIMBS_ARRAY( rop ) + offset);
    }

    /* Set the type flags. */
    MPS_TYPE( rop ) = MPS_TYPE( op );

    return 0;
}


/*
 * Same as mps_copy but the result matrix will be resized if it is not the same
 * size. rop must be initialized though.
 */
int mps_clone( mps_ptr rop, const mps_ptr op )
{
    MPS_ASSERT_MSG( MPS_NOT_ALIAS(rop,op),
        "Result operand alias the argument in mps_clone()\n" );

    mps_set( rop, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), MPS_GET_ORDER(op) );

    mps_copy( rop, op );

    return 0;
}


int mps_isalias( const mps_ptr op1, const mps_ptr op2 )
{
    if ( MPS_MPFR_ARRAY(op1) == MPS_MPFR_ARRAY(op2) )
        return 1;

    return 0;
}


/* swaps 2 matrices */
int mps_swap( mps_ptr op1, mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_swap()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subop1 = mps_get_ele_seq( op1, i );
        subop2 = mps_get_ele_seq( op2, i );
        mpfr_swap( subop1, subop2 );
    }

    return 0;
}


int mps_set_copy( mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop, subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Operands differ in dimension in mps_copy()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele_seq( op, i );

        mpfr_set( subrop, subop, rnd );
    }

    return 0;
}


/*
 * Set the size and configuration of a matrix. Realloc if required.
 * May destroy the data contained in the matrix.
 */
int mps_set( mps_ptr mpsptr,
             const unsigned int m,
             const unsigned int n,
             const mpfr_prec_t precision,
             const mps_order_t order )
{
    unsigned int i, size;
    /* Declare and evaluate all the storage sizes required */
    size_t mpsalloclistsize = sizeof(___mps_alloc_struct);
    size_t mpfrsize = sizeof(mpfr_t);
    size_t mpfrarraysize;

    void * mpfr_array_ptr = MPS_MPFR_ARRAY( mpsptr );
    void * limbs_array_ptr = MPS_LIMBS_ARRAY( mpsptr );

    mps_alloc_ptr limbsptr = MPS_LIMBS_ARRAY( mpsptr );

    size_t significandsize = mpfr_custom_get_size(precision);
    size_t limbsarraysize;

    mps_alloc_ptr nextptr = NULL;
    mps_alloc_ptr prevptr = NULL;

    void *headerptr;
    void *dataptr;

    /* Quickly return if the matrix is already set as specified. */
    if ((MPS_NUMROW(mpsptr) == m) && (MPS_NUMCOL(mpsptr) == n) &&
        (MPS_PREC(mpsptr) == precision) && (MPS_GET_ORDER(mpsptr) == order))
    {
        return 0;
    }

    size = m * n;
    /* Calculate the space required. */
    mpfrarraysize = mpfrsize * size;
    limbsarraysize = significandsize * size + mpsalloclistsize;

    /*
     * Check if the allocated size is exactly the amount needed for the mpfr array.
     */
    if( MPS_ALLOC_SIZE( mpsptr ) != mpfrarraysize )
    {
        mpfr_array_ptr = realloc( mpfr_array_ptr, mpfrarraysize );
    }

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    nextptr = MPS_NEXT( limbsptr );
    prevptr = MPS_PREV( limbsptr );

    /* Check for the allocated size for the data. */
    if ( MPS_LIMBS_ALLOC_SIZE( mpsptr ) != limbsarraysize )
    {

        limbs_array_ptr = realloc( limbs_array_ptr, limbsarraysize );

        /*
         * Update the previous element in the list if it exist
         * if not than update the head pointer.
         */
        if ( prevptr == NULL )
            ListHead = limbs_array_ptr;
        else
            MPS_NEXT( prevptr ) = limbs_array_ptr;

        /* Update the next element if it exist.*/
        if ( nextptr != NULL )
            MPS_PREV( nextptr ) = limbs_array_ptr;
    }

    /* Set the metadata. */
    MPS_NUMROW( mpsptr ) = m;
    MPS_NUMCOL( mpsptr ) = n;
    MPS_PREC( mpsptr ) = precision;
    MPS_MPFR_ARRAY( mpsptr ) = mpfr_array_ptr;
    MPS_LIMBS_ARRAY( mpsptr ) = limbs_array_ptr;
    MPS_ALLOC_SIZE( mpsptr ) = mpfrarraysize;
    MPS_LIMBS_ALLOC_SIZE( mpsptr ) = limbsarraysize;
    mps_set_order( mpsptr, order );

    MPS_MPFR_ARRAY( (mps_alloc_ptr)limbs_array_ptr ) = mpfr_array_ptr;
    MPS_LIMBS_ARRAY( (mps_alloc_ptr)limbs_array_ptr ) = limbs_array_ptr;
    MPS_ALLOC_SIZE( (mps_alloc_ptr)limbs_array_ptr ) = mpfrarraysize;
    MPS_LIMBS_ALLOC_SIZE( (mps_alloc_ptr)limbs_array_ptr ) = limbsarraysize;
    MPS_NEXT( (mps_alloc_ptr)limbs_array_ptr ) = nextptr;
    MPS_PREV( (mps_alloc_ptr)limbs_array_ptr ) = prevptr;

    #ifdef WITH_OPENMP
        }
    #endif

    headerptr = mpfr_array_ptr;
    dataptr = (unsigned char*)limbs_array_ptr + mpsalloclistsize;

    for ( i = 0; i < size; ++i )
    {
        mpfr_custom_init_set( headerptr, MPFR_ZERO_KIND, 0, precision, dataptr );
        mpfr_set_ui( headerptr, 0, GMP_RNDN );
        headerptr = (unsigned char*)headerptr + mpfrsize;
        dataptr = (unsigned char*)dataptr + significandsize;
    }

    return 0;
}


/*
 * Swap the dimensions of a matrix whitout changing anything else.
 * Generally used by in place transposition functions.
 */
void mps_exg_dim( mps_ptr mps )
{
    const unsigned int rowsize = MPS_NUMROW(mps);

    MPS_NUMROW(mps) = MPS_NUMCOL(mps);
    MPS_NUMCOL(mps) = rowsize;

    return;
}


/*
 * Count the number of allocated matrices.
 */
unsigned int mps_alloc_list_size()
{
    unsigned int count = 0;

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while( ptr != NULL )
    {
        ++count;
        ptr = MPS_NEXT(ptr);
    }

    #ifdef WITH_OPENMP
        }
    #endif

    return count;
}


/*
 * Count the memory used by all the matrices.
 */
size_t mps_alloc_size()
{
    size_t count = 0;

    #ifdef WITH_OPENMP
        #pragma omp critical (alloc_list_access)
        {
    #endif

    mps_alloc_ptr ptr = ListHead;

    while( ptr != NULL )
    {
        count = count + MPS_LIMBS_ALLOC_SIZE(ptr) + MPS_ALLOC_SIZE(ptr);
        ptr = MPS_NEXT(ptr);
    }

    #ifdef WITH_OPENMP
        }
    #endif

    return count;
}


/* Return a size estimate based on the given parameters. */
size_t mps_size_est( const unsigned int n, const unsigned int m, const mpfr_prec_t precision )
{
    size_t significandsize = mpfr_custom_get_size(precision);

    /* Calculate the space required. */
    return (sizeof(mpfr_t) + significandsize) * n * m + sizeof(___mps_alloc_struct);
}

