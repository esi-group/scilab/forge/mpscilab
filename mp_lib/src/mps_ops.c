/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Exchange two elements of a matrix by coordinate. */
int mps_coord_exg( const mps_ptr mpsptr,
                   const unsigned int row1,
                   const unsigned int col1,
                   const unsigned int row2,
                   const unsigned int col2 )
{
    mpfr_ptr subop1, subop2;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_coord_exg()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col1),
        "First column index out of bound in mps_coord_exg()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_coord_exg()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col2),
        "Second column index out of bound in mps_coord_exg()\n" );

    subop1 = mps_get_ele( mpsptr, row1, col1 );
    subop2 = mps_get_ele( mpsptr, row2, col2 );

    mps_ele_fastswap( subop1, subop2 );

    return 0;
}

/* Exchange two elements of a matrix by index. */
int mps_indx_exg( const mps_ptr mpsptr, const unsigned int indx1, const unsigned int indx2 )
{
    mpfr_ptr subop1, subop2;

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx1),
        "First index out of bound in mps_indx_exg()\n" );

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx2),
        "Second index out of bound in mps_indx_exg()\n" );

    subop1 = mps_get_ele_seq( mpsptr, indx1 );
    subop2 = mps_get_ele_seq( mpsptr, indx2 );

    mps_ele_fastswap( subop1, subop2 );

    return 0;
}

/* Exchange two rows of the same matrix. */
int mps_row_exg( const mps_ptr mpsptr, const unsigned int row1, const unsigned int row2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_row_exg()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_row_exg()\n" );

    if( row1 == row2 )
        return 0;

    for( i = 1; i <= numcol; ++i )
    {
        subop1 = mps_get_ele( mpsptr, row1, i );
        subop2 = mps_get_ele( mpsptr, row2, i );

        mps_ele_fastswap( subop1, subop2 );
    }

    return 0;
}

/* Scale a row by an mpfr constant. */
int mps_row_scale_mpfr( const mps_ptr mpsptr,
                        const unsigned int row,
                        const mpfr_ptr scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_row_scale_mpfr()\n" );

    for( i = 1; i <= numcol; ++i )
    {
        subop = mps_get_ele( mpsptr, row, i );

        mpfr_mul( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Scale a row by a double constant. */
int mps_row_scale_double( const mps_ptr mpsptr,
                          const unsigned int row,
                          const double scaleop,
                          const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_row_scale_double()\n" );

    for ( i = 1; i <= numcol; ++i )
    {
        subop = mps_get_ele( mpsptr, row, i );
        mpfr_mul_d( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Divide a row by an mpfr constant. */
int mps_row_div_mpfr( const mps_ptr mpsptr,
                      const unsigned int row,
                      const mpfr_ptr scaleop,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_row_div_mpfr()\n" );

    for ( i = 1; i <= numcol; ++i )
    {
        subop = mps_get_ele( mpsptr, row, i );
        mpfr_div( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Divide a row by a double constant. */
int mps_row_div_double( const mps_ptr mpsptr,
                        const unsigned int row,
                        const double scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_row_div_double()\n" );

    for ( i = 1; i <= numcol; ++i )
    {
        subop = mps_get_ele( mpsptr, row, i );
        mpfr_div_d( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Add a scaled version of a row to another row. */
int mps_row_add_mpfr( const mps_ptr mpsptr,
                      const unsigned int row1,
                      const unsigned int row2,
                      const mpfr_ptr scaleop,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_row_add_mpfr()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_row_add_mpfr()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for( i = 1; i <= numcol; ++i )
    {
        subop1 = mps_get_ele( mpsptr, row1, i );
        subop2 = mps_get_ele( mpsptr, row2, i );

        mpfr_mul( tempop, scaleop, subop2, rnd );

        mpfr_add( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}

/* Add a scaled version of a row to another row. */
int mps_row_add_double( const mps_ptr mpsptr,
                        const unsigned int row1,
                        const unsigned int row2,
                        const double scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_row_add_double()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_row_add_double()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for ( i = 1; i <= numcol; ++i )
    {
        subop1 = mps_get_ele( mpsptr, row1, i );
        subop2 = mps_get_ele( mpsptr, row2, i );

        mpfr_mul_d( tempop, subop2, scaleop, rnd );
        mpfr_add( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}

/* Subtract a scaled version of a row to another row. */
int mps_row_sub_mpfr( const mps_ptr mpsptr,
                      const unsigned int row1,
                      const unsigned int row2,
                      const mpfr_ptr scaleop,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
    unsigned int i;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_row_sub_mpfr()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_row_sub_mpfr()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for ( i = 1; i <= numcol; ++i )
    {
        subop1 = mps_get_ele( mpsptr, row1, i );
        subop2 = mps_get_ele( mpsptr, row2, i );

        mpfr_mul( tempop, scaleop, subop2, rnd );

        mpfr_sub( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}

/* Subtract a scaled version of a row to another row. */
int mps_row_sub_double( const mps_ptr mpsptr,
                        const unsigned int row1,
                        const unsigned int row2,
                        const double scaleop,
                        const mpfr_rnd_t rnd )
{
    unsigned int i;
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
	const unsigned int numcol = MPS_NUMCOL(mpsptr);

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row1),
        "First row index out of bound in mps_row_sub_double()\n" );

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row2),
        "Second row index out of bound in mps_row_sub_double()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for ( i = 1; i <= numcol; ++i )
    {
        subop1 = mps_get_ele( mpsptr, row1, i );
        subop2 = mps_get_ele( mpsptr, row2, i );

        mpfr_mul_d( tempop, subop2, scaleop, rnd );

        mpfr_sub( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}

/* Exchange two columns of the same matrix. */
int mps_col_exg( const mps_ptr mpsptr, const unsigned int col1, const unsigned int col2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col1),
        "First column index out of bound in mps_col_exg()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col2),
        "Second column index out of bound in mps_col_exg()\n" );

    if( col1 == col2 )
        return 0;

    for ( i = 1; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( mpsptr, i, col1 );
        subop2 = mps_get_ele( mpsptr, i, col2 );

        mps_ele_fastswap( subop1, subop2 );
    }

    return 0;
}

/* Scale a column by an mpfr constant. */
int mps_col_scale_mpfr( const mps_ptr mpsptr,
                        const unsigned int col,
                        const mpfr_ptr scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_col_scale_mpfr()\n" );

    for ( i = 1; i <= numrow; ++i )
    {
        subop = mps_get_ele( mpsptr, i, col );
        mpfr_mul( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Scale a column by a double constant. */
int mps_col_scale_double( const mps_ptr mpsptr,
                          const unsigned int col,
                          const double scaleop,
                          const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_col_scale_double()\n" );

    for ( i = 1; i <= numrow; ++i )
    {
        subop = mps_get_ele( mpsptr, i, col );
        mpfr_mul_d( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Divide a column by an mpfr constant. */
int mps_col_div_mpfr( const mps_ptr mpsptr,
                        const unsigned int col,
                        const mpfr_ptr scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_col_scale_mpfr()\n" );

    for ( i = 1; i <= numrow; ++i )
    {
        subop = mps_get_ele( mpsptr, i, col );
        mpfr_div( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Divide a column by a double constant. */
int mps_col_div_double( const mps_ptr mpsptr,
                          const unsigned int col,
                          const double scaleop,
                          const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_col_scale_double()\n" );

    for ( i = 1; i <= numrow; ++i )
    {
        subop = mps_get_ele( mpsptr, i, col );
        mpfr_div_d( subop, subop, scaleop, rnd );
    }

    return 0;
}

/* Add a scaled version of a column to another column. */
int mps_col_add_mpfr( const mps_ptr mpsptr,
                      const unsigned int col1,
                      const unsigned int col2,
                      const mpfr_ptr scaleop,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col1),
        "First column index out of bound in mps_col_add_mpfr()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col2),
        "Second column index out of bound in mps_col_add_mpfr()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for( i = 1; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( mpsptr, i, col1 );
        subop2 = mps_get_ele( mpsptr, i, col2 );

        mpfr_mul( tempop, scaleop, subop2, rnd );

        mpfr_add( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}

/* Add a scaled version of a column to another column. */
int mps_col_add_double( const mps_ptr mpsptr,
                        const unsigned int col1,
                        const unsigned int col2,
                        const double scaleop,
                        const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t tempop;
    unsigned int i;
	const unsigned int numrow = MPS_NUMROW(mpsptr);

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col1),
        "First row index out of bound in mps_col_add_double()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col2),
        "Second row index out of bound in mps_col_add_double()\n" );

    mpfr_init2( tempop, MPS_PREC(mpsptr) );

    for ( i = 1; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( mpsptr, i, col1 );
        subop2 = mps_get_ele( mpsptr, i, col2 );

        mpfr_mul_d( tempop, subop2, scaleop, rnd );
        mpfr_add( subop1, subop1, tempop, rnd );
    }

    mpfr_clear( tempop );

    return 0;
}
