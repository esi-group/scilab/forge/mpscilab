/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"


/* fill a matrix with random numbers from rand()
	TODO: more options with the range, seed and such too
	TODO: this is basically 2 copies of the same thing which is dumb
	0 to RAND_MAX if min == max */
int mps_random(const mps_ptr op, const int min, const int max, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	if (min == max)
	{
		for ( i = 1; i <= matsize; ++i)
		{
			subop = mps_get_ele_seq(op, i);
			mpfr_set_si(subop, rand(), rnd);
		}
	}
	else
	{
		for ( i = 1; i <= matsize; ++i)
		{
			subop = mps_get_ele_seq(op, i);
			mpfr_set_si(subop, rand() % max + min, rnd);
		}
	}

	return 0;
}


/* identical to mps_random except using srand with seed */
int mps_srandom(const mps_ptr op,
                const int min,
                const int max,
                const unsigned int seed,
                const mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	srand(seed);

	if (min == max)
	{
		for ( i = 1; i <= matsize; ++i)
		{
			subop = mps_get_ele_seq(op, i);
			mpfr_set_si(subop, rand(), rnd);
		}
	}
	else
	{
		for ( i = 1; i <= matsize; ++i)
		{
			subop = mps_get_ele_seq(op, i);
			mpfr_set_si(subop, rand() % max + min, rnd);
		}
	}

	return 0;
}


int mps_urandomb( const mps_ptr rop, gmp_randstate_t state )
{
	mpfr_ptr subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq(rop, i);
		mpfr_urandomb(subrop, state);
	}

	return 0;
}


int mps_urandom( const mps_ptr rop, gmp_randstate_t state )
{
	mpfr_ptr subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq(rop, i);
		mpfr_urandom(subrop, state, GMP_RNDN);
	}

	return 0;
}


/* Uniform distribution [0,1]. */
int mps_rand_def( const mps_ptr rop, gmp_randstate_t state )
{
	mpfr_ptr subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq( rop, i );
		mpfr_urandom( subrop, state, GMP_RNDZ );
	}

    return 0;
}


/* Uniform distribution [low,high]. */
int mps_rand_unf( const mps_ptr rop, const mps_ptr mpslow, const mps_ptr mpshigh, gmp_randstate_t state )
{
	mpfr_ptr subrop, high, low;
    mpfr_t tmp;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpslow),
        "mpslow not a scalar in mps_rand_unf()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpshigh),
        "mpshigh not a scalar in mps_rand_unf()\n" );

    high = mps_get_ele( mpshigh, 1, 1 );
    low = mps_get_ele( mpslow, 1, 1 );

    mpfr_init2( tmp, MPS_PREC(rop) );
    mpfr_sub( tmp, high, low, GMP_RNDZ );

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq( rop, i );
		mpfr_urandom( subrop, state, GMP_RNDN );
        mpfr_mul( subrop, subrop, tmp, GMP_RNDN );
        mpfr_add( subrop, subrop, low, GMP_RNDN );
	}

    mpfr_clear( tmp );

    return 0;
}


/* Uniform distribution [low,high] using doubles for the interval. */
int mps_rand_unf_double( const mps_ptr rop, double low, double high, gmp_randstate_t state )
{
	mpfr_ptr subrop;
    mpfr_t tmp;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    mpfr_init2( tmp, MPS_PREC(rop) );

    mpfr_set_d( tmp, high, GMP_RNDN );
    mpfr_sub_d( tmp, tmp, low, GMP_RNDN );

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq( rop, i );
		mpfr_urandom( subrop, state, GMP_RNDZ );
        mpfr_mul( subrop, subrop, tmp, GMP_RNDN );
        mpfr_add_d( subrop, subrop, low, GMP_RNDN );
    }

    mpfr_clear( tmp );

    return 0;
}


/* Normal distribution using the Box-Muller transform.  */
int mps_rand_nor( const mps_ptr rop, const mps_ptr mpsmean, const mps_ptr mpsvariance, gmp_randstate_t state )
{
	mpfr_ptr subrop, mean, variance;
    mpfr_t u2, pi2;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsmean),
        "mpsmean not a scalar in mps_rand_nor()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsvariance),
        "mpsvariance not a scalar in mps_rand_nor()\n" );

    mpfr_init2( u2, MPS_PREC(rop) );
    mpfr_init2( pi2, MPS_PREC(rop) );

    mpfr_const_pi( pi2, GMP_RNDN );
    mpfr_mul_d( pi2, pi2, 2, GMP_RNDN );

    mean = mps_get_ele( mpsmean, 1, 1 );
    variance = mps_get_ele( mpsvariance, 1, 1 );

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq( rop, i );
		mpfr_urandom( subrop, state, GMP_RNDN );
		mpfr_urandom( u2, state, GMP_RNDN );

        /* cos(2*pi*U1) */
        mpfr_mul( subrop, subrop, pi2, GMP_RNDN );
        mpfr_cos( subrop, subrop, GMP_RNDN );

        /* sqrt(-2*ln(U2)) */
        mpfr_log( u2, u2, GMP_RNDN );
        mpfr_mul_d( u2, u2, -2, GMP_RNDN );
        mpfr_sqrt( u2, u2, GMP_RNDN );

        mpfr_mul( subrop, subrop, u2, GMP_RNDN );

        mpfr_mul( subrop, subrop, variance, GMP_RNDN );
        mpfr_add( subrop, subrop, mean, GMP_RNDN );
        
    }

    mpfr_clear( u2 );
    mpfr_clear( pi2 );

    return 0;
}


/* Normal distribution using the Box-Muller transform using doubles for the mean and variance  */
int mps_rand_nor_double( const mps_ptr rop, double mean, double variance, gmp_randstate_t state )
{
	mpfr_ptr subrop, nextrop;
    mpfr_t u2, pi2;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    mpfr_init2( u2, MPS_PREC(rop) );
    mpfr_init2( pi2, MPS_PREC(rop) );

    mpfr_const_pi( pi2, GMP_RNDN );
    mpfr_mul_d( pi2, pi2, 2, GMP_RNDN );

	for ( i = 1; i <= matsize; i = i + 2 )
	{
		subrop = mps_get_ele_seq( rop, i );
		mpfr_urandom( subrop, state, GMP_RNDN );
		mpfr_urandom( u2, state, GMP_RNDN );

        /* sqrt(-2*ln(U2)) */
        mpfr_log( u2, u2, GMP_RNDN );
        mpfr_mul_d( u2, u2, -2, GMP_RNDN );
        mpfr_sqrt( u2, u2, GMP_RNDN );

        /* cos(2*pi*U1) */
        mpfr_mul( subrop, subrop, pi2, GMP_RNDN );

        if( i + 1 <= matsize )
        {
            nextrop = mps_get_ele_seq( rop, i+1 );
            mpfr_sin_cos( subrop, nextrop, subrop, GMP_RNDN );

            mpfr_mul( nextrop, nextrop, u2, GMP_RNDN );

            mpfr_mul_d( nextrop, nextrop, variance, GMP_RNDN );
            mpfr_add_d( nextrop, nextrop, mean, GMP_RNDN );
        }
        else
            mpfr_cos( subrop, subrop, GMP_RNDN );

        mpfr_mul( subrop, subrop, u2, GMP_RNDN );

        mpfr_mul_d( subrop, subrop, variance, GMP_RNDN );
        mpfr_add_d( subrop, subrop, mean, GMP_RNDN );
    }

    mpfr_clear( u2 );
    mpfr_clear( pi2 );

    return 0;
}


/* Normal distribution using the Marsaglia polar method.  */
int mps_rand_nor1( const mps_ptr rop, const mps_ptr mpsmean, const mps_ptr mpsvariance, gmp_randstate_t state )
{
	mpfr_ptr subrop, mean, variance;
    mpfr_t x, y, s;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsmean),
        "mpsmean not a scalar in mps_rand_nor1()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsvariance),
        "mpsvariance not a scalar in mps_rand_nor1()\n" );

    mean = mps_get_ele( mpsmean, 1, 1 );
    variance = mps_get_ele( mpsvariance, 1, 1 );

    mpfr_init2( x, MPS_PREC(rop) );
    mpfr_init2( y, MPS_PREC(rop) );
    mpfr_init2( s, MPS_PREC(rop) );

	for ( i = 1; i <= matsize; i = i + 2 )
	{
        do
        {
		    subrop = mps_get_ele_seq( rop, i );
            mpfr_urandom( x, state, GMP_RNDZ );
            mpfr_urandom( y, state, GMP_RNDZ );

            /* Scale the [0,1] uniform distribution to [-1,1] */
            mpfr_mul_d( x, x, 2, GMP_RNDN );
            mpfr_sub_d( x, x, 1, GMP_RNDN );
            mpfr_mul_d( y, y, 2, GMP_RNDN );
            mpfr_sub_d( y, y, 1, GMP_RNDN );
            
            /* s = x**2 + y**2 */
            mpfr_sqr( s, y, GMP_RNDN );
            mpfr_sqr( subrop, x, GMP_RNDN );
            mpfr_add( s, s, subrop, GMP_RNDN );

        } while( mpfr_cmp_d( s, 1 ) >= 0 || mpfr_zero_p(s) );

        /* x*sqrt(-2*ln(s)/s) */
        mpfr_log( subrop, s, GMP_RNDN );
        mpfr_mul_d( subrop, subrop, -2, GMP_RNDN );
        mpfr_div( s, subrop, s, GMP_RNDN );
        mpfr_sqrt( s, s, GMP_RNDN);
        mpfr_mul( subrop, s, x, GMP_RNDN );

        mpfr_mul( subrop, subrop, variance, GMP_RNDN );
        mpfr_add( subrop, subrop, mean, GMP_RNDN );

        /* If possible generate the next random value. */
        if( i + 1 <= matsize )
        {
            subrop = mps_get_ele_seq( rop, i+1 );
            mpfr_mul( subrop, s, y, GMP_RNDN );

            mpfr_mul( subrop, subrop, variance, GMP_RNDN );
            mpfr_add( subrop, subrop, mean, GMP_RNDN );
        }
    }

    mpfr_clear( x );
    mpfr_clear( y );
    mpfr_clear( s );

    return 0;
}


/* Normal distribution using the Marsaglia polar method with doubles argument for the variance and mean.  */
int mps_rand_nor1_double( const mps_ptr rop, double mean, double variance, gmp_randstate_t state )
{
	mpfr_ptr subrop;
    mpfr_t x, y, s;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    mpfr_init2( x, MPS_PREC(rop) );
    mpfr_init2( y, MPS_PREC(rop) );
    mpfr_init2( s, MPS_PREC(rop) );

	for ( i = 1; i <= matsize; i = i + 2 )
	{
        do
        {
		    subrop = mps_get_ele_seq( rop, i );
            mpfr_urandom( x, state, GMP_RNDN );
            mpfr_urandom( y, state, GMP_RNDN );

            /* Scale the [0,1] uniform distribution to [-1,1] */
            mpfr_mul_d( x, x, 2, GMP_RNDN );
            mpfr_sub_d( x, x, 1, GMP_RNDN );
            mpfr_mul_d( y, y, 2, GMP_RNDN );
            mpfr_sub_d( y, y, 1, GMP_RNDN );
            
            /* s = x**2 + y**2 */
            mpfr_sqr( s, y, GMP_RNDN );
            mpfr_sqr( subrop, x, GMP_RNDN );
            mpfr_add( s, s, subrop, GMP_RNDN );

        } while( mpfr_cmp_d( s, 1 ) >= 0 || mpfr_zero_p(s) );

        /* x*sqrt(-2*ln(s)/s) */
        mpfr_log( subrop, s, GMP_RNDN );
        mpfr_mul_d( subrop, subrop, -2, GMP_RNDN );
        mpfr_div( s, subrop, s, GMP_RNDN );
        mpfr_sqrt( s, s, GMP_RNDN);
        mpfr_mul( subrop, s, x, GMP_RNDN );

        mpfr_mul_d( subrop, subrop, variance, GMP_RNDN );
        mpfr_add_d( subrop, subrop, mean, GMP_RNDN );

        /* If possible generate the next random value. */
        if( i + 1 <= matsize )
        {
            subrop = mps_get_ele_seq( rop, i+1 );
            mpfr_mul( subrop, s, y, GMP_RNDN );

            mpfr_mul_d( subrop, subrop, variance, GMP_RNDN );
            mpfr_add_d( subrop, subrop, mean, GMP_RNDN );
        }
    }

    mpfr_clear( x );
    mpfr_clear( y );
    mpfr_clear( s );

    return 0;
}


/* Exponential distribution using a simple inverse transform sampling. */
int mps_rand_exp( const mps_ptr rop, const mps_ptr mpslambda, gmp_randstate_t state )
{
	mpfr_ptr subrop, lambda;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpslambda),
        "mpslambda not a scalar in mps_rand_exp()\n" );

    lambda = mps_get_ele( mpslambda, 1, 1 );

    MPS_ASSERT_MSG( mpfr_sgn( lambda ) > 0,
        "Lambda parameter is negative or zero in mps_rand_exp()\n" );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );
        mpfr_urandom( subrop, state, GMP_RNDN );

        mpfr_log( subrop, subrop, GMP_RNDN );
        mpfr_div( subrop, subrop, lambda, GMP_RNDN );
        mpfr_neg( subrop, subrop, GMP_RNDN );
    }

    return 0;
}


/* Exponential distribution using a simple inverse transform sampling. */
int mps_rand_exp_double( const mps_ptr rop, double lambda, gmp_randstate_t state )
{
	mpfr_ptr subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( lambda > 0,
        "Lambda parameter is negative or zero in mps_rand_exp_double()\n" );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );
        mpfr_urandom( subrop, state, GMP_RNDN );

        mpfr_log( subrop, subrop, GMP_RNDN );
        mpfr_div_d( subrop, subrop, lambda, GMP_RNDN );
        mpfr_neg( subrop, subrop, GMP_RNDN );
    }

    return 0;
}


/* Weibull distribution using inverse transform. */
int mps_rand_wei( const mps_ptr rop, const mps_ptr mpsshape, const mps_ptr mpsscale, gmp_randstate_t state )
{
	mpfr_ptr subrop, shape, scale;
    mpfr_t tmp;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsshape),
        "mpsshape not a scalar in mps_rand_wei()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsscale),
        "mpsscale not a scalar in mps_rand_wei()\n" );

    shape = mps_get_ele( mpsshape, 1, 1 );
    scale = mps_get_ele( mpsscale, 1, 1 );

    MPS_ASSERT_MSG( mpfr_sgn( shape ) > 0,
        "Shape parameter is negative or zero in mps_rand_wei()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( scale ) > 0,
        "Scale parameter is negative or zero in mps_rand_wei()\n" );

    mpfr_init2( tmp, MPS_PREC(rop) );
    mpfr_ui_div( tmp, 1, shape, GMP_RNDN );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        mpfr_urandom( subrop, state, GMP_RNDN );

        mpfr_log( subrop, subrop, GMP_RNDN );
        mpfr_neg( subrop, subrop, GMP_RNDN );
        mpfr_pow( subrop, subrop, tmp, GMP_RNDN );
        mpfr_mul( subrop, subrop, scale, GMP_RNDN );
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_rand_wei_double( const mps_ptr rop, double shape, double scale, gmp_randstate_t state )
{
    mps_t mpsshape, mpsscale;

    MPS_ASSERT_MSG( shape > 0,
        "Shape parameter is negative or zero in mps_rand_wei_double()\n" );

    MPS_ASSERT_MSG( scale > 0,
        "Scale parameter is negative or zero in mps_rand_wei_double()\n" );

    mps_init( mpsshape, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsscale, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsshape, 1, 1 ), shape, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsscale, 1, 1 ), scale, GMP_RNDN );

    mps_rand_wei( rop, mpsshape, mpsscale, state );

    mps_free( mpsshape );
    mps_free( mpsscale );

    return 0;
}

