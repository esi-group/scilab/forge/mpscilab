/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * General reference :
 * Luc Devroye (1986) "Non-Uniform Random Variate Generation" Springer-Verlag,
 * New York, ISBN 978-0387963051
 * Conveniently available from the author :
 * http://cg.scs.carleton.ca/~luc/rnbookindex.html
 */

/* 
 * Cheng's rejection algorithm (BA)
 * R.C.H. Cheng, "Generating beta variates with nonintegral shape parameters",
 * Communications of the ACM, vol. 26, pp. 71-75, 1977. DOI : 10.1145/359460.359482
 */
int mps_rand_beta_ba( const mps_ptr rop, const mps_ptr mpsa, const mps_ptr mpsb, gmp_randstate_t state )
{
	mpfr_ptr subrop, a, b;
    mpfr_t alpha, beta, gamma, U, V, tmp, log4;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsa),
        "mpsa not a scalar in mps_rand_beta_ba()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsb),
        "mpsb not a scalar in mps_rand_beta_ba()\n" );

    a = mps_get_ele( mpsa, 1, 1 );
    b = mps_get_ele( mpsb, 1, 1 );

    MPS_ASSERT_MSG( mpfr_sgn( a ) > 0,
        "First shape parameter is negative or zero in mps_rand_beta_ba()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( b ) > 0,
        "Second shape parameter is negative or zero in mps_rand_beta_ba()\n" );

    mpfr_init2( log4, MPS_PREC(rop) );
    mpfr_init2( alpha, MPS_PREC(rop) );
    mpfr_init2( beta, MPS_PREC(rop) );
    mpfr_init2( gamma, MPS_PREC(rop) );
    mpfr_init2( U, MPS_PREC(rop) );
    mpfr_init2( V, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );

    /* log(4) */
    mpfr_set_ui( log4, 4, GMP_RNDN );
    mpfr_log( log4, log4, GMP_RNDN );

    /* alpha = a + b */
    mpfr_add( alpha, a, b, GMP_RNDN );

    mpfr_min( beta, a, b, GMP_RNDN );

    /* if min(a,b) <= 1 ? beta = max(1/a,1/b) : lambda = sqrt((alpha-2)/(2ab-alpha))/*/
    if( mpfr_cmp_ui( beta, 1 ) > 0 )
    {
        mpfr_sub_ui( tmp, alpha, 2, GMP_RNDN );
    
        mpfr_mul( beta, a, b, GMP_RNDN );
        mpfr_mul_ui( beta, beta, 2, GMP_RNDN );
        mpfr_sub( beta, beta, alpha, GMP_RNDN );
    
        mpfr_div( beta, tmp, beta, GMP_RNDN );
        mpfr_sqrt( beta, beta, GMP_RNDN );
    }
    else
        mpfr_ui_div( beta, 1, beta, GMP_RNDN );

    /* gamma = a + 1/beta  */
    mpfr_ui_div( gamma, 1, beta, GMP_RNDN );
    mpfr_add( gamma, a, gamma, GMP_RNDN );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        do
        {
            mpfr_urandom( U, state, GMP_RNDZ );

            /* V = beta * log(U1/(1-U1)) */
            mpfr_ui_sub( V, 1, U, GMP_RNDN );
            mpfr_div( V, U, V, GMP_RNDN );
            mpfr_log( V, V, GMP_RNDN );
            mpfr_mul( V, V, beta, GMP_RNDN );

            /* W = a*exp(V) */
            mpfr_exp( subrop, V, GMP_RNDN );
            mpfr_mul( subrop, subrop, a, GMP_RNDN );

            /* alpha * log(alpha / (b+W)) + gamma*V - log(4) */
            mpfr_add( tmp, b, subrop, GMP_RNDN );
            mpfr_div( tmp, alpha, tmp, GMP_RNDN );
            mpfr_log( tmp, tmp, GMP_RNDN );
            mpfr_mul( tmp, alpha, tmp, GMP_RNDN );

            mpfr_mul( V, gamma, V, GMP_RNDN );
            mpfr_add( tmp, tmp, V, GMP_RNDN );

            mpfr_sub( tmp, tmp, log4, GMP_RNDN );

            /* log(U1**2 * U2) */
            mpfr_sqr( V, U, GMP_RNDN );
            mpfr_urandom( U, state, GMP_RNDZ );
            mpfr_mul( V, V, U, GMP_RNDN );
            mpfr_log( V, V, GMP_RNDN );

            if( mpfr_less_p(tmp, V) )
                continue;
            else
            {
                mpfr_add( tmp, b, subrop, GMP_RNDN );
                mpfr_div( subrop, subrop, tmp, GMP_RNDN );
                break;
            }
        } while(1);
    }

    mpfr_clear( log4 );
    mpfr_clear( alpha );
    mpfr_clear( beta );
    mpfr_clear( gamma );
    mpfr_clear( U );
    mpfr_clear( V );
    mpfr_clear( tmp );

    return 0;
}


int mps_rand_beta_ba_double( const mps_ptr rop, double a, double b, gmp_randstate_t state )
{
    mps_t mpsa, mpsb;

    MPS_ASSERT_MSG( a > 0,
        "First shape parameter is negative or zero in mps_rand_beta_ba_double()\n" );

    MPS_ASSERT_MSG( b > 0,
        "Second shape parameter is negative or zero in mps_rand_beta_ba_double()\n" );

    mps_init( mpsa, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsb, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsa, 1, 1 ), a, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsb, 1, 1 ), b, GMP_RNDN );

    mps_rand_beta_ba( rop, mpsa, mpsb, state );

    mps_free( mpsa );
    mps_free( mpsb );

    return 0;
}


/* Cheng's algorithm BB for min(a,b) > 1 */
int mps_rand_beta_bb( const mps_ptr rop, const mps_ptr mpsa, const mps_ptr mpsb, gmp_randstate_t state )
{
	mpfr_ptr subrop, a0, b0, a, b;
    mpfr_t alpha, beta, gamma, log4, log5p1;
    mpfr_t tmp, U, W, Z, R, S;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsa),
        "mpsa not a scalar in mps_rand_beta_bb()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsb),
        "mpsb not a scalar in mps_rand_beta_bb()\n" );

    a0 = mps_get_ele( mpsa, 1, 1 );
    b0 = mps_get_ele( mpsb, 1, 1 );

    MPS_ASSERT_MSG( mpfr_cmp_ui( a0, 1 ) > 0,
        "First shape parameter is less than or equal to 1 in mps_rand_beta_bb()\n" );

    MPS_ASSERT_MSG( mpfr_cmp_ui( b0, 1 ) > 0,
        "Second shape parameter is less than or equal to 1 in mps_rand_beta_bb()\n" );

    mpfr_init2( alpha, MPS_PREC(rop) );
    mpfr_init2( beta, MPS_PREC(rop) );
    mpfr_init2( gamma, MPS_PREC(rop) );

    mpfr_init2( log4, MPS_PREC(rop) );
    mpfr_init2( log5p1, MPS_PREC(rop) );

    mpfr_init2( tmp, MPS_PREC(rop) );
    mpfr_init2( U, MPS_PREC(rop) );
    mpfr_init2( W, MPS_PREC(rop) );
    mpfr_init2( Z, MPS_PREC(rop) );
    mpfr_init2( R, MPS_PREC(rop) );
    mpfr_init2( S, MPS_PREC(rop) );

    /* log(4) */
    mpfr_set_ui( log4, 4, GMP_RNDN );
    mpfr_log( log4, log4, GMP_RNDN );

    /* log(5) + 1 */
    mpfr_set_ui( log5p1, 5, GMP_RNDN );
    mpfr_log( log5p1, log5p1, GMP_RNDN );
    mpfr_add_ui(  log5p1, log5p1, 1, GMP_RNDN );

    /* a = min(a0,b0), b = max(a0,b0) */
    if( mpfr_less_p( a0, b0 ) )
    {
        a = a0;
        b = b0;
    }
    else
    {
        a = b0;
        b = a0;
    }

    /* alpha = a + b */
    mpfr_add( alpha, a, b, GMP_RNDN );

    /* beta = sqrt[(alpha-2)/(2ab-alpha)] */
    mpfr_sub_ui( tmp, alpha, 2, GMP_RNDN );

    mpfr_mul( beta, a, b, GMP_RNDN );
    mpfr_mul_ui( beta, beta, 2, GMP_RNDN );
    mpfr_sub( beta, beta, alpha, GMP_RNDN );

    mpfr_div( beta, tmp, beta, GMP_RNDN );
    mpfr_sqrt( beta, beta, GMP_RNDN );

    /* gamma = a + 1 / beta */
    mpfr_ui_div( gamma, 1, beta, GMP_RNDN );
    mpfr_add( gamma, gamma, a, GMP_RNDN );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        do
        {
            mpfr_urandom( U, state, GMP_RNDZ );
            
            /* V = beta * log(U1/(1-U1)) */
            mpfr_ui_sub( tmp, 1, U, GMP_RNDN );
            mpfr_div( subrop, U, tmp, GMP_RNDN );
            mpfr_log( subrop, subrop, GMP_RNDN );
            mpfr_mul( subrop, subrop, beta, GMP_RNDN );

            /* W = a*exp(V) */
            mpfr_exp( W, subrop, GMP_RNDN );
            mpfr_mul( W, W, a, GMP_RNDN );

            /* Z = U1**2 * U2 */
            mpfr_sqr( Z, U, GMP_RNDN );
            mpfr_urandom( U, state, GMP_RNDZ );
            mpfr_mul( Z, Z, U, GMP_RNDN );

            /* R = gamma * V - log(4) */
            mpfr_mul( R, gamma, subrop, GMP_RNDN );
            mpfr_sub( R, R, log4, GMP_RNDN );

            /* S = a + R - W */
            mpfr_add( S, a, R, GMP_RNDN );
            mpfr_sub( S, S, W, GMP_RNDN );

            /* S + log(5) + 1 */
            mpfr_add( tmp, S, log5p1, GMP_RNDN );

            /* 5Z */
            mpfr_mul_ui( U, Z, 5, GMP_RNDN );

            if( mpfr_less_p( tmp, U ) )
            {
                /* T = log(Z) */
                mpfr_log( tmp, Z, GMP_RNDN );

                if( mpfr_less_p( S, tmp ) )
                {
                    /* R + alpha * log(alpha/(b+W)) */
                    mpfr_add( U, b, W, GMP_RNDN );
                    mpfr_div( U, alpha, U, GMP_RNDN );
                    mpfr_log( U, U, GMP_RNDN );
                    mpfr_mul( U, alpha, U, GMP_RNDN );
                    mpfr_add( U, R, U, GMP_RNDN );

                    if( mpfr_less_p( U, tmp ) )
                        continue;
                }
            }

            if( a == a0 )
            {
                mpfr_add( subrop, b, W, GMP_RNDN );
                mpfr_div( subrop, W, subrop, GMP_RNDN );
            }
            else
            {
                mpfr_add( subrop, b, W, GMP_RNDN );
                mpfr_div( subrop, b, subrop, GMP_RNDN );
            }

            break;

        } while(1);
    }

    mpfr_clear( alpha );
    mpfr_clear( beta );
    mpfr_clear( gamma );

    mpfr_clear( log4 );
    mpfr_clear( log5p1 );

    mpfr_clear( tmp );
    mpfr_clear( U );
    mpfr_clear( W );
    mpfr_clear( Z );
    mpfr_clear( R );
    mpfr_clear( S );

    return 0;
}


int mps_rand_beta_bb_double( const mps_ptr rop, double a, double b, gmp_randstate_t state )
{
    mps_t mpsa, mpsb;

    MPS_ASSERT_MSG( a > 1,
        "First shape parameter is less than or equal to 1 in mps_rand_beta_bb_double()\n" );

    MPS_ASSERT_MSG( b > 1,
        "Second shape parameter is less than or equal to 1 in mps_rand_beta_bb_double()\n" );

    mps_init( mpsa, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsb, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsa, 1, 1 ), a, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsb, 1, 1 ), b, GMP_RNDN );

    mps_rand_gam( rop, mpsa, mpsb, state );

    mps_free( mpsa );
    mps_free( mpsb );

    return 0;
}


/*
 * Algorithms BN from : 
 * J. H. Ahrens and U. Dieter, "Computer methods for sampling from gamma, beta, poisson and bionomial distributions ",
 * Computing,  Volume 12, Number 3, 223-246, DOI: 10.1007/BF02293108
 */
int mps_rand_beta_bn( const mps_ptr rop, const mps_ptr mpsa, const mps_ptr mpsb, gmp_randstate_t state )
{
	mpfr_ptr subrop, a, b;
    mpfr_t A, B, C, L, V, mu, sigma;
    mpfr_t s, x, y;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsa),
        "mpsa not a scalar in mps_rand_beta_bn()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsb),
        "mpsb not a scalar in mps_rand_beta_bn()\n" );

    a = mps_get_ele( mpsa, 1, 1 );
    b = mps_get_ele( mpsb, 1, 1 );

    MPS_ASSERT_MSG( mpfr_cmp_ui( a, 1 ) > 0,
        "First shape parameter is less than or equal to 1 in mps_rand_beta_bn()\n" );

    MPS_ASSERT_MSG( mpfr_cmp_ui( b, 1 ) > 0,
        "Second shape parameter is less than or equal to 1 in mps_rand_beta_bn()\n" );

    mpfr_init2( A, MPS_PREC(rop) );
    mpfr_init2( B, MPS_PREC(rop) );
    mpfr_init2( C, MPS_PREC(rop) );
    mpfr_init2( L, MPS_PREC(rop) );
    mpfr_init2( V, MPS_PREC(rop) );
    mpfr_init2( mu, MPS_PREC(rop) );
    mpfr_init2( sigma, MPS_PREC(rop) );

    mpfr_init2( s, MPS_PREC(rop) );
    mpfr_init2( x, MPS_PREC(rop) );
    mpfr_init2( y, MPS_PREC(rop) );

    /* A = a - 1 */
    mpfr_sub_ui( A, a, 1, GMP_RNDN );

    /* B = b - 1 */
    mpfr_sub_ui( B, b, 1, GMP_RNDN );

    /* C = A + B */
    mpfr_add( C, A, B, GMP_RNDN );

    /* L = C * ln(C) */
    mpfr_log( L, C, GMP_RNDN );
    mpfr_mul( L, L, C, GMP_RNDN );

    /* mu = A / C */
    mpfr_div( mu, A, C, GMP_RNDN );

    /* sigma = 0.5 / C**0.5 */
    mpfr_sqrt( sigma, C, GMP_RNDN );
    mpfr_d_div( sigma, 0.5, sigma, GMP_RNDN );

	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        do
        {
            /* Sample from the stantard normal distribution. */
            do
            {
                mpfr_urandom( x, state, GMP_RNDN );
                mpfr_urandom( y, state, GMP_RNDN );

                /* Scale the [0,1] uniform distribution to [-1,1] */
                mpfr_mul_d( x, x, 2, GMP_RNDN );
                mpfr_sub_d( x, x, 1, GMP_RNDN );
                mpfr_mul_d( y, y, 2, GMP_RNDN );
                mpfr_sub_d( y, y, 1, GMP_RNDN );

                /* s = x**2 + y**2 */
                mpfr_sqr( s, y, GMP_RNDN );
                mpfr_sqr( subrop, x, GMP_RNDN );
                mpfr_add( s, s, subrop, GMP_RNDN );
            } while( mpfr_cmp_d( s, 1 ) >= 0 || mpfr_zero_p(s) );

            /* x*sqrt(-2*ln(s)/s) */
            mpfr_log( subrop, s, GMP_RNDN );
            mpfr_mul_d( subrop, subrop, -2, GMP_RNDN );
            mpfr_div( s, subrop, s, GMP_RNDN );
            mpfr_sqrt( s, s, GMP_RNDN);
            mpfr_mul( s, s, x, GMP_RNDN );


            /* x = sigma*s + mu */
            mpfr_mul( subrop, s, sigma, GMP_RNDN );
            mpfr_add( subrop, subrop, mu, GMP_RNDN );
            
            /* Rejection test. */
            if( mpfr_sgn(subrop) < 0 || mpfr_cmp_ui(subrop, 1) > 0 )
                continue;

            /* A*ln(x/a) + B*ln((1-x)/B) + L + 0.5s**2 */
            mpfr_div( x, subrop, a, GMP_RNDN );
            mpfr_log( x, x, GMP_RNDN );
            mpfr_mul( x, A, x, GMP_RNDN );

            mpfr_ui_sub( y, 1, subrop, GMP_RNDN );
            mpfr_div( y, y, B, GMP_RNDN );
            mpfr_log( y, y, GMP_RNDN );
            mpfr_mul( y, B, y, GMP_RNDN );
            mpfr_add( x, x, y, GMP_RNDN );

            mpfr_add( x, x, L, GMP_RNDN );

            mpfr_sqr( y, s, GMP_RNDN );
            mpfr_mul_d( y, y, 0.5, GMP_RNDN );
            mpfr_add( x, x, y, GMP_RNDN );

            /* log(u) */
            mpfr_urandom( y, state, GMP_RNDZ );
            mpfr_log( y, y, GMP_RNDN );

            /* Rejection test. */
            if( mpfr_greater_p( y, x ) )
                continue;
            else
                break;

        } while(1);

    }

    mpfr_clear( A );
    mpfr_clear( B );
    mpfr_clear( C );
    mpfr_clear( L );
    mpfr_clear( V );
    mpfr_clear( mu );
    mpfr_clear( sigma );

    mpfr_clear( s );
    mpfr_clear( x );
    mpfr_clear( y );

    return 0;
}


int mps_rand_beta_bn_double( const mps_ptr rop, double a, double b, gmp_randstate_t state )
{
    mps_t mpsa, mpsb;

    MPS_ASSERT_MSG( a > 1,
        "First shape parameter is less than or equal to 1 in in mps_rand_beta_bn_double()\n" );

    MPS_ASSERT_MSG( b > 1,
        "Second shape parameter is less than or equal to 1 in mps_rand_beta_bn_double()\n" );

    mps_init( mpsa, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsb, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsa, 1, 1 ), a, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsb, 1, 1 ), b, GMP_RNDN );

    mps_rand_beta_bn( rop, mpsa, mpsb, state );

    mps_free( mpsa );
    mps_free( mpsb );

    return 0;
}


int mps_rand_beta( const mps_ptr rop, const mps_ptr mpsa, const mps_ptr mpsb, gmp_randstate_t state )
{
	mpfr_ptr a, b;

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsa),
        "mpsa not a scalar in mps_rand_beta()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsb),
        "mpsb not a scalar in mps_rand_beta()\n" );

    a = mps_get_ele( mpsa, 1, 1 );
    b = mps_get_ele( mpsb, 1, 1 );

    MPS_ASSERT_MSG( mpfr_sgn( a ) > 0,
        "First shape parameter is negative or zero in mps_rand_beta()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( b ) > 0,
        "Second shape parameter is negative or zero in mps_rand_beta()\n" );

    mps_rand_beta_ba( rop, mpsa, mpsb, state );

    return 0;
}


int mps_rand_beta_double( const mps_ptr rop, double a, double b, gmp_randstate_t state )
{
    mps_t mpsa, mpsb;

    MPS_ASSERT_MSG( a > 0,
        "First shape parameter is negative or zero in mps_rand_beta_double()\n" );

    MPS_ASSERT_MSG( b > 0,
        "Second shape parameter is negative or zero in mps_rand_beta_double()\n" );

    mps_init( mpsa, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsb, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsa, 1, 1 ), a, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsb, 1, 1 ), b, GMP_RNDN );

    mps_rand_beta_ba( rop, mpsa, mpsb, state );

    mps_free( mpsa );
    mps_free( mpsb );

    return 0;
}

