/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mps_priv.h"
#include "mps_assert.h"

#define LOG2_10 3.321928094887362347870319429489390175864831393024580612054756395815935

#define LINE_WIDTH 80
#define COLS_MSG_LEN 38     /* max possible length of the Columns %u through %u message */
#define ROW_MSG_LEN 15

/*
 * Simple print function. For debugging use only.
 * Prints all the matrix elements sequentially.
 */
char* mps_print_list( const mps_ptr mpsptr, const unsigned int signfdigits )
{
    mpfr_ptr mpfr_temp;
    unsigned int i;
    size_t str_size = (signfdigits + 2 + 20) * MPS_NUMROW(mpsptr) * MPS_NUMCOL(mpsptr);
    char* list = malloc( str_size );
    char* mpfr_string = malloc( signfdigits + 2 + 20 );

    for ( i = 1; i <= MPS_NUMROW(mpsptr) * MPS_NUMCOL(mpsptr); i++ )
    {
        mpfr_temp = mps_get_ele_seq( mpsptr, i );
        mpfr_sprintf( mpfr_string, "%.*Rg\n", signfdigits, mpfr_temp );
        strcat( list, mpfr_string );
    }

    free( mpfr_string );

    return list;
}

/*
 * Simple print function. For debugging use only.
 * Prints a somewhat formated view of an mps matrix.
 * FIXME?: valgrind errors if matrix of size 0 */
char* mps_printbuf_overview( const mps_ptr mpsptr, const unsigned int signfdigits )
{
    mpfr_ptr mpfr_temp;
    unsigned int i, j;
    size_t str_size = (signfdigits + 2 + 20) * MPS_NUMROW(mpsptr) * MPS_NUMCOL(mpsptr);
    char* list = calloc( str_size, 1 );
    char* mpfr_string = malloc( signfdigits + 2 + 20 );
    const unsigned int numrow = MPS_NUMROW(mpsptr);
    const unsigned int numcol = MPS_NUMCOL(mpsptr);

    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol; ++j )
        {
            mpfr_temp = mps_get_ele( mpsptr, i, j );
            mpfr_sprintf( mpfr_string, "%12.*Rg", signfdigits, mpfr_temp );
            strcat( list, mpfr_string );
        }
        strcat( list, "\n" );
    }

    free( mpfr_string );

    return list;
}

/* print directly to stdout avoiding annoying needing to free
 * which made the old printing painful for debugging. */
void mps_print_overview( const mps_ptr mpsptr, const unsigned int signfdigits )
{
    mpfr_ptr mpfr_temp;
    unsigned int i, j;
    const unsigned int numrow = MPS_NUMROW(mpsptr);
    const unsigned int numcol = MPS_NUMCOL(mpsptr);

    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol; ++j )
        {
            mpfr_temp = mps_get_ele( mpsptr, i, j );
            mpfr_printf("%12.*Rg", signfdigits, mpfr_temp );
        }
        mpfr_printf("\n" );
    }

}

/* Print all the fields of the mps_ptr. For debugging.
 * Name optional. */
void mps_print_info( const mps_ptr mat, const char* name )
{
	mpfr_printf("mps_ptr %s\n", name);
	mpfr_printf("\tArray: %p\n", MPS_MPFR_ARRAY(mat));
	mpfr_printf("\tLimbs Array: %p\n", MPS_LIMBS_ARRAY(mat));
	mpfr_printf("\tNum row: %u\n", MPS_NUMROW(mat));
	mpfr_printf("\tNum col: %u\n", MPS_NUMCOL(mat));
	mpfr_printf("\tAlloc size: %Zd\n", MPS_ALLOC_SIZE(mat));
	mpfr_printf("\tLimbs alloc size: %Zd\n", MPS_LIMBS_ALLOC_SIZE(mat));
	mpfr_printf("\tPrec: %u\n", MPS_PREC(mat));
	mpfr_printf("\tType: %u\n", MPS_TYPE(mat));
}

static void printf_formatted_item(const mpfr_ptr ptr,
                                         const unsigned int width,
                                         const unsigned int signfdigits,
                                         const char format,
                                         const char always_sign)
{

    /* I don't think building up the format string is allowed */
    if ( format == 'v' )        /* use sci. not. or not */
    {
        if ( always_sign )
            mpfr_printf("%+*.*Rg", width, signfdigits, ptr );
        else
            mpfr_printf("%*.*Rg", width, signfdigits, ptr );
    }
    else
    {
        if ( always_sign )
            mpfr_printf("%+*.*eg", width, signfdigits, ptr );
        else
            mpfr_printf("%*.*eg", width, signfdigits, ptr );
    }

}

static void sprintf_formatted_item(char* substr,
                                          mpfr_ptr ptr,
                                          unsigned int width,
                                          unsigned int signfdigits,
                                          char format,
                                          char always_sign)
{
    if ( format == 'v' )        /* use sci. not. or not */
    {
        if ( always_sign )
            mpfr_sprintf(substr, "%+*.*Rg", width, signfdigits, ptr );
        else
            mpfr_sprintf(substr, "%*.*Rg", width, signfdigits, ptr );
    }
    else
    {
        if ( always_sign )
            mpfr_sprintf(substr, "%+*.*eg", width, signfdigits, ptr );
        else
            mpfr_sprintf(substr, "%*.*eg", width, signfdigits, ptr );
    }
}

/* New one using size and format variables */
void mps_print_format( const mps_ptr mpsptr,
                       const unsigned int signfdigits,
                       const char format,
                       const char always_sign )
{
    mpfr_ptr ptr;
    mp_exp_t exp;
    unsigned int i, j, k, lastone, colsperline;
    unsigned int width, startwidth, sciwidth, fixedwidth, tmpwidth;
	const unsigned int numrow = MPS_NUMROW(mpsptr);
    const unsigned int numcol = MPS_NUMCOL(mpsptr);
    const unsigned int matsize = numrow * numcol;

    /* sigdigits + decimal + possible sign + space */
    startwidth = width = signfdigits + 3;

    /* find the widest the printing needs to be so the whole thing is consistent */
    for ( i = 1; i <= matsize; ++i )
    {
        ptr = mps_get_ele_seq(mpsptr, i);

        /* get base 10 exponent approximation. Use this to calculate the width. */
        exp = (mp_exp_t) ceil((double) mpfr_get_exp(ptr) / LOG2_10);

        if ( exp < 0 )
        {
            /* basewidth + leading 0 after decimal + 1 leading 0 before */
            fixedwidth = startwidth + (-exp);

            /* base width + # digits of exponent + letter e + sign exponent */
            sciwidth = startwidth + (unsigned int) ceil(log10(-exp)) + 2;
        }
        else if ( exp > 0 )
        {
            /* base width + # digits of exponent + letter e + sign exponent */
            sciwidth = startwidth + (unsigned int) ceil(log10(exp)) + 2;

            /* FIXME: Determine when it decides to change to using sci. not.
             * how does this go? For now just use the sci width */
            fixedwidth = sciwidth;
        }
        else    /* exp == 0 log of 0 makes unhappy */
        {
            sciwidth = fixedwidth = startwidth;
        }

        /* try to guess which format will be used by g format */
        tmpwidth = (sciwidth < fixedwidth ) ? sciwidth : fixedwidth;
        if ( tmpwidth > width )
            width = tmpwidth;

    }

    ++width; /* Add the spacing between columns at the end */

    colsperline = LINE_WIDTH / width;

    if ( colsperline == 0 ) /* prevent breaking for high prec. printing */
        colsperline = 1;

    for ( i = 1; i <= numrow; ++i )
    {
        if ( numcol > colsperline )
        {
            j = 1;
            mpfr_printf("Row %u\n", i);
            while ( j <= numcol )
            {
                if ( !((lastone = j + colsperline - 1) < numcol) )
                    lastone = numcol;

                mpfr_printf("Columns %u through %u\n", j, lastone);

                for ( k = j; k <= lastone; ++k )
                {
                    ptr = mps_get_ele( mpsptr, i, k );
                    printf_formatted_item(ptr, width, signfdigits, format, always_sign);
                }

                j = k;

                mpfr_printf("\n");
            }
        }
        else        /* don't need to split up columns on multiple lines */
        {
            for ( j = 1; j <= numcol; ++j )
            {
                ptr = mps_get_ele( mpsptr, i, j );
                printf_formatted_item(ptr, width, signfdigits, format, always_sign);
            }
        }
        mpfr_printf("\n" );
    }
}

/* New one using size and format variables */
char* mps_print_format_buf( mps_ptr mpsptr,
                            const unsigned int signfdigits,
                            const char format,
                            const char always_sign )
{
    mpfr_ptr ptr;
    mp_exp_t exp;
    unsigned int i, j, k, lastone, colsperline;
    unsigned int width, startwidth, sciwidth, fixedwidth, tmpwidth;
	const unsigned int numrow = MPS_NUMROW(mpsptr);
    const unsigned int numcol = MPS_NUMCOL(mpsptr);
    const unsigned int matsize = numrow * numcol;
    size_t strsize;
    char* buf;
    char* substr;

    /* sigdigits + decimal + possible sign + space */
    startwidth = width = signfdigits + 3;

    /* find the widest the printing needs to be so the whole thing is consistent */
    for ( i = 1; i <= matsize; ++i )
    {
        ptr = mps_get_ele_seq(mpsptr, i);

        /* get base 10 exponent approximation. Use this to calculate the width. */
        exp = (mp_exp_t) ceil((double) mpfr_get_exp(ptr) / LOG2_10);

        if ( exp < 0 )
        {
            /* basewidth + leading 0 after decimal + 1 leading 0 before */
            fixedwidth = startwidth + (-exp);

            /* base width + # digits of exponent + letter e + sign exponent */
            sciwidth = startwidth + (unsigned int) ceil(log10(-exp)) + 2;
        }
        else if ( exp > 0 )
        {
            /* base width + # digits of exponent + letter e + sign exponent */
            sciwidth = startwidth + (unsigned int) ceil(log10(exp)) + 2;

            /* FIXME: Determine when it decides to change to using sci. not.
             * how does this go? For now just use the sci width */
            fixedwidth = sciwidth;
        }
        else    /* exp == 0 log of 0 makes unhappy */
        {
            sciwidth = fixedwidth = startwidth;
        }

        /* try to guess which format will be used by g format */
        tmpwidth = (sciwidth < fixedwidth ) ? sciwidth : fixedwidth;
        if ( tmpwidth > width )
            width = tmpwidth;

    }

    ++width; /* Add the spacing between columns at the end */

    colsperline = LINE_WIDTH / width;

    if ( colsperline == 0 ) /* prevent breaking for high prec. printing */
        colsperline = 1;

    /* size of elements + number of newlines + space for column message.
     numcols / colsperline should be number of times printed, ceil/be sure with + 1, */
    strsize = width * matsize + numrow;

    if ( numcol > colsperline )
        strsize += numrow * (numcol * COLS_MSG_LEN + ROW_MSG_LEN);

    buf = malloc( strsize + 1 );
    MPS_ASSERT_MSG( buf, "malloc() failed!");

    /* now find the longest that substr will need to be. Should be the larger of these */
    tmpwidth = width + 2;
    tmpwidth = (tmpwidth > COLS_MSG_LEN ? tmpwidth : COLS_MSG_LEN);
    tmpwidth = (tmpwidth > ROW_MSG_LEN ? tmpwidth : ROW_MSG_LEN);

    substr = malloc( tmpwidth );  /* nothing should be larger than this */
    buf[0] = '\0';
    substr[0] = '\0';

    for ( i = 1; i <= numrow; ++i )
    {
        if ( numcol > colsperline )
        {
            j = 1;
            mpfr_sprintf(substr, "Row %u\n", i);
            buf = strcat(buf, substr);

            while ( j <= numcol )
            {
                if ( !((lastone = j + colsperline - 1) < numcol) )
                    lastone = numcol;

                /* maxlength = 38 = 18 + 2 uint32, max digits = 2 * 10 */
                mpfr_sprintf(substr, "Columns %u through %u\n", j, lastone);
                buf = strcat(buf, substr);

                for ( k = j; k <= lastone; ++k )
                {
                    ptr = mps_get_ele( mpsptr, i, k );
                    sprintf_formatted_item(substr, ptr, width, signfdigits, format, always_sign);
                    buf = strcat(buf, substr);
                }

                j = k;
                buf = strcat(buf, "\n");
            }
        }
        else        /* don't need to split up columns on multiple lines */
        {
            for ( j = 1; j <= numcol; ++j )
            {
                ptr = mps_get_ele( mpsptr, i, j );
                sprintf_formatted_item(substr, ptr, width, signfdigits, format, always_sign);
                buf = strcat(buf, substr);
            }
        }
        buf = strcat(buf, "\n");
    }

    free( substr );
    return buf;
}

