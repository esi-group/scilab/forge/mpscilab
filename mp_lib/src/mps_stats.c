/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"


/* agm matrices of equal size by element */
int mps_agm( mps_ptr rop, mps_ptr op1, mps_ptr op2, mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i, matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_agm()\n" );
    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_agm()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_agm( subrop, subop1, subop2, rnd );
    }

    return 0;
}


/* Mean average of 2 matrices of equal size by element */
int mps_mean_mats( mps_ptr rop, mps_ptr op1, mps_ptr op2, mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i, matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_avg()\n" );
    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_avg()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );

		mpfr_add(subrop, subop1, subop2, rnd);	/* a + b */
		mpfr_div_ui(subrop, subrop, 2, rnd);	/* (a + b)/2 */
    }

    return 0;
}


/* Gives the total of all the elements in op */
int mps_total( mpfr_ptr rop, mps_ptr op, mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i, matsize = MPS_SIZE(op);

	mpfr_set_ui(rop, 0, rnd);
    for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_col(op, i);
		mpfr_add(rop, rop, subop, rnd);
	}

	return 0;
}


/* Gives the mean matrix of op */
int mps_mean_mat( mps_ptr rop, mps_ptr op, mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i, j, sizerow, sizecol;

	sizecol = MPS_NUMCOL(op);
	sizerow = MPS_NUMROW(op);

	MPS_ASSERT_MSG( MPS_NUMCOL(rop) == MPS_NUMCOL(op),
                    "Result operand wrong size in mps_mean_mat()\n");

    for( i = 1; i <= sizecol; ++i )
	{
		subrop = mps_get_ele_col(rop, i);
		mpfr_set_ui(subrop, 0, rnd);
		for ( j = 1; j <= sizerow; ++j)
		{
			subop = mps_get_ele(op, j, i);
			mpfr_add(subrop, subrop, subop, rnd);
		}

		mpfr_div_ui(subrop, subrop, sizerow, rnd);
	}

	return 0;
}


/* Gives the standard deviation of all the elements in op */
int mps_stddev( mpfr_ptr rop, mps_ptr op, mpfr_rnd_t rnd )
{
//	mps_variance(rop, op, rnd);
	mpfr_sqrt(rop, rop, rnd);

	return 0;
}


/* gives the single element covariance for a single column matrix.
 * I don't think this function is even necessary. I haven't even bothered testing it. */
int mps_covariance_cols(mpfr_ptr rop, mps_ptr op1, mps_ptr op2, mpfr_rnd_t rnd)
{
	mpfr_ptr subop;
	mpfr_t mean1, mean2, tmp1, tmp2;			/* need lots of temp vars here */
	unsigned int i, size = MPS_NUMCOL(op1);	/* number of columns */

	MPS_ASSERT_MSG(MPS_IS_COL(op1) && MPS_IS_COL(op2),
                    "Expected column in mps_covariance_col()\n");
	MPS_ASSERT_MSG(MPS_SAME_SIZE(op1, op2),
                    "Expected columns of equal size in mps_covariance_col()\n");

	mpfr_inits2(MPS_PREC(op1), mean1, mean2, tmp1, tmp2, (mpfr_ptr) NULL);

	mpfr_set_ui(rop, 0, rnd);
	mpfr_set_ui(mean1, 0, rnd);
	mpfr_set_ui(mean2, 0, rnd);

	for (i = 1; i <= size; ++i)		/* TODO: maybe want to use existing mean function for this? */
	{
		subop = mps_get_ele_col(op1, i);
		mpfr_add(mean1, mean1, subop, rnd);
		subop = mps_get_ele_col(op2, i);
		mpfr_add(mean2, mean2, subop, rnd);
	}

	mpfr_div_ui(mean1, mean1, size, rnd);
	mpfr_div_ui(mean2, mean2, size, rnd);

	for ( i = 1; i <= size; ++i)
	{
		subop = mps_get_ele_col(op1, i);
		mpfr_sub(tmp1, subop, mean1, rnd);

		subop = mps_get_ele_col(op2, i);
		mpfr_sub(tmp2, subop, mean2, rnd);

		mpfr_mul(tmp1, tmp1, tmp2, rnd);
		mpfr_add(rop, rop, tmp1, rnd);
	}

	mpfr_div_ui(rop, rop, --size, rnd);	/* 1/(N - 1)*/

	mpfr_clears(mean1, mean2, tmp1, tmp2, (mpfr_ptr) NULL);

	return 0;
}


/* gives the covariance matrix of op in rop for a real matrix.
 * I think this could be done a better way */
int mps_covariance_mat(mps_ptr rop, mps_ptr op, mpfr_rnd_t rnd)
{
	mpfr_t mean, tmp, total;
	mpfr_ptr subop, subrop;
	unsigned int i, j, k;
    const unsigned int numcols = MPS_NUMCOL(op);
    const unsigned int numrows = MPS_NUMROW(op);

	mps_t tmpmat;	/* for each column, find X - Xbar */
					/* I think there's a way to do this without a temp matrix */

	MPS_ASSERT_MSG(MPS_IS_SQUARE(rop) && MPS_NUMCOL(rop) == MPS_NUMCOL(op),	/* want num columns */
                    "Result operand wrong size in mps_covariance_mat()\n");

	/* for op w/n cols, covariance is a nxn matrix of covariances between columns of op */

	mps_init(tmpmat, numrows, numcols, MPS_PREC(op), MPS_COL_ORDER);

	mpfr_inits2(MPS_PREC(op), mean, tmp, total, (mps_ptr) NULL);

	for ( i = 1; i <= numcols; ++i )		/* for each column take care of the mean business */
	{
		mpfr_set_ui(mean, 0, rnd);			/* reset mean for each column */
		for ( j = 1; j <= numrows; ++j )	/* go down the rows adding the elements */
		{
			subop = mps_get_ele(op, j, i);
			mpfr_set(mps_get_ele(tmpmat, j, i), subop, rnd);	/* copy into tmp matrix */
			mpfr_add(mean, mean, subop, rnd);
		}

		mpfr_div_ui(mean, mean, numrows, rnd);	/* now have the mean of the column */

		for ( j = 1; j <= numrows; ++j )		/* go down column of tmp matrix subtracting mean */
		{
			subop = mps_get_ele(tmpmat, j, i);
			mpfr_sub(subop, subop, mean, rnd);
		}
	}

	/* need to find the covariance between each column */
	/* the covariance between 2 columns is sum of product down tmp matrix */

	for ( i = 1; i <= numcols; ++i ) 		/* go across the rows, for each variable */
	{
		for ( k = i; k <= numcols; ++k )	/* since symmetric can skip some work */
		{
			mpfr_set_ui(total, 0, rnd);
			for ( j = 1; j <= numrows; ++j )
			{
				mpfr_mul(tmp,
						mps_get_ele(tmpmat, j, k),
						mps_get_ele(tmpmat, j, i),
						rnd);

				mpfr_add(total, total, tmp, rnd);
			}

			mpfr_div_ui(total, total, numrows - 1, rnd);	/* 1/(N-1) */

			subrop = mps_get_ele(rop, i, k);	/* symmetric, put result in both of these places */
			mpfr_set(subrop, total, rnd);

			if (i == k)		/* no reason to redo this */
				continue;

			subrop = mps_get_ele(rop, k, i);	/* rop[m][n] == rop[n][m], symmetric */
			mpfr_set(subrop, total, rnd);
		}
	}

	mpfr_clears(tmp, mean, total, (mps_ptr) NULL);
	mps_free(tmpmat);

	return 0;
}


/* at each element in matrix, the correlation coefficient is the covariance divided by the product
 * of the standard deviation of both columns. */
/* calculates the correlation matrix of op */
int mps_correlation_mat(const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
	mpfr_t mean, tmp, total;
	mpfr_t* stddevs;
	mpfr_ptr subop, subrop, subarr1, subarr2;
	unsigned int i, j, k;
    const mpfr_prec_t prec = MPS_PREC(op);
    const unsigned int numcols = MPS_NUMCOL(op);
    const unsigned int numrows = MPS_NUMROW(op);

	mps_t tmpmat;	/* for each column, find X - Xbar */
					/* I think there's a way to do this without a temp matrix */

	MPS_ASSERT_MSG(MPS_IS_SQUARE(rop) && MPS_NUMCOL(rop) == MPS_NUMCOL(op),	/* want num columns */
                    "Result operand wrong size in mps_correlation_mat()\n");

	/* for op w/n cols, covariance is a nxn matrix of covariances between columns of op */

	mps_init(tmpmat, numrows, numcols, prec, MPS_COL_ORDER);

	/* calculate the stddevs for each column for use later.
	 * I'm pretty sure I could rewrite this for this to not be necessary*/
	stddevs = malloc(sizeof(mpfr_t) * numcols);

	mpfr_inits2(prec, mean, tmp, total, (mps_ptr) NULL);

	for ( i = 1; i <= numcols; ++i )		/* for each column take care of the mean business */
	{
		mpfr_set_ui(mean, 0, rnd);			/* reset mean for each column */
		subarr1 = stddevs[i-1];
		mpfr_init2(subarr1, prec);
		mpfr_set_ui(subarr1, 0, rnd);
		for ( j = 1; j <= numrows; ++j )	/* go down the rows adding the elements */
		{
			subop = mps_get_ele(op, j, i);
			subrop = mps_get_ele(tmpmat, j, i);
			mpfr_set(subrop, subop, rnd);	/* copy into tmp matrix */
			mpfr_add(mean, mean, subop, rnd);
		}

		mpfr_div_ui(mean, mean, numrows, rnd);	/* now have the mean of the column */

		for ( j = 1; j <= numrows; ++j )		/* go down column of tmp matrix subtracting mean */
		{
			subop = mps_get_ele(tmpmat, j, i);
			mpfr_sub(subop, subop, mean, rnd);

			/* now get the std dev of this column and store it for later */
			mpfr_sqr(tmp, subop, rnd);
			mpfr_add(subarr1, subarr1, tmp, rnd);
		}
		mpfr_div_ui(subarr1, subarr1, numrows, rnd);
		mpfr_sqrt(subarr1, subarr1, rnd);		/* stddev for column done */
	}

	/* need to find the correlation coefficients between each column */
	/* covariance of the columns / product of std devs of each column */

	for ( i = 1; i <= numcols; ++i ) 		/* go across the rows, for each variable */
	{
		for ( k = i; k <= numcols; ++k )	/* since symmetric can skip some work */
		{
			mpfr_set_ui(total, 0, rnd);
			for ( j = 1; j <= numrows; ++j )
			{
				mpfr_mul(tmp,
						mps_get_ele(tmpmat, j, k),
						mps_get_ele(tmpmat, j, i),
						rnd);

				mpfr_add(total, total, tmp, rnd);
			}

			mpfr_div_ui(total, total, numrows, rnd);	/* 1/N */

			/* total is covariance here. */
			subarr1 = stddevs[i-1];
			subarr2 = stddevs[k-1];
			mpfr_mul(tmp, subarr1, subarr2, rnd);	/* product std devs */

			mpfr_div(total, total, tmp, rnd);		/* cov(i,k) / ( stddev(i) * stddev(k) )*/

			subrop = mps_get_ele(rop, i, k);		/* symmetric, put result in both of these places */
			mpfr_set(subrop, total, rnd);

			if (i == k)		/* no reason to redo this */
				continue;

			subrop = mps_get_ele(rop, k, i);	/* rop[m][n] == rop[n][m], symmetric */
			mpfr_set(subrop, total, rnd);
		}
	}

	for ( i = 0; i < numcols; ++i)	/* free stddevs */
		mpfr_clear(stddevs[i]);
	free(stddevs);

	mpfr_clears(tmp, mean, total, (mps_ptr) NULL);
	mps_free(tmpmat);

	return 0;
}


/* for a list, calculates the central moment of order order */
void mps_central_moment( const mpfr_ptr cm,
                         const mps_ptr A,
                         const unsigned int order,
                         const mpfr_rnd_t rnd)
{
    unsigned int i;
    mpfr_t mean, utmp1;
    mpfr_ptr subA;
    const unsigned int matsize = MPS_SIZE(A);

    mpfr_inits2(MPS_PREC(A), mean, utmp1, (mpfr_ptr) NULL );

    mpfr_set_ui(mean, 0, rnd);

    for ( i = 1; i <= matsize; ++i )
    {
        subA = mps_get_ele_seq(A, i);
        mpfr_add(mean, mean, subA, rnd);
    }

    mpfr_div_ui(mean, mean, matsize, rnd);
    mpfr_set_ui(cm, 0, rnd);

    for ( i = 1; i <= matsize; ++i )
    {
        subA = mps_get_ele_seq(A, i);
        mpfr_sub(utmp1, subA, mean, rnd);
        mpfr_pow_ui(utmp1, utmp1, order, rnd);
        mpfr_add(cm, cm, utmp1, rnd);
    }

    /* scale by 1/N */
    mpfr_div_ui(cm, cm, matsize, rnd);

    mpfr_clears( mean, utmp1, (mpfr_ptr) NULL );
}


/* tries to do what scilab cmoment function does with the mode thing, and rows and cols */
/* FIXME: Naming */
void mps_cmoments( const mps_ptr cm,
                   const mps_ptr A,
                   const unsigned int order,
                   const char mode,
                   const mpfr_rnd_t rnd)
{
    unsigned int i, j;
    mpfr_t mean, utmp1;
    mpfr_ptr subA, subcm;
    const unsigned int numcol = MPS_NUMCOL(A);
    const unsigned int numrow = MPS_NUMROW(A);

    MPS_ASSERT_MSG( (mode == 'r' && MPS_IS_ROW(cm) && MPS_NUMCOL(cm) == numcol)
                    || (mode == 'c' && MPS_IS_COL(cm) && MPS_NUMROW(cm) == numrow),
                    "Received unexpected mode or destination size in mps_cmoments()\n");

    mpfr_inits2(MPS_PREC(A), mean, utmp1, (mpfr_ptr) NULL );

    if ( mode == 'c' )
    {
        for ( i = 1; i <= numrow; ++i )
        {
            subcm = mps_get_ele_col(cm, i);
            mpfr_set_ui(subcm, 0, rnd);
            mpfr_set_ui(mean, 0, rnd);
            for ( j = 1; j <= numcol; ++j )     /* calculate mean of the column */
            {
                subA = mps_get_ele(A, i, j);
                mpfr_add(mean, mean, subA, rnd);
            }

            mpfr_div_ui(mean, mean, numcol, rnd);

            for ( j = 1; j <= numcol; ++j )
            {
                subA = mps_get_ele(A, i, j);
                mpfr_sub(utmp1, subA, mean, rnd);
                mpfr_pow_ui(utmp1, utmp1, order, rnd);
                mpfr_add(subcm, subcm, utmp1, rnd);
            }

            mpfr_div_ui(subcm, subcm, numcol, rnd);
        }
    }
    else if ( mode == 'r' )
    {
        for ( i = 1; i <= numcol; ++i )
        {
            subcm = mps_get_ele_row(cm, i);
            mpfr_set_ui(subcm, 0, rnd);
            mpfr_set_ui(mean, 0, rnd);
            for ( j = 1; j <= numrow; ++j )     /* calculate mean of the column */
            {
                subA = mps_get_ele(A, j, i);
                mpfr_add(mean, mean, subA, rnd);
            }

            mpfr_div_ui(mean, mean, numcol, rnd);

            for ( j = 1; j <= numrow; ++j )
            {
                subA = mps_get_ele(A, j, i);
                mpfr_sub(utmp1, subA, mean, rnd);
                mpfr_pow_ui(utmp1, utmp1, order, rnd);
                mpfr_add(subcm, subcm, utmp1, rnd);
            }

            mpfr_div_ui(subcm, subcm, numrow, rnd);
        }
    }

    mpfr_clears( mean, utmp1, (mpfr_ptr) NULL );
}

