/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * LU factorization using the Doolitle method.
 * Reference : R. L. Burden, J.D Faires, Numerical analysis 8th edition,
 * Thomson Brooks/Cole, 2005, pp392-395 ISBN : 0-534-39200-8
 */
int mps_lu_decomp( const mps_ptr ropL, const mps_ptr op )
{

    mpfr_ptr subop1, subop2;
    mpfr_t mpfrtemp;
    unsigned int i, j;
	const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_lu_decomp()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(ropL,op),
        "Second result operand differ in dimension in mps_lu_decomp()\n" );

    MPS_ASSERT_MSG( MPS_NOT_ALIAS(ropL,op),
        "Second result operand alias the input operand in mps_lu_decomp()\n" );

    mpfr_init2( mpfrtemp, MPS_PREC(op) );

    mps_identity( ropL, GMP_RNDN );

    for ( i = 1; i < numcol; ++i )
    {
        subop1 = mps_get_ele( op, i, i );

        for ( j = i + 1; j <= numcol; ++j )
        {
            subop2 = mps_get_ele( op, j, i );
            mpfr_div( mpfrtemp, subop2, subop1, GMP_RNDN );

            mps_row_sub_mpfr( op, j, i, mpfrtemp, GMP_RNDN );

            subop2 = mps_get_ele( ropL, j, i );

            mpfr_set( subop2, mpfrtemp, GMP_RNDN );
        }
    }

    mpfr_clear( mpfrtemp );

    return 0;
}

/*
 * LU factorization using the Crout method.
 * Reference : R. L. Burden, J.D Faires, Numerical analysis 8th edition,
 * Thomson Brooks/Cole, 2005, pp392-395 ISBN : 0-534-39200-8
 */
int mps_lu_decomp2( const mps_ptr ropU, const mps_ptr op )
{

    mpfr_ptr subop1, subop2;
    mpfr_t mpfrtemp;
    unsigned int i, j;
	const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_lu_decomp2()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(ropU,op),
        "Second result operand differ in dimension in mps_lu_decomp2()\n" );

    MPS_ASSERT_MSG( MPS_NOT_ALIAS(ropU,op),
        "Second result operand alias the input operand in mps_lu_decomp2()\n" );

    mpfr_init2( mpfrtemp, MPS_PREC(op) );

    mps_identity( ropU, GMP_RNDN );

    for ( i = numcol; i > 1; --i )
    {
        subop1 = mps_get_ele( op, i, i );

        for ( j = i-1; j >= 1; --j )
        {
            subop2 = mps_get_ele( op, j, i );
            mpfr_div( mpfrtemp, subop2, subop1, GMP_RNDN );

            mps_row_sub_mpfr( op, j, i, mpfrtemp, GMP_RNDN );

            subop2 = mps_get_ele( ropU, j, i );

            mpfr_set( subop2, mpfrtemp, GMP_RNDN );
        }
    }

    mpfr_clear( mpfrtemp );

    return 0;
}


/* In-place decomposition lapack-style. */
int mps_iplu_decomp( const mps_ptr op )
{

    mpfr_ptr subop, subop1, subop2, pivot;
    unsigned int i, j, k, l;
    mpfr_t mpfrtemp;
	const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_iplu_decomp()\n" );

    mpfr_init2( mpfrtemp, MPS_PREC(op) );

    for( k = 1; k < numcol; k++ )
    {
        pivot = mps_get_ele( op, k, k );
        for( l = k+1; l <= numcol; l++ )
        {
            subop = mps_get_ele( op, l, k );
            mpfr_div( subop, subop, pivot, GMP_RNDN );
        }

        for( i = k+1; i <= numcol; i++ )
        {
            for( j = k+1; j <= numcol; j++ )
            {
                subop = mps_get_ele( op, i, j );
                subop1 = mps_get_ele( op, i, k );
                subop2 = mps_get_ele( op, k, j );

                mpfr_mul( mpfrtemp, subop1, subop2, GMP_RNDN );
                mpfr_sub( subop, subop, mpfrtemp, GMP_RNDN );
            }
        }

    }


    mpfr_clear( mpfrtemp );

    return 0;
}


/* In-place decomposition with partial pivoting. */
int mps_iplup_decomp( const mps_ptr op )
{

    mpfr_ptr subop, subop1, subop2, pivot;
    unsigned int i, j, k, l, kbar;
    mpfr_t mpfrtemp;
	const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_iplup_decomp()\n" );

    mpfr_init2( mpfrtemp, MPS_PREC(op) );

    for( k = 1; k < numcol; k++ )
    {
        kbar = k;
        pivot = mps_get_ele( op, k, k );
/**/
        for( i = k+1; i <= numcol; i++ )
        {
            subop = mps_get_ele( op, i, k );
            if( mpfr_cmpabs( subop, pivot ) > 0 )
            {
                pivot = subop;
                kbar = i;
            }
        }

        if( kbar != k )
        {
            mps_row_exg( op, k, kbar );
            pivot = mps_get_ele( op, k, k );
        }
/**/
        for( l = k+1; l <= numcol; l++ )
        {
            subop = mps_get_ele( op, l, k );
            mpfr_div( subop, subop, pivot, GMP_RNDN );
        }

        for( i = k+1; i <= numcol; i++ )
        {
            for( j = k+1; j <= numcol; j++ )
            {
                subop = mps_get_ele( op, i, j );
                subop1 = mps_get_ele( op, i, k );
                subop2 = mps_get_ele( op, k, j );

                mpfr_mul( mpfrtemp, subop1, subop2, GMP_RNDN );
                mpfr_sub( subop, subop, mpfrtemp, GMP_RNDN );
            }
        }

    }


    mpfr_clear( mpfrtemp );

    return 0;
}
