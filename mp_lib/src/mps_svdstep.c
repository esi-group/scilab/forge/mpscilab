/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"
#include "mps_priv.h"

/* following algorithm from GNU Scientific Library */

/* TODO: These functions aren't tested and almost definitely don't work. */

/* TODO: Epsilon constants */

/* takes 2 vectors */
void chop_small_elements( const mps_vec_ptr D, const mps_vec_ptr F, const mpfr_rnd_t rnd)
{
	unsigned int i;
	mpfr_ptr d_i, f_i, d_ip1;
	mpfr_t tmp1, tmp2, tmp3;					/* TODO: Temporary variables */
	mpfr_t epsilon;
	const unsigned int N = MPS_VEC_SIZE(D);

	MPS_ASSERT_MSG( MPS_VEC_VALID(D),
					"Expected vector in chop_small_elements()\n");
	MPS_ASSERT_MSG( MPS_VEC_VALID(F),
					"Expected vector in chop_small_elements()\n");

	mps_epsilon(epsilon, MPS_VEC_PREC(D), rnd);

	d_i = mps_get_ele_vec(D, 1);		/* 1st element */
	for ( i = 1; i < N; ++i )
	{
		f_i = mps_get_ele_vec(F, i);
		d_ip1 = mps_get_ele_vec(D, i + 1);

		mpfr_abs(tmp1, f_i, rnd);

		mpfr_abs(tmp2, d_i, rnd);	/* epsilon * (fabs(d_i) + fabs(d_ip1)) */
		mpfr_abs(tmp3, d_ip1, rnd);
		mpfr_add(tmp2, tmp2, tmp3, rnd);
		mpfr_mul(tmp2, tmp2, epsilon, rnd);

		if ( mpfr_less_p(tmp1, tmp2) )
		{
			mpfr_set_ui(f_i, 0, rnd);
		}

		mpfr_set(d_i, d_ip1, rnd);
	}
}

static void trailing_eigenvalue(const mpfr_ptr mu,
                                const mps_vec_ptr D,
                                const mps_vec_ptr F,
                                const mpfr_rnd_t rnd)
{
	mpfr_ptr da, db, fafb;
	mpfr_t ta, tb, tab, dt;		/* TODO: Temporary variables */
	mpfr_t tmp1;
	const unsigned int N = MPS_VEC_SIZE(D);

	/* TODO: Temporary constant 0 */
	/* TODO/FIXME/HOWDOIDOTHIS: Need more to clean up after this? */
	mpfr_t mpfr_zero;
	void* significand = malloc(mpfr_custom_get_size(MPFR_PREC_MIN));
	mpfr_custom_init(significand, MPFR_PREC_MIN);
	mpfr_custom_init_set(mpfr_zero, MPFR_ZERO_KIND, (mp_exp_t) 0, MPFR_PREC_MIN, significand);

	da = mps_get_ele_vec(D, N - 2);
	db = mps_get_ele_vec(D, N - 1);

	if ( N > 2 )
		fafb = mps_get_ele_vec(F, N - 3);
	else
		fafb = mpfr_zero;

	/* here mu is used as a tmp variable */
	mpfr_sqr(tmp1, da, rnd);		/* ta = da^2 + fa^2 */
	mpfr_sqr(mu, fafb, rnd);
	mpfr_add(ta, tmp1, mu, rnd);

	fafb = mps_get_ele_vec(F, N - 2);

	mpfr_sqr(tmp1, db, rnd);		/* tb = db^2 + fb^2 */
	mpfr_sqr(mu, fafb, rnd);
	mpfr_add(tb, tmp1, mu, rnd);

	mpfr_sub(dt, ta, tab, rnd);		/* dt = (ta - tb) / 2 */
	mpfr_div_ui(dt, dt, 2, rnd);

	if ( mpfr_sgn(dt) >= 0 )
	{
		mpfr_sqr(tmp1, tab, rnd);		/* mu = tb - tab^2 / ( dt + hypot(dt, tab) ) */
		mpfr_hypot(mu, dt, tab, rnd);	/* mu used just a tmp var */
		mpfr_add(mu, mu, dt, rnd);
		mpfr_div(tmp1, tmp1, mu, rnd);
		mpfr_sub(mu, tb, tmp1, rnd);	/* set mu to desired value */
	}
	else
	{
		mpfr_sqr(tmp1, tab, rnd);		/* mu = tb + tab^2 / ( hypot(dt, tab) - dt ) */
		mpfr_hypot(mu, dt, tab, rnd);	/* mu used as tmp */
		mpfr_sub(mu, mu, dt, rnd);
		mpfr_div(tmp1, tmp1, mu, rnd);
		mpfr_add(mu, tmp1, tb, rnd);	/* actual calculation of mu done */
	}

	free(significand);
}

static void create_schur(const mpfr_ptr d0,
                         const mpfr_ptr f0,
                         const mpfr_ptr d1,
                         const mpfr_ptr c,
                         const mpfr_ptr s,
                         const mpfr_rnd_t rnd)
{
	mpfr_t apq;	/* TODO: Temporary variables */
	mpfr_t one, sqrtmin, sqrtmax;
	mpfr_t tmp1, tmp2, tmp3;
	mp_exp_t d0_exp, f0_exp;


	if ( mpfr_zero_p(d0) || mpfr_zero_p(f0) )
	{
		mpfr_set_ui(c, 1, rnd);
		mpfr_set_ui(s, 0, rnd);
		return;
	}

	mpfr_inits2(mpfr_get_prec(d0), apq, one, sqrtmin, sqrtmax, (mpfr_ptr) NULL);
	mpfr_inits2(mpfr_get_prec(d0), tmp1, tmp2, tmp3, (mpfr_ptr) NULL);
	mpfr_set_ui(one, 1, rnd);

	mpfr_mul(apq, d0, f0, rnd);
	mpfr_mul_ui(apq, apq, 2, rnd);

	/* get the absolute values needed */
	mpfr_abs(tmp1, d0, rnd);
	mpfr_abs(tmp2, f0, rnd);
	mpfr_abs(tmp3, d1, rnd);

	/* TODO: Need 2 constants, equivs of SQRT_DBL_MAX, SQRT_DBL_MIN
	 * Also, do we actually need to do this? */

	mpfr_minval(sqrtmin);
	mpfr_sqrt(sqrtmin, sqrtmin, rnd);
	mpfr_maxval(sqrtmax);
	mpfr_sqrt(sqrtmax, sqrtmax, rnd);

	if ( mpfr_less_p(tmp1, sqrtmin) || mpfr_greater_p(tmp1, sqrtmax)
			|| mpfr_less_p(tmp2, sqrtmin) || mpfr_greater_p(tmp2, sqrtmax)
			|| mpfr_less_p(tmp3, sqrtmin) || mpfr_greater_p(tmp3, sqrtmax) )
	{
		/* use tmp3 as scale */
		d0_exp = mpfr_get_exp(d0);
		f0_exp = mpfr_get_exp(f0);

		mpfr_set_ui(tmp3, 1, rnd);		/* FIXME: Is this right? */
		mpfr_set_exp(tmp3, -(d0_exp + f0_exp)/4);
		mpfr_mul(d0, d0, tmp3, rnd);	/* FIXME: destroying passed in values bad here? */
		mpfr_mul(f0, f0, tmp3, rnd);
		mpfr_mul(d1, d1, tmp3, rnd);
		mpfr_mul(apq, d0, f0, rnd);
		mpfr_mul_ui(apq, apq, 2, rnd);
	}

	if ( mpfr_sgn(apq) )				/* if apq != 0 */
	{									/* calculate tau */
		mpfr_add(tmp2, d1, d0, rnd);	/* tau = (f0^2 + (d1 + d0)*(d1 - d0) ) / apq */
		mpfr_sub(tmp1, d1, d0, rnd);
		mpfr_mul(tmp1, tmp1, tmp2, rnd);
		mpfr_sqr(tmp2, f0, rnd);
		mpfr_add(tmp1, tmp1, tmp2, rnd);
		mpfr_div(tmp1, tmp1, apq, rnd);		/* tau = tmp1 */

		mpfr_hypot(tmp2, tmp1, one, rnd);
		if ( mpfr_sgn(tmp1) >= 0 )
		{
			mpfr_add(tmp1, tmp1, tmp2, rnd);	/* t = 1 / (tau + hypot(1, tau)) */
			mpfr_ui_div(tmp1, 1, tmp1, rnd);	/* tmp1 = t */
		}
		else
		{
			mpfr_sub(tmp1, tmp2, tmp1, rnd);	/* t = -1 / (hypot(1, tau) - tau) */
			mpfr_si_div(tmp1, -1, tmp1, rnd);
		}

		mpfr_hypot(tmp2, tmp1, one, rnd);
		mpfr_ui_div(c, 1, tmp2, rnd);		/* c = 1 / hypot(1, tau) */
		mpfr_mul(s, tmp1, c, rnd);			/* s = t * c */
	}
	else
	{
		mpfr_set_ui(c, 1, rnd);
		mpfr_set_ui(s, 0, rnd);
	}

	mpfr_clears(one, apq, sqrtmin, sqrtmax, (mpfr_ptr) NULL);
	mpfr_clears(tmp1, tmp2, tmp3, (mpfr_ptr) NULL);
}

/* generates a Givens rotation (cos, sin) which takes v = (x, y) to (|v|,0)
 * TODO: if useful, move to another file and expose */
/* NOTE: a and b are not overwritten, c and s are. */
static void create_givens(const mpfr_ptr a,
                                 const mpfr_ptr b,
                                 const mpfr_ptr c,
                                 const mpfr_ptr s,
                                 const mpfr_rnd_t rnd)
{
	mpfr_t tmp1, tmp2;	/* TODO: Temporary variables */

	mpfr_inits2(mpfr_get_prec(a), tmp1, tmp2, (mpfr_ptr) NULL);

	mpfr_abs(tmp1, a, rnd);
	mpfr_abs(tmp2, b, rnd);

	if ( mpfr_zero_p(b) )
	{
		mpfr_set_ui(c, 1, rnd);
		mpfr_set_ui(s, 0, rnd);
	}
	else if ( mpfr_greater_p(tmp2, tmp1) )
	{
		mpfr_div(tmp1, a, b, rnd);		/* t = -a / b */
		mpfr_neg(tmp1, tmp1, rnd);

		mpfr_sqr(tmp2, tmp2, rnd);		/* s = 1 / sqrt(1 + t^2) */
		mpfr_add_ui(tmp2, tmp2, 1, rnd);
		mpfr_sqrt(tmp2, tmp2, rnd);
		mpfr_ui_div(s, 1, tmp2, rnd);
		mpfr_mul(c, s, tmp1, rnd);		/* c = s1 * t */
	}
	else
	{
		mpfr_div(tmp1, b, a, rnd);		/* t = -b / a */
		mpfr_neg(tmp1, tmp1, rnd);

		mpfr_sqr(tmp2, tmp1, rnd);		/* c = 1 / sqrt(1 + t^2) */
		mpfr_add_ui(tmp2, tmp2, 1, rnd);
		mpfr_sqrt(tmp2, tmp2, rnd);
		mpfr_ui_div(c, 1, tmp2, rnd);
		mpfr_mul(s, c, tmp1, rnd);		/* s = c * t */
	}

	mpfr_clears(tmp1, tmp2, (mpfr_ptr) NULL);
}

static void svd2( const mps_vec_ptr D,
                  const mps_vec_ptr F,
                  const mps_blk_ptr U,
                  const mps_blk_ptr V,
                  const mpfr_rnd_t rnd)
{
	unsigned int i;
	mpfr_t c, s, a11, a12, a21, a22, tmp1, tmp2;

	mpfr_ptr d0, f0, d1;
	mpfr_ptr Uip, Uiq, Vip, Viq;
	mpfr_t zero;		/* TODO: Temporary variables and constants */

	const unsigned int M = MPS_BLOCK_NUMROW(U);	/* FIXME: Is this right? */
	const unsigned int N = MPS_BLOCK_NUMROW(V);

	MPS_ASSERT_MSG( MPS_VEC_VALID(D), "Expected vector as 1st argument in svd2()\n");
	MPS_ASSERT_MSG( MPS_VEC_VALID(F), "Expected vector as 2nd argument in svd2()\n");

	mpfr_inits2(MPS_VEC_PREC(D), c, s, a11, a12, a21, a22, tmp1, tmp2, (mpfr_ptr) NULL);

	d0 = mps_get_ele_vec(D, 1);
	f0 = mps_get_ele_vec(F, 1);

	d1 = mps_get_ele_vec(D, 2);

	if ( mpfr_zero_p(d0) )
	{
		/* eliminate off-diagonal element in [0,f0; 0,d1] to make [d,0; 0,0] */

		create_givens(f0, d1, c, s, rnd);

		/* compute B <= G^T B X, where X = [0, 1; 1, 0]*/

		mpfr_mul(tmp1, c, f0, rnd);
		mpfr_mul(tmp2, s, d1, rnd);
		mpfr_sub(tmp1, tmp1, tmp2, rnd);	/* c * f0 - c * d1 */

		mps_set_ele_vec(D, 1, tmp1, rnd);

		mpfr_mul(tmp1, s, f0, rnd);
		mpfr_mul(tmp2, c, d1, rnd);
		mpfr_add(tmp1, tmp1, tmp2, rnd);	/* s * f0 + c * d1 */

		mps_set_ele_vec(F, 1, tmp1, rnd);
		mps_set_ele_vec(D, 2, zero, rnd);

		/* compute U <= U G */
		for ( i = 1; i <= M; ++i )
		{
			Uip = mps_get_ele_blk(U, i, 1);
			Uiq = mps_get_ele_blk(U, i, 2);

			mpfr_mul(tmp1, c, Uip, rnd);	/* c * Uip - s * Uiq */
			mpfr_mul(tmp2, s, Uiq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, Uip, s, rnd);	/* s * Uip + c * Uiq */

			mpfr_set(Uip, tmp1, rnd);		/* no longer need to save value  in Uip */

			mpfr_mul(tmp1, c, Uiq, rnd);
			mpfr_add(Uiq, tmp1, tmp2, rnd);
		}

		/* compute V <= V X */
		mps_col_exg_blk(V, 1, 2);

		mpfr_clears(c, s, a11, a12, a21, a22, tmp1, tmp2, (mpfr_ptr) NULL);
		return;
	}
	else if ( mpfr_zero_p(d1) )
	{
		/* eliminate off-diagonal element in [d0,f0; 0,0] */

		create_givens(d0, f0, c, s, rnd);

		/* compute B <= B G */

		mpfr_mul(tmp1, d0, c, rnd);		/* tmp1 = d0 * c - f0 * s */
		mpfr_mul(tmp2, f0, s, rnd);
		mpfr_sub(tmp1, tmp1, tmp2, rnd);

		mpfr_set_ui(f0, 0, rnd);

		/* compute V <= V G */

		for ( i = 1; i <= N; ++i )
		{
			Vip = mps_get_ele_blk(V, i, 1);
			Viq = mps_get_ele_blk(V, i, 2);

			mpfr_mul(tmp1, c, Vip, rnd);	/* c * Vip - s * Viq */
			mpfr_mul(tmp2, s, Viq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, s, Vip, rnd);	/* done with value in Vip */
			mpfr_set(Vip, tmp1, rnd);		/* s * Vip + c * Viq */
			mpfr_mul(tmp1, c, Viq, rnd);
			mpfr_add(Viq, tmp1, tmp2, rnd);
		}

		mpfr_clears(c, s, a11, a12, a21, a22, tmp1, tmp2, (mpfr_ptr) NULL);
		return;
	}
	else
	{
		/* Make columns orthogonal, A = [d0, f0; 0, d1] */
		create_schur(d0, f0, d1, c, s, rnd);

		/* compute B <= B G */

		mpfr_mul(tmp1, c, d0, rnd);		/* a11 = c * d0 - s * f0 */
		mpfr_mul(tmp2, s, f0, rnd);
		mpfr_sub(a11, tmp1, tmp2, rnd);

		mpfr_mul(a21, s, d1, rnd);		/* a21 = -s * d1*/
		mpfr_neg(a21, a21, rnd);


		mpfr_mul(tmp1, s, d0, rnd);		/* a12 = s * d0 + c * f0 */
		mpfr_mul(tmp2, c, f0, rnd);
		mpfr_add(a12, tmp1, tmp2, rnd);

		mpfr_mul(a22, c, d1, rnd);		/* a22 = c * d1 */

		/* compute V <= V G */

		for ( i = 1; i <= N; ++i )
		{
			Vip = mps_get_ele_blk(V, i, 1);
			Viq = mps_get_ele_blk(V, i, 2);

			mpfr_mul(tmp1, c, Vip, rnd);	/* c * Vip - s * Viq */
			mpfr_mul(tmp2, s, Viq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, s, Vip, rnd);	/* s * Vip + c * Viq */
			mpfr_set(Vip, tmp1, rnd);
			mpfr_mul(tmp1, c, Viq, rnd);
			mpfr_add(Viq, tmp1, tmp2, rnd);
		}

		/* eliminate off-diagonal elements,
		 * bringing column with largest norm to first column */

		mpfr_hypot(tmp1, a11, a21, rnd);
		mpfr_hypot(tmp2, a12, a22, rnd);

		if ( mpfr_less_p(tmp1, tmp2) )
		{
			/* B <= B X */
			mpfr_swap(a11, a12);
			mpfr_swap(a21, a22);

			/* V <= V X */
			mps_col_exg_blk(V, 1, 2);
		}

		create_givens(a11, a21, c, s, rnd);

		/* compute B <= G^T B */

		mpfr_mul(tmp1, c, a11, rnd);		/* c * a11 - s * a21 */
		mpfr_mul(tmp2, s, a21, rnd);
		mpfr_sub(d0, tmp1, tmp2, rnd);

		mpfr_mul(tmp1, c, a12, rnd);		/* c * a12 - s * a22 */
		mpfr_mul(tmp2, s, a22, rnd);
		mpfr_sub(f0, tmp1, tmp2, rnd);

		mpfr_mul(tmp1, s, a12, rnd);		/* s * a12 + c * a22 */
		mpfr_mul(tmp2, c, a22, rnd);
		mpfr_add(d1, tmp1, tmp2, rnd);

		/* compute U <= U G */

		for ( i = 1; i <= M; ++i )
		{
			Uip = mps_get_ele_blk(U, i, 1);
			Uiq = mps_get_ele_blk(U, i, 2);

			mpfr_mul(tmp1, c, Uip, rnd);	/* c * Uip - s * Uiq */
			mpfr_mul(tmp2, s, Uiq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, s, Uip, rnd);	/* s * Uip + c * Uiq */
			mpfr_set(Uip, tmp1, rnd);
			mpfr_mul(tmp1, c, Uiq, rnd);
			mpfr_add(Uiq, tmp1, tmp2, rnd);
		}

		mpfr_clears(c, s, a11, a12, a21, a22, tmp1, tmp2, (mpfr_ptr) NULL);
		return;
	}

}

static void chase_out_intermediate_zero( const mps_blk_ptr D,
                                         const mps_blk_ptr F,
                                         const mps_blk_ptr U,
                                         const unsigned int k0,
                                         const mpfr_rnd_t rnd)
{
	const unsigned int M = MPS_BLOCK_NUMCOL(U);	/* TODO: Is this right? */
	const unsigned int N = MPS_BLOCK_NUMCOL(U);
	mpfr_t c, s;		/* TODO: Temporary variables */
	mpfr_ptr x, y, Uip, Uiq, z;
	unsigned int k, i;
	mpfr_t tmp1, tmp2;

	mpfr_inits2(MPS_BLOCK_PREC(U), c, s, (mpfr_ptr) NULL);
	mpfr_inits2(MPS_BLOCK_PREC(U), tmp1, tmp2, (mpfr_ptr) NULL);

	MPS_ASSERT_MSG( MPS_VEC_VALID(D),
			"1st argument expected vector in chase_out_intermediate_zero()\n");
	MPS_ASSERT_MSG( MPS_VEC_VALID(F),
			"2nd argument expected vector in chase_out_intermediate_zero()\n");


	x = mps_get_ele_vec(F, k0);
	y = mps_get_ele_vec(D, k0 + 1);

	for ( k = k0; k < N - 1; ++k )
	{
		mpfr_set(tmp1, x, rnd);		/* to pass in -x to create_givens() */
		mpfr_neg(tmp1, tmp1, rnd);
		create_givens(y, tmp1, c, s, rnd);

		/* compute U <= U G */
		for ( i = 1; i <= M; ++i )
		{
			Uip = mps_get_ele_blk(U, i, k0);
			Uiq = mps_get_ele_blk(U, i, k + 1);

			mpfr_mul(tmp1, c, Uip, rnd);		/* c * Uip - s * Uiq */
			mpfr_mul(tmp2, s, Uiq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, s, Uip, rnd);		/* s * Uip + c * Uiq */
			mpfr_set(Uip, tmp1, rnd);
			mpfr_mul(tmp1, c, Uiq, rnd);
			mpfr_add(Uiq, tmp1, tmp2, rnd);
		}

		/* compute B <= G^T B */

		mpfr_mul(tmp1, s, x, rnd);				/* s * x + c * y */
		mpfr_mul(tmp2, c, y, rnd);
		mpfr_add(mps_get_ele_vec(D, k + 1), tmp1, tmp2, rnd);


		if ( k == k0 )
		{
			mpfr_mul(tmp1, c, x, rnd);
			mpfr_mul(tmp2, s, y, rnd);
			mpfr_sub(mps_get_ele_vec(F, k), tmp1, tmp2, rnd);
		}

		if ( k < N - 2 )
		{
			z = mps_get_ele_vec(F, k + 1);

			mpfr_mul(x, s, z, rnd);			/* x = -s * z */
			mpfr_neg(x, x, rnd);

			mpfr_mul(z, c, z, rnd);			/* c * z */

			y = mps_get_ele_vec(D, k + 2);
		}
	}

	mpfr_clears(c, s, (mpfr_ptr) NULL);
	mpfr_clears(tmp1, tmp2, (mpfr_ptr) NULL);
}

static void chase_out_trailing_zero( const mps_vec_ptr D,
                                     const mps_vec_ptr F,
                                     const mps_blk_ptr V,
                                     const mpfr_rnd_t rnd)
{
	mpfr_ptr Vip, Viq, z;
	mpfr_t c, s;		/* TODO: Temporary variables */
	mpfr_t x, y, tmp1, tmp2;
	unsigned int k, i;
	const unsigned int n = MPS_VEC_SIZE(D);		/* TODO: Is this right? */
	const unsigned int N = MPS_BLOCK_NUMROW(V);	/* TODO: Is this right? */

	MPS_ASSERT_MSG(MPS_VEC_VALID(D),
					"Expected vector as 1st argument to chase_out_trailing_zero()\n");
	MPS_ASSERT_MSG(MPS_VEC_VALID(F),
					"Expected vector as 2nd argument to chase_out_trailing_zero()\n");

	mpfr_inits2(MPS_VEC_PREC(D), c, s, x, y, tmp1, tmp2, (mpfr_ptr) NULL);

	mpfr_set(x, mps_get_ele_vec(D, n - 2), rnd);
	mpfr_set(y, mps_get_ele_vec(F, n - 2), rnd);

	/* TODO: Might not need to copy x, but I don't think so. Too tired to figure it out */
	for ( k = n; k-- > 1;)
	{
		create_givens(x, y, c, s, rnd);

		/* compute V <= V G where G = [ c, s; -s, c] */
		for ( i = 1; i <= N; ++i )
		{
			Vip = mps_get_ele_blk(V, i, k);
			Viq = mps_get_ele_blk(V, i, n - 1);

			mpfr_mul(tmp1, c, Vip, rnd);		/* c * Vip - s * Viq */
			mpfr_mul(tmp2, s, Viq, rnd);
			mpfr_sub(tmp1, tmp1, tmp2, rnd);

			mpfr_mul(tmp2, s, Vip, rnd);		/* s * Vip + c * Viq */
			mpfr_set(Vip, tmp1, rnd);
			mpfr_mul(tmp1, c, Viq, rnd);
			mpfr_add(Viq, tmp1, tmp2, rnd);
		}

		/* compute B <= B G */

		mpfr_mul(tmp1, c, x, rnd);				/* c * x - s * y */
		mpfr_mul(tmp2, s, y, rnd);

		z = mps_get_ele_vec(D, k);
		mpfr_sub(z, tmp1, tmp2, rnd);

		if ( k == n - 2 )
		{
			z = mps_get_ele_vec(F, k);

			mpfr_mul(tmp1, s, x, rnd);			/* s * x + c * y*/
			mpfr_mul(tmp2, c, y, rnd);
			mpfr_add(z, tmp1, tmp2, rnd);
		}

		if ( k > 0 )
		{
			z = mps_get_ele_vec(F, k - 1);
			mpfr_mul(y, s, z, rnd);
			mpfr_mul(z, z, c, rnd);
			mpfr_set(x, mps_get_ele_vec(D, k - 1), rnd);
		}
	}

	mpfr_clears(c, s, x, y, tmp1, tmp2, (mpfr_ptr) NULL);
}

/* local aliases for shared temporary variables. I can't decide if I love this or hate it */
/* SO MANY TEMPORARY VARIABLES. With more work can maybe reduce. */
#define ak tmp1
#define bk tmp2
#define zk tmp3
#define c tmp4
#define s tmp5
#define y tmp6
#define z tmp7
#define utmp1 tmp8 /* user temps, for other uses and less confusion */
#define utmp2 tmp9
void qrstep( const mps_vec_ptr D,
             const mps_vec_ptr F,
             const mps_blk_ptr U,
             const mps_blk_ptr V,
             const mpfr_rnd_t rnd)
{
	const unsigned int M = MPS_BLOCK_NUMROW(U);	/* TODO: Is this right? */
	const unsigned int N = MPS_BLOCK_NUMROW(V);
	const unsigned int n = MPS_VEC_SIZE(D);
	unsigned int k, i;

	mpfr_ptr ap, bp, aq, bq;
	mpfr_ptr Vip, Viq;
	mpfr_ptr Uip, Uiq;

	/* Note: aq is fine as a pointer */

	mpfr_t tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, zero;

	mpfr_inits2(MPS_VEC_PREC(D), tmp1, tmp2, tmp3, tmp4, \
                tmp5, tmp6, tmp7, tmp8, tmp9, zero, (mpfr_ptr) NULL);
	mpfr_set_ui(zero, 0, rnd);

	MPS_ASSERT_MSG( MPS_VEC_VALID(D),
					"Expected vector as 1st argument in qrstep()\n");
	MPS_ASSERT_MSG( MPS_VEC_VALID(F),
					"Expected vector as 2nd argument in qrstep()\n");

	if ( n == 1 )
		return;		/* shouldn't happen */

	if ( n == 2 )	/* compute 2x2 svd directly */
	{
		svd2(D, F, U, V, rnd);
		return;
	}

	/* chase out any zeroes on the diagonal */
	{
		mpfr_ptr d_i;
		for ( i = 1; i < n; ++i )
		{
			d_i = mps_get_ele_vec(D, i);

			if ( mpfr_zero_p(d_i) )
			{
				chase_out_intermediate_zero(D, F, U, i, rnd);
				return;
			}
		}
	}

	{
		/* chase out any zeroes at the end of the diagonal */
		mpfr_ptr d_nm1 = mps_get_ele_vec(D, n - 1);
		if ( mpfr_zero_p(d_nm1) )
		{
			chase_out_trailing_zero(D, F, V, rnd);
			return;
		}
	}

	#define mu utmp1
	/* apply QR reduction steps to the diagonal and offdiagonal */
	{
		mpfr_ptr d0, f0, d1, f1;

		d0 = mps_get_ele_vec(D, 1);
		f0 = mps_get_ele_vec(F, 1);

		d1 = mps_get_ele_vec(D, 2);
		f1 = mps_get_ele_vec(F, 2);

		trailing_eigenvalue(mu, D, F, rnd);

		mpfr_sqr(y, d0, rnd);		/* y = d0^2 - mu */
		mpfr_sub(y, y, mu, rnd);
		mpfr_mul(z, d0, f0, rnd);	/* z = d0 * f0 */

		/* set up the recurrence for Givens rotation on a bidiagonal matrix */

		mpfr_set_ui(ak, 0, rnd);
		mpfr_set_ui(bk, 0, rnd);

		ap = d0;
		bp = f0;

		aq = d1;
		bq = f1;
	}
	#undef mu

	for ( k = 1; k < n; ++k )
	{
		create_givens(y, z, c, s, rnd);
		/* Compute V <= V G */

		for ( i = 1; i <= N; ++i )
		{
			Vip = mps_get_ele_blk(V, i, k);
			Viq = mps_get_ele_blk(V, i, k + 1);

			mpfr_mul(utmp1, c, Vip, rnd);		/* c * Vip - s * Viq */
			mpfr_mul(utmp2, s, Viq, rnd);
			mpfr_sub(utmp1, utmp1, utmp2, rnd);

			mpfr_mul(utmp2, s, Vip, rnd);		/* s * Vip + c * Viq */
			mpfr_set(Vip, utmp1, rnd);
			mpfr_mul(utmp1, c, Viq, rnd);
			mpfr_add(Viq, utmp1, utmp2, rnd);
		}

		/* compute B <= B G */
		{
			mpfr_ptr bk1;
			/* calculate ap1 ( put directly into ak) */
			mpfr_mul(ak, c, ap, rnd);	/* ap1 = s * ap + c * bp */
			mpfr_mul(utmp1, s, bp, rnd);
			mpfr_sub(ak, ak, utmp1, rnd);

			if ( k > 1 )	/* do this before overwriting bk */
			{
				/* calculate bk1. it's only used under this condition, so why bother before? */
				bk1 = mps_get_ele_vec(F, k - 1);
				mpfr_mul(bk1, c, bk, rnd);	/* bk1 = c * bk - s * z */
				mpfr_mul(utmp1, s, z, rnd);
				mpfr_sub(bk1, bk1, utmp1, rnd);
			}

			/* calculate bp1 (directly into bk) */
			mpfr_mul(bk, s, ap, rnd);	/* bp1 = s * ap + c * bp */
			mpfr_mul(utmp1, c, bp, rnd);
			mpfr_add(bk, bk, utmp1, rnd);

			/* calculate zp1 (directly into zk) */
			mpfr_mul(zk, s, aq, rnd);	/* zp1 = -s * aq */
			mpfr_neg(zk, zk, rnd);

			/* calculate aq1 (directly into ap) */
			mpfr_mul(ap, c, aq, rnd);	/* aq1 = c * aq */

			if ( k < n - 2 )
			{
				bp = mps_get_ele_vec(F, k + 1);
			}
			else
			{
				bp = zero;
			}

			mpfr_set(y, ak, rnd);
			mpfr_set(z, zk ,rnd);
		}

		create_givens(y, z, c, s, rnd);

		/* compute U <= U G */

		for ( i = 1; i <= M; ++i )
		{
			Uip = mps_get_ele_blk(U, i, k);
			Uiq = mps_get_ele_blk(U, i, k + 1);
			mpfr_mul(utmp1, c, Uip, rnd);	/* c * Uip - s * Uiq */
			mpfr_mul(utmp2, s, Uiq, rnd);
			mpfr_sub(utmp1, utmp1, utmp2, rnd);

			mpfr_mul(utmp2, s, Uip, rnd);	/* s * Uip + c * Uiq */
			mpfr_set(Uip, utmp1, rnd);
			mpfr_mul(utmp1, c, Uiq, rnd);
			mpfr_add(Uiq, utmp1, utmp1, rnd);
		}

		#define ap1 utmp2		/* local alias */
		/* compute B <= G^T B */
		{
			/* calculate ak1 (directly into ak) */
			mpfr_mul(ak, c, ak, rnd);		/* ak1 = c * ak - s * zk */
			mpfr_mul(utmp1, s, zk, rnd);
			mpfr_sub(ak, ak, utmp1, rnd);

			/* calculate ap1 */
			mpfr_mul(ap1, s, bk, rnd);		/* ap1 = s * bk + c * ap */
			mpfr_mul(utmp1, c, ap, rnd);
			mpfr_add(ap1, ap1, utmp1, rnd);

			/* calculate bk1 (directly into bk) */
			mpfr_mul(bk, c, bk, rnd);		/* bk1 = c * bk - s * ap */
			mpfr_mul(zk, s, ap, rnd);
			mpfr_sub(bk, bk, zk, rnd);

			/* calculate zk1 (directly into zk) */
			mpfr_mul(zk, s, bp, rnd);		/* zk1 = -s * bp */
			mpfr_neg(zk, zk, rnd);

			/* calculate bp1 (directly into bp) */
			mpfr_mul(bp, c, bp, rnd);		/* bp1 = c * bp */

			mps_set_ele_vec(D, k, ak, rnd);	/* this is the new value of ak (ak1) */

			mpfr_set(ap, ap1, rnd);

			if ( k < n - 2 )
			{
				aq = mps_get_ele_vec(D, k + 2);
			}
			else
			{
				aq = zero;
			}

			mpfr_set(y, bk, rnd);
			mpfr_set(z, zk, rnd);
		}
	#undef ap1

	}
	mps_set_ele_vec(F, n - 2, bk, rnd);
	mps_set_ele_vec(D, n - 1, ap, rnd);

}
#undef ak
#undef bk
#undef zk
#undef c
#undef s
#undef y
#undef z
#undef utmp1
#undef utmp2

