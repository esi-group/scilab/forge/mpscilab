/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"
#include <math.h>

/* Set each element of rop to the factorial of op. */
int mps_factorial( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);
    long unsigned int tmp;

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_factorial()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        if( mpfr_nan_p(subop) || mpfr_sgn(subop) < 0 || !mpfr_integer_p(subop) )
        {
            mpfr_set_nan(subrop);
            continue;
        }

        tmp = mpfr_get_ui( subop, GMP_RNDN );
        mpfr_fac_ui( subrop, tmp, rnd );
    }

    return 0;
}

int mps_factorial_double( const mps_ptr rop, const double op[], const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            if( isnan(op[i-1]) || op[i-1] < 0 || fmod(op[i-1], 1) != 0 )
            {
                mpfr_set_nan(subrop);
                continue;
            }
            mpfr_fac_ui( subrop, (long unsigned int)op[i-1], rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            if( isnan(op[i-1]) || op[i-1] < 0 || fmod(op[i-1], 1) != 0 )
            {
                mpfr_set_nan(subrop);
                continue;
            }
            mpfr_fac_ui( subrop, (long unsigned int)op[i-1], rnd );

        }
    }

    return 0;
}
