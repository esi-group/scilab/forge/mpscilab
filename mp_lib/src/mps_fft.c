/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <math.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* Apparently this might not work without the imaginary part.
 Also, some bounds are wrong and get out of bounds assertion failures. */
/* in-place radix-2 decimation in time.
 Just accesses the elements sequentially from the matrix.
 The size of the input matrix must be a power of 2 */
void mps_real_fft_dit( const mps_ptr data, const mpfr_rnd_t rnd  )
{
    unsigned int i, j, k, n1, n2;
    mpfr_t a, e, npi2, c, s;
    mpfr_t utmp1, utmp2;
    mpfr_ptr xkn1, xk;
    const unsigned int N = MPS_SIZE(data);

#ifdef _MSC_VER
    const unsigned int M = log(N) / log(2);
#else
    const unsigned int M = log2(N);
#endif

    /* Test for size is a power of 2 */
    MPS_ASSERT_MSG( (N > 0) && (( N & (N - 1)) == 0),
        "Expected size to be a power of 2 in mps_real_fft_dit()\n");

    mpfr_inits2( MPS_PREC(data), a, e, c, s, npi2, utmp1, utmp2, (mpfr_ptr) NULL );

    j = 1;      /* reverse */
    n2 = N / 2;

    for ( i = 2; i < N; ++i )
    {
        n1 = n2;
        while ( j >= n1 )       /* TODO: Check bounds */
        {
            j -= n1;
            n1 /= 2;
        }

        j += n1;

        if ( i < j )
        {
            mps_indx_exg(data, i, j);
        }
    }

    n1 = 1;
    n2 = 2;

    mpfr_const_pi(npi2, rnd);       /* npi2 = - 2 * pi */
    mpfr_mul_si(npi2, npi2, -2, rnd);

    for ( i = 1; i <= M; ++i )
    {
        n1 = n2;
        n2 = 2 * n2;
        mpfr_div_ui(e, npi2, n2, rnd);
        mpfr_set_ui(a, 0, rnd);

        for ( j = 1; j <= n1; ++j )
        {
            mpfr_cos(c, a, rnd);
            mpfr_sin(s, a, rnd);
            mpfr_add(a, a, e, rnd);

            for ( k = j; k <= N; k += n2 )
            {
                xkn1 = mps_get_ele_seq(data, k + n1 - 1);
                xk = mps_get_ele_seq(data, k);

                mpfr_mul(utmp1, c, xkn1, rnd);
                mpfr_mul(utmp2, s, xkn1, rnd);

                mpfr_sub(xkn1, xk, utmp1, rnd);
                mpfr_add(xk, xk, utmp1, rnd);
            }
        }
    }

    mpfr_clears( a, e, c, s, npi2, utmp1, utmp2, (mpfr_ptr) NULL );

    return;
}

/* straightforward discrete sine transform */
void mps_real_dst( const mps_ptr out, const mps_ptr in, const mpfr_rnd_t rnd)
{
    unsigned int bin, k;
    mpfr_t utmp1, utmp2, piN;
    mpfr_ptr subin, subout;
    const unsigned int length = MPS_SIZE(in);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(out, in),
                    "Expected input and output lists to be same size in mps_real_dst()\n");

    mpfr_inits2(MPS_PREC(out), utmp1, utmp2, piN, (mpfr_ptr) NULL);

    mpfr_const_pi(piN, rnd);
    mpfr_div_ui(piN, piN, length, rnd);     /* pi / length. Only need to calc. this once */

    for ( bin = 1; bin <= length; ++bin )
    {
        subout = mps_get_ele_seq(out, bin);
        mpfr_set_ui(subout, 0, rnd);

        mpfr_mul_ui(utmp1, piN, bin - 1, rnd);

        for ( k = 1; k <= length; ++k )
        {
            subin = mps_get_ele_seq(in, k);
            mpfr_mul_ui(utmp2, utmp1, k - 1, rnd);  /* a = ((bin-1) * pi * (k-1)) / length*/
            mpfr_sin(utmp2, utmp2, rnd);
            mpfr_mul(utmp2, utmp2, subin, rnd);
            mpfr_add(subout, subin, utmp2, rnd);     /* out[bin] += sin(a) * input[k] */
        }

    }

    mpfr_clears(utmp1, utmp2, piN, (mpfr_ptr) NULL);
}

/* straightforward discrete cosine transform */
void mps_real_dct( const mps_ptr out, const mps_ptr in, const mpfr_rnd_t rnd)
{
    unsigned int bin, k;
    mpfr_t utmp1, utmp2, piN;
    mpfr_ptr subin, subout;
    const unsigned int length = MPS_SIZE(in);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(out, in),
                    "Expected input and output lists to be same size in mps_real_dct()\n");

    mpfr_inits2(MPS_PREC(out), utmp1, utmp2, piN, (mpfr_ptr) NULL);

    mpfr_const_pi(piN, rnd);
    mpfr_div_ui(piN, piN, length, rnd);     /* pi / length. Only need to calc. this once */

    for ( bin = 1; bin <= length; ++bin )
    {
        subout = mps_get_ele_seq(out, bin);
        mpfr_set_ui(subout, 0, rnd);

        mpfr_mul_ui(utmp1, piN, bin - 1, rnd);

        for ( k = 1; k <= length; ++k )
        {
            subin = mps_get_ele_seq(in, k);
            mpfr_mul_ui(utmp2, utmp1, k - 1, rnd);  /* a = ((bin-1) * pi * (k-1)) / length*/
            mpfr_cos(utmp2, utmp2, rnd);
            mpfr_mul(utmp2, utmp2, subin, rnd);
            mpfr_add(subout, subout, utmp2, rnd);    /* out[bin] += cos(a) * input[k] */
        }

    }

    mpfr_clears(utmp1, utmp2, piN, (mpfr_ptr) NULL);
}

/* straightforward discrete fourier transform without the FFT / efficiency.
 Setting sign to true does forward, false inverse transform */
void mps_real_dft( const mps_ptr sinout,
                   const mps_ptr cosout,
                   const mps_ptr in,
                   const char sign,
                   const mpfr_rnd_t rnd)
{
    unsigned int bin, k;
    mpfr_t utmp1, utmp2, utmp3, pi2N;
    mpfr_ptr subin, subsin, subcos;
    const unsigned int length = MPS_SIZE(in);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(sinout, in) && MPS_SAME_SIZE(cosout, in),
                    "Expected input and output lists to be same size in mps_real_dct()\n");

    mpfr_inits2(MPS_PREC(sinout), utmp1, utmp2, utmp3, pi2N, (mpfr_ptr) NULL);

    mpfr_const_pi(pi2N, rnd);
    mpfr_div_ui(pi2N, pi2N, length, rnd);     /* 2*pi / length. Only need to calc. this once */
    mpfr_mul_ui(pi2N, pi2N, 2, rnd);

    for ( bin = 1; bin <= length; ++bin )
    {
        subsin = mps_get_ele_seq(sinout, bin);
        subcos = mps_get_ele_seq(cosout, bin);
        mpfr_set_ui(subsin, 0, rnd);
        mpfr_set_ui(subcos, 0, rnd);

        mpfr_mul_ui(utmp1, pi2N, bin - 1, rnd);

        for ( k = 1; k <= length / 2; ++k )
        {
            subin = mps_get_ele_seq(in, k);         /* common part */
            mpfr_mul_ui(utmp2, utmp1, k - 1, rnd);  /* a = ((bin-1) * pi * (k-1)) / length*/

            mpfr_cos(utmp3, utmp2, rnd);            /* cos part */
            mpfr_mul(utmp3, utmp3, subin, rnd);
            mpfr_add(subcos, subcos, utmp3, rnd);   /* cosout[bin] += cos(a) * input[k] */

            mpfr_sin(utmp3, utmp2, rnd);            /* sin part */
            if ( sign )
                mpfr_neg(utmp3, utmp3, rnd);
            mpfr_add(subsin, subsin, utmp3, rnd);
        }

    }

    mpfr_clears(utmp1, utmp2, utmp3, pi2N, (mpfr_ptr) NULL);
}

