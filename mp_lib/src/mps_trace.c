/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* Evaluate the trace of a square matrix. */
int mps_trace( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    unsigned int i;
    const unsigned int matsize = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_trace().\n" );

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_trace()\n" );

    tab = malloc( sizeof(void*) * matsize );

    for( i = 0; i < matsize; i++ )
    {
        tab[i] = mps_get_ele( op, i+1, i+1 );
    }

    mpfr_sum( mps_get_ele( rop, 1, 1 ), tab, matsize, GMP_RNDN );

    free( tab );

    return 0;
}

int mps_trace_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop1, subrop;
    unsigned int i;
    const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_trace_quick().\n" );

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_trace_quick()\n" );

    subrop = mps_get_ele( rop, 1, 1 );
    subop1 = mps_get_ele( op, 1, 1 );
    mpfr_set( subrop, subop1 , GMP_RNDN );

    for ( i = 2; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( op, i, i );
        mpfr_add( subrop, subrop, subop1, GMP_RNDN );
    }

    return 0;
}

int mps_trace_double( const mps_ptr rop, const double op[], const mps_order_t order, const unsigned int m )
{
    mpfr_ptr subrop;
    unsigned int i, l;

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_trace_double().\n" );

    subrop = mps_get_ele( rop, 1, 1 );
    mpfr_set_zero( subrop, 1);

    for ( i = 1; i <= m; ++i )
    {
        l = (i-1) + m*(i-1);
        mpfr_add_d( subrop, subrop, op[l], GMP_RNDN );
    }

    return 0;
}
