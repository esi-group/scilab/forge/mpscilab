/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"


unsigned int mpt_linear( unsigned int n, 
                         mpfr_prec_t prec, 
                         int a, 
                         int b, 
                         int c )
{
    unsigned int num = 1;
    unsigned int max;

    MPS_ASSERT_MSG( n >= 1,
        "n is less than 1 in mpt_linear()\n" );

    MPS_ASSERT_MSG( prec >= 2,
        "prec is less than 2 in mpt_linear()\n" );

    MPS_ASSERT_MSG( a >= 0,
        "a is negative in mpt_linear()\n" );

    MPS_ASSERT_MSG( b >= 0,
        "b is negative in mpt_linear()\n" );

    MPS_ASSERT_MSG( c >= 0,
        "c is negative in mpt_linear()\n" );


    if( n <= (unsigned)a )
        return 1;
    else
    {
        num = 1 + n/b;
    }

    max = mps_get_num_threads();

    if( num > max )
        num = max;

    return num;

}


unsigned int mpt_const( unsigned int a )
{

/*    MPS_ASSERT_MSG( a >= 0,
        "Number of threads specified is negative in mpt_const()\n" ); */

    return a;
}


unsigned int mpt_global()
{
    return mps_get_num_threads();
}

