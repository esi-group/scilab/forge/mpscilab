/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

int mps_mean( mps_ptr rop, mps_ptr op )
{
    mpfr_ptr subrop;
    unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_mean().\n" );

    subrop = mps_get_ele(rop, 1, 1);
    mps_sum( rop, op );
    
	mpfr_div_ui(subrop, subrop, matsize, GMP_RNDN);

	return 0;
}


int mps_mean_quick( mps_ptr rop, mps_ptr op )
{
    mpfr_ptr subrop;
    unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_mean_quick().\n" );

    subrop = mps_get_ele(rop, 1, 1);
    mps_sum_quick( rop, op );
    
	mpfr_div_ui(subrop, subrop, matsize, GMP_RNDN);

	return 0;
}


int mps_meanr( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    mpfr_ptr subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Result operand is of the wrong size in mps_meanr().\n" );

    tab = malloc( sizeof(void*) * MPS_NUMCOL(op) );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 0; j < MPS_NUMCOL(op); j++ )
        {
            tab[j] = mps_get_ele( op, i, j+1 );
        }

        subrop = mps_get_ele_seq( rop, i );
        mpfr_sum( subrop, tab, MPS_NUMCOL(op), GMP_RNDN );
    	mpfr_div_ui(subrop, subrop, MPS_NUMCOL(op), GMP_RNDN);
    }

    free( tab );

    return 0;
}


int mps_meanr_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop, subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Result operand is of the wrong size in mps_meanr_quick().\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele( op, i, 1 );
        mpfr_set( subrop, subop, GMP_RNDN );

        for( j = 2; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );
            mpfr_add( subrop, subrop, subop, GMP_RNDN );
        }
        
        mpfr_div_ui(subrop, subrop, MPS_NUMCOL(op), GMP_RNDN);
    }

    return 0;
}


int mps_meanc( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    mpfr_ptr subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Result operand is of the wrong size in mps_meanc().\n" );

    tab = malloc( sizeof(void*) * MPS_NUMROW(op) );

    for( i = 1; i <= MPS_NUMCOL(op); i++ )
    {
        for( j = 0; j < MPS_NUMROW(op); j++ )
        {
            tab[j] = mps_get_ele( op, j+1, i );
        }

        subrop = mps_get_ele_seq( rop, i );
        mpfr_sum( subrop, tab, MPS_NUMROW(op), GMP_RNDN );
    	mpfr_div_ui(subrop, subrop, MPS_NUMROW(op), GMP_RNDN);
    }

    free( tab );

    return 0;
}


int mps_meanc_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop, subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Result operand is of the wrong size in mps_meanc_quick().\n" );

    for( i = 1; i <= MPS_NUMCOL(op); i++ )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele( op, 1, i );
        mpfr_set( subrop, subop, GMP_RNDN );

        for( j = 2; j <= MPS_NUMROW(op); j++ )
        {
            subop = mps_get_ele( op, j, i );
            mpfr_add( subrop, subrop, subop, GMP_RNDN );
        }

        mpfr_div_ui(subrop, subrop, MPS_NUMROW(op), GMP_RNDN);
    }

    return 0;
}
