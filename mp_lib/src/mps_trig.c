/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Set each element of rop to sin(op) */
int mps_sin( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_sin()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sin, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_sin( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_sin accepting an array of double as an input. */
int mps_sin_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sin_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_sin( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to cos(op) */
int mps_cos( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_cos()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_cos, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_cos( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_cos accepting an array of double as an input. */
int mps_cos_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_cos_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_cos( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


/* Set each element of rop to tan(op) */
int mps_tan( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_tan()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_tan, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_tan( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_tan accepting an array of double as an input. */
int mps_tan_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_tan_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_tan( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to csc(op) */
int mps_csc( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_csc()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_csc, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_csc( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_csc accepting an array of double as an input. */
int mps_csc_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_csc_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_csc( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to sec(op) */
int mps_sec( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_sec()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sec, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_sec( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_sec accepting an array of double as an input. */
int mps_sec_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sec_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_sec( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to cot(op) */
int mps_cot( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_cot()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_cot, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_cot( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_cot accepting an array of double as an input. */
int mps_cot_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_cot_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_cot( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to asin(op) */
int mps_asin( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_asin()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_asin, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_asin( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_asin accepting an array of double as an input. */
int mps_asin_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_asin_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_asin( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to acos(op) */
int mps_acos( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_acos()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_acos, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_acos( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_acos accepting an array of double as an input. */
int mps_acos_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_acos_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_acos( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to atan(op) */
int mps_atan( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_atan()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_atan, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_atan( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_atan accepting an array of double as an input. */
int mps_atan_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_atan_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_atan( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to sinh(op) */
int mps_sinh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_sinh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sinh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_sinh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_sinh accepting an array of double as an input. */
int mps_sinh_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_sinh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_sinh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to cosh(op) */
int mps_cosh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_cosh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_cosh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_cosh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_cosh accepting an array of double as an input. */
int mps_cosh_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_cosh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_cosh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to tanh(op) */
int mps_tanh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_tanh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_tanh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_tanh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_tanh accepting an array of double as an input. */
int mps_tanh_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_tanh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_tanh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to csch(op) */
int mps_csch( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_csch()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_csch, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_csch( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_csch accepting an array of double as an input. */
int mps_csch_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_csch_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_csch( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to sech(op) */
int mps_sech( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_sech()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sech, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_sech( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_sech accepting an array of double as an input. */
int mps_sech_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_sech_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_sech( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to coth(op) */
int mps_coth( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_coth()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_coth, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_coth( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_coth accepting an array of double as an input. */
int mps_coth_double( const mps_ptr rop, 
                     const double op[],
                     const mps_order_t order,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_coth_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_coth( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to asinh(op) */
int mps_asinh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_asinh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_asinh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_asinh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_asinh accepting an array of double as an input. */
int mps_asinh_double( const mps_ptr rop, 
                      const double op[],
                      const mps_order_t order,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_asinh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_asinh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Set each element of rop to acosh(op) */
int mps_acosh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_acosh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_asinh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_acosh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_acosh accepting an array of double as an input. */
int mps_acosh_double( const mps_ptr rop, 
                      const double op[],
                      const mps_order_t order,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_acosh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_acosh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
} 

/* Set each element of rop to atanh(op) */
int mps_atanh( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_atanh()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_atanh, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_atanh( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

/* Version of mps_atanh accepting an array of double as an input. */
int mps_atanh_double( const mps_ptr rop, 
                      const double op[],
                      const mps_order_t order,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP

        num_thr = GET_NUM_THREADS( mps_atanh_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_atanh( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
} 

