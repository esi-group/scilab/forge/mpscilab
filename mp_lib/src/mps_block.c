/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* access within a submatrix the desired element
 * This is within range of being a macro. */
mpfr_ptr mps_get_ele_blk(const mps_blk_ptr blkptr, unsigned int row, unsigned int col)
{
	char* ptr;
	mps_ptr matptr;

	MPS_ASSERT_MSG( MPS_BLOCK_ROW_BOUND(blkptr, row),
						"Row index out of submatrix bounds in mps_get_ele_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_COL_BOUND(blkptr, col),
						"Column index out of submatrix bounds in mps_get_ele_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_ROW_BOUND(blkptr, row),
						"Row index out of real bounds in mps_get_ele_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_COL_BOUND(blkptr, col),
						"Column index out of real bounds in mps_get_ele_blk()\n");

	row = MPS_BLOCK_MINROW(blkptr) + row - 2;
	col = MPS_BLOCK_MINCOL(blkptr) + col - 2;

	matptr = MPS_BLOCK_MAT(blkptr);
	ptr = (char*) MPS_MPFR_ARRAY(matptr);

	if ( MPS_GET_ORDER(matptr) == MPS_COL_ORDER )
		ptr = ptr + sizeof(mpfr_t) * (row + MPS_NUMROW(matptr) * col);
	else
		ptr = ptr + sizeof(mpfr_t) * (col + MPS_NUMCOL(matptr) * row);

	return (mpfr_ptr) ptr;

}

mpfr_ptr mps_get_ele_seq_blk( const mps_blk_ptr blkptr, unsigned int indx)
{
	char* ptr;
	const mps_ptr matptr = MPS_BLOCK_MAT(blkptr);
	const unsigned int num = MPS_BLOCK_NUMROW(blkptr);

	MPS_ASSERT_MSG( MPS_BLOCK_INDX_BOUND(blkptr, indx),
						"Index out of submatrix bounds in mps_get_ele_seq_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_INDX_BOUND(blkptr, indx),
						"Real index out of real bounds in mps_get_ele_seq_blk()\n");

	--indx;
    indx = (indx/num + MPS_BLOCK_MINCOL(blkptr) - 1) * MPS_NUMROW(matptr) + (indx % num) + MPS_BLOCK_MINROW(blkptr) - 1;

    /* Takes into account tranposed matrices. Currently note optimally fast though. */
    if ( MPS_GET_ORDER(matptr) == MPS_COL_ORDER )
        ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * indx;
    else
        ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t)  * ( ((indx) \
            / MPS_NUMROW(matptr)) + MPS_NUMCOL(matptr) * ((indx) % MPS_NUMROW(matptr)) );

	return (mpfr_ptr) ptr;
}

void mps_set_ele_seq_blk( const mps_blk_ptr blkptr,
                                 unsigned int indx,
                                 const mpfr_ptr item,
                                 const mpfr_rnd_t rnd )
{
	char* ptr;
	mps_ptr matptr = MPS_BLOCK_MAT(blkptr);
	const unsigned int num = MPS_BLOCK_NUMROW(blkptr);

	MPS_ASSERT_MSG( MPS_BLOCK_INDX_BOUND(blkptr, indx),
						"Index out of submatrix bounds in mps_get_ele_seq_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_INDX_BOUND(blkptr, indx),
						"Real index out of real bounds in mps_get_ele_seq_blk()\n");

	--indx;
	indx = (indx/num + MPS_BLOCK_MINCOL(blkptr) - 1) * MPS_NUMROW(matptr) + (indx % num) + MPS_BLOCK_MINROW(blkptr) - 1;

	ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * indx;
	mpfr_set((mpfr_ptr) ptr, item, rnd);
}

/* TODO: Check/fix these other access functions */
mpfr_ptr mps_get_ele_col_blk(const mps_blk_ptr blkptr, unsigned int indx)
{
	char* ptr;
	mps_ptr matptr;
	/* const unsigned int num = MPS_BLOCK_NUMCOL(blkptr); */

    /*Temp vars */
    unsigned int i_tilde, j_tilde;

	MPS_ASSERT_MSG( MPS_BLOCK_INDX_BOUND(blkptr, indx),
						"Index out of submatrix bounds in mps_get_ele_col_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_INDX_BOUND(blkptr, indx),
						"Real index out of real bounds in mps_get_ele_col_blk()\n");

	matptr = MPS_BLOCK_MAT(blkptr);
#if 0
	/* TODO: These indexing calculations might be able to be simplified */
	--indx;
	indx = (indx/num + 1)*MPS_NUMCOL(matptr) + (indx % num) + MPS_BLOCK_MINCOL(blkptr) - 1;
#endif

    --indx;
    j_tilde = ( indx / MPS_BLOCK_NUMROW(blkptr) ) + 1;
    i_tilde = ( indx % MPS_BLOCK_NUMROW(blkptr) ) + 1;

    ptr = (char*) mps_get_ele_blk( blkptr, i_tilde, j_tilde );

#if 0
	if ( MPS_GET_ORDER(matptr) == MPS_COL_ORDER )
	{
		ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * indx;
	}
	else
	{
		ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * (indx / MPS_NUMROW(matptr)) + \
			MPS_NUMCOL(matptr) * (indx % MPS_NUMROW(matptr));
	}
#endif

	return (mpfr_ptr) ptr;
}

mpfr_ptr mps_get_ele_row_blk(const mps_blk_ptr blkptr, unsigned int indx)
{
	char* ptr;
	const mps_ptr matptr = MPS_BLOCK_MAT(blkptr);
	const unsigned int num = MPS_BLOCK_NUMCOL(blkptr);

	MPS_ASSERT_MSG( MPS_BLOCK_INDX_BOUND(blkptr, indx),
						"Index out of submatrix bounds in mps_get_ele_row_blk()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_INDX_BOUND(blkptr, indx),
						"Real index out of real bounds in mps_get_ele_row_blk()\n");

	/* TODO: These indexing calculations might be able to be simplified */
	--indx;	/* indx - 1 is always what is used */
	indx = (indx/num + 1)*MPS_NUMCOL(matptr) + (indx % num) + MPS_BLOCK_MINCOL(blkptr) - 1;
	ptr = (char*) MPS_MPFR_ARRAY(matptr);

	if ( MPS_GET_ORDER(matptr) == MPS_COL_ORDER )
	{
#if 0
		--indx;
		indx = (indx/num + MPS_BLOCK_MINROW(blkptr) - 1) * MPS_NUMCOL(matptr) + (indx % num) + MPS_BLOCK_MINCOL(blkptr) - 1;
#endif
		ptr = ptr + sizeof(mpfr_t) * (indx / MPS_NUMCOL(matptr)) + \
			MPS_NUMROW(matptr) * (indx % MPS_NUMCOL(matptr));
	}
	else
	{
		ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * indx;
	}

	return (mpfr_ptr) ptr;
}

/* puts the new submat in submat; space must be preallocated in some way */
void mps_submat(mps_blk_ptr submat,
                       const mps_ptr mat,
                       const unsigned int lowrow,
                       const unsigned int sizerow,
                       const unsigned int lowcol,
                       const unsigned int sizecol)
{
	MPS_ASSERT_MSG( MPS_ROW_BOUND(mat, lowrow) && MPS_ROW_BOUND(mat, lowrow + sizerow - 1),
						"Row index out of bounds in mps_submat()\n");
	MPS_ASSERT_MSG( MPS_COL_BOUND(mat, lowcol) && MPS_COL_BOUND(mat, lowcol + sizecol - 1),
						"Row index out of bounds in mps_submat()\n");

	submat->_mat = mat;
	submat->_minrow = lowrow;
	submat->_mincol = lowcol;
	submat->_numrow = sizerow;
	submat->_numcol = sizecol;
}

/* wrap the entire matrix in a block matrix */
/* TODO: Rename to something better */
void mps_full_submat( mps_blk_ptr submat, const mps_ptr mat )
{
	submat->_mat = mat;
	submat->_minrow = 1;
	submat->_mincol = 1;
	submat->_numrow = MPS_NUMROW(mat);
	submat->_numcol = MPS_NUMCOL(mat);
}

/* should work for submat of submat for 2 levels and beyond */
void mps_subsubmat(mps_blk_ptr newblk,
                          const mps_blk_ptr submat,
                          const unsigned int lowrow,
                          const unsigned int sizerow,
                          const unsigned int lowcol,
                          const unsigned int sizecol)
{
	MPS_ASSERT_MSG( MPS_BLOCK_ROW_BOUND(submat, lowrow - 1)
					&& MPS_BLOCK_ROW_BOUND(submat, lowrow + sizerow - 1),
						"Row index out of submatrix bounds in mps_subsubmat()\n");

	MPS_ASSERT_MSG( MPS_BLOCK_COL_BOUND(submat, lowcol - 1)
					&& MPS_BLOCK_COL_BOUND(submat, lowcol + sizecol - 1),
						"Column index out of submatrix bounds in mps_subsubmat()\n");

	MPS_ASSERT_MSG( MPS_BLOCK_REAL_ROW_BOUND(submat, MPS_BLOCK_MINROW(submat) + lowrow - 1)
					&& MPS_BLOCK_REAL_ROW_BOUND(submat, MPS_BLOCK_MINROW(submat) + lowrow - 2 + sizerow),
						"Real row index out of submatrix bounds in mps_subsubmat()\n");

	MPS_ASSERT_MSG( MPS_BLOCK_REAL_COL_BOUND(submat, MPS_BLOCK_MINCOL(submat) + lowcol - 1)
					&& MPS_BLOCK_REAL_COL_BOUND(submat, MPS_BLOCK_MINCOL(submat) + lowcol - 2 + sizecol),
						"Real column index out of submatrix bounds in mps_subsubmat()\n");

	newblk->_mat = MPS_BLOCK_MAT(submat);
	newblk->_minrow = MPS_BLOCK_MINROW(submat) + lowrow - 1;
	newblk->_mincol = MPS_BLOCK_MINCOL(submat) + lowcol - 1;
	newblk->_numrow = sizerow;
	newblk->_numcol = sizecol;
}

/* FIXME: I'm pretty sure all the submatrix assertions are wrong, doesn't account for min and stuff */
/* TODO: Rename this to something less horrible perhaps? */
/* This is meant to carve a smaller part by rows */
void mps_subsubrow(mps_blk_ptr newblk,
                          const mps_blk_ptr submat,
                          const unsigned int low,
                          const unsigned int size)
{
	MPS_DEBUG("STUFF: minrow=%u mincol=%u, low=%u size=%u\n", MPS_BLOCK_MINROW(submat), MPS_BLOCK_MINCOL(submat), low, size)
#if 0
	MPS_ASSERT_MSG( MPS_BLOCK_ROW_BOUND(submat, low),
						"Lower index out of submatrix bounds in mps_subsubmat_indx_to_indx()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_ROW_BOUND(submat, low + size - 1),
						"Upper index out of submatrix bounds in mps_subsubmat_indx_to_indx()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_ROW_BOUND(submat, MPS_BLOCK_MINROW(submat) + low - 1),
						"Low index out of real matrix bounds in mps_subsubmat_indx_to_indx()\n");
	MPS_ASSERT_MSG( MPS_BLOCK_REAL_ROW_BOUND(submat, MPS_BLOCK_MINROW(submat) + low + size - 2),
						"Low index out of real matrix bounds in mps_subsubmat_indx_to_indx()\n");
#endif

	newblk->_mat = MPS_BLOCK_MAT(submat);
	newblk->_minrow = MPS_BLOCK_MINROW(submat);
	newblk->_mincol = MPS_BLOCK_MINCOL(submat) + low - 1;
	newblk->_numrow = MPS_BLOCK_NUMROW(submat);
	newblk->_numcol = size;
}

/* similar idea to mps_subsubrow (maybe replace it?)
 * Cut out of a row or column within some bounds.
 * Basically get a subrow or a subcolumn depending on the dimensions of what you are given.
 * e.g. if you have a 4x1 block, you can slice a row from position 2 of size 2.
 * or you could have a 1x4 block, and the elements will be accessed appropriately in the column */
void mps_subsubslice(mps_blk_ptr newblk,
                            const mps_blk_ptr submat,
                            const unsigned int low,
                            const unsigned int size)
{
	MPS_ASSERT_MSG( MPS_BLOCK_IS_ROW(submat)|| MPS_BLOCK_IS_COL(submat),
					"Expected a row or a column in mps_subsubslice()\n");

	newblk->_mat = MPS_BLOCK_MAT(submat);
	if ( MPS_BLOCK_IS_COL(submat))
	{
		newblk->_minrow = MPS_BLOCK_MINROW(submat) + low - 1;
		newblk->_mincol = MPS_BLOCK_MINCOL(submat);
		newblk->_numcol = 1;
		newblk->_numrow = size;
	}
	else
	{
		newblk->_minrow = MPS_BLOCK_MINROW(submat);
		newblk->_mincol = MPS_BLOCK_MINCOL(submat) + low - 1;
		newblk->_numcol = size;
		newblk->_numrow = 1;
	}
}

/* get a block matrix of a row */
void mps_row_blk( mps_blk_ptr newblk, const mps_ptr mat, const unsigned int row )
{
	newblk->_mat = mat;
	newblk->_minrow = row;
	newblk->_mincol = 1;
	newblk->_numrow = 1;
	newblk->_numcol = MPS_NUMCOL(mat);
}

char* mps_printbuf_overview_blk( const mps_blk_ptr blkptr, const unsigned int signfdigits )
{
    mpfr_ptr mpfr_temp;
    unsigned int i, j, numrow, numcol;
	size_t str_size;
	char* list;
	char* mpfr_string;

	numrow = MPS_BLOCK_NUMROW(blkptr);
	numcol = MPS_BLOCK_NUMCOL(blkptr);

    str_size = (signfdigits + 2 + 20) * numrow * numcol;
    list = calloc( str_size, 1 );
    mpfr_string = malloc( signfdigits + 2 + 20 );

    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol; ++j )
        {
            mpfr_temp = mps_get_ele_blk( blkptr, i, j );
            mpfr_sprintf( mpfr_string, "%12.*Rg", signfdigits, mpfr_temp );
            strcat( list, mpfr_string );
        }
        strcat( list, "\n" );
    }

    free( mpfr_string );

    return list;
}

char* mps_print_seq_blk( const mps_blk_ptr blk, const unsigned int signfdigits )
{
	mpfr_ptr sub;
	unsigned int i;
	size_t str_size;
	char* buf;
	char* list;
	const unsigned int size = MPS_BLOCK_SIZE(blk);

    str_size = (signfdigits + 2 + 20) * size;
    list = calloc( str_size, 1 );
    buf = malloc( signfdigits + 2 + 20 );

	for ( i = 1; i <= size; ++i )
	{
		sub = mps_get_ele_seq_blk(blk, i);
		mpfr_sprintf(buf, "%12.*Rg\n", signfdigits, sub);
        strcat( list, buf);
	}

	free(buf);

	return list;
}

/* mostly for debugging access functions */
char* mps_print_col_blk( const mps_blk_ptr blk, const unsigned int signfdigits )
{
	mpfr_ptr sub;
	unsigned int i;
	size_t str_size;
	char* buf;
	char* list;
	const unsigned int size = MPS_BLOCK_SIZE(blk);

    str_size = (signfdigits + 2 + 20) * size;
    list = calloc( str_size, 1 );
    buf = malloc( signfdigits + 2 + 20 );

	for ( i = 1; i <= size; ++i )
	{
		sub = mps_get_ele_col_blk(blk, i);
		mpfr_sprintf(buf, "%12.*Rg\n", signfdigits, sub);
        strcat( list, buf);
	}

	free(buf);

	return list;
}

/* mostly for debugging access functions */
char* mps_print_row_blk( const mps_blk_ptr blk, const unsigned int signfdigits )
{
	mpfr_ptr sub;
	unsigned int i;
	size_t str_size;
	char* buf;
	char* list;
	const unsigned int size = MPS_BLOCK_SIZE(blk);

    str_size = (signfdigits + 2 + 20) * size;
    list = calloc( str_size, 1 );
    buf = malloc( signfdigits + 2 + 20 );

	for ( i = 1; i <= size; ++i )
	{
		sub = mps_get_ele_row_blk(blk, i);
		mpfr_sprintf(buf, "%12.*Rg\n", signfdigits, sub);
        strcat( list, buf);
	}

	free(buf);

	return list;
}

/* copy contents of 1 submatrix into another, overwriting it */
void mps_copy_blk( const mps_blk_ptr rop, const mps_blk_ptr op, const mpfr_rnd_t rnd)
{
	unsigned int i;
    mpfr_ptr subop, subrop;
    const unsigned int blksize = MPS_BLOCK_SIZE(op);

	MPS_ASSERT_MSG( MPS_BLOCK_SAME_SIZE(rop, op),
						"Expected block sizes for op and rop to match in mps_copy_blk()\n");

	for ( i = 1; i <= blksize; ++i )
	{
		subop = mps_get_ele_seq_blk(op, i);
		subrop = mps_get_ele_seq_blk(rop, i);
		mpfr_set(subrop, subop, rnd);
	}

}

/* TODO: Better names for these block functions */
/* copies the submatrix op into the real matrix op. Size must match. */
void mps_copy_real_blk( const mps_ptr rop, const mps_blk_ptr op, const mpfr_rnd_t rnd)
{
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);
	mpfr_ptr subop, subrop;

	MPS_ASSERT_MSG( MPS_NUMROW(rop) == MPS_BLOCK_NUMROW(op)
					&& MPS_NUMCOL(rop) == MPS_BLOCK_NUMCOL(op),
					"Destination matrix size does not match in mps_copy_real_blk()\n");

	for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_seq_blk(op, i);
		subrop = mps_get_ele_seq(rop, i);
		mpfr_set(subrop, subop, rnd);
	}

}

/* copies matrix op into the submatrix rop */
void mps_copy_blk_real( const mps_blk_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subop, subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	MPS_ASSERT_MSG( MPS_NUMROW(op) == MPS_BLOCK_NUMROW(rop)
					&& MPS_NUMCOL(op) == MPS_BLOCK_NUMCOL(rop),
					"Destination matrix size does not match in mps_copy_blk_real()\n");

	for ( i = 1; i <= matsize; ++i )
	{
		subrop = mps_get_ele_seq_blk(rop, i);
		subop = mps_get_ele_seq(op, i);
		mpfr_set(subrop, subop, rnd);
	}

}

/*  swap 2 submatrices */
void mps_swap_blk( const mps_blk_ptr op1, const mps_blk_ptr op2 )
{
	mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int blksize = MPS_BLOCK_SIZE(op1);

	MPS_ASSERT_MSG( MPS_BLOCK_SAME_SIZE(op1, op2),
						"Expected block sizes for op1 and op2 to match in mps_swap_blk()\n");

	for ( i = 1; i <= blksize; ++i )
	{
		subop1 = mps_get_ele_seq_blk(op1, i);
		subop2 = mps_get_ele_seq_blk(op2, i);
		mpfr_swap(subop1, subop2);
	}

}

void mps_col_exg_blk( const mps_blk_ptr blk, const unsigned int col1, const unsigned int col2 )
{
	mpfr_ptr subop1, subop2;
	unsigned int i;
	const unsigned int numrow = MPS_BLOCK_NUMROW(blk);

	MPS_ASSERT_MSG( MPS_BLOCK_COL_BOUND(blk, col1),
					"First column index out of bounds int mps_col_exg_blk()\n");

	MPS_ASSERT_MSG( MPS_BLOCK_COL_BOUND(blk, col1),
					"Second column index out of bounds int mps_col_exg_blk()\n");

	for ( i = 1; i <= numrow; ++i )
	{
		subop1 = mps_get_ele_blk(blk, i, col1);
		subop2 = mps_get_ele_blk(blk, i, col2);
		mpfr_swap(subop1, subop2);
	}

}

void mps_scale_blk( const mps_blk_ptr op, const mpfr_ptr alpha, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop;
	unsigned int i;
	const unsigned int size = MPS_BLOCK_SIZE(op);

	for ( i = 1; i <= size; ++i )
	{
		subop = mps_get_ele_seq_blk(op, i);
		mpfr_mul(subop, subop, alpha, rnd);
	}

}

/* print fields of a block. for debugging */
void mps_print_info_blk( const mps_blk_ptr blk, const char* name )
{
	mpfr_printf("Block %s\n", name);
	mpfr_printf("\tMat: %p\n", (void*) MPS_BLOCK_MAT(blk));
	mpfr_printf("\tMinrow: %u\n", MPS_BLOCK_MINROW(blk));
	mpfr_printf("\tMincol: %u\n", MPS_BLOCK_MINCOL(blk));
	mpfr_printf("\tSizerow: %u\n", MPS_BLOCK_NUMROW(blk));
	mpfr_printf("\tSizecol: %u\n", MPS_BLOCK_NUMCOL(blk));
}

void mps_print_overview_blk( const mps_blk_ptr blkptr, const unsigned int signfdigits )
{
    mpfr_ptr sub;
    unsigned int i, j;
    const unsigned int numrow = MPS_BLOCK_NUMROW(blkptr);
    const unsigned int numcol = MPS_BLOCK_NUMCOL(blkptr);

    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol; ++j )
        {
            sub = mps_get_ele_blk( blkptr, i, j );
            mpfr_printf( "%12.*Rg", signfdigits, sub );
        }
		mpfr_printf("\n");
    }

}

/* dot product */
void mps_dot_product_blk( const mpfr_ptr rop,
                          const mps_blk_ptr op1,
                          const mps_blk_ptr op2,
                          const mpfr_rnd_t rnd )
{
    mpfr_ptr subop1, subop2;
    mpfr_t utmp1;
    unsigned int i;
    const unsigned int blksize = MPS_BLOCK_SIZE(op1);

    mpfr_init2(utmp1, MPS_BLOCK_PREC(op1));

    MPS_ASSERT_MSG( MPS_BLOCK_SAME_SIZE(op1, op2) &&
                    (MPS_BLOCK_IS_ROW(op1) || MPS_BLOCK_IS_COL(op1)),
                    "Expected 2 row / col blocks of same size in mps_dot_product_blk()\n");

    for ( i = 1; i <= blksize; ++i )
    {
        subop1 = mps_get_ele_seq_blk(op1, i);
        subop2 = mps_get_ele_seq_blk(op2, i);
        mpfr_mul(utmp1, subop1, subop2, rnd);
        mpfr_add(rop, rop, utmp1, rnd);
    }

    mpfr_clear(utmp1);

    return;
}

