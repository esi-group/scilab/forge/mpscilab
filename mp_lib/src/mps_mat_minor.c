/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* gives the minor of the matrix with row m and column n removed */
int mps_mat_minor( mps_ptr rop, const mps_ptr op, const unsigned int m, const unsigned int n, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i, j, x, y;
	const unsigned int numrow = MPS_NUMROW(op), numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_NUMROW(rop) == (numrow - 1)
				&& MPS_NUMCOL(rop) == (numcol - 1),
				"Result operand wrong size in mps_mat_minor()\n");

    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol; ++j)
        {
			if ( i < n )
				x = i;
			else if (i > n)
				x = i - 1;
			else	/* i == n, skip this one */
				continue;

			if ( j < m )
				y = j;
			else if ( j > m)
				y = j - 1;
			else	/* j == m */
				continue;

			subrop = mps_get_ele(rop, x, y);
			subop = mps_get_ele(op, i, j);
			mpfr_set(subrop, subop, rnd);
        }
    }

    return 0;
}

