/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

int mps_diag_set( const mps_ptr rop, const mps_ptr op, const int k, const mpfr_rnd_t rnd )
{
    unsigned int i;
    mpfr_ptr subrop, subop;
    unsigned int dim = MPS_NUMROW(rop) <= MPS_NUMCOL(rop) ? MPS_NUMROW(rop) : MPS_NUMCOL(rop);
    unsigned int len = MPS_SIZE(op);

#ifdef WITH_ASSERT_CHECKS
    unsigned int rlen;
#endif

#ifdef WITH_ASSERT_CHECKS
    if( k >= 0 )
        rlen = MPS_NUMCOL(rop) - k > dim ? dim : MPS_NUMCOL(rop) - k;
    else
        rlen = MPS_NUMROW(rop) + k > dim ? dim : MPS_NUMROW(rop) + k;
#endif

    MPS_ASSERT_MSG( MPS_DIAG_BOUND(rop,k),
        "Diagonal index out of bound in mps_diag_set()\n" );

    MPS_ASSERT_MSG( MPS_IS_VECTOR(op),
        "Second argument is not a vector in mps_diag_set()\n" );

    MPS_ASSERT_MSG( (len == rlen),
        "Second argument is not of the correct length in mps_diag_set()\n" );

    for( i = 1; i <= len; i++ )
    {
        if( k >= 0 )
            subrop = mps_get_ele( rop, i, i+k );
        else
            subrop = mps_get_ele( rop, i-k, i );

        subop = mps_get_ele_seq( op, i );

        mpfr_set( subrop, subop, rnd );
    }

    return 0;
}

int mps_diag_set_double( const mps_ptr rop, const double op[], const int k, const mpfr_rnd_t rnd )
{
    unsigned int i;
    mpfr_ptr subrop;
    unsigned int dim = MPS_NUMROW(rop) <= MPS_NUMCOL(rop) ? MPS_NUMROW(rop) : MPS_NUMCOL(rop);
    unsigned int len;

    MPS_ASSERT_MSG( MPS_DIAG_BOUND(rop,k),
        "Diagonal index out of bound in mps_diag_set_double()\n" );

    if( k >= 0 )
        len = MPS_NUMCOL(rop) - k > dim ? dim : MPS_NUMCOL(rop) - k;
    else
        len = MPS_NUMROW(rop) + k > dim ? dim : MPS_NUMROW(rop) + k;

    for( i = 1; i <= len; i++ )
    {
        if( k >= 0 )
            subrop = mps_get_ele( rop, i, i+k );
        else
            subrop = mps_get_ele( rop, i-k, i );

        mpfr_set_d( subrop, op[i-1], rnd );
    }

    return 0;
}

int mps_diag_get( const mps_ptr rop, const mps_ptr op, const int k, const mpfr_rnd_t rnd )
{
    unsigned int i;
    mpfr_ptr subrop, subop;
    unsigned int len = MPS_SIZE(rop);
    unsigned int dim = MPS_NUMROW(op) <= MPS_NUMCOL(op) ? MPS_NUMROW(op) : MPS_NUMCOL(op);

#ifdef WITH_ASSERT_CHECKS
    unsigned int rlen;
#endif

#ifdef WITH_ASSERT_CHECKS
    if( k >= 0 )
        rlen = MPS_NUMCOL(op) - k > dim ? dim : MPS_NUMCOL(op) - k;
    else
        rlen = MPS_NUMROW(op) + k > dim ? dim : MPS_NUMROW(op) + k;
#endif

    MPS_ASSERT_MSG( MPS_DIAG_BOUND(op,k),
        "Diagonal index out of bound in mps_diag_get()\n" );

    MPS_ASSERT_MSG( MPS_IS_VECTOR(rop),
        "First argument is not a vector in mps_diag_get()\n" );

    MPS_ASSERT_MSG( (len == rlen),
        "First argument is not of the correct length in mps_diag_get()\n" );

    for( i = 1; i <= len; i++ )
    {
        if( k >= 0 )
            subop = mps_get_ele( op, i, i+k );
        else
            subop = mps_get_ele( op, i-k, i );

        subrop = mps_get_ele_seq( rop, i );

        mpfr_set( subrop, subop, rnd );
    }

    return 0;
}

int mps_diag_get_double( const mps_ptr rop, const double op[], const mps_order_t order, const unsigned int m, const unsigned int n, const int k, const mpfr_rnd_t rnd )
{
    unsigned int i, l;
    mpfr_ptr subrop;
    unsigned int dim = m <= n ? m : n;
    unsigned int len;

    MPS_ASSERT_MSG( ( (k == 0) || (k > 0 && (unsigned)k < n) || (k < 0 && (unsigned)-k < m) ),
        "Diagonal index out of bound in mps_diag_get_double()\n" );

    if( k >= 0 )
        len = n - k > dim ? dim : n - k;
    else
        len = m + k > dim ? dim : m + k;


    for( i = 1; i <= len; i++ )
    {
        subrop = mps_get_ele_seq( rop, i );

        if( k >= 0 )
        {
            if( order == MPS_COL_ORDER )
                l = (i-1) + m*(i+k-1);
            else
                l = (i+k-1) + n*(i-1);
        }
        else
        {
            if( order == MPS_COL_ORDER )
                l = (i-k-1) + m*(i-1);
            else
                l = (i-1) + n*(i-k-1);
        }

        mpfr_set_d( subrop, op[l], rnd );
    }

    return 0;
}
