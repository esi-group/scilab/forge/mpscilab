/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Set each element of rop to the error function on op  */
int mps_erf( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_erf()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_erf( subrop, subop, rnd );
    }

    return 0;
}

int mps_erf_double( const mps_ptr rop, const double op[], const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_erf( subrop, subrop, rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_erf( subrop, subrop, rnd );
        }
    }

    return 0;
}

/* Set each element of rop to the complementary error function on op  */
int mps_erfc( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_erfc()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_erfc( subrop, subop, rnd );
    }

    return 0;
}

int mps_erfc_double( const mps_ptr rop, const double op[], const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_erfc( subrop, subrop, rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_erfc( subrop, subrop, rnd );
        }
    }

    return 0;
}
