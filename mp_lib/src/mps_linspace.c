/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* Fills rop column by column with equally spaced value from start to end. */
int mps_linspace( const mps_ptr rop, const mps_ptr mpsstart, const mps_ptr mpsend )
{
    mpfr_ptr subrop, start, end, first;
    unsigned int i;
    unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsstart),
        "mpsstart not a scalar in mps_linspace()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsend),
        "mpsend not a scalar in mps_linspace()\n" );

    start = mps_get_ele( mpsstart, 1, 1 );
    end = mps_get_ele( mpsend, 1, 1 );

    first = mps_get_ele_col( rop, 1 );
    if( matsize > 2 )
    {
        mpfr_sub( first, end, start, GMP_RNDN );
        mpfr_div_ui( first, first, matsize - 1, GMP_RNDN );
    }

    for ( i = 2; i < matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        mpfr_mul_ui( subrop, first, i-1, GMP_RNDN );
        mpfr_add( subrop, subrop, start, GMP_RNDN );
    }

    /* Set the last element to end. */
    subrop = mps_get_ele_seq( rop, matsize );
    mpfr_set( subrop, end, GMP_RNDN );

    /* Set the first element to start. */
    if( matsize > 1 )
    {
        mpfr_set( first, start, GMP_RNDN );
    }

    return 0;
}


/* Fills rop column by column with equally spaced value from start to end. */
int mps_linspace_double( const mps_ptr rop, double start, double end )
{
    mpfr_ptr subrop, first;
    unsigned int i;
    unsigned int matsize = MPS_SIZE(rop);

    first = mps_get_ele_col( rop, 1 );
    if( matsize > 2 )
    {
        mpfr_set_d( first, end, GMP_RNDN );
        mpfr_sub_d( first, first, start, GMP_RNDN );
        mpfr_div_ui( first, first, matsize - 1, GMP_RNDN );
    }

    for ( i = 2; i < matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        mpfr_mul_ui( subrop, first, i-1, GMP_RNDN );
        mpfr_add_d( subrop, subrop, start, GMP_RNDN );
    }

    /* Set the last element to end. */
    subrop = mps_get_ele_seq( rop, matsize );
    mpfr_set_d( subrop, end, GMP_RNDN );

    /* Set the first element to start. */
    if( matsize > 1 )
    {
        mpfr_set_d( first, start, GMP_RNDN );
    }

    return 0;
}

