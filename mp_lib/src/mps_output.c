/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * Set the content of rop from the values in op.  The size of the double matrix
 * is assumed to be the same as the op matrix. The third argument specify in what
 * order the matrix of double should be read.
 */
int mps_double_output( double* rop, const mps_ptr op, const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

	if ( order == MPS_COL_ORDER )
	{
		for ( i = 1; i <= matsize; ++i )
		{
			subop = mps_get_ele_col( op, i );
			rop[i - 1] = mpfr_get_d( subop, rnd );
		}
	}
	else
	{
		for ( i = 1; i <= matsize; ++i )
		{
			subop = mps_get_ele_row( op, i );
			rop[i - 1] = mpfr_get_d( subop, rnd );
		}
	}

    return 0;
}

