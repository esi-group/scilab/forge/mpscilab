/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* this function requires an absurd amount of temporary variables */
#if 0
tmp4, tmp5, tmp6, tmp7, tmp8, tmp9;
static mpfr_t mpfr_zero;
#endif

/* replacement for cblas_dnrm2 */
#define scale tmp1
#define ssq tmp2
#define absx tmp3
/* CHECKME: They had incX stuff but don't think I care. */
/* TODO: Move this into another file */
void mps_nrm2_blk(mpfr_ptr rop, const mps_blk_ptr X, mpfr_rnd_t rnd)
{
	mpfr_t tmp1, tmp2, tmp3;
	mpfr_ptr subX;
	unsigned int i;
	const unsigned int N = MPS_BLOCK_SIZE(X);

	mpfr_printf("In mps_nrm2_blk()\n");

	if ( N <= 0 )
	{
		mpfr_printf("N <= 0, return 0\n");
		mpfr_set_ui(rop, 0, rnd);
		return;
	}
	else if ( N == 1 )
	{
		subX = mps_get_ele_seq_blk(X, 1);
		mpfr_abs(rop, subX, rnd);
		mpfr_printf("N == 1, return |%6.RNg| = %6.RNg\n", subX, rop);
		return;
	}

	mpfr_inits2(MPS_BLOCK_PREC(X), tmp1, tmp2, tmp3, (mpfr_ptr) NULL);

	mpfr_set_ui(scale, 0, rnd);
	mpfr_set_ui(ssq, 1, rnd);

	for ( i = 1; i <= N; ++i )
	{
		subX = mps_get_ele_seq_blk(X, i);
		MPS_DEBUG("subX: %2.RNg\n", subX);
		if ( mpfr_sgn(subX) )						/* x != 0 */
		{
			mpfr_abs(absx, subX, rnd);
			MPS_DEBUG("ax = %6.RNg\n", absx)
			if ( mpfr_less_p(scale, absx) )			/* use rop as tmp space */
			{
				mpfr_div(rop, scale, absx, rnd);	/* ssq = 1 + ssq * (scale/absx)^2 */
				mpfr_sqr(rop, rop, rnd);
				mpfr_mul(rop, rop, ssq, rnd);
				mpfr_add_ui(ssq, rop, 1, rnd);

				MPS_DEBUG("Set A ssq = %6.RNg\n", ssq)

				mpfr_set(scale, absx, rnd);
			}
			else
			{
				mpfr_div(absx, absx, scale, rnd);	/* ssq += (absx/scale)^2 */
				mpfr_sqr(absx, absx, rnd);
				mpfr_add(ssq, ssq, absx, rnd);
				MPS_DEBUG("Set B ssq = %6.RNg\n", ssq)
			}
		}
	}

	MPS_DEBUG("nrm2 loop end, scale = %6.RNg, ssq = %6.RNg\n", scale, ssq);

	mpfr_sqrt(rop, ssq, rnd);			/* scale * sqrt(ssq) */
	mpfr_mul(rop, rop, scale, rnd);

	mpfr_clears(tmp1, tmp2, tmp3, (mpfr_ptr) NULL);
}
#undef scale
#undef ssq
#undef absx

/* following algorithm from GNU Scientific Library */

/* Factorise a general M x N matrix A into,
 *
 *   A = U D V^T
 *
 * where U is a column-orthogonal M x N matrix (U^T U = I),
 * D is a diagonal N x N matrix,
 * and V is an N x N orthogonal matrix (V^T V = V V^T = I)
 *
 * U is stored in the original matrix A, which has the same size
 *
 * V is stored as a separate matrix (not V^T). You must take the
 * transpose to form the product above.
 *
 * The diagonal matrix D is stored in the vector S,  D_ii = S_i
 */


#define norm tmp1
#define utmp1 tmp2
int mps_SV_decomp( const mps_ptr A,
                   const mps_ptr V,
                   const mps_vec_ptr S,
                   const mps_vec_ptr work,
                   const mpfr_rnd_t rnd)
{
	mpfr_t tmp1, tmp2;
	unsigned int a, b, i, j, iter;
	mps_vec_t view;
    mpfr_ptr Sj;
    mpfr_ptr S_max;
    mpfr_ptr fbm1;
    mpfr_ptr fam1;
    mps_vec_t f;
    unsigned int i_max;
    mps_blk_t U_block, V_block;
    mps_vec_t S_block, f_block;
    unsigned int n_block;

	const unsigned int M = MPS_NUMROW(A);
	const unsigned int N = MPS_NUMROW(A);
	const unsigned int K = M < N ? M : N;	/* min of these */

	MPS_ASSERT_MSG(MPS_VEC_VALID(S),
					"Expected vector as 3rd argument in mps_SVD_decomp()\n");
	MPS_ASSERT_MSG(MPS_VEC_VALID(work),
					"Expected vector as 4th argument in mps_SVD_decomp()\n");
	MPS_ASSERT_MSG( M >= N,
					"SVD of MxN, M < N not implemented in mps_SVD_decomp()\n");
	MPS_ASSERT_MSG( MPS_NUMROW(V) == N,
					"Expected size of V to match numrow A in mps_SV_decomp()\n");
	MPS_ASSERT_MSG(MPS_IS_SQUARE(V),
					"Expected V to be square matrix in mps_SV_decomp()\n");
	MPS_ASSERT_MSG(MPS_VEC_SIZE(S) == N,
					"Expected size of vector S to match matrix A sizerow in mps_SV_decomp()\n");
	MPS_ASSERT_MSG(MPS_VEC_SIZE(work) == N,
					"Expected length of workspace to match matrix A sizerow in mps_SV_decomp()\n");

	mpfr_inits2(MPS_PREC(A), tmp1, tmp2, (mpfr_ptr) NULL);

	/* handle the case of N = 1 (SVD of a column vector) */
	if ( N == 1 )
	{
		mps_subvec(view, A , 1);
		mps_nrm2_blk(norm, view, rnd);
		mps_set_ele_vec(S, 1, norm, rnd);
		mpfr_set_ui(utmp1, 1, rnd);		/* TODO: Constant 1? or set mat double/ui? */
		mps_set_ele(V, 1, 1, utmp1, rnd);

		if ( !mpfr_zero_p(norm) )
		{
			mpfr_ui_div(utmp1, 1, norm, rnd);	/* scale by 1/norm */
			mps_scale_vec(view, utmp1, rnd);
		}

		return 0;	/* TODO Return success? or not care. */
	}

	{
		/* TODO/CHECKME: K - 1? */
		mps_subsubvec(f, work, 1, K);

		/* bidiagonalize matrix A, unpack A into U S V */

		/* TODO: functions that don't exist yet */

		/* apply reduction steps to B=(S,Sd) */

		chop_small_elements(S, f, rnd);

		/* Progressively reduce the matrix until it is diagonal */

		b = N;		/* TODO/CHECKME: N - 1?? */
		iter = 0;

		while ( b > 1 )
		{
			fbm1 = mps_get_ele_vec(f, b - 1);

			if ( mpfr_zero_p(fbm1) || mpfr_nan_p(fbm1) )
			{
				--b;
				continue;
			}

			/* find the largest unreduced block (a, b)
			 * starting from b and working backwards */

			a = b;		/* b - 1???*/

			while ( a > 1 )
			{
				fam1 = mps_get_ele_vec( f, a - 1);
				if ( mpfr_zero_p(fam1) ||  mpfr_nan_p(fam1) )
					break;
				--a;
			}

			++iter;

			if ( iter > 100 * N )
			{
				MPS_WARNING_MSG("SVD decomposition failed to converge\n");
			}

			{
				n_block = b - a + 1;		/* TODO: a and b = ?? */
				mps_subsubvec(S_block, S, a, n_block);
				mps_subsubvec(f_block, f, a, n_block - 1);

				mps_submat(U_block, A, 1, a, MPS_NUMROW(A), n_block);
				mps_submat(V_block, V, 1, 1, MPS_NUMROW(V), n_block);

				qrstep(S_block, f_block, U_block, V_block, rnd);

				/* remove any small off-diagonal elements */
				chop_small_elements(S_block, f_block, rnd);
			}
		}
	}

	/* Make singular values positive by reflections if necessary */
	for ( j = 1; j <= K; ++j )
	{
		Sj = mps_get_ele_vec(S, j);

		if ( mpfr_cmp_si(Sj, 0) < 0 )
		{
			for ( i = 1; i <= N; ++i )
			{
				mpfr_ptr Vij = mps_get_ele(V, i, j);
				mpfr_neg(Vij, Vij, rnd);
			}
			mpfr_neg(Sj, Sj, rnd);
		}

	}

	/* sort singular values into decreasing order */

	for ( i = 1; i <= K; ++i )
	{
		S_max = mps_get_ele_vec(S, i);
		i_max = i;

		for ( j = i + 1; j <= K; ++j )
		{
			mpfr_ptr Sj = mps_get_ele_vec(S, j);

			if ( mpfr_greater_p(Sj, S_max) )
			{
				S_max = Sj;
				i_max = j;
			}
		}

		if ( i_max != i )
		{
			/* swap eigenvalues */
			mps_indx_exg_vec(S, i, i_max);

			/* swap eigenvectors */
			mps_col_exg(A, i, i_max);
			mps_col_exg(V, i, i_max);
		}
	}

	mpfr_clears(tmp1, tmp2, (mpfr_ptr) NULL);

	/* return success; */
	return 0;

}
#undef norm
#undef utmp1

/* TODO: the one better for M >> N */

