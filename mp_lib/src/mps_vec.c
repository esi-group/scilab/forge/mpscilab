/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* TODO: Convert these functions to subblock vectors only? */

/* gives the length of the vector op */
int mps_vec_length( const mpfr_ptr length, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
	mpfr_t tmp;
    unsigned int i;
    const unsigned int matsize = MPS_NUMCOL(op);

	MPS_ASSERT_MSG(MPS_IS_COL(op), "Expected vector in mps_vec_length()\n");

	mpfr_init2(tmp, MPS_PREC(op));
	mpfr_set_ui(length, 0, rnd);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col(op, i);
		mpfr_sqr(tmp, subop, rnd);
		mpfr_add(length, length, tmp, rnd);
    }

	mpfr_sqrt(length, length, rnd);
	mpfr_clear(tmp);

    return 0;
}

/* calculates the dot product for 2 vectors.
Technically could do matrix multiply of transpose of 1, but should be faster. */
int mps_dot_product( const mpfr_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop1, subop2;
	mpfr_t tmp;
	unsigned int i;
    const unsigned int matsize = MPS_NUMCOL(op1);

	MPS_ASSERT_MSG(MPS_IS_COL(op1) && MPS_SAME_SIZE(op1, op2),
					"Expected 2 vectors of equal length in mps_dot_product()\n");

	mpfr_init2(tmp, MPS_PREC(op1));
	mpfr_set_ui(rop, 0, rnd);

	for ( i = 1; i <= matsize; ++i )
	{
		subop1 = mps_get_ele_col(op1, i);
		subop2 = mps_get_ele_col(op2, i);
		mpfr_mul(tmp, subop1, subop2, rnd);
		mpfr_add(rop, rop, tmp, rnd);
	}

	mpfr_clear(tmp);
	return 0;
}

/* gives the unit vector of op in rop */
int mps_unit_vector( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
	mpfr_ptr subop, subrop;
	mpfr_t tmp, length;
	unsigned int i;
    const unsigned int matsize = MPS_NUMCOL(op);

	MPS_ASSERT_MSG(MPS_IS_COL(op) && MPS_SAME_SIZE(rop, op),
					"Expected 2 vectors of equal length in mps_unit_vector()\n");

	mpfr_inits2(MPS_PREC(op), tmp, length, (mpfr_ptr) NULL);
	mpfr_set_ui(tmp, 0, rnd);

	for ( i = 1; i <= matsize; ++i )		/* calculate the length of the vector */
	{
        subop = mps_get_ele_col(op, i);
		mpfr_sqr(tmp, subop, rnd);
		mpfr_add(length, length, tmp, rnd);
	}

	mpfr_sqrt(length, length, rnd);

	for ( i = 1; i <= matsize; ++i )
	{
        subop = mps_get_ele_col(op, i);
        subrop = mps_get_ele_col(rop, i);
		mpfr_div(subrop, subop, length, rnd);
	}

	mpfr_clear(tmp);
	mpfr_clear(length);

	return 0;

}

/* functions to treat a vector as a sub-block.
 * All the subblock functions should still work with vectors,
 * however the vector versions make assumptions such as only a single
 * column and column ordered which might be beneficial in some cases */
/* FIXME: Doesn't work when dealing with subvectors of a vector */
mpfr_ptr mps_get_ele_vec( const mps_vec_ptr vecptr, const unsigned int indx )
{
	char* ptr;
	mps_ptr matptr;

	MPS_ASSERT_MSG( MPS_VEC_VALID(vecptr),
					"Expected valid vector in mps_get_ele_vec()\n");

	matptr = MPS_VEC_MAT(vecptr);
	ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * (indx + MPS_VEC_MIN(vecptr) - 2);

	return (mpfr_ptr) ptr;
}

/* sets vector element at index to op */
void mps_set_ele_vec( const mps_vec_ptr vecptr,
                      const unsigned int indx,
                      const mpfr_ptr op,
                      const mpfr_rnd_t rnd )
{
	char* ptr;
	mps_ptr matptr;

	MPS_ASSERT_MSG( MPS_VEC_VALID(vecptr),
					"Expected valid vector in mps_set_ele_vec()\n");

	matptr = MPS_VEC_MAT(vecptr);
	ptr = (char*) MPS_MPFR_ARRAY(matptr) + sizeof(mpfr_t) * (indx + MPS_VEC_MIN(vecptr) - 2);

	mpfr_set((mpfr_ptr) ptr, op, rnd);
}

/* gets a new subvector from matrix, column col put into subvec */
void mps_subvec( mps_vec_ptr subvec, const mps_ptr mat, const unsigned int col )
{
	subvec->_mat = mat;
	subvec->_minrow = 1;
	subvec->_mincol = col;
	subvec->_numrow = MPS_NUMROW(mat);
	subvec->_numcol = 1;
}

/* return new subvector of a single column from low to high */
void mps_subvec_partial( mps_vec_ptr subvec,
                         const mps_ptr mat,
                         const unsigned int colnum,
                         const unsigned int low,
                         const unsigned int high)
{
	subvec->_mat = mat;
	subvec->_minrow = low;
	subvec->_mincol = colnum;
	subvec->_numrow = high - 1;
	subvec->_numcol = 1;
}

/* returns subvec in subvec, space must be preallocated */
void mps_subsubvec(mps_vec_ptr subvec,
                   const mps_vec_ptr vec,
                   const unsigned int low,
                   const unsigned int size)
{
	/* TODO: Could use more bound checking assertions for length/upper bound */
	MPS_ASSERT_MSG( MPS_VEC_VALID(subvec),
						"Expected valid vector as 1st argument in mps_subsubvec()\n");
	MPS_ASSERT_MSG( MPS_VEC_VALID(vec),
						"Expected valid vector as 2nd argument in mps_subsubvec()\n");
	MPS_ASSERT_MSG( MPS_VEC_BOUND(vec, MPS_VEC_MINROW(vec) + low - 1),
						"Low bound out of range in mps_subsubvec()\n");

	subvec->_mat = MPS_VEC_MAT(vec);
	subvec->_minrow = MPS_VEC_MINROW(vec) + low - 1;
	subvec->_mincol = MPS_VEC_MINCOL(vec);
	subvec->_numrow = size;
	subvec->_numcol = 1;
}

/* produce a totally new vector. this creates a new matrix as well.
 * Not sure this is the best idea. */
mps_vec_ptr mps_new_vec( const unsigned int length, const mpfr_prec_t prec )
{
	mps_vec_ptr vec = malloc(sizeof(mps_vec_t));
	mps_ptr mat = malloc(sizeof(mps_t));
	mps_init(mat, length, 1, prec, MPS_COL_ORDER);
	vec->_mat = mat;
	vec->_minrow = 1;
	vec->_mincol = 1;
	vec->_numrow = length;
	vec->_numcol = 1;

	return vec;
}

void mps_indx_exg_vec(const mps_vec_ptr vec, const unsigned int indx1, const unsigned int indx2)
{
	MPS_ASSERT_MSG( MPS_VEC_VALID(vec),
					"Expected vector in mps_vec_indx_swap()\n");

	MPS_ASSERT_MSG( MPS_VEC_BOUND(vec, indx1),
					"1st index out of bounds in mps_vec_indx_swap()\n");
	MPS_ASSERT_MSG( MPS_VEC_BOUND(vec, indx2),
					"2nd index out of bounds in mps_vec_indx_swap()\n");

	mpfr_swap(mps_get_ele_vec(vec, indx1), mps_get_ele_vec(vec, indx2));
}

/* free independent vector created with mps_new_vec() */
void mps_destroy_vec( mps_vec_ptr vec )
{
	mps_ptr mat = MPS_VEC_MAT(vec);
	mps_free(mat);
	free(mat);
	free(vec);
}

/* copy vector op into rop */
void mps_copy_vec( const mps_vec_ptr rop, const mps_vec_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i;
    const unsigned int size = MPS_VEC_SIZE(op);

	MPS_ASSERT_MSG( MPS_VEC_VALID(rop) && MPS_VEC_VALID(op),
					"Expected valid vectors in mps_copy_vec()\n");
	MPS_ASSERT_MSG( MPS_VEC_SAME_SIZE(rop, op),
					"Expected vectors of equal size in mps_copy_vec()\n");

	for ( i = 1; i <= size; ++i )
	{
		subrop = mps_get_ele_vec(rop, i);
		subop = mps_get_ele_vec(op, i);
		mpfr_set(subrop, subop, rnd);
	}

}

/* copy block op sequentially into vector rop */
void mps_copy_vec_from_blk( const mps_vec_ptr rop, const mps_blk_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i;
    const unsigned int size = MPS_BLOCK_SIZE(op);

	MPS_ASSERT_MSG( MPS_VEC_VALID(rop),
					"Expected valid vector in mps_copy_vec_from_blk()\n");
	MPS_ASSERT_MSG( MPS_VEC_SIZE(rop) == MPS_BLOCK_SIZE(op),
					"Expected vector size to match block size in mps_copy_vec_from_blk()\n");

	for ( i = 1; i <= size; ++i )
	{
		subrop = mps_get_ele_vec(rop, i);
		subop = mps_get_ele_seq_blk(op, i);
		mpfr_set(subrop, subop, rnd);
	}

}

/* creates a newly allocated vector clone which owns a new matrix
 * Should be equivalent to combining mps_vec_new and mps_copy_vec */
mps_vec_ptr mps_vec_new_clone( const mps_vec_ptr op, const mpfr_rnd_t rnd )
{
	mps_vec_ptr ret;
	mpfr_ptr subop, subret;
	unsigned int i;
    const unsigned int size = MPS_VEC_SIZE(op);

	MPS_ASSERT_MSG( MPS_VEC_VALID(op),
					"Expected valid vector in mps_vec_new_clone()\n");

	ret = mps_new_vec( size, MPS_PREC(MPS_VEC_MAT(op)) );
	for ( i = 1; i <= size; ++i )
	{
		subret = mps_get_ele_vec(ret, i);
		subop = mps_get_ele_vec(op, i);
		mpfr_set(subret, subop, rnd);
	}

	return ret;
}

/* scales each element of op by factor alpha */
void mps_scale_vec( const mps_vec_ptr op, const mpfr_ptr alpha, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop;
	unsigned int i;
    const unsigned int size = MPS_VEC_SIZE(op);

	MPS_ASSERT_MSG( MPS_VEC_VALID(op),
					"Expected valid vector in mps_scale_vec()\n");

	for ( i = 1; i <= size; ++i )
	{
		subop = mps_get_ele_vec(op, i);
		mpfr_mul(subop, subop, alpha, rnd);
	}

}

void mps_fill_seq_si_vec( const mps_vec_ptr op,
                          const int start,
                          const int step,
                          const mpfr_rnd_t rnd )
{
	mpfr_ptr subop;
	unsigned int i;
	const unsigned int size = MPS_VEC_SIZE(op);

	MPS_ASSERT_MSG( MPS_VEC_VALID(op),
					"Expected valid vector in mps_fill_seq_si_vec()\n");

	for ( i = 1; i <= size; ++i )
	{
		subop = mps_get_ele_vec(op, i);
		mpfr_set_si(subop, start + ((int) i - 1) * step, rnd);
	}

}

