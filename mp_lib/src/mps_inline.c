/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/*
 * External definition of inlined function as per C99 inlining standard.
 */

/*
 * Define __REMOVE_INLINE_DEF__ to prevent inclusion of inlined definition
 * for this file.
 */
#define __REMOVE_INLINE_DEF__

#include <string.h>

#include "mps_priv.h"
#include "mps_assert.h"

#undef __REMOVE_INLINE_DEF__

/*
 * Return a pointer to the mpfr struct located at a given matrix index.
 * Access is done sequentially, following linear memory ordering.
 * Note that the index value starts at 1.
 *
 * Linear access mean the value will be returned column by column if
 * MPS_GET_ORDER(x) is FALSE or row by row if TRUE. This will give the fastest
 * traversal of the matrix values but will give different results depending on
 * the ordering. mps_get_ele_row and mps_get_ele_col may be used if this
 * is not the required behavior.
 */
mpfr_ptr mps_get_ele_seq( const mps_ptr mpsptr, unsigned int indx )
{
    unsigned char *ptr;

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_get_ele_seq()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_get_ele_seq()\n" );

    ptr = (unsigned char*)MPS_MPFR_ARRAY(mpsptr);
    ptr = ptr + sizeof(mpfr_t) * (--indx) ;
    return (mpfr_ptr) ptr;
}

void mps_set_ele_seq( const mps_ptr mpsptr,
                      unsigned int indx,
                      const mpfr_ptr op,
                      const mpfr_rnd_t rnd )
{
    unsigned char* ptr;

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_set_ele_seq()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_set_ele_seq()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);
    ptr = ptr + sizeof(mpfr_t) * (indx-1) ;
	mpfr_set( (mpfr_ptr) ptr, op, rnd);
}

/*
 * Return the mpfr struct located at a given matrix coordinate. Accessed row-wise.
 * Note that index values start at 1.
 */
mpfr_ptr mps_get_ele( const mps_ptr mpsptr, unsigned int row, unsigned int col )
{
    unsigned char *ptr;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_get_ele()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_get_ele()\n" );

    MPS_ASSERT_MSG( row != 0,
        "Trying to access row 0 in mps_get_ele()\n" );

    MPS_ASSERT_MSG( col != 0,
        "Trying to access col 0 in mps_get_ele()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if( MPS_GET_ORDER(mpsptr) ==  MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * ( (row-1) + MPS_NUMROW(mpsptr)*(col-1) );
    else
        ptr = ptr + sizeof(mpfr_t) * ( (col-1) + MPS_NUMCOL(mpsptr)*(row-1) );

    return (mpfr_ptr) ptr;
}

/* set the element at given position to op */
void mps_set_ele( const mps_ptr mpsptr,
                  unsigned int row,
                  unsigned int col,
                  const mpfr_ptr op,
                  const mpfr_rnd_t rnd )
{
    unsigned char* ptr;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_set_ele()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_set_ele()\n" );

    MPS_ASSERT_MSG( row != 0,
        "Trying to access row 0 in mps_set_ele()\n" );

    MPS_ASSERT_MSG( col != 0,
        "Trying to access col 0 in mps_set_ele()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if ( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * ( (row-1) + MPS_NUMROW(mpsptr)*(col-1) );
    else
        ptr = ptr + sizeof(mpfr_t) * ( (col-1) + MPS_NUMCOL(mpsptr)*(row-1) );

	mpfr_set((mpfr_ptr) ptr, op, rnd);
}

/* set the element at given position to (double)op */
void mps_set_ele_double( const mps_ptr mpsptr, unsigned int row, unsigned int col, const double op, const mpfr_rnd_t rnd )
{
    unsigned char* ptr;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_set_ele()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_set_ele()\n" );

    MPS_ASSERT_MSG( row != 0,
        "Trying to access row 0 in mps_set_ele()\n" );

    MPS_ASSERT_MSG( col != 0,
        "Trying to access col 0 in mps_set_ele()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * ( (row-1) + MPS_NUMROW(mpsptr)*(col-1) );
    else
        ptr = ptr + sizeof(mpfr_t) * ( (col-1) + MPS_NUMCOL(mpsptr)*(row-1) );

	mpfr_set_d((mpfr_ptr) ptr, op, rnd);
}

/* Set the element at the given position to 0. */
void mps_set_ele_zero( const mps_ptr mpsptr, unsigned int row, unsigned int col )
{
    unsigned char* ptr;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(mpsptr,row),
        "Row index out of bound in mps_set_ele_zero()\n" );

    MPS_ASSERT_MSG( MPS_COL_BOUND(mpsptr,col),
        "Column index out of bound in mps_set_ele_zero()\n" );

    MPS_ASSERT_MSG( row != 0,
        "Trying to access row 0 in mps_set_ele_zero()\n" );

    MPS_ASSERT_MSG( col != 0,
        "Trying to access col 0 in mps_set_ele_zero()\n" );   

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * ( (row-1) + MPS_NUMROW(mpsptr)*(col-1) );
    else
        ptr = ptr + sizeof(mpfr_t) * ( (col-1) + MPS_NUMCOL(mpsptr)*(row-1) );

	mpfr_set_zero( (mpfr_ptr)ptr, 1 ); 
}


/*
 * Return the mpfr struct located at a given matrix index sequentially row-wise
 * Note that the index value starts at 1.
 */
mpfr_ptr mps_get_ele_row( const mps_ptr mpsptr, unsigned int indx )
{
    unsigned char *ptr;
    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_get_ele_row()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_get_ele_row()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if ( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
	{
        ptr = ptr + sizeof(mpfr_t)  * ( ((indx-1) / MPS_NUMCOL(mpsptr)) + \
				MPS_NUMROW(mpsptr)*((indx-1) % MPS_NUMCOL(mpsptr)) );
	}
    else
        ptr = ptr + sizeof(mpfr_t) * (indx-1);

    return (mpfr_ptr) ptr;
}

void mps_set_ele_row( const mps_ptr mpsptr,
                      unsigned int indx,
                      const mpfr_ptr op,
                      const mpfr_rnd_t rnd )
{
    unsigned char* ptr;
    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_set_ele_row()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_set_ele_row()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if ( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
	{
        ptr = ptr + sizeof(mpfr_t)  * ( ((indx-1) / MPS_NUMCOL(mpsptr)) +
          MPS_NUMROW(mpsptr)*((indx-1) % MPS_NUMCOL(mpsptr)) );
	}
    else
        ptr = ptr + sizeof(mpfr_t) * (indx-1) ;

	mpfr_set( (mpfr_ptr) ptr, op, rnd);
}

/*
 * Return the mpfr struct located at a given matrix index sequentially column-wise
 * Note that the index value starts at 1.
 */
mpfr_ptr mps_get_ele_col( const mps_ptr mpsptr, unsigned int indx )
{
    unsigned char *ptr;

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_get_ele_col()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_get_ele_col()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if ( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * (indx-1) ;
    else
	{
        ptr = ptr + sizeof(mpfr_t)  * ( ((indx-1) / MPS_NUMROW(mpsptr)) + \
				MPS_NUMCOL(mpsptr) * ((indx-1) % MPS_NUMROW(mpsptr)) );
	}

    return (mpfr_ptr) ptr;
}

void mps_set_ele_col( const mps_ptr mpsptr,
                      unsigned int indx,
                      const mpfr_ptr op,
                      const mpfr_rnd_t rnd )
{
    unsigned char* ptr;

    MPS_ASSERT_MSG( MPS_INDX_BOUND(mpsptr, indx),
        "Index out of bound in mps_set_ele_col()\n" );

    MPS_ASSERT_MSG( indx != 0,
        "Trying to access element 0 in mps_set_ele_col()\n" );

    ptr = (unsigned char*) MPS_MPFR_ARRAY(mpsptr);

    if ( MPS_GET_ORDER(mpsptr) == MPS_COL_ORDER )
        ptr = ptr + sizeof(mpfr_t) * (indx-1) ;
    else
	{
        ptr = ptr + sizeof(mpfr_t)  * ( ((indx-1) / MPS_NUMROW(mpsptr)) +
        MPS_NUMCOL(mpsptr)*((indx-1) % MPS_NUMROW(mpsptr)) );
	}

	mpfr_set( (mpfr_ptr) ptr, op, rnd);
}


/* Memcopy an mpfr struct. */
int mps_cpy_mpfr_struct(mpfr_ptr rop, const mpfr_ptr op)
{
    memcpy( rop, op, sizeof(mpfr_t) );
    return 0;
}

/* TODO: Make these functions look more like they make sense with the order enum */

/*
 * Change the ordering of the given matrix.
 */
void mps_set_order( mps_ptr mpsptr, mps_order_t order )
{
    MPS_TYPE(mpsptr) = order;
    return;
}

/*
 * Toggle the order flag. Effectively transposing the matrix.
 */
void mps_flip_order( mps_ptr mpsptr )
{
    MPS_TYPE(mpsptr) ^= MPS_TYPE_ORDER;

    return;
}

/* Reimplementation of mpfr_swap for inlining and optimization. */
void mps_ele_fastswap( mpfr_ptr op1, mpfr_ptr op2 )
{
/*    mpfr_prec_t p1, p2; */
    mpfr_sign_t s1, s2;
    mpfr_exp_t e1, e2;
    mp_limb_t *m1, *m2;

#if 0
    p1 = op1->_mpfr_prec;
    p2 = op2->_mpfr_prec;
    op2->_mpfr_prec = p1;
    op1->_mpfr_prec = p2;
#endif

    s1 = op1->_mpfr_sign;
    s2 = op2->_mpfr_sign;
    op2->_mpfr_sign = s1;
    op1->_mpfr_sign = s2;

    e1 = op1->_mpfr_exp;
    e2 = op2->_mpfr_exp;
    op2->_mpfr_exp = e1;
    op1->_mpfr_exp = e2;

    m1 = op1->_mpfr_d;
    m2 = op2->_mpfr_d;
    op2->_mpfr_d = m1;
    op1->_mpfr_d = m2;
}
