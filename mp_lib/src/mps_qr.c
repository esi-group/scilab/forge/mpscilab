/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

void mps_qr_decomp( const mps_ptr A, const mps_vec_ptr tau, const mpfr_rnd_t rnd )
{
	unsigned int i;
	mps_vec_t c_full, c;
	mps_blk_t m;
	mpfr_t tau_i;
	const unsigned int M = MPS_NUMROW(A);
	const unsigned int N = MPS_NUMCOL(A);
	const unsigned int min = ( M < N ? M : N );

	MPS_ASSERT_MSG( min  == MPS_VEC_SIZE(tau),
		"Expected size of tau to be the smaller side of matrix in mps_qr_decomp()\n");

	mpfr_init2(tau_i, MPS_PREC(A));

	for ( i = 1; i <= min; ++i )
	{
		/* Compute the householder transformation to reduce the j-th
		 column of the matrix to a multiple of the j-th unit vector */
		mps_subvec(c_full, A, i);

		mps_subsubvec(c, c_full, i, M - i + 1);

		mps_householder_transform(tau_i, c, rnd);

		mps_set_ele_vec(tau, i, tau_i, rnd);

		/* Apply the transformation to the remaining columns and update the norms. */
		if ( i + 1 <= N )
		{
			mps_submat(m, A, i, M - i + 1, i + 1, N - i);
			mps_householder_hm(tau_i, c, m, rnd);
		}

	}

	mpfr_clear(tau_i);

	return;		/* success */
}

