/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

void mps_convolution( const mps_ptr conv,  const mps_ptr A, const mps_ptr B, const mpfr_rnd_t rnd )
{
	unsigned int m, k, pos;
	mpfr_ptr subA, subB, subConv;
	mpfr_t utmp1, mpfr_zero;
	const unsigned int NA = MPS_SIZE(A);
	const unsigned int NB = MPS_SIZE(B);
	const unsigned int convN = MPS_SIZE(conv);

	MPS_ASSERT_MSG( convN == NA + NB - 1,
					"Wrong result size in mps_convolution()\n");

	mpfr_init2(utmp1, MPS_PREC(A));
	mpfr_init2(mpfr_zero, MPFR_PREC_MIN);

	mpfr_set_ui(mpfr_zero, 0, rnd);

	for ( k = 1; k <= convN; ++k )
	{
		subConv = mps_get_ele_seq(conv, k);
		mpfr_set_ui(subConv, 0, rnd);
		for ( m = 1; m <= k; ++m )
		{
			if ( m > NA )		/* Maybe uses unsigned overflow? bad maybe? */
				subA = mpfr_zero;
			else
				subA = mps_get_ele_seq(A, m);

			pos = k - m + 1;
			if ( pos > NB )
				subB = mpfr_zero;
			else
				 subB = mps_get_ele_seq(B, pos);

			mpfr_mul(utmp1, subA, subB, rnd);
			mpfr_add(subConv, subConv, utmp1, rnd);
		}
	}

	mpfr_clear(utmp1);
	mpfr_clear(mpfr_zero);

	return;
}

#if 0
/* I haven't found this in scilab...
   Does this need the padding / be larger stuff too?
   For now assuming conv is the same size as A, which is the same size as B.
   And probably square. Overall this function is bad.
   Also better way is use FFT. This is crazy loops (yay O(n^4)!)
   It also probably could be done more intelligently without FFT */
void mps_convolution2d( const mps_ptr conv, const mps_ptr A, const mps_ptr B, const mpfr_rnd_t rnd )
{
	mpfr_t utmp1, mpfr_zero;
	mpfr_ptr subConv, subA, subB;
	unsigned int i, j, x, y, indx1, indx2;
	const unsigned int numcolA = MPS_NUMCOL(A);
	const unsigned int numcolB = MPS_NUMCOL(B);
	const unsigned int numrowA = MPS_NUMROW(A);
	const unsigned int numrowB = MPS_NUMROW(B);

	mpfr_init2(utmp1, MPS_PREC(A));
	mpfr_init2(mpfr_zero, MPFR_PREC_MIN);

	mpfr_set_ui(mpfr_zero, 0, rnd);

	for ( x = 1; x <= numrowA; ++x )
	{
		for ( y = 1; y <= numcolA; ++y )
		{
			subConv = mps_get_ele(conv, x, y);
			mpfr_set_ui(subConv, 0, rnd);
			for ( i = 1; i <= numrowB; ++i )
			{
				for ( j = 1; j <= numcolB; ++j )
				{
					subA = mps_get_ele(A, i, j);
					indx1 = x - i + 1;
					indx2 = y - j + 1;

					if ( indx1 == 0 || indx2 == 0 || indx1 > numrowB || indx2 > numcolB )
						subB = mpfr_zero;
					else
						subB = mps_get_ele(B, indx1, indx2);

					mpfr_mul(utmp1, subA, subB, rnd);
					mpfr_add(subConv, subConv, utmp1, rnd);
				}
			}
		}
	}

	mpfr_clear(utmp1);
	mpfr_clear(mpfr_zero);

	return;
}
#endif

void mps_correlation( const mps_ptr corr,
                      const mps_ptr A,
                      const mps_ptr B,
                      const unsigned int nlags,
                      const mpfr_rnd_t rnd )
{
	unsigned int m, k, limit;
	mpfr_ptr subA, subB, subCorr;
	mpfr_t utmp1, utmp2, sum, meanA, meanB;         /* TODO: Temporary variables */
	const unsigned int N = MPS_SIZE(A);

	MPS_ASSERT_MSG( MPS_SAME_SIZE(A,B), "Expected same size inputs in mps_correlation()\n");
    MPS_ASSERT_MSG( MPS_SIZE(corr) == nlags, "Wrong destination size in mps_correlation()\n");

	mpfr_inits2(MPS_PREC(A), utmp1, utmp2, sum, meanA, meanB, (mpfr_ptr) NULL);

//    mps_mean(meanA, A);
//    mps_mean(meanB, B);

    for ( m = 1; m <= nlags; ++m )
    {
        subCorr = mps_get_ele_seq(corr, m);
        mpfr_set_ui(sum, 0, rnd);

        limit = N - m + 1;
        for ( k = 1; k <= limit; ++k )
        {
            subA = mps_get_ele_seq(A, k);
            subB = mps_get_ele_seq(B, m + k - 1);

            mpfr_sub(utmp1, subA, meanA, rnd);
            mpfr_sub(utmp2, subB, meanB, rnd);
            mpfr_mul(utmp1, utmp1, utmp2, rnd);

            mpfr_div_ui(utmp1, utmp1, N, rnd);
            mpfr_add(sum, sum, utmp1, rnd);
        }
        mpfr_set(subCorr, sum, rnd);
    }

	mpfr_clears(utmp1, utmp2, sum, meanA, meanB, (mpfr_ptr) NULL);

	return;
}

