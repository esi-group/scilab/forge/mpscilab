/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

typedef enum _Retcodes	/* TODO: Do we want something like this? */
{
	MPS_SUCCESS,
	MPS_EMAXITER
} RetCodes;

static mpfr_t one;					/* function called several times needs constant 1 */
static mpfr_t tmp1, tmp2, tmp3;		/* other temp variables */

/* following algorithm used in GNU Scientific Library */

/* create local aliases for the global temporary variables.
 * This way keep better names without having to reinit them multiple times
 * when finding the eigenvalues for a single matrix. */
#define tau tmp1
#define t tmp2
/* out gets used as a temporary variable and is the final result */
static void mps_symschur2( const mpfr_ptr out,
                                  const mps_ptr A,
                                  unsigned int p,
                                  unsigned int q,
                                  const mpfr_ptr c,
                                  const mpfr_ptr s,
                                  const mpfr_rnd_t rnd )
{
	mpfr_ptr Apq, App, Aqq;

	Apq = mps_get_ele(A, p, q);

	if ( mpfr_sgn(Apq) )
	{
		App = mps_get_ele(A, p, p);
		Aqq = mps_get_ele(A, q, q);

		mpfr_sub(tau, Aqq, App, rnd);			/* tau = (Aqq - App) / (2.0 * Apq ) */
		mpfr_div(tau, tau, Apq, rnd);
		mpfr_div_ui(tau, tau, 2, rnd);

		if ( mpfr_sgn(tau) >= 0 )
		{
			mpfr_hypot(out, one, tau, rnd);		/* 1.0 / (tau + hypot(1.0, tau)) */
			mpfr_add(out, out, tau, rnd);
			mpfr_ui_div(t, 1, out, rnd);
		}
		else
		{
			mpfr_hypot(out, one, tau, rnd);		/* -1.0 / (hypot(1.0, tau) - tau) */
			mpfr_sub(out, out, tau, rnd);
			mpfr_si_div(t, -1, out, rnd);
		}

		mpfr_hypot(out, one, t, rnd);
		mpfr_ui_div(c, 1, out, rnd);

		mpfr_mul(s, t, c, rnd);
	}
	else
	{
		mpfr_set_ui(c, 1, rnd);
		mpfr_set_ui(s, 0, rnd);
	}

	mpfr_abs(out, Apq, rnd);

}
#undef tau
#undef t

#define ApjT tmp1
#define tmp tmp2
static void apply_jacobi_L( const mps_ptr A,
                                   const unsigned int p,
                                   const unsigned int q,
                                   const mpfr_ptr c,
                                   const mpfr_ptr s,
                                   const mpfr_rnd_t rnd)
{
	mpfr_ptr Apj, Aqj;
	unsigned int j;
	const unsigned int N = MPS_NUMCOL(A);	/* Is this right? */

	for ( j = 1; j <= N; ++j )
	{
		Apj = mps_get_ele(A, p, j);
		Aqj = mps_get_ele(A, q, j);
		mpfr_set(ApjT, Apj, rnd);		/* need to hold onto the value */

		mpfr_mul(Apj, Apj, c, rnd);		/* A(p,j) = Apj * c - Aqj * s */
		mpfr_mul(tmp, Aqj, s, rnd);
		mpfr_sub(Apj, Apj, tmp, rnd);

		mpfr_mul(tmp, Aqj, c, rnd);		/* A(q,j) = Apj * s + Aqj * c */
		mpfr_mul(Aqj, ApjT, s, rnd);	/* use orig value of Apj */
		mpfr_add(Aqj, Aqj, tmp, rnd);
	}

}
#undef ApjT
#undef tmp

#define AipT tmp1
#define tmp tmp2
static void apply_jacobi_R( const mps_ptr A,
                                   const unsigned int p,
                                   const unsigned int q,
                                   const mpfr_ptr c,
                                   const mpfr_ptr s,
                                   const mpfr_rnd_t rnd)
{
	mpfr_ptr Aip, Aiq;
	unsigned int i;
	const unsigned int M = MPS_NUMROW(A);	/* Is this right? */

	for ( i = 1; i <= M; ++i )
	{
		Aip = mps_get_ele(A, i, p);
		Aiq = mps_get_ele(A, i, q);
		mpfr_set(AipT, Aip, rnd);		/* need to hold onto the value */

		mpfr_mul(Aip, Aip, c, rnd);		/* A(i,p) = Aip * c - Aiq * s */
		mpfr_mul(tmp, Aiq, s, rnd);
		mpfr_sub(Aip, Aip, tmp, rnd);

		mpfr_mul(tmp, Aiq, c, rnd);		/* A(i,q) = Aip * s + Aiq * c */
		mpfr_mul(Aiq, AipT, s, rnd);	/* use orig value of Aip */
		mpfr_add(Aiq, Aiq, tmp, rnd);
	}

}
#undef AipT
#undef tmp

#define scale tmp1
#define ssq tmp2
#define ax tmp3
/* returns an mpfr type that needs to be cleared */
static void mps_norm( const mpfr_ptr sum, const mps_ptr A, const mpfr_rnd_t rnd)
{
	unsigned int i, j;
	const unsigned int M = MPS_NUMROW(A), N = MPS_NUMCOL(A);
	mpfr_ptr Aij;

	mpfr_set_ui(sum, 0, rnd);
	mpfr_set_ui(scale, 0, rnd);
	mpfr_set_ui(ssq, 1, rnd);

	for ( i = 1; i <= M; ++i )
	{
		for ( j = 1; j <= N; ++j )
		{
			Aij = mps_get_ele(A, j, i);

			if ( mpfr_sgn(Aij) )
			{
				mpfr_abs(ax, Aij, rnd);

				if ( mpfr_less_p(scale, ax) )
				{
					mpfr_div(scale, scale, ax, rnd);
					mpfr_sqr(scale, scale, rnd);
					mpfr_mul(scale, scale, ssq, rnd);
					mpfr_add_ui(ssq, scale, 1, rnd);
					mpfr_set(scale, ax, rnd);
				}
				else
				{
					mpfr_div(ax, ax, scale, rnd);
					mpfr_sqr(ax, ax, rnd);
					mpfr_add(ssq, ssq, ax, rnd);
				}
			}
		}
	}

	mpfr_sqrt(ssq, ssq, rnd);		/* sum = scale * sqrt(ssq) */
	mpfr_mul(sum, ssq, scale, rnd);

}
#undef scale
#undef ssq
#undef ax

int mps_eigen_jacobi(const mps_ptr A,
                     const mps_ptr eval,
                     const mps_ptr evec,
                     const unsigned int max_rot,
                     unsigned int* nrot,
                     const mpfr_rnd_t rnd)
{
	unsigned int i, p, q;
	mpfr_ptr ep, subeval;
	mpfr_t c, s;
	mpfr_t red, redsum, nrm;
    const unsigned int N = MPS_NUMCOL(A);

	/* TODO: eval is a vector. 1 x N matrix... These may be wrong */
	MPS_ASSERT_MSG(MPS_IS_SQUARE(A), "Expected square matrix in mps_eigen_jacobi()\n");
	MPS_ASSERT_MSG(MPS_NUMROW(A) == MPS_NUMROW(evec) || N == MPS_NUMCOL(evec),
			"Expected eigenvector to match input matrix in mps_eigen_jacobi()\n");
	MPS_ASSERT_MSG(MPS_NUMROW(A) == MPS_NUMCOL(eval),
					"Expected eigenvalue vector to match input in mps_eigen_jacobi()\n");

	/* initialize shared stuff */
	mpfr_inits2(MPS_PREC(A), one, tmp1, tmp2, tmp3, (mpfr_ptr) NULL);
	mpfr_set_ui(one, 1, rnd);

	/* initialize local stuff */
	mpfr_inits2(MPS_PREC(A), red, redsum, nrm, c, s, (mpfr_ptr) NULL);
	mpfr_set_ui(red, 0, rnd);
	mpfr_set_ui(redsum, 0, rnd);

	mps_zero(eval, rnd);
	mps_identity(evec, rnd);

	for ( i = 0; i < max_rot; ++i )
	{
		mps_norm(nrm, A, rnd);
		if ( mpfr_zero_p(nrm) )
			break;

		for ( p = 1; p <= N; ++p )
		{
			for ( q = p + 1; q <= N; ++q )
			{
				mps_symschur2(red, A, p, q, c, s, rnd);
				mpfr_add(redsum, redsum, red, rnd);
				/* compute A <- J^T A J */
				apply_jacobi_L(A, p, q, c, s, rnd);
				apply_jacobi_R(A, p, q, c, s, rnd);

				/* compute V <- V J */
				apply_jacobi_R(evec, p, q, c, s, rnd);
			}
		}
	}

	*nrot = i;

	for ( p = 1; p <= N; ++p )
	{
		ep = mps_get_ele(A, p, p);
		subeval = mps_get_ele_seq(eval, p);	/* TODO: Should be vector...get columnwise? */
		mpfr_set(subeval, ep, rnd);
	}

	/* free local stuff */
	mpfr_clears(red, redsum, nrm, c, s, (mpfr_ptr) NULL);

	/* free shared stuff */
	mpfr_clears(one, tmp1, tmp2, tmp3, (mpfr_ptr) NULL);

	if ( i == max_rot )
		return MPS_EMAXITER;

	return MPS_SUCCESS;

}

int mps_eigen_invert_jacobi( const mps_ptr A,
                             const mps_ptr Ainv,
                             const unsigned int max_rot,
                             const mpfr_rnd_t rnd)
{
	int status;
	mps_t eval, evec, tmpmat;
	mpfr_ptr sub, vik, vjk;
	mpfr_t Ainv_ij, tmp, f;
	unsigned int i, j, k, nrot = 0;
    const unsigned int N = MPS_NUMCOL(A);

	MPS_ASSERT_MSG(MPS_IS_SQUARE(A), "Jacobi method expects square matrix in mps_eigen_invert_jacobi()\n");
	MPS_ASSERT_MSG(MPS_SAME_SIZE(A, Ainv), "Mismatched matrix size in mps_eigen_invert_jacobi()\n");

	mpfr_init2(Ainv_ij, MPS_PREC(A));
	mpfr_inits2(MPS_PREC(A), f, tmp, (mpfr_ptr) NULL);

	mps_init(eval, N, N, MPS_PREC(A), MPS_COL_ORDER);
	mps_init(evec, N, 1, MPS_PREC(A), MPS_COL_ORDER);	/* vector, single column */
	mps_init(tmpmat, N, N, MPS_PREC(A), MPS_COL_ORDER);
	mps_clone(tmpmat, A);

	status = mps_eigen_jacobi(tmpmat, eval, evec, max_rot, &nrot, rnd);

	for ( i = 1; i <= N; ++i )
	{
		for ( j = 1; j <= N; ++j )
		{
			mpfr_set_ui(Ainv_ij, 0, rnd);
			for ( k = 1; k <= N; ++k )
			{
				sub = mps_get_ele_seq(eval, k);
				mpfr_ui_div(f, 1, sub, rnd);

				vik = mps_get_ele(evec, i, k);
				vjk = mps_get_ele(evec, j, k);
				mpfr_mul(tmp, vik, vjk, rnd);		/* Ainv_ij += vik * vjk * f */
				mpfr_mul(tmp, tmp, f, rnd);
				mpfr_add(Ainv_ij, Ainv_ij, tmp, rnd);
			}
			sub = mps_get_ele(Ainv, i, j);
			mpfr_set(sub, Ainv_ij, rnd);
		}
	}

	mps_frees(eval, evec, tmpmat, (mps_ptr) NULL);
	mpfr_clears(f, tmp, (mpfr_ptr) NULL);

	if (status)
		return status;
	else
		return MPS_SUCCESS;

}

