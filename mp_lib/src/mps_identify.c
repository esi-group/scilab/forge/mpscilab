/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * This file contains basic test to identify certain class of matrix.
 */

/* Return 1 if the operand is a scalar. i.e. a 1x1 matrix. */
int mps_is_scalar( const mps_ptr op )
{
    if( (MPS_NUMCOL(op) == 1) && (MPS_NUMROW(op) == 1) )
        return 1;
    else
        return 0;
}

/* Return 1 if a matrix is square. */
int mps_is_square( const mps_ptr op )
{
    if( MPS_NUMCOL(op) == MPS_NUMROW(op) )
        return 1;
    else
        return 0;
}

/* Return 1 if the matrix is a column vector. */
int mps_is_col_vector( const mps_ptr op )
{
    if( MPS_NUMCOL(op) == 1 )
        return 1;
    else
        return 0;
}

/* Return 1 if the matrix is a row vector. */
int mps_is_row_vector( const mps_ptr op )
{
    if( MPS_NUMROW(op) == 1 )
        return 1;
    else
        return 0;
}

/* Return 1 if all the element of a matrix are exactly 0. */
int mps_is_zeros( const mps_ptr op )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );

        if( mpfr_zero_p(subop) )
            continue;
        else
            return 0;
    }

    return 1;
}

/* Return 1 if all the element of a matrix are exactly 1. */
int mps_is_ones( const mps_ptr op )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );

        if( mpfr_cmp_ui( subop, 1 ) != 0 )
            return 0;
    }

    return 1;
}

