/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

int mps_sum( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_sum().\n" );

    tab = malloc( sizeof(void*) * matsize );

    for( i = 0; i < matsize; i++ )
    {
        tab[i] = mps_get_ele_seq( op, i+1 );
    }

    mpfr_sum( mps_get_ele( rop, 1, 1 ), tab, matsize, GMP_RNDN );

    free( tab );

    return 0;
}

int mps_sum_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop, subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_sum_quick().\n" );

    subrop = mps_get_ele_col( rop, 1 );
    subop = mps_get_ele_col( op, 1 );
    mpfr_set( subrop, subop, GMP_RNDN );

    for ( i = 2; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );

        mpfr_add( subrop, subrop, subop, GMP_RNDN );
    }

    return 0;
}

int mps_sumr( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Result operand is of the wrong size in mps_sumr().\n" );

    tab = malloc( sizeof(void*) * MPS_NUMCOL(op) );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 0; j < MPS_NUMCOL(op); j++ )
        {
            tab[j] = mps_get_ele( op, i, j+1 );
        }

        mpfr_sum( mps_get_ele_seq( rop, i ), tab, MPS_NUMCOL(op), GMP_RNDN );
    }

    free( tab );

    return 0;
}

int mps_sumr_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop, subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Result operand is of the wrong size in mps_sumr_quick().\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele( op, i, 1 );
        mpfr_set( subrop, subop, GMP_RNDN );

        for( j = 2; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );
            mpfr_add( subrop, subrop, subop, GMP_RNDN );
        }
    }

    return 0;
}

int mps_sumc( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr* tab;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Result operand is of the wrong size in mps_sumc().\n" );

    tab = malloc( sizeof(void*) * MPS_NUMROW(op) );

    for( i = 1; i <= MPS_NUMCOL(op); i++ )
    {
        for( j = 0; j < MPS_NUMROW(op); j++ )
        {
            tab[j] = mps_get_ele( op, j+1, i );
        }

        mpfr_sum( mps_get_ele_seq( rop, i ), tab, MPS_NUMROW(op), GMP_RNDN );
    }

    free( tab );

    return 0;
}

int mps_sumc_quick( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop, subrop;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Result operand is of the wrong size in mps_sumc_quick().\n" );

    for( i = 1; i <= MPS_NUMCOL(op); i++ )
    {
        subrop = mps_get_ele_seq( rop, i );
        subop = mps_get_ele( op, 1, i );
        mpfr_set( subrop, subop, GMP_RNDN );

        for( j = 2; j <= MPS_NUMROW(op); j++ )
        {
            subop = mps_get_ele( op, j, i );
            mpfr_add( subrop, subrop, subop, GMP_RNDN );
        }
    }

    return 0;
}
