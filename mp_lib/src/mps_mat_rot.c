/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* puts op rotated clockwise by 1 in rop */
int mps_mat_rotate_cw( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i, j, bound, outbound, mid;
    const unsigned int numcol = MPS_NUMCOL(op), numrow = MPS_NUMROW(op);

	if (numcol > numrow)		/* this keeps the middle untouched in the odd cases */
		outbound = numrow >> 1;
	else
		outbound = numcol >> 1;

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_mat_rotate_cw()\n");

	/* go in through the "loops" */
	for ( i = 1; i <= outbound; ++i )
	{
		/* go across 1st row */
		bound = numcol - i;
		for ( j = i; j <= bound; ++j )
		{
			subop = mps_get_ele(op, i, j);
			subrop = mps_get_ele(rop, i, j + 1);
			mpfr_set(subrop, subop, rnd);
		}

		/* go along bottom row, towards the left */
		bound = numrow - i + 1;
		for ( j = numcol - i + 1; j > i ; --j )
		{
			subop = mps_get_ele(op, bound, j);
			subrop = mps_get_ele(rop, bound, j - 1);
			mpfr_set(subrop, subop, rnd);
		}

		/* go down right column */
		bound = numcol - i + 1;
		for ( j = i; j <= numrow - i; ++j )
		{
			subop = mps_get_ele(op, j, bound);
			subrop = mps_get_ele(rop, j + 1, bound);
			mpfr_set(subrop, subop, rnd);
		}

		/* go up left column */
		bound = numrow - i + 1;
		for ( j = bound; j > i; --j )
		{
			subop = mps_get_ele(op, j, i);
			subrop = mps_get_ele(rop, j - 1, i);
			mpfr_set(subrop, subop, rnd);
		}
	}

	if ( numcol > numrow )		/* need to go across central row */
	{
		outbound = mid = (numcol + 1) >> 1;
		if (numcol % 2 == 0)	/* this seems to indicate doing it wrong */
			++outbound;

		for ( j = i - 1; j <= outbound; ++j )
		{
			subop = mps_get_ele(op, i, j + 1);
			subrop = mps_get_ele(rop, i, numcol - j);
			mpfr_set(subrop, subop, rnd);
		}
	}
	else if ( numcol < numrow )		/* need to go down center pillar */
	{
		outbound = mid = (numrow + 1) >> 1;
		if (numrow % 2 == 0)	/* this seems to indicate doing it wrong */
			++outbound;

		for ( j = i - 1; j <= outbound; ++j )
		{
			subop = mps_get_ele(op, j + 1, i);
			subrop = mps_get_ele(rop, numrow - j, i);
			mpfr_set(subrop, subop, rnd);
		}
	}
	else if ( numrow % 2 != 0 && numrow == numcol)	/* just the middle, unchanged */
	{
		subop = mps_get_ele(op, (numrow + 1) >> 1, (numcol + 1) >> 1);
		subrop = mps_get_ele(rop, (numrow + 1) >> 1, (numcol + 1) >> 1);
		mpfr_set(subrop, subop, rnd);
	}
	/* else numrow == numcol, even, should be fine */

    return 0;
}

/* gives a copy of op shifted over some number of places. signed int means can go backwards. */
int mps_shift_row( const mps_ptr rop, const mps_ptr op, int shift, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_shift()\n");

	--shift;
	for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_row(op, i);
		subrop = mps_get_ele_row(rop, (i + shift) % matsize + 1);
		mpfr_set(subrop, subop, rnd);
	}

	return 0;
}

/* I'm not sure this makes sense */
int mps_shift_col( const mps_ptr rop, const mps_ptr op, int shift, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_shift()\n");

	--shift;
	for ( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_col(op, i);
		subrop = mps_get_ele_col(rop, (i + shift) % matsize + 1);
		mpfr_set(subrop, subop, rnd);
	}

	return 0;
}

int mps_reflect_x( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i, j;
    const unsigned int numcol = MPS_NUMCOL(op), numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_reflect_x()\n");

	for ( i = 1; i <= numcol; ++i )
	{
		for ( j = 1; j <= numrow; ++j )
		{
			subop = mps_get_ele(op, j, i);
			subrop = mps_get_ele(rop, j, 1 + numcol - i);
			mpfr_set(subrop, subop, rnd);
		}
	}

	return 0;
}

int mps_reflect_y( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i, j;
    const unsigned int numcol = MPS_NUMCOL(op), numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_reflect_y()\n");

	for ( i = 1; i <= numrow; ++i )
	{
		for ( j = 1; j <= numcol; ++j )
		{
			subop = mps_get_ele(op, i, j);
			subrop = mps_get_ele(rop, 1 + numrow - i, j);
			mpfr_set(subrop, subop, rnd);
		}
	}

	return 0;
}

/* reflect about diagonal. Only makes sense for square matrix.
 * TODO: Rename better. */
int mps_reflect_diag_lr( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i, j;
    const unsigned int sizeside = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_reflect_diag_lr()\n");

	MPS_ASSERT_MSG( MPS_IS_SQUARE(op), "Expected square matrix in mps_reflect_diag_lr()\n");

	for ( i = 1; i <= sizeside; ++i )
	{
		for ( j = 1; j <= sizeside; ++j )
		{
			subop = mps_get_ele(op, i, j);
			subrop = mps_get_ele(rop,  j, i);
			mpfr_set(subrop, subop, rnd);
		}
	}

	return 0;
}

int mps_reflect_diag_rl( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
	mpfr_ptr subop, subrop;
	unsigned int i, j;
    const unsigned int sizeside = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop, op),
				"Expected same size result operand in mps_reflect_diag_rl()\n");

	MPS_ASSERT_MSG( MPS_IS_SQUARE(op), "Expected square matrix in mps_reflect_diag_rl()\n");

	for ( i = 1; i <= sizeside; ++i )
	{
		for ( j = 1; j <= sizeside; ++j )
		{
			subop = mps_get_ele(op, i, j);
			subrop = mps_get_ele(rop, sizeside - j + 1, sizeside - i + 1);
			mpfr_set(subrop, subop, rnd);
		}
	}

	return 0;
}

