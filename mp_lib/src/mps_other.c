/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"


/* Set each element of rop to exp(op - 1) */
int mps_expm1( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_expm1()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_log1p( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to exponential integral of op */
int mps_eint( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_eint()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_eint( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the dilogarithm of op */
int mps_li2( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_li2()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_li2( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the ln of gamma function of op */
int mps_lngamma( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_lngamma()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_lngamma( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the Riemann zeta function of op */
int mps_zeta( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_zeta()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_zeta( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 1st kind, order 0 */
int mps_j0( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_j0()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_j0( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 1st kind, order 1 */
int mps_j1( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_j1()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_j1( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 1st kind, order n */
int mps_jn( const mps_ptr rop, const long n, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_jn()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_jn( subrop, n, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 2nd kind, order 0 */
int mps_y0( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_y0()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_y0( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 2nd kind, order 1 */
int mps_y1( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_y1()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_y1( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the bessel function of the 2nd kind, order n */
int mps_yn( const mps_ptr rop, const long n, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_yn()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_yn( subrop, n, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the absolute value of op */
int mps_abs( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_abs()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_abs( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the absolute value of op */
int mps_abs_double( const mps_ptr rop, const double op[], const mps_order_t order, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_abs( subrop, subrop, rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            mpfr_set_d( subrop, op[i-1], rnd );
            mpfr_abs( subrop, subrop, rnd );
        }
    }

    return 0;
}


/* Set each element of rop to the negative of op */
int mps_neg( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_neg()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_neg( subrop, subop, rnd );
    }

    return 0;
}


/* Set each element of rop to the negative of (double)op */
int mps_neg_double( const mps_ptr rop, const double op1[], const mps_order_t order1, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    if( order1 == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_col( rop, i );
            mpfr_set_d( subrop, op1[i-1], rnd );
            mpfr_neg( subrop, subrop, rnd );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; ++i )
        {
            subrop = mps_get_ele_row( rop, i );
            mpfr_set_d( subrop, op1[i-1], rnd );
            mpfr_neg( subrop, subrop, rnd );
        }
    }

    return 0;
}

