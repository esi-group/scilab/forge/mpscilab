/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* calculates the epsilon value for given prec.
 * Might want a constant that gets calculated once when you
 * change precision.
 * Wikipedia:In floating point arithmetic, the machine epsilon (also called macheps, machine precision) is, for a particular floating point unit, the difference between 1 and the smallest exactly representable number greater than one. It gives an upper bound on the relative error due to rounding of floating point numbers. */
/* FIXME: Not sure this is always right */
void mps_epsilon( const mpfr_ptr rop, const mp_prec_t prec, const mpfr_rnd_t rnd )
{
	/* FIXME: Not a good place to cast? */
	mpfr_set_si(rop, 1 - (int) prec, rnd);
	mpfr_exp2(rop, rop, rnd);
}

void mpfr_maxval( const mpfr_ptr rop )
{
	mpfr_set_inf(rop, 1);
	mpfr_nextbelow(rop);
}

void mpfr_minval( const mpfr_ptr rop )
{
	mpfr_set_inf(rop, -1);
	mpfr_nextabove(rop);
}

