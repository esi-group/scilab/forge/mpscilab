/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * Return a string containing a serialized representation of the matrix in base
 * 16 ordered column major. Column are separated by a line break "\n" and rows
 * by a semi-colon ";". Information about the precision and ordering are not
 * exported.
 */
char* mps_get_str16( const mps_ptr op )
{
    char* outp;
    char* tempstr;
    char* subopstr;
    mpfr_ptr subop;
    size_t len;
    mp_exp_t e;
    unsigned int i, j;
	const unsigned int numrow = MPS_NUMROW(op), numcol = MPS_NUMCOL(op);

    /* Estimate the space required by the matrix */
    len = MPS_SIZE( op ) * (ceil(((double)MPS_PREC( op )/(double)4)) + 10) + 1;

    subopstr = malloc( (ceil(((double)MPS_PREC( op )/(double)4)) + 10) + 1 );
    outp = malloc( len );
    tempstr = outp;

    for ( j = 1; j <= numcol; ++j )
    {
        for ( i = 1; i <= numrow; ++i )
        {
            subop = mps_get_ele( op, i, j );

            mpfr_get_str( subopstr, &e, 16, 0, subop, GMP_RNDN );

            if ( *subopstr == '-' )
            {
                tempstr += sprintf( tempstr, "-." );
                tempstr += sprintf( tempstr, "%s", subopstr+1 );
            }
            else
            {
                tempstr += sprintf( tempstr, "." );
                tempstr += sprintf( tempstr, "%s", subopstr );
            }

            tempstr += strlen( tempstr );

            if ( e >= 0 )
                tempstr += sprintf( tempstr, "@%x ", (unsigned int) abs(e) );
            else
                tempstr += sprintf( tempstr, "@-%x ", (unsigned int) abs(e) );
        }

        tempstr += sprintf( tempstr, "; " );
    }

    outp = realloc( outp, strlen(outp) + 1 );

    free( subopstr );

    return outp;
}

/*  Reconstruct a matrix from a string returned by the mps_set_str16() function. */
int mps_set_str16( const mps_ptr rop,
                   const char * op,
                   const mpfr_prec_t prec,
                   const mps_order_t order,
                   const mpfr_rnd_t rnd )
{
    char * substr;
    char * strc = malloc( strlen(op) + 1 );
    unsigned int rowcount = 0, colcount = 0, i, size;
    mpfr_ptr subrop;

    strcpy( strc, op );

    /* Count the number of columns. */
    substr = strchr( strc, ';' );
    while ( substr != NULL )
    {
        colcount = colcount + 1;
        substr = strchr( substr + 1, ';' );
    }

    if ( colcount == 0 )
    {
        MPS_WARNING_MSG("Malformed matrix intput in mps_set_str16().\n");
        free( strc );
        return 1;
    }

    /* Count the number of rows. */
    substr = strtok( strc, ";" );
    substr = strtok( substr, " " );
    while ( substr != NULL )
    {
        rowcount = rowcount + 1;
        substr = strtok( 0, " " );
    }

    mps_set( rop, rowcount, colcount, prec, order );

    strcpy( strc, op );
    substr = strtok( strc, "; " );

	size = colcount * rowcount;
    for ( i = 1; i <= size; ++i )
    {
        if ( substr == NULL )
        {
            MPS_WARNING_MSG("Malformed matrix intput in mps_set_str16().\n");
            free( strc );
            return 1;
        }

        subrop = mps_get_ele_col( rop, i );

        if ( mpfr_set_str( subrop, substr, 16, rnd ) != 0 )
        {
            MPS_WARNING_MSG("Malformed matrix intput in mps_set_str16().\n");
            free( strc );
            return 1;
        }

        substr = strtok( NULL, "; " );
    }

    free( strc );

    return 0;
}

