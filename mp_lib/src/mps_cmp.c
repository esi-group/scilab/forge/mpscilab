/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

#include <math.h>

/* Tests if every elements are equal. returns 1 if they are, 0 otherwise */
int mps_equal( const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG(MPS_SAME_SIZE(op1, op2), "Expected operands of same size in mps_equal()\n");

    for ( i = 1; i <= matsize; ++i )
    {
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );
        if ( !mpfr_equal_p(subop1, subop2) )
            return 0;
    }

    return 1;
}

/* Tests if every elements are equal. returns 1 if they are, 0 otherwise */
int mps_equal_double( const mps_ptr op1, const double op2[], const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            if ( mpfr_cmp_d(subop1, op2[i-1]) != 0 || mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
                return 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            if ( mpfr_cmp_d(subop1, op2[i-1]) != 0 )
                return 0;
        }
    }

    return 1;
}

/* Tests if every elements are equal. returns 1 if they are, 0 otherwise */
int mps_double_equal_double( const double op1[], 
                             const mps_order_t order1, 
                             const double op2[], 
                             const mps_order_t order2, 
                             const unsigned int m, 
                             const unsigned int n )
{
    /* TODO */
    return 1;
}

/* Tests if every elements are equal. returns 1 if they are, 0 otherwise */
int mps_equal_margin( const mps_ptr op1, const mps_ptr op2, const mpfr_ptr margin )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_t mpfrtmp;

    MPS_ASSERT_MSG(MPS_SAME_SIZE(op1, op2), "Expected operands of same size in mps_equal()\n");

    mpfr_init2( mpfrtmp, MPS_PREC(op1) );

    for ( i = 1; i <= matsize; ++i )
    {
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );
        if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
        {
            mpfr_clear( mpfrtmp );
            return 0;
        }
        mpfr_sub( mpfrtmp, subop1, subop2, GMP_RNDN );
        if( mpfr_cmpabs( mpfrtmp, margin ) > 0 )
        {
            mpfr_clear( mpfrtmp );
            return 0;
        }
    }

    mpfr_clear( mpfrtmp );

    return 1;
}

/* Tests if every elements are equal. returns 1 if they are, 0 otherwise */
int mps_equal_margin_double( const mps_ptr op1, const double op2[], const mps_order_t order, const mpfr_ptr margin )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_t mpfrtmp;

    mpfr_init2( mpfrtmp, MPS_PREC(op1) );

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( mpfrtmp );
                return 0;
            }

            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, margin ) > 0 )
            {
                mpfr_clear( mpfrtmp );
                return 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( mpfrtmp );
                return 0;
            }

            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, margin ) > 0 )
            {
                mpfr_clear( mpfrtmp );
                return 0;
            }
        }
    }

    mpfr_clear( mpfrtmp );

    return 1;
}

/* Tests if every elements are equal within the specified margin in bits. */
int mps_equal_bmargin( const mps_ptr op1, const mps_ptr op2, const unsigned int bits )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_t mpfrtmp, margin, expo, adjmargin;

    MPS_ASSERT_MSG(MPS_SAME_SIZE(op1, op2), "Expected operands of same size in mps_equal_bmargin()\n");

    mpfr_init2( mpfrtmp, MPS_PREC(op1) );
    mpfr_init2( margin, MPS_PREC(op1) );
    mpfr_init2( expo, MPS_PREC(op1) );
    mpfr_init2( adjmargin, MPS_PREC(op1) );

    if( bits > 1 )
    {
        mpfr_set_ui( expo, MPS_PREC(op1) - bits, GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( mpfrtmp, expo, GMP_RNDN );

        mpfr_set_ui( expo, MPS_PREC(op1), GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );

        mpfr_sub( margin, mpfrtmp, margin, GMP_RNDN );
    }
    else if( bits == 1 )
    {
        mpfr_set_ui( expo, MPS_PREC(op1), GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );
    }
    else
        mpfr_set_zero( margin, 1 );

    mpfr_clear( expo );

    for ( i = 1; i <= matsize; ++i )
    {
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );
        if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
        {
            mpfr_clear( mpfrtmp );
            mpfr_clear( margin );
            mpfr_clear( adjmargin );
            return 0;
        }

        mpfr_mul_2si( adjmargin, margin, mpfr_get_exp(subop1), GMP_RNDN );
        mpfr_sub( mpfrtmp, subop1, subop2, GMP_RNDN );

        if( mpfr_cmpabs( mpfrtmp, adjmargin ) > 0 )
        {
            mpfr_clear( mpfrtmp );
            mpfr_clear( margin );
            mpfr_clear( adjmargin );
            return 0;
        }
    }

    mpfr_clear( mpfrtmp );
    mpfr_clear( margin );
    mpfr_clear( adjmargin );

    return 1;
}

/* Tests if every elements are equal within the specified margin in bits. */
int mps_equal_bmargin_double( const mps_ptr op1, const double op2[], const mps_order_t order, const unsigned int bits )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_t mpfrtmp, margin, expo, adjmargin;

    mpfr_init2( mpfrtmp, MPS_PREC(op1) );
    mpfr_init2( margin, MPS_PREC(op1) );
    mpfr_init2( expo, MPS_PREC(op1) );
    mpfr_init2( adjmargin, MPS_PREC(op1) );

    if( bits > 1 )
    {
        mpfr_set_ui( expo, MPS_PREC(op1) - bits, GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( mpfrtmp, expo, GMP_RNDN );

        mpfr_set_ui( expo, MPS_PREC(op1), GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );

        mpfr_sub( margin, mpfrtmp, margin, GMP_RNDN );
    }
    else if( bits == 1 )
    {
        mpfr_set_ui( expo, MPS_PREC(op1), GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );
    }
    else
        mpfr_set_zero( margin, 1 );

    mpfr_clear( expo );

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }

            mpfr_mul_2si( adjmargin, margin, mpfr_get_exp(subop1), GMP_RNDN );
            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, adjmargin ) > 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }

            mpfr_mul_2si( adjmargin, margin, mpfr_get_exp(subop1), GMP_RNDN );
            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, adjmargin ) > 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }
        }
    }

    mpfr_clear( margin );
    mpfr_clear( mpfrtmp );
    mpfr_clear( adjmargin );

    return 1;
}

/* Tests if every elements are equal within the specified margin in bits. */
int mps_equal_double_bmargin( const double op2[], const mps_order_t order, const mps_ptr op1, const unsigned int bits )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_t mpfrtmp, margin, expo, adjmargin;
    int fexp;

    mpfr_init2( mpfrtmp, 53 );
    mpfr_init2( margin, 53 );
    mpfr_init2( expo, 53 );
    mpfr_init2( adjmargin, 53 );

    if( bits > 1 )
    {
        mpfr_set_ui( expo, 53 - bits, GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( mpfrtmp, expo, GMP_RNDN );

        mpfr_set_ui( expo, 53, GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );

        mpfr_sub( margin, mpfrtmp, margin, GMP_RNDN );
    }
    else if( bits == 1 )
    {
        mpfr_set_ui( expo, 53, GMP_RNDN );
        mpfr_neg( expo, expo, GMP_RNDN );
        mpfr_exp2( margin, expo, GMP_RNDN );
    }
    else
        mpfr_set_zero( margin, 1 );

    mpfr_clear( expo );

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }
            
            frexp( op2[i-1], &fexp );
            mpfr_mul_2si( adjmargin, margin, fexp, GMP_RNDN );
            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, adjmargin ) > 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            if ( mpfr_nan_p(subop1) != 0 || isnan(op2[i-1]) != 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }

            frexp( op2[i-1], &fexp );
            mpfr_mul_2si( adjmargin, margin, fexp, GMP_RNDN );
            mpfr_sub_d( mpfrtmp, subop1, op2[i-1], GMP_RNDN );

            if( mpfr_cmpabs( mpfrtmp, adjmargin ) > 0 )
            {
                mpfr_clear( margin );
                mpfr_clear( mpfrtmp );
                mpfr_clear( adjmargin );
                return 0;
            }
        }
    }

    mpfr_clear( margin );
    mpfr_clear( mpfrtmp );
    mpfr_clear( adjmargin );

    return 1;
}

/* compare each elements absolute value (for equality only) */
int mps_equal_abs( const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG(MPS_SAME_SIZE(op1, op2), "Expected operands of same size in mps_equal_abs()\n");

    for ( i = 1; i <= matsize; ++i )
    {
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );
        if ( mpfr_cmp_abs(subop1, subop2) )
            return 0;
    }

    return 1;
}

/* Entry-wise comparison of two mps matrices of the same size. */
int mps_cmp( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_cmp()\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            rop[i-1] = mpfr_cmp( subop1, subop2 );
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            rop[i-1] = mpfr_cmp( subop1, subop2 );
        }
    }

    return 1;
}

/* Entry-wise comparison of an mps matrix and a matrix of double. */
int mps_cmp_double( int rop[],
                    const mps_order_t roporder,
                    const mps_ptr op1,
                    const double op2[],
                    const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] );
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] );
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] );
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] );
            }
        }
    }

    return 1;
}

/* Entry-wise comparison of a matrix of double and an mps matrix. */
int mps_double_cmp( int rop[],
                    const mps_order_t roporder,
                    const double op1[],
                    const mps_order_t order,
                    const mps_ptr op2 )
{
    mpfr_ptr subop2;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op2);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_col( op2, i );

            if( roporder == MPS_COL_ORDER )
                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) * -1;
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op2)) + \
				    MPS_NUMCOL(op2) * ((i-1) % MPS_NUMROW(op2)) );
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) * -1;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_row( op2, i );

            if( roporder == MPS_ROW_ORDER )
                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) * -1;
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op2)) + \
				    MPS_NUMROW(op2) * ((i-1) % MPS_NUMCOL(op2)) );
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) * -1;
            }
        }
    }

    return 1;
}

/* For the sake of being complete, comparison between two matrices of double. */
int mps_double_cmp_double( int rop[],
                           const mps_order_t roporder,
                           const double op1[],
                           const mps_order_t order1,
                           const double op2[],
                           const mps_order_t order2,
                           const unsigned int m,
                           const unsigned int n )
{
    /* TODO */
    return 1;
}

/* rop(i) is set to 1 if both coresponding elements of the input matrices are equal, 0 otherwise. */
int mps_equal_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in equal_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) == 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) == 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_equal_p_double( int rop[],
                        const mps_order_t roporder,
                        const mps_ptr op1,
                        const double op2[],
                        const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_equal_p_double( int rop[],
                           const mps_order_t roporder,
                           const double op1[],
                           const mps_order_t order1,
                           const double op2[],
                           const mps_order_t order2,
                           const unsigned int m,
                           const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] == op2[ai] ? 1 : 0;
    }
    
    return 0;
}

/* rop(i) is set to 1 if both coresponding elements of the input matrices are less or greater, 0 otherwise. */
int mps_lessgreater_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in equal_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 1;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) == 0 ? 0 : 1;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 1;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) == 0 ? 0 : 1;
        }
    }

    return 0;
}

int mps_lessgreater_p_double( int rop[],
                              const mps_order_t roporder,
                              const mps_ptr op1,
                              const double op2[],
                              const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 1;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 0 : 1;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 1;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 0 : 1;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 1;
                    continue;
                }
                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 0 : 1;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 1;
                    continue;
                }

                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) == 0 ? 0 : 1;
            }
        }
    }

    return 0;
}

int mps_double_lessgreater_p_double( int rop[],
                                     const mps_order_t roporder,
                                     const double op1[],
                                     const mps_order_t order1,
                                     const double op2[],
                                     const mps_order_t order2,
                                     const unsigned int m,
                                     const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] == op2[ai] ? 0 : 1;
    }
    
    return 0;
}

/* rop(i) is set to 1 if op1(i) > op2(i). */
int mps_greater_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in greater_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) > 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) > 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_greater_p_double( int rop[],
                          const mps_order_t roporder,
                          const mps_ptr op1,
                          const double op2[],
                          const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) > 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) > 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) > 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 0;
                    continue;
                }

                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) > 0 ? 1 : 0;
            }
        }
    }

    return 0;
}

int mps_double_greater_p( int rop[],
                          const mps_order_t roporder,
                          const double op1[],
                          const mps_order_t order,
                          const mps_ptr op2 )
{
    mpfr_ptr subop2;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op2);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_col( op2, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) < 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op2)) + \
				    MPS_NUMCOL(op2) * ((i-1) % MPS_NUMROW(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[j] = 0;
                    continue;
                }

                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) < 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_row( op2, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) < 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op2)) + \
				    MPS_NUMROW(op2) * ((i-1) % MPS_NUMCOL(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) < 0 ? 1 : 0;
            }
        }
    }

    return 0;
}

int mps_double_greater_p_double( int rop[],
                                     const mps_order_t roporder,
                                     const double op1[],
                                     const mps_order_t order1,
                                     const double op2[],
                                     const mps_order_t order2,
                                     const unsigned int m,
                                     const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] > op2[ai] ? 1 : 0;
    }
    
    return 0;
}

/* rop(i) is set to 1 if op1(i) >= op2(i). */
int mps_greaterequal_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in greaterequal_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) >= 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) >= 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_greaterequal_p_double( int rop[],
                               const mps_order_t roporder,
                               const mps_ptr op1,
                               const double op2[],
                               const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) >= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) >= 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) >= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[j] = 0;
                    continue;
                }

                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) >= 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_greaterequal_p( int rop[],
                               const mps_order_t roporder,
                               const double op1[],
                               const mps_order_t order,
                               const mps_ptr op2 )
{
    mpfr_ptr subop2;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op2);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_col( op2, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) <= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op2)) + \
				    MPS_NUMCOL(op2) * ((i-1) % MPS_NUMROW(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[j] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) <= 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_row( op2, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) <= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op2)) + \
				    MPS_NUMROW(op2) * ((i-1) % MPS_NUMCOL(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) <= 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_greaterequal_p_double( int rop[],
                                      const mps_order_t roporder,
                                      const double op1[],
                                      const mps_order_t order1,
                                      const double op2[],
                                      const mps_order_t order2,
                                      const unsigned int m,
                                      const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] >= op2[ai] ? 1 : 0;
    }
    
    return 0;
}

/* rop(i) is set to 1 if op1(i) < op2(i). */
int mps_less_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in greaterequal_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) < 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) < 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_less_p_double( int rop[],
                               const mps_order_t roporder,
                               const mps_ptr op1,
                               const double op2[],
                               const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) < 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) < 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) < 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) < 0 ? 1 : 0;
            }
        }
    }

    return 0;
}

int mps_double_less_p( int rop[],
                               const mps_order_t roporder,
                               const double op1[],
                               const mps_order_t order,
                               const mps_ptr op2 )
{
    mpfr_ptr subop2;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op2);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_col( op2, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) > 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op2)) + \
				    MPS_NUMCOL(op2) * ((i-1) % MPS_NUMROW(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) > 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_row( op2, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) > 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op2)) + \
				    MPS_NUMROW(op2) * ((i-1) % MPS_NUMCOL(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) > 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_less_p_double( int rop[],
                                      const mps_order_t roporder,
                                      const double op1[],
                                      const mps_order_t order1,
                                      const double op2[],
                                      const mps_order_t order2,
                                      const unsigned int m,
                                      const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] < op2[ai] ? 1 : 0;
    }
    
    return 0;
}

/* rop(i) is set to 1 if op1(i) <= op2(i). */
int mps_lessequal_p( int rop[], const mps_order_t roporder, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in greaterequal_p\n" );

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            subop2 = mps_get_ele_col( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) <= 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            subop2 = mps_get_ele_row( op2, i );
            if( mpfr_nan_p(subop1) || mpfr_nan_p(subop2) )
            {
                rop[i-1] = 0;
                continue;
            }
            rop[i-1] = mpfr_cmp( subop1, subop2 ) <= 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_lessequal_p_double( int rop[],
                               const mps_order_t roporder,
                               const mps_ptr op1,
                               const double op2[],
                               const mps_order_t order )
{
    mpfr_ptr subop1;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) <= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op1)) + \
				    MPS_NUMCOL(op1) * ((i-1) % MPS_NUMROW(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) <= 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop1, op2[i-1] ) <= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op1)) + \
				    MPS_NUMROW(op1) * ((i-1) % MPS_NUMCOL(op1)) );
                if( mpfr_nan_p(subop1) || isnan(op2[i-1]) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop1, op2[i-1] ) <= 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_lessequal_p( int rop[],
                               const mps_order_t roporder,
                               const double op1[],
                               const mps_order_t order,
                               const mps_ptr op2 )
{
    mpfr_ptr subop2;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(op2);

    if ( order == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_col( op2, i );
            
            if( roporder == MPS_COL_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) >= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMROW(op2)) + \
				    MPS_NUMCOL(op2) * ((i-1) % MPS_NUMROW(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) >= 0 ? 1 : 0;
            }
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop2 = mps_get_ele_row( op2, i );

            if( roporder == MPS_ROW_ORDER )
            {
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }

                rop[i-1] = mpfr_cmp_d( subop2, op1[i-1] ) >= 0 ? 1 : 0;
            }
            else
            {
                j = ( ((i-1) / MPS_NUMCOL(op2)) + \
				    MPS_NUMROW(op2) * ((i-1) % MPS_NUMCOL(op2)) );
                if( isnan(op1[i-1]) || mpfr_nan_p(subop2) )
                {
                    rop[i-1] = 0;
                    continue;
                }
                rop[j] = mpfr_cmp_d( subop2, op1[i-1] ) >= 0 ? 1 : 0;
            }
        }
    }

    return 1;
}

int mps_double_lessequal_p_double( int rop[],
                                      const mps_order_t roporder,
                                      const double op1[],
                                      const mps_order_t order1,
                                      const double op2[],
                                      const mps_order_t order2,
                                      const unsigned int m,
                                      const unsigned int n )
{
    unsigned int i, j, ri, ai, bi;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        bi = order2 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = op1[ai] <= op2[ai] ? 1 : 0;
    }
    
    return 0;
}

/* rop(i) is set to 1 if the corresponding element is NaN. */
int mps_nan_p( int rop[], const mps_order_t roporder, const mps_ptr op1 )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            rop[i-1] = mpfr_nan_p( subop1 ) != 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            rop[i-1] = mpfr_nan_p( subop1 ) != 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_nan_p_double( int rop[],
                      const mps_order_t roporder,
                      const double op1[],
                      const mps_order_t order1,
                      const unsigned int m,
                      const unsigned int n )
{
    unsigned int i, j, ri, ai;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = isnan(op1[ai]) ? 1 : 0;
    }
    return 0;
}

/* rop(i) is set to 1 if the corresponding element is an infinity. */
int mps_inf_p( int rop[], const mps_order_t roporder, const mps_ptr op1 )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    if ( roporder == MPS_COL_ORDER )
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_col( op1, i );
            rop[i-1] = mpfr_inf_p( subop1 ) != 0 ? 1 : 0;
        }
    }
    else
    {
        for ( i = 1; i <= matsize; i++ )
        {
            subop1 = mps_get_ele_row( op1, i );
            rop[i-1] = mpfr_inf_p( subop1 ) != 0 ? 1 : 0;
        }
    }

    return 0;
}

int mps_inf_p_double( int rop[],
                      const mps_order_t roporder,
                      const double op1[],
                      const mps_order_t order1,
                      const unsigned int m,
                      const unsigned int n )
{
    unsigned int i, j, ri, ai;

    for( i = 1; i <= m*n; i++ )
    {
        j = ( ((i-1) / n) + m * ((i-1) % n) );
        ri = roporder == MPS_ROW_ORDER ? i : j;
        ai = order1 == MPS_ROW_ORDER ? i : j;
        
        rop[ri] = isinf(op1[ai]) ? 1 : 0;
    }
    return 0;
}
