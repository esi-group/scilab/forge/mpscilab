/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"


int mps_nextabove( const mps_ptr op )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_nextabove( subop );
    }

    return 0;
}


int mps_nextbelow( const mps_ptr op )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i );
        mpfr_nextbelow( subop );
    }

    return 0;
}

