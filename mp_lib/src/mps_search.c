/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/* Brute force linear search for element item in op.
 * Returns 1st index found, or 0 for not found */
unsigned int mps_linsearch( const mps_ptr op, const mpfr_ptr item )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
		if ( mpfr_equal_p(subop, item) )
			return i;
    }

    return 0;
}

/* linear search for 1st element x low <= x <= high */
unsigned int mps_linsearch_range( const mps_ptr op, const mpfr_ptr low, const mpfr_ptr high )
{
    mpfr_ptr subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
		if ( mpfr_greaterequal_p(subop, low) && mpfr_lessequal_p(subop, high))
			return i;
    }

    return 0;
}

/* find all instances of item in op. res will be allocated, and should be freed.
 * ( unless count is 0 in which case nothing is allocated )
 * returns number of matching elements
 * Primarily intended for further testing of quicksort */
unsigned int mps_linsearch_all( const mps_ptr op, const mpfr_ptr item, unsigned int** res )
{
    mpfr_ptr subop;
    unsigned int i, count;
    const unsigned int matsize = MPS_SIZE(op);

	count = 0;
	*res = NULL;

    for ( i = 1; i <= matsize; ++i )
    {
        subop = mps_get_ele_col( op, i );
		if ( mpfr_equal_p(subop, item) )
		{
			*res = (unsigned int*) realloc(*res, ++count * sizeof(unsigned int));
			(*res)[count - 1] = i;
		}
    }

    return count;
}

/* binary search, assumes op is sorted.
 * returns 1st found index, otherwise 0 if not found */
/* Searches between elements low and high
   TODO: Rename something better */
unsigned int mps_binsearch_section( const mps_ptr op, const mpfr_ptr item, unsigned int low, unsigned int high )
{
	mpfr_ptr subarr;
	unsigned int mid;
    const unsigned int N = high;

	while ( low < high )
	{
		mid = (high + low) >> 1;

		subarr = mps_get_ele_seq(op, mid);
		if ( mpfr_less_p(subarr, item) )
			low = mid + 1;
		else
			high = mid;
	}

	subarr = mps_get_ele_seq(op, low);
	if ( low < N && mpfr_equal_p(subarr, item) )
		return low;

	return 0;	/* not found */
}

/* FIXME: This has some issues, particularly with values at ends...or does it? */
/* search for numbers between lower range (lr) to upper range (ur).
 * ret must have enough space for 2, which are used for getting the range */
void mps_binsearch_range_section( const mps_ptr op,
                                  const mpfr_ptr lr,
                                  const mpfr_ptr ur,
                                  const unsigned int floor,
                                  const unsigned int ceil,
                                  unsigned int* ret)
{
	mpfr_ptr subarr2, subarr = NULL;
	unsigned int low, high, mid = 0;

	MPS_ASSERT_MSG(floor <= ceil,
			"Attempting to search in invalid range int mps_binsearch_range_section()\n");

	low  = floor;
	high = ceil;

	/* find lower limit */
	while (low <= high)
	{
		mid = (high + low) >> 1;
		subarr = mps_get_ele_seq(op, mid);

		if ( mpfr_greaterequal_p(subarr, lr) )
			high = mid - 1;
		else
			low = mid + 1;
	}

	if ( mpfr_less_p(subarr, lr))	/* I think this should be unnecessary. I'm probably doing it wrong */
		++mid;						/* also seems not quite needed for finding upper...should be symmetryish? */

	if (mid > ceil)
	{
		ret[0] = 0;
	}
	else
	{
		/* mid 1 more than bottom. get adjacent equal values. I also think this should be unnecessary */
		while ( mid >= floor + 1 && mpfr_equal_p(subarr2 = mps_get_ele_seq(op, mid - 1), subarr) )
		{
			subarr = subarr2;
			--mid;
		}
		ret[0] = mid;
	}

	/* find upper limit */
	low = floor;
	high = ceil;
	while (low <= high)
	{
		mid = (high + low) >> 1;
		subarr = mps_get_ele_seq(op, mid);

		if ( mpfr_lessequal_p(subarr, ur) )
			low = mid + 1;
		else
			high = mid - 1;
	}

	if ( mpfr_less_p(subarr, lr) || mpfr_greater_p(subarr, ur))
		++mid;

	if ( mid > ceil )
		ret[1] = 0;
	else
	{
		while ( mid < floor && mpfr_equal_p( subarr2 = mps_get_ele_seq(op, mid + 1), subarr) )
		{
			subarr = subarr2;
			++mid;
		}
		ret[1] = mid;
	}

}

