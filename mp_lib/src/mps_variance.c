/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"


/* 
 * Gives the variance of all the elements in op 
 * using a corrected two-pass algorithm. 
 */
int mps_variance( mps_ptr rop, mps_ptr op, int norm )
{
    mpfr_ptr subrop, subop;
    mpfr_t S, CS, mean;
    unsigned int i;
    unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Result operand is not a scalar in mps_variance().\n" );

    MPS_ASSERT_MSG( norm > 0 && norm < 2,
        "Invalid norm parameter in mps_variance().\n" );

    MPS_ASSERT_MSG( norm != 0 || matsize > 1,
        "Division by zero in mps_variance()." );

    mpfr_init2( S, MPS_PREC(rop) );
    mpfr_init2( CS, MPS_PREC(rop) );
    mpfr_init2( mean, MPS_PREC(rop) );
    
    mpfr_set_zero( S, 1 );
    mpfr_set_zero( CS, 1 );

    subrop = mps_get_ele(rop, 1, 1);

    /* Calculating the mean. */
    mps_sum( rop, op );
	mpfr_div_ui( mean, subrop, matsize, GMP_RNDN );

    for ( i = 1; i <= matsize; i++ )
    {
        subop = mps_get_ele_col( op, i ); 

        mpfr_sub( subrop, subop, mean, GMP_RNDN );
        mpfr_add( CS, CS, subrop, GMP_RNDN );

        mpfr_sqr( subrop, subrop, GMP_RNDN );
        mpfr_add( S, S, subrop, GMP_RNDN );
    }

    mpfr_sqr( CS, CS, GMP_RNDN );
    mpfr_div_ui( CS, CS, matsize, GMP_RNDN );

    mpfr_sub( S, S, CS, GMP_RNDN );

    if( norm == 0 )
        mpfr_div_ui( subrop, S, matsize, GMP_RNDN );
    else
        mpfr_div_ui( subrop, S, matsize - 1, GMP_RNDN );

    mpfr_clear( S );
    mpfr_clear( CS );
    mpfr_clear( mean );

	return 0;
}


int mps_variancer( mps_ptr rop, mps_ptr op, int norm )
{
    mpfr_ptr subrop, subop;
    mpfr_t S, CS, tmp;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Result operand is of the wrong size in mps_variancer().\n" );

    MPS_ASSERT_MSG( norm > 0 && norm < 2,
        "Invalid norm parameter in mps_variancer().\n" );

    MPS_ASSERT_MSG( norm != 0 || MPS_NUMCOL(rop) > 1,
        "Division by zero in mps_variancer()." );

    mpfr_init2( S, MPS_PREC(rop) );
    mpfr_init2( CS, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );

    mps_meanr( rop, op );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        mpfr_set_zero( S, 1 );
        mpfr_set_zero( CS, 1 );

        subrop = mps_get_ele_seq( rop, i );

        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            mpfr_sub( tmp, subop, subrop, GMP_RNDN );
            mpfr_add( CS, CS, tmp, GMP_RNDN );

            mpfr_sqr( tmp, tmp, GMP_RNDN );
            mpfr_add( S, S, tmp, GMP_RNDN );
        }

        mpfr_sqr( CS, CS, GMP_RNDN );
        mpfr_div_ui( CS, CS, MPS_NUMCOL(op), GMP_RNDN );

        mpfr_sub( S, S, CS, GMP_RNDN );

        if( norm == 0 )
            mpfr_div_ui( subrop, S, MPS_NUMCOL(op), GMP_RNDN );
        else
            mpfr_div_ui( subrop, S, MPS_NUMCOL(op) - 1, GMP_RNDN );

    }

    mpfr_clear( S );
    mpfr_clear( CS );
    mpfr_clear( tmp );

	return 0;
}


int mps_variancec( mps_ptr rop, mps_ptr op, int norm )
{
    mpfr_ptr subrop, subop;
    mpfr_t S, CS, tmp;
    unsigned int i, j;

    MPS_ASSERT_MSG( MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Result operand is of the wrong size in mps_variancec().\n" );

    MPS_ASSERT_MSG( norm > 0 && norm < 2,
        "Invalid norm parameter in mps_variancec().\n" );

    MPS_ASSERT_MSG( norm != 0 || MPS_NUMROW(rop) > 1,
        "Division by zero in mps_variancec()." );

    mpfr_init2( S, MPS_PREC(rop) );
    mpfr_init2( CS, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );

    mps_meanc( rop, op );

    for( i = 1; i <= MPS_NUMCOL(op); i++ )
    {
        mpfr_set_zero( S, 1 );
        mpfr_set_zero( CS, 1 );

        subrop = mps_get_ele_seq( rop, i );

        for( j = 1; j <= MPS_NUMROW(op); j++ )
        {
            subop = mps_get_ele( op, j, i );

            mpfr_sub( tmp, subop, subrop, GMP_RNDN );
            mpfr_add( CS, CS, tmp, GMP_RNDN );

            mpfr_sqr( tmp, tmp, GMP_RNDN );
            mpfr_add( S, S, tmp, GMP_RNDN );
        }

        mpfr_sqr( CS, CS, GMP_RNDN );
        mpfr_div_ui( CS, CS, MPS_NUMROW(op), GMP_RNDN );

        mpfr_sub( S, S, CS, GMP_RNDN );

        if( norm == 0 )
            mpfr_div_ui( subrop, S, MPS_NUMROW(op), GMP_RNDN );
        else
            mpfr_div_ui( subrop, S, MPS_NUMROW(op) - 1, GMP_RNDN );

    }

    mpfr_clear( S );
    mpfr_clear( CS );
    mpfr_clear( tmp );

	return 0;
}


#if 0
/* Gives the variance of all the elements in op */
int mps_variance( mpfr_ptr rop, mps_ptr op, mpfr_rnd_t rnd )
{
    mpfr_ptr subop;
	mpfr_t mean, tmp;		/* this could be avoided... */
    unsigned int i, matsize = MPS_SIZE(op);

	mpfr_inits2(MPS_PREC(op), mean, tmp, (mpfr_ptr) NULL);
//	mps_mean( mean, op );

	mpfr_set_ui(rop, 0, rnd);
    for( i = 1; i <= matsize; ++i )
	{
		subop = mps_get_ele_col(op, i);
		mpfr_sub(tmp, subop, mean, rnd);
		mpfr_sqr(tmp, tmp, rnd);
		mpfr_add(rop, rop, tmp, rnd);
	}

	mpfr_clears(mean, tmp, (mpfr_ptr) NULL);
	mpfr_div_ui(rop, rop, --matsize, rnd);

	return 0;
}
#endif

