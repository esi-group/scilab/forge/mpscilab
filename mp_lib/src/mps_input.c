/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <string.h>
#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * Very simple matrix input. For debugging only.
 */
int mps_str_input( const mps_ptr rop, const char* str, const mpfr_prec_t prec )
{
    char* substr;
    char* strc = malloc( strlen(str) + 1 );
    unsigned int i, rowcount = 0, colcount = 0;
    mpfr_ptr subrop;

    strcpy( strc, str );

    /* Count the number of rows. */
    substr = strchr( strc, ';' );
    while ( substr != NULL )
    {
        ++rowcount;
        substr = strchr( substr + 1, ';' );
    }

    /* Count the number of columns. */
    substr = strtok( strc, ";" );
    substr = strtok( substr, " " );
    while ( substr != NULL )
    {
        ++colcount;
        substr = strtok( NULL, " " );
    }

	MPS_ASSERT_MSG( MPS_NUMCOL(rop) == colcount && MPS_NUMROW(rop) == rowcount,
					"Mismatched destination size in mps_str_input()\n");

    mps_set( rop, rowcount, colcount, prec, MPS_COL_ORDER );

    strcpy( strc, str );
    substr = strtok( strc, "; " );

    for ( i = 1; i <= colcount*rowcount; i++ )
    {
        if ( substr == NULL )
        {
            MPS_WARNING_MSG("Malformed matrix intput in mps_str_input()\n");
            free( strc );
            return 1;
        }

        subrop = mps_get_ele_row( rop, i );

        if ( mpfr_set_str( subrop, substr, 10, GMP_RNDN ) != 0 )
        {
            MPS_WARNING_MSG("Invalid number in mps_str_input()\n");
            free( strc );
            return 1;
        }

        substr = strtok( NULL, "; " );
    }

    free( strc );

    return 0;
}

/*
 * Set the content of rop from the values in op.  The size of the double matrix
 * is assumed to be the same as the rop matrix. The third argument specify in what
 * order the matrix of double should be read.
 */
int mps_double_input( const mps_ptr rop,
                      const double* op,
                      const mps_order_t order,
                      const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

	if ( order == MPS_COL_ORDER )
	{
		for ( i = 1; i <= matsize; ++i )
		{
			subrop = mps_get_ele_col( rop, i );
			mpfr_set_d( subrop, op[i-1], rnd );
		}
	}
	else
	{
		for ( i = 1; i <= matsize; ++i )
		{
			subrop = mps_get_ele_row( rop, i );
			mpfr_set_d( subrop, op[i-1], rnd );
		}
	}

    return 0;
}

