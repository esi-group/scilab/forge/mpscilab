/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* ceil each element in op1 */
int mps_ceil( const mps_ptr rop, const mps_ptr op)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_ceil()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_ceil(subrop, subop);
    }

    return 0;
}


/* floor each element in op */
int mps_floor( const mps_ptr rop, const mps_ptr op)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_floor()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_floor(subrop, subop);
    }

    return 0;
}


/* round each element in op */
int mps_round( const mps_ptr rop, const mps_ptr op)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_round()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_round( subrop, subop);
    }

    return 0;
}


/* drops decimal component of each element in op */
int mps_trunc( const mps_ptr rop, const mps_ptr op)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_trunc()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_trunc(subrop, subop);
    }

    return 0;
}


/* rounds to nearest representable integer given rounding mode rnd */
int mps_rint( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rint()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rint(subrop, subop, rnd);
    }

    return 0;
}


/* set rop to op rounded to an integer, next higher or equal*/
int mps_rint_ceil( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rint_ceil()\n" );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rint_ceil(subrop, subop, rnd);
    }

    return 0;
}


int mps_rint_floor( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rint_floor()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rint_ceil(subrop, subop, rnd);
    }

    return 0;
}


int mps_rint_round( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rint_round()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rint_round(subrop, subop, rnd);
    }

    return 0;
}


int mps_rint_trunc( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_rint_trunc()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_rint_trunc(subrop, subop, rnd);
    }

    return 0;
}


/* take the decimal part of op */
int mps_frac( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_frac()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_frac(subrop, subop, rnd);
    }

    return 0;
}


/* get 2 matrices, 1 for integer part, another for fractional in  */
int mps_modf( const mps_ptr irop, const mps_ptr frop, const mps_ptr op, const mpfr_rnd_t rnd)
{
    mpfr_ptr subirop, subfrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(irop,op),
        "Result operand  1 differs in dimension in mps_modf()\n" );
    MPS_ASSERT_MSG( MPS_SAME_SIZE(frop,op),
        "Result operand 2 differs in dimension in mps_modf()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subirop = mps_get_ele_col( irop, i );
        subfrop = mps_get_ele_col( frop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_modf(subirop, subfrop, subop, rnd);
    }

    return 0;
}


/* Integer part of op. */
int mps_int( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_int()\n" );

    for ( i = 1; i <= matsize; i++ )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_trunc( subrop, subop );
    }

    return 0;
}
