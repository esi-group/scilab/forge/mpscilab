/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include "mps_priv.h"
#include "mps_assert.h"

/* Matrix invertion using the Gauss-Jordan method without pivoting. */
int mps_inv( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop1, subop2;
    mpfr_t mji;
    unsigned int i, j, p;
	const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_inv()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_inv()\n" );

    MPS_ASSERT_MSG( MPS_NOT_ALIAS(op,rop),
        "Operand alias the output operand in mps_inv()\n" );

    mpfr_init2( mji, MPS_PREC(op) );
    mps_identity( rop, GMP_RNDN );

    for ( i = 1; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( op, i, i );

        if ( mpfr_zero_p( subop1 ) )
        {
            for ( p = i + 1; p <= numrow; p++ )
            {
                subop1 = mps_get_ele( op, p, i );
                if ( !mpfr_zero_p( subop1 ) )
                    break;
            }

            if ( mpfr_zero_p( subop1 ) )
            {
                mpfr_clear( mji );
                return 1;
            }

            mps_row_exg( op, p, i );
            mps_row_exg( rop, p, i );
            subop1 = mps_get_ele( op, i, i );
        }

        for ( j = 1; j <= numrow; ++j )
        {
            if ( j != i )
            {
                subop2 = mps_get_ele( op, j, i );
                mpfr_div( mji, subop2, subop1, GMP_RNDN );

                mps_row_sub_mpfr( op, j, i, mji, GMP_RNDN );
                mps_row_sub_mpfr( rop, j, i, mji, GMP_RNDN );
            }
        }
    }

    /* Normalize the main diagonal. */
    for ( j = 1; j <= numrow; ++j )
    {
        subop2 = mps_get_ele( op, j, j );

        mps_row_div_mpfr( rop, j, subop2, GMP_RNDN );
        mps_row_div_mpfr( op, j, subop2, GMP_RNDN );
    }

    mpfr_clear( mji );

    return 0;
}

/* Matrix invertion using the Gauss-Jordan method with partial pivoting. */
/* FIXME not finished. */
int mps_inv2( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subop1, subop2;
    mpfr_t mji;
    unsigned int newpivot, p;
    unsigned int i, j;
	const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_inv2()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_inv2()\n" );

    MPS_ASSERT_MSG( MPS_NOT_ALIAS(op,rop),
        "Operand alias the output operand in mps_inv2()\n" );

    mpfr_init2( mji, MPS_PREC(op) );
    mps_identity( rop, GMP_RNDN );

    for ( i = 1; i <= numrow; i++ )
    {
        subop1 = mps_get_ele( op, i, i );
        newpivot = i;

        for ( p = i + 1 ; p <= numrow; p++ )
        {
            subop2 = mps_get_ele( op, p, i );
            if ( mpfr_cmpabs( subop2, subop1 ) > 0 )
            {
                subop1 = subop2;
                newpivot = p;
            }
        }

        if ( mpfr_zero_p( subop1 ) )
        {
            mpfr_clear( mji );
            return 1;
        }

        if ( newpivot != i )
        {
            mps_row_exg( op, newpivot, i );
            mps_row_exg( rop, newpivot, i );
            subop1 = mps_get_ele( op, i, i );
        }

        for ( j = 1; j <= numrow; ++j )
        {
            if ( j != i )
            {
                subop2 = mps_get_ele( op, j, i );
                mpfr_div( mji, subop2, subop1, GMP_RNDN );

                mps_row_sub_mpfr( op, j, i, mji, GMP_RNDN );
                mps_row_sub_mpfr( rop, j, i, mji, GMP_RNDN );
            }
        }
    }

    /* Normalize the main diagonal. */
    for ( j = 1; j <= numrow; ++j )
    {
        subop2 = mps_get_ele( op, j, j );

        mps_row_div_mpfr( rop, j, subop2, GMP_RNDN );
        mps_row_div_mpfr( op, j, subop2, GMP_RNDN );
    }

    mpfr_clear( mji );

    return 0;
}

/*
 * In-place inversion using the Gauss-Jordan elimination without pivoting.
 * REF :
 * Quintana, E.S., G. Quintana, X. Sun & R. van de Geijn, 
 * "Efficient matrix inversion via Gauss-Jordan elimination and its 
 * parallelization", PLAPACK Working Note #8, TR-98-19, 
 * Department of Computer Sciences, University of Texas
 * (August 1998, 18 pages, WN#40)
 * Available from :
 * http://www.cs.utexas.edu/users/plapack/papers/inverse-tr.ps
 * 
 * Number of operations as implemented :
 * DIV : ((k-1) + (k-1)) * k = 2k^2 - 2k
 * MUL : (k-1) * (k-1) * k = k^3 - 2k^2 + k
 * ADD : (k-1) * (k-1) * k = k^3 - 2k^2 + k
 *
 */
int mps_inv_ipgje( const mps_ptr op )
{
    mpfr_ptr subop, subopi, subopj;
    mpfr_t pivot;
    unsigned int i, j, k;
	const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_inv_ipgje()\n" );

    mpfr_init2( pivot, MPS_PREC(op) );

    for( k = 1; k <= numrow; k++ )
    {
        mpfr_set( pivot, mps_get_ele(op,k,k), GMP_RNDN );
        mps_set_ele_zero( op, k, k );

        if( mpfr_zero_p( pivot ) )
        {
            mpfr_clear( pivot );
            return 1;
        }

        /* Column scaling. */
        mpfr_neg( pivot, pivot, GMP_RNDN );
        mps_col_div_mpfr( op, k, pivot, GMP_RNDN );

        /* Rank-1 update. */
        for( i = 1; i <= numrow; i++ )
        {
            if( i == k )
                continue;

            for( j = 1; j<= numrow; j++ )
            {
                if( j == k )
                    continue;

                subopi = mps_get_ele( op, i, k );
                subopj = mps_get_ele( op, k, j );
                subop = mps_get_ele( op, i, j );
                
                mpfr_fma( subop, subopi, subopj, subop, GMP_RNDN );
            }
        }

        /* Row scaling. */
        mpfr_neg( pivot, pivot, GMP_RNDN );
        mps_set_ele_double( op, k, k, 1, GMP_RNDN );
        mps_row_div_mpfr( op, k, pivot, GMP_RNDN );
    }

    mpfr_clear( pivot );

    return 0;
}

/*
 * In-place inversion using the Gauss-Jordan elimination with partial pivoting.
 * REF :
 * Quintana, E.S., G. Quintana, X. Sun & R. van de Geijn, 
 * "Efficient matrix inversion via Gauss-Jordan elimination and its 
 * parallelization", PLAPACK Working Note #8, TR-98-19, 
 * Department of Computer Sciences, University of Texas
 * (August 1998, 18 pages, WN#40)
 * Available from :
 * http://www.cs.utexas.edu/users/plapack/papers/inverse-tr.ps
 * 
 * Number of operations as implemented :
 * DIV : ((k-1) + (k-1)) * k = 2k^2 - 2k
 * MUL : (k-1) * (k-1) * k = k^3 - 2k^2 + k
 * ADD : (k-1) * (k-1) * k = k^3 - 2k^2 + k
 *
 */
int mps_inv_ipgjep( const mps_ptr op )
{
    mpfr_ptr subop, subopi, subopj, newpivot;
    mpfr_t pivot;
    unsigned int kbar;
    unsigned int i, j, k;
    unsigned int *P;
	const unsigned int numrow = MPS_NUMROW(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_inv_ipgjep()\n" );

    mpfr_init2( pivot, MPS_PREC(op) );
    P = malloc( sizeof(unsigned int) * (numrow + 1) );
    for( k = 1; k <= numrow; k++ )
        P[k] = k;

    for( k = 1; k <= numrow; k++ )
    {

        /* Partial pivoting. */
        kbar = k;
        newpivot = mps_get_ele( op, k, k );
        for( i = k+1; i <= numrow; i++ )
        {
            subop = mps_get_ele( op, i, k );
            if( mpfr_cmpabs( subop, newpivot ) > 0 )
            {
                newpivot = subop;
                kbar = i;
            }
        }

        if( mpfr_zero_p( pivot ) )
        {
            mpfr_clear( pivot );
            free( P );
            return 1;
        }

        mpfr_set( pivot, newpivot, GMP_RNDN );

        if( kbar != k )
        {
            P[k] = kbar;
            mps_row_exg( op, k, kbar );
        }

        mps_set_ele_zero( op, k, k );

        /* Column scaling. */
        mpfr_neg( pivot, pivot, GMP_RNDN );
        mps_col_div_mpfr( op, k, pivot, GMP_RNDN );

        /* Rank-1 update. */
        for( i = 1; i <= numrow; i++ )
        {
            if( i == k )
                continue;

            for( j = 1; j<= numrow; j++ )
            {
                if( j == k )
                    continue;

                subopi = mps_get_ele( op, i, k );
                subopj = mps_get_ele( op, k, j );
                subop = mps_get_ele( op, i, j );
                
                mpfr_fma( subop, subopi, subopj, subop, GMP_RNDN );
            }
        }

        /* Row scaling. */
        mpfr_neg( pivot, pivot, GMP_RNDN );
        mps_set_ele_double( op, k, k, 1, GMP_RNDN );
        mps_row_div_mpfr( op, k, pivot, GMP_RNDN );
    }

    /* Backward permutation. */
    for( k = numrow; k >= 1; k-- )
    {
        if( P[k] == k )
            continue;
        else
            mps_col_exg( op, k, P[k] );
    }

    mpfr_clear( pivot );
    free( P );

    return 0;
}
