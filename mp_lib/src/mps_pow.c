/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

int mps_pow( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_pow()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow()\n" );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_pow( subrop, subop1, subop2, rnd );
    }

    return 0;
}


/* Raise each element in op1 to the power op2 */
int mps_pow_scalar( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow_scalar()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op2),
        "op2 not a scalar in mps_pow_scalar()\n" );

    subop2 = mps_get_ele( op2, 1, 1 );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_pow( subrop, subop1, subop2, rnd );
    }

    return 0;
}


int mps_scalar_pow( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_scalar_pow()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op1),
        "op2 not a scalar in mps_scalar_pow()\n" );

    subop1 = mps_get_ele( op1, 1, 1 );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_pow( subrop, subop1, subop2, rnd );
    }

    return 0;
}


/* Raise each element in op1 to the power op2 */
int mps_pow_ui_mpfr( const mps_ptr rop, const mps_ptr op1, const unsigned int op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow_ui_mpfr()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_pow_ui( subrop, subop1, op2, rnd );
    }

    return 0;
}


/* Raise each element in op1 to the power op2 */
int mps_ui_pow( const mps_ptr rop, const unsigned int op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_ui_pow()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_ui_pow( subrop, op1, subop2, rnd );
    }

    return 0;
}


/* Raise each element in op1 to the power op2 */
int mps_pow_si( const mps_ptr rop, const mps_ptr op1, const long int op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow_si()\n" );

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_pow_si( subrop, subop1, op2, rnd );
    }

    return 0;
}


int mps_pow_double( const mps_ptr rop,
                    const mps_ptr op1,
                    const double op2[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow_double()\n" );

    mpfr_init2( tmp, 53 );

	for( i = 1; i <= matsize; ++i )
	{
        if( order == MPS_COL_ORDER )
	    {
		    subrop = mps_get_ele_col( rop, i );
		    subop1 = mps_get_ele_col( op1, i );
        }
        else
        {
			subrop = mps_get_ele_row( rop, i );
			subop1 = mps_get_ele_row( op1, i );
        }

        mpfr_set_d( tmp, op2[i-1], GMP_RNDN );

		mpfr_pow( subrop, subop1, tmp, rnd );
	}

    mpfr_clear( tmp );

    return 0;
}


int mps_double_pow( const mps_ptr rop,
                    const double op1[],
                    const mps_ptr op2,
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);
    mpfr_t tmp;


    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_double_pow()\n" );

    mpfr_init2( tmp, 53 );

	for( i = 1; i <= matsize; ++i )
	{
	    if( order == MPS_COL_ORDER )
	    {
		    subrop = mps_get_ele_col( rop, i );
		    subop2 = mps_get_ele_col( op2, i );
        }
        else
        {
			subrop = mps_get_ele_row( rop, i );
			subop2 = mps_get_ele_row( op2, i );
        }

        mpfr_set_d( tmp, op1[i-1], GMP_RNDN );

		mpfr_pow( subrop, tmp, subop2, rnd );
	}

    mpfr_clear( tmp );

    return 0;
}


int mps_double_pow_double( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order1,
                           const double op2[],
                           const mps_order_t order2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    unsigned int j;
    const unsigned int matsize = MPS_SIZE(rop);
    mpfr_t tmp;

    mpfr_init2( tmp, 53 );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_set_d( subrop, op1[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op1[j-1], rnd);
        }

        if( order2 == MPS_COL_ORDER )
        {
            mpfr_set_d( tmp, op2[i-1], GMP_RNDN );
            mpfr_pow( subrop, subrop, tmp, rnd);
        }
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( tmp, op2[j-1], GMP_RNDN );
            mpfr_pow( subrop, subrop, tmp, rnd);
        }
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_pow_scalar_double( const mps_ptr rop, const mps_ptr op1, const double op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(op1);


    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_pow_scalar_double()\n" );

    mpfr_init2( tmp, 53 );
    mpfr_set_d( tmp, op2, GMP_RNDN );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_pow( subrop, subop1, tmp, rnd );
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_scalar_double_pow( const mps_ptr rop, const double op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(op2);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_scalar_double_pow()\n" );

    mpfr_init2( tmp, 53 );
    mpfr_set_d( tmp, op1, GMP_RNDN );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_pow( subrop, tmp, subop2, rnd );
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_scalar_pow_double( const mps_ptr rop,
                           const mps_ptr op1,
                           const double op2[],
                           const mps_order_t order,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op1),
        "op1 not a scalar in mps_scalar_pow_double()\n" );

    subop1 = mps_get_ele( op1, 1, 1 );

    mpfr_init2( tmp, 53 );

    for( i = 1; i <= matsize; i++ )
    {
        if( order == MPS_COL_ORDER )
            subrop = mps_get_ele_col( rop, i );
        else
            subrop = mps_get_ele_row( rop, i );

        mpfr_set_d( tmp, op2[i-1], GMP_RNDN );
        mpfr_pow( subrop, subop1, tmp, rnd );
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_double_pow_scalar( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order,
                           const mps_ptr op2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op2),
        "op2 not a scalar in mps_double_pow_scalar()\n" );

    mpfr_init2( tmp, 53 );

    subop2 = mps_get_ele( op2, 1, 1 );

    for( i = 1; i <= matsize; i++ )
    {
        if( order == MPS_COL_ORDER )
            subrop = mps_get_ele_col( rop, i );
        else
            subrop = mps_get_ele_col( rop, i );

        mpfr_set_d( tmp, op1[i-1], GMP_RNDN );
        mpfr_pow( subrop, tmp, subop2, rnd );
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_scalar_double_pow_double( const mps_ptr rop,
                                  const double op1,
                                  const double op2[],
                                  const mps_order_t order2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(rop);

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op1, rnd );

    mpfr_init2( tmp, 53 );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order2 == MPS_COL_ORDER )
        {
            mpfr_set_d( tmp, op2[i-1], GMP_RNDN );
            mpfr_pow( subrop, scalarop, tmp, rnd);
        }
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( tmp, op2[j-1], GMP_RNDN );
            mpfr_pow( subrop, scalarop, tmp, rnd);
        }
    }

    mpfr_clear( tmp );

    return 0;
}


int mps_double_pow_scalar_double( const mps_ptr rop,
                                  const double op1[],
                                  const mps_order_t order1,
                                  const double op2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    mpfr_t tmp;
    const unsigned int matsize = MPS_SIZE(rop);

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op2, rnd );

    mpfr_init2( tmp, 53 );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
        {
            mpfr_set_d( tmp, op1[i-1], GMP_RNDN );
            mpfr_pow( subrop, tmp,  scalarop, rnd);
        }
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( tmp, op1[j-1], GMP_RNDN );
            mpfr_pow( subrop, tmp,  scalarop, rnd);
        }
    }

    mpfr_clear( tmp );

    return 0;
}

