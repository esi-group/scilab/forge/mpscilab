/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

#if 0
/* finds eigenvalues of op */
int mps_eigenvalues( const mps_ptr op )
{
    MPS_ASSERT_MSG( MPS_IS_SQUARE(op), "Expected square matrix in mps_eigenvalues()");


    return 0;
}
#endif

