/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

static int prod( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mpfr_ptr subop1;
    unsigned int i;
    const unsigned int numrow = MPS_NUMROW(mps);

    subop1 = mps_get_ele( mps, 1, 1 );
    mpfr_set( mpfr, subop1 , GMP_RNDN );

    for ( i = 2; i <= numrow; ++i )
    {
        subop1 = mps_get_ele( mps, i, i );
        mpfr_mul( mpfr, mpfr, subop1, GMP_RNDN );
    }

    return 0;
}

int mps_det_gauss( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mps_t temp;

    mps_init( temp, 1, 1, 2, MPS_COL_ORDER );

    mps_clone( temp, mps );
    mps_det_gauss_dest( mpfr, temp );

    mps_free( temp );
    return 0;
}

int mps_det_gauss2( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mps_t temp;

    mps_init( temp, 1, 1, 2, MPS_COL_ORDER );

    mps_clone( temp, mps );
    mps_det_gauss2_dest( mpfr, temp );

    mps_free( temp );
    return 0;
}

int mps_det_gauss3( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mps_t temp;

    mps_init( temp, 1, 1, 2, MPS_COL_ORDER );

    mps_clone( temp, mps );
    mps_det_gauss3_dest( mpfr, temp );

    mps_free( temp );
    return 0;
}

/* Calculate the determinant using the Gaussian elimination method. */
int mps_det_gauss_dest( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mpfr_ptr subop1, subop2;
    mpfr_t mji;
    int sign = 1;
    unsigned int i, p, j;
    const unsigned int numrow = MPS_NUMROW(mps);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(mps),
        "Input matrix not square in mps_det_gauss_dest()\n" );

    mpfr_init2( mji, MPS_PREC(mps) );

    for ( i = 1; i < numrow; i++ )
    {
        subop1 = mps_get_ele( mps, i, i );

        if ( mpfr_zero_p( subop1 ) )
        {
            for ( p = i + 1; p <= numrow; p++ )
            {
                subop1 = mps_get_ele( mps, p, i );
                if ( !mpfr_zero_p( subop1 ) )
                    break;
            }

            if ( mpfr_zero_p( subop1 ) )
            {
                mpfr_set_d( mpfr, 0, GMP_RNDN );
                mpfr_clear( mji );
                return 0;
            }

            mps_row_exg( mps, p, i );
            subop1 = mps_get_ele( mps, i, i );
            sign = sign * -1;
        }

        for ( j = i + 1; j <= numrow; j++ )
        {
            subop2 = mps_get_ele( mps, j, i );
            mpfr_div( mji, subop2, subop1, GMP_RNDN );

            mps_row_sub_mpfr( mps, j, i, mji, GMP_RNDN );
        }
    }

    /* Evaluate the determinant. */
    prod( mpfr, mps );

    if ( sign < 0 )
        mpfr_neg( mpfr, mpfr, GMP_RNDN );

    mpfr_clear( mji );

    return 0;
}

/* Calculate the determinant using the Gaussian elimination method with partial pivoting */
int mps_det_gauss2_dest( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mpfr_ptr subop1, subop2;
    mpfr_t mji;
    unsigned int i, p, j;
    unsigned int newpivot;
    int sign = 1;

	const unsigned int numrow = MPS_NUMROW(mps);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(mps),
        "Input matrix not square in mps_det_gauss2_dest()\n" );

    mpfr_init2( mji, MPS_PREC(mps) );

    for ( i = 1; i < numrow; ++i )
    {
        subop1 = mps_get_ele( mps, i, i );
        newpivot = i;

        for ( p = i + 1; p <= numrow; ++p )
        {
            subop2 = mps_get_ele( mps, p, i );
            if ( mpfr_cmpabs( subop2, subop1 ) > 0 )
            {
                subop1 = subop2;
                newpivot = p;
            }
        }

        if ( mpfr_zero_p( subop1 ) )
        {
            mpfr_set_d( mpfr, 0, GMP_RNDN );
            mpfr_clear( mji );
            return 0;
        }

        if ( newpivot != i )
        {
            mps_row_exg( mps, newpivot, i );
            subop1 = mps_get_ele( mps, i, i );
            sign = sign * -1;
        }

        for ( j = i + 1; j <= numrow; ++j )
        {
            subop2 = mps_get_ele( mps, j, i );
            mpfr_div( mji, subop2, subop1, GMP_RNDN );

            mps_row_sub_mpfr( mps, j, i, mji, GMP_RNDN );
        }
    }

    /* Evaluate the determinant. */
    prod( mpfr, mps );

    if ( sign < 0 )
        mpfr_neg( mpfr, mpfr, GMP_RNDN );

    mpfr_clear( mji );

    return 0;
}

/* Calculate the determinant using the Gaussian elimination method with full pivoting */
int mps_det_gauss3_dest( const mpfr_ptr mpfr, const mps_ptr mps )
{
    mpfr_ptr subop1, subop2;
    mpfr_t mji;
    unsigned int p, q, i, j;
    unsigned int newpivotp, newpivotq;
    int sign = 1;
	const unsigned int numrow = MPS_NUMROW(mps);
    const unsigned int numcol = MPS_NUMCOL(mps);


    MPS_ASSERT_MSG( MPS_IS_SQUARE(mps),
        "Input matrix not square in mps_det_gauss3_dest()\n" );

    mpfr_init2( mji, MPS_PREC(mps) );

    for ( i = 1; i < numrow; ++i )
    {
        subop1 = mps_get_ele( mps, i, i );
        newpivotp = i;
        newpivotq = i;

        for ( p = i; p <= numrow; ++p )
        {
            for ( q = i; q <= numcol; ++q )
            {
                subop2 = mps_get_ele( mps, p, q );
                if ( mpfr_cmpabs( subop2, subop1 ) > 0 )
                {
                    subop1 = subop2;
                    newpivotp = p;
                    newpivotq = q;
                }
            }
        }

        if ( mpfr_zero_p( subop1 ) )
        {
            mpfr_set_d( mpfr, 0, GMP_RNDN );
            mpfr_clear( mji );
            return 0;
        }

        /* Exchange rows if necessary. */
        if ( newpivotp != i )
        {
            mps_row_exg( mps, newpivotp, i );
            sign = sign * -1;
        }

        /* Exchange columns if necessary. */
        if ( newpivotq != i )
        {
            mps_col_exg( mps, newpivotq, i );
            sign = sign * -1;
        }

        /* Load the pivot location again. */
        subop1 = mps_get_ele( mps, i, i );

        for ( j = i + 1; j <= numrow; ++j )
        {
            subop2 = mps_get_ele( mps, j, i );
            mpfr_div( mji, subop2, subop1, GMP_RNDN );

            mps_row_sub_mpfr( mps, j, i, mji, GMP_RNDN );
        }
    }

    /* Evaluate the determinant. */
    prod( mpfr, mps );

    if ( sign < 0 )
        mpfr_neg( mpfr, mpfr, GMP_RNDN );

    mpfr_clear( mji );

    return 0;
}


/* 
 * Evaluate the determinant of op using an in-place lu decomposition with
 * partial pivoting. 
 */
int mps_det_iplup( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subrop, subop, subop1, subop2, pivot;
    unsigned int i, j, k, l, kbar;
    int par = 1;
    mpfr_t mpfrtemp;
	const unsigned int numcol = MPS_NUMCOL(op);

    MPS_ASSERT_MSG( MPS_IS_SQUARE(op),
        "Input matrix not square in mps_det_iplup()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(rop),
        "Output not a scalar in mps_det_iplup()\n" );

    mpfr_init2( mpfrtemp, MPS_PREC(op) );

    for( k = 1; k < numcol; k++ )
    {
        kbar = k;
        pivot = mps_get_ele( op, k, k );

        /* Find pivot element. */
        for( i = k+1; i <= numcol; i++ )
        {
            subop = mps_get_ele( op, i, k );
            if( mpfr_cmpabs( subop, pivot ) > 0 )
            {
                pivot = subop;
                kbar = i;
            }
        }

        /* op is singular. */
        if( mpfr_zero_p( pivot ) )
        {
            subrop = mps_get_ele( rop, 1, 1 );
            mpfr_set_zero( subrop, 1 );
            mpfr_clear( mpfrtemp );

            return 0;
        }

        /* Row exchange. */
        if( kbar != k )
        {
            mps_row_exg( op, k, kbar );
            pivot = mps_get_ele( op, k, k );
            par = par * -1;
        }

        for( l = k+1; l <= numcol; l++ )
        {
            subop = mps_get_ele( op, l, k );
            mpfr_div( subop, subop, pivot, GMP_RNDN );
        }

        for( i = k+1; i <= numcol; i++ )
        {
            for( j = k+1; j <= numcol; j++ )
            {
                subop = mps_get_ele( op, i, j );
                subop1 = mps_get_ele( op, i, k );
                subop2 = mps_get_ele( op, k, j );

                mpfr_mul( mpfrtemp, subop1, subop2, GMP_RNDN );
                mpfr_sub( subop, subop, mpfrtemp, GMP_RNDN );
            }
        }

    }

    mpfr_clear( mpfrtemp );

    subrop = mps_get_ele( rop, 1, 1 );
    prod( subrop, op );

    if( par == -1  )
        mpfr_neg( subrop, subrop, GMP_RNDN );

    return 0;
}
