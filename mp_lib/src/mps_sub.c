/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Perform subtraction of an mpfr scalar from a real matrix */
int mps_sub_scalar( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_sub_scalar()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op2),
        "op2 not a scalar in mps_sub_scalar()\n" );

    subop2 = mps_get_ele( op2, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sub_scalar, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_sub( subrop, subop1, subop2, rnd );
    }

    return 0;
}

/* Partner to mps_sub_scalar, subtraction of a real matrix from an mpfr scalar since
 * subtraction isn't commutative */
int mps_scalar_sub( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_scalar_sub()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op1),
        "op2 not a scalar in mps_scalar_sub()\n" );

    subop1 = mps_get_ele( op1, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_scalar_sub, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop2) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; i++ )
    {
        subrop = mps_get_ele_col( rop, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_sub( subrop, subop1, subop2, rnd );
    }

    return 0;
}

/* Perform subtraction between a real matrix and a double scalar. */
int mps_sub_scalar_double( const mps_ptr rop, const mps_ptr op1, const double op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_sub_scalar_double()\n" );


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sub_scalar_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_sub_d( subrop, subop1, op2, rnd );
    }

    return 0;
}

/*
 * Perform subtraction between a double matrix and an mpfr scalar. Careful as
 * the dimension for the double matrix will be inferred from the result operand.
 */
int mps_scalar_sub_double( const mps_ptr rop,
                           const mps_ptr op1,
                           const double op2[],
                           const mps_order_t order,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op1),
        "op1 not a scalar in mps_scalar_sub_double()\n" );

    subop1 = mps_get_ele( op1, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_scalar_sub_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; i++ )
    {
        if( order == MPS_COL_ORDER )
            subrop = mps_get_ele_col( rop, i );
        else
            subrop = mps_get_ele_row( rop, i );

        mpfr_sub_d( subrop, subop1, op2[i-1], rnd );
    }

    return 0;
}

/* partner to mps_sub_scalar_double */
int mps_scalar_double_sub( const mps_ptr rop, const double op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_scalar_double_sub()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_scalar_double_sub, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop2) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_d_sub( subrop, op1, subop2, rnd );
    }

    return 0;
}

/* partner to mps_sub_double_scalar */
int mps_double_sub_scalar( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order,
                           const mps_ptr op2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op2),
        "op2 not a scalar in mps_double_sub_scalar()\n" );

    subop2 = mps_get_ele( op2, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_sub_scalar, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; i++ )
    {
        if( order == MPS_COL_ORDER )
            subrop = mps_get_ele_col( rop, i );
        else
            subrop = mps_get_ele_col( rop, i );

        mpfr_d_sub( subrop, op1[i-1], subop2, rnd );
    }

    return 0;
}

/*
 * Entry-wise matrix subtraction. op1 and op2 must be of the same dimension else
 * the result is undefined. (segfault)
 */
int mps_sub( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_sub()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_sub()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sub, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1, subop2) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_sub( subrop, subop1, subop2, rnd );
    }

    return 0;
}

/*
 * Entry-wise matrix subtraction between a multi-precision matrix and an array of
 * doubles. op1 and op2 must be of the same dimension else
 * the result is undefined. (segfault)
 */
int mps_sub_double( const mps_ptr rop,
                    const mps_ptr op1,
                    const double op2[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_sub_double()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_sub_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
            schedule(guided) num_threads(num_thr)
    #endif
	for( i = 1; i <= matsize; ++i )
	{
        if( order == MPS_COL_ORDER )
	    {
		    subrop = mps_get_ele_col( rop, i );
		    subop1 = mps_get_ele_col( op1, i );
        }
        else
        {
			subrop = mps_get_ele_row( rop, i );
			subop1 = mps_get_ele_row( op1, i );
        }

		mpfr_sub_d( subrop, subop1, op2[i-1], rnd );
	}

    return 0;
}

/* partner to mps_sub_d */
int mps_double_sub( const mps_ptr rop,
                    const double op1[],
                    const mps_ptr op2,
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op2);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_double_sub()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_sub, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop2) \
            schedule(guided) num_threads(num_thr)
    #endif
	for( i = 1; i <= matsize; ++i )
	{
	    if( order == MPS_COL_ORDER )
	    {
		    subrop = mps_get_ele_col( rop, i );
		    subop2 = mps_get_ele_col( op2, i );
        }
        else
        {
			subrop = mps_get_ele_row( rop, i );
			subop2 = mps_get_ele_row( op2, i );
        }

		mpfr_d_sub( subrop, op1[i-1], subop2, rnd );
	}

    return 0;
}

/*
 * Perform an subtraction between two matrix of double. The size is inferred by
 * taking the size of the output argument.
 */
int mps_double_sub_double( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order1,
                           const double op2[],
                           const mps_order_t order2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    unsigned int j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif
    

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_sub_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_set_d( subrop, op1[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op1[j-1], rnd);
        }

        if( order2 == MPS_COL_ORDER )
            mpfr_sub_d( subrop, subrop, op2[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_sub_d( subrop, subrop, op2[j-1], rnd);
        }
    }

    return 0;
}

/*
 * Perform an element-wise subtraction between a double scalar and a matrix of
 * double.
 */
#ifndef WITH_OPENMP
int mps_scalar_double_sub_double( const mps_ptr rop,
                                  const double op1,
                                  const double op2[],
                                  const mps_order_t order2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op1, rnd );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order2 == MPS_COL_ORDER )
            mpfr_sub_d( subrop, scalarop, op2[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_sub_d( subrop, scalarop, op2[j-1], rnd);
        }
    }

    return 0;
}
#else
int mps_scalar_double_sub_double( const mps_ptr rop,
                                  const double op1,
                                  const double op2[],
                                  const mps_order_t order2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op1, rnd );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_scalar_double_sub_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i < matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order2 == MPS_COL_ORDER )
            mpfr_sub_d( subrop, scalarop, op2[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_sub_d( subrop, scalarop, op2[j-1], rnd);
        }
    }

    /* The final entry must be done last to prevent overwriting the scalar early. */
    subrop = mps_get_ele_col( rop, matsize );

    if( order2 == MPS_COL_ORDER )
        mpfr_sub_d( subrop, scalarop, op2[matsize-1], rnd);
    else
    {
        j = ( ((matsize-1) / MPS_NUMROW(rop)) + \
			MPS_NUMCOL(rop) * ((matsize-1) % MPS_NUMROW(rop)) );

        mpfr_sub_d( subrop, scalarop, op2[j-1], rnd);
    }

    return 0;
}
#endif /* !WITH_OPENMP */

/*
 * Perform an element-wise subtraction between a double scalar and a matrix of
 * double.
 */
#ifndef WITH_OPENMP
int mps_double_sub_scalar_double( const mps_ptr rop,
                                  const double op1[],
                                  const mps_order_t order1,
                                  const double op2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op2, rnd );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_d_sub( subrop, op1[i-1],  scalarop, rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_d_sub( subrop, op1[j-1],  scalarop, rnd);
        }
    }

    return 0;
}
#else
int mps_double_sub_scalar_double( const mps_ptr rop,
                                  const double op1[],
                                  const mps_order_t order1,
                                  const double op2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    /*
     * To save some space we use the last element of the result matrix as a
     * remporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op2, rnd );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_sub_scalar_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i < matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_d_sub( subrop, op1[i-1],  scalarop, rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_d_sub( subrop, op1[j-1],  scalarop, rnd);
        }
    }

    subrop = mps_get_ele_col( rop, matsize );

    if( order1 == MPS_COL_ORDER )
        mpfr_d_sub( subrop, op1[matsize-1],  scalarop, rnd);
    else
    {
        j = ( ((matsize-1) / MPS_NUMROW(rop)) + \
			MPS_NUMCOL(rop) * ((matsize-1) % MPS_NUMROW(rop)) );

        mpfr_d_sub( subrop, op1[j-1],  scalarop, rnd);
    }

    return 0;
}
#endif /* !WITH_OPENMP */

