/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

 /* matrix multiplication. can't assign to one of the ops */
int mps_mat_mul( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2 )
{
    mpfr_ptr subrop;
    /* mpfr_t tmp; */
    unsigned int i, j, k;
	unsigned int numcol1, numcol2, numrow;

	numrow = MPS_NUMROW(op1);
	numcol1 = MPS_NUMCOL(op1);
	numcol2 = MPS_NUMCOL(op2);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_CAN_MULT(op1,op2),
        "Cannot multiply matrices in mps_mat_mul()\n" );
    MPS_ASSERT_MSG( MPS_NUMROW(rop) == MPS_NUMROW(op1)
        && MPS_NUMCOL(rop) == MPS_NUMCOL(op2),
        "Result operand wrong size in mps_mat_mult()\n");
    MPS_ASSERT_MSG( MPS_NOT_ALIAS(rop,op1),
        "Result operand alias the first argument in mps_mat_mul()\n" );
    MPS_ASSERT_MSG( MPS_NOT_ALIAS(rop,op2),
        "Result operand alias the second argument in mps_mat_mul()\n" );

	/* mpfr_init2(tmp, MPS_PREC(rop)); */
	/* mpfr_set_ui(tmp, 0, rnd); */

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mat_mul, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, k, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol2; ++j )
        {
            subrop = mps_get_ele( rop, i, j );
            mpfr_set_ui( subrop, 0, GMP_RNDN );
            for ( k = 1; k <= numcol1; ++k )
            {
                /* mpfr_mul( tmp, mps_get_ele(op1, i, k), mps_get_ele(op2, k, j), rnd ); */
                /* mpfr_add( subrop, subrop, tmp, rnd ); */
                mpfr_fma( subrop, mps_get_ele(op1, i, k), mps_get_ele(op2, k, j), subrop, GMP_RNDN );
            }
        }
    }

    /* mpfr_clear(tmp); */

    return 0;
}

int mps_mat_mul_double( const mps_ptr rop,
                        const mps_ptr op1,
                        const double op2[],
                        const unsigned int numcol2,
                        const mps_order_t order )
{
    mpfr_ptr subrop;
    mpfr_t tmp;
    unsigned int i, j, k, l;
	unsigned int numcol1, numrow;

	numrow = MPS_NUMROW(op1);
	numcol1 = MPS_NUMCOL(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

	/* TODO: Size check assertion */
    MPS_ASSERT_MSG( MPS_NUMROW(rop) == MPS_NUMROW(op1)
        && MPS_NUMCOL(rop) == numcol2,
        "Wrong size for the result operand in mps_mat_mul_double()\n");


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mat_mul_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel private(tmp) num_threads(num_thr)
        {
    #endif

        mpfr_init2( tmp, MPS_PREC(rop) );
        mpfr_set_ui( tmp, 0, GMP_RNDN );

#ifdef WITH_OPENMP
        #pragma omp for private(i, j, k, l, subrop) \
                schedule(guided)
    #endif
    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol2; ++j )
        {
            subrop = mps_get_ele( rop, i, j );
            mpfr_set_ui( subrop, 0, GMP_RNDN );
            for ( k = 1; k <= numcol1; ++k)
            {
                if( order == MPS_COL_ORDER )
                    l = (k-1) + numcol1*(j-1);
                else
                    l = (j-1) + numcol2*(k-1);

                mpfr_mul_d( tmp, mps_get_ele(op1, i, k), op2[l], GMP_RNDN );
                mpfr_add( subrop, subrop, tmp, GMP_RNDN );
            }
        }
    }

    mpfr_clear(tmp);
    #ifdef WITH_OPENMP
        }
    #endif

    return 0;
}

int mps_double_mat_mul( const mps_ptr rop,
                        const double op1[],
                        const unsigned int numrow,
                        const mps_order_t order,
                        const mps_ptr op2 )
{
    mpfr_ptr subrop;
    mpfr_t tmp;
    unsigned int i, j, k, l;
	unsigned int numcol1, numcol2;

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    numcol1 = MPS_NUMROW(op2);
	numcol2 = MPS_NUMCOL(op2);

	/* TODO: Size check assertion */
    MPS_ASSERT_MSG( MPS_NUMROW(rop) == numrow
        && MPS_NUMCOL(rop) == numcol2,
        "Wrong size for the result operand in mps_double_mat_mul()\n");

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_mat_mul, matsize, MPS_PREC(rop) );

        #pragma omp parallel private(tmp) num_threads(num_thr)
        {
    #endif

        mpfr_init2( tmp, MPS_PREC(rop) );
        mpfr_set_ui( tmp, 0, GMP_RNDN );

#ifdef WITH_OPENMP
        #pragma omp for private(i, j, k, l, subrop) \
                schedule(guided)
    #endif
    for ( i = 1; i <= numrow; ++i )
    {
        for ( j = 1; j <= numcol2; ++j )
        {
            subrop = mps_get_ele( rop, i, j );
            mpfr_set_ui( subrop, 0, GMP_RNDN );
            for ( k = 1; k <= numcol1; ++k)
            {
                if( order == MPS_COL_ORDER )
                    l = (i-1) + numrow*(k-1);
                else
                    l = (k-1) + numcol1*(i-1);
                mpfr_mul_d( tmp, mps_get_ele(op2, k, j), op1[l], GMP_RNDN );
                mpfr_add( subrop, subrop, tmp, GMP_RNDN );
            }
        }
    }

    mpfr_clear(tmp);
    #ifdef WITH_OPENMP
        }
    #endif

    return 0;
}
