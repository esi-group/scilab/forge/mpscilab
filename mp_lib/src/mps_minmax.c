/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"
#include <math.h>

/* Return the maximum value found in op and its position m, n. */
mpfr_ptr mps_max( const mps_ptr op, unsigned int *m, unsigned int *n )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    maxop = mps_get_ele( op, 1, 1 );
    *m = 1;
    *n = 1;

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 1; j<= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                *m = i;
                *n = j;
                continue;
            }
            
            if( mpfr_greater_p( subop, maxop ) )
            {
                maxop = subop;
                *m = i;
                *n = j;
            }
        }
    }

    return maxop;
}

/* Find the maximum of each row. */
int mps_maxr( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Operands differ in dimension in mps_maxr\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        maxop = mps_get_ele( op, i, 1 );
        m[i-1] = 1;

        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[i-1] = j;
                continue;
            }

            if( mpfr_greater_p( subop, maxop ) )
            {
                maxop = subop;
                m[i-1] = j;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, i ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the maximum of each column. */
int mps_maxc( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Operands differ in dimension in mps_maxc\n" );

    for( j = 1; j <= MPS_NUMCOL(op); j++ )
    {
        maxop = mps_get_ele( op, 1, j );
        m[j-1] = 1;

        for( i = 1; i <= MPS_NUMROW(op); i++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[j-1] = i;
                continue;
            }

            if( mpfr_greater_p( subop, maxop ) )
            {
                maxop = subop;
                m[j-1] = i;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, j ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the maximum entry of a row. */
mpfr_ptr mps_row_max( const mps_ptr op, unsigned int *m, const unsigned int row )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(op,row),
        "Row index out of bound in mps_row_max\n" );

    maxop = mps_get_ele( op, row, 1 );
    *m = 1;

    for( i = 2; i <= MPS_NUMCOL(op); i++ )
    {
        subop = mps_get_ele( op, row, i );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_greater_p( subop, maxop ) )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Find the maximum entry of a column. */
mpfr_ptr mps_col_max( const mps_ptr op, unsigned int *m, const unsigned int col )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_COL_BOUND(op,col),
        "Column index out of bound in mps_col_max\n" );

    maxop = mps_get_ele( op, 1, col );
    *m = 1;

    for( i = 2; i <= MPS_NUMROW(op); i++ )
    {
        subop = mps_get_ele( op, i, col );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_greater_p( subop, maxop ) )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Return the minimum value found in op and its position m, n. */
mpfr_ptr mps_min( const mps_ptr op, unsigned int *m, unsigned int *n )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    maxop = mps_get_ele( op, 1, 1 );
    *m = 1;
    *n = 1;

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 1; j<= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                *m = i;
                *n = j;
                continue;
            }
            
            if( mpfr_less_p( subop, maxop ) )
            {
                maxop = subop;
                *m = i;
                *n = j;
            }
        }
    }

    return maxop;
}

/* Find the minimum of each row. */
int mps_minr( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Operands differ in dimension in mps_minr\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        maxop = mps_get_ele( op, i, 1 );
        m[i-1] = 1;

        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[i-1] = j;
                continue;
            }

            if( mpfr_less_p( subop, maxop ) )
            {
                maxop = subop;
                m[i-1] = j;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, i ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the minimum of each column. */
int mps_minc( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Operands differ in dimension in mps_minc\n" );

    for( j = 1; j <= MPS_NUMCOL(op); j++ )
    {
        maxop = mps_get_ele( op, 1, j );
        m[j-1] = 1;

        for( i = 1; i <= MPS_NUMROW(op); i++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[j-1] = i;
                continue;
            }

            if( mpfr_less_p( subop, maxop ) )
            {
                maxop = subop;
                m[j-1] = i;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, j ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the minimum entry of a row. */
mpfr_ptr mps_row_min( const mps_ptr op, unsigned int *m, const unsigned int row )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(op,row),
        "Row index out of bound in mps_row_max\n" );

    maxop = mps_get_ele( op, row, 1 );
    *m = 1;

    for( i = 2; i <= MPS_NUMCOL(op); i++ )
    {
        subop = mps_get_ele( op, row, i );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_less_p( subop, maxop ) )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Find the minimum entry of a column. */
mpfr_ptr mps_col_min( const mps_ptr op, unsigned int *m, const unsigned int col )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_COL_BOUND(op,col),
        "Column index out of bound in mps_col_max\n" );

    maxop = mps_get_ele( op, 1, col );
    *m = 1;

    for( i = 2; i <= MPS_NUMROW(op); i++ )
    {
        subop = mps_get_ele( op, i, col );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_less_p( subop, maxop ) )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Return the absolute maximum value found in op and its position m, n. */
mpfr_ptr mps_maxabs( const mps_ptr op, unsigned int *m, unsigned int *n )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    maxop = mps_get_ele( op, 1, 1 );
    *m = 1;
    *n = 1;

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 1; j<= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                *m = i;
                *n = j;
                continue;
            }
            
            if( mpfr_cmpabs( subop, maxop ) > 0 )
            {
                maxop = subop;
                *m = i;
                *n = j;
            }
        }
    }

    return maxop;
}

/* Find the absolute maximum of each row. */
int mps_maxabsr( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Operands differ in dimension in mps_maxabsr\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        maxop = mps_get_ele( op, i, 1 );
        m[i-1] = 1;

        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[i-1] = j;
                continue;
            }

            if( mpfr_cmpabs( subop, maxop ) > 0 )
            {
                maxop = subop;
                m[i-1] = j;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, i ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the absolute maximum of each column. */
int mps_maxabsc( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Operands differ in dimension in mps_maxabsc\n" );

    for( j = 1; j <= MPS_NUMCOL(op); j++ )
    {
        maxop = mps_get_ele( op, 1, j );
        m[j-1] = 1;

        for( i = 1; i <= MPS_NUMROW(op); i++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[j-1] = i;
                continue;
            }

            if( mpfr_cmpabs( subop, maxop ) > 0 )
            {
                maxop = subop;
                m[j-1] = i;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, j ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the absolute maximum entry of a row. */
mpfr_ptr mps_row_maxabs( const mps_ptr op, unsigned int *m, const unsigned int row )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(op,row),
        "Row index out of bound in mps_row_maxabs\n" );

    maxop = mps_get_ele( op, row, 1 );
    *m = 1;

    for( i = 2; i <= MPS_NUMCOL(op); i++ )
    {
        subop = mps_get_ele( op, row, i );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_cmpabs( subop, maxop ) > 0 )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Find the absolute maximum entry of a column. */
mpfr_ptr mps_col_maxabs( const mps_ptr op, unsigned int *m, const unsigned int col )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_COL_BOUND(op,col),
        "Column index out of bound in mps_col_maxabs\n" );

    maxop = mps_get_ele( op, 1, col );
    *m = 1;

    for( i = 2; i <= MPS_NUMROW(op); i++ )
    {
        subop = mps_get_ele( op, i, col );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_cmpabs( subop, maxop ) > 0 )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Return the absolute minimum value found in op and its position m, n. */
mpfr_ptr mps_minabs( const mps_ptr op, unsigned int *m, unsigned int *n )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    maxop = mps_get_ele( op, 1, 1 );
    *m = 1;
    *n = 1;

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 1; j<= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                *m = i;
                *n = j;
                continue;
            }
            
            if( mpfr_cmpabs( subop, maxop ) < 0 )
            {
                maxop = subop;
                *m = i;
                *n = j;
            }
        }
    }

    return maxop;
}

/* Find the absolute minimum of each row. */
int mps_minabsr( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMROW(op),
        "Operands differ in dimension in mps_minabsr\n" );

    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        maxop = mps_get_ele( op, i, 1 );
        m[i-1] = 1;

        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[i-1] = j;
                continue;
            }

            if( mpfr_cmpabs( subop, maxop ) < 0 )
            {
                maxop = subop;
                m[i-1] = j;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, i ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the absolute minimum of each column. */
int mps_minabsc( const mps_ptr rop, const mps_ptr op, unsigned int m[] )
{
    mpfr_ptr subop, maxop;
    unsigned int i, j;

    MPS_ASSERT_MSG( rop == NULL || MPS_NUMROW(rop)*MPS_NUMCOL(rop) == MPS_NUMCOL(op),
        "Operands differ in dimension in mps_minabsc\n" );

    for( j = 1; j <= MPS_NUMCOL(op); j++ )
    {
        maxop = mps_get_ele( op, 1, j );
        m[j-1] = 1;

        for( i = 1; i <= MPS_NUMROW(op); i++ )
        {
            subop = mps_get_ele( op, i, j );

            if( mpfr_nan_p(subop) )
                continue;

            if( mpfr_nan_p(maxop) )
            {
                maxop = subop;
                m[j-1] = i;
                continue;
            }

            if( mpfr_cmpabs( subop, maxop ) < 0 )
            {
                maxop = subop;
                m[j-1] = i;
            }
            
        }

        if( rop != NULL )
            mpfr_set( mps_get_ele_seq( rop, j ), maxop, GMP_RNDN );
    }

    return 0;
}

/* Find the absolute minimum entry of a row. */
mpfr_ptr mps_row_minabs( const mps_ptr op, unsigned int *m, const unsigned int row )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_ROW_BOUND(op,row),
        "Row index out of bound in mps_row_minabs\n" );

    maxop = mps_get_ele( op, row, 1 );
    *m = 1;

    for( i = 2; i <= MPS_NUMCOL(op); i++ )
    {
        subop = mps_get_ele( op, row, i );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_cmpabs( subop, maxop ) < 0 )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}

/* Find the absolute minimum entry of a column. */
mpfr_ptr mps_col_minabs( const mps_ptr op, unsigned int *m, const unsigned int col )
{
    mpfr_ptr subop, maxop;
    unsigned int i;

    MPS_ASSERT_MSG( MPS_COL_BOUND(op,col),
        "Column index out of bound in mps_col_minabs\n" );

    maxop = mps_get_ele( op, 1, col );
    *m = 1;

    for( i = 2; i <= MPS_NUMROW(op); i++ )
    {
        subop = mps_get_ele( op, i, col );

        if( mpfr_nan_p(subop) )
            continue;

        if( mpfr_nan_p(maxop) )
        {
            maxop = subop;
            *m = i;
            continue;
        }

        if( mpfr_cmpabs( subop, maxop ) < 0 )
        {
            maxop = subop;
            *m = i;
        }

    }

    return maxop;
}
