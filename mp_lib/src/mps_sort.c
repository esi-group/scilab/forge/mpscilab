/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

#define SMALLSIZE 10	/* experiment with this */

/* singly linked list act as stack. head at bottom. */
typedef struct _stackItem
{
	struct _stackItem* next;
	unsigned int a;
	unsigned int b;
} QSStack;				/* quick sort stack */

/* returns the new top of the stack */
static void qs_push(QSStack** top, unsigned int a, unsigned int b)
{
	QSStack* new = malloc(sizeof(QSStack));
	new->next = *top;
	new->a = a;
	new->b = b;
	*top = new;
}

/* this is a little messy. */
static void qs_pop(QSStack** top, unsigned int* a, unsigned int* b)
{
	unsigned int acont, bcont;
	QSStack* tmp;

	if (*top == NULL)		/* trying to pop off empty list */
		return;

	acont = (*top)->a;
	bcont = (*top)->b;
	tmp = (*top)->next;
	free(*top);

	*top = tmp;
	*a = acont;
	*b = bcont;
}

static QSStack* top = NULL;		/* stack used by nonrecursive quick sort */
static mpfr_t tmp;		/* sorting functions need temps in functions called multiple times */

/* insertion sort used on smaller ones */
static void insertion_sort(const mps_ptr mat,
                           const unsigned int first,
                           const unsigned int last,
                           const mpfr_rnd_t rnd)
{
	unsigned int i, c;
	mpfr_ptr subarr1, subarr2;

	for ( i = first; i <= last; ++i)
	{
		subarr2 = mps_get_ele_seq(mat, i);
		mpfr_set(tmp, subarr2, rnd);
		c = i;

		while ((c > first) && mpfr_greater_p(subarr1 = mps_get_ele_seq(mat, c - 1), tmp))
		{
			subarr2 = mps_get_ele_seq(mat, c--);
			mpfr_set(subarr2, subarr1, rnd);
		}

		subarr2 = mps_get_ele_seq(mat, c);
		mpfr_set(subarr2, tmp, rnd);
	}

}

/* used by nonrecursive quicksort */
static void split(const mps_ptr mat,
                  const unsigned int first,
                  const unsigned int last,
                  unsigned int* splitpoint)
{
	unsigned int x, i, j, s, g;
	mpfr_ptr subarr1, subarr2;

	subarr1 = mps_get_ele_seq(mat, first);
	subarr2 = mps_get_ele_seq(mat, (first + last)/2);

	/* might as well keep checking this for a while */
	MPS_ASSERT_MSG( first < last, "Quick sort function is probably broken" );

	if ( mpfr_less_p(subarr1, subarr2) )
	{
		s = first;
		g = (first + last) / 2;
	}
	else
	{
		g = first;
		s = (first + last) / 2;
	}

	subarr1 = mps_get_ele_seq(mat, last);
	subarr2 = mps_get_ele_seq(mat, s);

	if ( mpfr_lessequal_p(subarr1, subarr2) )
		x = s;
	else if ( mpfr_lessequal_p(subarr1, mps_get_ele_seq(mat, g)) )
		x = last;
	else
		x = g;

	mps_indx_exg(mat, x, first);	/* swap the split point element */

	subarr1 = mps_get_ele_seq(mat, first);
	i = first;
	j = last + 1;

	while ( i < j )
	{
		do
		{
			subarr2 = mps_get_ele_seq(mat, --j);
		} while ( mpfr_greater_p(subarr2, subarr1) );

		do
		{
			subarr2 = mps_get_ele_seq(mat, ++i);
		} while ( mpfr_less_p(subarr2, subarr1) );

		mps_indx_exg(mat, i, j);
	}

	mps_indx_exg(mat, i, j);		/* undo the extra swap */
	mps_indx_exg(mat, first, j);	/* bring split point element to 1st */

	*splitpoint = j;

}

/* quick sorts the matrix from index low to high */
static void nonrec_qsort_range(const mps_ptr mat,
                               const unsigned int low,
                               const unsigned int high,
                               const mpfr_rnd_t rnd)
{
	unsigned int first, last, splitpoint;

	if ( MPS_SIZE(mat) == 0)		/* sorting an empty matrix doesn't work here */
		return;

	mpfr_init2(tmp, MPS_PREC(mat));		/* initialized the shared tmp variable */

    qs_push(&top, low, high);

	while (top != NULL)
	{
		qs_pop(&top, &first, &last);
		while (1)
		{
			/* YAY!: Must cast to int since we are unsigned, and this sometimes must be negative.
			   This fixes the sometimes never completes issue. */
			if ( (signed int) (last - first) > SMALLSIZE)
			{
				split(mat, first, last, &splitpoint);
				/* possible warning: I don't think this needs to be a signed comparison. */
				if ( last - splitpoint < splitpoint - first)
				{
					qs_push(&top, first, splitpoint - 1);
					first = splitpoint + 1;
				}
				else
				{
					qs_push(&top, splitpoint + 1, last);
					last = splitpoint - 1;
				}
			}
			else		/* sort smaller lists with insertion sort */
			{
				insertion_sort(mat, first, last, rnd);
				break;
			}
		}
	}

	mpfr_clear(tmp);		/* free the shared tmp variable */

}

/* used by heap_sort and is of no use / shouldn't be used anywhere else */
static void sift_down(const mps_ptr mat, unsigned int root, const unsigned int bottom)
{
	unsigned int maxChild;
	mpfr_ptr ele1, ele2;
	char notdone = 1;	/* bool, TRUE */

	while (( root * 2 - 1 <= bottom) && notdone)
	{
		ele1 = mps_get_ele_seq(mat, root * 2 - 1);
		ele2 = mps_get_ele_seq(mat, root * 2);

		if (root * 2 - 1 == bottom || mpfr_greater_p(ele1, ele2))
			maxChild = root * 2 - 1;
		else
			maxChild = root * 2;

		ele1 = mps_get_ele_seq(mat, root);
		ele2 = mps_get_ele_seq(mat, maxChild);

		if ( mpfr_less_p(ele1, ele2) )
		{
			mpfr_swap(ele1, ele2);
			root = maxChild;
		}
		else
		{
			notdone = 0;	/* FALSE */
		}
	}
}

static void heap_sort(const mps_ptr mat, const unsigned int matsize)
{
	unsigned int i;

	for ( i = (matsize / 2); i >= 1; --i)
		sift_down(mat, i, matsize);

	for ( i = matsize; i >= 2; --i)
	{
		mps_indx_exg(mat, 1, i);
		sift_down(mat, 1, i - 1);
	}
}

/* Do an in place sort of op in column order.
 * using a nonrecursive heap sort O(n log n) in best, average and worst cases */
int mps_heapsort_col( const mps_ptr op )
{
	heap_sort(op, MPS_SIZE(op));

    return 0;
}

/* Do an in place sort of op in column order.
 * using a nonrecursive quick sort O(n log n) */
int mps_quicksort_col( const mps_ptr op, const mpfr_rnd_t rnd )
{
	nonrec_qsort_range(op, 1, MPS_SIZE(op), rnd);
    return 0;
}

/* sort each individual column.
 * TODO: Better function name */
int mps_quicksort_each_col(const mps_ptr op, const mpfr_rnd_t rnd)
{
	unsigned int i, colsize, numcol;
	colsize = MPS_NUMROW(op);			/* colsize = number of rows */
	numcol = MPS_NUMCOL(op);

	for ( i = 1; i <= numcol; ++i )		/* for each column */
	{
		nonrec_qsort_range(op, (i - 1) * colsize + 1, i * colsize, rnd);
	}

	return 0;
}

/* FIXME?: sort by rows instead of columns.
	All of the matrix accessing in the sort functions should be done.
	It seems to work if you don't flip the order back.
	I think I need to investigate this more?
	Actually it sort of makes sense. The items get sorted in the same
	space and don't move, you just interpret them differently.
	Is there any consequence to not flipping back?
	Should there be a reorganize function or something? */
int mps_heapsort_row( mps_ptr op )
{
	mps_flip_order(op);
	heap_sort(op, MPS_SIZE(op));
	/* mps_flip_order(op); */

    return 0;
}

int mps_quicksort_row( mps_ptr op, const mpfr_rnd_t rnd )
{
	mps_flip_order(op);
	nonrec_qsort_range(op, 1, MPS_SIZE(op), rnd);
	/* mps_flip_order(op); */

    return 0;
}

/* reverses the current order of op */
int mps_reverse( const mps_ptr op )
{
	unsigned int i, size = MPS_SIZE(op);

	for ( i = 1; i <= size / 2; ++i )
		mps_indx_exg(op, i, size - i + 1);

	return 0;
}

/* big matrices I thought were breaking it, but they may just have been
too big to print / string manipulation took too long, so this can be used to verify.
Returns 0 if the matrix passes sort test, > 1 if it fails */
/* checks that A is a sorted version of B */
int mps_check_sort(const mps_ptr A, const mps_ptr B)
{
	int ordered, elements;

	ordered = mps_check_ordered(A);
	elements = mps_check_each_element(A, B);

	ordered = ordered << 1;

	return ordered|elements;
}

int mps_check_ordered(const mps_ptr op)
{
	mpfr_ptr subop1, subop2;
	unsigned int size, i = 1;

	size = MPS_SIZE(op);

	while( i < size )
	{
		subop1 = mps_get_ele_seq(op, i);		/* take and element */
		subop2 = mps_get_ele_seq(op, ++i);		/* and the next one */

		if (mpfr_greater_p(subop1, subop2))		/* if 1st > 2nd, this isn't sorted right */
			return 1;

	}

	return 0;
}

/* check that each element of B is in A in right quantity.
 * meant to be simple function to test quicksort */
int mps_check_each_element(const mps_ptr A, const mps_ptr B)
{
	mpfr_ptr subB;
	unsigned int countA, countB, i;
	unsigned int* resA;
	unsigned int* resB;

	for ( i = 1; i <= MPS_SIZE(B); ++i )
	{
		subB = mps_get_ele_seq(B, i);
		countA = mps_linsearch_all(A, subB, &resA);
		countB = mps_linsearch_all(B, subB, &resB);
		free(resA);
		free(resB);

		if (countA != countB)	/* failure */
		{
			mpfr_printf("mps_check_each_element(): count %2.RNg A = %u, B = %u\n", subB, countA, countB);
			return 1;
		}
	}

	return 0;
}

