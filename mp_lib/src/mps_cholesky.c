/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* symmetric, positive-definite matrix */
void mps_cholesky_decomp( const mps_ptr A, const mpfr_rnd_t rnd )
{
    unsigned int i, j, k;
    mpfr_ptr A00, A10, A11, Aki, Aii, Akk, Aij;
    mps_blk_t ci, ck;
    mps_blk_t di, dk;
    mpfr_t sum, diag;
    const unsigned int M = MPS_NUMROW(A);

    /* TODO: Checking for conditions like symmetric, Hermitian */

    MPS_ASSERT_MSG( MPS_IS_SQUARE(A),
                    "Expected square matrix in mps_cholesky_decomp()\n");

    mpfr_inits2(MPS_PREC(A), sum, diag, (mpfr_ptr) NULL);

    A00 = mps_get_ele(A, 1, 1);

    if ( mpfr_sgn(A00) <= 0 )
        goto error;

    mpfr_sqrt(A00, A00, rnd);

    if ( M > 1 )
    {
        A10 = mps_get_ele(A, 2, 1);
        A11 = mps_get_ele(A, 2, 2);
        mpfr_div(A10, A10, A00, rnd);
        mpfr_sqr(diag, A10, rnd);           /* A11 - L10^2*/
        mpfr_sub(diag, A11, diag, rnd);

        if ( mpfr_sgn(diag) <= 0 )
            goto error;

        mpfr_sqrt(A11, diag, rnd);
    }

    for ( k = 3; k <= M; ++k )
    {
        Akk = mps_get_ele(A, k, k);

        for ( i = 1; i < k; ++i )
        {
            mpfr_set_ui(sum, 0, rnd);
            Aki = mps_get_ele(A, k, i);
            Aii = mps_get_ele(A, i, i);

            mps_row_blk(ci, A, i);
            mps_row_blk(ck, A, k);

            if ( i > 1 )
            {
                mps_subsubrow(di, ci, 1, i - 1);
                mps_subsubrow(dk, ck, 1, i - 1);
                mps_dot_product_blk(sum, di, dk, rnd);
            }

            mpfr_sub(Aki, Aki, sum, rnd);       /* Aki = (Aki - sum) / Aii */
            mpfr_div(Aki, Aki, Aii, rnd);
        }

        {
            mps_blk_t ck, dk;
            mps_row_blk(ck, A, k - 1);
            mps_subsubrow(dk, ck, 1, k - 1);

            mps_nrm2_blk(sum, dk, rnd);
            mpfr_sqr(diag, sum, rnd);          /* diag = Akk - sum^2 */
            mpfr_sub(diag, Akk, diag, rnd);

            if ( mpfr_sgn(diag) <= 0 )
                goto error;

            mpfr_sqrt(Akk, diag, rnd);
        }

    }

    /* copy transposed lower triangle to upper triangle, diagonal common */
    for ( i = 2; i <= M; ++i )
    {
        for ( j = 1; j < i; ++j )
        {
            Aij = mps_get_ele(A, i, j);
            mps_set_ele(A, j, i, Aij, rnd);
        }
    }

    mpfr_clears(sum, diag, (mpfr_ptr) NULL);

	return;		/* success */

error:
    mpfr_clears(sum, diag, (mpfr_ptr) NULL);
    MPS_WARNING_MSG("Matrix must be a positive definite in mps_cholesky_decomp()\n");

}

