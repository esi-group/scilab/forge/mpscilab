/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

/* Set each element of rop to e^op */
int mps_exp( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_exp()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_exp( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


int mps_exp_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_exp( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


/* Set each element of rop to 2^op */
int mps_exp2( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_exp2()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp2, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_exp2( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


int mps_exp2_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp2_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_exp2( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


/* Set each element of rop to 10^op*/
int mps_exp10( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_exp10()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp10, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, subrop, subop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop = mps_get_ele_col( op, i );

        mpfr_exp10( subrop, subop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}


int mps_exp10_double( const mps_ptr rop, 
                    const double op[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_exp10_double, matsize, MPS_PREC(rop) );

        #pragma omp parallel num_threads(num_thr)
        {

        #pragma omp for private(i, j, subrop) \
                schedule(guided)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        if( order == MPS_COL_ORDER )
            mpfr_set_d( subrop, op[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op[j-1], rnd);
        }

        mpfr_exp10( subrop, subrop, rnd );
    }

    #ifdef WITH_OPENMP
        if( omp_get_thread_num() != 0 || omp_get_active_level() > 1 )
            mpfr_free_cache();
        }
    #endif

    return 0;
}

