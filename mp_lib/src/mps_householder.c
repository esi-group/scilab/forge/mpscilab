/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"
#include "mps_priv.h"

/* replace v[0:n-1] with a householder vector (v[0:n-1]) and
 * coefficient tau that annihilates v[1:n-1] */

void mps_householder_transform( const mpfr_ptr tau, const mps_blk_ptr v, const mpfr_rnd_t rnd )
{
	mpfr_t utmp1, beta, xnorm;	/* TODO: Temporary variables */
	mpfr_ptr alpha;
	mps_blk_t x;
	const unsigned int N = MPS_BLOCK_SIZE(v);

	if ( N == 1 )
	{
		mpfr_set_ui(tau, 0, rnd);
		MPS_DEBUG("N = 1, %7.RNg returning 0\n", tau)
		return;		/* tau = 0 */
	}

	mpfr_inits2(mpfr_get_prec(tau), utmp1, beta, xnorm, (mpfr_ptr) NULL);
	mps_subsubslice(x, v, 2, N - 1);

	mpfr_printf("Got slice: %u, %u\n", 2, N - 1);
	mps_print_overview_blk(x, 6);

	mpfr_printf("Calling mps_nrm2_blk()\n");
	mps_nrm2_blk(xnorm, x, rnd);

	MPS_DEBUG("got xnorm = %7.RNg, scaled x:\n", xnorm);
	mps_print_overview_blk(x, 6);

	if ( mpfr_zero_p(xnorm) )
	{
		mpfr_set_ui(tau, 0, rnd);
		MPS_DEBUG("XNORM == 0 !! %7.RNg\n", tau)
		return;
	}

	alpha = mps_get_ele_seq_blk(v, 1);
	MPS_DEBUG("got alpha: %7.RNg\n", alpha);

	mpfr_hypot(beta, alpha, xnorm, rnd);	/* (alpha >= 0 ? -1 : + 1) * hypot(alpha, xnorm) */
	if ( mpfr_sgn(alpha) >= 0 )
	{
		mpfr_neg(beta, beta, rnd);
		MPS_DEBUG("cmp(alpha, 0) >= 0, negating beta = %7.RNg\n", beta);
	}

	MPS_DEBUG("beta = %7.RNg, alpha = %7.RNg\n", beta, alpha);

	mpfr_sub(tau, beta, alpha, rnd);
	mpfr_neg(utmp1, tau, rnd);		/* we want alpha - beta, and have beta - alpha already */
	mpfr_div(tau, tau, beta, rnd);

	mpfr_ui_div(utmp1, 1, utmp1, rnd);	/* 1 / (alpha - beta) */
	MPS_DEBUG("Using scale factor %7.RNg\n", utmp1);
	mps_scale_blk(x, utmp1, rnd);

	MPS_DEBUG("x after scale by %7.RNg\n", utmp1);
	mps_print_overview_blk(x, 7);

	mps_set_ele_seq_blk(v, 1, beta, rnd);
	mpfr_clears(utmp1, beta, xnorm, (mpfr_ptr) NULL);
}

/* applies a householder transformation v, tau to matrix m */
void mps_householder_hm( const mpfr_ptr tau,
                         const mps_blk_ptr v,
                         const mps_blk_ptr A,
                         const mpfr_rnd_t rnd )
{
	unsigned int i, j;
    mpfr_ptr A0j;
	mpfr_ptr subA, subv, Aij, vi;
	mpfr_t wj, utmp1, utmp2;		/* TODO: Temporary variables */

	const unsigned int numrow = MPS_BLOCK_NUMROW(A);
	const unsigned int numcol = MPS_BLOCK_NUMCOL(A);

	if ( mpfr_zero_p(tau) )
		return;		/* Success */

	mpfr_inits2(MPS_VEC_PREC(v), wj, utmp1, utmp2, (mpfr_ptr) NULL);

	mpfr_printf("A in %s\n", __func__);
	mps_print_overview_blk(A, 5);
	mpfr_printf("v in %s\n", __func__);
	mps_print_overview_blk(v, 5);

	for ( j = 1; j <= numcol; ++j )
	{
		/* Compute wj = Akj vk */
		subA = mps_get_ele_blk(A, 1, j);		/* subA = wj */
		MPS_DEBUG("Got wj SubA %u, %u = %7.RNg\n", 1, j, subA);
		mpfr_set(wj, subA, rnd);
		MPS_DEBUG("wj set to %7.RNg\n", wj);

		for ( i = 2; i <= numrow; ++i )			/* note: computed for v(1) = 1 above */
		{
			subA = mps_get_ele_blk(A, i, j);
			MPS_DEBUG("Got SubA %u, %u = %7.RNg\n", i, j, subA);
			subv = mps_get_ele_seq_blk(v, i);
			MPS_DEBUG("Got subv[%u] = %2.RNG\n", i, subv);
			mpfr_mul(utmp1, subA, subv, rnd);
			mpfr_add(wj, wj, utmp1, rnd);
			MPS_DEBUG("wj LOOP set to %7.RNg\n", wj);
		}

		/* Aij = Aij - tau vi wj */

		/* i = 1 */
		{
			MPS_DEBUG("Wj is %7.RNg\n", wj);
			A0j = mps_get_ele_blk(A, 1, j);
			MPS_DEBUG("Got SubA %u, %u = %7.RNg\n", 1, j, A0j);
			mpfr_mul(utmp1, tau, wj, rnd);		/* A0j - tau * wj */
			mpfr_sub(A0j, A0j, utmp1, rnd);
			MPS_DEBUG("A0j changed to %7.RNg\n", A0j);
		}

		/* i = 1 .. M - 1 */

		/* tau * wj will be same for every loop, do that before */
		mpfr_mul(utmp1, tau, wj, rnd);
		MPS_DEBUG("tau = %7.RNg, wj = %7.RNg, prod = %7.RNg\n", tau, wj, utmp1);
		for ( i = 2; i <= numrow; ++i )
		{
			Aij = mps_get_ele_blk(A, i, j);
			MPS_DEBUG("Got SubA %u, %u = %7.RNg\n", i, j, Aij);
			vi = mps_get_ele_seq_blk(v, i);
			MPS_DEBUG("Got vi %u = %7.RNg\n", i, vi);
			mpfr_mul(utmp2, vi, utmp1, rnd);
			MPS_DEBUG("Aij = %7.RNg, vi * tau * wj = %7.RNg\n", Aij, utmp2);
			mpfr_sub(Aij, Aij, utmp2, rnd);		/* Aij - tau * vi * wj */
			MPS_DEBUG("Aij changed to %7.RNg\n", Aij);
		}
	}

	mpfr_clears(wj, utmp1, utmp2, (mpfr_ptr) NULL);

	return;	/* success */

}

/* applies a householder transformation v, tau to matrix m from the right hand side
 * in order to zero out rows */
void mps_householder_mh( const mpfr_ptr tau,
                         const mps_blk_ptr v,
                         const mps_blk_ptr A,
                         const mpfr_rnd_t rnd )
{
	unsigned int i, j;
	mpfr_ptr subA, subv, vj, Aij;
	mpfr_t wi, utmp1, utmp2;		/* TODO: Temporary variables */
	const unsigned int numrow = MPS_BLOCK_NUMROW(A);
	const unsigned int numcol = MPS_BLOCK_NUMCOL(A);

	if ( mpfr_zero_p(tau) )
		return;		/* Success */

	mpfr_inits2(MPS_VEC_PREC(v), wi, utmp1, utmp2, (mpfr_ptr) NULL);

	/* A = A - tau w v' */

	for ( i = 1; i <= numrow; ++i )
	{

		subA = mps_get_ele_blk(A, i, 1);	/* subA = wi */
		mpfr_set(wi, subA, rnd);

		for ( j = 2; j <= numcol; ++j )		/* note, computed for v(1) = 1 above */
		{
			subA = mps_get_ele_blk(A, i, j);
			subv = mps_get_ele_seq_blk(v, j);
			mpfr_mul(utmp1, subA, subv, rnd);
			mpfr_add(wi, wi, utmp1, rnd);
		}

		/* j = 1 */
		{
			mpfr_ptr Ai0 = mps_get_ele_blk(A, i, 1);
			mpfr_mul(utmp1, tau, wi, rnd);			/* Ai0 - tau * wi */
			mpfr_sub(Ai0, Ai0, utmp1, rnd);
		}

		/* j = 2 .. N - 1 */
		mpfr_mul(utmp1, tau, wi, rnd);	/* this doesn't change in the loop */
		for ( j = 2; j <= numcol; ++j )
		{
			vj = mps_get_ele_seq_blk(v, j);
			Aij = mps_get_ele_blk(A, i, j);
			mpfr_mul(utmp2, vj, utmp1, rnd);	/* Aij - tau * wi * vj */
			mpfr_sub(Aij, Aij, utmp2, rnd);
		}
	}

	mpfr_clears(wi, utmp1, utmp2, (mpfr_ptr) NULL);

	return;	/* success */
}

#if 1
void mps_householder_reflection( const mpfr_ptr tau, const mps_blk_ptr v, const mpfr_rnd_t rnd )
{
	mpfr_t utmp1, utmp2, beta, mx, xnorm;	/* TODO: Temporary variables */
	mpfr_ptr alpha, subv;
    unsigned int i;
	const unsigned int N = MPS_BLOCK_SIZE(v);

    if ( N <= 1 )
    {
        mpfr_set_ui(tau, 0, rnd);
        return;
    }

    mpfr_inits2(mpfr_get_prec(tau), utmp1, utmp2, beta, xnorm, mx, (mpfr_ptr) NULL);

    alpha = mps_get_ele_seq_blk(v, 1);

/*    mps_nrm2_blk(xnorm, v, rnd); */

    mpfr_set_ui(mx, 0, rnd);

    /* nrm2 */
    for ( i = 2; i <= N; ++i )
    {
        subv = mps_get_ele_seq_blk(v, i);
        mpfr_abs(utmp1, subv, rnd);
        mpfr_max(mx, utmp1, mx, rnd);
    }

    mpfr_set_ui(xnorm, 0, rnd);

    if ( mpfr_sgn(mx) )
    {
        for ( i = 2; i <= N; ++i )
        {
            subv = mps_get_ele_seq_blk(v, i);
            mpfr_div(utmp1, subv, mx, rnd);
            mpfr_sqr(utmp1, utmp1, rnd);
            mpfr_add(xnorm, xnorm, utmp1, rnd);
        }
        mpfr_sqrt(xnorm, xnorm, rnd);
        mpfr_mul(xnorm, xnorm, mx, rnd);
    }

    if ( mpfr_zero_p(xnorm) )
    {
        mpfr_set_ui(tau, 0, rnd);
        return;
    }

    mpfr_abs(utmp1, alpha, rnd);
    mpfr_abs(utmp2, xnorm, rnd);

    mpfr_max(mx, utmp1, utmp2, rnd);

    mpfr_div(utmp1, alpha, mx, rnd);
    mpfr_div(utmp2, xnorm, mx, rnd);
    mpfr_hypot(beta, utmp1, utmp2, rnd);
    mpfr_mul(beta, beta, mx, rnd);
/*
    mpfr_neg(beta, beta, rnd);

    if ( mpfr_sgn(alpha) < 0 )
        mpfr_neg(beta, beta, rnd);
*/

    if ( mpfr_sgn(alpha) >= 0 )
        mpfr_neg(beta, beta, rnd);

    mpfr_sub(tau, beta, alpha, rnd);
    mpfr_div(tau, tau, beta, rnd);

    mpfr_sub(utmp1, alpha, beta, rnd);      /* 1 / (alpha - beta)*/
    mpfr_ui_div(utmp1, 1, utmp1, rnd);

    for ( i = 2; i <= N; ++i )              /* partially scale */
    {
        subv = mps_get_ele_seq_blk(v, i);
        mpfr_mul(subv, subv, utmp1, rnd);
    }

    mps_set_ele_seq_blk(v, 1, beta, rnd);

    mpfr_clears(utmp1, utmp2, beta, xnorm, mx, (mpfr_ptr) NULL);
}
#endif

