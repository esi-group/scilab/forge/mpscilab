/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2010 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

#if 0
/* Perform multiplication between a real matrix and an mpfr scalar. */
int mps_mul_scalar( const mps_ptr rop, const mps_ptr op1, const mpfr_ptr op2, const mpfr_rnd_t rnd )
{
    unsigned int num_thr = 1;

    GET_NUMTHR(num_thr, op1)

    xmps_mul_scalar( rop, op1, op2, rnd, num_thr );
    return 0;
}
#endif

/* Perform multiplication between a real matrix and an mpfr scalar. */
int mps_mul_scalar( const mps_ptr rop,
                     const mps_ptr op1,
                     const mps_ptr op2,
                     const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_mul_scalar()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_mul_scalar()\n" );

subop2 = mps_get_ele( op2, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mul_scalar, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_mul( subrop, subop1, subop2, rnd );
    }

    return 0;
}

/* Perform multiplication between a real matrix and a double scalar. */
int mps_mul_scalar_double( const mps_ptr rop, const mps_ptr op1, const double op2, const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_mul_scalar_double()\n" );


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mul_scalar_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );

        mpfr_mul_d( subrop, subop1, op2, rnd );
    }

    return 0;
}

/*
 * Perform multiplication between a double matrix and an mpfr scalar. Careful as
 * the dimension for the double matrix will be inferred from the result operand.
 */
int mps_scalar_mul_double( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order,
                           const mps_ptr op2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop2;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_IS_SCALAR(op2),
        "op2 not a scalar in mps_scalar_mul_double()\n" );

    subop2 = mps_get_ele( op2, 1, 1 );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_scalar_mul_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop) \
            schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; i++ )
    {
        if( order == MPS_COL_ORDER )
            subrop = mps_get_ele_col( rop, i );
        else
            subrop = mps_get_ele_row( rop, i );

        mpfr_mul_d( subrop, subop2, op1[i-1], rnd );
    }

    return 0;
}

#if 0
/* Perform multiplication between a real matrix and an mpfr scalar. */
int mps_mul( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    unsigned int num_thr = 1;

    GET_NUMTHR(num_thr, op1)

    xmps_mul( rop, op1, op2, rnd, num_thr );
    return 0;
}
#endif

/*
 * Entry-wise matrix multiplication. (Hadamard product) op1 and op2 must be of
 * the same dimension else the result is undefined. (segfault)
 */
int mps_mul( const mps_ptr rop, const mps_ptr op1, const mps_ptr op2, const mpfr_rnd_t rnd )
{
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);
    mpfr_ptr subrop, subop1, subop2;

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(op1,op2),
        "Operands differ in dimension in mps_mul()\n" );

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_mul()\n" );

    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mul, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1, subop2) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );
        subop1 = mps_get_ele_col( op1, i );
        subop2 = mps_get_ele_col( op2, i );

        mpfr_mul( subrop, subop1, subop2, rnd );
    }

    return 0;
}

/*
 * Entry-wise matrix multiplication (Hadamard product) between a multi-precision
 * matrix and an array of doubles. op1 and op2 must be of the same dimension
 * else the result is undefined. (segfault)
 */

int mps_mul_double( const mps_ptr rop,
                    const mps_ptr op1,
                    const double op2[],
                    const mps_order_t order,
                    const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, subop1;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op1);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_add_double()\n" );


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_mul_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, subrop, subop1) \
                schedule(guided) num_threads(num_thr)
    #endif
	for( i = 1; i <= matsize; ++i )
	{
    	if ( order == MPS_COL_ORDER )
	    {
            subrop = mps_get_ele_col( rop, i );
            subop1 = mps_get_ele_col( op1, i );
        }
        else
		{
			subrop = mps_get_ele_row( rop, i );
			subop1 = mps_get_ele_row( op1, i );
		}

        mpfr_mul_d( subrop, subop1, op2[i-1], rnd );
	}

    return 0;
}

/*
 * Perform an element-wise multiplication between two matrix of double. The size 
 * is inferred by taking the size of the output argument.
 */
int mps_double_mul_double( const mps_ptr rop,
                           const double op1[],
                           const mps_order_t order1,
                           const double op2[],
                           const mps_order_t order2,
                           const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop;
    unsigned int i;
    unsigned int j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_mul_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_set_d( subrop, op1[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_set_d( subrop, op1[j-1], rnd);
        }

        if( order2 == MPS_COL_ORDER )
            mpfr_mul_d( subrop, subrop, op2[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_mul_d( subrop, subrop, op2[j-1], rnd);
        }
    }

    return 0;
}

/*
 * Perform an element-wise multiplication between a matrix of double and a 
 * double scalar.
 */
#ifndef WITH_OPENMP
int mps_double_mul_scalar_double( const mps_ptr rop,
                                  const double op1[],
                                  const mps_order_t order1,
                                  const double op2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    /*
     * To save some space we use the last element of the result matrix as a
     * temporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op2, rnd );

    for( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_mul_d( subrop, scalarop, op1[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_mul_d( subrop, scalarop, op1[j-1], rnd);
        }
    }

    return 0;
}
#else
int mps_double_mul_scalar_double( const mps_ptr rop,
                                  const double op1[],
                                  const mps_order_t order1,
                                  const double op2,
                                  const mpfr_rnd_t rnd )
{
    mpfr_ptr subrop, scalarop;
    unsigned int i, j;
    const unsigned int matsize = MPS_SIZE(rop);

    #ifdef WITH_OPENMP
        unsigned int num_thr = 1;
    #endif

    /*
     * To save some space we use the last element of the result matrix as a
     * temporary space to store the converted scalar.
     */
    scalarop = mps_get_ele_col( rop, matsize );
    mpfr_set_d( scalarop, op2, rnd );


    #ifdef WITH_OPENMP
        num_thr = GET_NUM_THREADS( mps_double_mul_scalar_double, matsize, MPS_PREC(rop) );
        #pragma omp parallel for private(i, j, subrop) \
                schedule(guided) num_threads(num_thr)
    #endif
    for( i = 1; i < matsize; ++i )
    {
        subrop = mps_get_ele_col( rop, i );

        if( order1 == MPS_COL_ORDER )
            mpfr_mul_d( subrop, scalarop, op1[i-1], rnd);
        else
        {
            j = ( ((i-1) / MPS_NUMROW(rop)) + \
				MPS_NUMCOL(rop) * ((i-1) % MPS_NUMROW(rop)) );

            mpfr_mul_d( subrop, scalarop, op1[j-1], rnd);
        }
    }

    /* The final entry must be done last to prevent overwriting the scalar early. */
    subrop = mps_get_ele_col( rop, matsize );

    if( order1 == MPS_COL_ORDER )
        mpfr_mul_d( subrop, scalarop, op1[matsize-1], rnd);
    else
    {
        j = ( ((matsize-1) / MPS_NUMROW(rop)) + \
			MPS_NUMCOL(rop) * ((matsize-1) % MPS_NUMROW(rop)) );

        mpfr_mul_d( subrop, scalarop, op1[j-1], rnd);
    }
    

    return 0;
}
#endif /* !WITH_OPENMP */

