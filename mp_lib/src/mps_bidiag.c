/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"

void mps_bidiag_decomp( mps_ptr A, mps_vec_ptr tau_U, mps_vec_ptr tau_V, const mpfr_rnd_t rnd )
{
	unsigned int i;
	mps_vec_t c, v;
	mps_blk_t m, r;
	mpfr_t tau_i;		/* TODO: Temporary variables */

	const unsigned int M = MPS_NUMROW(A);
	const unsigned int N = MPS_NUMCOL(A);

	MPS_ASSERT_MSG( MPS_NUMROW(A) >= MPS_NUMCOL(A),
					"Bidiagonal decomposition requires M >= N in mps_bidiag_decomp()\n");

	MPS_ASSERT_MSG( MPS_VEC_SIZE(tau_U) == MPS_NUMCOL(A),
					"Expected size of vector to be N in mps_bidiag_decomp()\n");

	MPS_ASSERT_MSG( MPS_VEC_SIZE(tau_V) + 1 == MPS_NUMCOL(A),
					"Expected size of vector to be (N - 1) in mps_bidiag_decomp()\n");

	mpfr_init2(tau_i, MPS_PREC(A));

	for ( i = 1; i <= N; ++i )
	{
		mpfr_printf("***********************************************************************\n");
		/* apply householder transformation to current column */
		{
			mpfr_printf("-------------------------------------------------A\n");
			MPS_DEBUG("Outer loop: i = %u, M - i + 1 = %u\n", i, M - i + 1);
			mpfr_printf("Matrix A:\n");
			mps_print_overview(A, 7);

			mps_subvec(c, A, i);

			mpfr_printf("subvec c:\n");
			mps_print_overview_blk(c, 7);

			mps_subsubvec(v, c, i, M - i + 1);

			mpfr_printf("subsubvec v:\n");
			mps_print_overview_blk(v, 7);
			mpfr_printf("-------------------------------------------------A\n");

			MPS_DEBUG("Calling mps_householder_transform()...i = %u\n", i);
			mps_householder_transform(tau_i, v, rnd);
			MPS_DEBUG("tau_i = %7.RNg\n", tau_i);

			mpfr_printf("-------------------------------------------------B\n");
			mpfr_printf("\tAfter transform Matrix A:\n");
			mps_print_overview(A, 7);
			mpfr_printf("\tAfter transform subsubvec v:\n");
			mps_print_overview_blk(v, 7);
			mpfr_printf("-------------------------------------------------B\n");

			/* apply the transformation to the remaining columns */

			if ( i + 1 <= N )
			{
				mpfr_printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
				MPS_DEBUG("Getting submat m. lowrow = %u, numrow = %u, lowcol = %u, numcol = %u\n",
								i, M - i + 1, i + 1, N - i);
				mps_submat(m, A, i, M - i + 1, i + 1, N - i);
				mpfr_printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$A\n");
				mpfr_printf("\tSubmat m:\n");
				mps_print_overview_blk(m, 7);
				mpfr_printf("\tSubsubvec v\n");
				mps_print_overview_blk(v, 7);
				mpfr_printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$A\n");
				/* mps_householder_hm() seems to be the point of failure.
				 * I'm guessing wrong block element access */
				MPS_DEBUG("Calling mps_householder_hm()...i = %u\n", i);
				mps_householder_hm(tau_i, v, m, rnd);
				MPS_DEBUG("\ttau_i = %7.RNg\n", tau_i);

				mpfr_printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$B\n");
				mpfr_printf("\tMatrix A:\n");
				mps_print_overview(A, 7);
				mpfr_printf("\tSubsubvec v\n");
				mps_print_overview_blk(v, 7);
				mpfr_printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$B\n");
				mpfr_printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
			}

			mps_set_ele_vec(tau_U, i, tau_i, rnd);
			MPS_DEBUG("Set tau_U = %9.RNg\n", tau_i);
		}

		/* Apply householder transformation to current row */

		if ( i + 1 <= N )
		{
			mpfr_printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			mpfr_printf("/////////////////////////////////////A\n");
			mpfr_printf("\tMatrix A:\n");
			mps_print_overview(A, 7);
			mps_row_blk(r, A, i);

			mpfr_printf("\tSubrow r\n");
			mps_print_overview_blk(r, 7);

			mps_subsubrow(v, r, i + 1, N - i);

			MPS_DEBUG("\tSlice r of v: %u, %u\n", i + 1, N - i);
			mps_print_overview_blk(v, 7);
			mpfr_printf("/////////////////////////////////////A\n");

			MPS_DEBUG("Calling mps_householder_transform on v...i = %u\n", i);
			mps_householder_transform(tau_i, v, rnd);
			MPS_DEBUG("tau_i = %7.RNg\n", tau_i);

			mpfr_printf("/////////////////////////////////////B\n");
			mpfr_printf("\tMatrix A:\n");
			mps_print_overview(A, 7);

			mpfr_printf("\tSlice v\n");
			mps_print_overview_blk(v, 7);
			mpfr_printf("/////////////////////////////////////B\n");

			if ( i + 1 <= M )
			{
				mpfr_printf("#####################################################\n");
				mpfr_printf("\t\tGetting submatrix m: lowrow = %u, numrow = %u, lowcol = %u, sizecol= %u\n", \
						i + 1, M - i, i + 1, N - i);
				mps_submat(m, A, i + 1, M - i, i + 1, N - i);
				mpfr_printf("```````````````````````````````````````A\n");
				mps_print_overview_blk(m, 7);
				mpfr_printf("```````````````````````````````````````A\n");

				MPS_DEBUG("Calling mps_householder_mh(), i = %u\n", i);
				mps_householder_mh(tau_i, v, m, rnd);

				mpfr_printf("```````````````````````````````````````B\n");
				mpfr_printf("\t\tMatrix A:\n");
				mps_print_overview(A, 7);

				mpfr_printf("\t\tSub m:\n");
				mps_print_overview_blk(m, 7);

				mpfr_printf("\t\tSubsubvec v\n");
				mps_print_overview_blk(v, 7);
				mpfr_printf("```````````````````````````````````````B\n");

				mpfr_printf("#####################################################\n");
			}

			mps_set_ele_vec(tau_V, i, tau_i, rnd);
			MPS_DEBUG("Set tau_V = %9.RNg\n", tau_i);
			mpfr_printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
		mpfr_printf("***********************************************************************\n");
	}

	mpfr_clear(tau_i);

	return;	/* return success */

}

