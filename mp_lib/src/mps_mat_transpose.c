/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_assert.h"


/* Gives the full transpose of op in rop. */
int mps_full_mat_transpose( const mps_ptr rop, const mps_ptr op )
{
    mpfr_ptr subrop, subop;
    unsigned int i;
    const unsigned int matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_NUMROW(rop) == MPS_NUMCOL(op)
            && MPS_NUMCOL(rop) == MPS_NUMROW(op),
            "Result operand wrong size in mps_full_mat_transpose()");

    for ( i = 1; i <= matsize; ++i )
    {
        subrop = mps_get_ele_row( rop, i );
        subop = mps_get_ele_col( op, i  );
        mpfr_set( subrop, subop, GMP_RNDN );
    }

    return 0;
}


/* Quickly transpose a matrix in place by changing the order flag. */
int mps_mat_transpose( mps_ptr op )
{
    const unsigned int numcol = MPS_NUMCOL( op );
    mps_flip_order( op );

    MPS_NUMCOL( op ) = MPS_NUMROW( op );
    MPS_NUMROW( op ) = numcol;

    return 0;
}

