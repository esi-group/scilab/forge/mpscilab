/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps_priv.h"
#include "mps_assert.h"

/*
 * Algorithms for sampling the Gamma distribution are taken from : 
 * J. H. Ahrens and U. Dieter, "Computer methods for sampling from gamma, beta, poisson and bionomial distributions ",
 * Computing,  Volume 12, Number 3, 223-246, DOI: 10.1007/BF02293108
 */

/* Generates a gamma distributed variable using the two parts GT algorithm. */
int mps_rand_gam_gt( const mps_ptr rop, const mps_ptr mpsshape, const mps_ptr mpsscale, gmp_randstate_t state )
{
	mpfr_ptr subrop, scale, shape;
    mpfr_t p, b, x, fshape, tmp;
	unsigned int i, j;
    unsigned int mshape;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsshape),
        "mpshape not a scalar in mps_rand_gam_gt()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsscale),
        "mpscale not a scalar in mps_rand_gam_gt()\n" );

    mpfr_init2( p, MPS_PREC(rop) );
    mpfr_init2( b, MPS_PREC(rop) );
    mpfr_init2( x, MPS_PREC(rop) );
    mpfr_init2( fshape, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );

    shape = mps_get_ele( mpsshape, 1, 1 );
    scale = mps_get_ele( mpsscale, 1, 1 );

    mshape = mpfr_get_ui( shape, GMP_RNDZ ); /* Integer part of a. */
    mpfr_frac( fshape, shape, GMP_RNDN ); /* Fractional part of a. */

    MPS_ASSERT_MSG( mpfr_sgn( shape ) > 0,
        "Shape parameter is negative or zero in mps_rand_gam_gt()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( scale ) > 0,
        "Scale parameter is negative or zero in mps_rand_gam_gt()\n" );

    /* Compute b. */
    mpfr_set_ui( b, 1, GMP_RNDN );
    mpfr_exp( b, b, GMP_RNDN ); /* Set b to e. */
    mpfr_add( p, b, fshape, GMP_RNDN );
    mpfr_div( b, p, b, GMP_RNDN ); /* b = (e+a)/e */
    
	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );
        mpfr_set_zero( subrop, 1 );

        /* Algorithm GM for integer values of the shape parameter. */
        if( mshape > 0 )
        {
            for( j = 1; j <= mshape; j++ )
            {
                mpfr_urandom( p, state, GMP_RNDZ );
                mpfr_log( p, p, GMP_RNDN );
                mpfr_sub( subrop, subrop, p, GMP_RNDN );
            }
        }

        /* Algorithm GS for 0 < a <= 1. */
        if( !mpfr_zero_p( fshape ) )
        {
            do
            {
                mpfr_urandom( p, state, GMP_RNDZ );
                mpfr_mul( p, p, b, GMP_RNDN );

                if( mpfr_cmp_ui(p, 1) > 0 ) /* (Case x > 1) */
                {
                    /* x = -ln((b-p)/a) */
                    mpfr_sub( x, b, p, GMP_RNDN );
                    mpfr_div( x, x, fshape, GMP_RNDN );
                    mpfr_log( x, x, GMP_RNDN );
                    mpfr_neg( x, x, GMP_RNDN );

                    /* Compute x**(a-1) for the rejection test. */
                    mpfr_sub_d( tmp, fshape, 1, GMP_RNDN ); /* Constant! */
                    mpfr_pow( tmp, x, tmp, GMP_RNDN );

                    /* Rejection test. */
                    mpfr_urandom( p, state, GMP_RNDZ );
                    if( mpfr_lessequal_p( p, tmp ) )
                        break;
                }
                else /* (Case x <= 1) */
                {
                    /* x = p**(1/a) */
                    mpfr_si_div( tmp, 1, fshape, GMP_RNDN ); /* Constant! */
                    mpfr_pow( x, p, tmp, GMP_RNDN );

                    /* Compute e**-x for the rejection test. */
                    mpfr_neg( tmp, x, GMP_RNDN );
                    mpfr_exp( tmp, tmp, GMP_RNDN );

                    /* Rejection test. */
                    mpfr_urandom( p, state, GMP_RNDZ );
                    if( mpfr_lessequal_p( p, tmp ) )
                        break;
                }

            } while(1);

            mpfr_add( subrop, subrop, x, GMP_RNDN );
        }

        /* Applying scale parameter. */
        mpfr_mul( subrop, subrop, scale, GMP_RNDN );    
    }

    mpfr_clear( p );
    mpfr_clear( b );
    mpfr_clear( x );
    mpfr_clear( fshape );
    mpfr_clear( tmp );

    return 0;
}


int mps_rand_gam_gt_double( const mps_ptr rop, double shape, double scale, gmp_randstate_t state )
{
    mps_t mpsshape, mpsscale;

    MPS_ASSERT_MSG( shape > 0,
        "Shape parameter is less than or equal to 1 in mps_rand_gam_gt_double()\n" );

    MPS_ASSERT_MSG( scale > 0,
        "Scale parameter is negative or zero in mps_rand_gam_gt_double()\n" );

    mps_init( mpsshape, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsscale, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsshape, 1, 1 ), shape, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsscale, 1, 1 ), scale, GMP_RNDN );

    mps_rand_gam_gt( rop, mpsshape, mpsscale, state );

    mps_free( mpsshape );
    mps_free( mpsscale );

    return 0;
}


/* Generates a gamma distributed variable using the GC algorithm. Sligthly broken at the moment...*/
int mps_rand_gam_gc( const mps_ptr rop, const mps_ptr mpsshape, const mps_ptr mpsscale, gmp_randstate_t state )
{
	mpfr_ptr subrop, scale, shape;
    mpfr_t p, b, tmp, pi;
    mpfr_t A, s;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsshape),
        "mpshape not a scalar in mps_rand_gam_gc()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsscale),
        "mpscale not a scalar in mps_rand_gam_gc()\n" );

    mpfr_init2( p, MPS_PREC(rop) );
    mpfr_init2( b, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );
    mpfr_init2( A, MPS_PREC(rop) );
    mpfr_init2( s, MPS_PREC(rop) );
    mpfr_init2( pi, MPS_PREC(rop) );

    shape = mps_get_ele( mpsshape, 1, 1 );
    scale = mps_get_ele( mpsscale, 1, 1 );

    MPS_ASSERT_MSG( mpfr_cmp_ui( shape, 1 ) > 0,
        "Shape parameter is less than or equal to 1 in mps_rand_gam_gc()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( scale ) > 0,
        "Scale parameter is negative or zero in mps_rand_gam_gc()\n" );

    mpfr_const_pi( pi, GMP_RNDN );

    mpfr_sub_ui( b, shape, 1, GMP_RNDN ); /* b = a - 1 */
    mpfr_add( A, shape, b, GMP_RNDN ); /* A = a + b */
    mpfr_sqrt( s, A, GMP_RNDN ); /* s = A**(1/2) */

    /* Algorithm GC for a > 1. */
	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        do
        {
            mpfr_urandom( p, state, GMP_RNDZ );
            
            /* t = s*tan(pi(u-0.5)) */
            mpfr_sub_d( p, p, 0.5, GMP_RNDN );
            mpfr_mul( p, p, pi, GMP_RNDN );
            mpfr_tan( p, p, GMP_RNDN );
            mpfr_mul( p, p, s, GMP_RNDN );

            /* x = b + t. */
            mpfr_add( subrop, b, p, GMP_RNDN );

            /* First rejection check. */
            if( mpfr_sgn( subrop ) < 0 )
                continue;

            /* t + ln(1 + (t**2)/A ) */
            mpfr_sqr( tmp, p, GMP_RNDN );
            mpfr_div( tmp, tmp, A, GMP_RNDN );
            mpfr_add_ui( tmp, tmp, 1, GMP_RNDN );
            mpfr_log( tmp, tmp, GMP_RNDN );
            mpfr_add( tmp, tmp, p, GMP_RNDN );

            /* b*ln(x/b) */
            mpfr_div( p, subrop, b, GMP_RNDN );
            mpfr_log( p, p, GMP_RNDN );
            mpfr_mul( p, p, b, GMP_RNDN );

            /* exp(... - ...)  */
            mpfr_sub( tmp, p, tmp, GMP_RNDN );
            mpfr_exp( tmp, tmp, GMP_RNDN );

            /* Rejection test. */
            mpfr_urandom( p, state, GMP_RNDZ );

            if( mpfr_lessequal_p(p, tmp) )
                break;

        } while(1);
     
        /* Applying scale parameter. */
        mpfr_mul( subrop, subrop, scale, GMP_RNDN );       
    }

    mpfr_clear( p );
    mpfr_clear( b );
    mpfr_clear( tmp );
    mpfr_clear( A );
    mpfr_clear( s );
    mpfr_clear( pi );

    return 0;
}


int mps_rand_gam_gc_double( const mps_ptr rop, double shape, double scale, gmp_randstate_t state )
{
    mps_t mpsshape, mpsscale;

    MPS_ASSERT_MSG( shape > 1,
        "Shape parameter is less than or equal to 1 in mps_rand_gam_gc_double()\n" );

    MPS_ASSERT_MSG( scale > 0,
        "Scale parameter is negative or zero in mps_rand_gam_gc_double()\n" );

    mps_init( mpsshape, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsscale, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsshape, 1, 1 ), shape, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsscale, 1, 1 ), scale, GMP_RNDN );

    mps_rand_gam_gc( rop, mpsshape, mpsscale, state );

    mps_free( mpsshape );
    mps_free( mpsscale );

    return 0;
}


/* Generates a gamma distributed variable using the GO algorithm. */
int mps_rand_gam_go( const mps_ptr rop, const mps_ptr mpsshape, const mps_ptr mpsscale, gmp_randstate_t state )
{
	mpfr_ptr subrop, scale, shape;
    mpfr_t p, b, x, tmp, B, B2;
    mpfr_t u, V, sigma, sigma2, W, d;
    mpfr_t y, s, S;
	unsigned int i;
    const unsigned int matsize = MPS_SIZE(rop);

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsshape),
        "mpsmean not a scalar in mps_rand_gam()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsscale),
        "mpsvariance not a scalar in mps_rand_gam()\n" );

    shape = mps_get_ele( mpsshape, 1, 1 );
    scale = mps_get_ele( mpsscale, 1, 1 );

    MPS_ASSERT_MSG( mpfr_cmp_ui( shape, 3 ) >= 0,
        "Shape parameter is less than 3 in mps_rand_gam_go()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( scale ) > 0,
        "Scale parameter is negative or zero in mps_rand_gam_go()\n" );

    mpfr_init2( p, MPS_PREC(rop) );
    mpfr_init2( b, MPS_PREC(rop) );
    mpfr_init2( tmp, MPS_PREC(rop) );
    mpfr_init2( u, MPS_PREC(rop) );
    mpfr_init2( V, MPS_PREC(rop) );
    mpfr_init2( sigma, MPS_PREC(rop) );
    mpfr_init2( sigma2, MPS_PREC(rop) );
    mpfr_init2( W, MPS_PREC(rop) );
    mpfr_init2( d, MPS_PREC(rop) );

    mpfr_init2( x, MPS_PREC(rop) );
    mpfr_init2( y, MPS_PREC(rop) );
    mpfr_init2( s, MPS_PREC(rop) );
    mpfr_init2( S, MPS_PREC(rop) );

    mpfr_init2( B, MPS_PREC(rop) );
    mpfr_init2( B2, MPS_PREC(rop) );

    mpfr_sub_ui( u, shape, 1, GMP_RNDN ); /* u = a - 1 */
    mpfr_sqrt( V, shape, GMP_RNDN ); /* V = a**(1/2) */

    /* Precomputed value of B. */
    mpfr_set_str( B, "0.009572265238288886750919543157686391918774066406431575154378390663047", 10, GMP_RNDN );

    /* B2 = -ln((2*pi)**0.5) * B / (1-B) */
    mpfr_const_pi( tmp, GMP_RNDN );
    mpfr_mul_ui( tmp, tmp, 2, GMP_RNDN );
    mpfr_sqrt( tmp, tmp, GMP_RNDN );
    
    mpfr_ui_sub( B2, 1, B, GMP_RNDN );
    mpfr_div( B2, B, B2, GMP_RNDN );
    mpfr_mul( B2, B2, tmp, GMP_RNDN );
    mpfr_log( B2, B2, GMP_RNDN );
    mpfr_neg( B2, B2, GMP_RNDN );
    
    /* sigma**2 = a + (8/3)**(1/2) */
    mpfr_set_ui( sigma2, 8, GMP_RNDN );
    mpfr_div_ui( sigma2, sigma2, 3, GMP_RNDN );
    mpfr_sqrt( sigma2, sigma2, GMP_RNDN );
    mpfr_mul( sigma2, sigma2, V, GMP_RNDN );
    mpfr_add( sigma2, sigma2, shape, GMP_RNDN );

    /* sigma = sigma2**(1/2) */
    mpfr_sqrt( sigma, sigma2, GMP_RNDN );

    /* W = sigma2 / u */
    mpfr_div( W, sigma2, u, GMP_RNDN );

    /* d = 6**(1/2) * sigma */
    mpfr_set_ui( d, 6, GMP_RNDN );
    mpfr_sqrt( d, d, GMP_RNDN );
    mpfr_mul( d, d, sigma, GMP_RNDN );

    /* b = u + d */
    mpfr_add( b, u, d, GMP_RNDN );


	for ( i = 1; i <= matsize; i++ )
	{
        subrop = mps_get_ele_seq( rop, i );

        do
        {
            mpfr_urandom( p, state, GMP_RNDZ );

            if( mpfr_cmp(p, B) > 0 )
            {
                do
                {
                    /* Sample from the stantard normal distribution. */
                    mpfr_urandom( x, state, GMP_RNDN );
                    mpfr_urandom( y, state, GMP_RNDN );

                    /* Scale the [0,1] uniform distribution to [-1,1] */
                    mpfr_mul_d( x, x, 2, GMP_RNDN );
                    mpfr_sub_d( x, x, 1, GMP_RNDN );
                    mpfr_mul_d( y, y, 2, GMP_RNDN );
                    mpfr_sub_d( y, y, 1, GMP_RNDN );

                    /* s = x**2 + y**2 */
                    mpfr_mul( s, y, y, GMP_RNDN );
                    mpfr_mul( subrop, x, x, GMP_RNDN );
                    mpfr_add( s, s, subrop, GMP_RNDN );
                } while( mpfr_cmp_d( s, 1 ) >= 0 || mpfr_zero_p(s) );

                /* x*sqrt(-2*ln(s)/s) */
                mpfr_log( subrop, s, GMP_RNDN );
                mpfr_mul_d( subrop, subrop, -2, GMP_RNDN );
                mpfr_div( s, subrop, s, GMP_RNDN );
                mpfr_sqrt( s, s, GMP_RNDN);
                mpfr_mul( s, s, x, GMP_RNDN );

                /* x = u + sigma*s */
                mpfr_mul( subrop, sigma, s, GMP_RNDN );
                mpfr_add( subrop, subrop, u, GMP_RNDN );

                /* Rejection test. */
                if( mpfr_sgn(subrop) < 0 || mpfr_greater_p(subrop, b) )
                    continue;

                mpfr_urandom( p, state, GMP_RNDZ );

                /* S = s**2/2 */
                mpfr_sqr( S, s, GMP_RNDN );
                mpfr_div_d( S, S, 2, GMP_RNDN );

                if( mpfr_sgn(s) >= 0 )
                {
                    /* if u <= 1 - S*(W-1), !!!This could be pre-computed. */
                    mpfr_sub_ui( tmp, W, 1, GMP_RNDN );
                    mpfr_mul( tmp, tmp, S, GMP_RNDN );
                    mpfr_d_sub( tmp, 1, tmp, GMP_RNDN );
                    
                    if( mpfr_cmp( p, tmp ) <= 0 )
                        break;
                }
                else
                {
                    /* if u <= 1 - S*((1-2s/V)*W - 1) */
                    mpfr_mul_d( tmp, s, 2, GMP_RNDN );
                    mpfr_div( tmp, tmp, V, GMP_RNDN );
                    mpfr_ui_sub( tmp, 1, tmp, GMP_RNDN );
                    mpfr_mul( tmp, tmp, W, GMP_RNDN );
                    mpfr_sub_ui( tmp, tmp, 1, GMP_RNDN );
                    mpfr_mul( tmp, tmp, S, GMP_RNDN );
                    mpfr_ui_sub( tmp, 1, tmp, GMP_RNDN );

                    if( mpfr_cmp( p, tmp ) <= 0 )
                        break;
                }

                /* if ln(u) > u*(1 + ln(x/u)) - x + S */
                mpfr_log( p, p, GMP_RNDN );

                mpfr_div( tmp, subrop, u, GMP_RNDN );
                mpfr_log( tmp, tmp, GMP_RNDN );
                mpfr_add_ui( tmp, tmp, 1, GMP_RNDN );
                mpfr_mul( tmp, tmp, u, GMP_RNDN );
                mpfr_sub( tmp, tmp, subrop, GMP_RNDN );
                mpfr_add( tmp, tmp, S, GMP_RNDN );

                if( mpfr_cmp( p, tmp ) > 0 )
                    continue;
                else
                    break;
            }
            else
            {
                /* Sample from the exponential distribution. */
                mpfr_urandom( subrop, state, GMP_RNDN );

                mpfr_log( subrop, subrop, GMP_RNDN );
                mpfr_neg( subrop, subrop, GMP_RNDN );

                /* x = b*(1 + s/d) */
                mpfr_div( subrop, subrop, d, GMP_RNDN );
                mpfr_add_ui( subrop, subrop, 1, GMP_RNDN );
                mpfr_mul( subrop, subrop, b, GMP_RNDN );

                mpfr_urandom( p, state, GMP_RNDZ );

                /* if ln(u) > u*(2+ln(x/u) - x/b) + 3.7203284924588 - b - ln(sigma*d/b) */
                mpfr_div( tmp, subrop, u, GMP_RNDN );
                mpfr_log( tmp, tmp, GMP_RNDN );
                mpfr_add_ui( tmp, tmp, 2, GMP_RNDN );

                mpfr_div( x, subrop, b, GMP_RNDN );
                mpfr_sub( tmp, tmp, x, GMP_RNDN );
                mpfr_mul( tmp, tmp, u, GMP_RNDN );
                mpfr_add( tmp, tmp, B2, GMP_RNDN );
                mpfr_sub( tmp, tmp, b, GMP_RNDN );

                mpfr_mul( x, sigma, d, GMP_RNDN );
                mpfr_div( x, x, b, GMP_RNDN );
                mpfr_log( x, x, GMP_RNDN );
                mpfr_sub( tmp, tmp, x, GMP_RNDN );

                mpfr_log( p, p, GMP_RNDN );

                if( mpfr_cmp( p, tmp ) > 0 )
                    continue;
                else
                    break;
            }

        } while(1);

        /* Applying scale parameter. */
        mpfr_mul( subrop, subrop, scale, GMP_RNDN );        

    }
    
    mpfr_clear( p );
    mpfr_clear( b );
    mpfr_clear( tmp );
    mpfr_clear( u );
    mpfr_clear( V );
    mpfr_clear( sigma );
    mpfr_clear( sigma2 );
    mpfr_clear( W );
    mpfr_clear( d );

    mpfr_clear( x );
    mpfr_clear( y );
    mpfr_clear( s );
    mpfr_clear( S );

    mpfr_clear( B );
    mpfr_clear( B2 );

    return 0;
}


int mps_rand_gam_go_double( const mps_ptr rop, double shape, double scale, gmp_randstate_t state )
{
    mps_t mpsshape, mpsscale;

    MPS_ASSERT_MSG( shape >= 3,
        "Shape parameter is less than 3 in mps_rand_gam_go_double()\n" );

    MPS_ASSERT_MSG( scale > 0,
        "Scale parameter is negative or zero in mps_rand_gam_go_double()\n" );

    mps_init( mpsshape, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsscale, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsshape, 1, 1 ), shape, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsscale, 1, 1 ), scale, GMP_RNDN );

    mps_rand_gam_go( rop, mpsshape, mpsscale, state );

    mps_free( mpsshape );
    mps_free( mpsscale );

    return 0;
}


int mps_rand_gam( const mps_ptr rop, const mps_ptr mpsshape, const mps_ptr mpsscale, gmp_randstate_t state )
{
	mpfr_ptr scale, shape;

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsshape),
        "mpshape not a scalar in mps_rand_gam()\n" );

    MPS_ASSERT_MSG( MPS_IS_SCALAR(mpsscale),
        "mpscale not a scalar in mps_rand_gam()\n" );

    shape = mps_get_ele( mpsshape, 1, 1 );
    scale = mps_get_ele( mpsscale, 1, 1 );

    MPS_ASSERT_MSG( mpfr_sgn( shape ) > 0,
        "Shape parameter is negative or zero in mps_rand_gam()\n" );

    MPS_ASSERT_MSG( mpfr_sgn( scale ) > 0,
        "Scale parameter is negative or zero in mps_rand_gam()\n" );

    if( mpfr_cmp_d(shape, 3) >= 0 )
        mps_rand_gam_go( rop, mpsshape, mpsscale, state );
    else
        mps_rand_gam_gt( rop, mpsshape, mpsscale, state );

    return 0;
}


int mps_rand_gam_double( const mps_ptr rop, double shape, double scale, gmp_randstate_t state )
{
    mps_t mpsshape, mpsscale;

    MPS_ASSERT_MSG( shape > 0,
        "Shape parameter is less than or equal to 1 in mps_rand_gam_double()\n" );

    MPS_ASSERT_MSG( scale > 0,
        "Scale parameter is negative or zero in mps_rand_gam_double()\n" );

    mps_init( mpsshape, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );
    mps_init( mpsscale, 1, 1, MPS_PREC(rop), MPS_COL_ORDER );

    mpfr_set_d( mps_get_ele( mpsshape, 1, 1 ), shape, GMP_RNDN );
    mpfr_set_d( mps_get_ele( mpsscale, 1, 1 ), scale, GMP_RNDN );

    mps_rand_gam( rop, mpsshape, mpsscale, state );

    mps_free( mpsshape );
    mps_free( mpsscale );

    return 0;
}

