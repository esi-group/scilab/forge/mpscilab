/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Matrix elementwise exp and log demo. <demoexp.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_init( A, 4, 4, 64, MPS_COL_ORDER );
    mps_init( B, 4, 4, 64, MPS_COL_ORDER );

    /* Put some values into the matrices. */
	mps_fill_seq_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_exp(B, A, GMP_RNDN);
	print_info(B, "B", "= exp(A)");

	mps_exp2(B, A, GMP_RNDN);
	print_info(B, "B", "= exp2(A)");

	mps_exp10(B, A, GMP_RNDN);
	print_info(B, "B", "= exp10(A)");

	mps_log(B, A, GMP_RNDN);
	print_info(B, "B", "= ln(A)");

	mps_log2(B, A, GMP_RNDN);
	print_info(B, "B", "= log2(A)");

	mps_log10(B, A, GMP_RNDN);
	print_info(B, "B", "= log10(A)");

    /* Free the allocated memory. */
    mps_frees( A, B, (mps_ptr) NULL );

    return 0;
}

