/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps_priv.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A;
	mpfr_t norm;
	mps_vec_ptr vector;
	mps_blk_t row;

    printf("Program start!\n");
    printf("nrm2 demo. <demonrm2.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);
	vector = mps_new_vec(4, PREC);
	mpfr_init2(norm, PREC);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_si_vec(vector, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);
	print_block_info(vector, "vector", NULL);

	mpfr_printf("Trying mps_nrm2_blk on vector\n");
	mps_nrm2_blk(norm, vector, GMP_RNDN);

	print_info(A, "A", NULL);
	print_block_info(vector, "vector", NULL);
	print_info_sca(norm, "norm", NULL);

	mps_row_blk(row, A, 3);
	print_block_info(row, "row", NULL);
	mpfr_printf("Trying mps_nrm2_blk on row\n");
	mps_nrm2_blk(norm, row, GMP_RNDN);
	print_block_info(row, "row", NULL);
	print_info_sca(norm, "norm", NULL);

	mps_destroy_vec(vector);
    mps_frees( A, (mps_ptr) NULL);
	mpfr_clear(norm);

    return 0;
}
