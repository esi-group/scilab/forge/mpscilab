/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A, B, D;
	mpfr_t C;

    printf("Program start!\n");
    printf("Central moments demo. <democentralmoments.c>\n\n");

	mpfr_init2(C, PREC);

    mps_init( A, 4, 4, PREC, MPS_COL_ORDER );
    mps_init( B, 1, 4, PREC, MPS_COL_ORDER );   /* row vector */
    mps_init( D, 4, 1, PREC, MPS_COL_ORDER );   /* column vector */

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

    print_info(A, "A", NULL);
    mps_central_moment(C, A, 2, GMP_RNDN);
    print_info_sca(C, "C", "central moment 2 of A");

    mps_cmoments(D, A, 2, 'c', GMP_RNDN);
    print_info(D, "D", "cmoments 2 'c' of A");

    mps_cmoments(B, A, 2, 'r', GMP_RNDN);
    print_info(B, "B", "cmoments 2 'r' of A");

	mps_frees(A, B, D, (mps_ptr) NULL);
	mpfr_clear( C );

    return 0;
}

