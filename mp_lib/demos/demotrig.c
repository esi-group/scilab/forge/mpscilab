/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Matrix elementwise trig functions demo. <demotrig.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_sin(B, A, GMP_RNDN);
	print_info(B, "B", "= sin(A)");

	mps_cos(B, A, GMP_RNDN);
	print_info(B, "B", "=  cos(A)");

	mps_tan(B, A, GMP_RNDN);
	print_info(B, "B", "=  tan(A)");


	mps_asin(B, A, GMP_RNDN);
	print_info(B, "B", "=  sin^-1(A)");

	mps_acos(B, A, GMP_RNDN);
	print_info(B, "B", "=  cos^-1(A)");

	mps_atan(B, A, GMP_RNDN);
	print_info(B, "B", "=  tan^-1(A)");


	mps_csc(B, A, GMP_RNDN);
	print_info(B, "B", "= csc(A)");

	mps_sec(B, A, GMP_RNDN);
	print_info(B, "B", "= sec(A)");

	mps_cot(B, A, GMP_RNDN);
	print_info(B, "B", "= cot(A)");


	mps_sinh(B, A, GMP_RNDN);
	print_info(B, "B", "= sinh(A)");

	mps_cosh(B, A, GMP_RNDN);
	print_info(B, "B", "= cosh(A)");

	mps_tanh(B, A, GMP_RNDN);
	print_info(B, "B", "= tanh(A)");


	mps_asinh(B, A, GMP_RNDN);
	print_info(B, "B", "= asinh(A)");

	mps_acosh(B, A, GMP_RNDN);
	print_info(B, "B", "= acosh(A)");

	mps_atanh(B, A, GMP_RNDN);
	print_info(B, "B", "= atanh(A)");


	mps_csch(B, A, GMP_RNDN);
	print_info(B, "B", "= csch(A)");

	mps_sech(B, A, GMP_RNDN);
	print_info(B, "B", "= sech(A)");

	mps_coth(B, A, GMP_RNDN);
	print_info(B, "B", "= coth(A)");


    mps_frees( A, B, (mps_ptr) NULL );

    return 0;
}

