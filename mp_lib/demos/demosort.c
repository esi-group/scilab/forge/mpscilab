/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
	/* TODO: Make this less of a mess */
    mps_t A, B, C, D, E, F, G, H, I, J, K, L, M;
	int rc;

    printf("Program start!\n");
    printf("Matrix sort. <demosort.c>\n\n");

    mps_inits(4, 4, PREC, MPS_COL_ORDER, A, C, I, (mps_ptr) NULL );
    mps_inits( 3, 4, PREC, MPS_COL_ORDER, B, D, (mps_ptr) NULL );
    mps_init(E, 0, 0, PREC, MPS_COL_ORDER);
    mps_init(J, 3, 3, PREC, MPS_COL_ORDER);
    mps_inits( 1, 100, PREC, MPS_COL_ORDER, F, G, L, M, (mps_ptr) NULL);
    mps_inits( 1, 155, PREC, MPS_COL_ORDER, H, K, (mps_ptr) NULL);
	/* 252x252, 690x690  1x19231*/

	/* a pair to test quick sort */
    mps_str_input( A, "16 6 9 3; 2 1 5 7; 15 8 14 10; 13 11 12 4;", PREC );
    mps_str_input( B, "12 6 9 3; 2 1 5 7; 11 8 4 10;", PREC );

	/* a pair to test heap sort */
    mps_str_input( C, "16 6 9 3; 2 1 5 7; 15 8 14 10; 13 11 12 4;", PREC );
    mps_str_input( D, "12 6 9 3; 2 1 5 7; 11 8 4 10;", PREC );


	/* FIXME: mps_copy is broken, so the test will fail anyways */
	mps_random(F, 1, 100, GMP_RNDN);
	mps_copy(M, F);
	mps_random(G, 1, 100, GMP_RNDN);
	mps_copy(L, G);
	mps_random(J, 1, 100, GMP_RNDN);
	mps_random(I, 1, 100, GMP_RNDN);

	/* this one used to break it. */
	mps_str_input(H, "62 36 67 68 4 68 24 35 97 91 90 56 15 21 100 20 29 31 71 44 25 7 85 97 19 58 49 93 80 60 31 41 47 49 61 50 69 36 84 65 26 25 72 92 46 71 63 74 1 85 69 77 91 6 25 61 63 74 53 42 85 36 35 32 36 95 33 4 30 69 69 7 93 92 98 90 63 12 16 15 96 84 44 38 41 68 98 4 93 3 97 30 90 83 13 25 77 45 29 58 13 49 16 58 40 65 47 54 76 62 69 71 98 64 8 90 83 58 45 76 12 42 57 1 76 69 77 5 13 5 62 78 53 78 35 45 42 33 98 70 47 18 40 44 81 100 85 64 9 30 91 20 23 47 72;", 64 );
	print_info(H, "H", "Initial");
	mps_clone(K, H);

	/* heap sort */
	print_info(A, "A", "Unsorted");
	mps_heapsort_col(A);
	print_info(A, "A", "After heap sorting");

	/* test reversing for even size */
	print_info(A, "A", "Sorted");
	mps_reverse(A);
	print_info(A, "A", "Reversed");

	print_info(B, "B", "Unsorted");
	mps_heapsort_col(B);
	print_info(B, "B", "After heap sorting");

	/* test reversing for odd size */
	print_info(B, "B", "Sorted");
	mps_reverse(B);
	print_info(B, "B", "Reversed");
	/* quicksort */
	print_info(C, "C", "Unsorted");
	mps_quicksort_col(C, GMP_RNDN);
	print_info(C, "C", "After quick sorting");
	print_info(D, "D", "Unsorted");
	mps_quicksort_col(D, GMP_RNDN);
	print_info(D, "D", "After quick sorting");

	/* show sorting a sorted matrix remains unchanged */
	print_info(B, "B", "To be resorted");
	mps_quicksort_col(B, GMP_RNDN);
	print_info(B, "B", "After quick sorting");

	print_info(D, "D", "To be resorted");
	mps_heapsort_col(D);
	print_info(D, "D",  "After heap sorting");

	/* test for 0 x 0 matrix breaking things */
	print_info(E, "E", "Empty, 0 x 0");
	mps_heapsort_col(E);
	print_info(E, "E", "Empty matrix after heap sorting");

	print_info(E, "E", "Empty, 0 x 0");
	mps_quicksort_col(E, GMP_RNDN);
	print_info(E, "E", "Empty matrix after quick sorting");

	/* test for random, big sizes */
	/* I thought it wasn't working, turns out just takes forever to print. */
	printf("Matrix F: Too big to print\n");
	mps_heapsort_col(F);

	if ( (rc = mps_check_sort(F, M)) )
		printf("Matrix F: Heap sort failed! : %d\n\n", rc);
	else
		printf("Matrix F: Heap sort success!\n\n");

	printf("Matrix G: Too big to print\n");
	mps_quicksort_col(G, GMP_RNDN);
	if ( (rc = mps_check_sort(G, L)) )
		printf("Matrix G: Quick sort failed! : %d\n\n", rc);
	else
		printf("Matrix G: Quick sort success!\n\n");

	print_info(H, "H", NULL);
	mps_quicksort_col(H, GMP_RNDN);
	print_info(H, "H", "Sorted");
	if ( (rc = mps_check_sort(H, K)) )
		printf("Matrix H: Quick sort failed! : %d\n\n", rc);
	else
		printf("Matrix H: Quick sort success!\n\n");

	/* check for per column sorting */
	print_info(I, "I", "Unsorted");
	mps_quicksort_each_col(I, GMP_RNDN);
	print_info(I, "I", "Sorted by column");

	print_info(J, "J", "Unsorted (odd # elements)");
	mps_quicksort_each_col(J, GMP_RNDN);
	print_info(J, "J", "Sorted by column");

    mps_frees( A, B, C, D, E, F, G, H, I, J, K, L, M, (mps_ptr) NULL );

    return 0;
}

