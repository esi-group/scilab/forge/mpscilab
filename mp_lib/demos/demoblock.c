/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B, C;
	mps_blk_t p, q;
	mpfr_ptr a;
	char* buf;

    printf("Program start!\n");
    printf("Block matrix demo. <demoblock.c>\n\n");

    mps_init( A, 4, 4, 64, MPS_COL_ORDER);
    mps_init( B, 4, 4, 64, MPS_ROW_ORDER);
    mps_init( C, 5, 5, 64, MPS_ROW_ORDER);

	mps_fill_seq_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_si(B, 1, 1, GMP_RNDN);
	mps_fill_seq_si(C, 1, 1, GMP_RNDN);

	mps_submat(p, A, 2, 2, 2, 2);
	mps_submat(q, C, 2, 3, 2, 3);

	a = mps_get_ele_blk(p, 1, 1);

	print_info(A, "A", NULL);
	print_block_info(p, "p", "2, 2, 2, 2");

	mpfr_printf("Sequential access test of p in A:\n");
	buf = mps_print_seq_blk(p, 5);
	puts(buf);
	free(buf);

	print_info(C, "C", NULL);
	print_block_info(q, "q", NULL);

	mpfr_printf("Sequential access test of q in C:\n");
	buf = mps_print_seq_blk(q, 5);
	puts(buf);
	free(buf);

    mps_frees( A, B, C, (mps_ptr) NULL);

    return 0;
}

