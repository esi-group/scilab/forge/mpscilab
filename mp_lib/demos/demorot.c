/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B, C, D, E, F, G, H, I, J, K, L, M, N;

    printf("Program start!\n");
    printf("Matrix rotation demo. <demorot.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL );
	mps_inits( 3, 5, 64, MPS_COL_ORDER, C, D, (mps_ptr) NULL );
	mps_inits( 3, 6, 64, MPS_COL_ORDER, E, F, (mps_ptr) NULL );
	mps_inits( 6, 3, 64, MPS_COL_ORDER, G, H, (mps_ptr) NULL );
	mps_inits( 3, 3, 64, MPS_COL_ORDER, I, J, (mps_ptr) NULL );
	mps_inits( 5, 5, 64, MPS_COL_ORDER, K, L, (mps_ptr) NULL );
	mps_inits( 5, 7, 64, MPS_COL_ORDER, M, N, (mps_ptr) NULL );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(C, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(E, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(G, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(I, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(K, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(M, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_shift_row( B, A, 1, GMP_RNDN);
	print_info(B, "B", "= A row shifted 1");
	mps_shift_row( B, A, -2, GMP_RNDN);
	print_info(B, "B", "= A row shifted -2");
	mps_shift_col( B, A, 1, GMP_RNDN);
	print_info(B, "B", "= A col shifted 1");
	mps_mat_rotate_cw( B, A, GMP_RNDN);
	print_info(B, "B", "= A rotated clockwise 1");
	print_info(C, "C", NULL);
	mps_mat_rotate_cw( D, C, GMP_RNDN);
	print_info(D, "D", "= C rotated clockwise 1");

	print_info(E, "E", NULL);
	mps_mat_rotate_cw( F, E, GMP_RNDN);
	print_info(F, "F", "= E rotated clockwise 1");

	print_info(G, "G", NULL);
	mps_mat_rotate_cw( H, G, GMP_RNDN);
	print_info(H, "H", "= G rotated clockwise 1");

	print_info(I, "I", NULL);
	mps_mat_rotate_cw( J, I, GMP_RNDN);
	print_info(J, "J", "= I rotated clockwise 1");

	print_info(K, "K", NULL);
	mps_mat_rotate_cw( L, K, GMP_RNDN);
	print_info(L, "L", "= K rotated clockwise 1");

	print_info(M, "M", NULL);
	mps_mat_rotate_cw( N, M, GMP_RNDN);
	print_info(N, "N", "= M rotated clockwise 1");

    mps_frees( A, B, C, D, E, F, G, H, I, J, K, L, M, N, (mps_ptr) NULL );

    return 0;
}

