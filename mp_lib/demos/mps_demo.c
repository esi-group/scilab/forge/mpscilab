/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <stdio.h>

#include "mps.h"
#include "mps_demo.h"

/* print matrix information */
void print_info(mps_ptr item, char* name, char* desc)
{
	char* str;
    str = mps_printbuf_overview( item, 7 );
	if (desc)
		printf("Matrix %s: %s\n", name, desc);
	else
		printf("Matrix %s:\n", name);
	printf("%s\n\n", str);
	free(str);
}

/* print scalar information */
void print_info_sca(mpfr_ptr item, char* name, char* desc)
{
	if (desc)
	{
		printf("Scalar %s: %s\n", name, desc);
		mpfr_printf("\t%.7RNg\n", item);
	}
	else
	{
		mpfr_printf("Scalar %s: %.7RNg\n\n", name, item);
	}
}

/* print info about a submatrix */
void print_block_info(mps_blk_ptr item, char* name, char* desc)
{
	char* str;
    str = mps_printbuf_overview_blk( item, 7 );
	if (desc)
		printf("Block matrix %s: %s\n", name, desc);
	else
		printf("Block matrix %s:\n", name);
	printf("%s\n\n", str);
	free(str);
}

