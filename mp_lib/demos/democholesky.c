/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Cholesky decomp demo. <democholesky.c>\n\n");

    mps_inits( 2, 2, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);
    mps_inits( 4, 4, PREC, MPS_COL_ORDER, B, (mpfr_ptr) NULL);

    mps_str_input(A, "2 1; 1 2;", PREC);
    mps_str_input(B, "1 1 1 1; 1 5 5 5; 1 5 14 14; 1 5 14 15;", PREC);

	print_info(A, "A", NULL);
	mps_cholesky_decomp(A, GMP_RNDN);
	print_info(A, "A", "After Cholesky decomposition");

    print_info(B, "B", NULL);
	mps_cholesky_decomp(B, GMP_RNDN);
	print_info(B, "B", "After Cholesky decomposition");

    mps_frees( A, B, (mps_ptr) NULL);

    return 0;
}

