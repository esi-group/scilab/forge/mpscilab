/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A;

    printf("Program start!\n");
    printf("FFT demo. <demofft.c>\n\n");

    mps_inits( 1, 16, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", "Initial A");

	mps_real_fft_dit( A, GMP_RNDN);

	print_info(A, "A", "Initial A");

    mps_frees( A, (mps_ptr) NULL);

    return 0;
}

