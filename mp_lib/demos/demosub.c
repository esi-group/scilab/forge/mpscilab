/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;
	mpfr_t C;

    printf("Program start!\n");
    printf("Matrix elementwise multiplication demo. <demosub.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL);
	mpfr_init_set_ui(C, 5, GMP_RNDN);

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);

    /* Print them. */
	print_info(A, "A", NULL);
	print_info(B, "B", NULL);
	print_info_sca(C, "C", NULL);

    /* Perform matrix subtraction B = A - B */
    mps_sub( B, A, B, GMP_RNDN );

    /* Print the result. */
	print_info(B, "B", "= Matrix A - B");

    /* Reset values in the matrices. */
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);

	mps_sub_scalar_double(A, B, 3.0, GMP_RNDN);
	print_info(A, "A", "= B - 3.0");

	mps_scalar_double_sub(A, 3.0, B, GMP_RNDN);
	print_info(A, "A", "= 3.0 - B");

	mps_sub_scalar(A, B, C, GMP_RNDN);
	print_info(A, "A", "= B - C");

	mps_scalar_sub(A, C, B, GMP_RNDN);
	print_info(A, "A", "= C - B");

#if 0	/* the double matrix stuff isn't right */
	printf("A = B - D\n");
	mps_sub_d(A, B, D, GMP_RNDN);
	string = mps_printbuf_overview(A, 5);
	puts(string);
	free(string);

	printf("A = D - B\n");
	mps_d_sub(A, D, B, GMP_RNDN);
	string = mps_printbuf_overview(A, 5);
	puts(string);
	free(string);
#endif

    /* Free the allocated memory. */
    mps_frees( A, B, (mps_ptr) NULL );
	mpfr_clear( C );

    return 0;
}

