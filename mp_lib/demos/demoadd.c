/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Matrix addition demo. <demoadd.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits(4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL);

    /* Put some values into the matrices. */
	mps_fill_seq_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_si(B, 1, 1, GMP_RNDN);

    /* Print them. */
	print_info(A, "A", NULL);
	print_info(B, "B", NULL);

    /* Perform matrix addition. B = A + B */
    mps_add( B, A, B, GMP_RNDN );

    /* Print the result. */
	print_info(B, "B", "= Matrix A + B");

    /* Free the allocated memory. */
    mps_frees( A, B, (mps_ptr) NULL );

    return 0;
}

