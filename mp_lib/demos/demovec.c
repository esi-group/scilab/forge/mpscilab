/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A;
	mps_vec_t p, r, s;
	mps_vec_ptr q;
	mpfr_ptr subq;
	unsigned int i, size;

    printf("Program start!\n");
    printf("Vector type demo. <demovec.c>\n\n");

    mps_init( A, 5, 5, 64, MPS_COL_ORDER);
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_subvec(p, A, 3);
	print_block_info(p, "p", "Subvector of A, column 3");

	mps_subvec_partial(s, A, 4, 2, 5);
	print_block_info(s, "s", "Partial subvector of A");

	mps_subsubvec(r, s, 2, 1);
	print_block_info(r, "r", "Subsubvector of A");

	q = mps_new_vec(3, 64);
	size = MPS_VEC_SIZE(q);
	for ( i = 1; i <= size; ++i )
	{
		subq = mps_get_ele_vec(q, i);
		mpfr_set_ui(subq, i, GMP_RNDN);
	}

	print_block_info(q, "q", "New filled vector");
	mps_destroy_vec(q);

    mps_frees( A, (mps_ptr) NULL);

    return 0;
}

