/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Matrix minors demo. <demominor.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_init( A, 4, 4, 64, MPS_COL_ORDER );
    mps_init( B, 3, 3, 64, MPS_COL_ORDER );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_mat_minor(B, A, 2, 2, GMP_RNDN);
	print_info(B, "B", "= A_(2,2)");

	mps_mat_minor(B, A, 1, 4, GMP_RNDN);
	print_info(B, "B", "= A_(1,4)");

	mps_mat_minor(B, A, 3, 1, GMP_RNDN);
	print_info(B, "B", "= A_(3,1)");

    mps_free( A );
    mps_free( B );

    return 0;
}

