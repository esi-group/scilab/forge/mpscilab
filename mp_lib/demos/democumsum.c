/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Cumulative sum demo. <democumsum.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_init( A, 4, 4, 64, MPS_COL_ORDER );
    mps_init( B, 4, 4, 64, MPS_COL_ORDER );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_col_cumsum(B, A, GMP_RNDN);
	print_info(B, "B", "= col cumsum A");

	mps_row_cumsum(B, A, GMP_RNDN);
	print_info(B, "B", "= row cumsum A");

    mps_frees( A, B, (mps_ptr) NULL);

    return 0;
}

