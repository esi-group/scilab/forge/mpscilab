/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;
	mpfr_t C;

    printf("Program start!\n");
    printf("Matrix elementwise multiplication demo. <demomul.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_init( A, 4, 4, 64, MPS_COL_ORDER );
    mps_init( B, 4, 4, 64, MPS_COL_ORDER );
	mpfr_init_set_ui(C, 5, GMP_RNDN);

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);

    /* Print them. */
	print_info(A, "A", NULL);
	print_info(B, "B", NULL);
	print_info_sca(C, "C", NULL);

    /* Perform matrix addition. B = A + B */
    mps_mul( B, A, B, GMP_RNDN );
	print_info(B, "B", "= Matrix A.*B");

	mps_mul_scalar(A, A, C, GMP_RNDN);
	print_info(A, "A", "= Matrix A * C");

	mps_mul_scalar_double(A, A, (double) 3.0, GMP_RNDN);
	print_info(A, "A", "=  A * 3.0 (Double)");

	/* TODO: Test the double stuff */

    /* Free the allocated memory. */
    mps_frees( A, B, (mps_ptr) NULL );
	mpfr_clear( C );

    return 0;
}

