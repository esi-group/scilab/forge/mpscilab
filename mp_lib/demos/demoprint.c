/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 1000
#define DIGITS 100
#define FORMAT 'v'
#define ALWAYS_SIGN 0

int main()
{
    mps_t A, B, C, D;
    mpfr_t bignum;
    char* buf;

    printf("Program start!\n");
    printf("Printing functions demo. <demoprint.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, C, (mps_ptr) NULL );
    mps_inits( 8, 8, PREC, MPS_COL_ORDER, B, D, (mps_ptr) NULL );

    mpfr_init2(bignum, PREC);

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
    mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);

    mps_print_format(A, DIGITS, FORMAT, ALWAYS_SIGN);
    printf("\n");
    mps_print_format(B, DIGITS, FORMAT, ALWAYS_SIGN);
    printf("\n");

    mpfr_set_str(bignum, "10000", 10, GMP_RNDN);
    mps_mul_scalar(A, A, bignum, GMP_RNDN);
    mps_print_format(A, DIGITS, FORMAT, ALWAYS_SIGN);

	mps_sin(C, A, GMP_RNDN);
	mps_cos(D, B, GMP_RNDN);

    mps_print_format(C, DIGITS, FORMAT, ALWAYS_SIGN);
    printf("\n");
    mps_print_format(D, DIGITS, FORMAT, ALWAYS_SIGN);
    printf("\n");

	mps_tan(C, A, GMP_RNDN);
    mps_print_format(C, DIGITS, FORMAT, ALWAYS_SIGN);
    mpfr_printf("\n");

    mps_div_scalar_double(C, C, 1000, GMP_RNDN);
    mps_print_format(C, DIGITS, FORMAT, ALWAYS_SIGN);
    mpfr_printf("\n");

    mpfr_set_str(bignum, "1e9001", 10, GMP_RNDN);
    mps_div_scalar(C, C, bignum, GMP_RNDN);
    mps_print_format(C, DIGITS, FORMAT, ALWAYS_SIGN);

    mpfr_printf("\n");

    mps_mul_scalar(C, C, bignum, GMP_RNDN);
    mps_mul_scalar(C, C, bignum, GMP_RNDN);
    mps_print_format(C, DIGITS, FORMAT, ALWAYS_SIGN);

    mpfr_printf("\nPrinting from buffer:\n");
    buf = mps_print_format_buf(C, DIGITS, FORMAT, ALWAYS_SIGN);
    mpfr_printf("%s\n", buf);
    free(buf);

    mps_frees( A, B, (mps_ptr) NULL );
    mpfr_clear(bignum);

    return 0;
}

