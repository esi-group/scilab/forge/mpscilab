/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <stdio.h>

#include "mps.h"

void print_info(mps_ptr, char*, char*);
void print_info_sca(mpfr_ptr, char*, char*);
void print_block_info(mps_blk_ptr, char*, char*);

