/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;
	mpfr_t item;
	mpfr_t low, high;
	unsigned int res, count, i;
	unsigned int rng[2];
	unsigned int* found;
	char once = 1;

    printf("Program start!\n");
    printf("Matrix searching demo. <demosearch.c>\n\n");

    /* Allocate and initialize three 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL );
	mpfr_inits2(64, item, low, high, (mpfr_ptr) NULL);

    /* Put some values into the matrices. */
	mps_fill_seq_si(A, 1, 3, GMP_RNDN);
	mps_random(B, 1, 10, GMP_RNDN);

	print_info(A, "A", NULL);
	/* test linear search on sorted matrix */
	mpfr_set_ui(item, 10, GMP_RNDN);
	res = mps_linsearch(A, item);
	mpfr_printf("Linear search for %2.RNg in A returned index %u\n\n", item, res);

	/* test binary search */
	res = mps_binsearch(A, item);
	mpfr_printf("Binary search for %2.RNg in A returned index %u\n", item, res);
	mpfr_set_ui(item, 1, GMP_RNDN);		/* test finding first item */
	res = mps_binsearch(A, item);
	mpfr_printf("Binary search for %2.RNg in A returned index %u\n", item, res);
	mpfr_set_ui(item, 46, GMP_RNDN);	/* test finding last item */
	res = mps_binsearch(A, item);
	mpfr_printf("Binary search for %2.RNg in A returned index %u\n", item, res);
	mpfr_set_ui(item, 45, GMP_RNDN);	/* should not be found */
	res = mps_binsearch(A, item);
	mpfr_printf("Binary search for %2.RNg in A returned index %u\n\n", item, res);
	mpfr_set_ui(item, 10, GMP_RNDN);

testRange:
	print_info(A, "A", NULL);
	/* test binary search in a range */
	mpfr_set_ui(low, 0, GMP_RNDN);		/* show searching beyond the range of the matrix */
	mpfr_set_ui(high, 400, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);
	mpfr_set_ui(low, 1, GMP_RNDN);		/* show searching at ends */
	mpfr_set_ui(high, 46, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);
	mpfr_set_ui(low, 8, GMP_RNDN);		/* between values not in the matrix */
	mpfr_set_ui(high, 20, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);
	mpfr_set_ui(low, 19, GMP_RNDN);		/* between values in the matrix */
	mpfr_set_ui(high, 31, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);
	mpfr_set_ui(low, 200, GMP_RNDN);	/* between values beyond matrix */
	mpfr_set_ui(high, 300, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);
	mpfr_set_ui(low, 1, GMP_RNDN);		/* between a single value */
	mpfr_set_ui(high, 1, GMP_RNDN);
	mps_binsearch_range(A, low, high, rng);
	mpfr_printf("Binary range search in range %2.RNg to %2.RNg in A returned index %u %u\n", low, high, rng[0], rng[1]);

	mps_ones(A, GMP_RNDN);
	if (once)
	{
		once = 0;
		goto testRange;		/* lazy */
	}

	print_info(B, "B", "Random matrix");
	res = mps_linsearch(B, item);
	mpfr_printf("Linear search for %2.RNg in B returned index %u\n\n", item, res);

	count = mps_linsearch_all(B, item, &found);
	mpfr_printf("Linear search for all %2.RNg in B returned %u items:\n", item, count);
	for ( i = 0; i < count; ++i )
		mpfr_printf("\t%u\n", found[i]);
	mpfr_printf("\n");
	free(found);

    mps_frees( A, B, (mps_ptr) NULL);
	mpfr_clears(item, low, high, (mpfr_ptr) NULL);

    return 0;
}

