/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;

    printf("Program start!\n");
    printf("Matrix reflection demo. <demorefl.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mpfr_ptr) NULL );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

	mps_reflect_x( B, A, GMP_RNDN);
	print_info(B, "B", "= A reflected about x");

	mps_reflect_y( B, A, GMP_RNDN);
	print_info(B, "B", "= A reflected about y");

	mps_reflect_diag_lr( B, A, GMP_RNDN);
	print_info(B, "B", "= A reflected diagonally 1st diagonal");

	mps_reflect_diag_rl( B, A, GMP_RNDN);
	print_info(B, "B", "= A reflected diagonally 2nd diagonal");

    mps_frees( A, B, (mps_ptr) NULL );

    return 0;
}

