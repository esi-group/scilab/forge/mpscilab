/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A, B, C, D, E;
	mps_t A2, B2, C2;

    printf("Program start!\n");
    printf("Convolution demo. <democonvolve.c>\n\n");

	mps_inits( 1, 16, PREC, MPS_COL_ORDER, A, B, (mps_ptr) NULL );
	mps_inits( 1, 31, PREC, MPS_COL_ORDER, C, (mps_ptr) NULL );
	mps_init( D, 1, 5, PREC, MPS_COL_ORDER );
	mps_init( E, 1, 20, PREC, MPS_COL_ORDER );

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(D, 1, 1, GMP_RNDN);

	mps_convolution(C, A, B, GMP_RNDN);
	print_info(C, "C", "= A * B");

	mps_convolution(E, A, D, GMP_RNDN);
	print_info(E, "E", "= A * D");

	/* Test 2D convolution */
	mps_inits(4, 4, PREC, MPS_COL_ORDER, A2, B2, C2, (mps_ptr) NULL);

	mps_fill_seq_row_si(A2, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B2, 1, 1, GMP_RNDN);
#if 0
	mps_convolution2d(C2, A2, B2, GMP_RNDN);
	print_info(C2, "C2", "C2 = A2 * B2");
#endif

	mps_frees( A, B, C, A2, B2, C2, (mps_ptr) NULL );

    return 0;
}

