/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64
#define NLAGS 5

int main()
{
    mps_t A, B, C, D, E;

    printf("Program start!\n");
    printf("Correlation demo. <democorrelate.c>\n\n");

	mps_inits( 1, 16, PREC, MPS_COL_ORDER, A, B, (mps_ptr) NULL );
	mps_inits( 1, NLAGS, PREC, MPS_COL_ORDER, C, (mps_ptr) NULL );
	mps_init( D, 1, 20, PREC, MPS_COL_ORDER);
	mps_init( E, 1, NLAGS, PREC, MPS_COL_ORDER);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(D, 1, 1, GMP_RNDN);

	mps_correlation(C, A, B, NLAGS, GMP_RNDN);
	print_info(C, "C", "= A correlation B");

	mps_frees( A, B, C, (mps_ptr) NULL );

#if 0
	mps_t A2, B2, C2;
	/* Test 2D correlation */
	mps_inits(4, 4, PREC, 0, A2, B2, C2, (mps_ptr) NULL);

	mps_fill_seq_row_si(A2, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B2, 1, 1, GMP_RNDN);

	mps_correlation2d(C2, A2, B2, GMP_RNDN);
	print_info(C2, "C2", "C2 = A2 * B2");

	mps_frees( A2, B2, C2, (mps_ptr) NULL );
#endif

    return 0;
}

