/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B, C;
	mps_t iden;
	unsigned int nrot = 0;

    printf("Program start!\n");
    printf("Jacobi eigenvalue demo. <demojacobi.c>\n\n");

    mps_init( A, 4, 4, 64, MPS_COL_ORDER );		/* original matrix */
    mps_init( B, 4, 4, 64, MPS_COL_ORDER );		/* eigenmatrix */
    mps_init( C, 1, 4, 64, MPS_COL_ORDER );		/* column vector, the eigenvalues */

    mps_init( iden, 4, 4, 64, MPS_COL_ORDER );

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);
	mps_eigen_jacobi(A, C, B, 10, &nrot, GMP_RNDN);
	print_info(C, "C", "= Eigenvalues of A");
	print_info(B, "B", "= Eigenvector matrix of A");

	mps_str_input(A, "8 -1 3 -1; -1 6 2 0; 3 2 9 1; -1 0 1 7;", 64 );
	print_info(A, "A", NULL);
	mps_eigen_jacobi(A, C, B, 10, &nrot, GMP_RNDN);
	print_info(C, "C", "= Eigenvalues of A");
	print_info(B, "B", "= Eigenvector matrix of A");

	mps_identity(iden, GMP_RNDN);
	print_info(iden, "iden", NULL);
	mps_eigen_jacobi(iden, C, B, 10, &nrot, GMP_RNDN);
	print_info(C, "C", "= Eigenvalues of identity");
	print_info(B, "B", "= Eigenmatrix of identity");

    mps_frees( A, B, C, iden, (mps_ptr) NULL);

    return 0;
}

