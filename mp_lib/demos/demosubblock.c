/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A;
	mps_blk_t p, q, r;
	char* buf;

    printf("Program start!\n");
    printf("Misc. access stuff for blocks demo. <demosubblock.c>\n\n");

    mps_init( A, 5, 5, 64, MPS_COL_ORDER);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	mps_submat(p, A, 2, 4, 2, 4);

	print_info(A, "A", NULL);
	print_block_info(p, "p", "Submatrix of A");

	mps_subsubmat(q, p, 2, 3, 2, 3);
	print_block_info(q, "q", "Subsubmatrix of A");


	mps_subsubrow(r, q, 2, 2);
	print_block_info(r, "r", "Subsubrow of q");

	mpfr_printf("Sequential access test of subrow r\n");
	buf = mps_print_seq_blk(r, 5);
	puts(buf);
	free(buf);

	mps_subsubrow(r, q, 1, 2);
	print_block_info(r, "r", "Subsubrow of q");

	mpfr_printf("Sequential access test of subrow r\n");
	buf = mps_print_seq_blk(r, 5);
	puts(buf);
	free(buf);

	mps_row_blk(r, A, 2);
	print_block_info(r, "r", "Row 2 of A");
	mpfr_printf("Sequential access test of subrow r\n");
	buf = mps_print_seq_blk(r, 5);
	puts(buf);
	free(buf);

	mps_subsubslice(q, r, 2, 2);
	print_block_info(q, "q", "Slice of Row 2 of A");
	mpfr_printf("Sequential access test of the slice\n");
	buf = mps_print_seq_blk(q, 5);
	puts(buf);

    mps_frees( A, (mps_ptr) NULL);

    return 0;
}

