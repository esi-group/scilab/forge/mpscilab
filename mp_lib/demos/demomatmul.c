/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

#define SIZEDA 16

int main()
{
    mps_t A, B;
	double dA[] = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, \
					9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0 };

    printf("Program start!\n");
    printf("Matrix multiplication demo. <demomatmul.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL);

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);

    /* Perform matrix multiplication B = A.A */
    mps_mat_mul( B, A, A, GMP_RNDN );
	print_info(B, "B", "= Matrix A.A");

	mpfr_printf("Now trying double versions.\n");

	mps_mat_mul_double(B, A, dA, 4, GMP_RNDN);
	print_info(B, "B", "= Matrix A.Double A");

    mps_frees( A, B, (mps_ptr) NULL );

    return 0;
}

