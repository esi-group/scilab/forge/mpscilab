/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 52

int main()
{
    mps_t A;
	mpfr_t tau;
	mps_vec_ptr U, V;

    printf("Program start!\n");
    printf("Bidiag decomp demo. <demobidiag.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);
	U = mps_new_vec(4, PREC);
	V = mps_new_vec(3, PREC);

	mpfr_init2(tau, PREC);

	mps_fill_seq_si_vec(U, 1, 1, GMP_RNDN);
	mps_fill_seq_si_vec(V, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	mpfr_printf("Before bidiag_decomp():\n");
	print_info(A, "A", NULL);
	print_block_info(U, "U", NULL);
	print_block_info(V, "V", NULL);

	mps_bidiag_decomp(A, U, V, GMP_RNDN);

	mpfr_printf("After bidiag_decomp():\n");
	print_info(A, "A", NULL);
	print_block_info(U, "U", NULL);
	print_block_info(V, "V", NULL);

	mps_destroy_vec(U);
	mps_destroy_vec(V);
    mps_frees( A, (mps_ptr) NULL);
	mpfr_clear(tau);

    return 0;
}

