/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A;
	mpfr_t tau;
	mps_vec_ptr vector;

    printf("Program start!\n");
    printf("Householder transformation demo. <demohouseholder.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);
	mpfr_init2(tau, PREC);

	vector = mps_new_vec(4, PREC);

	mps_fill_seq_si_vec(vector, 1, 1, GMP_RNDN);

	print_block_info(vector, "vector", "Original vector");

//	mps_householder_transform(tau, vector, GMP_RNDN);
    mps_householder_reflection(tau, vector, GMP_RNDN);
	print_block_info(vector, "vector", "After householder transformation");
	print_info_sca(tau, "tau", "After householder transformation");

	mps_destroy_vec(vector);
    mps_frees( A, (mps_ptr) NULL);
	mpfr_clear(tau);

    return 0;
}

