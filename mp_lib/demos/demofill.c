/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B, C, D, E, F, G, H, I;

    printf("Program start!\n");
    printf("Named matrix filling demo. <demofill.c>\n\n");

    /* Allocate and initialize three 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, C, D, E, F, G, H, I, (mps_ptr) NULL );

    /* Put some values into the matrices. */
	mps_zero(A, GMP_RNDN);
	mps_ones(B, GMP_RNDN);
	mps_identity(C, GMP_RNDN);
	mps_const_pi(D, GMP_RNDN);
	mps_const_euler(E, GMP_RNDN);
	mps_const_log2(F, GMP_RNDN);
	mps_const_catalan(G, GMP_RNDN);
	mps_random(H, 0, 0, GMP_RNDN);
	mps_random(I, 1, 100, GMP_RNDN);

    /*Print them. */
	print_info(A, "A", "Matrix of 0's");
	print_info(B, "B", "Matrix of 1's");
	print_info(C, "C", "Identity matrix");
	print_info(D, "D", "Matrix of pi's");
	print_info(E, "E", "Matrix of e's");
	print_info(F, "F", "Matrix of log(2)");
	print_info(G, "G", "Matrix of catalan's constant");
	print_info(H, "H", "Matrix of random numbers");
	print_info(I, "I", "Matrix of random numbers from 1 to 100");

    mps_frees( A, B, C, D, E, F, G, H, I, (mps_ptr) NULL);

    return 0;
}

