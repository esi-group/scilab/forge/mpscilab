/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B;
	mpfr_t C;

    printf("Program start!\n");
    printf("Matrix elementwise powers demo. <demopow.c>\n\n");

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_inits( 4, 4, 64, MPS_COL_ORDER, A, B, (mps_ptr) NULL );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(B, 1, 1, GMP_RNDN);
	mpfr_init_set_ui(C, 3, GMP_RNDN);

	print_info(A, "A", NULL);
	print_info(B, "B", NULL);
	print_info_sca(C, "C", NULL);


	mps_pow(B, A, C, GMP_RNDN);
	print_info(B, "B", "= Matrix A.^C");

	mps_pow_ui_mpfr(B, A, (unsigned int) 3, GMP_RNDN);
	print_info(B, "B", "=  A.^3 (unsigned int)");

	mps_pow_si(B, A, (long int) -1, GMP_RNDN);
	print_info(B, "B", "=  A.^-1 (signed int)");

	mps_ui_pow(B, (unsigned int) 2, A, GMP_RNDN);
	print_info(B, "B", "=  (unsigned int) 2 ^ A");


    mps_frees( A, B, (mps_ptr) NULL );
	mpfr_clear( C );

    return 0;
}

