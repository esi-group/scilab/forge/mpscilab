/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A;
	mps_blk_t p, q, r, s;
	char* buf;

    printf("Program start!\n");
    printf("Subsubmatrix demo. <demosubsubmat.c>\n\n");

    mps_init( A, 5, 5, 64, MPS_COL_ORDER);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	mps_submat(p, A, 2, 4, 2, 4);

	print_info(A, "A", NULL);
	print_block_info(p, "p", "Submatrix of A");

	mpfr_printf("Sequential access test of p:\n");
	buf = mps_print_seq_blk(p, 5);
	puts(buf);
	free(buf);

	mps_subsubmat(q, p, 2, 3, 2, 3);
	print_block_info(q, "q", "Subsubmatrix of A");

	mpfr_printf("Sequential access test of q:\n");
	buf = mps_print_seq_blk(q, 5);
	puts(buf);
	free(buf);

	mps_subsubmat(r, q, 2, 2, 2, 2);
	print_block_info(r, "r", "Subsubsubmatrix of A");

	mpfr_printf("Sequential access test of r:\n");
	buf = mps_print_seq_blk(r, 5);
	puts(buf);
	free(buf);

	mps_subsubmat(s, r, 2, 1, 2, 1);
	print_block_info(s, "s", "Subsubsubsubmatrix of A");

	mpfr_printf("Sequential access test of s:\n");
	buf = mps_print_seq_blk(s, 5);
	puts(buf);
	free(buf);

    mps_frees( A, (mps_ptr) NULL);

    return 0;
}

