/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A;
	mps_blk_t subA;
	mpfr_t tau;
	mps_vec_ptr vector;

    printf("Program start!\n");
    printf("Householder hm transformation demo. <demohouseholderhm.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);

	mpfr_init2(tau, PREC);

	vector = mps_new_vec(4, PREC);

	mps_fill_seq_si_vec(vector, 1, 1, GMP_RNDN);
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);

	mps_full_submat(subA, A);
	mpfr_set_ui(tau, 1, GMP_RNDN);

	print_block_info(subA, "subA", NULL);
	print_block_info(vector, "vector", NULL);
	print_info_sca(tau, "tau", NULL);

	mps_householder_hm(tau, vector, subA, GMP_RNDN);

	print_info(A, "A", "The whole matrix");
	print_block_info(subA, "subA", "After mps_householder_hm()");
	print_block_info(vector, "vector", "After mps_householder_hm()");
	print_info_sca(tau, "tau", "After mps_householder_hm()");

	mps_destroy_vec(vector);
    mps_frees( A, (mps_ptr) NULL);
	mpfr_clear(tau);

    return 0;
}

