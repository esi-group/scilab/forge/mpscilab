/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "mps.h"
#include "mps_demo.h"

#define PREC 64

int main()
{
    mps_t A;
	mps_vec_ptr tau;

    printf("Program start!\n");
    printf("QR decomposition demo. <demodecomp.c>\n\n");

    mps_inits( 4, 4, PREC, MPS_COL_ORDER, A, (mpfr_ptr) NULL);
	tau = mps_new_vec(4, PREC);

	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
	mps_fill_seq_si_vec(tau, 1, 1, GMP_RNDN);

	mpfr_printf("Before qr_decomp():\n");
	print_info(A, "A", NULL);
	print_block_info(tau, "tau", NULL);

	mps_qr_decomp(A, tau, GMP_RNDN);

	mpfr_printf("After bidiag_decomp():\n");
	print_info(A, "A", NULL);
	print_block_info(tau, "tau", NULL);

	mps_destroy_vec(tau);
    mps_frees( A, (mps_ptr) NULL);

    return 0;
}

