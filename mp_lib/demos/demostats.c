/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>

#include "mps.h"
#include "mps_demo.h"

int main()
{
    mps_t A, B, X, Y, Z, Q, R, D;
	mpfr_t C;

    printf("Program start!\n");
    printf("Statistics demo. <demostats.c>\n\n");

	mpfr_init2(C, 64);

    /* Allocate and initialize two 4X4 matrix using 64 bits of precision. */
    mps_init( A, 4, 4, 64, MPS_COL_ORDER );
    mps_init( B, 4, 3, 64, MPS_COL_ORDER );
    mps_init( Z, 1, 4, 64, MPS_COL_ORDER );
    mps_init( D, 4, 4, 64, MPS_COL_ORDER );

    mps_init( X, 5, 3, 64, MPS_COL_ORDER );
    mps_init( Y, 3, 3, 64, MPS_COL_ORDER );

    mps_init( Q, 1, 4, 64, MPS_COL_ORDER );
    mps_init( R, 1, 1, 64, MPS_COL_ORDER );

    /* Put some values into the matrices. */
	mps_fill_seq_row_si(A, 1, 1, GMP_RNDN);
    mps_str_input( B, "1 2 3 4; 9 16 25 36; 3 6 9 27;", 64 );

    mps_str_input( X, "4.0 2.0 .60; 4.2 2.1 .59; 3.9 2.0 .58;\
						4.3 2.1 .62; 4.1 2.2 .63;", 64 );

	mps_fill_seq_si(Q, 1, 1, GMP_RNDN);

	print_info(A, "A", NULL);
	print_info(B, "B", NULL);
	print_info(X, "X", NULL);
	print_info(Q, "Q", NULL);


	/* matrix A stats */
	mps_mean(C, A, GMP_RNDN);
	print_info_sca(C, "C", "Mean of all elements of A");

	mps_variance(C, A, GMP_RNDN);
	print_info_sca(C, "C", "Variance of all elements of A");

	mps_mean_mat(Z, A, GMP_RNDN);
	print_info(Z, "Z", "= mean matrix A");

	mps_covariance_mat(D, A, GMP_RNDN);
	print_info(D, "D", "= Covariance matrix of A");

	mps_correlation_mat(D, A, GMP_RNDN);
	print_info(D, "D", "= Correlation matrix of A");


	/* matrix B stats */
	mps_mean(C, B, GMP_RNDN);
	print_info_sca(C, "C", "Mean of all elements of B");

	mps_variance(C, B, GMP_RNDN);
	print_info_sca(C, "C", "Variance of all elements of B");

	mps_stddev(C, B, GMP_RNDN);
	print_info_sca(C, "C", "Std dev of all elements of B");


	/* matrix X stats */
	mps_covariance_mat(Y, X, GMP_RNDN);
	print_info(Y, "Y", "= Covariance matrix of X");

	mps_correlation_mat(Y, X, GMP_RNDN);
	print_info(Y, "Y", "= Correlation matrix of X");


	/* matrix Q stats */
	mps_covariance_mat(R, Q, GMP_RNDN);
	print_info(R, "R", "= Covariance matrix of Q");

	mps_correlation_mat(R, Q, GMP_RNDN);
	print_info(R, "R", "= Correlation matrix of Q");

	mps_frees(A, B, Z, X, Y, Q, R, D, (mps_ptr) NULL);
	mpfr_clear( C );

    return 0;
}

