/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test matrix subtraction mps_sub_double(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_sub_double1()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_sub_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_sub_double1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_sub_double2()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    unsigned int R[10*10] = {
    99,      189,     279,     369,     459,     549,     639,     729,     819,     909,
    1098,    1188,    1278,    1368,    1458,    1548,    1638,    1728,    1818,    1908,
    2097,    2187,    2277,    2367,    2457,    2547,    2637,    2727,    2817,    2907,
    3096,    3186,    3276,    3366,    3456,    3546,    3636,    3726,    3816,    3906,
    4095,    4185,    4275,    4365,    4455,    4545,    4635,    4725,    4815,    4905,
    5094,    5184,    5274,    5364,    5454,    5544,    5634,    5724,    5814,    5904,
    6093,    6183,    6273,    6363,    6453,    6543,    6633,    6723,    6813,    6903,
    7092,    7182,    7272,    7362,    7452,    7542,    7632,    7722,    7812,    7902,
    8091,    8181,    8271,    8361,    8451,    8541,    8631,    8721,    8811,    8901,
    9090,    9180,    9270,    9360,    9450,    9540,    9630,    9720,    9810,    9900,
    };

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 100*i, GMP_RNDN );
    }

    mps_sub_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_sub_double2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_sub_double3()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_sub_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_sub_double3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_sub_double4()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i=1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_mat_transpose( B );

    mps_sub_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_sub_double4() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_double_sub1()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = 2*i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_double_sub( B, A, B, MPS_COL_ORDER, GMP_RNDN );

    for ( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_double_sub1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_double_sub2()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    unsigned int R[10*10] = {
    99,     1098,    2097,    3096,    4095,    5094,    6093,    7092,    8091,    9090,
    189,    1188,    2187,    3186,    4185,    5184,    6183,    7182,    8181,    9180,
    279,    1278,    2277,    3276,    4275,    5274,    6273,    7272,    8271,    9270,
    369,    1368,    2367,    3366,    4365,    5364,    6363,    7362,    8361,    9360,
    459,    1458,    2457,    3456,    4455,    5454,    6453,    7452,    8451,    9450,
    549,    1548,    2547,    3546,    4545,    5544,    6543,    7542,    8541,    9540,
    639,    1638,    2637,    3636,    4635,    5634,    6633,    7632,    8631,    9630,
    729,    1728,    2727,    3726,    4725,    5724,    6723,    7722,    8721,    9720,
    819,    1818,    2817,    3816,    4815,    5814,    6813,    7812,    8811,    9810,
    909,    1908,    2907,    3906,    4905,    5904,    6903,    7902,    8901,    9900,
    };

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = 100*i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_double_sub( B, A, B, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_sub2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_double_sub3()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = 2*i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_double_sub( B, A, B, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_double_sub3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_double_sub4()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = 2*i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_transpose( B );

    mps_double_sub( B, A, B, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("test_double_sub4() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int main()
{
    test_sub_double1();
    test_sub_double2();
    test_sub_double3();
    test_sub_double4();

    test_double_sub1();
    test_double_sub2();
    test_double_sub3();
    test_double_sub4();

    return 0;
}

