/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test block operation mps_get_ele_blk(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_get_ele_blk1()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 1, 11, 21, 31, 41 },
                              { 2, 12, 22, 32, 42 },
                              { 3, 13, 23, 33, 43 },
                              { 4, 14, 24, 34, 44 },
                              { 5, 15, 25, 35, 45 } };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 1, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for ( i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk1() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk2()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 12, 22, 32, 42, 52 },
                              { 13, 23, 33, 43, 53 },
                              { 14, 24, 34, 44, 54 },
                              { 15, 25, 35, 45, 55 },
                              { 16, 26, 36, 46, 56 } };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 2, 5, 2, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for (i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk2() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk3()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 56, 66, 76, 86, 96 },
                              { 57, 67, 77, 87, 97 },
                              { 58, 68, 78, 88, 98 },
                              { 59, 69, 79, 89, 99 },
                              { 60, 70, 80, 90, 100} };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 6, 5, 6, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for ( i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk3() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk4()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 5, 15, 25, 35, 45 },
                              { 6, 16, 26, 36, 46 },
                              { 7, 17, 27, 37, 47 },
                              { 8, 18, 28, 38, 48 },
                              { 9, 19, 29, 39, 49 } };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 5, 1, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for ( i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk4() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk5()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 1, 11, 21, 31, 41 },
                              { 2, 12, 22, 32, 42 },
                              { 3, 13, 23, 33, 43 },
                              { 4, 14, 24, 34, 44 },
                              { 5, 15, 25, 35, 45 } };

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 1, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for ( i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[j-1][i-1] ) != 0 )
            {
                printf("test_get_ele_blk5() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk6()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[5][5] ={  { 5, 15, 25, 35, 45 },
                              { 6, 16, 26, 36, 46 },
                              { 7, 17, 27, 37, 47 },
                              { 8, 18, 28, 38, 48 },
                              { 9, 19, 29, 39, 49 } };

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 5, 5);

    for ( j = 1; j <= 5; j++ )
    {
        for ( i = 1; i <= 5; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[j-1][i-1] ) != 0 )
            {
                printf("test_get_ele_blk6() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk7()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 1, 1, 1);

    xi = mps_get_ele_blk( p, 1, 1 );
    if ( mpfr_cmp_ui( xi, 1 ) != 0 )
    {
        printf("test_get_ele_blk7() failed. Value mismatch at index 1, 1\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk8()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 1, 5, 1);

    xi = mps_get_ele_blk( p, 1, 1 );
    if ( mpfr_cmp_ui( xi, 45 ) != 0 )
    {
        printf("test_get_ele_blk8() failed. Value mismatch at index 1, 1\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk9()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 1, 5, 1);

    xi = mps_get_ele_blk( p, 1, 1 );
    if( mpfr_cmp_ui( xi, 45 ) != 0 )
    {
        printf("test_get_ele_blk9() failed. Value mismatch at index 1, 1\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk10()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 10, 1, 1, 1);

    xi = mps_get_ele_blk( p, 1, 1 );
    if( mpfr_cmp_ui( xi, 91 ) != 0 )
    {
        printf("test_get_ele_blk10() failed. Value mismatch at index 1, 1\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk11()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 7, 2, 3, 1);

    xi = mps_get_ele_blk( p, 1, 1 );
    if( mpfr_cmp_ui( xi, 63 ) != 0 )
    {
        printf("test_get_ele_blk11() failed. Value mismatch at index 1, 1\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk12()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[3][4] ={  { 1,  8, 15, 22 },
                              { 2,  9, 16, 23 },
                              { 3, 10, 17, 24 } };

    mps_init( A, 7, 9, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 4);

    for( j=1; j<=4; j++ )
    {
        for(i=1; i<=3; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk12() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk13()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[3][4] ={  {  9, 16, 23, 30 },
                              { 10, 17, 24, 31 },
                              { 11, 18, 25, 32 } };

    mps_init( A, 7, 9, 100, MPS_COL_ORDER );

    for( i=1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 2, 3, 2, 4);

    for( j=1; j<=4; j++ )
    {
        for(i=1; i<=3; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk13() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_blk14()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    unsigned int R1[3][4] ={  {  1,  2,  3,  4 },
                              { 10, 11, 12, 13 },
                              { 19, 20, 21, 22 } };

    mps_init( A, 7, 9, 100, MPS_ROW_ORDER );

    for( i = 1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 4);

    for ( j = 1; j <= 4; j++ )
    {
        for ( i = 1; i <= 3; i++ )
        {
            xi = mps_get_ele_blk( p, i, j );
            if ( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_get_ele_blk14() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_get_ele_blk1();
    test_get_ele_blk2();
    test_get_ele_blk3();
    test_get_ele_blk4();

    /* Transposed tests. */
    test_get_ele_blk5();
    test_get_ele_blk6();

    /* Single elements access tests. */
    test_get_ele_blk7();
    test_get_ele_blk8();
    test_get_ele_blk9();
    test_get_ele_blk10();
    test_get_ele_blk11();

    /* Non square stuff. */
    test_get_ele_blk12();
    test_get_ele_blk13();

    /* Transposed non square stuff. */
    test_get_ele_blk14();

    return 0;
}

