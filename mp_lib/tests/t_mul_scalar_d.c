/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test scalar addition function mps_add_scalar_double(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mul_scalar_d1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mul_scalar_double( B, A, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_d1() failed. Value mismatch at index %u for matrix size n=%u, m=%u , prec=%u, order=%uu\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mul_scalar_d2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mul_scalar_double( A, A, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_d2() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_mul_scalar_d3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( A );

    mps_mul_scalar_double( A, A, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_d3() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int main()
{

    test_mul_scalar_d1( 10, 10, 100, MPS_COL_ORDER );
    test_mul_scalar_d1( 11, 13, 100, MPS_COL_ORDER );
    test_mul_scalar_d1( 10, 20, 50, MPS_COL_ORDER );
    test_mul_scalar_d1( 20, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d1( 1, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d1( 10, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d1( 1, 2, 64, MPS_COL_ORDER );
    test_mul_scalar_d1( 2, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d1( 1, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d1( 10, 10, 100, MPS_ROW_ORDER );
    test_mul_scalar_d1( 10, 20, 50, MPS_ROW_ORDER );
    test_mul_scalar_d1( 20, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d1( 1, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d1( 10, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d1( 1, 2, 64, MPS_ROW_ORDER );
    test_mul_scalar_d1( 2, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d1( 1, 1, 64, MPS_ROW_ORDER );

    test_mul_scalar_d2( 10, 10, 100, MPS_COL_ORDER );
    test_mul_scalar_d2( 11, 13, 100, MPS_COL_ORDER );
    test_mul_scalar_d2( 10, 20, 50, MPS_COL_ORDER );
    test_mul_scalar_d2( 20, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d2( 1, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d2( 10, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d2( 1, 2, 64, MPS_COL_ORDER );
    test_mul_scalar_d2( 2, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d2( 1, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d2( 10, 10, 100, MPS_ROW_ORDER );
    test_mul_scalar_d2( 10, 20, 50, MPS_ROW_ORDER );
    test_mul_scalar_d2( 20, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d2( 1, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d2( 10, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d2( 1, 2, 64, MPS_ROW_ORDER );
    test_mul_scalar_d2( 2, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d2( 1, 1, 64, MPS_ROW_ORDER );

    test_mul_scalar_d3( 10, 10, 100, MPS_COL_ORDER );
    test_mul_scalar_d3( 11, 13, 100, MPS_COL_ORDER );
    test_mul_scalar_d3( 10, 20, 50, MPS_COL_ORDER );
    test_mul_scalar_d3( 20, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d3( 1, 10, 53, MPS_COL_ORDER );
    test_mul_scalar_d3( 10, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d3( 1, 2, 64, MPS_COL_ORDER );
    test_mul_scalar_d3( 2, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d3( 1, 1, 64, MPS_COL_ORDER );
    test_mul_scalar_d3( 10, 10, 100, MPS_ROW_ORDER );
    test_mul_scalar_d3( 10, 20, 50, MPS_ROW_ORDER );
    test_mul_scalar_d3( 20, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d3( 1, 10, 53, MPS_ROW_ORDER );
    test_mul_scalar_d3( 10, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d3( 1, 2, 64, MPS_ROW_ORDER );
    test_mul_scalar_d3( 2, 1, 64, MPS_ROW_ORDER );
    test_mul_scalar_d3( 1, 1, 64, MPS_ROW_ORDER );

    return 0;
}
