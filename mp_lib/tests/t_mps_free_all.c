/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test mps_free_all(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_free_all1()
{
    mps_t A, B, C, D;

    mps_inits( 4, 4, 24, MPS_COL_ORDER, A, B, C, D, (mps_ptr) NULL );

    mps_free_all();

    if ( mps_exist( A ) != 0 )
    {
        printf("test_mps_free_all1() failed, mps_exist( A ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( B ) != 0 )
    {
        printf("test_mps_free_all1() failed, mps_exist( B ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( C ) != 0 )
    {
        printf("test_mps_free_all1() failed, mps_exist( C ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( D ) != 0 )
    {
        printf("test_mps_free_all1() failed, mps_exist( D ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    return 0;
}

int main()
{
    test_mps_free_all1();

    return 0;
}

