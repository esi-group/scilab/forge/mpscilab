/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the parallel scalar multiplication function mps_add_scalar(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

unsigned int n_threads;

int test_mul_scalar_mpfr1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    xmps_mul_scalar( B, A, mpfr_val, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_mpfr1() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mul_scalar_mpfr2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    xmps_mul_scalar( A, A, mpfr_val, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_mpfr2() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mul_scalar_mpfr3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 4, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( A );

    xmps_mul_scalar( A, A, mpfr_val, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i*4 ) != 0 )
        {
            mpfr_printf("test_mul_scalar_mpfr3() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int main()
{
    unsigned int i;

    for ( i = 1; i <= 4; i++ )
    {
        n_threads = i;

        test_mul_scalar_mpfr1( 30, 30, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 10, 10, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 11, 13, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 10, 20, 50, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 20, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 1, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 10, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 1, 2, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 2, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 1, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr1( 10, 10, 100, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 10, 20, 50, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 20, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 1, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 10, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 1, 2, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 2, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr1( 1, 1, 64, MPS_ROW_ORDER );

        test_mul_scalar_mpfr2( 30, 30, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 10, 10, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 11, 13, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 10, 20, 50, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 20, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 1, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 10, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 1, 2, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 2, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 1, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr2( 10, 10, 100, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 10, 20, 50, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 20, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 1, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 10, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 1, 2, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 2, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr2( 1, 1, 64, MPS_ROW_ORDER );

        test_mul_scalar_mpfr3( 30, 30, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 10, 10, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 11, 13, 100, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 10, 20, 50, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 20, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 1, 10, 53, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 10, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 1, 2, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 2, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 1, 1, 64, MPS_COL_ORDER );
        test_mul_scalar_mpfr3( 10, 10, 100, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 10, 20, 50, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 20, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 1, 10, 53, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 10, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 1, 2, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 2, 1, 64, MPS_ROW_ORDER );
        test_mul_scalar_mpfr3( 1, 1, 64, MPS_ROW_ORDER );
    }

    return 0;
}

