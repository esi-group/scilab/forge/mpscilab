/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_double_mat_mul() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_double_mat_mul1()
{
    double A[6] = {1, 2, 3, 4, 5, 6};
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;
    
    return 0;
}

int test_double_mat_mul2()
{
    mps_t A;
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    double D[10*10];

    double R[10*10] = {
    3355,  3410,  3465,  3520,  3575,  3630,  3685,  3740,  3795,  3850,
    7955,  8110,  8265,  8420,  8575,  8730,  8885,  9040,  9195,  9350,
    12555, 12810, 13065, 13320, 13575, 13830, 14085, 14340, 14595, 14850,
    17155, 17510, 17865, 18220, 18575, 18930, 19285, 19640, 19995, 20350,
    21755, 22210, 22665, 23120, 23575, 24030, 24485, 24940, 25395, 25850,
    26355, 26910, 27465, 28020, 28575, 29130, 29685, 30240, 30795, 31350,
    30955, 31610, 32265, 32920, 33575, 34230, 34885, 35540, 36195, 36850,
    35555, 36310, 37065, 37820, 38575, 39330, 40085, 40840, 41595, 42350,
    40155, 41010, 41865, 42720, 43575, 44430, 45285, 46140, 46995, 47850,
    44755, 45710, 46665, 47620, 48575, 49530, 50485, 51440, 52395, 53350 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_col( A, i );
        D[( ((i-1) / 10) + 10 * ((i-1) % 10) )] = i;
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_double_mat_mul( B, D, 10, MPS_ROW_ORDER, A, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_mat_mul2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}


int main()
{
    test_double_mat_mul1();
    test_double_mat_mul2();

    return 0;
}
