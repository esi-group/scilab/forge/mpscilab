/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test scalar division function mps_div_scalar(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mpfr_div1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 2, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_div_scalar( B, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mpfr_div1() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_div2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 2, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_div_scalar( A, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mpfr_div2() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_div3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 2, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_flip_order( A );

    mps_div_scalar( A, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mpfr_div3() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u, order=%u\n", i, n, m, prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int main()
{

    test_mpfr_div1( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_div1( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_div1( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_div1( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_div1( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_div1( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_div1( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_div1( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_div1( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_div1( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_div1( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_div1( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div1( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div1( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div1( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_div1( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div1( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_div2( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_div2( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_div2( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_div2( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_div2( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_div2( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_div2( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_div2( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_div2( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_div2( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_div2( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_div2( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div2( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div2( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div2( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_div2( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div2( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_div3( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_div3( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_div3( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_div3( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_div3( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_div3( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_div3( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_div3( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_div3( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_div3( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_div3( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_div3( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div3( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_div3( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div3( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_div3( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_div3( 1, 1, 64, MPS_ROW_ORDER );

    return 0;
}
