/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test mps_scale_blk(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_scale_blk1()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    mpfr_t s;
    unsigned int R1[10][10] ={
      { 2,  22, 21, 31, 41, 51,  61, 71, 81,  91 },
      { 4,  24, 22, 32, 42, 52,  62, 72, 82,  92 },
      { 6,  26, 23, 33, 43, 53,  63, 73, 83,  93 },
      { 4,  14, 24, 34, 44, 54,  64, 74, 84,  94 },
      { 5,  15, 25, 35, 45, 110, 65, 75, 85,  95 },
      { 6,  16, 26, 36, 46, 56,  66, 76, 86,  96 },
      { 7,  17, 27, 37, 47, 57,  67, 77, 87,  97 },
      { 8,  18, 28, 38, 48, 58,  68, 78, 88,  98 },
      { 9,  19, 29, 39, 49, 59,  69, 79, 89,  99 },
      { 10, 20, 30, 40, 50, 60,  70, 80, 180, 200} };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mpfr_init2( s, 100 );
    mpfr_set_ui( s, 2, GMP_RNDN );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 2);
    mps_scale_blk( p, s, GMP_RNDN );

    mps_submat(p, A, 5, 1, 6, 1);
    mps_scale_blk( p, s, GMP_RNDN );

    mps_submat(p, A, 10, 1, 9, 2);
    mps_scale_blk( p, s, GMP_RNDN );

    for( j=1; j<=10; j++ )
    {
        for(i=1; i<=10; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_scale_blk1() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_scale_blk2()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    mpfr_t s;
    unsigned int R1[10][10] ={
      { 2,  22, 42, 31, 41, 51, 61, 71, 81, 91  },
      { 4,  24, 44, 32, 42, 52, 62, 72, 82, 92  },
      { 3,  13, 23, 33, 43, 53, 63, 73, 83, 93  },
      { 4,  14, 24, 34, 44, 54, 64, 74, 84, 94  },
      { 5,  15, 25, 35, 45, 55, 65, 75, 85, 95  },
      { 6,  16, 26, 36, 92, 56, 66, 76, 86, 96  },
      { 7,  17, 27, 37, 47, 57, 67, 77, 87, 97  },
      { 8,  18, 28, 38, 48, 58, 68, 78, 88, 98  },
      { 9,  19, 29, 39, 49, 59, 69, 79, 89, 198 },
      { 10, 20, 30, 40, 50, 60, 70, 80, 90, 200 } };

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );
    mpfr_init2( s, 100 );
    mpfr_set_ui( s, 2, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 2);
    mps_scale_blk( p, s, GMP_RNDN );

    mps_submat(p, A, 5, 1, 6, 1);
    mps_scale_blk( p, s, GMP_RNDN );

    mps_submat(p, A, 10, 1, 9, 2);
    mps_scale_blk( p, s, GMP_RNDN );

    for ( j = 1; j <= 10; j++ )
    {
        for ( i = 1; i <= 10; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if ( mpfr_cmp_ui( xi, R1[j-1][i-1] ) != 0 )
            {
                printf("test_scale_blk2() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int test_scale_blk3()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i, j;
    mpfr_ptr xi;
    mpfr_t s;
    unsigned int R1[10][9] ={
      { 2,  22, 21, 31, 41, 51,  61, 71, 81  },
      { 4,  24, 22, 32, 42, 52,  62, 72, 82  },
      { 6,  26, 23, 33, 43, 53,  63, 73, 83  },
      { 4,  14, 24, 34, 44, 54,  64, 74, 84  },
      { 5,  15, 25, 35, 45, 110, 65, 75, 85  },
      { 6,  16, 26, 36, 46, 56,  66, 76, 86  },
      { 7,  17, 27, 37, 47, 57,  67, 77, 87  },
      { 8,  18, 28, 38, 48, 58,  68, 78, 88  },
      { 9,  19, 29, 39, 49, 59,  69, 79, 89  },
      { 10, 20, 30, 40, 50, 60,  70, 80, 90  } };

    mps_init( A, 10, 9, 100, MPS_COL_ORDER );
    mpfr_init2( s, 100 );
    mpfr_set_ui( s, 2, GMP_RNDN );

    for( i=1; i <= 10*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 2);
    mps_scale_blk( p, s, GMP_RNDN );

    mps_submat(p, A, 5, 1, 6, 1);
    mps_scale_blk( p, s, GMP_RNDN );

    for( j=1; j<=9; j++ )
    {
        for(i=1; i<=10; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if( mpfr_cmp_ui( xi, R1[i-1][j-1] ) != 0 )
            {
                printf("test_scale_blk3() failed. Value mismatch at index %d, %d\n", i, j);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_scale_blk1();
    test_scale_blk2();
    test_scale_blk3();

    return 0;
}
