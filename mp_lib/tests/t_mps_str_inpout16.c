/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test  mps_get_str16. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_str1()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str1() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str2()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, -i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str2() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str3()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 1, 100, MPS_COL_ORDER );
    mps_init( B, 10, 1, 100, MPS_COL_ORDER );

    for( i=1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i=1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str3() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str4()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 1, 10, 100, MPS_COL_ORDER );
    mps_init( B, 1, 10, 100, MPS_COL_ORDER );

    for ( i=1; i <= 1*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 1*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str4() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str5()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 1, 1, 100, MPS_COL_ORDER );
    mps_init( B, 1, 1, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 1*1; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 1*1; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str5() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str6()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        mpfr_div_ui( xi, xi, 1233, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str6() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str7()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 10, 2, MPS_COL_ORDER );
    mps_init( B, 10, 10, 2, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 2, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str7() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str8()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        mpfr_mul_ui( xi, xi, 1233145, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str8() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str9()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi;
    mpfr_ptr ri;

    mps_init( A, 11, 7, 100, MPS_COL_ORDER );
    mps_init( B, 11, 7, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 11*7; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        mpfr_mul_ui( xi, xi, 1233145, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 11*7; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str9() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_str10()
{
    mps_t A, B;
    unsigned int i;
    char * str;

    mpfr_ptr xi, ri;

    mps_init( A, 7, 13, 100, MPS_COL_ORDER );
    mps_init( B, 7, 13, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*13; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        mpfr_mul_ui( xi, xi, 44654, GMP_RNDN );
    }

    str = mps_get_str16( A );

    mps_set_str16( B, str, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*13; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( A, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_str10() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    free( str );
    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_mps_str1();
    test_mps_str2();
    test_mps_str3();
    test_mps_str4();
    test_mps_str5();
    test_mps_str6();
    test_mps_str7();
    test_mps_str8();
    test_mps_str9();
    test_mps_str10();

    return 0;
}

