/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test matrix addition mps_add_double(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_add_double1()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_add_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, 2*i ) != 0 )
        {
            printf("test_add_double1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_add_double2()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {
    2,      13,     24,     35,     46,     57,     68,     79,     90,     101,
    13,     24,     35,     46,     57,     68,     79,     90,     101,    112,
    24,     35,     46,     57,     68,     79,     90,     101,    112,    123,
    35,     46,     57,     68,     79,     90,     101,    112,    123,    134,
    46,     57,     68,     79,     90,     101,    112,    123,    134,    145,
    57,     68,     79,     90,     101,    112,    123,    134,    145,    156,
    68,     79,     90,     101,    112,    123,    134,    145,    156,    167,
    79,     90,     101,    112,    123,    134,    145,    156,    167,    178,
    90,     101,    112,    123,    134,    145,    156,    167,    178,    189,
    101,    112,    123,    134,    145,    156,    167,    178,    189,    200,
    };

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_add_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_add_double2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_add_double3()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for( i=1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_add_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for( i=1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if( mpfr_cmp_ui( xi, 2*i ) != 0 )
        {
            printf("test_add_double3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_add_double4()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_transpose( B );

    mps_add_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, 2*i ) != 0 )
        {
            printf("test_add_double4() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int main()
{
    test_add_double1();
    test_add_double2();
    test_add_double3();
    test_add_double4();

    return 0;
}

