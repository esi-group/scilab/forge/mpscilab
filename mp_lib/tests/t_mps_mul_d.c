/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test element wise multiplication mps_mul_double(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_mul_d1()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mul_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*i ) != 0 )
        {
            printf("test_mps_mul_d1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_mps_mul_d2()
{
    double A[10*10];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {
    1,      22,      63,      124,     205,     306,     427,     568,     729,     910,
    22,     144,     286,     448,     630,     832,     1054,    1296,    1558,    1840,
    63,     286,     529,     792,     1075,    1378,    1701,    2044,    2407,    2790,
    124,    448,     792,     1156,    1540,    1944,    2368,    2812,    3276,    3760,
    205,    630,     1075,    1540,    2025,    2530,    3055,    3600,    4165,    4750,
    306,    832,     1378,    1944,    2530,    3136,    3762,    4408,    5074,    5760,
    427,    1054,    1701,    2368,    3055,    3762,    4489,    5236,    6003,    6790,
    568,    1296,    2044,    2812,    3600,    4408,    5236,    6084,    6952,    7840,
    729,    1558,    2407,    3276,    4165,    5074,    6003,    6952,    7921,    8910,
    910,    1840,    2790,    3760,    4750,    5760,    6790,    7840,    8910,    10000,
    };

    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mul_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_mps_mul_d2() failed, Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_mps_mul_d3()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mul_double( B, B, A, MPS_COL_ORDER, GMP_RNDN );

    for ( i=1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*i ) != 0 )
        {
            printf("test_mps_mul_d3() failed, Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_mps_mul_d4()
{
    double A[7*17];
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( B, 7, 17, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*17; i++ )
    {
        A[i-1] = i;
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_transpose( B );

    mps_mul_double( B, B, A, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*17; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*i ) != 0 )
        {
            printf("test_mps_mul_d4() failed, Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int main()
{
    test_mps_mul_d1();
    test_mps_mul_d2();
    test_mps_mul_d3();
    test_mps_mul_d4();

    return 0;
}

