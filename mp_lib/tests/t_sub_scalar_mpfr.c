/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test scalar addition function mps_sub_scalar(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mpfr_sub1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_sub_scalar( B, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i - 1 ) != 0 )
        {
            mpfr_printf("test_mpfr_sub1() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_sub2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_sub_scalar( A, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i - 1 ) != 0 )
        {
            mpfr_printf("test_mpfr_sub2() failed. Value mismatch at index %u for matrix size n=%u , m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_sub3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( A );

    mps_sub_scalar( A, A, mpfr_val, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i - 1 ) != 0 )
        {
            mpfr_printf("test_mpfr_sub3() failed. Value mismatch at index %d for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_sub4( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1000, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_scalar_sub( B, mpfr_val, A, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, 1000 - i ) != 0 )
        {
            mpfr_printf("test_mpfr_sub4() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_sub5( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1000, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_scalar_sub( A, mpfr_val, A, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, 1000 - i ) != 0 )
        {
            mpfr_printf("test_mpfr_sub5() failed. Value mismatch at index %u for matrix size n=%u , m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int test_mpfr_sub6( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    mpfr_t mpfr_val;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    mpfr_init2( mpfr_val , prec );
    mpfr_set_ui( mpfr_val, 1000, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( A );

    mps_scalar_sub( A, mpfr_val, A, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, 1000 - i ) != 0 )
        {
            mpfr_printf("test_mpfr_sub6() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mpfr_clear( mpfr_val );

    return 0;
}

int main()
{

    test_mpfr_sub1( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub1( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub1( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub1( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub1( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub1( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub1( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub1( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub1( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub1( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub1( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub1( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub1( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub1( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub1( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub1( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub1( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_sub2( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub2( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub2( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub2( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub2( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub2( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub2( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub2( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub2( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub2( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub2( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub2( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub2( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub2( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub2( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub2( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub2( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_sub3( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub3( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub3( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub3( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub3( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub3( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub3( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub3( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub3( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub3( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub3( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub3( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub3( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub3( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub3( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub3( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub3( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_sub4( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub4( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub4( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub4( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub4( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub4( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub4( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub4( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub4( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub4( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub4( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub4( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub4( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub4( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub4( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub4( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub4( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_sub5( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub5( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub5( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub5( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub5( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub5( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub5( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub5( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub5( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub5( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub5( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub5( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub5( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub5( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub5( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub5( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub5( 1, 1, 64, MPS_ROW_ORDER );

    test_mpfr_sub6( 10, 10, 100, MPS_COL_ORDER );
    test_mpfr_sub6( 11, 13, 100, MPS_COL_ORDER );
    test_mpfr_sub6( 10, 20, 50, MPS_COL_ORDER );
    test_mpfr_sub6( 20, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub6( 1, 10, 53, MPS_COL_ORDER );
    test_mpfr_sub6( 10, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub6( 1, 2, 64, MPS_COL_ORDER );
    test_mpfr_sub6( 2, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub6( 1, 1, 64, MPS_COL_ORDER );
    test_mpfr_sub6( 10, 10, 100, MPS_ROW_ORDER );
    test_mpfr_sub6( 10, 20, 50, MPS_ROW_ORDER );
    test_mpfr_sub6( 20, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub6( 1, 10, 53, MPS_ROW_ORDER );
    test_mpfr_sub6( 10, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub6( 1, 2, 64, MPS_ROW_ORDER );
    test_mpfr_sub6( 2, 1, 64, MPS_ROW_ORDER );
    test_mpfr_sub6( 1, 1, 64, MPS_ROW_ORDER );

    return 0;
}

