/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_copy() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_set_copy1( unsigned int n, unsigned int m, mpfr_prec_t prec1, mpfr_prec_t prec2 )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec1, MPS_COL_ORDER );
    mps_init( B, n, m, prec2, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_set_copy( B, A, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_set_copy1() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec1=%u, prec2=%u\n", i, n, m, prec1, prec2);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_mps_set_copy1( 10, 10, 100, 100 );
    test_mps_set_copy1( 20, 20, 10, 20 );
    test_mps_set_copy1( 10, 1, 100, 83 );
    test_mps_set_copy1( 1, 10, 53, 103 );
    test_mps_set_copy1( 1, 1, 87, 45 );

    return 0;
}

