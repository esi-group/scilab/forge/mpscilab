/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the matrix inverse function mps_inv(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

#ifdef HAVE_OVERLENGTH_STRINGS
char R1[] = "-.2fed277f3077898430beaf471c@-2 -.da5d9c0e364b572797c666fcb0@-2 -.119c59409e4b95080d68f27784@-1 .99982d3b42c1a0456c25839220@-2 -.3890d62095aae1258ed7b9b61c@-2 .5247ebc3a122c9ace8e3281018@-2 .24cf3b1a49b68fb00daa1d9190@-1 .b66a01217568d0b3628812c1b0@-2 -.655406c032785fae6ca44b8640@-2 -.1911c924ed1d98d39c87c201f8@-1 ; .555b8458e2d40ca41bd1392390@-2 -.8552da780996be64d9c79fd700@-3 .4b2b664b777dbabfdaa677a1c8@-2 .4e5bf052ffed00a833e4e09168@-4 -.13612a433afa4fed2cd612771e@-2 -.1cbcf5914d08b4349c09adb1be@-2 -.54231a8d25d7389ca851df5cc8@-2 -.210129ba0f6234bfa1b525e0ac@-2 -.108430bd3a1dac2060e0045388@-2 .3e1ad882271e3943dfb410ea60@-2 ; .6e8450457682e5e5d20f49fa08@-3 .205a3756d14a57a8e4e5c742a0@-2 .a9457e00cf9570c768604aec40@-3 .268d800a2c3024b2bf375d3b74@-2 -.29a8109b3493a016baf680bfc4@-2 .f39eb2eb6a48f51ef1eb8dd9e0@-3 -.4fbf0adc49eebaafcdeaef8db0@-2 -.5ef26e54df419a11d17a9fb590@-2 .343c03161d070e2f20c739492c@-2 .4727ee364a910a07f5fd587f50@-2 ; -.19e585bb96b3465c3252a3d9de@-2 .caedac869c4c28eb8ac2d3e150@-2 .11bc0694657c45b9a156d6a12a@-1 -.4ebba92ce38b19944e4e4dcfc0@-2 .fd0ddf6778443eda09b5f8c220@-3 -.2bb3eac13d7f5631937030aa44@-2 -.2706ab40d5be6d461308cd6044@-1 -.c5b5aebee558280265f5891400@-2 .56387391fe3a7905deeef01a68@-2 .1dbb983ec4c92d234648b7a830@-1 ; -.46b8fa02ee0105730a12130998@-2 .1304bc02f779d51edcb1ed37fe@-1 .212fabfb0f0c44476794153060@-1 -.98eb996b0821761fd07a517a60@-2 -.f32973ba52db154c5ef1689cd0@-3 -.56ae0f8b9006aad02795a45d28@-2 -.3d0f79f51bbf84c08217ed3838@-1 -.18cf9ff2ff304a12e29cf9047e@-1 .ff3eb6319f33486f6a9372d260@-2 .2be49e95cb1bf8f4ae36193810@-1 ; .536f01fe46fb1a112f6ad4fa78@-2 .18e76565cf178d264f8768df18@-1 .28b717d45715f3cd1f2312a490@-1 -.ed809d88c64a8df6a69c4b8970@-2 -.1c3397a0b7ada69aac33b29966@-2 -.b4193d9403f140df2f2e7829c0@-2 -.520c9bfcb483cc9263fd4fb8f0@-1 -.1e39497b5d96987348eb07705c@-1 .13663a1f7b4cbf6f722e054ffa@-1 .3ddf7b708fd58745e563da8350@-1 ; .1c8fd94c2928abdf6ac76e7f72@-2 -.4ffe46515bcbd0e89ae65de0b0@-2 -.ef29026b49a874470c8ec26130@-2 .222432e8a14e6f5d111eb46448@-2 .651b4ebc3eb00dbccfeacce730@-2 -.274eb8cec98a08c459235d5d8c@-2 .1bb9281f67b5d19050ff09572e@-1 .fd0aa7bb2ed999fc7f324a1f90@-2 -.80e40b180c2e1911296511c7c0@-2 -.168dac9e3b43c10ca77963bf8e@-1 ; .283e777b80eb899cc67af9d988@-2 -.19157a05fd2043e159d81fdab6@-1 -.31584aac4ac067f924c7c614c4@-1 .c727e2a931505f2f2f9eb77ff0@-2 .40e4cd9b679f64e747449b19a8@-2 .8c4c8e3afe52e56ab5b82f1150@-2 .623baa1d9e52201c5a4ca5dc88@-1 .26d28b3c73ed647a919fda6098@-1 -.1a42530a4ae4670d309f0374d4@-1 -.46eecc96ab6f75ecbfebe4dab8@-1 ; -.378735245920a37cb93f8da740@-2 -.78dfe833feb8536812c25dc930@-2 -.43503e304dc33c991b72e28b78@-2 -.20fd00a9d6b3e34a64545ca3ac@-3 .12f0dde7cca701eda236d44838@-2 .4e2d5350fb8a06f4b823b32650@-2 .cff164f1b2baf319159ab2d4c0@-2 .237f041d17f5bd44f544d5606c@-2 .4ffb1568de2ce01c231fbbff78@-3 -.8e13984268942570be90622620@-2 ; -.1ac793526f4041f42664cae69c@-2 -.c5863854fdf5573119ae181bc0@-2 -.15bd1e011be00a2284738769de@-1 .728fad89765e1170827a7f93f8@-2 -.13feb3e69f762c4ecad26486a2@-2 .7529dc7f723b7d51af7e4f4598@-2 .2af578fa3ef42375ee982ed5f4@-1 .121afeda3034780f9b400da6ca@-1 -.8de7fbfc941069720001ac8a40@-2 -.2396f83a381e84e631da49063c@-1 ;";
char R2[] = ".1ebe5a7cc3e58420ac0ac21570@-1 .9cf0c287a1e88c9c1286e96690@-2 .685b6e535d47943c85bba46c18@-1 -.f6bd6d7d8dceb3508597404e20@-1 -.47f922af119cd7d11ef43fff98@-1 .11f9787297069ce10586d9520e@-1 .60982daa8805c6599620ad3f40@-1 .22510c676802cc3bc11ad1828c@-1 ; -.21102711f65e12e08c23f68e14@-1 .3c4b684023a2a89db489df302c@-1 -.84a22777621ccbc776c5aa3150@-1 .898714537b0f34498e0f38fa40@-2 .90217c8d8303d04a3c88798420@-1 .3e052d43c6e7e0bc29b00dbebc@-1 -.79224c788d6aa8e7c868b506c8@-1 .2a5e8e9fc55bd2f1106ee94d60@-1 ; -.6402bab2d5bdfbdae2bb77c7a8@-1 .390a73890e45a0ef75b55327e0@-1 -.c7d9065cbc5d26ab3b4f499390@-1 .108884241ac64de355832c2fca@0 .87e21fae434de62d8384f8dd70@-1 .362805bb918260655a954cfa6c@-1 -.a13a7ff107589af1496e921f50@-1 .a5d98403c52dac73d8fc3cb4e0@-2 ; .e9389c4233475d56a0af5bd650@-2 -.1489e27d2fa7a3ac43d895053e@-1 .3c935ff5bf808b0eb5195410c8@-1 .fd4a1d7dc54798e79359f1a0c0@-3 -.a8898ed8c484532b36874d2a00@-2 -.3563be13ff663099087d01e4d4@-1 .2fe3a6f999f185a2449285e5d0@-1 -.2061f41bb46338e26c87dc8890@-1 ; .1c9f967ba35269cdd771f9b5dc@-1 .478b3f0fef1b0db8527b8f9120@-1 -.106f83527cdef4ad67396f2db0@-1 -.379f751bb64b8eb6cebd2d1384@-1 .2822ebb398570ee22d3085d2c4@-1 .1af9ee3e01febfc2ab9bd70186@-1 -.433a4c678503c6af98ba50ca80@-1 -.8c67ddf43300d7bcd90a02b040@-2 ; .48c4c19633e7248a2ede0967a8@-1 -.4097c2168b54a366768b3a78a8@-1 .dcdd147c957c2f603c9e191cf0@-1 -.e716edb51b5b76efe460aaec00@-1 -.8c2bb9c65df069a28a837224f0@-1 -.29df8094e18a9c5f85c835fe14@-1 .88dda92d6335a5d278ec491810@-1 .992a2354827764d0f8fd8115d0@-2 ; -.257dd39e00c3e7785ef85d9cf4@-1 -.319a5505a2f1a1173d25bbe1f0@-1 -.45c56ea4c5e30638fa6d2e2f00@-1 .11e11611e2c1aa5c32f82440a2@0 -.1db30b80e79ea8bb715b5437be@-1 -.44e0fa1800f3fac3cae5b2c9e0@-1 -.2056581a1262cafaa3cd85c290@-2 -.a97fe9f4955fb72b11dec60370@-2 ; .2051479b9ea9813a9118c29e5c@-1 -.8d5dcfb60eff7116b3a85777f0@-2 -.e92d54cbb00686aa7363b35940@-2 -.1673ff7b02cce7d6a82212c532@-1 -.618347298041f32d2bcf2e09e0@-2 .310900888a1904c8c822775824@-1 .177d809f913542af587f8ddd5c@-1 -.1cbcf346c479962b1b8ebaaa8c@-1 ;";
#endif

char R3[] = "-.41c71d8@1 .1838e3a@2 -.e38e3a0@1 -.11c71c8@1 ; -.38e38ec@0 .271c71c@1 -.171c71c@1 -.38e38e4@0 ; .18e38ea@1 -.a1c71d0@1 .61c71d0@1 .8e38e40@0 ; .2e38e40@1 -.10c71c8@2 .9c71c70@1 .e38e390@0 ;";
char R6[] = ".5a29dc940@-1 -.3385a29d8@-1 ; -.179d3fdda@-1 .3e4179d3c@-1 ;";

#ifdef HAVE_OVERLENGTH_STRINGS
int test_mps_inv1()
{
    mps_t A, B, C;
    unsigned int i;
    mpfr_ptr xi, ri;

    int A1[10*10] = {
    115,    611,    678,    332,    25,     517,    391,    241,    506,    423,
    289,    88,     621,    345,    706,    521,    287,    650,    88,     449,
    722,    897,    242,    433,    967,    506,    523,    559,    561,    468,
    779,    790,    980,    818,    425,    246,    922,    100,    467,    395,
    36,     517,    832,    610,    187,    18,     843,    74,     853,    12,
    186,    492,    748,    941,    212,    579,    262,    436,    911,    808,
    810,    259,    413,    359,    691,    765,    357,    769,    547,    96,
    956,    220,    14,     819,    130,    968,    656,    244,    528,    846,
    787,    126,    788,    345,    265,    970,    887,    206,    852,    674,
    915,    28,     236,    701,    120,    828,    316,    530,    571,    47,
    };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );
    mps_init( C, 10, 10, 100, MPS_COL_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if( mps_inv( B, A ) != 0 )
    {
        printf("test_mps_inv1() failed. False detection of an un-invertible matrix.\n");
        exit(EXIT_FAILURE);
    }

    mps_set_str16( C, R1, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( C, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_inv1() failed. Value mismatch at index %d.", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_mps_inv2()
{
    mps_t A, B, C;
    unsigned int i;
    mpfr_ptr xi, ri;

    int A1[8*8] = {
    21,    75,    0,     33,    66,    62,    84,    68,
    87,    6,     56,    66,    72,    19,    54,    23,
    23,    21,    88,    65,    30,    93,    21,    31,
    36,    29,    56,    48,    33,    59,    50,    43,
    26,    63,    40,    91,    4,     48,    26,    41,
    28,    12,    77,    21,    11,    68,    15,    69,
    84,    40,    40,    87,    11,    19,    56,    58,
    68,    89,    50,    34,    38,    92,    94,    34,
    };

    mps_init( A, 8, 8, 100, MPS_COL_ORDER );
    mps_init( B, 8, 8, 100, MPS_COL_ORDER );
    mps_init( C, 8, 8, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 8*8; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if ( mps_inv( B, A ) != 0 )
    {
        printf("test_mps_inv2() failed. False detection of an un-invertible matrix.\n");
        exit(EXIT_FAILURE);
    }

    mps_set_str16( C, R2, 100, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 8*8; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( C, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_inv2() failed. Value mismatch at index %d.", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}
#endif

int test_mps_inv3()
{
    mps_t A, B, C;
    unsigned int i;
    mpfr_ptr xi, ri;

    int A1[4*4] = {
    3,    6,    2,    4,
    2,    4,    4,    1,
    2,    5,    6,    0,
    6,    1,    3,    7,
    };

    mps_init( A, 4, 4, 24, MPS_COL_ORDER );
    mps_init( B, 4, 4, 24, MPS_COL_ORDER );
    mps_init( C, 4, 4, 24, MPS_COL_ORDER );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if ( mps_inv( B, A ) != 0 )
    {
        printf("test_mps_inv3() failed, False detection of an un-invertible matrix.\n");
        exit(EXIT_FAILURE);
    }

    mps_set_str16( C, R3, 24, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( C, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_inv3() failed, Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_mps_inv4()
{
    mps_t A, B;
    unsigned int i;
    mpfr_ptr xi;

    int A1[13*13] = {
    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,
    };

    mps_init( A, 13, 13, 24, MPS_COL_ORDER );
    mps_init( B, 13, 13, 24, MPS_COL_ORDER );

    for ( i = 1; i <= 13*13; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if ( mps_inv( B, A ) != 0 )
    {
        printf("test_mps_inv4() failed, False detection of an un-invertible matrix.\n");
        exit(EXIT_FAILURE);
    }

    for ( i = 1; i <= 13*13; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, A1[i-1] ) != 0 )
        {
            printf("test_mps_inv4() failed, Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_inv5()
{
    mps_t A, B;
    unsigned int i;
    mpfr_ptr xi;

    int A1[13*13] = {
    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    };

    mps_init( A, 13, 13, 24, MPS_COL_ORDER );
    mps_init( B, 13, 13, 24, MPS_COL_ORDER );

    for ( i = 1; i <= 13*13; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if ( mps_inv( B, A ) == 0 )
    {
        printf("test_mps_inv5() failed, non-invertible matrix not detected.\n");
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_inv6()
{
    mps_t A, B, C;
    unsigned int i;
    mpfr_ptr xi, ri;

    int A1[2*2] = { 58, 48, 22, 84 };

    mps_init( A, 2, 2, 32, MPS_COL_ORDER );
    mps_init( B, 2, 2, 32, MPS_COL_ORDER );
    mps_init( C, 2, 2, 32, MPS_COL_ORDER );

    for ( i = 1; i <= 2*2; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    if ( mps_inv( B, A ) != 0 )
    {
        printf("test_mps_inv6() failed, False detection of an un-invertible matrix.\n");
        exit(EXIT_FAILURE);
    }

    mps_set_str16( C, R6, 32, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 2*2; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        ri = mps_get_ele_seq( C, i );
        if ( mpfr_cmp( xi, ri ) != 0 )
        {
            printf("test_mps_inv6() failed, Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int main()
{
#ifdef HAVE_OVERLENGTH_STRINGS
    test_mps_inv1();
    test_mps_inv2();
#endif

    test_mps_inv3();
    test_mps_inv4();
    test_mps_inv5();
    test_mps_inv6();

    return 0;
}

