/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test mps_subsubslice(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int t_mps_subsubslice1()
{
    mps_t v;
    mps_blk_t b_v, b_vslice;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( v, 10, 1, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( v, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_full_submat( b_v, v );

    for ( i = 1; i<=10*1; i++ )
    {
        xi = mps_get_ele_blk( b_v, i, MPS_ROW_ORDER );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("t_mps_subsubslice1() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_subsubslice( b_vslice, b_v, 2, 5 );

    for ( i = 1; i <= 5*1; i++ )
    {
        xi = mps_get_ele_blk( b_vslice, i, MPS_ROW_ORDER );
        if ( mpfr_cmp_ui( xi, i+1 ) != 0 )
        {
            printf("t_mps_subsubslice1() failed for subslice 2,5. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}

int t_mps_subsubslice2()
{
    mps_t v;
    mps_blk_t b_v, b_vslice;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( v, 1, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( v, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_full_submat( b_v, v );

    for ( i = 1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_blk( b_v, 1, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            printf("t_mps_subsubslice2() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_subsubslice( b_vslice, b_v, 2, 5 );

    for ( i = 1; i <= 5*1; i++ )
    {
        xi = mps_get_ele_blk( b_vslice, 1, i );
        if ( mpfr_cmp_ui( xi, i+1 ) != 0 )
        {
            printf("t_mps_subsubslice2() failed for subslice 2,5. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}


int main()
{
    t_mps_subsubslice1();
    t_mps_subsubslice2();

    return 0;
}

