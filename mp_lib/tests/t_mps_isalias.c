/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_isalias() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_isalias1()
{
    mps_t A, B;

    mps_init( A, 2, 2, 100, MPS_COL_ORDER );
    mps_init( B, 4, 4, 10, MPS_COL_ORDER );

    if ( mps_isalias( A, B ) != 0 )
    {
        printf( "test_mps_isalias1() failed. (mps_isalias( A, B ) != 0) )\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_isalias2()
{
    mps_t A, B;

    mps_init( A, 2, 2, 100, MPS_COL_ORDER );
    mps_init( B, 4, 4, 10, MPS_COL_ORDER );

    if ( mps_isalias( A, A ) != 1 )
    {
        printf( "test_mps_isalias2() failed. (mps_isalias( A, B ) != 1) )\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_mps_isalias1();
    test_mps_isalias2();

    return 0;
}

