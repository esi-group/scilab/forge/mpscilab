/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the matrix set by a double function mps_double_input(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_double_input1()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {0};

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_double_input2()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {0};

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i=1; i <= 10*10; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_row( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_double_input3()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[13*7] = {0};

    mps_init( A, 13, 7, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 13*7; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 13*7; i++ )
    {
        xi = mps_get_ele_col( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_double_input4()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[7*13] = {0};

    mps_init( A, 7, 13, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*13; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_COL_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*13; i++ )
    {
        xi = mps_get_ele_col( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input4() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_double_input5()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[13*7] = {0};

    mps_init( A, 13, 7, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 13*7; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 13*7; i++ )
    {
        xi = mps_get_ele_row( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input5() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_double_input6()
{
    mps_t A;
    mpfr_ptr xi;
    unsigned int i;

    double R[7*13] = {0};

    mps_init( A, 7, 13, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*13; i++ )
    {
        R[i-1] = i;
    }

    mps_double_input( A, R, MPS_ROW_ORDER, GMP_RNDN );

    for ( i = 1; i <= 7*13; i++ )
    {
        xi = mps_get_ele_row( A, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_double_input6() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_double_input1();
    test_double_input2();
    test_double_input3();
    test_double_input4();
    test_double_input5();
    test_double_input6();

    return 0;
}

