/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test basic MPFR functions that are required for the other tests. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"


int main()
{
  mpfr_t A, B, C;

  mpfr_init2( A, 100 );
  mpfr_init2( B, 100 );
  mpfr_init2( C, 100 );

  mpfr_set_ui( A, 2, GMP_RNDN );

  if( mpfr_cmp_ui( A, 2 ) != 0 )
  {
    printf("mpfr_cmp_ui() failed; mpfr_cmp_ui( A, 2 ) != 0\n");
    exit(EXIT_FAILURE);
  }

  if( mpfr_cmp_ui( A, 1 ) <= 0 )
  {
    printf("mpfr_cmp_ui() failed; mpfr_cmp_ui( A, 1 ) >= 0\n");
    exit(EXIT_FAILURE);
  }

  if( mpfr_cmp_ui( A, 3 ) >= 0 )
  {
    printf("mpfr_cmp_ui() failed; mpfr_cmp_ui( A, 3 ) <= 0\n");
    exit(EXIT_FAILURE);
  }

  mpfr_set_ui( B, 3, GMP_RNDN );

  mpfr_add(C, A, B, GMP_RNDN );

  if( mpfr_cmp_ui( C, 5 ) != 0 )
  {
    printf("mpfr_add() failed; mpfr_cmp_ui( C, 5 ) != 0\n");
    exit(EXIT_FAILURE);
  }

  mpfr_sub(A, C, B, GMP_RNDN );

  if( mpfr_cmp_ui( A, 2 ) != 0 )
  {
    printf("mpfr_sub() failed; mpfr_cmp_ui( C, 2 ) != 0\n");
    exit(EXIT_FAILURE);
  }

  mpfr_clear( A );
  mpfr_clear( B );
  mpfr_clear( C );

  return 0;
}
