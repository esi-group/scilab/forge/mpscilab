/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test parallel multiplication functions xmps_mul(). (Hadamard product) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

unsigned int n_threads;

int test_xmps_mul1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B, C;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );
    mps_init( C, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    xmps_mul( C, A, B, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, i*2*i ) != 0 )
        {
            mpfr_printf("test_xmps_mul1() failed. Value mismatch at index %u for params: n=%u, m=%u, prec=%u, order=%u, threads=%u\n", \
                i, n, m , prec, order, n_threads);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_xmps_mul2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    xmps_mul( B, A, B, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i*2*i ) != 0 )
        {
            mpfr_printf("test_xmps_mul2() failed. Value mismatch at index %u for params: n=%u, m=%u, prec=%u, order=%u, threads=%u\n", \
                i, n, m , prec, order, n_threads);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_xmps_mul3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    xmps_mul( A, A, B, GMP_RNDN, n_threads );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i*2*i ) != 0 )
        {
            mpfr_printf("test_xmps_mul3() failed. Value mismatch at index %u for params: n=%u, m=%u, prec=%u, order=%u, threads=%u\n", \
                i, n, m , prec, order, n_threads);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

/* Test transposed square matrices. */
int test_xmps_mul4( unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i, j;
    mpfr_ptr xi;

    mps_init( A, n, n, prec, order );
    mps_init( B, n, n, prec, order );

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_flip_order( B );

    xmps_mul( A, A, B, GMP_RNDN, n_threads );

    for ( j = 1; j <= n; j++ )
    {
        for ( i = 1; i <= n; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if ( mpfr_cmp_ui( xi, (i + (j-1)*n) * 2*(j + (i-1)*n) ) != 0 )
            {
                mpfr_printf("test_xmps_mul4() failed. Value mismatch at index %u for params: n=%u, prec=%u, order=%u, threads=%u\n", \
                    i, n , prec, order, n_threads);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    unsigned int i;

    for ( i = 0; i <= 4; i++ )
    {
        n_threads = i;

        test_xmps_mul1( 20, 20, 100, MPS_COL_ORDER );
        test_xmps_mul1( 11, 13, 100, MPS_COL_ORDER );
        test_xmps_mul1( 10, 20, 50, MPS_COL_ORDER );
        test_xmps_mul1( 20, 10, 53, MPS_COL_ORDER );
        test_xmps_mul1( 1, 10, 53, MPS_COL_ORDER );
        test_xmps_mul1( 10, 1, 64, MPS_COL_ORDER );
        test_xmps_mul1( 1, 2, 64, MPS_COL_ORDER );
        test_xmps_mul1( 2, 1, 64, MPS_COL_ORDER );
        test_xmps_mul1( 1, 1, 64, MPS_COL_ORDER );
        test_xmps_mul1( 10, 10, 100, MPS_ROW_ORDER );
        test_xmps_mul1( 10, 20, 50, MPS_ROW_ORDER );
        test_xmps_mul1( 20, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul1( 1, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul1( 10, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul1( 1, 2, 64, MPS_ROW_ORDER );
        test_xmps_mul1( 2, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul1( 1, 1, 64, MPS_ROW_ORDER );

        test_xmps_mul2( 10, 10, 100, MPS_COL_ORDER );
        test_xmps_mul2( 11, 12, 50, MPS_COL_ORDER );
        test_xmps_mul2( 10, 20, 50, MPS_COL_ORDER );
        test_xmps_mul2( 20, 10, 53, MPS_COL_ORDER );
        test_xmps_mul2( 1, 10, 53, MPS_COL_ORDER );
        test_xmps_mul2( 10, 1, 64, MPS_COL_ORDER );
        test_xmps_mul2( 1, 2, 64, MPS_COL_ORDER );
        test_xmps_mul2( 2, 1, 64, MPS_COL_ORDER );
        test_xmps_mul2( 1, 1, 64, MPS_COL_ORDER );
        test_xmps_mul2( 10, 10, 100, MPS_ROW_ORDER );
        test_xmps_mul2( 10, 20, 50, MPS_ROW_ORDER );
        test_xmps_mul2( 20, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul2( 1, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul2( 10, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul2( 1, 2, 64, MPS_ROW_ORDER );
        test_xmps_mul2( 2, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul2( 1, 1, 64, MPS_ROW_ORDER );

        test_xmps_mul3( 10, 10, 100, MPS_COL_ORDER );
        test_xmps_mul3( 11, 13, 100, MPS_COL_ORDER );
        test_xmps_mul3( 10, 20, 50, MPS_COL_ORDER );
        test_xmps_mul3( 20, 10, 53, MPS_COL_ORDER );
        test_xmps_mul3( 1, 10, 53, MPS_COL_ORDER );
        test_xmps_mul3( 10, 1, 64, MPS_COL_ORDER );
        test_xmps_mul3( 1, 2, 64, MPS_COL_ORDER );
        test_xmps_mul3( 2, 1, 64, MPS_COL_ORDER );
        test_xmps_mul3( 1, 1, 64, MPS_COL_ORDER );
        test_xmps_mul3( 10, 10, 100, MPS_ROW_ORDER );
        test_xmps_mul3( 10, 20, 50, MPS_ROW_ORDER );
        test_xmps_mul3( 20, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul3( 1, 10, 53, MPS_ROW_ORDER );
        test_xmps_mul3( 10, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul3( 1, 2, 64, MPS_ROW_ORDER );
        test_xmps_mul3( 2, 1, 64, MPS_ROW_ORDER );
        test_xmps_mul3( 1, 1, 64, MPS_ROW_ORDER );

        test_xmps_mul4( 10, 100, MPS_COL_ORDER );
        test_xmps_mul4( 20, 50, MPS_COL_ORDER );
        test_xmps_mul4( 21, 50, MPS_COL_ORDER );
        test_xmps_mul4( 2, 64, MPS_COL_ORDER );
        test_xmps_mul4( 1, 53, MPS_COL_ORDER );
    }

    return 0;
}
