/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test matrix subtraction mps_sub(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_matrix_sub1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B, C;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );
    mps_init( C, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_sub( C, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_matrix_sub1() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_matrix_sub2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_sub( B, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_matrix_sub2() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_matrix_sub3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_sub( A, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_matrix_sub3() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

/* Test transposed square matrices. */
int test_matrix_sub4( unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i, j;

    mpfr_ptr xi;

    mps_init( A, n, n, prec, order );
    mps_init( B, n, n, prec, order );

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 100*i, GMP_RNDN );
    }

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( B );

    mps_sub( A, A, B, GMP_RNDN );

    for ( j = 1; j <= n; j++ )
    {
        for ( i = 1; i <= n; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if ( mpfr_cmp_ui( xi, 100*(i + (j-1)*n) - (j + (i-1)*n) ) != 0 )
            {
                mpfr_printf("test_matrix_sub4() failed. Value mismatch at index %u,%u for matrix size n=%u, prec=%u, order=%u\n", i, j, n , prec, order);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_matrix_sub1( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_sub1( 11, 13, 100, MPS_COL_ORDER );
    test_matrix_sub1( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_sub1( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_sub1( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_sub1( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_sub1( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_sub1( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_sub1( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_sub1( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_sub1( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_sub1( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub1( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub1( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub1( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_sub1( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub1( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_sub2( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_sub2( 11, 12, 50, MPS_COL_ORDER );
    test_matrix_sub2( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_sub2( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_sub2( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_sub2( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_sub2( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_sub2( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_sub2( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_sub2( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_sub2( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_sub2( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub2( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub2( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub2( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_sub2( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub2( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_sub3( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_sub3( 11, 13, 100, MPS_COL_ORDER );
    test_matrix_sub3( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_sub3( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_sub3( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_sub3( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_sub3( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_sub3( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_sub3( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_sub3( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_sub3( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_sub3( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub3( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_sub3( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub3( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_sub3( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_sub3( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_sub4( 10, 100, MPS_COL_ORDER );
    test_matrix_sub4( 20, 50, MPS_COL_ORDER );
    test_matrix_sub4( 21, 50, MPS_COL_ORDER );
    test_matrix_sub4( 2, 64, MPS_COL_ORDER );
    test_matrix_sub4( 1, 53, MPS_COL_ORDER );

    return 0;
}
