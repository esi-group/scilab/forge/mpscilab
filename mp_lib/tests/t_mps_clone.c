/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_copy() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_clone1( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec, MPS_COL_ORDER );
    mps_init( B, 1, 1, 10, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_clone( B, A );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_clone1() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_clone2( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec, MPS_COL_ORDER );
    mps_init( B, 1, 1, 10, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_clone( B, A );

    mps_free( A );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_clone2() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );

    return 0;
}

int test_mps_clone3( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec, MPS_COL_ORDER );
    mps_init( B, 1, 1, 10, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_clone( B, A );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_clone3() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );
    mps_free( A );

    return 0;
}

int test_mps_clone4( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec, MPS_COL_ORDER );
    mps_init( B, 1, 1, 10, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_clone( B, A );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_add_ui( xi, xi, 1, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_clone4() failed. Value mismatch at index %d for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );
    mps_free( A );

    return 0;
}

int test_mps_clone5( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t A, B;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, n, m, prec, MPS_COL_ORDER );
    mps_init( B, 1, 1, 10, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_clone( B, A );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_add_ui( xi, xi, 1, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_mps_clone5() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( B );
    mps_free( A );

    return 0;
}

int main()
{
    test_mps_clone1( 10, 10, 100 );
    test_mps_clone1( 20, 20, 10 );
    test_mps_clone1( 10, 1, 100 );
    test_mps_clone1( 1, 10, 53 );
    test_mps_clone1( 1, 1, 87 );

    test_mps_clone2( 10, 10, 100 );
    test_mps_clone2( 20, 20, 10 );
    test_mps_clone2( 10, 1, 100 );
    test_mps_clone2( 1, 10, 53 );
    test_mps_clone2( 1, 1, 87 );

    test_mps_clone3( 10, 10, 100 );
    test_mps_clone3( 20, 20, 10 );
    test_mps_clone3( 10, 1, 100 );
    test_mps_clone3( 1, 10, 53 );
    test_mps_clone3( 1, 1, 87 );

    test_mps_clone4( 10, 10, 100 );
    test_mps_clone4( 20, 20, 10 );
    test_mps_clone4( 10, 1, 100 );
    test_mps_clone4( 1, 10, 53 );
    test_mps_clone4( 1, 1, 87 );

    test_mps_clone5( 10, 10, 100 );
    test_mps_clone5( 20, 20, 10 );
    test_mps_clone5( 10, 1, 100 );
    test_mps_clone5( 1, 10, 53 );
    test_mps_clone5( 1, 1, 87 );

    return 0;
}

