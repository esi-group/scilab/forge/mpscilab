/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test matrix addition mps_add(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_matrix_add1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B, C;
    unsigned int i;

    mpfr_ptr xi;

    mps_inits( n, m, prec, order, A, B, C, (mps_ptr) NULL );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_add( C, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, 3*i ) != 0 )
        {
            mpfr_printf("test_matrix_add1() failed. Value mismatch at index %u for matrix size n=%u , m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_frees( A, B, C, (mps_ptr) NULL );

    return 0;
}

int test_matrix_add2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_add( B, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, 3*i ) != 0 )
        {
            mpfr_printf("test_matrix_add2() failed. Value mismatch at index %d for matrix size n=%u, m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_matrix_add3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_add( A, A, B, GMP_RNDN );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        if ( mpfr_cmp_ui( xi, 3*i ) != 0 )
        {
            mpfr_printf("test_matrix_add3() failed. Value mismatch at index %u for matrix size n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

/* Test transposed square matrices. */
int test_matrix_add4( unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i, j;

    mpfr_ptr xi;

    mps_init( A, n, n, prec, order );
    mps_init( B, n, n, prec, order );

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*n; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2*i, GMP_RNDN );
    }

    mps_flip_order( B );

    mps_add( A, A, B, GMP_RNDN );

    for ( j = 1; j <= n; j++ )
    {
        for ( i = 1; i <= n; i++ )
        {
            xi = mps_get_ele( A, i, j );
            if ( mpfr_cmp_ui( xi, i + (j-1)*n + 2*(j + (i-1)*n) ) != 0 )
            {
                mpfr_printf("test_matrix_add4() failed. Value mismatch at index %u,%u for matrix size n=%u, prec=%u, order=%u\n", i, j, n , prec, order);
                exit(EXIT_FAILURE);
            }
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_matrix_add1( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_add1( 11, 13, 100, MPS_COL_ORDER );
    test_matrix_add1( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_add1( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_add1( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_add1( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_add1( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_add1( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_add1( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_add1( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_add1( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_add1( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_add1( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_add1( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_add1( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_add1( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_add1( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_add2( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_add2( 11, 12, 50, MPS_COL_ORDER );
    test_matrix_add2( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_add2( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_add2( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_add2( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_add2( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_add2( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_add2( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_add2( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_add2( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_add2( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_add2( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_add2( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_add2( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_add2( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_add2( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_add3( 10, 10, 100, MPS_COL_ORDER );
    test_matrix_add3( 11, 13, 100, MPS_COL_ORDER );
    test_matrix_add3( 10, 20, 50, MPS_COL_ORDER );
    test_matrix_add3( 20, 10, 53, MPS_COL_ORDER );
    test_matrix_add3( 1, 10, 53, MPS_COL_ORDER );
    test_matrix_add3( 10, 1, 64, MPS_COL_ORDER );
    test_matrix_add3( 1, 2, 64, MPS_COL_ORDER );
    test_matrix_add3( 2, 1, 64, MPS_COL_ORDER );
    test_matrix_add3( 1, 1, 64, MPS_COL_ORDER );
    test_matrix_add3( 10, 10, 100, MPS_ROW_ORDER );
    test_matrix_add3( 10, 20, 50, MPS_ROW_ORDER );
    test_matrix_add3( 20, 10, 53, MPS_ROW_ORDER );
    test_matrix_add3( 1, 10, 53, MPS_ROW_ORDER );
    test_matrix_add3( 10, 1, 64, MPS_ROW_ORDER );
    test_matrix_add3( 1, 2, 64, MPS_ROW_ORDER );
    test_matrix_add3( 2, 1, 64, MPS_ROW_ORDER );
    test_matrix_add3( 1, 1, 64, MPS_ROW_ORDER );

    test_matrix_add4( 10, 100, MPS_COL_ORDER );
    test_matrix_add4( 20, 50, MPS_COL_ORDER );
    test_matrix_add4( 21, 50, MPS_COL_ORDER );
    test_matrix_add4( 2, 64, MPS_COL_ORDER );
    test_matrix_add4( 1, 53, MPS_COL_ORDER );

    return 0;
}

