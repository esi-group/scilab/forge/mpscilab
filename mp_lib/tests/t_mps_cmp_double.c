/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_cmp() family of functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_cmp_double1( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    double B[m*n];
    mps_t A;
    mpfr_ptr xi;
    int rop[m*n];
    unsigned int i;

    mps_init( A, m, n, prec, order );

    for( i = 1; i <= m*n; i++ )
    {
        B[i-1] = i;
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_cmp_double( (int*)rop, roporder, A, (double*)B, MPS_ROW_ORDER );

    for ( i = 1; i <= m*n; i++ )
    {
        if( rop[i-1] != 0 )
        {
            mpfr_printf("test_mps_cmp_double1() failed. i=%u, m=%u , n=%u , prec=%u, roporder=%u, order=%u, rop=%d\n", i, m, n, prec, roporder, order, rop[i-1]);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_mps_cmp_double2( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    double B[m*n];
    mps_t A;
    mpfr_ptr xi;
    int rop[m*n];
    unsigned int i;

    mps_init( A, m, n, prec, order );

    for( i = 1; i <= m*n; i++ )
    {
        B[i-1] = i+1;
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_cmp_double( (int*)rop, roporder, A, (double*)B, MPS_ROW_ORDER );

    for ( i = 1; i <= m*n; i++ )
    {
        if( !(rop[i-1] < 0) )
        {
            mpfr_printf("test_mps_cmp_double2() failed. i=%u, m=%u , n=%u , prec=%u, roporder=%u, order=%u, rop=%d\n", i, m, n, prec, roporder, order, rop[i-1]);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_mps_cmp_double3( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    double B[m*n];
    mps_t A;
    mpfr_ptr xi;
    int rop[m*n];
    unsigned int i;

    mps_init( A, m, n, prec, order );

    for( i = 1; i <= m*n; i++ )
    {
        B[i-1] = i;
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i+1, GMP_RNDN );
    }

    mps_cmp_double( (int*)rop, roporder, A, (double*)B, MPS_ROW_ORDER );

    for ( i = 1; i <= m*n; i++ )
    {
        if( !(rop[i-1] > 0) )
        {
            mpfr_printf("test_mps_cmp_double3() failed. i=%u, m=%u , n=%u , prec=%u, roporder=%u, order=%u, rop=%d\n", i, m, n, prec, roporder, order, rop[i-1]);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_mps_cmp_double4()
{
    mps_t A;
    unsigned int i;
    double B[4*8];
    int rop[4*8];
    int ref[4*8] = {
    1,  0, 0, 0, 0,  0, 0, 0,
    0, -1, 0, 0, 0,  0, 0, 0,
    0,  0, 0, 0, 0, -1, 0, 0,
    0,  0, 0, 0, 0,  0, 1, 0 };

    mpfr_ptr xi;

    mps_init( A, 4, 8, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 4*8; i++ )
        B[i-1] = 0;

    xi = mps_get_ele( A, 1, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( A, 4, 7 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    B[9] = 2;
    B[21] = 3;

    mps_cmp_double( (int*)rop, MPS_ROW_ORDER, A, (double*)B, MPS_ROW_ORDER );

    for ( i = 1; i <= 4*8; i++ )
    {
        if( rop[i-1] != ref[i-1] )
        {
            mpfr_printf("test_mps_cmp_double4() failed. i=%u\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_mps_cmp_double5()
{
    mps_t A;
    double B[4*8];
    unsigned int i;
    int rop[4*8];
    int ref[4*8] = {
    1,  0,  0, 0,
    0, -1,  0, 0,
    0,  0,  0, 0,
    0,  0,  0, 0,
    0,  0,  0, 0,
    0,  0, -1, 0,
    0,  0,  0, 1,
    0,  0,  0, 0 };

    mpfr_ptr xi;

    mps_init( A, 4, 8, 100, MPS_COL_ORDER );
    for ( i = 1; i <= 4*8; i++ )
        B[i-1] = 0;

    xi = mps_get_ele( A, 1, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( A, 4, 7 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    B[5] = 2;
    B[22] = 3;

    mps_cmp_double( (int*)rop, MPS_COL_ORDER, A, (double*)B, MPS_COL_ORDER );

    for ( i = 1; i <= 4*8; i++ )
    {
        if( rop[i-1] != ref[i-1] )
        {
            mpfr_printf("test_mps_cmp_double5() failed. i=%u\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_mps_cmp_double1( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp_double1( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double1( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double1( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp_double2( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp_double2( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double2( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double2( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp_double3( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp_double3( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp_double3( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp_double3( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp_double4();
    test_mps_cmp_double5();

    return 0;
}
