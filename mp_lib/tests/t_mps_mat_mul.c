/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_mat_mul() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mat_mul1()
{
    mps_t A;
    mps_t B;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {
    3355,  3410,  3465,  3520,  3575,  3630,  3685,  3740,  3795,  3850,
    7955,  8110,  8265,  8420,  8575,  8730,  8885,  9040,  9195,  9350,
    12555, 12810, 13065, 13320, 13575, 13830, 14085, 14340, 14595, 14850,
    17155, 17510, 17865, 18220, 18575, 18930, 19285, 19640, 19995, 20350,
    21755, 22210, 22665, 23120, 23575, 24030, 24485, 24940, 25395, 25850,
    26355, 26910, 27465, 28020, 28575, 29130, 29685, 30240, 30795, 31350,
    30955, 31610, 32265, 32920, 33575, 34230, 34885, 35540, 36195, 36850,
    35555, 36310, 37065, 37820, 38575, 39330, 40085, 40840, 41595, 42350,
    40155, 41010, 41865, 42720, 43575, 44430, 45285, 46140, 46995, 47850,
    44755, 45710, 46665, 47620, 48575, 49530, 50485, 51440, 52395, 53350 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );
    mps_init( B, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_mul( B, A, A, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_mat_mul1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mat_mul2()
{
    mps_t A, B, C;
    mpfr_ptr xi;
    unsigned int i;

    double R[7*7] = {
    4082,  4160,  4238,  4316,  4394,  4472,  4550,
    9770,  9992,  10214, 10436, 10658, 10880, 11102,
    15458, 15824, 16190, 16556, 16922, 17288, 17654,
    21146, 21656, 22166, 22676, 23186, 23696, 24206,
    26834, 27488, 28142, 28796, 29450, 30104, 30758,
    32522, 33320, 34118, 34916, 35714, 36512, 37310,
    38210, 39152, 40094, 41036, 41978, 42920, 43862
    };

    mps_init( A, 7, 12, 100, MPS_COL_ORDER );
    mps_init( B, 12, 7, 100, MPS_COL_ORDER );
    mps_init( C, 7, 7, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*12; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_mul( C, A, B, GMP_RNDN );

    for ( i = 1; i <= 7*7; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_mat_mul2() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_mat_mul3()
{
    mps_t A;
    mps_t B;
    mps_t C;
    mpfr_ptr xi;
    unsigned int i;

    double R[12*12] = {
    1372,   1400,   1428,   1456,   1484,   1512,   1540,   1568,   1596,   1624,   1652,   1680,
    3185,   3262,   3339,   3416,   3493,   3570,   3647,   3724,   3801,   3878,   3955,   4032,
    4998,   5124,   5250,   5376,   5502,   5628,   5754,   5880,   6006,   6132,   6258,   6384,
    6811,   6986,   7161,   7336,   7511,   7686,   7861,   8036,   8211,   8386,   8561,   8736,
    8624,   8848,   9072,   9296,   9520,   9744,   9968,   10192,  10416,  10640,  10864,  11088,
    10437,  10710,  10983,  11256,  11529,  11802,  12075,  12348,  12621,  12894,  13167,  13440,
    12250,  12572,  12894,  13216,  13538,  13860,  14182,  14504,  14826,  15148,  15470,  15792,
    14063,  14434,  14805,  15176,  15547,  15918,  16289,  16660,  17031,  17402,  17773,  18144,
    15876,  16296,  16716,  17136,  17556,  17976,  18396,  18816,  19236,  19656,  20076,  20496,
    17689,  18158,  18627,  19096,  19565,  20034,  20503,  20972,  21441,  21910,  22379,  22848,
    19502,  20020,  20538,  21056,  21574,  22092,  22610,  23128,  23646,  24164,  24682,  25200,
    21315,  21882,  22449,  23016,  23583,  24150,  24717,  25284,  25851,  26418,  26985,  27552,
    };

    mps_init( A, 7, 12, 100, MPS_COL_ORDER );
    mps_init( B, 12, 7, 100, MPS_COL_ORDER );
    mps_init( C, 12, 12, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 7*12; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_mul( C, B, A, GMP_RNDN );

    for ( i = 1; i <= 12*12; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_mat_mul3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_mat_mul4()
{
    mps_t A;
    mps_t B;
    mps_t C;
    mpfr_ptr xi;
    unsigned int i;

    double R[10*10] = {
    1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
    2,  4,  6,  8,  10, 12, 14, 16, 18, 20,
    3,  6,  9,  12, 15, 18, 21, 24, 27, 30,
    4,  8,  12, 16, 20, 24, 28, 32, 36, 40,
    5,  10, 15, 20, 25, 30, 35, 40, 45, 50,
    6,  12, 18, 24, 30, 36, 42, 48, 54, 60,
    7,  14, 21, 28, 35, 42, 49, 56, 63, 70,
    8,  16, 24, 32, 40, 48, 56, 64, 72, 80,
    9,  18, 27, 36, 45, 54, 63, 72, 81, 90,
    10, 20, 30, 40, 50, 60, 70, 80, 90, 100,
    };

    mps_init( A, 10, 1, 100, MPS_COL_ORDER );
    mps_init( B, 1, 10, 100, MPS_COL_ORDER );
    mps_init( C, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_mul( C, A, B, GMP_RNDN );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( C, i );
        if ( mpfr_cmp_ui( xi, R[i-1] ) != 0 )
        {
            printf("test_mat_mul4() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    mps_free( C );

    return 0;
}

int test_mat_mul5()
{
    mps_t A, B, C;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, 10, 1, 100, MPS_COL_ORDER );
    mps_init( B, 1, 10, 100, MPS_COL_ORDER );
    mps_init( C, 1, 1, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*1; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_mat_mul( C, B, A, GMP_RNDN );

    xi = mps_get_ele_seq( C, 1 );
    if ( mpfr_cmp_ui( xi, 385 ) != 0 )
    {
        printf("test_mat_mul5() failed. Value mismatch at index %d\n", i);
        exit(EXIT_FAILURE);
    }

    mps_frees( A, B, C, (mps_ptr) NULL );

    return 0;
}

int test_mat_mul6()
{
    mps_t A, B, C;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( A, 1, 1, 100, MPS_COL_ORDER );
    mps_init( B, 1, 1, 100, MPS_COL_ORDER );
    mps_init( C, 1, 1, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 1*1; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, 2, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, 2, GMP_RNDN );
    }

    mps_mat_mul( C, B, A, GMP_RNDN );

    xi = mps_get_ele_seq( C, 1 );
    if ( mpfr_cmp_ui( xi, 4 ) != 0 )
    {
        printf("test_mat_mul6() failed. Value mismatch at index %d\n", i);
        exit(EXIT_FAILURE);
    }

    mps_frees( A, B, C, (mps_ptr) NULL );

    return 0;
}

int main()
{
    test_mat_mul1();
    test_mat_mul2();
    test_mat_mul3();
    test_mat_mul4();
    test_mat_mul5();
    test_mat_mul6();

    return 0;
}

