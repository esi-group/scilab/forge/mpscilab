/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_cmp() family of functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_cmp1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rop[n*m];

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_cmp( (int*)rop, roporder,  A, B );

    for ( i = 1; i <= n*m; i++ )
    {
        if( rop[i-1] != 0 )
        {
            mpfr_printf("test_mps_cmp_double1() failed. i=%u, n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_cmp2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rop[n*m];

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i+1, GMP_RNDN );
    }

    mps_cmp( (int*)rop, roporder,  A, B );

    for ( i = 1; i <= n*m; i++ )
    {
        if( rop[i-1] > 0 )
        {
            mpfr_printf("test_mps_cmp2() failed. i=%u, n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_cmp3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t roporder, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rop[n*m];

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i+1, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_cmp( (int*)rop, roporder, A, B );

    for ( i = 1; i <= n*m; i++ )
    {
        if( rop[i-1] < 0 )
        {
            mpfr_printf("test_mps_cmp3() failed. i=%u, n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_cmp4()
{
    mps_t A, B;
    unsigned int i;
    int rop[4*8];
    int ref[4*8] = {
    1,  0, 0, 0, 0,  0, 0, 0,
    0, -1, 0, 0, 0,  0, 0, 0,
    0,  0, 0, 0, 0, -1, 0, 0,
    0,  0, 0, 0, 0,  0, 1, 0 };

    mpfr_ptr xi;

    mps_init( A, 4, 8, 100, MPS_COL_ORDER );
    mps_init( B, 4, 8, 100, MPS_COL_ORDER );  

    xi = mps_get_ele( A, 1, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( A, 4, 7 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    xi = mps_get_ele( B, 2, 2 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( B, 3, 6 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    mps_cmp( (int*)rop, MPS_ROW_ORDER, A, B );

    for ( i = 1; i <= 4*8; i++ )
    {
        if( rop[i-1] != ref[i-1] )
        {
            mpfr_printf("test_mps_cmp4() failed. i=%u\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_cmp5()
{
    mps_t A, B;
    unsigned int i;
    int rop[4*8];
    int ref[4*8] = {
    1,  0,  0, 0,
    0, -1,  0, 0,
    0,  0,  0, 0,
    0,  0,  0, 0,
    0,  0,  0, 0,
    0,  0, -1, 0,
    0,  0,  0, 1,
    0,  0,  0, 0 };

    mpfr_ptr xi;

    mps_init( A, 4, 8, 100, MPS_COL_ORDER );
    mps_init( B, 4, 8, 100, MPS_COL_ORDER );  

    xi = mps_get_ele( A, 1, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( A, 4, 7 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    xi = mps_get_ele( B, 2, 2 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    xi = mps_get_ele( B, 3, 6 );
    mpfr_set_si( xi, 3, GMP_RNDN );

    mps_cmp( (int*)rop, MPS_COL_ORDER, A, B );

    for ( i = 1; i <= 4*8; i++ )
    {
        if( rop[i-1] != ref[i-1] )
        {
            mpfr_printf("test_mps_cmp5() failed. i=%u\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int main()
{
    test_mps_cmp1( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp1( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp1( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp1( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp2( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp2( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp2( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp2( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp3( 10, 10, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 11, 13, 100, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 20, 50, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 20, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 10, 53, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 2, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 2, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 1, 64, MPS_COL_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 10, 100, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 20, 50, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 20, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 10, 53, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 2, 64, MPS_ROW_ORDER, MPS_COL_ORDER);
    test_mps_cmp3( 2, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 1, 1, 64, MPS_ROW_ORDER, MPS_COL_ORDER );
    test_mps_cmp3( 10, 10, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 11, 13, 100, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 10, 20, 50, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 20, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 10, 53, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 10, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 2, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 2, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 1, 64, MPS_COL_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 10, 10, 100, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 10, 20, 50, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 20, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 10, 53, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 10, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 2, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 2, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );
    test_mps_cmp3( 1, 1, 64, MPS_ROW_ORDER, MPS_ROW_ORDER );

    test_mps_cmp4();
    test_mps_cmp5();

    return 0;
}
