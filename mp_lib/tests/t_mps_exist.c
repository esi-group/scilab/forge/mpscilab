/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the matrix inverse function mps_inv(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_exist1()
{
    mps_t A, B, C, D;

    mps_inits( 4, 4, 24, MPS_COL_ORDER, A, B, C, D, (mps_ptr) NULL );

    if ( mps_exist( A ) == 0 )
    {
        printf("test_mps_exist1() failed, mps_exist( A ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( B ) == 0 )
    {
        printf("test_mps_exist1() failed, mps_exist( B ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( C ) == 0 )
    {
        printf("test_mps_exist1() failed, mps_exist( C ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( D ) == 0 )
    {
        printf("test_mps_exist1() failed, mps_exist( D ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    mps_frees( A, B, C, D, (mps_ptr) NULL );

    return 0;
}

int test_mps_exist2()
{
    mps_t A, B, C, D;

    mps_inits( 4, 4, 24, MPS_COL_ORDER, A, B, C, D, (mps_ptr) NULL );

    mps_free( B );

    if ( mps_exist( A ) == 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( A ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( B ) != 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( B ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( C ) == 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( C ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( D ) == 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( D ) == 0\n" );
        exit(EXIT_FAILURE);
    }

    mps_frees( A, C, D, (mps_ptr) NULL );

    return 0;
}

int test_mps_exist3()
{
    mps_t A, B, C, D;

    mps_inits( 4, 4, 24, MPS_COL_ORDER, A, B, C, D, (mps_ptr) NULL );

    mps_frees( A, B, C, D, (mps_ptr) NULL );

    if ( mps_exist( A ) != 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( A ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( B ) != 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( B ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( C ) != 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( C ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    if ( mps_exist( D ) != 0 )
    {
        printf("test_mps_exist2() failed, mps_exist( D ) != 0\n" );
        exit(EXIT_FAILURE);
    }

    return 0;
}

int main()
{
    test_mps_exist1();
    test_mps_exist2();
    test_mps_exist3();

    return 0;
}

