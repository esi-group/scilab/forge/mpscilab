/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_equal_double() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_equal_double1( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    double B[m*n];
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        B[i-1] = i;
    }

    rnum = mps_equal_double( A, B, MPS_ROW_ORDER );

    if ( rnum != 1 )
    {
        mpfr_printf("test_mps_equal_double1() failed. m=%u , n=%u , prec=%u, order=%u\n", m, n, prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_mps_equal_double2( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A;
    double B[m*n];
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        B[i-1] = i+1;
    }

    rnum = mps_equal_double( A, B, MPS_ROW_ORDER );

    if ( rnum == 1 )
    {
        mpfr_printf("test_mps_equal_double2() failed. m=%u , n=%u , prec=%u, order=%u\n", m, n, prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_mps_equal_double3()
{
    mps_t A;
    double B[10*10];
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        B[i-1] = i;
    }

    B[2] = -1;
    xi = mps_get_ele_row( A, 3 );
    mpfr_set_si( xi, -1, GMP_RNDN );

    rnum = mps_equal_double( A, B, MPS_ROW_ORDER );

    if ( rnum != 1 )
    {
        mpfr_printf("test_mps_equal_double3 failed.\n");
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_mps_equal_double4()
{
    mps_t A;
    double B[10*10];
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_row( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        B[i-1] = i;
    }

    B[2] = -2;
    xi = mps_get_ele_row( A, 3 );
    mpfr_set_si( xi, -1, GMP_RNDN );

    rnum = mps_equal_double( A, B, MPS_ROW_ORDER );

    if ( rnum == 1 )
    {
        mpfr_printf("test_mps_equal_double4 failed.\n");
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_mps_equal_double1( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal_double1( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal_double1( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal_double1( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal_double1( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal_double1( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double1( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal_double1( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double1( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double1( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal_double1( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal_double1( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal_double1( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal_double1( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal_double1( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal_double1( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal_double1( 1, 1, 64, MPS_ROW_ORDER );

    test_mps_equal_double2( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal_double2( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal_double2( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal_double2( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal_double2( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal_double2( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double2( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal_double2( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double2( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal_double2( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal_double2( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal_double2( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal_double2( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal_double2( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal_double2( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal_double2( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal_double2( 1, 1, 64, MPS_ROW_ORDER );

    test_mps_equal_double3();
    test_mps_equal_double4();

    return 0;
}
