/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the core allocation functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

/* Allocation test. Must be called first. */
int test_alloc()
{
  mps_alloc_ptr x;

  x = mps_alloc( 10, 10, 100 );

  if ( x == NULL)
  {
    /* ..didn't go far.*/
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, 0 ) returned a NULL poiner.\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_MPFR_ARRAY( x ) == NULL )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_MPFR_ARRAY( x ) == NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ARRAY( x ) != x )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_LIMBS_ARRAY( x ) != x\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_NEXT( x ) != NULL)
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_NEXT( x ) != NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_PREV( x ) != NULL)
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_PREV( x ) != NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_ALLOC_SIZE( x ) != sizeof(mpfr_t)*100 )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_ALLOC_SIZE( x ) != sizeof(mpfr_t)*100\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ALLOC_SIZE( x ) != mpfr_custom_get_size(100)*100 + sizeof(___mps_alloc_struct) )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_LIMBS_ALLOC_SIZE( x ) != mpfr_custom_get_size(100)*100 + sizeof(___mps_alloc_struct)\n");
    exit(EXIT_FAILURE);
  }

  return 0;
}

int test_alloc_scalar()
{
  mps_alloc_ptr x;

  x = mps_alloc( 1, 1, 100 );

  if ( x == NULL)
  {
    /* ..didn't go far.*/
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, 0 ) returned a NULL poiner.\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_MPFR_ARRAY( x ) == NULL )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_MPFR_ARRAY( x ) == NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ARRAY( x ) != x )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_LIMBS_ARRAY( x ) != x\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_ALLOC_SIZE( x ) != sizeof(mpfr_t) )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_ALLOC_SIZE( x ) != sizeof(mpfr_t)*100\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ALLOC_SIZE( x ) != mpfr_custom_get_size(100) + sizeof(___mps_alloc_struct) )
  {
    printf("test_alloc() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_LIMBS_ALLOC_SIZE( x ) != mpfr_custom_get_size(100)*100 + sizeof(___mps_alloc_struct)\n");
    exit(EXIT_FAILURE);
  }

  return 0;
}

int test_init()
{
  mps_t x;
  mps_alloc_ptr mpfr_array;
  unsigned int i;

  mps_init( x, 10, 10, 100, MPS_COL_ORDER );
  mpfr_array =  MPS_MPFR_ARRAY( x );

  if ( MPS_MPFR_ARRAY(x) == NULL )
  {
    /* ..didn't go far.*/
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_MPFR_HEADER(x) == NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ARRAY(x) == NULL )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_LIMBS_HEADER(x) == NULL\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_ALLOC_SIZE(x) != sizeof(mpfr_t)*100 )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_ALLOC_SIZE(x) != sizeof(mpfr_t)*100\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_LIMBS_ALLOC_SIZE(x) != mpfr_custom_get_size(100)*100 + sizeof(___mps_alloc_struct) )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_DATA_ALLOC_SIZE( x ) != mpfr_custom_get_size(100)*100 + sizeof(___mps_alloc_struct)\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_PREC( x ) != 100 )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_PREC( x ) != 100\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_NUMROW( x ) != 10 )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_NUMROW( x ) != 10\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_NUMCOL( x ) != 10 )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_NUMCOL( x ) != 10\n");
    exit(EXIT_FAILURE);
  }

  if ( MPS_TYPE( x ) != 0 )
  {
    printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); MPS_TYPE( x ) != 0\n");
    exit(EXIT_FAILURE);
  }

  for ( i = 1; i <= 100; i++)
  {
    if ( !mpfr_zero_p( mps_get_ele_seq( x, i ) ) )
    {
      printf("test_init() failed for mps_alloc( 10, 10, 100, MPS_COL_ORDER ); non-zero entry at index %d\n", i);
      exit(EXIT_FAILURE);
    }
  }

  mps_free(x);

  return 0;
}

int main()
{
  /* test_alloc() must stay the first test. */
  test_alloc();
  test_alloc_scalar();
  test_init();

  return 0;
}

