/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the determinant related functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_det_gauss1()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss_dest( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss1() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss2()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss2_dest( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss2() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss3()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss3_dest( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss3() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss4()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss4() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss5()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss2( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss5() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss6()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 0, 0, 0,
                    0, 0, 0, 2,
                    0, 0, 2, 0,
                    0, 2, 0, 0 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss3( R, A );

    if ( mpfr_cmp_si( R, -16 ) != 0 )
    {
        printf("test_mps_det_gauss6() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss7()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 1, 1, 1,
                    1, 1, 1, 2,
                    1, 1, 2, 1,
                    1, 2, 1, 1 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss_dest( R, A );

    if ( mpfr_cmp_si( R, -5 ) != 0 )
    {
        printf("test_mps_det_gauss7() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss8()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 1, 1, 1,
                    1, 1, 1, 2,
                    1, 1, 2, 1,
                    1, 2, 1, 1 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss2_dest( R, A );

    if( mpfr_cmp_si( R, -5 ) != 0 )
    {
        printf("test_mps_det_gauss8() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss9()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    int A1[4*4] = { 2, 1, 1, 1,
                    1, 1, 1, 2,
                    1, 1, 2, 1,
                    1, 2, 1, 1 };

    mps_init( A, 4, 4, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    for ( i = 1; i <= 4*4; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss3_dest( R, A );

    if ( mpfr_cmp_si( R, -5 ) != 0 )
    {
        printf("test_mps_det_gauss9() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss10()
{
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    mps_init( A, 1, 1, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    xi = mps_get_ele_seq( A, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    mps_det_gauss_dest( R, A );

    if( mpfr_cmp_si( R, 2 ) != 0 )
    {
        printf("test_mps_det_gauss10() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss11()
{
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    mps_init( A, 1, 1, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    xi = mps_get_ele_seq( A, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    mps_det_gauss2_dest( R, A );

    if( mpfr_cmp_si( R, 2 ) != 0 )
    {
        printf("test_mps_det_gauss11() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss12()
{
    mpfr_ptr xi;
    mpfr_t R;
    mps_t A;

    mps_init( A, 1, 1, 100, MPS_COL_ORDER );
    mpfr_init2( R, 100 );

    xi = mps_get_ele_seq( A, 1 );
    mpfr_set_si( xi, 2, GMP_RNDN );

    mps_det_gauss3_dest( R, A );

    if ( mpfr_cmp_si( R, 2 ) != 0 )
    {
        printf("test_mps_det_gauss12() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss13()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mpfr_t ref;
    mps_t A;

    int A1[11*11] = {
    766, 119, 943, 65,  555, 676, 407, 584, 115, 51,  273,
    600, 500, 128, 533, 119, 970, 669, 752, 486, 414, 207,
    785, 329, 201, 33,  761, 544, 204, 51,  767, 722, 193,
    738, 480, 196, 315, 479, 20,  831, 595, 88,  77,  679,
    554, 330, 892, 378, 281, 894, 12,  383, 700, 585, 588,
    992, 630, 461, 461, 238, 349, 488, 490, 187, 370, 933,
    975, 211, 625, 628, 329, 110, 954, 527, 201, 211, 550,
    370, 448, 705, 287, 230, 202, 58,  68,  406, 190, 804,
    303, 591, 701, 329, 213, 130, 825, 884, 409, 560, 107,
    951, 680, 408, 471, 405, 857, 298, 719, 176, 942, 740,
    712, 73,  63,  335, 309, 637, 77,  69,  331, 681, 561,
    };

    mps_init( A, 11, 11, 83, MPS_COL_ORDER );
    mpfr_init2( R, 83 );
    mpfr_init2( ref, 83 );
    mpfr_set_str ( ref, "1.0011000000011011010011010101001011010101110011101101101111101000011100111001001010e101", 2, GMP_RNDN);

    for ( i = 1; i <= 11*11; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss_dest( R, A );

    if ( mpfr_cmp( R, ref ) != 0 )
    {
        printf("test_mps_det_gauss13() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss14()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mpfr_t ref;
    mps_t A;

    int A1[11*11] = {
    766, 119, 943, 65,  555, 676, 407, 584, 115, 51,  273,
    600, 500, 128, 533, 119, 970, 669, 752, 486, 414, 207,
    785, 329, 201, 33,  761, 544, 204, 51,  767, 722, 193,
    738, 480, 196, 315, 479, 20,  831, 595, 88,  77,  679,
    554, 330, 892, 378, 281, 894, 12,  383, 700, 585, 588,
    992, 630, 461, 461, 238, 349, 488, 490, 187, 370, 933,
    975, 211, 625, 628, 329, 110, 954, 527, 201, 211, 550,
    370, 448, 705, 287, 230, 202, 58,  68,  406, 190, 804,
    303, 591, 701, 329, 213, 130, 825, 884, 409, 560, 107,
    951, 680, 408, 471, 405, 857, 298, 719, 176, 942, 740,
    712, 73,  63,  335, 309, 637, 77,  69,  331, 681, 561,
    };

    mps_init( A, 11, 11, 83, MPS_COL_ORDER );
    mpfr_init2( R, 83 );
    mpfr_init2( ref, 83 );
    mpfr_set_str ( ref, "1.0011000000011011010011010101001011010101110011101101101111101000011100111001100101e101", 2, GMP_RNDN);

    for ( i = 1; i <= 11*11; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss2_dest( R, A );

    if ( mpfr_cmp( R, ref ) != 0 )
    {
        printf("test_mps_det_gauss14() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int test_mps_det_gauss15()
{
    unsigned int i;
    mpfr_ptr xi;
    mpfr_t R;
    mpfr_t ref;
    mps_t A;

    int A1[11*11] = {
    766, 119, 943, 65,  555, 676, 407, 584, 115, 51,  273,
    600, 500, 128, 533, 119, 970, 669, 752, 486, 414, 207,
    785, 329, 201, 33,  761, 544, 204, 51,  767, 722, 193,
    738, 480, 196, 315, 479, 20,  831, 595, 88,  77,  679,
    554, 330, 892, 378, 281, 894, 12,  383, 700, 585, 588,
    992, 630, 461, 461, 238, 349, 488, 490, 187, 370, 933,
    975, 211, 625, 628, 329, 110, 954, 527, 201, 211, 550,
    370, 448, 705, 287, 230, 202, 58,  68,  406, 190, 804,
    303, 591, 701, 329, 213, 130, 825, 884, 409, 560, 107,
    951, 680, 408, 471, 405, 857, 298, 719, 176, 942, 740,
    712, 73,  63,  335, 309, 637, 77,  69,  331, 681, 561,
    };

    mps_init( A, 11, 11, 83, MPS_COL_ORDER );
    mpfr_init2( R, 83 );
    mpfr_init2( ref, 83 );
    mpfr_set_str ( ref, "1.0011000000011011010011010101001011010101110011101101101111101000011100111001101011e101", 2, GMP_RNDN);

    for ( i = 1; i <= 11*11; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_si( xi, A1[i-1], GMP_RNDN );
    }

    mps_det_gauss3_dest( R, A );

    if ( mpfr_cmp( R, ref ) != 0 )
    {
        printf("test_mps_det_gauss15() failed.\n");
        exit(EXIT_FAILURE);
    }

    mpfr_clear( R );
    mps_free( A );
    return 0;
}

int main()
{
    test_mps_det_gauss1();
    test_mps_det_gauss2();
    test_mps_det_gauss3();
    test_mps_det_gauss4();
    test_mps_det_gauss5();
    test_mps_det_gauss6();
    test_mps_det_gauss7();
    test_mps_det_gauss8();
    test_mps_det_gauss9();
    test_mps_det_gauss10();
    test_mps_det_gauss11();
    test_mps_det_gauss12();
    test_mps_det_gauss13();
    test_mps_det_gauss14();
    test_mps_det_gauss15();

    return 0;
}

