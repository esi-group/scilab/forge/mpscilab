/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test block operation mps_get_ele_seq_blk(). */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_get_ele_seq_blk1()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 1, 2, 3, 4, 5, 11, 12, 13, 14, 15, 21, 22, 23, 24,
                                25, 31, 32, 33, 34, 35, 41, 42, 43, 44, 45 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 1, 5);

    for ( i = 1; i <= 5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk1() failed. Value mismatch at index %d \n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk2()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 12, 13, 14, 15, 16, 22, 23, 24, 25, 26, 32, 33, 34,
                               35, 36, 42, 43, 44, 45, 46, 52, 53, 54, 55, 56 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 2, 5, 2, 5);

    for ( i = 1; i <= 5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk2() failed. Value mismatch at index %d\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk3()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 56, 57, 58, 59, 60, 66, 67, 68, 69, 70, 76, 77, 78,
                             79, 80, 86, 87, 88, 89, 90, 96, 97, 98, 99 , 100 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 6, 5, 6, 5);

    for ( i = 1; i<=5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk3() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }


    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk4()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 5, 6, 7, 8, 9, 15, 16, 17, 18, 19, 25, 26, 27, 28,
                                29, 35, 36, 37, 38, 39, 45, 46, 47, 48, 49 };

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for( i=1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 5, 1, 5);

    for ( i = 1; i<=5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk4() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk5()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 1, 11, 21, 31, 41, 2, 12, 22, 32, 42, 3, 13, 23,
                                33, 43, 4, 14, 24, 34, 44, 5, 15, 25, 35, 45 };

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 1, 5);

    for ( i = 1; i <= 5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_col_blk5() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk6()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[5*5] = { 5, 15, 25, 35, 45, 6, 16, 26, 36, 46, 7, 17, 27,
                                37, 47, 8, 18, 28, 38, 48, 9, 19, 29, 39, 49 };

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 5, 5, 5);

    for ( i = 1; i<=5*5; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk6() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk7()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 1, 1, 1);

    xi = mps_get_ele_seq_blk( p, 1 );
    if ( mpfr_cmp_ui( xi, 1 ) != 0 )
    {
        printf("test_get_ele_seq_blk7() failed. Value mismatch at index 1.\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk8()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_COL_ORDER );

    for( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 1, 5, 1);

    xi = mps_get_ele_seq_blk( p, 1 );
    if ( mpfr_cmp_ui( xi, 45 ) != 0 )
    {
        printf("test_get_ele_seq_blk8() failed. Value mismatch at index 1.\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk9()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 5, 1, 5, 1);

    xi = mps_get_ele_seq_blk( p, 1 );
    if ( mpfr_cmp_ui( xi, 45 ) != 0 )
    {
        printf("test_get_ele_seq_blk9() failed. Value mismatch at index 1.\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk10()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 10, 1, 1, 1);

    xi = mps_get_ele_seq_blk( p, 1 );
    if ( mpfr_cmp_ui( xi, 91 ) != 0 )
    {
        printf("test_get_ele_seq_blk10() failed. Value mismatch at index 1.\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk11()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;

    mps_init( A, 10, 10, 100, MPS_ROW_ORDER );

    for ( i = 1; i <= 10*10; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 7, 2, 3, 1);

    xi = mps_get_ele_seq_blk( p, 1 );
    if ( mpfr_cmp_ui( xi, 63 ) != 0 )
    {
        printf("test_get_ele_seq_blk11() failed. Value mismatch at index 1.\n" );
        exit(EXIT_FAILURE);
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk12()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[3*4] = { 1, 2, 3, 8, 9, 10, 15, 16, 17, 22, 23, 24 };

    mps_init( A, 7, 9, 100, MPS_COL_ORDER );

    for( i=1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 4);

    for ( i = 1; i<=3*4; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk12() failed. Value mismatch at index %d.\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk13()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[3*4] = { 9, 10, 11, 16, 17, 18, 23, 24, 25, 30, 31, 32 };

    mps_init( A, 7, 9, 100, MPS_COL_ORDER );

    for( i = 1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 2, 3, 2, 4);

    for ( i = 1; i <= 3*4; i++ )
    {
        xi = mps_get_ele_seq_blk( p, i );
        if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk13() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int test_get_ele_seq_blk14()
{
    mps_t A;
    mps_blk_t p;
    unsigned int i;
    mpfr_ptr xi;
    unsigned int R1[3*4] = { 1, 10, 19, 2, 11, 20, 3, 12, 21, 4, 13, 22 };

    mps_init( A, 7, 9, 100, MPS_ROW_ORDER );

    for( i=1; i <= 7*9; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_submat(p, A, 1, 3, 1, 4);

    for (i = 1; i <= 3; i++ )
    {
        xi = mps_get_ele_col_blk( p, i );
        if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
        {
            printf("test_get_ele_seq_blk14() failed. Value mismatch at index %d.\n", i );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );

    return 0;
}

int main()
{
    test_get_ele_seq_blk1();
    test_get_ele_seq_blk2();
    test_get_ele_seq_blk3();
    test_get_ele_seq_blk4();

    /* Transposed stuff. */
    test_get_ele_seq_blk5();
    test_get_ele_seq_blk6();

    /* Single element access. */
    test_get_ele_seq_blk7();
    test_get_ele_seq_blk8();
    test_get_ele_seq_blk9();
    test_get_ele_seq_blk10();
    test_get_ele_seq_blk11();

    /* Non square stuff. */
    test_get_ele_seq_blk12();
    test_get_ele_seq_blk13();

    /* Transposed non square stuff. */
    test_get_ele_seq_blk14();

    return 0;
}

