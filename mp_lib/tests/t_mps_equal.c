/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the mps_equal() function. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_equal1( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    rnum = mps_equal( A, B );

    if ( rnum != 1 )
    {
        mpfr_printf("test_mps_equal1() failed. n=%u , m=%u , prec=%u, order=%u\n", n, m , prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_equal2( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    rnum = mps_equal_abs( A, B );

    if ( rnum != 1 )
    {
        mpfr_printf("test_mps_equal2() failed. n=%u , m=%u , prec=%u order=%u\n", n, m , prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_equal3( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_si( xi, (int) i*(-1), GMP_RNDN );
    }

    rnum = mps_equal_abs( A, B );

    if ( rnum != 1 )
    {
        mpfr_printf("test_mps_equal3() failed. n=%u , m=%u , prec=%u, order=%u\n", n, m , prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_equal4( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    xi = mps_get_ele_seq( A, 1 );
    mpfr_set_ui( xi, 2, GMP_RNDN );

    rnum = mps_equal( A, B );

    if ( rnum != 0 )
    {
        mpfr_printf("test_mps_equal4() failed. n=%u , m=%u , prec=%u, order=%u\n", n, m , prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}

int test_mps_equal5( unsigned int n, unsigned int m, mpfr_prec_t prec, mps_order_t order )
{
    mps_t A, B;
    unsigned int i;
    int rnum;

    mpfr_ptr xi;

    mps_init( A, n, m, prec, order );
    mps_init( B, n, m, prec, order );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
        xi = mps_get_ele_seq( B, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    xi = mps_get_ele_seq( A, 1 );
    mpfr_set_ui( xi, 2, GMP_RNDN );

    rnum = mps_equal_abs( A, B );

    if ( rnum != 0 )
    {
        mpfr_printf("test_mps_equal5() failed. n=%u , m=%u , prec=%u, order=%u\n", n, m , prec, order);
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    mps_free( B );

    return 0;
}


int main()
{
    test_mps_equal1( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal1( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal1( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal1( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal1( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal1( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal1( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal1( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal1( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal1( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal1( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal1( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal1( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal1( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal1( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal1( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal1( 1, 1, 64, MPS_ROW_ORDER );

    test_mps_equal2( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal2( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal2( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal2( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal2( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal2( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal2( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal2( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal2( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal2( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal2( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal2( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal2( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal2( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal2( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal2( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal2( 1, 1, 64, MPS_ROW_ORDER );
/*
    test_mps_equal3( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal3( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal3( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal3( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal3( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal3( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal3( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal3( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal3( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal3( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal3( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal3( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal3( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal3( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal3( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal3( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal3( 1, 1, 64, MPS_ROW_ORDER );
*/
    test_mps_equal4( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal4( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal4( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal4( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal4( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal4( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal4( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal4( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal4( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal4( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal4( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal4( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal4( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal4( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal4( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal4( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal4( 1, 1, 64, MPS_ROW_ORDER );

    test_mps_equal5( 10, 10, 100, MPS_COL_ORDER );
    test_mps_equal5( 11, 13, 100, MPS_COL_ORDER );
    test_mps_equal5( 10, 20, 50, MPS_COL_ORDER );
    test_mps_equal5( 20, 10, 53, MPS_COL_ORDER );
    test_mps_equal5( 1, 10, 53, MPS_COL_ORDER );
    test_mps_equal5( 10, 1, 64, MPS_COL_ORDER );
    test_mps_equal5( 1, 2, 64, MPS_COL_ORDER );
    test_mps_equal5( 2, 1, 64, MPS_COL_ORDER );
    test_mps_equal5( 1, 1, 64, MPS_COL_ORDER );
    test_mps_equal5( 10, 10, 100, MPS_ROW_ORDER );
    test_mps_equal5( 10, 20, 50, MPS_ROW_ORDER );
    test_mps_equal5( 20, 10, 53, MPS_ROW_ORDER );
    test_mps_equal5( 1, 10, 53, MPS_ROW_ORDER );
    test_mps_equal5( 10, 1, 64, MPS_ROW_ORDER );
    test_mps_equal5( 1, 2, 64, MPS_ROW_ORDER );
    test_mps_equal5( 2, 1, 64, MPS_ROW_ORDER );
    test_mps_equal5( 1, 1, 64, MPS_ROW_ORDER );


    return 0;
}

