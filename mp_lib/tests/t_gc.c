/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test Garbage Collection functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_gc()
{
  
  mps_t A, B, C, D, E;
  mpfr_ptr xi;

  if( mps_alloc_list_size() != 0 )
  {
    printf("test_gc() failed. mps_alloc_list_size() != 0\n");
    exit(EXIT_FAILURE);
  }

  mps_init( A, 10, 10, 10, 0 );
  xi = mps_get_ele( A, 1, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  if( mps_alloc_list_size() != 1 )
  {
    printf("test_gc() failed. mps_alloc_list_size() != 1\n");
    exit(EXIT_FAILURE);
  }

  if( MPS_NEXT(A) != NULL )
  {
    printf("test_gc() failed. MPS_NEXT(A) != NULL\n");
    exit(EXIT_FAILURE);
  }

  if( MPS_PREV(A) != NULL )
  {
    printf("test_gc() failed. MPS_PREV(A) != NULL\n");
    exit(EXIT_FAILURE);
  }

  mps_init( B, 1, 1, 20, 0 );
  xi = mps_get_ele( B, 1, 1 );
  mpfr_set_ui( xi, 2, GMP_RNDN );

  if( mps_alloc_list_size() != 2 )
  {
    printf("test_gc() failed. mps_alloc_list_size() != 2\n");
    exit(EXIT_FAILURE);
  }

  if( MPS_NEXT(B) != MPS_HEADER(A) )
  {
    printf("test_gc() failed. MPS_NEXT(B) != MPS_HEADER(A)\n");
    exit(EXIT_FAILURE);
  }

  if( MPS_PREV(B) != NULL )
  {
    printf("test_gc() failed. MPS_PREV(B) != NULL\n");
    exit(EXIT_FAILURE);
  }

  if( MPS_PREV(A) != MPS_HEADER(B) )
  {
    printf("test_gc() failed. MPS_PREV(A) != MPS_HEADER(B)\n");
    exit(EXIT_FAILURE);
  }

}

int main()
{
  
  test_gc();

  return 0;
}
