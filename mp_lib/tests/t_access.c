/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test the core access functions. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

/* Test basic linear access. */
int test_seq_access( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t x;
    mpfr_ptr xi;
    unsigned int i;

    mps_init( x, n, m, prec, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( x, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( x, i );
        if ( mpfr_cmp_ui( xi, i ) != 0 )
        {
            mpfr_printf("test_seq_access() failed. Value mismatch at index %u for matrix size n=%u , m=%u , prec=%u\n", i, n, m , prec);
        exit(EXIT_FAILURE);
        }
    }

    mps_free( x );

    return 0;
}

/* Test the basic access methods. */
int test_access( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t x;
    mpfr_ptr xi;
    unsigned int i, j;

    mps_init( x, m, n, prec, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( x, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    for ( j = 1; j <= n; j++ )
    {
        for ( i = 1; i <= m; i++ )
        {
            xi = mps_get_ele( x, i, j );
            if ( mpfr_cmp_ui( xi, i+(j-1)*m ) != 0 )
            {
                mpfr_printf("test_access() failed for mps_get_ele();  Value mismatch at index %u,%u for matrix n=%u , m=%u , prec=%u, order=%u\n", i, j, n, m , prec, order);
                exit(EXIT_FAILURE);
            }
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_col( x, i ), i ) != 0 )
        {
            mpfr_printf("test_access() failed for mps_get_ele_col(); Value mismatch at index %u for matrix n=%u , m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_row( x, i ), ((i-1) / n) + m*((i-1) % n) + 1 ) != 0 )
        {
            mpfr_printf("test_access() failed for mps_get_ele_row(); Value mismatch at index %u for matrix n=%u, m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( x );

    return 0;
}

/* Test transposed(fliped) access. */
int test_transposed_access( unsigned int m, unsigned int n, mpfr_prec_t prec, mps_order_t order )
{
    mps_t x;
    mpfr_ptr xi;
    unsigned int i, j;

    mps_init( x, m, n, prec, MPS_COL_ORDER );

    for ( i = 1; i <= n*m; i++ )
    {
        xi = mps_get_ele_seq( x, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    mps_flip_order( x );

    for ( j = 1; j <= n; j++ )
    {
        for ( i = 1; i <= m; i++ )
        {
            if ( mpfr_cmp_ui( mps_get_ele( x, i, j ), j+(i-1)*n ) != 0 )
            {
                mpfr_printf("test_transposed_access() failed for mps_get_ele();  Value mismatch at index %u,%u for matrix n=%u , m=%u , prec=%u, order=%u\n", i, j, n, m , prec, order);
                exit(EXIT_FAILURE);
            }
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_col( x, i ), ((i-1) / m) + n*((i-1) % m) + 1 ) != 0 )
        {
            mpfr_printf("test_transposed_access() failed for mps_get_ele_col(); Value mismatch at index %u for matrix n=%u, m=%u , prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_row( x, i ), i ) != 0 )
        {
            mpfr_printf("test_transposed_access() failed for mps_get_ele_row(); Value mismatch at index %u for matrix n=%u, m=%u, prec=%u, order=%u\n", i, n, m , prec, order);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( x );

    return 0;
}

/* Test linear access. Reverse of the first test. */
int test_access_seq_rev( unsigned int n, unsigned int m, mpfr_prec_t prec )
{
    mps_t x;
    mpfr_ptr xi;
    unsigned int i, j;

    mps_init( x, n, m, prec, MPS_COL_ORDER );

    for ( i = 1; i <= m; i++ )
    {
        for ( j = 1; j <= n; j++ )
        {
            xi = mps_get_ele( x, j, i );
            mpfr_set_ui( xi, j + n*(i-1), GMP_RNDN );
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_seq( x, i ), i ) != 0 )
        {
            mpfr_printf("test_access_seq_rev() failed for mps_get_ele_seq(); Value mismatch at index %u for matrix n=%u , m=%u , prec=%u\n", i, n, m , prec );
            exit(EXIT_FAILURE);
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if ( mpfr_cmp_ui( mps_get_ele_col( x, i ), i ) != 0 )
        {
            mpfr_printf("test_access_seq_rev() failed for mps_get_ele_col(); Value mismatch at index %u for matrix n=%u , m=%u , prec=%u\n", i, n, m , prec );
            exit(EXIT_FAILURE);
        }
    }

    for ( i = 1; i <= n*m; i++ )
    {
        if( mpfr_cmp_ui( mps_get_ele_row( x, i ), ((i-1) / m) + n*((i-1) % m) + 1 ) != 0 )
        {
            mpfr_printf("test_access_seq_rev() failed for mps_get_ele_row(); Value mismatch at index %u for matrix n=%u , m=%u , prec=%u\n", i, n, m , prec );
            exit(EXIT_FAILURE);
        }
    }

    mps_free( x );

    return 0;
}

int main()
{
    test_seq_access( 10, 10, 100 );

    test_seq_access( 10, 20, 50 );
    test_seq_access( 20, 10, 53 );
    test_seq_access( 1, 10, 53 );
    test_seq_access( 10, 1, 64 );
    test_seq_access( 1, 1, 64 );

    test_access( 10, 10, 100, MPS_COL_ORDER );
    test_access( 10, 20, 50, MPS_COL_ORDER );
    test_access( 20, 10, 53, MPS_COL_ORDER );
    test_access( 1, 10, 53, MPS_COL_ORDER );
    test_access( 10, 1, 64, MPS_COL_ORDER );
    test_access( 1, 1, 64, MPS_COL_ORDER );
    test_access( 10, 10, 100, MPS_ROW_ORDER );
    test_access( 10, 20, 50, MPS_ROW_ORDER );
    test_access( 20, 10, 53, MPS_ROW_ORDER );
    test_access( 1, 10, 53, MPS_ROW_ORDER );
    test_access( 10, 1, 64, MPS_ROW_ORDER );
    test_access( 1, 1, 64, MPS_ROW_ORDER );

    test_transposed_access( 10, 10, 100, MPS_COL_ORDER );
    test_transposed_access( 10, 20, 50, MPS_COL_ORDER );
    test_transposed_access( 20, 10, 53, MPS_COL_ORDER );
    test_transposed_access( 1, 10, 53, MPS_COL_ORDER );
    test_transposed_access( 10, 1, 64, MPS_COL_ORDER );
    test_transposed_access( 1, 1, 64, MPS_COL_ORDER );
    test_transposed_access( 10, 10, 100, MPS_ROW_ORDER );
    test_transposed_access( 10, 20, 50, MPS_ROW_ORDER );
    test_transposed_access( 20, 10, 53, MPS_ROW_ORDER );
    test_transposed_access( 1, 10, 53, MPS_ROW_ORDER );
    test_transposed_access( 10, 1, 64, MPS_ROW_ORDER );
    test_transposed_access( 1, 1, 64, MPS_ROW_ORDER );

    test_access_seq_rev( 10, 10, 100 );
    test_access_seq_rev( 10, 20, 50 );
    test_access_seq_rev( 20, 10, 53 );
    test_access_seq_rev( 1, 10, 53 );
    test_access_seq_rev( 10, 1, 64 );
    test_access_seq_rev( 1, 1, 64 );

  return 0;
}

