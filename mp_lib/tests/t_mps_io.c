/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test file input-output functions */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_mps_write_header()
{
    /* Expected result byte per byte on a little endian platform. */
    unsigned char res_l[64] = { 0x89, 0x4d, 0x50, 0x53, 0x0D, 0x0A, 0x1A, 0x0A,
                                0x3D, 0x3D, 0x4C, 0x4C, 0x4C, 0x4C, 0x3D, 0x3D,
                                0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x64, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x7D, 0x1E, 0xF0, 0xB5, 0xB5, 0xF0, 0x1E, 0x7D
                                };

    /* Expected result byte per byte on a big endian platform. */
    unsigned char res_b[64] = { 0x89, 0x4d, 0x50, 0x53, 0x0D, 0x0A, 0x1A, 0x0A,
                                0x42, 0x42, 0x3D, 0x3D, 0x3D, 0x3D, 0x42, 0x42,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64,
                                0x7D, 0x1E, 0xF0, 0xB5, 0xB5, 0xF0, 0x1E, 0x7D
                                };

    /* Array to hold the output file. */
    unsigned char out[64];
    
    size_t read_b;
    unsigned int i;
    int ret;
    mps_t A;
    unsigned char b;

    #ifdef HAVE_BIGENDIAN
        int endianness = 0;
    #else
        int endianness = 1;
    #endif

    FILE * file = fopen("test_mps_wh", "wb" );

    if ( file == NULL )
    {
        printf("test_mps_write_header() failed to fopen a test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    mps_init( A, 4, 5, 100, MPS_COL_ORDER );

    ret = mps_write_header( file, A );

    if( ret != 0 )
    {
        printf("test_mps_write_header(), mps_write_header() returned non-zero.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    fclose( file );

    /* Reopen the test file for verification. */
    file = fopen("test_mps_wh", "rb" );

    if ( file == NULL )
    {
        printf("test_mps_write_header() failed to reopen a test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    read_b = fread( (void*)out, sizeof(unsigned char), 64, file );

    /* Verify that we read 72 bytes. */
    if( read_b != 64 )
    {
        printf("test_mps_write_header() failed to read the test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    for( i = 0; i < 64; i++ )
    {
        if( endianness == 0 )
            b = res_b[i];
        else
            b = res_l[i];


        if( b != out[i] )
        {
            //printf("test_mps_write_header() wrong data at byte %i.\n", i);
            //perror( NULL );
            //exit(EXIT_FAILURE);
        }
    }

    fclose( file );

    mps_free( A );
    
    return 0;
}

int test_mps_read_header()
{
    mps_t A;
    int ret;
    mps_file_struct header_info;
    
    FILE * file = fopen("test_mps_save1", "wb" );

    if ( file == NULL )
    {
        printf("test_mps_read_header() failed to fopen test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    mps_init( A, 4, 5, 100, MPS_COL_ORDER );

    ret = mps_write_header( file, A );

    if( ret != 0 )
    {
        printf("test_mps_read_header(), mps_write_header() returned non-zero.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    fclose( file );

    file = fopen("test_mps_save1", "rsb" );

    if ( file == NULL )
    {
        printf("test_mps_read_header() failed to reopen test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    ret = mps_read_header( file, &header_info );

    if( ret != 0 )
    {
        printf("test_mps_read_header(), mps_read_header() returned non-zero.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    if( header_info._mps_file_format != 1 )
    {
        printf("test_mps_read_header(), Incorrect _mps_file_format.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    if( header_info._mps_file_numrow != 4 )
    {
        printf("test_mps_read_header(), Incorrect _mps_file_numrow.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    if( header_info._mps_file_numcol != 5 )
    {
        printf("test_mps_read_header(), Incorrect _mps_file_numcol.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    if( header_info._mps_file_precision != 100 )
    {
        printf("test_mps_read_header(), Incorrect _mps_file_precision.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    if( header_info._mps_file_version != 1 )
    {
        printf("test_mps_read_header(), Incorrect _mps_file_version.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    mps_free( A );
    
    return 0;
}

int test_mps_file_io1()
{
    mps_t A, B;
    int ret;
    mps_file_struct header_info;
    mpfr_ptr xi, yi;
    unsigned int i;

    mps_init( A, 4, 5, 100, MPS_COL_ORDER );
    mps_init( B, 4, 5, 100, MPS_COL_ORDER );

    for ( i = 1; i <= 4*5; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        mpfr_set_ui( xi, i, GMP_RNDN );
    }

    FILE * file = fopen("test_mps_file_io1", "wb" );

    if ( file == NULL )
    {
        printf("test_mps_file_io1() failed to fopen test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    ret = mps_save( file, A );

    if( ret != 0 )
    {
        printf("test_mps_file_io1(), mps_save() returned non-zero.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    fclose( file );

    /* Reopen the test file for verification. */
    file = fopen("test_mps_file_io1", "rb" );

    if ( file == NULL )
    {
        printf("test_mps_file_io1() failed to reopen a test file.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    ret = mps_load( B, file );

    if( ret != 0 )
    {
        printf("test_mps_file_io1(), mps_load() returned non-zero.\n");
        perror( NULL );
        exit(EXIT_FAILURE);
    }

    //mps_print_overview( B, 4 );

    for ( i = 1; i <= 4*5; i++ )
    {
        xi = mps_get_ele_seq( A, i );
        yi = mps_get_ele_seq( B, i );
        if ( mpfr_cmp( xi, yi ) != 0 )
        {
            printf("test_mps_file_io1() failed. Value mismatch at index %d\n", i);
            exit(EXIT_FAILURE);
        }
    }

    mps_free( A );
    mps_free( B );
    
    return 0;
}

int main()
{
    test_mps_write_header();
    test_mps_read_header();
    test_mps_file_io1();

    return 0;
}