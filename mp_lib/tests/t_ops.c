/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Test basic matrix ops. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mps_priv.h"

int test_coord_exg1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 4, 2, 3, 1, 5, 6, 7, 9, 8, 10, 11, 12, 13, 14, 15, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_coord_exg( A, 1, 3, 4, 2 );
  mps_coord_exg( A, 1, 1, 4, 1 );
  mps_coord_exg( A, 2, 2, 2, 2 );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_coord_exg1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_coord_exg2()
{
  mps_t A;
  unsigned int i;

  double R1[2*2] = { 4, 1, 3, 2 };

  mpfr_ptr xi;

  mps_init( A, 2, 2, 101, MPS_COL_ORDER );

  for ( i = 1; i <= 2*2; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_coord_exg( A, 1, 1, 2, 2 );
  mps_coord_exg( A, 1, 2, 1, 2 );
  mps_coord_exg( A, 2, 2, 2, 1 );

  for ( i = 1; i <= 2*2; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_coord_exg2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_coord_exg3()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 52, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 2, GMP_RNDN );

  mps_coord_exg( A, 1, 1, 1, 1 );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 2 ) != 0 )
  {
    printf("test_coord_exg3() failed. Value mismatch at index 1,1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_coord_exg4()
{
  mps_t A;
  unsigned int i;

  double R1[5*1] = { 2, 1, 5, 4, 3 };

  mpfr_ptr xi;

  mps_init( A, 5, 1, 205, MPS_COL_ORDER );

  for ( i = 1; i <= 5*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_coord_exg( A, 1, 1, 2, 1 );
  mps_coord_exg( A, 1, 1, 1, 1 );
  mps_coord_exg( A, 3, 1, 5, 1 );

  for( i=1; i <= 5*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_coord_exg4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_coord_exg5()
{
  mps_t A;
  unsigned int i;

  double R1[5*1] = { 2, 1, 5, 4, 3 };

  mpfr_ptr xi;

  mps_init( A, 1, 5, 205, MPS_COL_ORDER );

  for ( i = 1; i <= 5*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_coord_exg( A, 1, 1, 1, 2 );
  mps_coord_exg( A, 1, 1, 1, 1 );
  mps_coord_exg( A, 1, 3, 1, 5 );

  for ( i = 1; i <= 5*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_coord_exg5() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_indx_exg1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 16, 4, 3, 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 2 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_indx_exg( A, 4, 2 );
  mps_indx_exg( A, 4, 1 );
  mps_indx_exg( A, 1, 16 );
  mps_indx_exg( A, 2, 2 );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_indx_exg1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_indx_exg2()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 52, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 2, GMP_RNDN );

  mps_indx_exg( A, 1, 1 );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 2 ) != 0 )
  {
    printf("test_indx_exg2() failed. Value mismatch at index 1,1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_row_exg1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 15, 16, 13, 14 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_exg( A, 1, 3 );
  mps_row_exg( A, 1, 1 );
  mps_row_exg( A, 2, 4 );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_exg1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_exg2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 3, 1, 2, 6, 4, 5, 9, 7, 8, 12, 10, 11, 15, 13, 14 };

  mpfr_ptr xi;

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_exg( A, 1, 3 );
  mps_row_exg( A, 1, 1 );
  mps_row_exg( A, 3, 2 );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_exg2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_exg3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

  mpfr_ptr xi;

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_exg( A, 1, 1 );
  mps_row_exg( A, 1, 1 );
  mps_row_exg( A, 1, 1 );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_exg3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_exg4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 3, 2, 1, 4, 5, 7, 6, 8, 9, 10 };

  mpfr_ptr xi;

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_exg( A, 1, 1 );
  mps_row_exg( A, 1, 3 );
  mps_row_exg( A, 6, 7 );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_exg4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_exg5()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 52, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 2, GMP_RNDN );

  mps_row_exg( A, 1, 1 );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 2 ) != 0 )
  {
    printf("test_row_exg5() failed. Value mismatch at index 1,1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_row_scale_mpfr1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 4, 2, 6, 4, 20, 6, 14, 8, 36, 10, 22, 12, 52, 14, 30, 16 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i=1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 3, S, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_mpfr1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_scale_mpfr2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 4, 2, 6, 16, 5, 12, 28, 8, 18, 40, 11, 24, 52, 14, 30 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 3, S, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_mpfr2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_scale_mpfr3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_mpfr3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_scale_mpfr4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 4, 2, 6, 4, 5, 6, 14, 8, 9, 10 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 3, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 7, S, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_mpfr4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_scale_mpfr5()
{
  mps_t A;

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 4 ) != 0 )
  {
    printf("test_row_scale_mpfr5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_scale_double1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 4, 2, 6, 4, 20, 6, 14, 8, 36, 10, 22, 12, 52, 14, 30, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 3, 2.0, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_double1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_scale_double2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 4, 2, 6, 16, 5, 12, 28, 8, 18, 40, 11, 24, 52, 14, 30 };

  mpfr_ptr xi;

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 3, 2.0, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_double2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_scale_double3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };

  mpfr_ptr xi;

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_double3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_scale_double4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 4, 2, 6, 4, 5, 6, 14, 8, 9, 10 };

  mpfr_ptr xi;

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 3, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 7, 2.0, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_scale_double4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_scale_double5()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 4 ) != 0 )
  {
    printf("test_row_scale_double5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_row_div_mpfr1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_row_scale_mpfr( A, 3, S, GMP_RNDN );

  mps_row_div_mpfr( A, 1, S, GMP_RNDN );
  mps_row_div_mpfr( A, 1, S, GMP_RNDN );
  mps_row_div_mpfr( A, 3, S, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_div_mpfr1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_div_double1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i=1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_row_scale_double( A, 3, 2.0, GMP_RNDN );
  mps_row_div_double( A, 1, 2.0, GMP_RNDN );
  mps_row_div_double( A, 1, 2.0, GMP_RNDN );
  mps_row_div_double( A, 3, 2.0, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_div_double1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_add_mpfr1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 4, 9, 4, 5, 16, 21, 8, 9, 28, 33, 12, 13, 40, 45, 16 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_mpfr( A, 2, 1, S, GMP_RNDN );
  mps_row_add_mpfr( A, 3, 3, S, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_mpfr1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_add_mpfr2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 1, 4, 9, 4, 13, 18, 7, 22, 27, 10, 31, 36, 13, 40, 45 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_mpfr( A, 2, 1, S, GMP_RNDN );
  mps_row_add_mpfr( A, 3, 3, S, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_mpfr2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_add_mpfr3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_mpfr( A, 1, 1, S, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_mpfr3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_add_mpfr4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 3, 2, 11, 4, 5, 6, 7, 8, 9, 24 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_mpfr( A, 1, 1, S, GMP_RNDN );
  mps_row_add_mpfr( A, 3, 4, S, GMP_RNDN );
  mps_row_add_mpfr( A, 10, 7, S, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_mpfr4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_add_mpfr5()
{
  mps_t A;

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_row_add_mpfr( A, 1, 1, S, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 3 ) != 0 )
  {
    printf("test_row_add_mpfr5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_add_double1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 4, 9, 4, 5, 16, 21, 8, 9, 28, 33, 12, 13, 40, 45, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_double( A, 2, 1, 2, GMP_RNDN );
  mps_row_add_double( A, 3, 3, 2, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_double1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_add_double2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 1, 4, 9, 4, 13, 18, 7, 22, 27, 10, 31, 36, 13, 40, 45 };

  mpfr_ptr xi;

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_double( A, 2, 1, 2, GMP_RNDN );
  mps_row_add_double( A, 3, 3, 2, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_double2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_add_double3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 };

  mpfr_ptr xi;

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_double( A, 1, 1, 2, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_double3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_add_double4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 3, 2, 11, 4, 5, 6, 7, 8, 9, 24 };

  mpfr_ptr xi;

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_double( A, 1, 1, 2.0, GMP_RNDN );
  mps_row_add_double( A, 3, 4, 2.0, GMP_RNDN );
  mps_row_add_double( A, 10, 7, 2.0, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_add_double4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_row_add_double5()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_row_add_double( A, 1, 1, 2, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 3 ) != 0 )
  {
    printf("test_row_add_double5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_row_sub_mpfr1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_mpfr( A, 2, 1, S, GMP_RNDN );
  mps_row_sub_mpfr( A, 2, 1, S, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_sub_mpfr1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_row_sub_double1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_row_add_double( A, 2, 1, 2, GMP_RNDN );
  mps_row_sub_double( A, 2, 1, 2, GMP_RNDN );

  for( i=1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_row_sub_double1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_exg1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 9, 10, 11, 12, 5, 6, 7, 8, 1, 2, 3, 4, 13, 14, 15, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_exg( A, 1, 3 );
  mps_col_exg( A, 2, 2 );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_exg1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_exg2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 7, 8, 9, 4, 5, 6, 1, 2, 3, 10, 11, 12, 13, 14, 15 };

  mpfr_ptr xi;

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_exg( A, 1, 3 );
  mps_col_exg( A, 2, 2 );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_exg2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_exg3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 3, 2, 1, 4, 5, 6, 7, 8, 10, 9 };

  mpfr_ptr xi;

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_exg( A, 1, 3 );
  mps_col_exg( A, 10, 9 );
  mps_col_exg( A, 2, 2 );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_exg3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_exg4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

  mpfr_ptr xi;

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_exg( A, 1, 1 );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_exg4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_exg5()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_col_exg( A, 1, 1 );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 1 ) != 0 )
  {
    printf("test_col_exg4() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int test_col_scale_mpfr1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 4, 8, 12, 16, 5, 6, 7, 8, 18, 20, 22, 24, 13, 14, 15, 16 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 3, S, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_mpfr1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_col_scale_mpfr2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 4, 8, 12, 4, 5, 6, 14, 16, 18, 10, 11, 12, 13, 14, 15 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 3, S, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_mpfr2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_col_scale_mpfr3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 2, 2, 12, 4, 5, 6, 7, 8, 9, 20 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 3, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 3, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 10, S, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_mpfr3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_col_scale_mpfr4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_mpfr4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_col_scale_mpfr5()
{
  mps_t A;

  mpfr_ptr xi;
  mpfr_t S;
  mpfr_init2( S, 100 );
  mpfr_set_ui( S, 2, GMP_RNDN );

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );
  mps_col_scale_mpfr( A, 1, S, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 4 ) != 0 )
  {
    printf("test_col_scale_mpfr5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  mpfr_clear( S );
  return 0;
}

int test_col_scale_double1()
{
  mps_t A;
  unsigned int i;

  double R1[4*4] = { 4, 8, 12, 16, 5, 6, 7, 8, 18, 20, 22, 24, 13, 14, 15, 16 };

  mpfr_ptr xi;

  mps_init( A, 4, 4, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_double( A, 1, 2, GMP_RNDN );
  mps_col_scale_double( A, 1, 2, GMP_RNDN );
  mps_col_scale_double( A, 3, 2, GMP_RNDN );

  for ( i = 1; i <= 4*4; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_double1() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_scale_double2()
{
  mps_t A;
  unsigned int i;

  double R1[3*5] = { 4, 8, 12, 4, 5, 6, 14, 16, 18, 10, 11, 12, 13, 14, 15 };

  mpfr_ptr xi;

  mps_init( A, 3, 5, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_double( A, 1, 2, GMP_RNDN );
  mps_col_scale_double( A, 1, 2, GMP_RNDN );
  mps_col_scale_double( A, 3, 2, GMP_RNDN );

  for ( i = 1; i <= 3*5; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_double2() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_scale_double3()
{
  mps_t A;
  unsigned int i;

  double R1[1*10] = { 2, 2, 12, 4, 5, 6, 7, 8, 9, 20 };

  mpfr_ptr xi;

  mps_init( A, 1, 10, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_double( A, 1, 2, GMP_RNDN );
  mps_col_scale_double( A, 3, 2, GMP_RNDN );
  mps_col_scale_double( A, 3, 2, GMP_RNDN );
  mps_col_scale_double( A, 10, 2, GMP_RNDN );

  for ( i = 1; i <= 1*10; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_double3() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_scale_double4()
{
  mps_t A;
  unsigned int i;

  double R1[10*1] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };

  mpfr_ptr xi;

  mps_init( A, 10, 1, 100, MPS_COL_ORDER );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    mpfr_set_ui( xi, i, GMP_RNDN );
  }

  mps_col_scale_double( A, 1, 2, GMP_RNDN );

  for ( i = 1; i <= 10*1; i++ )
  {
    xi = mps_get_ele_seq( A, i );
    if ( mpfr_cmp_ui( xi, R1[i-1] ) != 0 )
    {
      printf("test_col_scale_double4() failed. Value mismatch at index %d\n", i );
      exit(EXIT_FAILURE);
    }
  }

  mps_free( A );
  return 0;
}

int test_col_scale_double5()
{
  mps_t A;

  mpfr_ptr xi;

  mps_init( A, 1, 1, 100, MPS_COL_ORDER );

  xi = mps_get_ele_seq( A, 1 );
  mpfr_set_ui( xi, 1, GMP_RNDN );

  mps_col_scale_double( A, 1, 2.0, GMP_RNDN );
  mps_col_scale_double( A, 1, 2.0, GMP_RNDN );

  xi = mps_get_ele_seq( A, 1 );
  if ( mpfr_cmp_ui( xi, 4 ) != 0 )
  {
    printf("test_col_scale_double5() failed. Value mismatch at index 1\n" );
    exit(EXIT_FAILURE);
  }

  mps_free( A );
  return 0;
}

int main()
{
  test_coord_exg1();
  test_coord_exg2();
  test_coord_exg3();
  test_coord_exg4();
  test_coord_exg5();

  test_indx_exg1();
  test_indx_exg2();

  test_row_exg1();
  test_row_exg2();
  test_row_exg3();
  test_row_exg4();
  test_row_exg5();

  test_row_scale_mpfr1();
  test_row_scale_mpfr2();
  test_row_scale_mpfr3();
  test_row_scale_mpfr4();
  test_row_scale_mpfr5();

  test_row_scale_double1();
  test_row_scale_double2();
  test_row_scale_double3();
  test_row_scale_double4();
  test_row_scale_double5();

  test_row_div_mpfr1();

  test_row_div_double1();

  test_row_add_mpfr1();
  test_row_add_mpfr2();
  test_row_add_mpfr3();
  test_row_add_mpfr4();
  test_row_add_mpfr5();

  test_row_add_double1();
  test_row_add_double2();
  test_row_add_double3();
  test_row_add_double4();
  test_row_add_double5();

  test_row_sub_mpfr1();
  test_row_sub_double1();

  test_col_exg1();
  test_col_exg2();
  test_col_exg3();
  test_col_exg4();
  test_col_exg5();

  test_col_scale_mpfr1();
  test_col_scale_mpfr2();
  test_col_scale_mpfr3();
  test_col_scale_mpfr4();
  test_col_scale_mpfr5();

  test_col_scale_double1();
  test_col_scale_double2();
  test_col_scale_double3();
  test_col_scale_double4();
  test_col_scale_double5();

  return 0;
}

