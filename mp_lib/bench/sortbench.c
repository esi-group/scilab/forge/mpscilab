/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.    The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "mps.h"

#define BUFSIZE 4096
#define VERIFY 0

typedef enum
{
	ALG_NONE = 0,
	ALG_HEAP = 1,
	ALG_QUICK = 2
} AlgChoice;

static AlgChoice alg = ALG_NONE;

/* output the results as tsv */
void run_bench(int fd, unsigned int rows, unsigned int cols, mpfr_prec_t prec)
{
	mps_t A;
	clock_t start, end;
	double sec;
	int len;
	char buf[BUFSIZE];
#if VERIFY
	int rc;
	mps_t B;
#endif

	buf[0] = '\0';

	printf("Sorting %u x %u matrix of random numbers\n", rows, cols);

	/* create a matrix and fill it with random numbers */
	mps_init(A, rows, cols, prec, 0);
	mps_random(A, 1, 1000, GMP_RNDN);

#if VERIFY
	mps_init(B, rows, cols, prec, 0);
	mps_copy(B, A);
#endif

	/* time how long it takes to sort it */
	if (alg == ALG_QUICK)
	{
		start = clock();
		mps_quicksort_col(A, GMP_RNDN);
		end = clock();
	}
	else if ( alg == ALG_HEAP)
	{
		start = clock();
		mps_heapsort_col(A);
		end = clock();
	}
	else
	{
		fprintf(stderr, "Something is broken with the demo\n");
		exit(EXIT_FAILURE);
	}

#if VERIFY
	if ( (rc = mps_check_sort(A, B)) )
	{
		fprintf(stderr, "Sorting %u x %u matrix FAILED: %d\n", rows, cols, rc);
		exit(EXIT_FAILURE);
	}
	else
	{
		printf("\tSorted %u x %u matrix correctly\n", rows, cols);
	}
#endif

	sec = (double)(end - start)/(double) CLOCKS_PER_SEC;

	sprintf(buf, "%u\t%u\t%u\t%20.20g\n", rows, cols, rows*cols, sec);
	len = strlen(buf);

	if ( -1 == write(fd, buf, len) )
	{
		fprintf(stderr, "Error writing output file\n");
		exit(EXIT_FAILURE);
	}

	mps_free(A);

}

int main(int argc, char** argv)
{
	int fd;
	unsigned int i;
	char* fname;
	char desc[256];
	char algname[10];
	char header[] = "rows, cols, size, time\n";
	unsigned int min, max, scale, step;

	min = max = scale = step = 0;

    printf("Sorting algorithm benchmark <sortbench.c>\n\n");

	sprintf(desc, "Benchmark of %s sort from %d to %d, step %d, scale %d\n", "heap", min, max, step, scale);

	/* output filename is 1st argument */
	if (argc == 7)
	{
		fname = argv[1];
		alg = atoi(argv[2]);
		min = atoi(argv[3]);
		max = atoi(argv[4]);
		step = atoi(argv[5]);
		scale = atoi(argv[6]);
	}
	else
	{
		printf("Usage: %s <output file> alg min max step scale\n", argv[0]);
		return 0;
	}

	switch (alg)
	{
		case ALG_HEAP:
			strncpy(algname,"heap", 10);
			break;
		case ALG_QUICK:
			strncpy(algname,"quick", 10);
			break;
		case ALG_NONE:
		default:
			fprintf(stderr, "invalid option\n");
			return 1;
	}

	sprintf(desc, "Benchmark of %s sort from %d to %d, step %d, scale %d\n", algname, min, max, step, scale);

	fd = open(fname, O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR|S_IWUSR|S_IRGRP|S_IROTH);
	if (fd == -1)
	{
		perror("open");
		exit(EXIT_FAILURE);
	}

	write(fd, desc, strlen(desc));
	write(fd, header, strlen(header));

	if (scale == 1)
	{
		for ( i = min; i <= max; i += step)
			run_bench(fd, i, i, 64);
	}
	else
	{
		for ( i = min; i <= max; i *= scale)
			run_bench(fd, i, i, 64);
	}

	close(fd);

    return 0;
}

