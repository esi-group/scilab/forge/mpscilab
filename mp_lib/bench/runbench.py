#!/usr/bin/env python

import os
import re
import sys
import codecs

def readFileContents(fn):
    fileHandle = codecs.open(fn, encoding='utf-8')
    fileContents = fileHandle.read()
    fileHandle.close()
    return fileContents

def Test1(threads):
    for i in range(1,26):
        os.system("./mpsbench1 -f test.txt -F mps_mul -p 100000 -P 100010 -m %(a)d -n %(a)d -t %(t)d" % {"a":i, "t":threads})
        print ("%(a)s," % {"a":i}) + ",".join(readFileContents("test.txt").split("\n"))

#uses just 1 long column thing for better control over size of matrix
#also varies the precision. The benchmark also loops over different precisions...
def Test2(filename, funcToTest, threads):
    precs = [52,64,100,128,150,175,200,225,256,512,1000,2000,5000,7500,9001,10000]
    for t in threads:
        print "threads = " + str(t)
        for p in precs:
            print "\tprec = " + str(p)
            os.system("./mpsbench1 -f %(fname)s -F %(func)s -t %(t)d -p %(prec)s -a %(avgs)d -s %(sizestart)d -e %(sizeend)d -i %(step)d -M %(scale)d"
                      % {"fname": filename, "prec":p, "t":t, "avgs":5,
                         "sizestart":1, "sizeend":250, "step":1, "scale":0,"func":funcToTest})


if len(sys.argv) > 1:
    fname = sys.argv[1]
else:
    fname = "test.txt"

if len(sys.argv) > 2:
    func = sys.argv[2]
else:
    print "Must specify function"
    exit()


Test2(fname, func, [1,2,3,4,5,8])

