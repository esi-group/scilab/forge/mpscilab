/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <popt.h>

#include "mps_priv.h"

int membench( int size, unsigned int replication )
{
    void *ptr1, *ptr2;
    double * res;
    unsigned int i;
    double f_time, avg, bw;

    #ifdef HAVE_GETHRTIME
        hrtime_t hr_ts, hr_te, time;
    #elif HAVE_MACH_TIME
        uint64_t start, end, elapsed;
        Nanoseconds elapsedNano;
    #else
        struct timeval start;
        struct timeval stop;
        /* struct timezone tz; */
        suseconds_t time;
    #endif

    res = malloc( sizeof(double)*replication );

    for( i = 0; i < replication; i++ )
    {
        ptr1 = calloc(size, 1);
        ptr2 = calloc(size, 1);

        #ifdef HAVE_GETHRTIME
            hr_ts = gethrtime();
        #elif HAVE_MACH_TIME
            start = mach_absolute_time();
        #else
            /* gettimeofday( &start, &tz ); */
            gettimeofday( &start, NULL );
        #endif

        memcpy( ptr1, ptr2, size );

        #ifdef HAVE_GETHRTIME
            hr_te = gethrtime();
            time = hr_te - hr_ts;
            f_time = (double)time / 1000000000;
            res[i] = f_time;
        #elif HAVE_MACH_TIME
            end = mach_absolute_time();
            elapsed = end - start;
            elapsedNano = AbsoluteToNanoseconds( *(AbsoluteTime *) &elapsed );
        #else
            /*gettimeofday( &stop, &tz );*/
            gettimeofday( &stop, NULL );

            if (start.tv_usec > stop.tv_usec)
            {
                stop.tv_usec += 1000000;
                stop.tv_sec--;
            }

            time = stop.tv_usec - start.tv_usec;
            time += (stop.tv_sec - start.tv_sec)*1000000;
            f_time = (double)time / 1000000;
            res[i] = f_time;
        #endif

        free(ptr1);
        free(ptr2);
    }

    avg = 0;
    for( i = 0; i < replication; i++ )
    {
        avg = avg + res[i];
    }

    avg = avg / replication;

    bw = size / avg;
    bw = bw / (double)1048576;

    printf("---Results---\n");
    printf("Size : %.2f MiB\n", (double)size / (double)1048576 );
    printf("Replications : %d\n", replication );
    printf("Average time : %.2f ms\n", avg );
    printf("Bandwidth : %.2f MiB/s\n", bw);
    
    return 0;
}

int main( int argc, char *argv[] )
{
    poptContext context;
    static int size, replication;
    int o;

    static const struct poptOption options[] =
    {
        { "size", 's', POPT_ARG_INT, &size, 's', "Memory segment size", NULL },
        { "replications", 'r', POPT_ARG_INT, &replication, 'r', "Number of replications", NULL },
        POPT_AUTOHELP
        { NULL, 0, 0, NULL, 0, NULL, NULL }
    };

    context = poptGetContext("memcpybench", argc, (const char**) argv, options, 0);

    while ( ( o = poptGetNextOpt(context)) >= 0 );

    if ( o < -1 )
    {
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    if ( size <= 0 )
    {
        fprintf( stdout, "Invalid memory segment size.\n" );
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    if ( replication <= 0 )
    {
        fprintf( stdout, "Invalid number of replications.\n" );
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    poptFreeContext(context);

    membench( size, replication );

    return 0;
}

