module Main where

import Data.List (transpose, sortBy, find)
import System.Environment (getArgs)
import System.Console.GetOpt
import Data.Maybe (fromMaybe)

import qualified Graphics.Rendering.Cairo as C
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Simple
import Graphics.Rendering.Chart.Gtk
import Graphics.Rendering.Chart.Grid
import Graphics.Rendering.Chart.Types
import Data.Accessor
import Data.Accessor.Tuple
import Data.Colour
import Data.Colour.Names
import Data.Colour.SRGB

type Precision = Int

data Content =
    Content
    {
      sizeInfo :: (Int, Int, Int, Int),
      prec :: Precision,
      threads :: Int,
      dat::[[Double]],
      avgs::[Double]
    }
    deriving (Show, Eq)

--from a (semi nonstandard??? Data.List.Split) module I don't feel like installing.
--better and uses break which I didn't know about
splitOn :: (a -> Bool) -> [a] -> [[a]]
splitOn _ [] = []
splitOn f l@(x:xs)
  | f x = splitOn f xs
  | otherwise = let (h,t) = break f l in h:(splitOn f t)


getAvgs::[[Double]] -> [Double]
getAvgs ns = map (\a -> (sum a) / fromIntegral (length a) :: Double) ns

--Get all at a number of threads
getThreads :: Int -> [Content] -> [Content]
getThreads t cs = filter (\a -> (threads a) == t) cs

--Get all with a given precision
getPrec :: Precision -> [Content] -> [Content]
getPrec p cs = filter (\a -> (prec a) == p) cs

--get specific set by threads and precision
getItem::Int -> Precision -> [Content] -> Content
getItem t p = (head.(getPrec p).(getThreads t))


--I don't think I need these
sortByPrec = sortBy (\a b-> compare (prec a) (prec b))
sortByThread = sortBy (\a b-> compare (threads a) (threads b))


getAvgRatios::Content -> Content -> [Double]
getAvgRatios a b = zipWith (/) (avgs a) (avgs b)


--break up on the blank lines, ignoring the first header
--transpose to get runs of the same size in the same list
processData:: String -> [Content]
processData input = let d = splitOn (== "") ((tail.lines) input)
                        f (x:xs) = Content ha hb hc dat (getAvgs dat)
                            where (ha, hb, hc) = getHeader x
                                  dat = (transpose (map getNums xs))
                        getNums ns = map (\c -> read c::Double) (words ns)
                    in map f d

--This could most certainly be done more intelligently
getHeader::String -> ((Int, Int, Int, Int), Int, Int)
getHeader x = let ws = words x
                  afterEq item = (tail.snd) (break (== '=') item)
                  -- p is for processed
                  f (a:b:c:d:_:e:f:[]) = ((pa, pb, pc, pd), pe, pf)
                      where pa = (read.afterEq) a :: Int
                            pb = read b:: Int
                            pc = read c:: Int
                            pd = read d:: Int
                            pe = (read.afterEq) e :: Int
                            pf = (read.afterEq) f :: Int
                  f _ = undefined
              in f ws

--convenient stuff mostly for playing in ghci. Could probably find a better way
file1 = readFile "/home/matt/jayne-openmp.txt"
file2 = readFile "/home/matt/jayne-noopenmp.txt"
file3 = readFile "/home/matt/src/mpscilab/project/mp_lib/bench/jayne-openmp-sin.txt"
file4 = readFile "/home/matt/src/mpscilab/project/mp_lib/bench/jayne-noopenmp-sin.txt"
file5 = readFile "/home/matt/src/mpscilab/project/mp_lib/bench/jayne-openmp-mps_sin_helper.txt"
file6 = readFile "/home/matt/src/mpscilab/project/mp_lib/bench/jayne-noopenmp-mps_sin_helper.txt"

quickRatios::String -> String -> Int -> Precision -> [Double]
quickRatios a b t p = let f = (getItem t p).processData
                      in getAvgRatios (f a) (f b)

quickPlot a b t p = let f = avgRatioChart (processData a) (processData b) t [p]
                    in renderableToWindow f 1024 768

fullPrecList = [52,64,100,128,150,175,200,225,256,512,1000,2000,5000,7500,9001,10000]

--take the 2 sets of data, and the number of threads at what precision
--to average and plot the ratio
avgRatioChart:: [Content] -> [Content] -> Int -> [Precision] -> Renderable ()
avgRatioChart dat1 dat2 t precList = toRenderable layout
    where
      titlestr = "Average Ratios for " ++ (show t) ++ " threads"

      --this way is stupid. figure out how to do it properly
      xaxis a = let (astart, aend, astep, amul) = sizeInfo a
                    aa = fromIntegral astart
                    ab = fromIntegral astep
                    ac = fromIntegral aend
                in [aa, aa + ab..ac]

      getPts :: Content -> Content -> [(Double,Double)]
      getPts a b = zip (xaxis a) (getAvgRatios a b)

      somecolor = opaque $ sRGB 0.3 0.3 1
      --things for some reason get unhappy if you try more than 10
      colors :: [AlphaColour Double]
      colors = somecolor : map opaque [ red, blue, green, orange, violet, darksalmon,
                                         paleturquoise, navy, lavender, springgreen,
                                         lightskyblue]

      --the 1st point is nearly always trouble. Some others. Silly filter
      --I kind of blame gettimeofday
      hackyRemoveGiantPoints::[(Double, Double)] -> [(Double, Double)]
      hackyRemoveGiantPoints = filter (\a -> snd a < 20)

      getAllPts::[[(Double,Double)]]
      getAllPts = map (\p -> hackyRemoveGiantPoints $ getPts
                             (getItem t p dat1)
                             (getItem t p dat2) ) precList

{-
  red1 = opaque $ sRGB 0.5 0.5 1

  fwhite = solidFillStyle $ opaque white
  fparchment = solidFillStyle $ opaque $ sRGB 1.0 0.99 0.90
-}
      --graph1::AlphaColour Double -> [(Double,Double)] -> CairoPointStyle
      graph1 color pts = plot_points_style ^= filledCircles 1.5 color
                         $ plot_points_values ^= pts
                        -- $ plot_all_points ^= Plot [0..]  [0..10]
                         $ plot_points_title ^= "avg ratios"
                         $ defaultPlotPoints

      allPlots = map (\(a, b) -> Left (toPlot (graph1 a b))) (zip colors getAllPts)


      layout = layout1_title ^= titlestr ++ " for precisions (bad points filtered)"
               ++ show precList
               $ layout1_plots ^= allPlots
               $ defaultLayout1

{-
      layout = layout1_plots ^= [Left (toPlot graph1)]
              $ layout1_background ^= fparchment
              $ layout1_plot_background ^= Just fwhite
              $ defaultLayout1
 -}


main = do
  args <- getArgs
  inp1 <- (readFile.head) args
  inp2 <- (readFile.head.tail) args
  tc <- readIO (args !! 2) :: IO Int
--  p <- readIO (args !! 3) :: IO Precision

  let dat1 = processData inp1
  let dat2 = processData inp2
  let plot = avgRatioChart dat1 dat2 tc [52,64,128,150,256,1000]


  renderableToWindow plot 1024 768
  renderableToPNGFile plot 1024 768 "out.png"

--  putStrLn (show (processData input))

{-
options :: [OptDescr Flag]
options = [ Option ['t'] ["threads"] (RecArg Int) "thread number to compare",
            Option ['p'] ["precision"] (RecArg Precision) "precision to compare",
            Option ['i'] ["data1"] (RecArg getArgFile "FILE") "first data to compare",
            Option ['o'] ["data2"] (RecArg getArgFile "FILE") "second data to compare" ]
-}

{-
-- Parse options, getting a list of option actions
  let (actions, nonOptions, errors) = getOpt RequireOrder options args

-- Here we thread startOptions through all supplied option actions
  opts <- foldl (>>=) (return startOptions) actions

  let Options { optInput1 = dat1,
                optInput2 = dat2,
                optThread = t,
                optPrec = p} = opts

  input >>= output
-}

{-

--I don't understand how to make these work
data Options = Options  { optVerbose     :: Bool,
                          optInput1      :: IO String,
                          optInput2      :: IO String,
                          optInputPrec   :: IO String,
                          optInputThread :: IO String,
                          optOutput      :: String -> IO ()
                        }


startOptions :: Options
startOptions = Options  { optVerbose    = False,
                          optInput      = getContents,
                          optOutput     = putStr
                        }

options :: [ OptDescr (Options -> IO Options) ]
options =
    [ Option "i" ["input1"]
        (ReqArg (\arg opt -> return opt { optInput = readFile arg }) "FILE") "Input file",

      Option "o" ["input2"]
        (ReqArg (\arg opt -> return opt { optInput = writeFile arg }) "FILE") "Output file",

      Option "t" ["threads"]
        (ReqArg (\arg opt -> return opt { optInput = return arg }) "threads") "thread #",

      Option "p" ["precision"]
        (ReqArg (\arg opt -> return opt { optInput = return arg }) "Prec") "precision"

{-
      Option "h" ["help"] (NoArg
        (\_ -> do
           prg <- getProgName
           hPutStrLn stderr (usageInfo prg options) exitWith ExitSuccess)) "Show help"
-}
    ]
-}

