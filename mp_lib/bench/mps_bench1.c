/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Very crude benchmarking program. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <popt.h>

#include "mps_priv.h"

#define PTHREAD_COMPARE 0

#if PTHREAD_COMPARE

#include <pthread.h>

typedef struct _TArgs        /* pthreads only gives you 1 argument, but we need more */
{
    mps_ptr rop;            /* struct shared between both map versions, get some extra stuff */
    mps_ptr op1;
    mps_ptr op2;
    unsigned int start;
    unsigned int end;
    MPFunc func;
    MPFunc2 func2;
    mpfr_rnd_t rnd;
} TArgs;

/* for threaded map with 1 op */
static void* tmap_worker1(void* _args)
{
    unsigned int i;
    mpfr_ptr subrop, subop;
    TArgs* args = (TArgs*) _args;

    for ( i = args->start; i <= args->end; ++i )
    {
        subrop = mps_get_ele_col( args->rop, i );
        subop = mps_get_ele_col( args->op1, i );
        args->func(subrop, subop, args->rnd);
    }

    return NULL;
}

/* for threaded map with 2 ops */
static void* tmap_worker2(void* _args)
{
    unsigned int i;
    mpfr_ptr subrop, subop1, subop2;
    TArgs* args = (TArgs*) _args;

    for ( i = args->start; i <= args->end; ++i )
    {
        subrop = mps_get_ele_col( args->rop, i );
        subop1 = mps_get_ele_col( args->op1, i );
        subop2 = mps_get_ele_col( args->op2, i );
        args->func2(subrop, subop1, subop2, args->rnd);
    }

    return NULL;
}

int mps_threaded_map( mps_ptr rop, mps_ptr op, pthread_attr_t* pta, unsigned int numthreads, MPFunc func, mpfr_rnd_t rnd )
{
    pthread_t* tids;
    TArgs* args;
    unsigned int i, j, step, matsize = MPS_SIZE(op);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op),
        "Result operand differ in dimension in mps_threaded_map()\n" );

    /* TODO: deal with this issue instead of failing on it*/
    MPS_ASSERT_MSG( numthreads <= matsize, "Trying to use too many threads in mps_threaded_map()\n");

    step = matsize / numthreads;

    if (matsize % numthreads > step)    /* helps a bit with odd distributions */
        ++step;

    tids = malloc(sizeof(pthread_t) * numthreads);
    args = malloc(sizeof(TArgs) * numthreads);

    for ( i = 0, j = 1; i < numthreads; ++i)
    {
        args[i].rop = rop;
        args[i].op1 = op;
        args[i].func = func;
        args[i].start = j;
        args[i].rnd = rnd;
        if (i < numthreads - 1)        /* not the last 1 */
            args[i].end = j += step;
        else                        /* give whatever's left to the last thread */
            args[i].end = matsize;

        pthread_create(&tids[i], pta, tmap_worker1, &args[i]);
    }

    for ( i = 0; i < numthreads; ++i)    /* wait for threads to finish */
        pthread_join(tids[0], NULL);

    free(args);
    free(tids);

    return 0;
}

int mps_threaded_map2( mps_ptr rop, mps_ptr op1, mps_ptr op2, pthread_attr_t* pta, unsigned int numthreads, MPFunc2 func, mpfr_rnd_t rnd )
{
    pthread_t* tids;
    TArgs* args;
    unsigned int i, j, step, matsize = MPS_SIZE(op1);

    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op1),
        "Result operand differ in dimension in mps_threaded_map2()\n" );
    MPS_ASSERT_MSG( MPS_SAME_SIZE(rop,op2),
        "Result operand differ in dimension in mps_threaded_map2()\n" );

    /* TODO: deal with this issue instead of failing on it*/
    MPS_ASSERT_MSG( numthreads <= matsize, "Trying to use too many threads in mps_threaded_map()\n");

    step = matsize / numthreads;

    if (matsize % numthreads > step)    /* helps a bit with odd distributions */
        ++step;

    tids = malloc(sizeof(pthread_t) * numthreads);
    args = malloc(sizeof(TArgs) * numthreads);

    for ( i = 0, j = 1; i < numthreads; ++i )
    {
        args[i].rop = rop;
        args[i].op1 = op1;
        args[i].op2 = op2;
        args[i].func2 = func;
        args[i].start = j;
        args[i].rnd = rnd;
        if (i < numthreads - 1)        /* not the last 1 */
            args[i].end = j += step;
        else                        /* give whatever's left to the last thread */
            args[i].end = matsize;

        pthread_create(&tids[i], pta, tmap_worker2, &args[i]);
    }

    for ( i = 0; i < numthreads; ++i )    /* wait for threads to finish */
        pthread_join(tids[0], NULL);

    free(args);
    free(tids);

    return 0;
}

static void* test_mul(void* _args)
{
    unsigned int i;
    mpfr_ptr subrop, subop1, subop2;
    TArgs* args = (TArgs*) _args;

    for ( i = args->start; i <= args->end; ++i )
    {
        subrop = mps_get_ele_col( args->rop, i );
        subop1 = mps_get_ele_col( args->op1, i );
		subop2 = mps_get_ele_col( args->op2, i );
        mpfr_mul(subrop, subop1, subop2, args->rnd);
    }

    return NULL;
}

int mps_test_mul( mps_ptr rop, mps_ptr op1, mps_ptr op2, pthread_attr_t* pta, unsigned int numthreads, mpfr_rnd_t rnd )
{
    pthread_t* tids;
    TArgs* args;
    unsigned int i, j, step, matsize = MPS_SIZE(op1);

    step = matsize / numthreads;

    if (matsize % numthreads > step)    /* helps a bit with odd distributions */
        ++step;

    tids = malloc(sizeof(pthread_t) * numthreads);
    args = malloc(sizeof(TArgs) * numthreads);

    for ( i = 0, j = 1; i < numthreads; ++i )
    {
        args[i].rop = rop;
        args[i].op1 = op1;
        args[i].op2 = op2;
        args[i].start = j;
        args[i].rnd = rnd;
        if (i < numthreads - 1)        /* not the last 1 */
            args[i].end = j += step;
        else                        /* give whatever's left to the last thread */
            args[i].end = matsize;

        pthread_create(&tids[i], pta, test_mul, &args[i]);
    }

    for ( i = 0; i < numthreads; ++i )    /* wait for threads to finish */
        pthread_join(tids[0], NULL);

    free(args);
    free(tids);

    return 0;
}

#endif    /* PTHREAD_COMPARE */

/* typedef a generic function pointer. */
typedef void(*gen_fptr)( void );

/* typdef the real function calls. */
typedef int(*call_type3)( mps_ptr, mps_ptr, double, mpfr_rnd_t );

#define NUM_FUNC 8

unsigned int rnum = 0;

gen_fptr func_array[NUM_FUNC];
int type_array[NUM_FUNC] = {0};
char* func_name[] = {   "mps_add",
                        "mps_sub",
                        "mps_mul",
                        "xmps_mul",
                        "mps_sin",
                        "mps_sin2",
                        "xmps_sin",
                        "mps_div"};

typedef enum _func_type
{
    FUNC_MPSFUNC,
    FUNC_MPSFUNC2,
    FUNC_XMPSFUNC,
    FUNC_XMPSFUNC2
} func_type_t;

void initfuncarray();
unsigned int searchfunc( const char*, int*, gen_fptr* );
void bench1( FILE*, gen_fptr, unsigned int, unsigned int, mpfr_prec_t, unsigned int );
void fillrandom( mps_ptr );

void initfuncarray()
{
    /* Init the array of function pointer. */
    /* Note the very evil cast to a generic function pointer. */
    unsigned int i = 0;
    type_array[i] = FUNC_MPSFUNC2;
    func_array[i++] = (gen_fptr)&mps_add;

    type_array[i] = FUNC_MPSFUNC2;
    func_array[i++] = (gen_fptr)&mps_sub;

    type_array[i] = FUNC_MPSFUNC2;
    func_array[i++] = (gen_fptr)&mps_mul;

    type_array[i] = FUNC_XMPSFUNC2;
    func_array[i++] = (gen_fptr)&xmps_mul;

    type_array[i] = FUNC_MPSFUNC;
    func_array[i++] = (gen_fptr)&mps_sin;

    type_array[i] = FUNC_MPSFUNC;
    func_array[i++] = (gen_fptr)&mps_sin2;

    type_array[i] = FUNC_XMPSFUNC;
    func_array[i++] = (gen_fptr)&xmps_sin;

    type_array[i] = FUNC_MPSFUNC2;
    func_array[i++] = (gen_fptr)&mps_div;

    return;
}

unsigned int searchfunc( const char* funcname, int* type, gen_fptr* fptr )
{
    unsigned int i = 0;

    for ( i = 0; i < NUM_FUNC; i++ )
    {
        if( strcmp( funcname, func_name[i] ) == 0 )
        {
            *fptr = func_array[i];
            *type = type_array[i];
            return i;
        }
    }

    return -1;
}

void fillrandom( mps_ptr rop )
{
    gmp_randstate_t randstate;
    gmp_randinit_mt ( randstate );

    mps_urandomb(rop, randstate);

    gmp_randclear( randstate );

    return;
}

void bench1( FILE* file,
             gen_fptr fptr,
             unsigned int sizem,
             unsigned int sizen,
             mpfr_prec_t prec,
             unsigned int numthreads )
{
    mps_t A, B;

#if PTHREAD_COMPARE
    mps_t C, D;    /* C and D are copies of A and B for pthreads */
	pthread_attr_t pta;

	pthread_attr_init(&pta);
#endif

#ifdef HAVE_GETHRTIME
    hrtime_t hr_ts, hr_te, time;
#elif HAVE_MACH_TIME
    uint64_t start, end, elapsed;
    Nanoseconds elapsedNano;
#else
    struct timeval start;
    struct timeval stop;
    /* struct timezone tz; */
    suseconds_t time;
#endif

    double f_time;
    mps_init( A, sizem, sizen, prec, 0 );
    mps_init( B, sizem, sizen, prec, 0 );
    fillrandom( A );
    fillrandom( B );

#if PTHREAD_COMPARE
    mps_init( C, sizem, sizen, prec, 0 );
    mps_init( D, sizem, sizen, prec, 0 );
    mps_copy(C, A);
    mps_copy(D, B);
#endif

#ifdef HAVE_GETHRTIME
    hr_ts = gethrtime();
#elif HAVE_MACH_TIME
    start = mach_absolute_time();
#else
    /* gettimeofday( &start, &tz ); */
    gettimeofday( &start, NULL );
#endif

    /* Even more evil, typecast it back to the right function type and call it. */
    /* ((MPSFunc)fptr)( A, A, B, GMP_RNDN ); */
    switch ( type_array[rnum] )
    {
        case FUNC_XMPSFUNC2:
            ((XMPSFunc2)fptr)( A, A, B, GMP_RNDN, numthreads );
            break;
        case FUNC_XMPSFUNC:
            ((XMPSFunc)fptr)( A, B, GMP_RNDN, numthreads );
            break;
        case FUNC_MPSFUNC2:
            ((MPSFunc2)fptr)( A, A, B, GMP_RNDN );
            break;
        case FUNC_MPSFUNC:
            ((MPSFunc)fptr)( A, B, GMP_RNDN );
            break;
        default:
            fprintf(stderr, "Bad function type?\n");
            exit(EXIT_FAILURE);
    }

#ifdef HAVE_GETHRTIME
    hr_te = gethrtime();
    time = hr_te - hr_ts;
    f_time = (double)time / 1000000000;
    fprintf( file, "%f\n", f_time );
#elif HAVE_MACH_TIME
    end = mach_absolute_time();
    elapsed = end - start;
    elapsedNano = AbsoluteToNanoseconds( *(AbsoluteTime *) &elapsed );
    fprintf( file, "%lld\n",  * (uint64_t *) &elapsedNano );
#else
    /*gettimeofday( &stop, &tz );*/
    gettimeofday( &stop, NULL );

    if (start.tv_usec > stop.tv_usec)
    {
        stop.tv_usec += 1000000;
        stop.tv_sec--;
    }

    time = stop.tv_usec - start.tv_usec;
    time += (stop.tv_sec - start.tv_sec)*1000000;
    f_time = (double)time / 1000000;

#if PTHREAD_COMPARE
    fprintf( file, "o:%f\t", f_time );
#else
    fprintf( file, "%.20g\t", f_time );
#endif

#endif

#if PTHREAD_COMPARE    /* Assuming gettimeofday for now at least */
    gettimeofday( &start, NULL );
    /* ((MPSFunc)fptr)( C, C, D, GMP_RNDN ); */

    mps_test_mul(C, C, D, &pta, numthreads, GMP_RNDN);
    gettimeofday( &stop, NULL );
    if (start.tv_usec > stop.tv_usec)
    {
        stop.tv_usec += 1000000;
        stop.tv_sec--;
    }

    time = stop.tv_usec - start.tv_usec;
    time += (stop.tv_sec - start.tv_sec)*1000000;
    f_time = (double)time / 1000000;

    fprintf( file, "p:%f\n", f_time );

#endif

    mps_frees(A, B, (mps_ptr) NULL);

	#if PTHREAD_COMPARE
	pthread_attr_destroy(&pta);
	#endif

    return;
}

#define HEADERSIZE 80

int main( int argc, char *argv[] )
{
    static int numrow, numcol, precision, numthreads, sizestart, sizeend, sizestep, loops, scalestep;
    int ftype, o;
    gen_fptr fptr = NULL;
    FILE* outfile;
    FILE* tmpstream;
    char useAbsoluteSize = 0;   /* loops over a range */
    int i, j;
    char headerstring[HEADERSIZE];

    poptContext context;
    static char* filename;
    static char* funcname;

    static const struct poptOption options[] =
    {
        { "filename", 'f', POPT_ARG_STRING, &filename, 'f', "Output filename", NULL },
        { "function-name", 'F', POPT_ARG_STRING, &funcname, 'f', "Function name", NULL },
        { "precision", 'p', POPT_ARG_INT, &precision, 'p', "Precision", NULL },
        { "numcol", 'm', POPT_ARG_INT, &numcol, 'm', "Number of columns", NULL },
        { "numrow", 'n', POPT_ARG_INT, &numrow, 'n', "Number of rows", NULL },
        { "sizestart", 's', POPT_ARG_INT, &sizestart, 's', "starting size. m n irrelevant", NULL},
        { "sizeend", 'e', POPT_ARG_INT, &sizeend, 's', "end size", NULL},
        { "sizestep", 'i', POPT_ARG_INT, &sizestep, 's', "i for increment", NULL},
                { "scalestep", 'M', POPT_ARG_INT, &scalestep, 'M', "M for multiply", NULL},
        { "loops", 'a', POPT_ARG_INT, &loops, 'a', "more loops so you can average later", NULL},
        { "numthread", 't', POPT_ARG_INT, &numthreads, 't', "Number of threads to use", NULL },
        POPT_AUTOHELP
        { NULL, 0, 0, NULL, 0, NULL, NULL }
    };

    context = poptGetContext(argv[0], argc, (const char**) argv, options, 0);

    while ( ( o = poptGetNextOpt(context)) >= 0 );

    if ( o < -1 )
    {
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    /* completely ignore m and n args if using sizestart and sizeend */
    if ( sizestart != 0 )
    {

        useAbsoluteSize = 1;
        if ( sizeend < sizestart )
        {
            fprintf( stdout, "Size start must be less than the end.\n" );
            return -1;
        }
        numcol = 1;
    }
    else
    {
        if ( numrow == 0 )
        {
            fprintf( stdout, "Invalid argument for the number of rows.\n" );
            poptPrintHelp(context, stderr, 0);
            return -1;
        }

        if ( numcol == 0 )
        {
            fprintf( stdout, "Invalid argument for the number of columns.\n" );
            poptPrintHelp(context, stderr, 0);
            return -1;
        }
    }

    if ( precision < MPFR_PREC_MIN )
    {
        fprintf( stdout, "Minimum precision must be at least 2 bits.\n" );
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    if ( funcname == NULL )
    {
        fprintf( stdout, "Must specify function.\n" );
        poptPrintHelp(context, stderr, 0);
        return -1;
    }

    if ( numthreads == 0 )        /* Default to using 1 thread */
        numthreads = 1;

    if ( loops == 0 )
        loops = 1;

    if ( scalestep != 0 )   /* override normal step */
    {
        sizestep = 0;
    }

    poptFreeContext(context);

    mps_set_num_threads( numthreads );

    initfuncarray();

    rnum = searchfunc( funcname, &ftype, &fptr );

    if ( filename == NULL )
        outfile = stdout;
    else
    {
        /* stupid hacky way of testing if file exists and adding a header if new */
        tmpstream = fopen(filename, "r");
        if (tmpstream)
        {
            fclose(tmpstream);
            outfile = fopen( filename, "a" );
        }
        else
        {
            sprintf(headerstring,
                    "Starting size : End Size : Step, iterations, precision, threads\n");

            outfile = fopen( filename, "a" );
            fprintf(outfile, "%s", headerstring);
        }
    }

    /* this is all so ugly and poorly organized */
    if ( useAbsoluteSize )
    {
        if ( sizestep <= 0 )
            sizestep = 1;

        fprintf(outfile,
                "size=%d %d %d %d iterations=%d precision=%d threads=%d\n",
                sizestart, sizeend, sizestep, scalestep, loops, precision, numthreads);

        for ( j = 0; j < loops; ++j )
        {
            fprintf(outfile, "\t");
            if ( scalestep == 0 )
            {
                for ( i = sizestart; i <= sizeend; i += sizestep )
                    bench1( outfile, fptr, i, numcol, precision, numthreads );
            }
            else
            {
                for ( i = sizestart; i <= sizeend; i *= scalestep )
                    bench1( outfile, fptr, i, numcol, precision, numthreads );
            }

            fprintf(outfile, "\n");
        }
    }
    else
        bench1( outfile, fptr, numrow, numcol, precision, numthreads );

    fprintf(outfile, "\n");

    free(filename);
    free(funcname);

    fclose(outfile);

    return 0;
}

