/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#ifndef __MPS_ASSERT_H__
#define __MPS_ASSERT_H__

/* mps_assert.c functions. */
void mps_assert_fail( const char *, int, const char * );
void mps_assert_fail_msg( const char *, int, const char *, const char * );
void mps_warning_msg( const char *, int, const char * );

/* Activate assertion checks if specified by the build system. */
#ifdef WITH_ASSERT_CHECKS

#define MPS_ASSERT(expr) ((void) ((expr) || (MPS_ASSERT_FAIL(expr),0) ))
#define MPS_ASSERT_MSG(expr, msg) ((void) ((expr) || (MPS_ASSERT_FAIL_MSG(expr, msg),0) ))

#define MPS_ASSERT_FAIL(expr)  mps_assert_fail(__FILE__, __LINE__, #expr)
#define MPS_ASSERT_FAIL_MSG(expr, msg)  mps_assert_fail_msg(__FILE__, __LINE__, #expr, msg)

#define MPS_WARNING_MSG(msg) mps_warning_msg(__FILE__, __LINE__, msg)

#else

#define MPS_ASSERT(expr) ((void) 0)
#define MPS_ASSERT_MSG(expr, msg) ((void) 0)
#define MPS_ASSERT_FAIL(expr)  ((void) 0)
#define MPS_ASSERT_FAIL_MSG(expr, msg)  ((void) 0)
#define MPS_WARNING_MSG(msg) ((void) 0)

#endif

/* Matrix squareness test macros. */
#define MPS_IS_SQUARE(x)  ( MPS_NUMROW(x) == MPS_NUMCOL(x) )
#define MPS_NOT_SQUARE(x) ( MPS_NUMROW(x) != MPS_NUMCOL(x) )

/* Matrix ordering macros. */
#define MPS_IS_COL_ORDER(x) ( MPS_GET_ORDER(x) == MPS_COL_ORDER )
#define MPS_IS_ROW_ORDER(x) ( MPS_GET_ORDER(x) == MPS_ROW_ORDER )

/* Bound and size checking macros. */
#define MPS_INDX_BOUND(x,y)     ( MPS_NUMCOL(x) * MPS_NUMROW(x) >= y )
#define MPS_ROW_BOUND(x,y)      ( MPS_NUMROW(x) >= y )
#define MPS_COL_BOUND(x,y)      ( MPS_NUMCOL(x) >= y )
#define MPS_DIAG_BOUND(x,r)     ( (r == 0) || (r > 0 && (unsigned)r < MPS_NUMCOL(x)) || (r < 0 && (unsigned)-r < MPS_NUMROW(x)) )
#define MPS_SAME_SIZE(x,y)      ( (MPS_NUMCOL(x) == MPS_NUMCOL(y) && MPS_NUMROW(x) == MPS_NUMROW(y)) )
#define MPS_SAME_PREC(x,y)      ( MPS_PREC(x) == MPS_PREC(y) )
#define MPS_SAME_ALLOC(x,y)     ( (MPS_ALLOC_SIZE(x) == MPS_ALLOC_SIZE(y) && MPS_LIMBS_ALLOC_SIZE(x) == MPS_LIMBS_ALLOC_SIZE(y)) )
#define MPS_CAN_MULT(x,y)       ( MPS_NUMCOL(x) == MPS_NUMROW(y) )
#define MPS_IS_COL(x)           ( MPS_NUMCOL(x) == 1 )
#define MPS_IS_ROW(x)           ( MPS_NUMROW(x) == 1 )
#define MPS_IS_SCALAR(x)        ( MPS_NUMROW(x) == 1 && MPS_NUMCOL(x) == 1 )
#define MPS_IS_VECTOR(x)        ( MPS_NUMROW(x) == 1 || MPS_NUMCOL(x) == 1 )
#define MPS_SAME_COUNT(x,y)     ( (MPS_NUMROW(x) * MPS_NUMCOL(x)) == (MPS_NUMROW(y) * MPS_NUMCOL(y)) )

/* FIXME: These are wrong. They don't account for minrow/mincol and stuff. */
#define MPS_BLOCK_ROW_BOUND(x,y)        ( MPS_BLOCK_NUMROW(x) >= y && MPS_BLOCK_NUMROW(x) > 0 )
#define MPS_BLOCK_COL_BOUND(x,y)        ( MPS_BLOCK_NUMCOL(x) >= y && MPS_BLOCK_NUMCOL(x) > 0 )
#define MPS_BLOCK_INDX_BOUND(x,y)       ( MPS_BLOCK_NUMROW(x) * MPS_BLOCK_NUMCOL(x) >= y )
#define MPS_BLOCK_SAME_SIZE(x,y)        ( MPS_BLOCK_NUMROW(x) == MPS_BLOCK_NUMROW(y) && MPS_BLOCK_NUMCOL(x) == MPS_BLOCK_NUMCOL(y) )

#define MPS_BLOCK_IS_COL(x)             ( MPS_BLOCK_NUMCOL(x) == 1 )
#define MPS_BLOCK_IS_ROW(x)             ( MPS_BLOCK_NUMROW(x) == 1 )

/* check that the bounds are within the whole matrix
 * TODO: Check these are right */
#define MPS_BLOCK_REAL_INDX_BOUND(x,y)  ( MPS_NUMROW(MPS_BLOCK_MAT(x)) * MPS_NUMCOL(MPS_BLOCK_MAT(x)) >= y )
#define MPS_BLOCK_REAL_ROW_BOUND(x,y)   ( MPS_NUMROW(MPS_BLOCK_MAT(x)) >= y && y > 0 )
#define MPS_BLOCK_REAL_COL_BOUND(x,y)   ( MPS_NUMCOL(MPS_BLOCK_MAT(x)) >= y && y > 0 )
#define MPS_BLOCK_REAL_BOUND(x,y,z)     ( MPS_BLOCK_REAL_ROW_BOUND(x,y) && MPS_BLOCK_COL_BOUND(x,z) )

#define MPS_BLOCK_SAME_MAT(x,y)         ( MPS_BLOCK_MAT(x) == MPS_BLOCK_MAT(y) )
#define MPS_VEC_SAME_MAT(x,y)           ( MPS_VEC_MAT(x) == MPS_VEC_MAT(y) )

#define MPS_VEC_VALID(x)                ( MPS_VEC_NUMCOL(x) == 1 )
#define MPS_VEC_BOUND(x,y)              ( MPS_VEC_NUMROW(x) >= y)
#define MPS_VEC_SAME_SIZE(x,y)          ( MPS_VEC_NUMROW(x) == MPS_VEC_NUMROW(y) )


/* Pointer related stuff. */
#define MPS_NOT_ALIAS(x,y)  ( MPS_MPFR_ARRAY(x) != MPS_MPFR_ARRAY(y) )
#define MPS_NOT_NULL(x)     ( MPS_MPFR_ARRAY(x) != NULL && MPS_LIMBS_ARRAY(x) != NULL )

/* convenient functions for printing debugging stuffs */
#define MPS_DEBUG(msg, ...)     mpfr_printf("%s():%d: ", __func__, __LINE__); \
                                    mpfr_printf(msg, __VA_ARGS__);

#endif /* __MPS_ASSERT_H__ */
