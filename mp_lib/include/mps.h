/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#ifndef __MPS_H__
#define __MPS_H__

#include <stdio.h>

#include "mpfr.h"

/* Configuration header generated at compile time. */
#include "mps_config.h"

/* Support for windows DLL done in a similar way to MPFR. */
#if defined(__MPS_WITHIN_MPS) && __GMP_LIBGMP_DLL
# define __MPS_DECLSPEC __GMP_DECLSPEC_EXPORT
#else
# define __MPS_DECLSPEC __GMP_DECLSPEC
#endif

/*
 * The basic type is a dense multi-precision matrix with the element being
 * single mpfr number of the same precision.
 *
 * The data is stored in three parts. The first is the multi precision header
 * used as the elementary type for all operations. It is defined by the
 * mps_struct structure and is typedef'd to an mps_ptr. This structure contains
 * all the information required to identify the matrix such as it's size and
 * precision. It also contains two pointers, one to the array of mpfr_t and the
 * second one to an array of limb.
 *
 * The first array is arranged as a packed list of mpfr_t data structure
 * linearily arranged column by column or row by row depending on it's order
 * flag. The limbs are stored in a second array prepended with an allocation
 * structure mps_alloc_struct. This allocation structure contains information
 * used for memory management and possible garbage collection. It also itself
 * an element of a double linked list. As such it is possible to traveres the
 * entire list of allocated matrix that have not be freed.
 */

/* Struct defining the header of an mps matrix. */
typedef struct {
    void *              _mps_mpfr_array;        /* Ptr to the array of mpfr_t. */
    void *              _mps_limbs_array;       /* Ptr to the array of limbs. */
    unsigned long int   _mps_sizerow;           /* Number of rows. */
    unsigned long int   _mps_sizecol;           /* Number of columns. */
    size_t              _mps_alloc_size;        /* Size of the mpfr_t array. */
    size_t              _mps_limbs_alloc_size;  /* Size of the limbs array. */
    mpfr_prec_t         _mps_prec;              /* Precision of the elements. */
    unsigned int        _mps_type;              /* Type structure. */
} ___mps_struct;

/* Allocation structure prepended to the array of limbs. */
typedef struct __mps_alloc_struct {
    struct __mps_alloc_struct *   _mps_next_alloc;
    struct __mps_alloc_struct *   _mps_prev_alloc;
    void *                        _mps_mpfr_array;
    void *                        _mps_limbs_array;
    size_t                        _mps_alloc_size;
    size_t                        _mps_limbs_alloc_size;
} ___mps_alloc_struct;

typedef ___mps_struct mps_t[1];
typedef ___mps_struct *mps_ptr;
typedef ___mps_alloc_struct *mps_alloc_ptr;

/* Used to pass the value of OpenMP ICVs to mpscilab for debugging. */
typedef struct _mps_omp_struct {
    int num_threads;            /* Number of threads in the current team. */
    int max_threads;            /* Maximum thread limit for a new team. */
    int thread_num;             /* Thread id of the current thread. */
    int num_procs;              /* Number of processors available to mplib. */
    int in_parallel;            /* True if inside an active parallel region. */
    int dynamic;                /* True if dynamic thread adjustement is enabled. */
    int nested;                 /* True if nested parallelism is enabled. */
    int schedule_kind;          /* Default schedule. */
    int schedule_modifier;      /* Modifier to the default schedule. */
    int thread_limit;           /* Maximum number of thread for OpenMP. */
    int max_active_levels;      /* Maximum active nested parallel region. */
    int level;                  /* Current nested level. */
    int ancestor_thread_num;    /* Thread id of the parent thread. */
    int team_size;              /* Current team size. */
    int active_level;           /* Current active level. */
} mps_omp_struct;
    

/* Some function pointers, used in benchmarking and some unitary tests. */
typedef void (*MPFunc) ( mpfr_ptr, mpfr_ptr, mpfr_rnd_t );
typedef void (*MPFunc2) ( mpfr_ptr, mpfr_ptr, mpfr_ptr, mpfr_rnd_t );

typedef void (*MPSFunc) ( mps_ptr, mps_ptr,  mpfr_rnd_t );
typedef void (*XMPSFunc) ( mps_ptr, mps_ptr,  mpfr_rnd_t, unsigned int);

typedef void (*MPSFunc2) ( mps_ptr, mps_ptr, mps_ptr,  mpfr_rnd_t );
typedef void (*XMPSFunc2) ( mps_ptr, mps_ptr, mps_ptr,  mpfr_rnd_t, unsigned int);

typedef void (*MPSFuncSca) ( mps_ptr, mps_ptr, mpfr_ptr,  mpfr_rnd_t );

typedef struct _mps_block_struct
{
    mps_ptr         _mat;
    unsigned int    _minrow;
    unsigned int    _mincol;
    unsigned int    _numrow;
    unsigned int    _numcol;
} __mps_block_struct;

typedef __mps_block_struct mps_blk_t[1];
typedef __mps_block_struct* mps_blk_ptr;

/* having all vectors be represented as submatrices might be convenient,
 * Is same thing so can cast if needed but will give you helpful warnings in some cases */
typedef __mps_block_struct mps_vec_t[1];
typedef __mps_block_struct* mps_vec_ptr;

typedef enum
{
    MPS_COL_ORDER,
    MPS_ROW_ORDER
} mps_order_t;

#define MPS_DEFAULT_ORDER MPS_COL_ORDER

/* Access macros for the struct ___mps_struct members. */
#define MPS_MPFR_ARRAY(x)       ((x)->_mps_mpfr_array)
#define MPS_ALLOC_SIZE(x)       ((x)->_mps_alloc_size)
#define MPS_LIMBS_ALLOC_SIZE(x) ((x)->_mps_limbs_alloc_size)
#define MPS_PREC(x)             ((x)->_mps_prec)
#define MPS_NUMROW(x)           ((x)->_mps_sizerow)
#define MPS_NUMCOL(x)           ((x)->_mps_sizecol)
#define MPS_TYPE(x)             ((x)->_mps_type)
#define MPS_SIZE(x)             (MPS_NUMROW(x) * MPS_NUMCOL(x))
#define MPS_VEC_LEN(x)          ((x)->_mps_sizerow)

/* Access macros for the struct ___mps_alloc_struct members. */
#define MPS_NEXT(x)             ((x)->_mps_next_alloc)
#define MPS_PREV(x)             ((x)->_mps_prev_alloc)
#define MPS_LIMBS_ARRAY(x)      ((x)->_mps_limbs_array)

/* Type flags */
typedef enum
{
    MPS_TYPE_ORDER = 1,         /* Row/column order flag. */
    MPS_TYPE_COMPLEX = 1 << 1,  /* Reserved, not used atm. */
    MPS_TYPE_SPARSE = 1 << 2    /* Reserved, not used atm. */
} mps_type_t;

/* Access macros for block matrix members */
#define MPS_BLOCK_NUMROW(x)     ((x)->_numrow)
#define MPS_BLOCK_NUMCOL(x)     ((x)->_numcol)
#define MPS_BLOCK_MINROW(x)     ((x)->_minrow)
#define MPS_BLOCK_MINCOL(x)     ((x)->_mincol)
#define MPS_BLOCK_MAXROW(x)     ((x)->_minrow + (x)->_maxrow - 1)
#define MPS_BLOCK_MAXCOL(x)     ((x)->_mincol + (x)->_maxcol - 1)
#define MPS_BLOCK_MAT(x)        ((x)->_mat)
#define MPS_BLOCK_SIZE(x)       ((x)->_numrow * (x)->_numcol)
#define MPS_BLOCK_PREC(x)       ((x)->_mat->_mps_prec)

/* vector aliases */
#define MPS_VEC_NUMROW(x)       MPS_BLOCK_NUMROW(x)
#define MPS_VEC_NUMCOL(x)       1	/* If this isn't 1, there are issues */
#define MPS_VEC_MINROW(x)       MPS_BLOCK_MINROW(x)
#define MPS_VEC_MIN(x)          MPS_BLOCK_MINROW(x)
#define MPS_VEC_MINCOL(x)       MPS_BLOCK_MINCOL(x)
#define MPS_VEC_MAXROW(x)       MPS_BLOCK_MAXROW(x)
#define MPS_VEC_MAXCOL(x)       MPS_BLOCK_MAXCOL(x)
#define MPS_VEC_MAT(x)          MPS_BLOCK_MAT(x)
#define MPS_VEC_SIZE(x)         MPS_BLOCK_NUMROW(x)
#define MPS_VEC_PREC(x)         MPS_BLOCK_PREC(x)

/* Type flags manipulation macros. */

/*
 * Return FALSE(0) if the matrix is in column major ordering,
 * TRUE(>0) otherwise.
 */
#define MPS_GET_ORDER(x)    ((x)->_mps_type & MPS_TYPE_ORDER)

/* File format structures, macros and type definitions. */

/* Selects a suitable 64 bit integer type. */
#if defined HAVE_ULINT64
    typedef unsigned long int mps_uint64_t;
#elif defined HAVE_ULLINT64
    typedef unsigned long long int mps_uint64_t;
#elif defined HAVE_UINT64
    typedef unsigned int mps_uint64_t;
#else
    #error "No 64 bit unsigned data type available."
#endif

/* Selects a suitable signed 64 bit integer type. */
#if defined HAVE_LINT64
    typedef unsigned long int mps_int64_t;
#elif defined HAVE_LLINT64
    typedef unsigned long long int mps_int64_t;
#elif defined HAVE_INT64
    typedef unsigned int mps_int64_t;
#else
    #error "No 64 bit signed data type available."
#endif

/* Selects a suitable 32 bit integer type. */
#if defined HAVE_UINT32
    typedef unsigned int mps_uint32_t;
#elif defined HAVE_ULINT32
    typedef unsigned long int mps_uint32_t;
#elif defined HAVE_USHORT32
    typedef unsigned int mps_uint32_t;
#else
    /* This should just never happen. */
    #error "No 32 bit unsigned data type available."
#endif

/* Selects a suitable 32 bit integer type. */
#if defined HAVE_INT32
    typedef unsigned int mps_int32_t;
#elif defined HAVE_LINT32
    typedef unsigned long int mps_int32_t;
#elif defined HAVE_SHORT32
    typedef unsigned int mps_int32_t;
#else
    /* This should just never happen. */
    #error "No 32 bit signed data type available."
#endif

/* 64 bit byte swapping macro. */
#define	MPS_SWAP_64(x)  (((mps_uint64_t)(x) << 56) | \
                        (((mps_uint64_t)(x) << 40) & 0xff000000000000ULL) | \
                        (((mps_uint64_t)(x) << 24) & 0xff0000000000ULL) | \
                        (((mps_uint64_t)(x) << 8)  & 0xff00000000ULL) | \
                        (((mps_uint64_t)(x) >> 8)  & 0xff000000ULL) | \
                        (((mps_uint64_t)(x) >> 24) & 0xff0000ULL) | \
                        (((mps_uint64_t)(x) >> 40) & 0xff00ULL) | \
                        ((mps_uint64_t)(x)  >> 56))

/* 32 bit byte swapping macro. */
#define	MPS_SWAP_32(x)  (((mps_uint32_t)(x) << 24) | \
                        (((mps_uint32_t)(x) << 8) & 0xff0000) | \
                        (((mps_uint32_t)(x) >> 8) & 0xff00) | \
                        ((mps_uint32_t)(x)  >> 24))

/* Endianness specifier. */
typedef enum
{
    MPS_BIG_ENDIAN,
    MPS_LITTLE_ENDIAN,
    MPS_NATIVE_ENDIAN
} mps_endian_t;

/*
 * Byte definitions for the mps file format signature. Blatantly inspired by
 * the png signature. Unstable, used at the moment for debugging, final
 * signature will probably be different.
 */
#define MPS_FS0 0x89
#define MPS_FS1 0x4D /* M */
#define MPS_FS2 0x50 /* P */
#define MPS_FS3 0x53 /* S */
#define MPS_FS4 0x0D
#define MPS_FS5 0x0A
#define MPS_FS6 0x1A
#define MPS_FS7 0x0A

/* Some defines to represent 32/64 bit big and little endian platforms. */
#define MPS_BIG_32 0x34343D3D3D3D3434
#define MPS_BIG_64 0x38383D3D3D3D3838
#define MPS_LIT_32 0x34344C4C4C4C3434
#define MPS_LIT_64 0x38384C4C4C4C3838

/* Struct containing all the data of a file header. */
typedef struct {
    unsigned int    _mps_file_format;       /* Format of the stored data. */
    mps_endian_t    _mps_file_endian;       /* Endianness of the target data. */
    unsigned int    _mps_file_word;         /* Word size of the target data. */
    unsigned int    _mps_file_numrow;       /* Number of rows. */
    unsigned int    _mps_file_numcol;       /* Number of columns. */
    unsigned int    _mps_file_precision;    /* Precision. */
    unsigned int    _mps_file_version;      /* Version field. */
} mps_file_struct;

/* mps_mem.c functions. */
__MPS_DECLSPEC void * mps_alloc( const unsigned int, const unsigned int, const mpfr_prec_t );
__MPS_DECLSPEC int mps_init( mps_ptr, const unsigned int, const unsigned int, const mpfr_prec_t, const mps_order_t );
__MPS_DECLSPEC int mps_free( const mps_ptr );
__MPS_DECLSPEC int mps_free_alloc( const mps_alloc_ptr limbs_array );
__MPS_DECLSPEC int mps_free_all();
__MPS_DECLSPEC int mps_exist( const mps_ptr );
__MPS_DECLSPEC int mps_position( const mps_ptr );
__MPS_DECLSPEC mps_alloc_ptr mps_return_alloc_pos( const int );
__MPS_DECLSPEC mps_alloc_ptr mps_return_alloc_list_head();
__MPS_DECLSPEC int mps_copy( mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_clone( mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_isalias( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_set_copy( mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_swap( mps_ptr, mps_ptr );
__MPS_DECLSPEC unsigned int mps_alloc_list_size();
__MPS_DECLSPEC size_t mps_alloc_size();
__MPS_DECLSPEC size_t mps_size_est( const unsigned int, const unsigned int, const mpfr_prec_t );
__MPS_DECLSPEC int mps_set( mps_ptr, const unsigned int, const unsigned int, const mpfr_prec_t, const mps_order_t );

__MPS_DECLSPEC void mps_exg_dim( mps_ptr );

__MPS_DECLSPEC void mps_frees( mps_ptr x, ...);
__MPS_DECLSPEC void mps_inits( const unsigned int, const unsigned int, const mpfr_prec_t, const mps_order_t, mps_ptr, ... );

/*OpenMP related stuff. Might be inlined in the future. */
__MPS_DECLSPEC void mps_set_num_threads( unsigned int );
__MPS_DECLSPEC unsigned int mps_get_num_threads();
__MPS_DECLSPEC int mps_get_num_procs();
__MPS_DECLSPEC int mps_openmp_p();
__MPS_DECLSPEC mps_omp_struct mps_omp_status();

#ifdef WITH_C99_INLINE
#define _INLINE_SPEC_ inline
#else
#define _INLINE_SPEC_
#endif

/* Inlinable element access functions. */
__MPS_DECLSPEC _INLINE_SPEC_ mpfr_ptr mps_get_ele( const mps_ptr, unsigned int, unsigned int );
__MPS_DECLSPEC _INLINE_SPEC_ mpfr_ptr mps_get_ele_seq( const mps_ptr, unsigned int );
__MPS_DECLSPEC _INLINE_SPEC_ mpfr_ptr mps_get_ele_row( const mps_ptr, unsigned int );
__MPS_DECLSPEC _INLINE_SPEC_ mpfr_ptr mps_get_ele_col( const mps_ptr, unsigned int );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele( const mps_ptr, unsigned int, unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele_double( const mps_ptr, unsigned int, unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele_zero( const mps_ptr, unsigned int, unsigned int );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele_seq( const mps_ptr, unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele_row( const mps_ptr, unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_ele_col( const mps_ptr, unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_ele_fastswap( mpfr_ptr, mpfr_ptr );

/* Misc inlinable functions. */
__MPS_DECLSPEC _INLINE_SPEC_ int mps_cpy_mpfr_struct( mpfr_ptr, const mpfr_ptr );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_set_order( mps_ptr, const mps_order_t );
__MPS_DECLSPEC _INLINE_SPEC_ void mps_flip_order( mps_ptr );

#ifdef _INLINE_SPEC_
    #undef _INLINE_SPEC_
#endif

/* mps_numthr.c functions. */
__MPS_DECLSPEC unsigned int mpt_linear( unsigned int, mpfr_prec_t, int, int, int );
__MPS_DECLSPEC unsigned int mpt_const( unsigned int );
__MPS_DECLSPEC unsigned int mpt_global();

/* mps_add.c functions. */
__MPS_DECLSPEC int mps_add_scalar( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_add_scalar_double( const mps_ptr, const mps_ptr, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_add_double( const mps_ptr, const double[], const mps_order_t, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_add( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_add_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_add_double( const mps_ptr, const double[], const mps_order_t, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_add_scalar_double( const mps_ptr, const double[], const mps_order_t, const double, const mpfr_rnd_t );

/* mps_sub.c functions. */
__MPS_DECLSPEC int mps_sub_scalar( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_sub( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sub_scalar_double( const mps_ptr, const mps_ptr, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_sub_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_sub( const mps_ptr, const double, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_sub_scalar( const mps_ptr, const double[], const mps_order_t, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sub( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sub_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_sub( const mps_ptr, const double[], const mps_ptr, const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_sub_double( const mps_ptr, const double[], const mps_order_t, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_sub_scalar_double( const mps_ptr, const double[], const mps_order_t, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_sub_double( const mps_ptr, const double, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_mul.c functions. */
__MPS_DECLSPEC int mps_mul_scalar( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_mul_scalar_double( const mps_ptr, const mps_ptr, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_mul_double( const mps_ptr, const double[], const mps_order_t, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_mul( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_mul_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_mul_double( const mps_ptr, const double[], const mps_order_t, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_mul_scalar_double( const mps_ptr, const double[], const mps_order_t, const double, const mpfr_rnd_t );

/* mps_div.c functions. */
__MPS_DECLSPEC int mps_div_scalar( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_div( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_div_scalar_double( const mps_ptr, const mps_ptr, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_div_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_div( const mps_ptr, const double, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_div_scalar( const mps_ptr, const double[], const mps_order_t, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_div( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_div_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_div( const mps_ptr, const double[], const mps_ptr, const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_div_double( const mps_ptr, const double[], const mps_order_t, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_div_scalar_double( const mps_ptr, const double[], const mps_order_t, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_div_double( const mps_ptr, const double, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_pow.c functions. */
__MPS_DECLSPEC int mps_pow( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_pow_scalar( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_pow( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_pow_ui_mpfr( const mps_ptr, const mps_ptr, const unsigned int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_ui_pow( const mps_ptr, const unsigned int, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_pow_si( const mps_ptr, const mps_ptr, const long int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_pow_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_pow( const mps_ptr, const double[], const mps_ptr, const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_pow_double( const mps_ptr, const double[], const mps_order_t, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_pow_scalar_double( const mps_ptr, const mps_ptr, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_pow( const mps_ptr, const double, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_pow_double( const mps_ptr, const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_pow_scalar( const mps_ptr, const double[], const mps_order_t, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_scalar_double_pow_double( const mps_ptr, const double, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_double_pow_scalar_double( const mps_ptr, const double[], const mps_order_t, const double, const mpfr_rnd_t );

/* mps_roots.c functions. */
__MPS_DECLSPEC int mps_sqrt( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sqrt_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_root( const mps_ptr, const mps_ptr, const unsigned long int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cbrt( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rec_sqrt( const mps_ptr, const mps_ptr, const mpfr_rnd_t );

/* mps_sqr.c functions. */
__MPS_DECLSPEC int mps_sqr( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sqr_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_exp.c functions. */
__MPS_DECLSPEC int mps_exp( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_exp_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_exp2( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_exp2_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_exp10( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_exp10_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_log.c functions. */
__MPS_DECLSPEC int mps_log( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log2( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log2_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log10( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log10_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log1p( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_log1p_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_factorial.c functions. */
__MPS_DECLSPEC int mps_factorial( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_factorial_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_trig.c functions. */
__MPS_DECLSPEC int mps_sin( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cos( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_tan( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_csc( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sec( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cot( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sin_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cos_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_tan_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_csc_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sec_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cot_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

__MPS_DECLSPEC int mps_asin( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_acos( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_atan( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_asin_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_acos_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_atan_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

__MPS_DECLSPEC int mps_sinh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cosh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_tanh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_csch( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sech( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_coth( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_asinh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_acosh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_atanh( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sinh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_cosh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_tanh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_csch_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_sech_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_coth_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_asinh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_acosh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_atanh_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_other.c functions. */
__MPS_DECLSPEC int mps_expm1( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_eint( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_li2( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_lngamma( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_zeta( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_j0( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_j1( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_jn( const mps_ptr, const long, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_y0( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_y1( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_yn( const mps_ptr, const long, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_abs( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_abs_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_neg( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_neg_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_gamma.c functions. */
__MPS_DECLSPEC int mps_gamma( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_gamma_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_erf.c functions. */
__MPS_DECLSPEC int mps_erf( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_erf_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_erfc( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_erfc_double( const mps_ptr, const double[], const mps_order_t, const mpfr_rnd_t );

/* mps_stats.c functions. */
__MPS_DECLSPEC int mps_agm( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_mean_mats( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_total( const mpfr_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_mean_mat( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_stddev( const mpfr_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_central_moment( const mpfr_ptr, const mps_ptr, const unsigned int, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_cmoments( const mps_ptr, const mps_ptr, const unsigned int, const char, const mpfr_rnd_t );

__MPS_DECLSPEC int mps_covariance_cols( const mpfr_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_covariance_mat( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_correlation_mat( const mps_ptr, const mps_ptr, const mpfr_rnd_t );

/* mps_fill.c functions. */
__MPS_DECLSPEC int mps_zero( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_ones( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_const_pi( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_const_e( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_const_euler( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_const_log2( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_const_catalan( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_identity( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rotation_2d( const mps_ptr, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_fill_seq( const mps_ptr, const mpfr_ptr, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_fill_seq_row( const mps_ptr, const mpfr_ptr, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_fill_seq_si( const mps_ptr, const int, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_fill_seq_row_si( const mps_ptr, const int, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_fill_seq_scale( const mps_ptr, const int, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_throw_nan( const mps_ptr, const mpfr_rnd_t );

/* mps_linspace.c */
__MPS_DECLSPEC int mps_linspace( const mps_ptr, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_linspace_double( const mps_ptr, double, double );

/* mps_rand.c functions. */
__MPS_DECLSPEC int mps_random( const mps_ptr, const int, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_urandomb( const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_urandom( const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_srandom( const mps_ptr, const int, const int, const unsigned int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rand_def( const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_unf( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_unf_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_nor( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_nor_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_nor1( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_nor1_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_exp( const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_exp_double( const mps_ptr, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_wei( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_wei_double( const mps_ptr, double, double, gmp_randstate_t );

/* mps_rand_gamma.c functions. */
__MPS_DECLSPEC int mps_rand_gam( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_gt( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_gt_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_gc( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_gc_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_go( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_gam_go_double( const mps_ptr, double, double, gmp_randstate_t );

/* mps_rand_beta.c functions. */
__MPS_DECLSPEC int mps_rand_beta( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_ba( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_ba_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_bb( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_bb_double( const mps_ptr, double, double, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_bn( const mps_ptr, const mps_ptr, const mps_ptr, gmp_randstate_t );
__MPS_DECLSPEC int mps_rand_beta_bn_double( const mps_ptr, double, double, gmp_randstate_t );

/* mps_mat_minor.c functions. */
__MPS_DECLSPEC int mps_mat_minor( const mps_ptr, const mps_ptr, const unsigned int, const unsigned int, const mpfr_rnd_t );

/* mps_diag.c functions. */
__MPS_DECLSPEC int mps_diag_set( const mps_ptr, const mps_ptr, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_diag_set_double( const mps_ptr, const double[], const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_diag_get( const mps_ptr, const mps_ptr, const int, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_diag_get_double( const mps_ptr, const double[], const mps_order_t, const unsigned int, const unsigned int, const int, const mpfr_rnd_t );

/* mps_mat_rot.c functions. */
__MPS_DECLSPEC int mps_mat_rotate_cw( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_shift_col( const mps_ptr, const mps_ptr, int, mpfr_rnd_t );
__MPS_DECLSPEC int mps_shift_row( const mps_ptr, const mps_ptr, int, mpfr_rnd_t );
__MPS_DECLSPEC int mps_reflect_x( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_reflect_y( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_reflect_diag_lr( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_reflect_diag_rl( const mps_ptr rop, const mps_ptr op, const mpfr_rnd_t );

/* mps_print.c functions. */
__MPS_DECLSPEC char* mps_print_list( const mps_ptr, const unsigned int );
__MPS_DECLSPEC char* mps_printbuf_overview( const mps_ptr, const unsigned int );
__MPS_DECLSPEC void mps_print_overview( const mps_ptr, const unsigned int );
__MPS_DECLSPEC void mps_print_info( const mps_ptr, const char* );
__MPS_DECLSPEC void mps_print_format( const mps_ptr, const unsigned int, const char, const char );
__MPS_DECLSPEC char* mps_print_format_buf( const mps_ptr, const unsigned int, const char, const char );

/* mps_mat_mul.c functions. */
__MPS_DECLSPEC int mps_mat_mul( const mps_ptr, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_mat_mul_double( const mps_ptr, const mps_ptr, const double[],
        const unsigned int, const mps_order_t );
__MPS_DECLSPEC int mps_double_mat_mul( const mps_ptr, const double[], const unsigned int,
        const mps_order_t order, const mps_ptr );

/* mps_mat_transpose.c functions. */
__MPS_DECLSPEC int mps_full_mat_transpose( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_mat_transpose( mps_ptr );

/* mps_sum.c functions. */
__MPS_DECLSPEC int mps_sum( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_sum_quick( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_sumr( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_sumr_quick( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_sumc( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_sumc_quick( const mps_ptr, const mps_ptr );

/* mps_prod.c functions. */
__MPS_DECLSPEC int mps_prod( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_prodr( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_prodc( const mps_ptr, const mps_ptr );

/* mps_mean.c functions. */
__MPS_DECLSPEC int mps_mean( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_mean_quick( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_meanr( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_meanr_quick( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_meanc( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_meanc_quick( const mps_ptr, const mps_ptr );

/* mps_variance.c functions. */
__MPS_DECLSPEC int mps_variance( const mps_ptr, const mps_ptr, int );
__MPS_DECLSPEC int mps_variancer( const mps_ptr, const mps_ptr, int );
__MPS_DECLSPEC int mps_variancec( const mps_ptr, const mps_ptr, int );

/* mps_stdev.c */
__MPS_DECLSPEC int mps_stdev( const mps_ptr, const mps_ptr, int );
__MPS_DECLSPEC int mps_stdevr( const mps_ptr, const mps_ptr, int );
__MPS_DECLSPEC int mps_stdevc( const mps_ptr, const mps_ptr, int );

/* mps_cumsum.c functions. */
__MPS_DECLSPEC int mps_col_cumsum( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_cumsum( const mps_ptr, const mps_ptr, const mpfr_rnd_t );

/* mps_cumprod.c functions. */
__MPS_DECLSPEC int mps_col_cumprod( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_cumprod( const mps_ptr, const mps_ptr, const mpfr_rnd_t );

/* mps_sort.c functions. */
__MPS_DECLSPEC int mps_heapsort_col( const mps_ptr );
__MPS_DECLSPEC int mps_quicksort_col( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_quicksort_each_col( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_heapsort_row( mps_ptr );
__MPS_DECLSPEC int mps_quicksort_row( mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_reverse( const mps_ptr );
__MPS_DECLSPEC int mps_check_sort( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_check_ordered( const mps_ptr );
__MPS_DECLSPEC int mps_check_each_element( const mps_ptr, const mps_ptr );

/* mps_search.c functions. */
__MPS_DECLSPEC unsigned int mps_linsearch( const mps_ptr, const mpfr_ptr );
__MPS_DECLSPEC unsigned int mps_linsearch_range( const mps_ptr, const mpfr_ptr, const mpfr_ptr );
__MPS_DECLSPEC unsigned int mps_linsearch_all( const mps_ptr, mpfr_ptr, unsigned int** );
__MPS_DECLSPEC unsigned int mps_binsearch_section( const mps_ptr, const mpfr_ptr, unsigned int, unsigned int );
#define mps_binsearch(op, item) mps_binsearch_section(op, item, 1, MPS_SIZE(op))
__MPS_DECLSPEC void mps_binsearch_range_section( const mps_ptr, const mpfr_ptr, const mpfr_ptr, unsigned int, unsigned int, unsigned int* );
#define mps_binsearch_range( op, lower, upper, ret ) mps_binsearch_range_section(op, lower, upper, 1, MPS_SIZE(op), ret)

/* mps_round.c functions. */
__MPS_DECLSPEC int mps_ceil( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_floor( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_round( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_trunc( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_int( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_rint( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rint_ceil( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rint_floor( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rint_round( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_rint_trunc( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_frac( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_modf( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );

/* mps_float.c functions. */
__MPS_DECLSPEC int mps_nextabove( const mps_ptr );
__MPS_DECLSPEC int mps_nextbelow( const mps_ptr );

/* mps_cmp.c functions. */
__MPS_DECLSPEC int mps_equal( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_equal_double( const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_equal_margin( const mps_ptr, const mps_ptr, const mpfr_ptr );
__MPS_DECLSPEC int mps_equal_margin_double( const mps_ptr, const double[], const mps_order_t, const mpfr_ptr );
__MPS_DECLSPEC int mps_double_equal_double( const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_equal_bmargin( const mps_ptr, const mps_ptr, const unsigned int );
__MPS_DECLSPEC int mps_equal_bmargin_double( const mps_ptr, const double[], const mps_order_t, const unsigned int );
__MPS_DECLSPEC int mps_equal_double_bmargin( const double[], const mps_order_t, const mps_ptr, const unsigned int );
__MPS_DECLSPEC int mps_equal_abs( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_cmp( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_cmp_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_cmp( int rop[], const mps_order_t, const double[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_double_cmp_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_equal_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_equal_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_equal_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_lessgreater_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_lessgreater_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_lessgreater_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_greater_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_greater_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_greater_p( int rop[], const mps_order_t, const double[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_double_greater_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_greaterequal_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_greaterequal_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_greaterequal_p( int rop[], const mps_order_t, const double[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_double_greaterequal_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_less_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_less_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_less_p( int rop[], const mps_order_t, const double[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_double_less_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_lessequal_p( int rop[], const mps_order_t, const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_lessequal_p_double( int rop[], const mps_order_t, const mps_ptr, const double[], const mps_order_t );
__MPS_DECLSPEC int mps_double_lessequal_p( int rop[], const mps_order_t, const double[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_double_lessequal_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_nan_p( int rop[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_nan_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_inf_p( int rop[], const mps_order_t, const mps_ptr );
__MPS_DECLSPEC int mps_inf_p_double( int rop[], const mps_order_t, const double[], const mps_order_t, const unsigned int, const unsigned int );

/* mps_minmax.c functions. */
__MPS_DECLSPEC mpfr_ptr mps_max( const mps_ptr, unsigned int*, unsigned int* );
__MPS_DECLSPEC int mps_maxr( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC int mps_maxc( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC mpfr_ptr mps_row_max( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_col_max( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_min( const mps_ptr, unsigned int*, unsigned int* );
__MPS_DECLSPEC int mps_minr( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC int mps_minc( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC mpfr_ptr mps_row_min( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_col_min( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_maxabs( const mps_ptr, unsigned int*, unsigned int* );
__MPS_DECLSPEC int mps_maxabsr( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC int mps_maxabsc( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC mpfr_ptr mps_row_maxabs( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_col_maxabs( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_minabs( const mps_ptr, unsigned int*, unsigned int* );
__MPS_DECLSPEC int mps_minabsr( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC int mps_minabsc( const mps_ptr, const mps_ptr, unsigned int m[] );
__MPS_DECLSPEC mpfr_ptr mps_row_minabs( const mps_ptr, unsigned int*, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_col_minabs( const mps_ptr, unsigned int*, const unsigned int );

/* mps_jacobi.c functions. */
__MPS_DECLSPEC int mps_eigen_jacobi( const mps_ptr, const mps_ptr, const mps_ptr, const unsigned int, unsigned int*, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_eigen_invert_jacobi( const mps_ptr, const mps_ptr, const unsigned int, const mpfr_rnd_t );

/* mps_input.c functions. */
__MPS_DECLSPEC int mps_str_input( const mps_ptr, const char *, const mpfr_prec_t );
__MPS_DECLSPEC int mps_double_input( const mps_ptr, const double*, const mps_order_t, const mpfr_rnd_t );

/* mps_output.c functions. */
__MPS_DECLSPEC int mps_double_output( double*, const mps_ptr, const mps_order_t, const mpfr_rnd_t );

/* mps_epsilon.c functions. */
__MPS_DECLSPEC void mps_epsilon( mpfr_ptr, const mp_prec_t, const mpfr_rnd_t );
__MPS_DECLSPEC void mpfr_maxval( mpfr_ptr );
__MPS_DECLSPEC void mpfr_minval( mpfr_ptr );

/* mps_str_inpout.c functions. */
__MPS_DECLSPEC char* mps_get_str16( const mps_ptr );
__MPS_DECLSPEC int mps_set_str16( const mps_ptr, const char *, const mpfr_prec_t, const mps_order_t, const mpfr_rnd_t );

/* mps_ops.c functions. */
__MPS_DECLSPEC int mps_coord_exg( const mps_ptr, const unsigned int, const unsigned int, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_indx_exg( const mps_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_row_exg( const mps_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_row_scale_mpfr( const mps_ptr, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_scale_double( const mps_ptr, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_div_mpfr( const mps_ptr, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_div_double( const mps_ptr, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_add_mpfr( const mps_ptr, const unsigned int, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_add_double( const mps_ptr, const unsigned int, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_sub_mpfr( const mps_ptr, const unsigned int, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_row_sub_double( const mps_ptr, const unsigned int, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_exg( const mps_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC int mps_col_scale_mpfr( const mps_ptr, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_scale_double( const mps_ptr, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_div_mpfr( const mps_ptr, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_div_double( const mps_ptr, const unsigned int, const double, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_add_mpfr( const mps_ptr, const unsigned int, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_col_add_double( const mps_ptr, const unsigned int, const unsigned int, const double, const mpfr_rnd_t );

/* mps_det.c functions. */
__MPS_DECLSPEC int mps_det_gauss_dest( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_gauss2_dest( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_gauss3_dest( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_gauss( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_gauss2( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_gauss3( const mpfr_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_det_iplup( const mps_ptr, const mps_ptr );

/* mps_trace.c functions. */
__MPS_DECLSPEC int mps_trace( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_trace_double( const mps_ptr, const double[], const mps_order_t, const unsigned int );
__MPS_DECLSPEC int mps_trace_quick( const mps_ptr, const mps_ptr );

/* mps_lu.c functions. */
__MPS_DECLSPEC int mps_lu_decomp( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_lu_decomp2( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_iplu_decomp( const mps_ptr );
__MPS_DECLSPEC int mps_iplup_decomp( const mps_ptr );

/* mps_inv.c functions. */
__MPS_DECLSPEC int mps_inv( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_inv2( const mps_ptr, const mps_ptr );
__MPS_DECLSPEC int mps_inv_ipgje( const mps_ptr );
__MPS_DECLSPEC int mps_inv_ipgjep( const mps_ptr );

/* mps_map.c functions. */
__MPS_DECLSPEC int mps_map( const mps_ptr, const mps_ptr, const MPFunc, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_map2( const mps_ptr, const mps_ptr, const mps_ptr, const MPFunc2, const mpfr_rnd_t );

/* mps_block.c functions. */
__MPS_DECLSPEC mpfr_ptr mps_get_ele_blk( const mps_blk_ptr, unsigned int, unsigned int);
__MPS_DECLSPEC mpfr_ptr mps_get_ele_seq_blk( const mps_blk_ptr, unsigned int);
__MPS_DECLSPEC mpfr_ptr mps_get_ele_col_blk( const mps_blk_ptr, unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_get_ele_row_blk( const mps_blk_ptr, unsigned int );
__MPS_DECLSPEC void mps_set_ele_seq_blk( const mps_blk_ptr, unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_submat( mps_blk_ptr, const mps_ptr, const unsigned int, const unsigned int, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_subsubrow( mps_blk_ptr, const mps_blk_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_subsubslice( mps_blk_ptr, const mps_blk_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_full_submat( mps_blk_ptr, const mps_ptr );
__MPS_DECLSPEC void mps_subsubmat( mps_blk_ptr, const mps_blk_ptr, const unsigned int, const unsigned int, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_row_blk( mps_blk_ptr, const mps_ptr, const unsigned int );
__MPS_DECLSPEC char* mps_printbuf_overview_blk( const mps_blk_ptr, const unsigned int );
__MPS_DECLSPEC char* mps_print_seq_blk( const mps_blk_ptr, const unsigned int );
__MPS_DECLSPEC char* mps_print_col_blk( const mps_blk_ptr, const unsigned int );
__MPS_DECLSPEC char* mps_print_row_blk( const mps_blk_ptr, const unsigned int );
__MPS_DECLSPEC void mps_copy_blk( const mps_blk_ptr, const mps_blk_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_copy_real_blk( const mps_ptr, const mps_blk_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_copy_blk_real( const mps_blk_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_swap_blk( const mps_blk_ptr, const mps_blk_ptr );
__MPS_DECLSPEC void mps_col_exg_blk( const mps_blk_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_scale_blk( const mps_blk_ptr, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_print_info_blk( const mps_blk_ptr, const char* );
__MPS_DECLSPEC void mps_print_overview_blk( const mps_blk_ptr, const unsigned int );
__MPS_DECLSPEC void mps_dot_product_blk( const mpfr_ptr, const mps_blk_ptr, const mps_blk_ptr, const mpfr_rnd_t );

/* mps_vec.c functions. */
__MPS_DECLSPEC int mps_vec_length( const mpfr_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_dot_product( const mpfr_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC int mps_unit_vector( const mps_ptr, const mps_ptr, const mpfr_rnd_t );

__MPS_DECLSPEC void mps_subvec( mps_vec_ptr, const mps_ptr, const unsigned int );
__MPS_DECLSPEC void mps_subsubvec( mps_vec_ptr, const mps_vec_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_subvec_partial( mps_vec_ptr, mps_ptr, unsigned int, const unsigned int, const unsigned int );
__MPS_DECLSPEC mpfr_ptr mps_get_ele_vec( const mps_vec_ptr, const unsigned int );
__MPS_DECLSPEC void mps_set_ele_vec( const mps_vec_ptr, const unsigned int, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC mps_vec_ptr mps_new_vec( const unsigned int, const mpfr_prec_t );
__MPS_DECLSPEC void mps_destroy_vec( mps_vec_ptr );
__MPS_DECLSPEC void mps_copy_vec( const mps_vec_ptr, const mps_vec_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_copy_vec_from_blk( const mps_vec_ptr, const mps_blk_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC mps_vec_ptr mps_vec_new_clone( const mps_vec_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_indx_exg_vec( const mps_vec_ptr, const unsigned int, const unsigned int );
__MPS_DECLSPEC void mps_scale_vec( const mps_vec_ptr, const mpfr_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_fill_seq_si_vec( const mps_vec_ptr, const int, const int, const mpfr_rnd_t );

/* mps_bidiag.c functions. */
__MPS_DECLSPEC void mps_bidiag_decomp( const mps_ptr, const mps_vec_ptr, const mps_vec_ptr, const mpfr_rnd_t );

/* mps_qr.c functions */
__MPS_DECLSPEC void mps_qr_decomp( const mps_ptr, const mps_vec_ptr, const mpfr_rnd_t );

/* mps_householder.c functions. */
__MPS_DECLSPEC void mps_householder_transform( const mpfr_ptr, const mps_vec_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_householder_hm( const mpfr_ptr, const mps_vec_ptr, const mps_blk_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_householder_mh( const mpfr_ptr, const mps_vec_ptr, const mps_blk_ptr, const mpfr_rnd_t );

/* mps_fft.c functions. */
__MPS_DECLSPEC void mps_real_fft_dit( const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_real_dst( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_real_dct( const mps_ptr, const mps_ptr, const mpfr_rnd_t );
__MPS_DECLSPEC void mps_real_dft( const mps_ptr, const mps_ptr, const mps_ptr, const char, const mpfr_rnd_t );

/* mps_convolve.c functions. */
__MPS_DECLSPEC void mps_convolution( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
#if 0
__MPS_DECLSPEC void mps_convolution2d( const mps_ptr, const mps_ptr, const mps_ptr, const mpfr_rnd_t );
#endif

/* mps_correlate.c functions. */
__MPS_DECLSPEC void mps_correlation( const mps_ptr, const mps_ptr, const mps_ptr, const unsigned int, const mpfr_rnd_t );

/* mps_cholesky.c functions. */
__MPS_DECLSPEC void mps_cholesky_decomp( const mps_ptr, const mpfr_rnd_t );

/* mps_identify.c functions. */
__MPS_DECLSPEC int mps_is_scalar( const mps_ptr );
__MPS_DECLSPEC int mps_is_square( const mps_ptr );
__MPS_DECLSPEC int mps_is_col_vector( const mps_ptr );
__MPS_DECLSPEC int mps_is_row_vector( const mps_ptr );
__MPS_DECLSPEC int mps_is_zeros( const mps_ptr );
__MPS_DECLSPEC int mps_is_ones( const mps_ptr );

/* mps_io.c function. */
__MPS_DECLSPEC int mps_save( FILE*, const mps_ptr );
__MPS_DECLSPEC int mps_load( const mps_ptr, FILE* );
__MPS_DECLSPEC int mps_write_header( FILE*, const mps_ptr );
__MPS_DECLSPEC int mps_read_header( FILE*, mps_file_struct* );
__MPS_DECLSPEC int mps_write_flat( FILE*, const mps_ptr );
__MPS_DECLSPEC int mps_read_flat( const mps_ptr, FILE*, mps_file_struct* );
__MPS_DECLSPEC mps_endian_t mps_get_endianness();
__MPS_DECLSPEC mps_uint32_t mps_crc32( mps_uint32_t, unsigned char *, size_t );
__MPS_DECLSPEC void mps_swap_to_native( mps_uint64_t*, mps_uint64_t*, mps_endian_t, unsigned int );

#endif /* __MPS_H__ */
