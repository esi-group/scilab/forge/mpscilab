/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#define MPT_TEST( n, prec )             ( n - n + 4 )
#define MPT_CONST( n, prec, a )         mpt_const( a )
#define MPT_GLOBAL()                    mpt_global()
#define MPT_LINEAR( n, prec, a, b, c )  mpt_linear( n, prec, a, b, c )

/* mps_add.c */
#define MPT_mps_add( n, prec )                      MPT_GLOBAL()
#define MPT_mps_add_scalar( n, prec )               MPT_GLOBAL()
#define MPT_mps_add_scalar_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_add_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_add_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_add_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_double_add_scalar_double( n, prec ) MPT_GLOBAL()

/* mps_sub.c */
#define MPT_mps_sub( n, prec )                      MPT_GLOBAL()
#define MPT_mps_sub_scalar( n, prec )               MPT_GLOBAL()
#define MPT_mps_scalar_sub( n, prec )               MPT_GLOBAL()
#define MPT_mps_sub_scalar_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_sub_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_double_sub( n, prec )        MPT_GLOBAL()
#define MPT_mps_double_sub_scalar( n, prec )        MPT_GLOBAL()
#define MPT_mps_sub_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_sub( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_sub_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_double_sub_double( n, prec ) MPT_GLOBAL()
#define MPT_mps_double_sub_scalar_double( n, prec ) MPT_GLOBAL()

/* mps_mul.c */
#define MPT_mps_mul( n, prec )                      MPT_LINEAR( n, prec, 500, 500, 0 )
#define MPT_mps_mul_scalar( n, prec )               MPT_LINEAR( n, prec, 500, 500, 0 )
#define MPT_mps_mul_scalar_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_mul_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_mul_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_mul_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_double_mul_scalar_double( n, prec ) MPT_GLOBAL()

/* mps_div.c */
#define MPT_mps_div( n, prec )                      MPT_GLOBAL()
#define MPT_mps_div_scalar( n, prec )               MPT_GLOBAL()
#define MPT_mps_scalar_div( n, prec )               MPT_GLOBAL()
#define MPT_mps_div_scalar_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_div_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_double_div( n, prec )        MPT_GLOBAL()
#define MPT_mps_double_div_scalar( n, prec )        MPT_GLOBAL()
#define MPT_mps_div_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_div( n, prec )               MPT_GLOBAL()
#define MPT_mps_double_div_double( n, prec )        MPT_GLOBAL()
#define MPT_mps_scalar_double_div_double( n, prec ) MPT_GLOBAL()
#define MPT_mps_double_div_scalar_double( n, prec ) MPT_GLOBAL()

/* mps_exp.c */
#define MPT_mps_exp( n, prec )                      MPT_GLOBAL()
#define MPT_mps_exp_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_exp2( n, prec )                     MPT_GLOBAL()
#define MPT_mps_exp2_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_exp10( n, prec )                    MPT_GLOBAL()
#define MPT_mps_exp10_double( n, prec )             MPT_GLOBAL()

/* mps_log.c */
#define MPT_mps_log( n, prec )                      MPT_GLOBAL()
#define MPT_mps_log_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_log2( n, prec )                     MPT_GLOBAL()
#define MPT_mps_log2_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_log10( n, prec )                    MPT_GLOBAL()
#define MPT_mps_log10_double( n, prec )             MPT_GLOBAL()

/* mps_trig.c */
#define MPT_mps_sin( n, prec )                      MPT_GLOBAL()
#define MPT_mps_sin_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_cos( n, prec )                      MPT_GLOBAL()
#define MPT_mps_cos_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_tan( n, prec )                      MPT_GLOBAL()
#define MPT_mps_tan_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_csc( n, prec )                      MPT_GLOBAL()
#define MPT_mps_csc_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_sec( n, prec )                      MPT_GLOBAL()
#define MPT_mps_sec_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_cot( n, prec )                      MPT_GLOBAL()
#define MPT_mps_cot_double( n, prec )               MPT_GLOBAL()
#define MPT_mps_asin( n, prec )                     MPT_GLOBAL()
#define MPT_mps_asin_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_acos( n, prec )                     MPT_GLOBAL()
#define MPT_mps_acos_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_atan( n, prec )                     MPT_GLOBAL()
#define MPT_mps_atan_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_sinh( n, prec )                     MPT_GLOBAL()
#define MPT_mps_sinh_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_cosh( n, prec )                     MPT_GLOBAL()
#define MPT_mps_cosh_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_tanh( n, prec )                     MPT_GLOBAL()
#define MPT_mps_tanh_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_csch( n, prec )                     MPT_GLOBAL()
#define MPT_mps_csch_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_sech( n, prec )                     MPT_GLOBAL()
#define MPT_mps_sech_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_coth( n, prec )                     MPT_GLOBAL()
#define MPT_mps_coth_double( n, prec )              MPT_GLOBAL()
#define MPT_mps_asinh( n, prec )                    MPT_GLOBAL()
#define MPT_mps_asinh_double( n, prec )             MPT_GLOBAL()
#define MPT_mps_acosh( n, prec )                    MPT_GLOBAL()
#define MPT_mps_acosh_double( n, prec )             MPT_GLOBAL()
#define MPT_mps_atanh( n, prec )                    MPT_GLOBAL()
#define MPT_mps_atanh_double( n, prec )             MPT_GLOBAL()

/* mps_mat_mul.c */
#define MPT_mps_mat_mul( n, prec )                  MPT_GLOBAL()
#define MPT_mps_mat_mul_double( n, prec )           MPT_GLOBAL()
#define MPT_mps_double_mat_mul( n, prec )           MPT_GLOBAL()

