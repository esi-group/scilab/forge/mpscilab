/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 * Copyright (C) 2009 - Matthew Arsenault
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#ifndef __MPS_INCL_H__
#define __MPS_INCL_H__

#ifdef _MSC_VER
#define __func__ __FUNCTION__
#endif

#ifdef _MSC_VER
    #include <float.h>

    #define isnan(x) _isnan(x)
    #define isinf(x) (!_finite(x))
#endif

#ifdef HAVE_STD_INT_H
    #include <stdint.h>
#endif

#include "mps_config.h"

#ifdef WITH_OPENMP
    #include <omp.h>
#endif

#ifdef HAVE_STD_ARG
    #include <stdarg.h>
#endif

#ifdef HAVE_VAR_ARG
    #include <varargs.h>
#endif

#include "mps.h"
#include "mps_assert.h"

#define __MPS_WITHIN_MPS 1

/* mps_svdstep.c private functions */
void chop_small_elements( mps_vec_ptr, mps_vec_ptr, mpfr_rnd_t );
void qrstep( mps_vec_ptr, mps_vec_ptr, mps_blk_ptr, mps_blk_ptr, mpfr_rnd_t );
void mps_nrm2_blk( mpfr_ptr, const mps_blk_ptr, mpfr_rnd_t );

/* Include the inlined function definitions */
#ifdef WITH_C99_INLINE
    #include "mps_inline.h"
#endif

#ifdef WITH_OPENMP
    #define GET_NUMTHR(n, x) { \
    const unsigned int _size = MPS_SIZE(x); \
    n = mps_get_num_threads(); \
	if ( n >= _size ) n = (_size >> 1) | 1; } \
/* we normally don't  want it to be whole size.
   Most functions, just 1 per thread isn't worth it.
   Divide, and avoid div by 0 */
#else
    #define GET_NUMTHR(a, b) ((void) 0);
#endif

#ifdef WITH_OPENMP
    #include "mps_tune.h"
#endif

#ifdef WITH_OPENMP
    #define GET_NUM_THREADS( func, n, prec ) MPT_##func( n, prec )
#else
    #define GET_NUM_THREADS( func, n, prec ) ((void) 0)
#endif

#if 0
#define MPS_mps_mul_scalar( n ) 4
#define MPS_mps_mul( n ) 4
#define GET_NUM_THREADS( func, n ) MPS_##func( n )
#endif

/* Global variable used to determine the wanted number of threads. */
extern unsigned int g_num_threads;

#endif /* __MPS_INCL_H__ */

