# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check if the compiler supports overlength strings.


FUNCTION(CHECK_OVERLENGTH_WARN_FLAG)

    IF(DEFINED OVERLENGTH_WARN_FLAG)
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking for the overlength strings warning disable flag")

    TRY_COMPILE(OS_CHECK ${PROJECT_BINARY_DIR} ${PROJECT_SOURCE_DIR}/cmake_modules/dummymain.c
                    CMAKE_FLAGS -DCMAKE_C_FLAGS:STRING="-Wno-overlength-strings")
    IF(OS_CHECK)
        SET(OVERLENGTH_WARN_FLAG "-Wno-overlength-strings" CACHE STRING "Flag to disable overlength strings warning.")
        MESSAGE(STATUS "Checking for the overlength strings warning disable flag - Wno-overlength-strings")
    ELSE()
        SET(OVERLENGTH_WARN_FLAG " " CACHE STRING "Flag to disable overlength strings warning.")
        MESSAGE(STATUS "Checking for the overlength strings warning disable flag - unknown")
    ENDIF()

    MARK_AS_ADVANCED(OVERLENGTH_WARN_FLAG)

ENDFUNCTION()
