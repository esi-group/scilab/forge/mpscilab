/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Source that check for C99 style inlining support. */

/*
 * Function foo is declared inline in "c99inline.h" but must also be defined
 * non-inline in another translation unit. A C99 compiler is free to choose
 * to use the inline function or refer to the implicit extern function.
 */

#include "c99inline.h"

int main(int argc, char *argv[])
{
    /* Dummy stuff just in case. */
    if( argc > 100 )
        return foo(argv[1]);

    return 0;
}