# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check if the compiler supports overlength strings.


FUNCTION(CHECK_OVERLENGTH_STRINGS)

    IF( DEFINED HAVE_OVERLENGTH_STRINGS )
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking if compiler supports overlength strings")

    TRY_COMPILE( OS_CHECK ${PROJECT_BINARY_DIR} ${PROJECT_SOURCE_DIR}/cmake_modules/strinitlen.c )

    IF(OS_CHECK)
        MESSAGE(STATUS "Checking if compiler supports overlength strings - yes")
        SET(HAVE_OVERLENGTH_STRINGS 1 CACHE BOOL "Support for overlength strings.")
    ELSE()
        MESSAGE(STATUS "Checking if compiler supports overlength strings - no")
        SET(HAVE_OVERLENGTH_STRINGS 0 CACHE BOOL "Support for overlength strings.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_OVERLENGTH_STRINGS)
 
ENDFUNCTION()
