/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <stdio.h>
#include <mpfr.h>

int main()
{
	mpfr_t x;
	mpfr_init2(x, 100);
	mpfr_set_ui(x, 12345, GMP_RNDN);
	mpfr_add(x, x, x, GMP_RNDN);

	if( mpfr_cmp_ui(x, 24690) != 0 )
	{
		fprintf(stderr, "Error : Wrong calculation result.");
		return 1;
	}
     
	mpfr_clear(x);

	return 0;
}
