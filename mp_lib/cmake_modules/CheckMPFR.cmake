# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Verify the functionality of MPFR.
# Must be called after FIND_PACKAGE(MPFR).


FUNCTION(CHECK_MPFR)

	IF(MPFR_CHECK)
		RETURN()
	ENDIF()

	MESSAGE(STATUS "Checking MPFR")

	IF(NOT LIBMPFR_FOUND)
		MESSAGE(STATUS "Error : No MPFR package present, MPFR_FOUND is not defined")
		MESSAGE(SEND_ERROR "Maintainers, make sure to call FIND_PACKAGE(MPFR) before CHECK_MPFR()")
		RETURN()
	ENDIF()

	TRY_RUN(MPFR_CHECK_RUN MPFR_CHECK_COMPILE ${PROJECT_BINARY_DIR}/Checks ${PROJECT_SOURCE_DIR}/cmake_modules/mpfrtest.c
		CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=${LIBGMP_LIB};${LIBMPFR_LIB}" 
          "-DINCLUDE_DIRECTORIES:STRING=${LIBMPFR_INCLUDE_DIR};${LIBGMP_INCLUDE_DIR}"
		COMPILE_OUTPUT_VARIABLE MPFR_CHECK_COMPILE_OUT
		RUN_OUTPUT_VARIABLE MPFR_CHECK_RUN_OUT)

	IF(MPFR_CHECK_COMPILE)
		MESSAGE(STATUS "MPFR compilation test successful")
	ELSE()
		MESSAGE(STATUS "MPFR compilation test failed")
		MESSAGE(STATUS "Error : Cannot compile an MPFR application")
		MESSAGE(SEND_ERROR "Compilation output : \n ${MPFR_CHECK_COMPILE_OUT}")
	ENDIF()

	IF(MPFR_CHECK_COMPILE)
		IF(NOT CMAKE_CROSSCOMPILING)
			IF(MPFR_CHECK_RUN EQUAL 0)
				MESSAGE(STATUS "MPFR run test successful.")
				SET(MPFR_CHECK 1 CACHE BOOL "MPFR test successful")
			ELSEIF(MP_CHECK_RUN EQUAL 1)
				MESSAGE(STATUS "MPFR run test failed")
				MESSAGE(STATUS "MPFR calculation error. Might signify a broken library")
			ELSE()
				MESSAGE(STATUS "MPFR run test failed.")
				MESSAGE(STATUS "Warning : Cannot run an MPFR application. Might signify a broken library")
				MESSAGE(SEND_ERROR "Run output : \n ${MPFR_CHECK_RUN_OUT}")
			ENDIF()
		ELSE()
			MESSAGE(STATUS "MPFR run test skipped, cross-compiling")
		ENDIF()
	ENDIF()

	MARK_AS_ADVANCED(MPFR_CHECK)
	
ENDFUNCTION()
