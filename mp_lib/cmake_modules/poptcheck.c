/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* Check if we have libpopt. */
#include <popt.h>

int main(int argc, char *argv[])
{
    poptContext context;
    static int dummy1, dummy2;

    static const struct poptOption options[] =
    {
        { "dummy1", 's', POPT_ARG_INT, &dummy1, 's', "Dummy arg 1", NULL },
        { "dummy2", 'r', POPT_ARG_INT, &dummy2, 'r', "Dummy arg 2", NULL },
        POPT_AUTOHELP
        { 0, 0 }
    };
    
    if( argc > 100 )
        return 2;

    return 0;
}

