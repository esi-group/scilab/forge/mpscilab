# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Verify the functionality of MPFR.
# Must be called after FIND_PACKAGE(MPFR).


FUNCTION(CHECK_MPFR_VER)

	IF(MPFR_CHECK_VER)
		RETURN()
	ENDIF()

	MESSAGE(STATUS "Checking if the MPFR library version is >= 2.4.0")

	IF(NOT LIBMPFR_FOUND)
		MESSAGE(STATUS "Error : No MPFR package present, MPFR_FOUND is not defined")
		MESSAGE(SEND_ERROR "Maintainers, make sure to call FIND_PACKAGE(MPFR) before CHECK_MPFR_VER()")
		RETURN()
	ENDIF()

	TRY_RUN(MPFR_CHECK_RUN MPFR_CHECK_COMPILE ${PROJECT_BINARY_DIR}/Checks ${PROJECT_SOURCE_DIR}/cmake_modules/mpfrcheckver.c
		CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=${LIBGMP_LIB};${LIBMPFR_LIB}"
          "-DINCLUDE_DIRECTORIES:STRING=${LIBMPFR_INCLUDE_DIR};${LIBGMP_INCLUDE_DIR}"
		COMPILE_OUTPUT_VARIABLE MPFR_CHECK_COMPILE_OUT
		RUN_OUTPUT_VARIABLE MPFR_CHECK_RUN_OUT)

	IF(MPFR_CHECK_COMPILE)
		MESSAGE(STATUS "Checking if the MPFR library version is >= 2.4.0 - yes")
        SET(MPFR_CHECK_VER 1 CACHE BOOL "MPFR version info is correct")
	ELSE()
		MESSAGE(STATUS "Checking if the MPFR library version is >= 2.4.0 - no")
		MESSAGE(SEND_ERROR "Version 2.4.0 or superior of MPFR is required.")
	ENDIF()

	MARK_AS_ADVANCED(MPFR_CHECK_VER)

ENDFUNCTION()
