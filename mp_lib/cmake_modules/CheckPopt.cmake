# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check if popt is available.

FUNCTION(CHECK_POPT)

    IF(DEFINED HAVE_POPT)
        RETURN()
    ENDIF()

    CHECK_INCLUDE_FILE(popt.h HAVE_POPT_H)

    IF(HAVE_POPT_H)
        MESSAGE(STATUS "Checking if popt is available")

        TRY_COMPILE(POPT_CHECK ${PROJECT_BINARY_DIR}/Checks/poptcheck ${PROJECT_SOURCE_DIR}/cmake_modules/poptcheck.c
                        CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=popt")
    ENDIF()

    IF(POPT_CHECK)
        SET(HAVE_POPT 1 CACHE BOOL "Set if popt is available.")
        MESSAGE(STATUS "Checking if popt is available - yes")
    ELSEIF(HAVE_POPT_H)
        SET(HAVE_POPT 0 CACHE BOOL "Set if popt is available.")
        MESSAGE(STATUS "Checking if popt is available - error")
        MESSAGE(STATUS "Warning : Unable to build a popt application. Benchmark programs will not build correctly.")
    ELSE()
        SET(HAVE_POPT 0 CACHE BOOL "Set if popt is available.")
        MESSAGE(STATUS "Checking if popt is available - no")
        MESSAGE(STATUS "Warning : Unable to build a popt application. Benchmark programs will not build correctly.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_POPT)

ENDFUNCTION()
