# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check if the compiler supports C99 inlining.

FUNCTION(CHECK_C99_INLINE)

    IF(NOT HAVE_C99_SUPPORT)
        RETURN()
    ENDIF()

    IF(DEFINED HAVE_C99_INLINE)
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking if the C compiler accepts C99 inlining by default")

    TRY_COMPILE(C99_INLINE_CHECK ${PROJECT_BINARY_DIR}/Checks/c99inline ${PROJECT_SOURCE_DIR}/cmake_modules/c99inline c99inlinecheck )

    IF(C99_INLINE_CHECK)
        SET(HAVE_C99_INLINE 1 CACHE STRING "Status of C99 inlining support.")
        MESSAGE(STATUS "Checking if the C compiler accepts C99 inlining by default - yes")
    ELSE()
        MESSAGE(STATUS "Checking if the C compiler accepts C99 inlining by default - no")
    ENDIF()

    IF(NOT C99_INLINE_CHECK)
        MESSAGE(STATUS "Checking if the C compiler needs -std=std99 for C99 inlining")

        TRY_COMPILE(C99_INLINE_CHECK ${PROJECT_BINARY_DIR}/Checks/c99inline2
                    ${PROJECT_SOURCE_DIR}/cmake_modules/c99inline c99inlinecheck
                    CMAKE_FLAGS -DCMAKE_C_FLAGS:STRING="-std=c99")

        IF(C99_INLINE_CHECK)
            SET(HAVE_C99_INLINE 1 CACHE STRING "Status of C99 inlining support.")

            IF(NOT DISABLE_C99_INLINE)
                SET(C99_FLAG "-std=c99" CACHE STRING "c99 activation flag, defined if required")
            ENDIF()

            MARK_AS_ADVANCED(C99_FLAG)
            MESSAGE(STATUS "Checking if the C compiler needs -std=std99 for C99 inlining - yes")
        ELSE()
            SET(HAVE_C99_INLINE 0 CACHE STRING "Status of C99 inlining support.")
            MESSAGE(STATUS "Checking if the C compiler needs -std=std99 for C99 inlining - no")
        ENDIF()
    ENDIF()

    MARK_AS_ADVANCED(HAVE_C99_INLINE)

ENDFUNCTION()
