# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check if ieeefp.h is required for some operations on floats.

FUNCTION(CHECK_IEEEFP)

    INCLUDE(CheckIncludeFile)

    IF(DEFINED NEED_IEEEFP_H)
        RETURN()
    ENDIF()

    CHECK_INCLUDE_FILE(ieeefp.h IEEEFP_H)

    IF(IEEEFP_H)
        MESSAGE(STATUS "Checking if ieeefp.h is required")

        TRY_COMPILE(IEEEFP_CHECK ${PROJECT_BINARY_DIR}/Checks/ieeecheck ${PROJECT_SOURCE_DIR}/cmake_modules/ieeefpcheck.c
                        CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=m")

        IF(NOT IEEEFP_CHECK)
            TRY_COMPILE(IEEEFP_CONFIRM ${PROECT_BINARY_DIR}/Checks/ieeeconfirm ${PROJECT_SOURCE_DIR}/cmake_modules/ieeefpconfirm.c
                            CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=m")

            IF(IEEEFP_CONFIRM)
                MESSAGE(STATUS "Checking if ieeefp.h is required - yes")
                SET(NEED_IEEEFP_H 1 CACHE BOOL "Set if ieeefp.h is needed.")
            ELSE()
                MESSAGE(STATUS "Checking if ieeefp.h is required - error")
            ENDIF()
        ELSE()
            MESSAGE(STATUS "Checking if ieeefp.h is required - no")
            SET(NEED_IEEEFP_H 0 CACHE BOOL "Set if ieeefp.h is needed.")
        ENDIF()
    ENDIF()

    MARK_AS_ADVANCED(NEED_IEEEFP_H)

ENDFUNCTION()
