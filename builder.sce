// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

lines(0);

TOOLBOX_NAME = 'MPScilab';
TOOLBOX_TITLE = 'MPScilab';
tbx_src_dir = get_absolute_file_path("builder.sce");

sh_ret = unix("cmake --version");

if(sh_ret <> 0)
    error("Cannot execute CMAKE.");
end

cd TMPDIR;

if(getos() == "Windows")
    sci_path = SCI;
    tbx_src_dir = getshortpathname(tbx_src_dir);
else
    sci_path = SCI + "/../../";
end

cmake_arg = "-DCMAKE_INSTALL_PREFIX:PATH=" + tbx_src_dir + " ";
cmake_arg = cmake_arg + "-DCOPY_GMP_MPFR=ON ";
cmake_arg = cmake_arg + "-DENABLE_OPENMP=OFF ";
cmake_arg = cmake_arg + "-DWITH_ASSERT_CHECKS=ON ";
cmake_arg = cmake_arg + "-DSCILAB_DIR=" + sci_path + " ";
cmake_arg = cmake_arg + tbx_src_dir;

sh_ret = unix("cmake " + cmake_arg);

if(sh_ret <> 0)
    error("Error configuring MPScilab.");
end

sh_ret = unix("cmake --build " + TMPDIR + " --target scimps --config Release");

if(sh_ret <> 0)
    error("Error building MPScilab.");
end

sh_ret = unix("cmake --build " + TMPDIR + " --target install --config Release");

if(sh_ret <> 0)
    error("Error building MPScilab.");
end

cd(tbx_src_dir);
clear sh_ret tbx_src_dir;
