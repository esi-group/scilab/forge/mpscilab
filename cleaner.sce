// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

tbx_src_dir = get_absolute_file_path("cleaner.sce");

if isdir(tbx_src_dir + "/jar") then
    rmdir(tbx_src_dir + "/jar", "s");
end

if isdir(tbx_src_dir + "/lib") then
    rmdir(tbx_src_dir + "/lib", "s");
end

if isdir(tbx_src_dir + "/macros") then
    rmdir(tbx_src_dir + "/macros", "s");
end

if isdir(tbx_src_dir + "/include") then
    rmdir(tbx_src_dir + "/include", "s");
end

if isdir(tbx_src_dir + "/etc") then
    rmdir(tbx_src_dir + "/etc", "s");
end

if isdir(tbx_src_dir + "/tests") then
    rmdir(tbx_src_dir + "/tests", "s");
end

if isfile(tbx_src_dir + "/loader.sce") then
    mdelete(tbx_src_dir + "/loader.sce");
end

if isfile(tbx_src_dir + "/DESCRIPTION") then
    mdelete(tbx_src_dir + "/DESCRIPTION");
end