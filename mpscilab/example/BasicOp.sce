// Multi precision toolbox for Scilab
// Copyright (C) 2010 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Demonstrates some basix matrix operations.
// This example use the full functions names instead
// of the possible overloaded operators.


// Normal double matrix for use in some double/mps operations.
A = [ 1, 2, 3, 4;
      5, 6, 7, 8;
      9, 10, 11, 12;
      13, 14, 15, 16];
      
B = [ 1  2  3  4; 
      0  1  7  8; 
      0  0  1  12; 
      0  0  0  1 ];
      
C = [ 1  0  0  0; 
      5  6  0  0; 
      9  10 11 0; 
      13 14 15 16 ];
      
// Some mps matrices defined from the previous double ones,
// the second arguments is the precision in bits.
mpsA = mps_init2( A, 100 );
mpsB = mps_init2( B, 100 );
mpsC = mps_init2( C, 100 );

// Creation of blank mps matrix. All elements will be 0.
mpsRES = mps_init( 4, 4, 100 );

// Some scalar values.
mpsScalarA = mps_init2( [3], 100 );
mpsScalarB = mps_init2( [3.1416], 100 );

// Element-wise operations :

// mps matrix to mps matrix operations.
// Note that mps_mul and mps_div are element-wise operations,
// this naming may change in the future.
mps_add( mpsRES, mpsA, mpsB );

// Operations between a multi-precision matrix and a standard
// double matrix are also possible.
mps_sub( mpsRES, mpsA, A );

// Also if one of the argument is a scalar the operation will be
// executed on all the elements of the non-scalar operand.
mps_mul( mpsRES, mpsA, mpsScalarA );

// Obviously a scalar double would work also.
mps_div( mpsRES, mpsA, 4 );

// Finally all of those can be done using two native double input operands.
// This saves the cost of performing an explicit conversion.
mps_add( mpsRES, A, B );

// Without using the overloaded operators, elements can be read or written
// with the set and get functions.
mps_get_ele( mpsScalarB, mpsRES, 3, 2 );
mps_set_ele( mpsRES, mpsScalarB, 2, 3 );

// mps_todouble can be used to convert an mps matrix to a Scilab matrix of double.
A = mps_todouble(mpsRES);
