// Multi precision toolbox for Scilab
// Copyright (C) 2010 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Demonstrate/test matrix multiplication.


// Normal double matrix for testing.
A = [ 1, 2, 3, 4;
      5, 6, 7, 8;
      9, 10, 11, 12;
      13, 14, 15, 16];
      
B = [ 1  2  3  4; 
      0  1  7  8; 
      0  0  1  12; 
      0  0  0  1 ];
      
C = [ 1  0  0  0; 
      5  6  0  0; 
      9  10 11 0; 
      13 14 15 16 ];
      
// Some mps matrices defined from the previous double ones,
// the second arguments is the precision in bits.
mpsA = mps_init2( A, 100 );
mpsB = mps_init2( B, 100 );
mpsC = mps_init2( C, 100 );

// Blank matrix for the result.
mpsRES = mps_init( 4, 4, 100 );

// Simple matrix multiplication AXB.
mps_mat_mul( mpsRES, mpsA, mpsB );

// Same with the second arguement as a double matrix.
mps_mat_mul( mpsRES, mpsA, B );

// Now the first.
mps_mat_mul( mpsRES, A, mpsB );