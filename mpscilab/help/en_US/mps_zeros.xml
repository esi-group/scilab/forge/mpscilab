<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mps_zeros" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>mps_zeros</refname>

    <refpurpose>Create and initialize a multi-precision matrix filled with zeros</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>rop = mps_zeros( prec )
    rop = mps_zeros( m1, n1, prec )
    mps_zeros( rop )</synopsis>

    <para>Overloaded usage:</para>
    <synopsis>rop = zeros( op )</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>rop, op</term>

        <listitem>
          <para>multi-precision matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>m1, n1, prec</term>

        <listitem>
          <para>integers</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Create and initialize a multri-precision matrix filled with the value
    0</para>

    <variablelist>
      <varlistentry>
        <term>mps_zeros( prec )</term>

        <listitem>
          <para>Create and initialize a scalar of precision 
          <literal>prec</literal> initialized to 0.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = mps_zeros( m1, n1, prec )</term>

        <listitem>
          <para>Create and initialize a matrix of size <literal>(m1,n1)</literal> 
          with precision <literal>prec</literal> filled with zeros.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = mps_zeros( rop )</term>

        <listitem>
          <para>Set every element of <literal>rop</literal> to 0.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = zeros( op )</term>

        <listitem>
          <para>Overloaded Scilab primitive zeros(). Create a multi-precision
          matrix the same size and precision as <literal>op</literal> 
          filled with zeros.</para>
        </listitem>
      </varlistentry>
    </variablelist>

  </refsection>

  <refsection>
    <title>Restrictions</title>

    <para>The precision must be two bits or higher.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
mpsA = mps_zeros(100)
mpsA = mps_zeros(10,10,100)

//Set every elements of mpsB to 1.
mpsB = mps_init(10,10,100)
mps_zeros(mpsB)

//Overloaded usage
mpsA = zeros(mpsB)
 ]]></programlisting>
  </refsection>

  <refsection role="see also">
<title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="mps_init">mps_init</link></member>

      <member><link linkend="mps_zeros">mps_ones</link></member>
    </simplelist>
  </refsection>
</refentry>
