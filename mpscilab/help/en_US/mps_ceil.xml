<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mps_ceil" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>mps_ceil</refname>

    <refpurpose>Round up</refpurpose>
  </refnamediv>


  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>mps_ceil( rop, op )
    rop = mps_ceil( op )</synopsis>

    <para>Overloaded usage:</para>
    <synopsis>rop = ceil( op )</synopsis>
  </refsynopsisdiv>


  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>rop, op</term>

        <listitem>
          <para>multi-precision matrix</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>


  <refsection>
    <title>Description</title>

    <para>Returns the nearest rounded up integer of <literal>op</literal>.
    </para>

    <variablelist>
      <varlistentry>
        <term>mps_ceil( rop, op )</term>

        <listitem>
          <para>Set each element of <literal>rop</literal> to the nearest 
          rounded up integer of the corresponding element of <literal>op</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = mps_ceil( op )</term>

        <listitem>
          <para>Creates a multi-precision matrix the same size and precision as 
          <literal>op</literal> containing the result.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = ceil( op )</term>

        <listitem>
          <para>Overloaded Scilab primitive ceil(). Create a multi-precision
          matrix the same size and precision as <literal>op</literal> 
          containing the result.</para>
        </listitem>
      </varlistentry>
    </variablelist>

  </refsection>



  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
// Standard usage.
A = [1.1 2; 3.9 4]
mpsA = mps_init2(A,100)
rop = mps_init(2,2,100)
rop = mps_ceil(mpsA)


// In-place computation.
A = [1.1 2; 3.9 4]
mpsA = mps_init2(A,100)
mps_ceil(mpsA, mpsA)

// Overloaded usage.
A = [1.1 2; 3.9 4]
mpsA = mps_init2(A,100)
rop = ceil(mpsA)
 ]]></programlisting>

  </refsection>


  <refsection role="see also">
<title>See Also</title>

    <simplelist type="inline">

      <member><link linkend="mps_int">mps_int</link></member>

      <member><link linkend="mps_floor">mps_floor</link></member>

      <member><link linkend="mps_round">mps_round</link></member>

    </simplelist>
  </refsection>
</refentry>

