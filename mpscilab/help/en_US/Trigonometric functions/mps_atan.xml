<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mps_atan" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>


  <refnamediv>
    <refname>mps_atan</refname>

    <refpurpose>Element-wise arctangent function</refpurpose>
  </refnamediv>


  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>mps_atan( rop, op )
    rop = mps_atan( op )</synopsis>

    <para>Overloaded usage:</para>
    <synopsis>rop = atan( op )</synopsis>
  </refsynopsisdiv>


  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>rop</term>

        <listitem>
          <para>multi-precision matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>op</term>

        <listitem>
          <para>Scilab or multi-precision matrix</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>


  <refsection>
    <title>Description</title>

    <para>Computes the element-wise arctangent of <literal>op</literal>
    and returns the result in <literal>rop</literal>.</para>

    <variablelist>
      <varlistentry>
        <term>mps_atan( rop, op )</term>

        <listitem>
          <para>Sets every element of <literal>rop</literal> to the arctangent 
            of the corresponding element of <literal>op</literal>.
            <literal>rop</literal> must be a pre-initialized matrix of the
            same dimension as <literal>op</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = mps_atan( op )</term>

        <listitem>
          <para>Creates a multi-precision matrix the same size and precision as 
          <literal>op</literal> containing the result.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = atan( op )</term>

        <listitem>
          <para>Overloaded Scilab primitive atan(). Creates a multi-precision
          matrix the same size and precision as <literal>op</literal> 
          containing the result.</para>
        </listitem>
      </varlistentry>
    </variablelist>

  </refsection>


  <refsection>
    <title>Usage notes</title>

    <para>Like most mps functions the mps_atan( rop, op ) form is faster and
    can reuse the space of an existing matrix but requires an already 
    initialized operand. Also, the same form can be used to perform an in-place
    operation when <literal>rop</literal> and <literal>op</literal> are the 
    same.</para>
  </refsection>


  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
// Standard usage.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
rop = mps_atan(mpsA)

// Standard usage with a pre-allocated matrix to receive the result.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
rop = mps_init(2,2,100)
mps_atan(rop,mpsA)

// Same but using a Scilab matrix as input.
A = [1 -2; 3 -4]
rop = mps_init(2,2,100)
mps_atan(rop,A)

// In-place computation.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
mps_atan(mpsA,mpsA)

// Overloaded usage.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
rop = atan(mpsA)
 ]]></programlisting>
  </refsection>


  <refsection role="see also">
<title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="mps_asin">mps_asin</link></member>

      <member><link linkend="mps_acos">mps_acos</link></member>

      <member><link linkend="mps_tan">mps_tan</link></member>
    </simplelist>
  </refsection>

</refentry>
