<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mps_sech" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>


  <refnamediv>
    <refname>mps_sech</refname>

    <refpurpose>Element-wise hyperbolic secant function</refpurpose>
  </refnamediv>


  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>mps_sech( rop, op )
    rop = mps_sech( op )</synopsis>

  </refsynopsisdiv>


  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>rop</term>

        <listitem>
          <para>multi-precision matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>op</term>

        <listitem>
          <para>Scilab or multi-precision matrix</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>


  <refsection>
    <title>Description</title>

    <para>Calculates the element-wise hyperbolic secant of <literal>op</literal>
    and returns the result in <literal>rop</literal>.</para>

  </refsection>


  <refsection>
    <title>Usage notes</title>

    <para>Like most mps functions the mps_sech( rop, op ) form is faster and
    can reuse the space of an existing matrix but requires an already 
    initialized operand. Also, the same form can be used to perform an in-place
    operation when <literal>rop</literal> and <literal>op</literal> are the 
    same.</para>
  </refsection>


  <refsection>
    <title>Limitations</title>

    <para>It is not currently possible to overload the Scilab sech() function.
    </para>
  </refsection>


  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
// Standard usage.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
rop = mps_sech(mpsA)

// Same but using a Scilab matrix as input.
A = [1 -2; 3 -4]
rop = mps_init(2,2,100)
mps_sech(rop,A)

// In-place computation.
A = [1 -2; 3 -4]
mpsA = mps_init2(A,100)
mps_sech(mpsA,mpsA)

 ]]></programlisting>
  </refsection>


  <refsection role="see also">
<title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="mps_csch">mps_csch</link></member>

      <member><link linkend="mps_coth">mps_coth</link></member>

      <member><link linkend="mps_sec">mps_sec</link></member>
    </simplelist>
  </refsection>

</refentry>
