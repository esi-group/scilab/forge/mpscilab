<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mps_div" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>


  <refnamediv>
    <refname>mps_div</refname>

    <refpurpose>Element-wise division operation</refpurpose>
  </refnamediv>


  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>mps_div( rop, op1, op2 )
    rop = mps_div( op1, op2 )</synopsis>

    <para>Overloaded usage:</para>
    <synopsis>rop = op1 ./ op2
              rop = op1 / op2 //With op1 or op2 a scalar.</synopsis>
  </refsynopsisdiv>


  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>rop</term>

        <listitem>
          <para>multi-precision matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>op1, op2</term>

        <listitem>
          <para>multi-precision or Scilab matrices or scalars</para>
        </listitem>
      </varlistentry>

    </variablelist>
  </refsection>


  <refsection>
    <title>Description</title>

    <para>Divide element-wise <literal>op1</literal> and <literal>op2</literal> and put the 
    result in <literal>rop</literal>. <literal>op1</literal> and <literal>op2</literal>
    can be any combinations of Scilab or multi-precision matrix. In case of division
    between a matrix and a scalar, element-wise division of every elements of the non-scalar
    operand will be performed. </para>

    <variablelist>
      <varlistentry>
        <term>mps_div( rop, op1, op2 )</term>

        <listitem>
          <para>Divide <literal>op1</literal> by <literal>op2</literal> and put 
          the result in <literal>rop</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = mps_div( op1, op2 )</term>

        <listitem>
          <para>Creates a multi-precision matrix the same size and precision as 
          <literal>op</literal> containing the result.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = op1 ./ op2</term>

        <listitem>
          <para>Overloaded Scilab element-wise division operation.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rop = op1 / op2</term>

        <listitem>
          <para>Exceptionally, when one of the input operands is a scalar the
          right matrix division symbol / can be used. This behavior is consistant
          with the one found in Scilab matrix operations.</para>
        </listitem>
      </varlistentry>
    </variablelist>

  </refsection>


  <refsection>
    <title>Usage notes</title>

    <para>Like most mps functions the mps_div( rop, op1, op2 ) form is faster and
    can reuse the space of an existing matrix but requires an already 
    initialized operand. Also, the same form can be used to perform an in-place
    operation when <literal>rop</literal> and <literal>op</literal> are the 
    same.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
// Division between two MPS matrix.
A = [1 2; 3 4]
B = [4 3; 2 1]
mpsA = mps_init2(A,100)
mpsB = mps_init2(B,100)
rop = mps_init(2,2,100)
eop = mps_div(mpsA,mpsB)

// Division between an MPS matrix and a Scilab double scalar.
A = [1 2; 3 4]
mpsA = mps_init2(A,100)
rop = mps_init(2,2,100)
mps_mul(rop,mpsA,2)

// In-place division mpsA = mpsA ./ mpsB. This is done without any intermediary
// variable or storage.
A = [1 2; 3 4]
B = [4 3; 2 1]
mpsA = mps_init2(A,100)
mpsB = mps_init2(B,100)
mps_div(mpsA,mpsA,mpsB)

//Overloaded usage
A = [1 2; 3 4]
B = [4 3; 2 1]
mpsA = mps_init2(A,100)
mpsB = mps_init2(B,100)
rop = mpsA ./ mpsB

//Overloaded usage with a scalar
A = [1 2; 3 4]
mpsA = mps_init2(A,100)
rop = mpsA / 2
 ]]></programlisting>
  </refsection>


  <refsection role="see also">
<title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="mps_mul">mps_mul</link></member>

    </simplelist>
  </refsection>
</refentry>
