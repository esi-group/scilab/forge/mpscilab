# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Verify the functionality of GMP.
# Must be called after FIND_PACKAGE(GMP).


FUNCTION(CHECK_GMP)

	IF(GMP_CHECK)
		RETURN()
	ENDIF()

	MESSAGE(STATUS "Checking GMP.")

	IF(NOT LIBGMP_FOUND)
		MESSAGE(STATUS "Error : No GMP package present, GMP_FOUND is not defined.")
		MESSAGE(SEND_ERROR "Maintainers, make sure to call FIND_PACKAGE(GMP) before CHECK_GMP().")
		RETURN()
	ENDIF()

	TRY_RUN(GMP_CHECK_RUN GMP_CHECK_COMPILE ${PROJECT_BINARY_DIR} ${PROJECT_SOURCE_DIR}/cmake_modules/gmptest.c
		CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=${LIBGMP_LIB}"
			"-DINCLUDE_DIRECTORIES:STRING=${LIBGMP_INCLUDE_DIR}"
		COMPILE_OUTPUT_VARIABLE GMP_CHECK_COMPILE_OUT
		RUN_OUTPUT_VARIABLE GMP_CHECK_RUN_OUT)

	IF(GMP_CHECK_COMPILE)
		MESSAGE(STATUS "GMP compilation test successful.")
	ELSE()
		MESSAGE(STATUS "GMP compilation test failed.")
		MESSAGE(STATUS "Error : Cannot compile a GMP application.")
		MESSAGE(SEND_ERROR "Compilation output : \n ${GMP_CHECK_COMPILE_OUT}")
	ENDIF()

	IF(GMP_CHECK_COMPILE)
		IF(NOT CMAKE_CROSSCOMPILING)
			IF(GMP_CHECK_RUN EQUAL 0)
				MESSAGE(STATUS "GMP run test successful.")
				SET(GMP_CHECK 1 CACHE BOOL "GMP test successful")
			ELSEIF(MP_CHECK_RUN EQUAL 1)
				MESSAGE(STATUS "GMP run test failed.")
				MESSAGE(STATUS "GMP calculation error. Might signify a broken library.")
			ELSE()
				MESSAGE(STATUS "GMP run test failed.")
				MESSAGE(STATUS "Warning : Cannot run a GMP application. Might signify a broken library.")
				MESSAGE(SEND_ERROR "Run output : \n ${GMP_CHECK_RUN_OUT}")
			ENDIF()
		ELSE()
			MESSAGE(STATUS "GMP run test skipped, cross-compiling.")
		ENDIF()
	ENDIF()

	MARK_AS_ADVANCED(GMP_CHECK)
	
ENDFUNCTION()
