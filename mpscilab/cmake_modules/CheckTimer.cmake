# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Check what timer functions are available.

#Check for gethrtime().
FUNCTION(CHECK_GETHRTIME)

    INCLUDE(CheckIncludeFile)

    IF(DEFINED HAVE_GETHRTIME AND DEFINED NEED_SYS_TIME_H)
        RETURN()
    ENDIF()

    CHECK_INCLUDE_FILE(time.h TIME_H)

    CHECK_INCLUDE_FILE(sys/time.h SYS_TIME_H)

    IF(TIME_H)
        MESSAGE(STATUS "Checking if gethrtime() is available in time.h")
        TRY_COMPILE(GETHR_TIME_CHECK ${PROJECT_BINARY_DIR}/Checks/gethr1
                        ${PROJECT_SOURCE_DIR}/cmake_modules/gethrtime1.c)
        IF(GETHR_TIME_CHECK)
            MESSAGE(STATUS "Checking if gethrtime() is available in time.h - yes")
        ELSE()
            MESSAGE(STATUS "Checking if gethrtime() is available in time.h - no")
        ENDIF()
    ENDIF()

    IF(SYS_TIME_H)
    MESSAGE(STATUS "Checking if gethrtime() is available in sys/time.h")
        TRY_COMPILE(GETHR_SYS_TIME_CHECK ${PROJECT_BINARY_DIR}/Checks/gethr2
                        ${PROJECT_SOURCE_DIR}/cmake_modules/gethrtime2.c)
        IF(GETHR_SYS_TIME_CHECK)
            MESSAGE(STATUS "Checking if gethrtime() is available in sys/time.h - yes")
        ELSE()
            MESSAGE(STATUS "Checking if gethrtime() is available in sys/time.h - no")
        ENDIF()
    ENDIF()

    IF(GETHR_TIME_CHECK)
        SET(HAVE_GETHRTIME 1 CACHE BOOL "Set if gethrtime() is available on the system.")
        SET(NEED_SYS_TIME_H 0 CACHE BOOL "Set if gethrtime() needs sys/time.h.")
    ELSEIF(GETHR_SYS_TIME_CHECK)
        SET(HAVE_GETHRTIME 1 CACHE BOOL "Set if gethrtime() is available on the system.")
        SET(NEED_SYS_TIME_H 1 CACHE BOOL "Set if gethrtime() needs sys/time.h.")
    ELSE()
        SET(HAVE_GETHRTIME 0 CACHE BOOL "Set if gethrtime() is available on the system.")
        SET(NEED_SYS_TIME_H 0 CACHE BOOL "Set if gethrtime() needs sys/time.h.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_GETHRTIME NEED_SYS_TIME_H)

ENDFUNCTION()

#Check for clock()
FUNCTION(CHECK_CLOCK)

    IF(DEFINED HAVE_CLOCK)
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking if clock() is available")
    TRY_COMPILE(CLOCK_CHECK ${PROJECT_BINARY_DIR}/Checks/clock
                    ${PROJECT_SOURCE_DIR}/cmake_modules/clockcheck.c)

    IF(CLOCK_CHECK)
        MESSAGE(STATUS "Checking if clock() is available - yes")
        SET(HAVE_CLOCK 1 CACHE BOOL "Set if clock() is available on the system.")
    ELSE()
        MESSAGE(STATUS "Checking if clock() is available - no")
        SET(HAVE_CLOCK 0 CACHE BOOL "Set if clock() is available on the system.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_CLOCK)

ENDFUNCTION()

#Check for gettimeofday().
FUNCTION(CHECK_GETTIME)

    IF(DEFINED HAVE_GETTIME)
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking if gettimeofday() is available")
    TRY_COMPILE(GETTIME_CHECK ${PROJECT_BINARY_DIR}/Checks/gettime
                    ${PROJECT_SOURCE_DIR}/cmake_modules/gettimeofdaycheck.c)

    IF(GETTIME_CHECK)
        MESSAGE(STATUS "Checking if gettimeofday() is available - yes")
        SET(HAVE_GETTIME 1 CACHE BOOL "Set if gettimeofday() is available on the system.")
    ELSE()
        MESSAGE(STATUS "Checking if gettimeofday() is available - no")
        SET(HAVE_GETTIME 0 CACHE BOOL "Set if gettimeofday() is available on the system.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_GETTIME)

ENDFUNCTION()

#Check for mach_absolute_time().
FUNCTION(CHECK_MACHTIME)

    IF(DEFINED HAVE_MACHTIME)
        RETURN()
    ENDIF()

    MESSAGE(STATUS "Checking if mach_absolute_time() is available")
    TRY_COMPILE(MACHTIME_CHECK ${PROJECT_BINARY_DIR}/Checks/machtime
                    ${PROJECT_SOURCE_DIR}/cmake_modules/macheabstimecheck.c)

    IF(MACHTIME_CHECK)
        MESSAGE(STATUS "Checking if mach_absolute_time() is available - yes")
        SET(HAVE_MACHTIME 1 CACHE BOOL "Set if mach_absolute_time() is available on the system.")
    ELSE()
        MESSAGE(STATUS "Checking if mach_absolute_time() is available - no")
        SET(HAVE_MACHTIME 0 CACHE BOOL "Set if mach_absolute_time() is available on the system.")
    ENDIF()

    MARK_AS_ADVANCED(HAVE_MACHTIME)

ENDFUNCTION()
