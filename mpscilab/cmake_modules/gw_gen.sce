// Multi precision toolbox for Scilab
// Copyright (C) 2009 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Input file for the gateway generation script.

toolbox_path = get_absolute_file_path('@GW_LIB_NAME@_gen.sce');

cd(toolbox_path);

functable = [ @GW_FUNC_TABLE@ ];

ilib_gen_gateway( "@GW_LIB_NAME@", functable );

exit
