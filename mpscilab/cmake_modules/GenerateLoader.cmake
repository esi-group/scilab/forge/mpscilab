# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Macro that will generate a Scilab toolbox loader script from a list of function name.

FUNCTION(GENERATE_LOADER TBX_FUNC_TABLE TBX_NAME GW_LIB_NAME)

	MESSAGE(STATUS "Starting the loader generation.")

	SET(LOADER_FILE ${PROJECT_SOURCE_DIR}/cmake_modules/loader.sce)
	SET(LOADER_GEN_DEST_FILE ${CMAKE_CURRENT_BINARY_DIR}/loader.sce)

	MESSAGE(STATUS "Generating the toolbox loader script.")
	CONFIGURE_FILE(${LOADER_FILE} ${LOADER_GEN_DEST_FILE})


	FIND_FILE(LOADER_SRC_FILE loader.sce PATHS ${CMAKE_CURRENT_BINARY_DIR} NO_DEFAULT_PATH)

	IF(NOT LOADER_SRC_FILE)
		MESSAGE(SEND_ERROR "Error : Loader file generation failed.")
	ENDIF()

	STRING(TOUPPER ${TBX_NAME} TBX_SRC_VAR)

	#SET(${TBX_SRC_VAR}_LOADER_FILE ${LOADER_SRC_FILE} PARENT_SCOPE)

ENDFUNCTION()
