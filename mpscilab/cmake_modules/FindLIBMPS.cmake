# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# - Attempts to find LibMPS
#
#  LIBMPS_FOUND - LibMPS was found
#  LIBMPS_INCLUDE_DIR - The LibMPS include directory
#  LIBMPS_LIB - The LibMPS shared library full path
#  LIBMPS_DIR - LibMPS main path, i.e. the one that contains include and lib.

# Check if LIBMPS_DIR is defined and use that path first.
IF(LIBMPS_DIR)
	FIND_PATH(LIBMPS_INCLUDE_DIR mps.h PATHS ${LIBMPS_DIR}/include NO_DEFAULT_PATH)

IF(WITH_STATIC_LIBMPS)
	FIND_LIBRARY(LIBMPS_LIB libmps.a PATHS ${LIBMPS_DIR}/lib NO_DEFAULT_PATH)
ELSE()
	FIND_LIBRARY(LIBMPS_LIB mps PATHS ${LIBMPS_DIR}/lib NO_DEFAULT_PATH)
ENDIF()

	IF(NOT LIBMPS_LIB)
		MESSAGE(STATUS "Warning : LibMPS not found in the path specified in LIBMPS_DIR")
		UNSET(LIBMPS_DIR)
	ENDIF()
ENDIF()

FIND_PATH(LIBMPS_INCLUDE_DIR mps.h)

IF(WITH_STATIC_LIBMPS)
    FIND_LIBRARY(LIBMPS_LIB NAMES libmps.a)
ELSE()
    FIND_LIBRARY(LIBMPS_LIB NAMES mps)
ENDIF()

#Get the root LibMPS path and add a cache entry.
GET_FILENAME_COMPONENT(LIBMPS_DIR ${LIBMPS_INCLUDE_DIR} PATH)
SET(LIBMPS_DIR ${LIBMPS_DIR} CACHE PATH "LibMPS root directory.")

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(LibMPS DEFAULT_MSG LIBMPS_LIB LIBMPS_INCLUDE_DIR)

MARK_AS_ADVANCED(LIBMPS_INCLUDE_DIR LIBMPS_LIB)
