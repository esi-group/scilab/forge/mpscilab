// Multi precision toolbox for Scilab
// Copyright (C) 2009 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Input file for the gateway generation script.

toolbox_path = get_absolute_file_path('loader.sce');

functable = [ @TBX_FUNC_TABLE@ ];

add_help_chapter('mpscilab - Arbitrary precision toolbox for Scilab', toolbox_path + "/jar/", %F);

disp("Loading dynamic lib");

gw_lib_name = '@GW_LIB_NAME@' + getdynlibext();

// Load GMP and MPFR if found in the toolbox directory.
if(getos() == "Linux")
    [finfo,err] = fileinfo(toolbox_path + "/lib/" + "libgmp" + getdynlibext());
    if( err == 0 )
        link(toolbox_path + "/lib/" + "libgmp" + getdynlibext());
    end

    [finfo,err] = fileinfo(toolbox_path + "/lib/" + "libmpfr" + getdynlibext());
    if( err == 0 )
        link(toolbox_path + "/lib/" + "libmpfr" + getdynlibext());
    end
end

if(getos() == "Windows")
    [finfo,err] = fileinfo(toolbox_path + "\lib\" + "gmp" + getdynlibext());
    if( err == 0 )
        link(toolbox_path + "\lib\" + "gmp" + getdynlibext());
    end
    
    [finfo,err] = fileinfo(toolbox_path + "\lib\" + "mpir" + getdynlibext());
    if( err == 0 )
        link(toolbox_path + "\lib\" + "mpir" + getdynlibext());
    end

    [finfo,err] = fileinfo(toolbox_path + "\lib\" + "mpfr" + getdynlibext());
    if( err == 0 )
        link(toolbox_path + "\lib\" + "mpfr" + getdynlibext());
    end
end

addinter(toolbox_path + '/lib/' + gw_lib_name, '@GW_LIB_NAME@', functable(:,1));

// Register the interface to handle arguments by reference.

intppty( floor(funptr( "mps_add" ) / 1000) );

// Load the compiled macros.
mpscilablib = lib( toolbox_path+'macros' );


