# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# Macro that will generate a Scilab toolbox gateway file from a list of function name.

FUNCTION(GENERATE_GATEWAY GW_FUNC_TABLE GW_LIB_NAME)

	MESSAGE(STATUS "Starting the gateway generation.")

	IF(NOT SCILAB_FOUND)
		MESSAGE("Error : No Scilab package present, SCILAB_FOUND is not defined.")
		MESSAGE("Maintainers, make sure to call FIND_PACKAGE(Scilab) before GENERATE_GATEWAY().")
		RETURN()
	ENDIF()

	SET(GW_GEN_FILE ${PROJECT_SOURCE_DIR}/cmake_modules/gw_gen.sce)
	SET(GW_GEN_DEST_FILE ${CMAKE_CURRENT_BINARY_DIR}/${GW_LIB_NAME}_gen.sce)

	MESSAGE(STATUS "Writing the Scilab gateway generator script.")
	CONFIGURE_FILE(${GW_GEN_FILE} ${GW_GEN_DEST_FILE})

	MESSAGE(STATUS "Generating the gateway source file.")
	
    IF(WIN32)
	EXECUTE_PROCESS(COMMAND ${SCILAB_PROGRAM_DIR}/scilex -nwni -nb -f ${CMAKE_CURRENT_BINARY_DIR}/${GW_LIB_NAME}_gen.sce 
        TIMEOUT 20 OUTPUT_VARIABLE GATEWAY_GEN_OUT ERROR_VARIABLE GATEWAY_GEN_OUT)
    ELSE()
	EXECUTE_PROCESS(COMMAND ${SCILAB_PROGRAM_DIR}/scilab -nwni -nb -f ${CMAKE_CURRENT_BINARY_DIR}/${GW_LIB_NAME}_gen.sce 
        TIMEOUT 20 OUTPUT_VARIABLE GATEWAY_GEN_OUT ERROR_VARIABLE GATEWAY_GEN_OUT)
    ENDIF()

    MESSAGE("${GATEWAY_GEN_OUT}")

	FIND_FILE(GATE_SRC_FILE ${GW_LIB_NAME}.c PATHS ${CMAKE_CURRENT_BINARY_DIR} NO_DEFAULT_PATH)

	IF(NOT GATE_SRC_FILE)
		MESSAGE(SEND_ERROR "Error : Gateway file generation failed.")
	ENDIF()

	STRING(TOUPPER ${GW_LIB_NAME} GATE_SRC_VAR)

	SET(LIBSCIMPS_SRC_FILE ${GATE_SRC_FILE} PARENT_SCOPE)

ENDFUNCTION()
