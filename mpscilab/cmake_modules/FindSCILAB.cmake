# Multi precision toolbox for Scilab
# Copyright (C) 2009 - Jonathan Blanchard
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

# - CMake find module for Scilab
# The following variables will be defined
#
#   SCILAB_FOUND - Scilab was found
#   SCIALB_DIR - Scilab root install directory
#   SCILAB_PROGRAM - Scilab executable full path
#   SCILAB_PROGRAM_DIR - Scilab executable directory
#   SCILAB_LIB_DIR - Scilab library path
#   SCILAB_INCLUDE_DIR - The Scilab core module include path

#Set the known possible Scilab master directory name
SET(SCILAB_MASTER_PATH_NAME
  scilab-5.3.2 scilab-5.3.1 scilab-5.3.0 scilab-5.2.2 scilab-5.2.1 scilab-5.2.0 
  scilab-5.1.1 scilab-5.1 scilab-5.0.3 scilab-5.0.2 scilab-5.0.1 scilab-5.0
	scilab-4.1.2 scilab-4.1.1 scilab-4.1 scilab-4.0
	scilab
  )

IF(WIN32)

	FOREACH(SPATH ${SCILAB_MASTER_PATH_NAME})
		SET(SCILAB_PATHS  "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Scilab\\${SPATH};SCIPATH]/modules/core/includes" ${SCILAB_PATHS})
	ENDFOREACH()

	IF(SCILAB_DIR)
		FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS ${SCILAB_DIR}/includes NO_DEFAULT_PATH)
		IF(NOT SCILAB_INCLUDE_DIR)
			MESSAGE(STATUS "WARNING : Scilab not found in the path specified in SCILAB_DIR")
			UNSET(SCILAB_DIR)
		ENDIF()
	ENDIF()

	# Try to find a scilab registry key.
	IF(NOT SCILAB_DIR)
        FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS ${SCILAB_PATHS} NO_DEFAULT_PATH)
	ENDIF()

	# As a last resort tries to scan the directories.
	IF(NOT SCILAB_DIR)
		FOREACH(SPATH ${SCILAB_MASTER_PATH_NAME})
			FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS C:/Program_files/${SPATH}/includes NO_DEFAULT_PATH)
		ENDFOREACH()

		IF(SCILAB_INCLUDE_DIR)
			GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_INCLUDE_DIR} PATH)
			GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_DIR} PATH)
			GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_DIR} PATH)
		ENDIF()
	ENDIF()

	#Find the core module include directory.
	FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS ${SCILAB_DIR}/modules/core/includes NO_DEFAULT_PATH)

	#Find the library directory
	FIND_PATH(SCILAB_LIB_DIR LibScilab.lib ${SCILAB_DIR}/bin NO_DEFAULT_PATH)

	#Get the main executable path
	FIND_PROGRAM(SCILAB_PROGRAM Wscilex PATHS ${SCILAB_DIR}/bin NO_DEFAULT_PATH)
	GET_FILENAME_COMPONENT(SCILAB_PROGRAM_DIR ${SCILAB_PROGRAM} PATH)

	SET(SCILAB_DIR ${SCILAB_DIR} CACHE PATH "Scilab installation root directory.")

    SET(SCILAB_INCLUDE_DIR ${SCILAB_DIR}/modules/api_scilab/includes
                           ${SCILAB_DIR}/modules/core/includes
                           ${SCILAB_DIR}/modules/output_stream/includes
                           ${SCILAB_DIR}/libs/MALLOC/includes
                           ${SCILAB_DIR}/modules/mexlib/includes
                           ${SCILAB_DIR}/modules/shell/includes
                           PATH)

ENDIF(WIN32)

IF(UNIX)

	#Check if the SCILAB_DIR variable is already specified
	IF(SCILAB_DIR)
		FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS ${SCILAB_DIR}/include/scilab NO_DEFAULT_PATH)
		IF(NOT SCILAB_INCLUDE_DIR)
			MESSAGE("WARNING : Scilab not found in the path specified in SCILAB_DIR")
			UNSET(SCILAB_DIR)
		ENDIF()
	ENDIF()

	#Check the default unix/linux paths for a scilab installation
	FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS /usr/local/include/scilab NO_DEFAULT_PATH)
	FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS /usr/include/scilab NO_DEFAULT_PATH)

	#Check under the different version number directory.
	FOREACH(SPATH ${SCILAB_MASTER_PATH_NAME})
		FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS /opt/${SPATH}/include/scilab NO_DEFAULT_PATH)
		FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS /usr/local/${SPATH}/include/scilab NO_DEFAULT_PATH)
		FIND_PATH(SCILAB_INCLUDE_DIR stack-c.h PATHS /usr/${SPATH}/include/scilab NO_DEFAULT_PATH)
	ENDFOREACH(SPATH)

	#Find the library directory
	FIND_PATH(SCILAB_LIB_DIR libscicore.so ${SCILAB_INCLUDE_DIR}/../../lib/scilab NO_DEFAULT_PATH)

	#Get the main executable path
	FIND_PROGRAM(SCILAB_PROGRAM scilab PATHS ${SCILAB_INCLUDE_DIR}/../../bin NO_DEFAULT_PATH)
	GET_FILENAME_COMPONENT(SCILAB_PROGRAM_DIR ${SCILAB_PROGRAM} PATH)

	#Cache the root Scilab path
	GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_INCLUDE_DIR} PATH)
	GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_DIR} PATH)
	GET_FILENAME_COMPONENT(SCILAB_DIR ${SCILAB_DIR} PATH)
	SET(SCILAB_DIR ${SCILAB_DIR} CACHE PATH "Scilab installation root directory.")

ENDIF(UNIX)

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Scilab DEFAULT_MSG SCILAB_DIR 
                                         SCILAB_INCLUDE_DIR 
                                         SCILAB_PROGRAM 
                                         SCILAB_PROGRAM_DIR 
                                         SCILAB_LIB_DIR)

MARK_AS_ADVANCED( SCILAB_INCLUDE_DIR
                  SCILAB_PROGRAM 
                  SCILAB_PROGRAM_DIR 
                  SCILAB_LIB_DIR)

