/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_maxabs( char *fname, unsigned long fname_len )
{
    SciErr sciErr;
    mps_ptr rop, op;
    int *arg[3] = {NULL};
    int typearg[3] = {0};
    int ms;
    int ns;
    unsigned int m, n;
    unsigned int *vect;
    double *indx;
    int pilen[1];
    char opchar[2] = "m";
    unsigned int i;

    if( Rhs > 3 || Rhs < 1 )
    {
        sciprint("%s: Wrong number of input argument(s): 1 to 3 expected.\n",fname);
        return 0;
    }

    for( i = 0; i < (unsigned)Rhs; i++ )
    {
        getVarAddressFromPosition( pvApiCtx, i+1, &arg[i] );
        getVarType( pvApiCtx, arg[i], &typearg[i] );
    }

    if( (Rhs == 2 && typearg[1] == sci_strings) || (Rhs == 3 && typearg[2] == sci_strings) )
    {
        getMatrixOfString( pvApiCtx, arg[Rhs-1], &ms, &ns, NULL, NULL );
        if( ms != 1 || ns != 1 )
        {
            sciprint( "%s: Wrong size for argument %d. Single character string expected.\n", fname, Rhs );
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg[Rhs-1], &ms, &ns, pilen, NULL );
        if( pilen[0] != 1 )
        {
            sciprint( "%s: Wrong size for argument %d. Single character string expected.\n", fname, Rhs );
            return 0;
        }

        GetRhsStringVar( Rhs, &ms, &ns, pilen, opchar );
        if( !(opchar[0] == 'r' || opchar[0] == 'c') )
        {
            sciprint("%s: Invalid operation specified. Valid options : 'r', 'c'.\n",fname);
            return 0;
        }
    }
    else if( Rhs == 3 && typearg[2] != sci_strings )
    {
        sciprint( "%s: Wrong type for argument 3. Single character string expected.\n", fname );
        return 0;
    }

    if( opchar[0] == 'm' )
    {
        if( typearg[0] == sci_mlist && typearg[1] == sci_mlist )
            goto M_MLIST_MLIST;
        else if( typearg[0] == sci_mlist )
            goto M_MLIST;
        else
        {
            sciprint("%s: Function not defined for those input types.\n",fname);
            return 0;            
        }
    }
    else if( opchar[0] == 'r' )
    {
        if( typearg[0] == sci_mlist && typearg[1] == sci_mlist )
            goto R_MLIST_MLIST;
        else if( typearg[0] == sci_mlist )
            goto R_MLIST;
        else
        {
            sciprint("%s: Function not defined for those input types.\n",fname);
            return 0;            
        }
    }
    else
    {
        if( typearg[0] == sci_mlist && typearg[1] == sci_mlist )
            goto C_MLIST_MLIST;
        else if( typearg[0] == sci_mlist )
            goto C_MLIST;
        else
        {
            sciprint("%s: Function not defined for those input types.\n",fname);
            return 0;            
        }
    }

    M_MLIST:
        op = MpsGetMatrix( 1 );

        mps_maxabs( op, &m, &n );

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, 1, 2, &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }

        indx[0] = m;
        indx[1] = n;

        LhsVar(1) = Rhs + 1;

        goto END;

    M_MLIST_MLIST:
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for result operand. Scalar expected.\n",fname);
            return 0;
        }

        mpfr_set( mps_get_ele(rop,1,1), mps_maxabs(op,&m,&n), GMP_RNDN );

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, 1, 2, &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }

        indx[0] = m;
        indx[1] = n;

        LhsVar(1) = Rhs + 1;

        goto END;

    R_MLIST:
        op = MpsGetMatrix( 1 );
        vect = malloc( sizeof(unsigned int) * MPS_NUMROW(op) );        

        mps_maxabsr( NULL, op, vect );        

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, MPS_NUMROW(op), 1, &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }        

        for( i = 0; i < MPS_NUMROW(op); i++ )
            indx[i] = vect[i];

        free( vect );        

        LhsVar(1) = Rhs + 1;

        goto END;

    R_MLIST_MLIST:
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMROW(op) )
        {
            sciprint("%s: Wrong size for result operand.\n",fname);
            return 0;
        }

        vect = malloc( sizeof(unsigned int) * MPS_NUMROW(op) );        

        mps_maxabsr( rop, op, vect );        

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, MPS_NUMROW(op), 1, &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }        

        for( i = 0; i < MPS_NUMROW(op); i++ )
            indx[i] = vect[i];

        free( vect );        

        LhsVar(1) = Rhs + 1;

        goto END;

    C_MLIST:
        op = MpsGetMatrix( 1 );
        vect = malloc( sizeof(unsigned int) * MPS_NUMCOL(op) );        

        mps_maxabsc( NULL, op, vect );        

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, 1, MPS_NUMCOL(op), &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }        

        for( i = 0; i < MPS_NUMCOL(op); i++ )
            indx[i] = vect[i];

        free( vect );        

        LhsVar(1) = Rhs + 1;

        goto END;

    C_MLIST_MLIST:
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Wrong size for result operand.\n",fname);
            return 0;
        }

        vect = malloc( sizeof(unsigned int) * MPS_NUMCOL(op) );        

        mps_maxabsc( rop, op, vect );        

        sciErr =  allocMatrixOfDouble( pvApiCtx, Rhs+1, 1, MPS_NUMCOL(op), &indx );
        if(sciErr.iErr)
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }        

        for( i = 0; i < MPS_NUMCOL(op); i++ )
            indx[i] = vect[i];

        free( vect );        

        LhsVar(1) = Rhs + 1;

        goto END;

    END:

    return 0;
}
