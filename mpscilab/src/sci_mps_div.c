/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"


/* 
 * This function is constructed by including a "template" defined in x_macro_c.c
 * , see the aforementioned file for details.
 */

#define SCI_FUNCTION sci_mps_div
#define MPS_FUNCTION mps_div
#define MPS_FUNCTION_DOUBLE mps_div_double
#define MPS_DOUBLE_FUNCTION mps_double_div
#define MPS_DOUBLE_FUNCTION_DOUBLE mps_double_div_double
#define MPS_SCALAR_FUNCTION mps_scalar_div
#define MPS_FUNCTION_SCALAR mps_div_scalar
#define MPS_SCALAR_FUNCTION_DOUBLE mps_scalar_div_double
#define MPS_FUNCTION_SCALAR_DOUBLE mps_div_scalar_double
#define MPS_SCALAR_DOUBLE_FUNCTION mps_scalar_double_div
#define MPS_DOUBLE_FUNCTION_SCALAR mps_double_div_scalar
#define MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE mps_double_div_scalar_double
#define MPS_SCALAR_DOUBLE_FUNCTION_DOUBLE mps_scalar_double_div_double

#include "x_macro_c.c"

