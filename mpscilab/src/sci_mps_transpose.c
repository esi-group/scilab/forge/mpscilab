/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"


int sci_mps_transpose( char *fname, unsigned long fname_len )
{
    unsigned int sig;
    mps_ptr rop, op;
    mps_t mps;
    int ret, StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist ) )
    {
        op = MpsGetMatrix( 1 );

        MpsCollect();
        ret = mps_init( mps, MPS_NUMCOL(op), MPS_NUMROW(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_full_mat_transpose( mps, op );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( !mps_isalias( rop, op ) )
        {
            if( (MPS_NUMROW(rop) != MPS_NUMCOL(op)) || (MPS_NUMCOL(rop) != MPS_NUMROW(op)) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }

            mps_full_mat_transpose( rop, op );
        }
        else
            mps_mat_transpose( rop );
    }
    else
    {
        if( Rhs < 1 || Rhs > 2 )
        {
            sciprint("%s: Wrong number of input arguments: 1 or 2 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

