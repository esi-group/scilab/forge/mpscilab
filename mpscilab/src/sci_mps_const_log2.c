/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

/*
 * Return a matrix filled with the natural logarithm of 2.
 * Scilab definitions :
 * rop = mps_pi( prec, [rnd] )
 * rop = mps_pi( m, n, prec, [rnd] )
 * mps_pi( rop, [rnd] )
 */
int sci_mps_const_log2( char *fname, unsigned long fname_len )
{
    int *arg[4] = {NULL};
    int typearg[4] = {0};
    int m0, m1, m2;
    int n0, n1, n2;
    double *dptr0, *dptr1, *dptr2;
    mp_rnd_t rnd = GMP_RNDN;
    mps_ptr rop;
    mps_t mps;
    int StkAdd;
    unsigned int i;

    if( Rhs > 4 || Rhs < 1 )
    {
        sciprint("%s: Wrong number of input argument(s): 1 to 4 expected.\n",fname);
        return 0;
    }

    for( i = 0; i < (unsigned)Rhs; i++ )
    {
        getVarAddressFromPosition( pvApiCtx, i+1, &arg[i] );
        getVarType( pvApiCtx, arg[i], &typearg[i] );
    }

    if( typearg[0] == sci_matrix && typearg[1] == sci_matrix && typearg[2] == sci_matrix )
        goto DOUBLE_DOUBLE_DOUBLE;
    else if ( typearg[0] == sci_matrix )
        goto DOUBLE_;
    else if( typearg[0] == sci_mlist )
        goto TLIST;
    else
    {
        sciprint("%s: Function not defined for those input types.\n",fname);
        return 0;
    }


    DOUBLE_DOUBLE_DOUBLE:
        getMatrixOfDouble( pvApiCtx, arg[0], &m0, &n0, &dptr0 );
        getMatrixOfDouble( pvApiCtx, arg[1], &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg[2], &m2, &n2, &dptr2 );

        if( m0 != 1 || n0 != 1 )
        {
            sciprint("%s: Wrong size for argument 1. Scalar expected.\n",fname);
            return 0;
        }

        if( m1 != 1 || n1 != 1 )
        {
            sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
            return 0;
        }

        if( m2 != 1 || n2 != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
            return 0;
        }

        if( dptr2[0] < 2 )
        {
            sciprint("%s: Precision must be 2 or higher.\n",fname);
            return 0;
        }


        if( Rhs == 4 )
            if( MpsGetRoundingArg( fname, &rnd, arg[3], 4 ) != 0 )
                return 0;

        MpsCollect();

        mps_init( mps, dptr0[0], dptr1[0], dptr2[0], MPS_COL_ORDER );

        mps_const_log2( mps, rnd );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs+1;

        goto END;

    DOUBLE_:
        getMatrixOfDouble( pvApiCtx, arg[0], &m0, &n0, &dptr0 );

        if( m0 != 1 || n0 != 1 )
        {
            sciprint("%s: Wrong size for argument 1. Scalar expected.\n",fname);
            return 0;
        }

        if( dptr0[0] < 2 )
        {
            sciprint("%s: Precision must be 2 or higher.\n",fname);
            return 0;
        }

        if( Rhs == 2 )
            if( MpsGetRoundingArg( fname, &rnd, arg[1], 2 ) != 0 )
                return 0;

        MpsCollect();

        mps_init( mps, 1, 1, dptr0[0], MPS_COL_ORDER );

        mps_const_log2( mps, rnd );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs+1;

        goto END;

    TLIST:
        rop = MpsGetMatrix( 1 );

        if( Rhs == 2 )
            if( MpsGetRoundingArg( fname, &rnd, arg[1], 2 ) != 0 )
                return 0;

        mps_const_log2( rop, rnd );

        goto END;

    END:

    return 0;
}
