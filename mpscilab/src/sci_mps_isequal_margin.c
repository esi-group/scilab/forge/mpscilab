/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"

#include "scimps.h"


/*
 * Compare two matrices element by element and return true if they are equal.
 * Scilab definition :
 * res = mps_isequal( op1, op2, [margin] );
 * res : Comparison result,
 * true if op1 == op2,
 * false otherwise.
 * op1, op2 : Matrices to be compared, double or multi-precision matrices.
 * margin : optional margin.
 */
int sci_mps_isequal_margin( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3;
    int m1, m2, m3, n1, n2, n3;
    int typearg1, typearg2, typearg3;
    mps_ptr op1, op2, op3;
    mpfr_ptr margin = NULL;
    int *rop = NULL;
    double *dptr1, *dptr2, *dptr3;
    unsigned int result = 0;
    SciErr sciErr;
    MPFR_DECL_INIT( mpfrtmp, 53 );
    int pilen[1];
    int mode = 0;
    char modechar[2];


    CheckRhs(3,4);
    CheckLhs(1,1);

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );    

    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
    getVarType( pvApiCtx, arg3, &typearg3 );
    if( typearg3 == sci_matrix )
    {
        getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );
        if( m3 != 1 || n3 != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
            return 0;
        }
        mpfr_init2( mpfrtmp, 53 );
        mpfr_set_d( mpfrtmp, dptr3[0], GMP_RNDN );
        margin = mpfrtmp;
        
    }
    else if( typearg3 == sci_mlist )
    {
        op3 = MpsGetMatrix( 3 );
        if( MPS_NUMCOL(op3) != 1 || MPS_NUMROW(op3) != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
            return 0;
        }
        margin = mps_get_ele( op3, 1, 1 );
    }
    
    if( Rhs == 4 )
    {
        GetRhsStringVar( 4 , &m1, &n1, pilen, modechar );
        if( modechar[0] == 'b' )
            mode = 1;
        else if( modechar[0] == 'a' )
            mode = 0;
        else
        {
            sciprint("%s: Invalid mode specified. Valid options : 'a', 'b'.\n",fname);
            return 0;
        }
    }

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg1)
    {
        case sci_matrix:
            switch (typearg2)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    return 0;
    		}
        case sci_mlist:
            switch (typearg2)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    return 0;
            }
        default:
            return 0;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 2 );

        if( margin == NULL )
            result = mps_equal_double( op2, dptr1, MPS_COL_ORDER );
        else if( mode == 0 )
            result = mps_equal_margin_double( op2, dptr1, MPS_COL_ORDER, margin );
        else
            result = mps_equal_double_bmargin( dptr1, MPS_COL_ORDER, op2, (unsigned int)dptr3[0] );

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( margin == NULL )
            result = mps_equal_double( op1, dptr2, MPS_COL_ORDER );
        else if( mode == 0 )
            result = mps_equal_margin_double( op1, dptr2, MPS_COL_ORDER, margin );
        else
            result = mps_equal_bmargin_double( op1, dptr2, MPS_COL_ORDER, (unsigned int)dptr3[0] );

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 1 );
        op2 = MpsGetMatrix( 2 );

        if( margin == NULL )
            result = mps_equal( op1, op2 );
        else if( mode == 0 )
            result = mps_equal_margin( op1, op2, margin );
        else
            result = mps_equal_bmargin( op1, op2, (unsigned int)dptr3[0] );

    END:

    sciErr = allocMatrixOfBoolean(pvApiCtx, Rhs+1, 1, 1, &rop);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    rop[0] = result;

    LhsVar(1) = Rhs + 1;


    return 0;
}
