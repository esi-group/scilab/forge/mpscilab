/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* 
 * X-Macro for commutative functions with the signature :
 * mps_func( rop, op1, op2 [, rnd] )
 * rop = mps_func( op1, op2 [, rnd] )
 *
 * Two "arguments" are required :
 * SCI_FUNCTION : Function name as it would be defined in a stand-alone c file.
 * MPS_FUNCTION : mp_lib function to map this function to.
 *
 * Functions using this macro :
 * sci_mps_add
 * sci_mps_mul
 *
 */

#ifndef SCI_FUNCTION
    #error "SCI_FUNCTION is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_FUNCTION
    #error "MPS_FUNCTION is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_FUNCTION_DOUBLE
    #error "MPS_FUNCTION_DOUBLE is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_DOUBLE_FUNCTION_DOUBLE
    #error "MPS_DOUBLE_FUNCTION_DOUBLE is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_FUNCTION_SCALAR
    #error "MPS_FUNCTION_SCALAR is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_FUNCTION_SCALAR_DOUBLE
    #error "MPS_FUNCTION_SCALAR_DOUBLE is not defined in invocation of x_macro_b.c"
#endif

#ifndef MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE
    #error "MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE is not defined in invocation of x_macro_b.c"
#endif

#define XMB_SCI_FUNC_DEF( FUNC ) int FUNC ( char *fname, unsigned long fname_len )
#define XMB_FUNC( FUNC ) FUNC

XMB_SCI_FUNC_DEF( SCI_FUNCTION )
{
    int *arg1, *arg2, *arg3, *arg4;
    int m, n, m1, n1, m2, n2;
    mps_ptr rop, op1, op2;
    mp_rnd_t rnd = GMP_RNDN;
    double *dptr1, *dptr2;
    char* rndptr[1];
    char rndchar[2];
    int pilen[1];
    int ret;
    unsigned int sig, prec;
    mps_t mps;
    int StkAdd;

    sig = MpsGetSig();


    if( sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_matrix, sci_strings ) ||
        sig == MpsSig( sci_matrix, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_matrix, sci_matrix, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_matrix, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_matrix, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_matrix, sci_matrix, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, Rhs, &arg4 );

        getMatrixOfString( pvApiCtx, arg4, &m, &n, NULL, NULL );

        if( m != 1 || n != 1 )
        {
            sciprint("%s: Wrong size for rounding mode. Single character string expected.\n",fname);
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg4, &m, &n, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong length for rounding mode. Single character string expected.\n",fname);
            return 0;
        }

        rndptr[0] = rndchar;
        getMatrixOfString( pvApiCtx, arg4, &m, &n, pilen, rndptr );

        if( rndchar[0] == 'n' )
            rnd = GMP_RNDN;
        else if( rndchar[0] == 'z' )
            rnd = GMP_RNDZ;
        else if( rndchar[0] == 'u')
            rnd = GMP_RNDU;
        else if( rndchar[0] == 'd' )
            rnd = GMP_RNDD;
        else
        {
            sciprint("%s: Invalid rounding mode specified. Valid options : 'n', 'z', 'u', 'd'.\n",fname);
            return 0;
        }

    }


    if( sig == MpsSig( sci_mlist, sci_mlist ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) )
    {
        /* MPS = MPS_FUNC( MPS, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        op1 = MpsGetMatrix( 1 );
        op2 = MpsGetMatrix( 2 );

        prec = MPS_PREC(op1) > MPS_PREC(op2) ? MPS_PREC(op1) : MPS_PREC(op2);

        if( MPS_NUMROW(op1) == 1 && MPS_NUMCOL(op1) == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op2), MPS_NUMCOL(op2), prec, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR ) ( mps, op2, op1, rnd );
        }
        else if( MPS_NUMROW(op2) == 1 && MPS_NUMCOL(op2) == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op1), MPS_NUMCOL(op1), prec, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR ) ( mps, op1, op2, rnd );
        }
        else
        {
            if( MPS_NUMROW(op1) != MPS_NUMROW(op2) || MPS_NUMCOL(op1) != MPS_NUMCOL(op2) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }

            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op1), MPS_NUMCOL(op1), prec, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION ) ( mps, op1, op2, rnd );
        }

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix ) ||
             sig == MpsSig( sci_mlist, sci_matrix, sci_strings ) )
    {
        /* MPS = MPS_FUNC( MPS, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( MPS_NUMROW(op1) == 1 && MPS_NUMCOL(op1) == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, m2, n2, MPS_PREC(op1), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_SCALAR_FUNCTION_DOUBLE ) ( mps, dptr2, 0, op1, rnd );
        }
        else if( m2 == 1 && n2 == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op1), MPS_NUMCOL(op1), MPS_PREC(op1), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR_DOUBLE ) ( mps, op1, dptr2[0], rnd );
        }
        else
        {
            if( MPS_NUMROW(op1) != (unsigned)m2 || MPS_NUMCOL(op1) != (unsigned)n2 )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }

            MpsCollect();
            ret = mps_init( mps, m2, n2, MPS_PREC(op1), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_DOUBLE ) ( mps, op1, dptr2, 0, rnd );
        }

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_matrix, sci_mlist ) ||
             sig == MpsSig( sci_matrix, sci_mlist, sci_strings ) )
    {
        /* MPS = MPS_FUNC( DOUBLE, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 2 );

        if( m1 == 1 && n1 == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op2), MPS_NUMCOL(op2), MPS_PREC(op2), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR_DOUBLE ) ( mps, op2, dptr1[0], rnd );
        }
        else if( MPS_NUMROW(op2) == 1 && MPS_NUMCOL(op2) == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, m1, n1, MPS_PREC(op2), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_SCALAR_FUNCTION_DOUBLE ) ( mps, dptr1, 0, op2, rnd );
        }
        else
        {
            if( MPS_NUMROW(op2) != (unsigned)m1 || MPS_NUMCOL(op2) != (unsigned)n1 )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }

            MpsCollect();
            ret = mps_init( mps, m1, n1, MPS_PREC(op2), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_DOUBLE ) ( mps, op2, dptr1, 0, rnd );
        }

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_matrix, sci_matrix ) ||
             sig == MpsSig( sci_matrix, sci_matrix, sci_strings ) )
    {
        /* MPS = MPS_FUNC( DOUBLE, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( m1 == 1 && n1 == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, m2, n2, 53, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XMB_FUNC( MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE ) ( mps, dptr2, 0, dptr1[0], rnd );
        }
        else if( m2 == 1 && n2 == 1 )
        {
            MpsCollect();
            ret = mps_init( mps, m1, n1, 53, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XMB_FUNC( MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE ) ( mps, dptr1, 0, dptr2[0], rnd );
        }
        else
        {
            if( m1 != m2 || n1 != n2 )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }

            MpsCollect();
            ret = mps_init( mps, m1, n1, 53, 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XMB_FUNC( MPS_DOUBLE_FUNCTION_DOUBLE ) ( mps, dptr1, 0, dptr2, 0, rnd );
        }

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist, sci_mlist ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_mlist, sci_strings ) )
    {
        /* MPS_FUNC( MPS, MPS, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        op1 = MpsGetMatrix( 2 );
        op2 = MpsGetMatrix( 3 );

        if( MPS_NUMROW(op1) == 1 && MPS_NUMCOL(op1) == 1 )
        {
            if( MPS_NUMROW(op2) != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR ) ( rop, op2, op1, rnd );
        }
        else if( MPS_NUMROW(op2) == 1 && MPS_NUMCOL(op2) == 1 )
        {
            if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || MPS_NUMCOL(op1) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR ) ( rop, op1, op2, rnd );
        }
        else
        {
            if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || MPS_NUMCOL(op1) != MPS_NUMCOL(rop) ||
                MPS_NUMROW(op2) != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION ) ( rop, op1, op2, rnd );
        }
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix, sci_mlist ) ||
             sig == MpsSig( sci_mlist, sci_matrix, sci_mlist, sci_strings ) )
    {
        /* MPS_FUNC( MPS, DOUBLE, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 3 );

        if( m1 == 1 && n1 == 1 )
        {
            if( MPS_NUMROW(op2) != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR_DOUBLE ) ( rop, op2, dptr1[0], rnd );
        }
        else if( MPS_NUMROW(op2) == 1 && MPS_NUMCOL(op2) == 1 )
        {
            if( (unsigned)m1 != MPS_NUMROW(rop) || (unsigned)n1 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_SCALAR_FUNCTION_DOUBLE ) ( rop, dptr1, 0, op2, rnd );
        }
        else
        {
            if( (unsigned)m1 != MPS_NUMROW(rop) || (unsigned)n1 != MPS_NUMCOL(rop) ||
                MPS_NUMROW(op2) != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_DOUBLE ) ( rop, op2, dptr1, 0, rnd );
        }
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist, sci_matrix ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_matrix, sci_strings ) )
    {
        /* MPS_FUNC( MPS, MPS, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        op1 = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        if( MPS_NUMROW(op1) == 1 && MPS_NUMCOL(op1) == 1 )
        {
            if( (unsigned)m2 != MPS_NUMROW(rop) || (unsigned)n2 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_SCALAR_FUNCTION_DOUBLE ) ( rop, dptr2, 0, op1, rnd );
        }
        else if( m2 == 1 && n2 == 1 )
        {
            if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || MPS_NUMCOL(op1) != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_SCALAR_DOUBLE ) ( rop, op1, dptr2[0], rnd );
        }
        else
        {
            if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || MPS_NUMCOL(op1) != MPS_NUMCOL(rop) ||
                (unsigned)m2 != MPS_NUMROW(rop) || (unsigned)n2 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_FUNCTION_DOUBLE ) ( rop, op1, dptr2, 0, rnd );
        }
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix, sci_matrix ) ||
             sig == MpsSig( sci_mlist, sci_matrix, sci_matrix, sci_strings ) )
    {
        /* MPS_FUNC( MPS, DOUBLE, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        if( m1 == 1 && n1 == 1 )
        {
            if( (unsigned)m2 != MPS_NUMROW(rop) || (unsigned)n2 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE ) ( rop, dptr2, 0, dptr1[0], rnd );  
        }
        else if( m2 == 1 && n2 == 1 )
        {
            if( (unsigned)m1 != MPS_NUMROW(rop) || (unsigned)n1 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE ) ( rop, dptr1, 0, dptr2[0], rnd );
        }
        else
        {
            if( (unsigned)m1 != MPS_NUMROW(rop) || (unsigned)n1 != MPS_NUMCOL(rop) ||
                (unsigned)m2 != MPS_NUMROW(rop) || (unsigned)n2 != MPS_NUMCOL(rop) )
            {
                sciprint("%s: Inconsistent matrix operation.\n",fname);
                return 0;
            }
            XMB_FUNC( MPS_DOUBLE_FUNCTION_DOUBLE ) ( rop, dptr1, 0, dptr2, 0, rnd );
        }
    }
    else
    {
        if( Rhs > 4 || Rhs < 2 )
        {
            sciprint("%s: Wrong number of input arguments: 2 to 4 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

#undef SCI_FUNCTION
#undef MPS_FUNCTION
#undef MPS_FUNCTION_DOUBLE
#undef MPS_DOUBLE_FUNCTION_DOUBLE
#undef MPS_FUNCTION_SCALAR
#undef MPS_FUNCTION_SCALAR_DOUBLE
#undef MPS_DOUBLE_FUNCTION_SCALAR_DOUBLE

#undef XMB_SCI_FUNC_DEF
#undef XMB_FUNC

