/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"

/*
 * Return the precision of an mps matrix.
 * Scilab definition :
 * rop = mps_size( op )
 * rop : Result operand, 1x1 matrix of double.
 * op : Input multi-precision matrix.
 */

int sci_mps_prec( char *fname, unsigned long fname_len )
{
    int *arg1;
    int m, n;
    mps_ptr op;
    double* dptr;
    int stkadd;

    int rnum;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(1,1);
    CheckLhs(1,1);

    /* Get address of input. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

    rnum = MpsIsValid( fname, arg1, 1 );
    if( rnum != 0 )
        return 0;

    op = MpsGetMatrix( 1 );
    
    m = 1;
    n = 1;

    CreateVar( Rhs+1, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &stkadd );

    dptr = stk(stkadd);

    m = MPS_PREC(op);

    dptr[0] = m;

    LhsVar(1) = Rhs+1;

    return 0;
}
