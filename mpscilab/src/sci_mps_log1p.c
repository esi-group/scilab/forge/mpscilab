/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"


/* 
 * This function is constructed by including a "template" defined in x_macro_a.c
 * , see the aforementioned file for details.
 */

#define SCI_FUNCTION sci_mps_log1p
#define MPS_FUNCTION mps_log1p

#include "x_macro_a.c"

