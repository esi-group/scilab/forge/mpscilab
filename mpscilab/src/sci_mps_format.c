/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

extern int g_scimps_format; /* 'v' = 0; 'e' = 1; 'i' = 2 */
extern int g_scimps_width;

int sci_mps_format( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2 = NULL;
    SciErr sciErr;
    int m, n;
    int pilen[1];
    double *dptr2;
    char fmtchar[2] = "\0";
    int typearg1, typearg2;


    CheckRhs(0,2);
    CheckLhs(1,1);

    if( Rhs == 0 )
    {
        sciErr = allocMatrixOfDouble(pvApiCtx, Rhs + 1, 1, 2, &dptr2 );

        dptr2[0] = g_scimps_format;
        dptr2[1] = g_scimps_width;

        LhsVar(1) = Rhs + 1;
    }
    else if( Rhs == 1 )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarType( pvApiCtx, arg1, &typearg1 );

        if( typearg1 != sci_matrix )
        {
            sciprint("%s: Wrong type for argument 1. 1x2 matrix expected.\n",fname);
            return 0;
        }

        if( dptr2[0] < 0 || dptr2[0] > 2 )
        {
            sciprint( "%s: Invalid mode specified.\n", fname );
            return 0;
        }

        if( dptr2[1] < 5 )
        {
            sciprint( "%s: Minimum width is 5.\n", fname );
            return 0;
        }

        if( dptr2[1] > 1000 )
        {
            sciprint( "%s: Maximum width is 1000.\n", fname );
            return 0;
        }

        g_scimps_format = dptr2[0];

        if( dptr2[0] != 2 )
            g_scimps_width = dptr2[1];
    }
    else
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarType( pvApiCtx, arg1, &typearg1 );
        getVarType( pvApiCtx, arg2, &typearg2 );

        if( typearg1 != sci_strings )
        {
            sciprint("%s: Wrong type for argument 1. Single character string expected.\n",fname);
            return 0;
        }

        if( typearg2 != sci_matrix )
        {
            sciprint("%s: Wrong type for argument 2. Scalar integer expected.\n",fname);
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg1, &m, &n, NULL, NULL );
        if( m != 1 || n != 1 )
        {
            sciprint( "%s: Wrong size for argument 1. Single character string expected.\n", fname );
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg1, &m, &n, pilen, NULL );
        if( pilen[0] != 1 )
        {
            sciprint( "%s: Wrong size for argument 1. Single character string expected.\n", fname );
            return 0;
        }

        GetRhsStringVar( 1, &m, &n, pilen, fmtchar );
        getMatrixOfDouble( pvApiCtx, arg2, &m, &n, &dptr2 );
        if( m != 1 || n != 1 )
        {
            sciprint( "%s: Wrong size for argument 2. Scalar integer expected.\n", fname );
            return 0;
        }

        if( dptr2[0] < 5 )
        {
            sciprint( "%s: Minimum width is 5.\n", fname );
            return 0;
        }

        if( dptr2[0] > 1000 )
        {
            sciprint( "%s: Maximum width is 1000.\n", fname );
            return 0;
        }

        if( fmtchar[0] == 'v' )
            g_scimps_format = 0;
        else if( fmtchar[0] == 'e' )
            g_scimps_format = 1;
        else if( fmtchar[0] == 'i' )
            g_scimps_format = 2;
        else
        {
            sciprint( "%s: Invalid mode specified.\n", fname );
            return 0;
        }
        
        if( fmtchar[0] != 'i' )
            g_scimps_width = dptr2[0];

    }

    return 0;
}
