/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_acos( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3;
    int m1, n1;
    mps_ptr rop, op1;
    mp_rnd_t rnd = GMP_RNDN;
    int rnum, typearg2;
    double *dptr1;
    char rndchar[2];
    int pilen[1];

    arg3 = NULL;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(2,3);
    CheckLhs(1,1);

    /* Get address of inputs */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

    /* If there is a fourth argument, get it. */
    if( Rhs == 3 )
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

    /* Check the size and type of the arguments. */
    rnum = MpsCheck2( fname, arg1, arg2, arg3 );

    if( rnum != 0 )
        return 0;

    /* Get the rounding mode. */
    if( Rhs == 3 )
    {
        GetRhsStringVar( 3 , &m1, &n1, pilen, rndchar );
        if( rndchar[0] == 'n' )
            rnd = GMP_RNDN;
        else if( rndchar[0] == 'z' )
            rnd = GMP_RNDZ;
        else if( rndchar[0] == 'u')
            rnd = GMP_RNDU;
        else if( rndchar[0] == 'd' )
            rnd = GMP_RNDD;
        else
        {
            sciprint("%s: Invalid rounding mode specified. Valid options : 'n', 'z', 'u', 'd'.\n",fname);
            return 0;
        }
    }

    /* Fetch the result operand. */
    rop = MpsGetMatrix( 1 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    if( typearg2 == sci_mlist )
    {
        op1 = MpsGetMatrix( 2 );
        mps_acos( rop, op1, rnd );
    }
    else if( typearg2 == sci_matrix )
    {
        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        mps_acos_double( rop, dptr1, 0, rnd );
    }

    return 0;
}
