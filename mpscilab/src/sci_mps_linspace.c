/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

/*
 * mps_linspace( rop, start, end )
 * rop = mps_linspace( rop, start, end, n, prec )
 */

int sci_mps_linspace( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4;
    int typearg1, typearg2, typearg3, typearg4;
    int m1, n1, m2, n2, m3, n3, m4, n4;
    mps_ptr rop, op1, op2, op3;
    mps_t mps;
    int StkAdd;
    double *dptr1, *dptr2, *dptr3, *dptr4;

    if( Rhs > 4 || Rhs < 3 )
    {
        sciprint("%s: Wrong number of input argument(s): 3 to 4 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarType( pvApiCtx, arg2, &typearg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
    getVarType( pvApiCtx, arg3, &typearg3 );

    if( Rhs == 4 )
    {
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );
    }


    if( Rhs == 3 ) /* mps_linspace( rop, start, end ) */
    {
        if( typearg1 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
            return 0;
        }

        rop = MpsGetMatrix( 1 );

        if( typearg2 == sci_mlist && typearg3 == sci_mlist )
        {
            op2 = MpsGetMatrix( 2 );
            op3 = MpsGetMatrix( 3 );

            if( MPS_NUMROW(op2) != 1 || MPS_NUMCOL(op2) != 1 )
            {
                sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(op3) != 1 || MPS_NUMCOL(op3) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }
            
            mps_linspace( rop, op2, op3 );
        }
        else if( typearg2 == sci_matrix && typearg3 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );
            getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );

            if( m2 != 1 || n2 != 1 )
            {
                sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
                return 0;
            }

            if( m3 != 1 || n3 != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            mps_linspace_double( rop, dptr2[0], dptr3[0] );
        }
        else
        {
            sciprint("%s: Invalid input type for arguments 2 and 3. Scilab double scalar or multi-precision scalar expected.\n",fname);
            return 0;
        }

    }
    else /* mps_linspace( start, end, n, prec ) */
    {
        if( typearg3 != sci_matrix )
        {
            sciprint("%s: Invalid input type for argument 3. Integer scalar expected.\n",fname);
            return 0;
        }

        if( typearg4 != sci_matrix )
        {
            sciprint("%s: Invalid input type for argument 4. Integer scalar expected.\n",fname);
            return 0;
        }

        getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );
        getMatrixOfDouble( pvApiCtx, arg4, &m4, &n4, &dptr4 );

        if( m3 != 1 || n3 != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
            return 0;
        }

        if( m4 != 1 || n4 != 1 )
        {
            sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
            return 0;
        }

        if( dptr3[0] < 1 )
        {
            sciprint("%s: Length must be strictly positive.\n",fname);
            return 0;
        }

        if( dptr4[0] < 2 )
        {
            sciprint("%s: Precision must be 2 bits or higher.\n",fname);
            return 0;
        }

        if( typearg1 == sci_mlist && typearg2 == sci_mlist )
        {
            op1 = MpsGetMatrix( 1 );
            op2 = MpsGetMatrix( 2 );

            if( MPS_NUMROW(op1) != 1 || MPS_NUMCOL(op1) != 1 )
            {
                sciprint("%s: Wrong size for argument 1. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(op2) != 1 || MPS_NUMCOL(op2) != 1 )
            {
                sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
                return 0;
            }

            mps_init( mps, 1, dptr3[0], dptr4[0], 0 );
            mps_linspace( mps, op1, op2 );
        }
        else if( typearg1 == sci_matrix && typearg2 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
            getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

            if( m1 != 1 || n1 != 1 )
            {
                sciprint("%s: Wrong size for argument 1. Scalar expected.\n",fname);
                return 0;
            }

            if( m2 != 1 || n2 != 1 )
            {
                sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
                return 0;
            }

            mps_init( mps, 1, dptr3[0], dptr4[0], 0 );
            mps_linspace_double( mps, dptr1[0], dptr2[0] );
        }
        else
        {
            sciprint("%s: Invalid input type for arguments 1 and 2. Scilab double scalar or multi-precision scalar expected.\n",fname);
            return 0;
        }

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }

    return 0;
}

