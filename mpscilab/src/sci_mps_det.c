/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_det( char *fname, unsigned long fname_len )
{
    unsigned int sig;
    mps_ptr rop, op, tmp;
    mps_t mps, mpstmp;
    int ret;
    int StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist ) )
    {
        op = MpsGetMatrix( 1 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        ret = mps_init( mpstmp, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        ret = mps_init( mps, 1, 1, MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_copy( mpstmp, op );

        mps_det_iplup( mps, mpstmp );

        mps_free( mpstmp );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for output argument. Scalar expected.\n",fname);
            return 0;
        }

        ret = mps_init( mpstmp, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_copy( mpstmp, op );

        mps_det_iplup( rop, mpstmp );

        mps_free( mpstmp );
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );
        tmp = MpsGetMatrix( 3 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for output argument. Scalar expected.\n",fname);
            return 0;
        }

        if( (MPS_NUMROW(op) != MPS_NUMROW(tmp)) || (MPS_NUMCOL(op) != MPS_NUMCOL(tmp)) )
        {
            sciprint("%s: Inconsistent matrix operation. Second and third arguments differ in size.\n",fname);
            return 0;
        }

        if( !mps_isalias( tmp, op ) )
            mps_copy( tmp, op );

        mps_det_iplup( rop, tmp );
    }
    else
    {
        if( Rhs > 3 || Rhs < 1 )
        {
            sciprint("%s: Wrong number of input arguments: 1 to 3 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

