/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"

/*
 * Element-wise division.
 * Scilab definition :
 * rop = mps_todouble( op, [rnd] )
 * rop : Result operand, matrix of double.
 * op : Input multi-precision matrix.
 * rnd : Optional rounding mode. One character from the following :
 * 'n' : Round to nearest
 * 'z' : Round toward 0 (Truncate)
 * 'u' : Round toward +infinity
 * 'd' : Round toward -infinity
 */
int sci_mps_todouble( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2;
    int m, n;
    mps_ptr op;
    double* dptr;
    int stkadd;
    mp_rnd_t rnd = GMP_RNDN;

    int rnum;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(1,2);
    CheckLhs(1,1);

    /* Get address of input. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

    rnum = MpsIsValid( fname, arg1, 1 );
    if( rnum != 0 )
        return 0;

    if( Rhs == 2 )
    {
        getVarAddressFromPosition( pvApiCtx,2, &arg2 );
        rnum = MpsGetRoundingArg( fname, &rnd, arg2, 2 );
        if( rnum != 0 )
            return 0;
    }

    op = MpsGetMatrix( 1 );

    m = MPS_NUMROW(op);
    n = MPS_NUMCOL(op);

    CreateVar( Rhs+1, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &stkadd );

    dptr = stk(stkadd);

    mps_double_output( dptr, op, 0, rnd );

    LhsVar(1) = Rhs+1;

    return 0;
}

