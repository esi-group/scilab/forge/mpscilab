/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <string.h>

#include "MALLOC.h"
#include "Scierror.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_list.h"

#include "scimps.h"

/* Create a Scilab stack entry from an existing mps_t matrix. */
int MpsCreateVarFrom( int stkpos, mps_t mps, int *stkadd  )
{
	int m;
	int n;
    /*int elem1;
    int elem2;
    int pilen[1] = {8};
    int rnum;*/
	int lr = 1;
	int lar = -1;

    /*mpx_stack();*/

    /*double* dptr;*/

    static char * ListType[] = {"MPSLIST"};
    /*char* strptr;*/

    /* Create a new tlist in the Scilab stack. */
    /* stkadd = iAllocTList( stkpos, 2 ); */
    m = 2;
    n = 1;
    CreateVar(stkpos, MATRIX_ORIENTED_TYPED_LIST_DATATYPE, &m,  &n, stkadd);

#if(0) /* The new api seem broken for string item. */
    /* Set the type of the tlist */
    /* This will be used by the Scilab overloading framework
     to differentiate between different lists. */
    rnum = iListAllocString( stkpos, stkadd, 1, 1, 1, pilen, &strptr );

    if( rnum != 0 )
    {
      sciprint("Error inserting string in new TList. Return value = %d\n",rnum);
      return 1;
    }

    iGetListItemString( stkpos, 1, int &m, &n, pilen, strptr );

    strncpy( strptr, "MPSLIST", 8 );

    /* Evaluate the size of the mps_t struct in term of double. */
    m = (sizeof(mps_t) + 1) / 8;
    n = 1;

    /* Create a matrix of double into the previously created tlist, */
    /*iListAllocMatrixOfDouble( stkpos, stkadd, 2, m, n, &dptr);*/

    /* Then memcpy the mps_t struc into it. */
    /*iGetListItemDouble( stkpos, 2, &n, &m, &dptr, NULL );*/
    /*memcpy( (void*)dptr, (void*)mps, sizeof(mps) );*/
#endif

    /* Set the type of the mlist */
    /* This will be used by the Scilab overloading framework
        to differentiate between different lists. */
    m = 1;
    n = 1;
    CreateListVarFromPtr( stkpos, 1, "S", &m,  &n,  ListType );

    /* Evaluate the size of the mps_t struct in term of double. */
    m = (sizeof(mps_t) + 1) / 8;
    n = 1;

    /* Create a matrix of double into the previously created mlist, */
    CreateListVarFrom( stkpos, 2, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &lr, &lar );
    /* Then memcpy the mps_t struc into it. */
    memcpy( stk(lr), (void*)mps, sizeof(mps_t) );

    return 0;
}

/* Get the matrix from a typed list.  */
mps_ptr MpsGetMatrix( int stkpos )
{
    int m,n;
    int *addr;
    mps_ptr ptr = NULL;

//    iGetListItemDouble( stkpos, 2, &m, &n, (double**)&ptr, NULL);

    getVarAddressFromPosition( pvApiCtx, stkpos, &addr );

    getMatrixOfDoubleInList( pvApiCtx, addr, 2, &m, &n, (double**)&ptr );

    return ptr;
}

/* Get the matrix from a typed list address. */
mps_ptr MpsGetMatrixFromAddr( int *stkadd )
{
    int m, n;
    mps_ptr ptr = NULL;
    int* dptr;

    getListItemAddress( pvApiCtx, stkadd, 2, &dptr );

    getMatrixOfDouble( pvApiCtx, dptr, &m, &n, (double**)&ptr );

    return ptr;
}

int MpsCollect()
{
    int i;
    int Type, pos;
    mps_ptr mps;
    mps_alloc_ptr ptr;
    int isiz = C2F(vstk).isiz;
    int count = mps_alloc_list_size();
    mps_alloc_ptr *AllocList = calloc(count, sizeof(mps_alloc_ptr));

    for( i = 0; i <= isiz; i++ )
    {
        Type = *istk(iadr(*Lstk(isiz-i)));

        if( Type < 0 )
            Type = *istk(iadr(*Lstk(isiz-i))+1);

        if( Type == 17 )
        {
            mps = MpsGetMatrixFromAddr( istk(iadr(*Lstk(isiz-i))) );
            if( mps != 0 )
            {
                pos = mps_position( mps );
                if( pos >= 0 )
                    AllocList[pos] = (mps_alloc_ptr)1;
            }
        }
            
    }

    for( i = 0; i < count; i++ )
    {
        if( AllocList[i] == NULL )
        {
            ptr = mps_return_alloc_pos( i );
            AllocList[i] = ptr;
        }
        else
            AllocList[i] = NULL;
    }

    for( i = 0; i < count; i++ )
    {
        if( AllocList[i] != NULL )
            mps_free_alloc( AllocList[i] );
    }

    free(AllocList);
    return 0;
}


int mpx_stack()
{
    int i;
    int Type;
    int isiz = C2F(vstk).isiz;
    sciprint("Stack Info: Top: %d Bot: %d Fin: %d isiz: %d \n", Top, Bot, Fin, C2F(vstk).isiz);

    for( i = 0; i <= isiz; i++ )
    {
        Type = *istk(iadr(*Lstk(isiz-i)));

        if( Type < 0 )
            Type = *istk(iadr(*Lstk(isiz-i))+1);

        if( Type == 17 )
            sciprint("Position: %d Type : %d Addr: %d \n",isiz-i,  Type, *Lstk(isiz-i) );
    }

    return 0;
}
