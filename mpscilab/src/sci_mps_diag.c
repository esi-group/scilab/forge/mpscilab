/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_diag( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3 = NULL;
    int typearg1, typearg2, typearg3 = 0;
    int m2, n2, m3, n3;
    double *dptr2, *dptr3;
    mps_ptr rop, op = NULL;
    int diag = 0;
    unsigned int rlen, dim;

    if( Rhs > 3 || Rhs < 2 )
    {
        sciprint("%s: Wrong number of input argument(s): 2 to 3 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    if( typearg1 != sci_mlist )
    {
        sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
        return 0;
    }

    rop = MpsGetMatrix( 1 );

    if( Rhs == 3 )
    {
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );

        if( typearg3 != sci_matrix )
        {
            sciprint("%s: Wrong type for argument 3. Scilab scalar expected.\n",fname);
            return 0;
        }

        getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );

        if( m3 != 1 || n3 != 1 )
        {
          sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
          return 0;
        }

        diag = dptr3[0];
    }

    if( typearg2 == sci_mlist )
    {
        op = MpsGetMatrix( 2 );
        m2 = MPS_NUMROW(op);
        n2 = MPS_NUMCOL(op);
    }
    else if( typearg2 == sci_matrix )
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );
    else    
    {
        sciprint("%s: Wrong type for argument 2. Multi-precision or Scilab matrix expected.\n",fname);
        return 0;
    }

    if( MPS_NUMROW(rop) == 1 || MPS_NUMCOL(rop) == 1 )
    {
        if( !((diag == 0) || (diag > 0 && diag < n2) || (diag < 0 && -diag < m2)) )
        {
            sciprint("%s: Diagonal index out of bound.\n",fname);
            return 0;
        }

        dim = m2 <= n2 ? m2 : n2;

        if( diag >= 0 )
            rlen = (unsigned)(n2 - diag) > dim ? dim : (unsigned)(n2 - diag);
        else
            rlen = (unsigned)(m2 + diag) > dim ? dim : (unsigned)(m2 + diag);

        if( rlen != MPS_SIZE(rop) )
        {
            sciprint("%s: Wrong length for argument 1.\n",fname);
            return 0;
        }        
        
        if( typearg2 == sci_mlist )
            mps_diag_get( rop, op, diag, GMP_RNDN );
        else
            mps_diag_get_double( rop, dptr2, MPS_COL_ORDER, m2, n2, diag, GMP_RNDN );
    }
    else if( m2 == 1 || n2 == 1 )
    {
        if( !((diag == 0) || (diag > 0 && (unsigned)diag < MPS_NUMCOL(rop) ) || (diag < 0 && (unsigned)-diag < MPS_NUMROW(rop) )) )
        {
            sciprint("%s: Diagonal index out of bound.\n",fname);
            return 0;
        }

        dim = MPS_NUMROW(rop) <= MPS_NUMCOL(rop) ? MPS_NUMROW(rop) : MPS_NUMCOL(rop);

        if( diag >= 0 )
            rlen = MPS_NUMCOL(rop) - diag > dim ? dim : MPS_NUMCOL(rop) - diag;
        else
            rlen = MPS_NUMROW(rop) + diag > dim ? dim : MPS_NUMROW(rop) + diag;

        if( rlen != (unsigned)(m2*n2) )
        {
            sciprint("%s: Wrong size for argument 1.\n",fname);
            return 0;
        }      

        if( typearg2 == sci_mlist )
            mps_diag_set( rop, op, diag, GMP_RNDN );
        else
            mps_diag_set_double( rop, dptr2, diag, GMP_RNDN );    
    }
    else
    {
        sciprint("%s: Inconsistent matrix operation.\n",fname);
        return 0;
    }

    return 0;
}
