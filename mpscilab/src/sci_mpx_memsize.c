/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Return the size in memory of an MPS matrix.
 * Scilab definition :
 * res = mpx_memsize( op );
 */

int sci_mpx_memsize( char *fname, unsigned long fname_len )
{
    double size;
    int typearg;
    int *arg;
    mps_ptr op;

    CheckRhs(1,1);
    CheckLhs(1,1);

    getVarAddressFromPosition( pvApiCtx, 1, &arg );
    getVarType( pvApiCtx, arg, &typearg );

    if( typearg != sci_mlist )
    {
        sciprint( "%s: Wrong type for argument 1. Multi-precision matrix expected.\n", fname );
        return 0;        
    }

    op = MpsGetMatrix( 1 );

    size = MPS_ALLOC_SIZE(op) + MPS_LIMBS_ALLOC_SIZE(op);

    createScalarDouble( pvApiCtx, Rhs+1, size );

    LhsVar(1) = Rhs + 1;

    return 0;
}
