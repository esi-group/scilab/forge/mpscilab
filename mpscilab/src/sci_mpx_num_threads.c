/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Get or set the global thread limit used by mplib.
 */

int sci_mpx_num_threads( char *fname, unsigned long fname_len )
{
    int typearg;
    int *arg;
    int m, n;
    double *dptr;

    CheckRhs(0,1);
    CheckLhs(1,1);

    if( Rhs == 0 )
    {
        createScalarDouble( pvApiCtx, Rhs+1, mps_get_num_threads() );
        LhsVar(1) = Rhs + 1;
    }
    else
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg );
        getVarType( pvApiCtx, arg, &typearg );

        if( typearg != sci_matrix )
        {
            sciprint( "%s: Wrong type for argument 1. Scalar integer expected.\n", fname );
            return 0;   
        }
        
        getMatrixOfDouble( pvApiCtx, arg, &m, &n, &dptr );

        if( m != 1 || n != 1 )
        {
            sciprint( "%s: Wrong size for argument 1. Scalar integer expected.\n", fname );
            return 0;   
        }        

        if( dptr[0] < 0 )
        {
            sciprint( "%s: Number of thread must be positive or 0.\n", fname );
            return 0;  
        }

        mps_set_num_threads( dptr[0] );

    }

    return 0;
}

