/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* 
 * X-Macro for functions like  :
 * mps_func( rop, op [, orientation] )
 * rop = mps_func( op [, orientation] )
 *
 * Two "arguments" are required :
 * SCI_FUNCTION : Function name as it would be defined in a stand-alone c file.
 * MPS_FUNCTION : mp_lib function to map this function to.
 *
 * Functions using this macro :
 * sci_mps_sum
 * sci_mps_prod
 * sci_mps_mean
 */

#ifndef SCI_FUNCTION
    #error "SCI_FUNCTION is not defined in invocation of x_macro_e.c"
#endif

#ifndef MPS_FUNCTION
    #error "MPS_FUNCTION is not defined in invocation of x_macro_e.c"
#endif

#ifndef MPS_FUNCTION_COLUMN
    #error "MPS_FUNCTION_COLUMN is not defined in invocation of x_macro_e.c"
#endif

#ifndef MPS_FUNCTION_ROW
    #error "MPS_FUNCTION_ROW is not defined in invocation of x_macro_e.c"
#endif

#define XME_SCI_FUNC_DEF( FUNC ) int FUNC ( char *fname, unsigned long fname_len )
#define XME_FUNC( FUNC ) FUNC


XME_SCI_FUNC_DEF( SCI_FUNCTION )
{
    int *arg;
    int m, n;
    int pilen[1];
    char* opptr[1];
    char opchar[2] = "*";
    unsigned int sig;
    mps_ptr rop, op;
    mps_t mps;
    int ret, StkAdd;

    sig = MpsGetSig();


    if( sig == MpsSig( sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, Rhs, &arg );

        getMatrixOfString( pvApiCtx, arg, &m, &n, NULL, NULL );

        if( m != 1 || n != 1 )
        {
            sciprint("%s: Wrong size for orientation. Single character string expected.\n",fname);
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg, &m, &n, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong length for orientation. Single character string expected.\n",fname);
            return 0;
        }

        opptr[0] = opchar;
        getMatrixOfString( pvApiCtx, arg, &m, &n, pilen, opptr );

        if( !(opchar[0] == '*' || opchar[0] == 'r' || opchar[0] == 'c') )
        {
            sciprint("%s: Invalid operation specified. Valid options : 'r', 'c', '*'.\n",fname);
            return 0;
        }
    }


    if( sig == MpsSig( sci_mlist ) ||
        sig == MpsSig( sci_mlist, sci_strings ) )
    {
        op = MpsGetMatrix( 1 );

        if( opchar[0] == '*' )
        {
            MpsCollect();
            ret = mps_init( mps, 1, 1, MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION ) ( mps, op );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
        else if( opchar[0] == 'r' )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op), 1, MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION_ROW ) ( mps, op );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
        else if( opchar[0] == 'c' )
        {
            MpsCollect();
            ret = mps_init( mps, 1, MPS_NUMCOL(op), MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION_COLUMN ) ( mps, op );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( opchar[0] == '*' )
        {
            if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
            {
                sciprint("%s: Wrong size for the output operand. Scalar expected.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION ) ( rop, op );
        }
        else if( opchar[0] == 'r' )
        {
            if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMROW(op) )
            {
                sciprint("%s: Wrong size for the output operand.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION_ROW ) ( rop, op );
        }
        else if( opchar[0] == 'c' )
        {
            if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMCOL(op) )
            {
                sciprint("%s: Wrong size for the output operand.\n",fname);
                return 0;
            }

            XME_FUNC( MPS_FUNCTION_COLUMN ) ( rop, op );
        }
    }
    else
    {
        if( Rhs < 1 || Rhs > 3 )
        {
            sciprint("%s: Wrong number of input arguments: 1 to 3 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}


#undef SCI_FUNCTION
#undef MPS_FUNCTION
#undef MPS_FUNCTION_COLUMN
#undef MPS_FUNCTIONS_ROW

#undef XME_SCI_FUNC_DEF
#undef XME_FUNC

