/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_iplu( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2;
    int typearg1, typearg2;
    mps_ptr rop, op;

    if( Rhs > 3 || Rhs < 1 )
    {
        sciprint("%s: Wrong number of input argument(s): 1 to 3 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarType( pvApiCtx, arg1, &typearg1 );

    if( typearg1 != sci_mlist )
    {
        sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
        return 0;
    }

    rop = MpsGetMatrix( 1 );

    if( Rhs >= 2 )
    {
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarType( pvApiCtx, arg2, &typearg2 );

        if( typearg2 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 2. Multi-precision matrix expected.\n",fname);
            return 0;
        }

        op = MpsGetMatrix( 2 );
    }

    if( Rhs >= 2 )
    {
        if( (MPS_NUMROW(rop) != MPS_NUMROW(op)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 0;
        }

        if( mps_isalias( rop, op ) == 0 )
            mps_copy( rop, op );
    }

    if( MPS_NUMROW(rop) != MPS_NUMCOL(rop) )
    {
        sciprint("%s: Input matrix must be square.\n",fname);
        return 0;
    }

    mps_iplu_decomp( rop );

    return 0;
}
