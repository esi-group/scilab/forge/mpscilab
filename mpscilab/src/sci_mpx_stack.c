/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Return the number of allocated mps matrices.
 * Scilab definition :
 * res = mpx_alloc_count( );
 * res : Number of allocated matrices.
 */

int sci_mpx_stack( char *fname, unsigned long fname_len )
{
    int i;
    int Type;
    int isiz = C2F(vstk).isiz;
    mps_ptr mps;
    sciprint( "Stack Info: Top: %d Bot: %d Fin: %d isiz: %d \n", Top, Bot, Fin, C2F(vstk).isiz );
    sciprint( "MPS stats: Count: %u \n", mps_alloc_list_size() );

    for( i = 0; i <= isiz; i++ )
    {
        Type = *istk(iadr(*Lstk(isiz-i)));
//        Addr = istk(iadr(*Lstk(isiz-i)));

//        if( Type < 0 )
//            Type = *istk(iadr(*Lstk(isiz-i))+1);

        if( Type == 17 )
        {
            mps = MpsGetMatrixFromAddr( istk(iadr(*Lstk(isiz-i))) );
            if( mps != 0 )
                sciprint("Position: %d Type : %d Pos: %u \n",isiz-i, Type, mps_position(mps) );
        }
    }

    MpsCollect();

    return 0;
}
