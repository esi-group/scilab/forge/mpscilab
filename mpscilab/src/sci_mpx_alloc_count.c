/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Return the number of allocated mps matrices.
 * Scilab definition :
 * res = mpx_alloc_count( );
 * res : Number of allocated matrices.
 */

int sci_mpx_alloc_count( char *fname, unsigned long fname_len )
{
    unsigned int count;

    CheckRhs(0,0);
    CheckLhs(1,1);

    count = mps_alloc_list_size();

    createScalarUnsignedInteger32( pvApiCtx, Rhs+1, count );

    LhsVar(1) = Rhs + 1;

    return 0;
}
