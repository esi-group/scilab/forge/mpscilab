/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Get various static and runtime information about MPScilab.
 */

int sci_mpx_getdebuginfo( char *fname, unsigned long fname_len )
{
    char *outputstr;
    int count = 0;
    
    outputstr = malloc(1000);

    count += sprintf( outputstr, "GMP header version : %d.%d.%d\n",
                        __GNU_MP_VERSION, __GNU_MP_VERSION_MINOR, __GNU_MP_VERSION_PATCHLEVEL );

    count += sprintf( &outputstr[count], "GMP runtime version : %s\n", gmp_version );

    count += sprintf( &outputstr[count], "MPFR header version : %s\n", MPFR_VERSION_STRING );

    count += sprintf( &outputstr[count], "MPFR runtime version : %s\n", mpfr_get_version() );

    count += sprintf( &outputstr[count], "Precision field width : %ld\n", sizeof(mpfr_prec_t) );
    count += sprintf( &outputstr[count], "Sign field width : %ld\n", sizeof(mpfr_sign_t) );
    count += sprintf( &outputstr[count], "Exponent field width : %ld\n", sizeof(mpfr_exp_t) );
    count += sprintf( &outputstr[count], "Limb size : %ld\n", sizeof(mp_limb_t) );
    count += sprintf( &outputstr[count], "MPFR header struct size : %ld\n", sizeof(__mpfr_struct) );
    count += sprintf( &outputstr[count], "MPS header struct size : %ld\n", sizeof(___mps_struct) );
    count += sprintf( &outputstr[count], "MPS alloc struct size : %ld\n", sizeof(___mps_alloc_struct) );

    sciprint( "%s", outputstr );

    free(outputstr);

    return 0;
}
