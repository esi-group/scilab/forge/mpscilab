/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"

/*
 * Init a multi-precision matrix.
 * Scilab definition :
 * rop = mps_init( m, n, prec )
 * rop : Result operand, zero filled multi-precision matrix.
 * m, n : Size of the matrix.
 * prec : Precision of the created matrix.
 */
int sci_mps_init( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3;
	int typearg;
    int m1, n1, m2, n2, m3, n3;
    double *dptr1, *dptr2, *dptr3;
    mps_t mpsvar;
    int StkAdd;
    int rnum;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(3,3);
    CheckLhs(1,1);

    /* Get Address of inputs arguments. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

    /* Check that the input arguments are of the correct type. */
	getVarType(pvApiCtx, arg1, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 1. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType(pvApiCtx, arg2, &typearg );
    if( typearg != sci_matrix )
    {
      sciprint("%s: Wrong type for argument 2. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType(pvApiCtx, arg3, &typearg );
    if( typearg != sci_matrix )
    {
      sciprint("%s: Wrong type for argument 3. Scalar double expected.\n",fname);
      return 0;
    }

    /* Retrieve the rhs arguments. */
    getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
    getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );
    getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );

    /* Check that they are of the correct size. */
    if( m1 != 1 || n1 != 1 )
    {
      sciprint("%s: Wrong size for argument 1. Scalar expected.\n",fname);
      return 0;
    }

    if( m2 != 1 || n2 != 1 )
    {
      sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
      return 0;
    }

    if( m3 != 1 || n3 != 1 )
    {
      sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
      return 0;
    }

    /* Bounds checking. */
    if( dptr1[0] < 1 )
    {
      sciprint("%s: Number of rows must be at least 1.\n",fname);
      return 0;
    }

    if( dptr2[0] < 1 )
    {
      sciprint("%s: Number of columns must be at least 1.\n",fname);
      return 0;
    }

    if( dptr3[0] < 2 )
    {
      sciprint("%s: Minimum precision is 2.\n",fname);
      return 0;
    }

    /* Run garbage collection first. */
    MpsCollect();

    /* Init an mps matrix. */
    mps_init( mpsvar, dptr1[0], dptr2[0], dptr3[0], 0 );

    /* Add the matrix to the Scilab stack. */
    rnum = MpsCreateVarFrom( 4, mpsvar, &StkAdd );

    /* Output the 4th stack position. */
    LhsVar(1) = 4;

    return 0;
}

