/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

/*
 * This file contains many argument cheking functions used by the toolbox. Note
 * that most of those functions are not used anymore and will be removed in the
 * future.
 */

/*
 * Out of desperation, instead of trying to figure out a clever way of naming
 * the functions it was decided to just name them MpsCheck1..2..3 and so on.
 * A short comment before the function implementation tells which arguments
 * and conditions the function is checking for.
 */

/*
 * MpsCheck1()
 *
 * Validate inputs of functions accepting two matrices of either the same size,
 * or with one of the arguments being a scalar. Used for example for matrix
 * addition where two matrices may be added together or a scalar can be added to
 * all the elements of a matrix. The output must be of the same size as the
 * relevent intput. The optional 4th argument is the rounding mode used for the
 * corresponding mpfr operation.
 *
 * arg1 : MPS matrix the same size as either of the non-scalar arg2 or arg3.
 * arg2 : MPS or double the same size as arg1 or scalar.
 * arg3 : MPS or double the same size as arg1 or scalar.
 * arg4 : String, Valid rounding mode indicator.
 */
int MpsCheck1( char* fname, int *arg1, int *arg2, int *arg3, int *arg4 )
{
    int typearg1, typearg2, typearg3;
    int m0, n0, m1, m2, n1, n2;
    mps_ptr rop, op1, op2;
    double *dptr1, *dptr2;
    int pilen[1];
    /*    StrErr strErr; */

    getVarType(pvApiCtx, arg1, &typearg1 );
    getVarType(pvApiCtx, arg2, &typearg2 );
    getVarType(pvApiCtx, arg3, &typearg3 );

    rop = MpsGetMatrix( 1 );

    /* Result operand must be a multi-precision matrix. */
    if(typearg1 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
      return 1;
    }

    /* Check the rounding argument for the correct type. */
    if( arg4 != NULL )
    {
		int typearg4;
		getVarType(pvApiCtx, arg4, &typearg4 );
        if( typearg4 != sci_strings )
        {
            sciprint("%s: Wrong type for argument 4. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg4, &m1, &n1, NULL, NULL );

        if( m1 != 1 || n1 != 1 )
        {
            sciprint("%s: Wrong size for argument 4. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg4, &m1, &n1, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong size for argument 4. Single character string expected.\n",fname);
            return 1;
        }
    }

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg2)
    {
        case sci_matrix:
            switch (typearg3)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
    		}
        case sci_mlist:
            switch (typearg3)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
            }
        default:
            sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
            return 1;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 3 );

        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 2 );
        op2 = MpsGetMatrix( 3 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);
        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    END:

    /* Fetch the size of the output operand. */
    m0 = MPS_NUMROW(rop);
    n0 = MPS_NUMCOL(rop);

    /*
     * Checking if the size of the arguments make sense.
     *
     * This could be compounded into one epic conditional line but it's easier
     * to read this way and allow pinpointing the exact argument at fault.
     */
    if( (m0 == m1) && (m0 == m2) && (n0 == n1) && (n0 == n2) )
    {
        return 0;
    }
    else if( (m1 == n1) && (m1 == 1) )
    {
        if( m0 != m2 || n0 != n2 )
        {
            sciprint("%s: Inconsistent matrix operation. Second input operand and output operands differ in size.\n",fname);
            return 1;
        }
    }
    else if( (m2 == n2) && (m2 == 1) )
    {
        if( m0 != m1 || n0 != n1 )
        {
            sciprint("%s: Inconsistent matrix operation. First input operand and output operands differ in size.\n",fname);
            return 1;
        }
    }
    else
    {
        sciprint("%s: Inconsistent matrix operation. Input operands differ in sizes.\n",fname);
        return 1;
    }

    return 0;
}

/*
 * MpsCheck2()
 *
 * Validate inputs of functions accepting one matrix which must be the same size
 * as the output operand. For example this may be used for mps_sin(). Also the
 * type of the input may be an mps matrix or a matrix of double.The optional 3rd
 * argument is the rounding mode used for the corresponding mpfr operation.
 *
 * arg1 : MPS matrix the same size as either of the non-scalar arg2 or arg3.
 * arg2 : MPS or double the same size as arg1 or scalar.
 * arg3 : String, Valid rounding mode indicator.
 */
int MpsCheck2( char* fname, int *arg1, int *arg2, int *arg3 )
{
    int typearg1, typearg2;
    int m0, n0, m1, n1;
    mps_ptr rop, op1;
    double *dptr1;
    int pilen[1];
    /*    StrErr strErr; */

    getVarType(pvApiCtx, arg1, &typearg1 );
    getVarType(pvApiCtx, arg2, &typearg2 );

    rop = MpsGetMatrix( 1 );

    /* Result operand must be a multi-precision matrix. */
    if(typearg1 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
      return 1;
    }

    /* Check the rounding argument for the correct type. */
    if( arg3 != NULL )
    {
		int typearg3;
		getVarType(pvApiCtx, arg3, &typearg3 );
        if( typearg3 != sci_strings )
        {
            sciprint("%s: Wrong type for argument 3. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg3, &m1, &n1, NULL, NULL );

        if( m1 != 1 || n1 != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg3, &m1, &n1, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Single character string expected.\n",fname);
            return 1;
        }
    }

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg2)
    {
        case sci_matrix:
            goto DOUBLE;
        case sci_mlist:
            goto TLIST;
        default:
            sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
            return 1;
    }

    /* Never place something here...*/

    DOUBLE: /* (double*) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );

    goto END;

    TLIST: /* (mps_t) */

        op1 = MpsGetMatrix( 2 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);

    END:

    /* Fetch the size of the output operand. */
    m0 = MPS_NUMROW(rop);
    n0 = MPS_NUMCOL(rop);

    /*
     * Checking if the size of the arguments make sense.
     *
     * This could be compounded into one epic conditional line but it's easier
     * to read this way and allow pinpointing the exact argument at fault.
     */
    if( (m0 == m1) && (n0 == n1) )
    {
        return 0;
    }
    else
    {
        sciprint("%s: Inconsistent matrix operation. Input and output operands differ in sizes.\n",fname);
        return 1;
    }

    return 0;
}

/*
 * MpsCheck3()
 *
 * Validate inputs of functions like matrix multiplication. Mainly check for
 * correct arguments type and that the multiplication is consistent.
 *
 * arg1 : MPS matrix the same size as either of the non-scalar arg2 or arg3.
 * arg2 : MPS or double.
 * arg3 : MPS or double.
 */
int MpsCheck3( char* fname, int *arg1, int *arg2, int *arg3 )
{
    int typearg1, typearg2, typearg3;
    int m0, n0, m1, n1, m2, n2;
    mps_ptr rop, op1, op2;
    double *dptr1, *dptr2;
    /*    StrErr strErr; */

    getVarType(pvApiCtx, arg1, &typearg1 );
    getVarType(pvApiCtx, arg2, &typearg2 );
    getVarType(pvApiCtx, arg3, &typearg3 );

    rop = MpsGetMatrix( 1 );

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg2)
    {
        case sci_matrix:
            switch (typearg3)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
    		}
        case sci_mlist:
            switch (typearg3)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
            }
        default:
            sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
            return 1;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 3 );

        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 2 );
        op2 = MpsGetMatrix( 3 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);
        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    END:

    /* Fetch the size of the output operand. */
    m0 = MPS_NUMROW(rop);
    n0 = MPS_NUMCOL(rop);

    /*
     * Checking if the size of the arguments make sense.
     */

    if( n1 != m2 )
    {
        sciprint("%s: Inconsistent matrix multiplication. Number of column of the first input operand differ "
                "from the number of row of the second intput argument\n",fname);
        return 1;
    }

    if( m0 != m1)
    {
        sciprint("%s: Inconsistent matrix operation. Wrong size for the output operand.",fname);
        return 1;
    }

    if( n0 != n2)
    {
        sciprint("%s: Inconsistent matrix operation. Wrong size for the output operand.",fname);
        return 1;
    }

    return 0;
}

/*
 * MpsCheck4()
 *
 * Validate inputs of functions accepting one matrix which must be the same size
 * as the output operand. For example this may be used for mps_sin(). Also the
 * type of the input may be an mps matrix or a matrix of double.The optional 3rd
 * argument is the rounding mode used for the corresponding mpfr operation.
 *
 * arg1 : MPS matrix or double.
 * arg2 : MPS or double the same size as arg1.
 */
int MpsCheck4( char* fname, int *arg1, int *arg2 )
{
    int typearg1, typearg2;
    int m1, n1, m2, n2;
    mps_ptr op1, op2;
    double *dptr1, *dptr2;


    getVarType(pvApiCtx, arg1, &typearg1 );
    getVarType(pvApiCtx, arg2, &typearg2 );

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg1)
    {
        case sci_matrix:
            switch (typearg2)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
                    return 1;
    		}
        case sci_mlist:
            switch (typearg2)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
                    return 1;
            }
        default:
            sciprint("%s: Wrong type for argument 1. Double or multi-precision matrix expected.\n",fname);
            return 1;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 2 );

        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 1 );
        op2 = MpsGetMatrix( 2 );

        m1 = MPS_NUMROW(op1);
        n1 = MPS_NUMCOL(op1);
        m2 = MPS_NUMROW(op2);
        n2 = MPS_NUMCOL(op2);

    END:

    /*
     * Checking if the size of the arguments make sense.
     *
     * This could be compounded into one epic conditional line but it's easier
     * to read this way and allow pinpointing the exact argument at fault.
     */
    if( (m1 == m2) && (n1 == n2) )
    {
        return 0;
    }
    else
    {
        sciprint("%s: Inconsistent matrix operation. Input operands differ in sizes.\n",fname);
        return 1;
    }

    return 0;
}

/*
 * Check that all the operands are of the same size.
 * Also validate the rounding mode argument.
 * Used for example for matrix addition.
 */
int MpsCheckSameSize( char* fname, int *arg1, int *arg2, int *arg3, int *arg4 )
{
    int typearg1, typearg2, typearg3;
    int m1, m2, n1, n2;
    mps_ptr rop, op1, op2;
    double *dptr1, *dptr2;
    int pilen[1];
    /* StrErr strErr; */

    getVarType(pvApiCtx, arg1, &typearg1 );
    getVarType(pvApiCtx, arg2, &typearg2 );
    getVarType(pvApiCtx, arg3, &typearg3 );

    rop = MpsGetMatrix( 1 );

    /* Result operand must be a multi-precision matrix. */
    if(typearg1 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
      return 1;
    }

    /* Check the rounding argument for the correct type. */
    if( arg4 != NULL )
    {
		int typearg4;
		getVarType(pvApiCtx, arg4, &typearg4 );
        if( typearg4 != sci_strings )
        {
            sciprint("%s: Wrong type for argument 4. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg4, &m1, &n1, NULL, NULL );

        if( m1 != 1 || n1 != 1 )
        {
            sciprint("%s: Wrong size for argument 4. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString(pvApiCtx, arg4, &m1, &n1, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong size for argument 4. Single character string expected.\n",fname);
            return 1;
        }
    }

	/* Find which situation we are in and jump to the corresponding case. */
    switch (typearg2)
    {
        case sci_matrix:
            switch (typearg3)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
    		}
        case sci_mlist:
            switch (typearg3)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    sciprint("%s: Wrong type for argument 3. Double or multi-precision matrix expected.\n",fname);
                    return 1;
            }
        default:
            sciprint("%s: Wrong type for argument 2. Double or multi-precision matrix expected.\n",fname);
            return 1;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        if( (m1 != m2) || (n1 != n2) )
        {
            sciprint("%s: Inconsistent matrix operation. Input operands must be of the same size.\n",fname);
            return 1;
        }

        if( (MPS_NUMROW(rop) != (unsigned)m1) || (MPS_NUMCOL(rop) != (unsigned)n1) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 1;
        }

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 3 );

        if( (MPS_NUMROW(op2) != (unsigned)m1) || (MPS_NUMCOL(op2) != (unsigned)n1) )
        {
            sciprint("%s: Inconsistent matrix operation. Input operands must be of the same size.\n",fname);
            return 1;
        }

        if( (MPS_NUMROW(rop) != (unsigned)m1) || (MPS_NUMCOL(rop) != (unsigned)n1) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 1;
        }

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        if( (MPS_NUMROW(op1) != (unsigned)m2) || (MPS_NUMCOL(op1) != (unsigned)n2) )
        {
            sciprint("%s: Inconsistent matrix operation. Input operands must be of the same size.\n",fname);
            return 1;
        }

        if( (MPS_NUMROW(rop) != (unsigned)m2) || (MPS_NUMCOL(rop) != (unsigned)n2) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 1;
        }

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 2 );
        op2 = MpsGetMatrix( 3 );

        if( (MPS_NUMROW(op1) != MPS_NUMROW(op2)) || (MPS_NUMCOL(op1) != MPS_NUMCOL(op2)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input operands must be of the same size.\n",fname);
            return 1;
        }

        if( (MPS_NUMROW(rop) != MPS_NUMROW(op2)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op2)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 1;
        }

    END:

    return 0;
}

/*
 * Check that all the operands are of the same size.
 * Also validate the rounging mode argument.
 * Used for example for element wise sin.
 */
int MpsCheckSameSize2( char* fname, int *arg1, int *arg2, int *arg3 )
{
    int typearg1, typearg2;
    int m1, n1;
    mps_ptr rop, op1;
    int pilen[1];

    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    rop = MpsGetMatrix( 1 );

    /* Result operand must be a multi-precision matrix. */
    if( typearg1 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
      return 1;
    }

    /* Check the rounding argument for the correct type. */
    if( arg3 != NULL )
    {
		int typearg3;
		getVarType( pvApiCtx, arg3, &typearg3 );
        if( typearg3 != sci_strings )
        {
            sciprint("%s: Wrong type for argument 3. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString( pvApiCtx, arg3, &m1, &n1, NULL, NULL );

        if( m1 != 1 || n1 != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Single character string expected.\n",fname);
            return 1;
        }

        getMatrixOfString( pvApiCtx, arg3, &m1, &n1, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong size for argument 3. Single character string expected.\n",fname);
            return 1;
        }
    }

    if( typearg2 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 2. Multi-precision matrix expected.\n",fname);
      return 1;
    }

    op1 = MpsGetMatrix( 2 );

    if( (MPS_NUMROW(rop) != MPS_NUMROW(op1)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op1)) )
    {
        sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
        return 1;
    }

    return 0;
}

/* Check if all the given arguments are mps matrices and of the same size. */
int MpsCheckAllSameMps( char *fname, int* arg1, int* arg2, int* arg3, int* arg4 )
{
    int m1, n1, m, n;
    mps_ptr op;

    m = 0;
    n = 0;

    if( arg1 != NULL )
    {
		int typearg1;
		getVarType( pvApiCtx, arg1, &typearg1 );
        if( typearg1 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
            return 1;
        }

        op = MpsGetMatrixFromAddr( arg1 );

        m = MPS_NUMROW(op);
        n = MPS_NUMCOL(op);
    }

    if( arg2 != NULL )
    {
		int typearg2;
		getVarType( pvApiCtx, arg2, &typearg2 );
        if( typearg2 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 2. Multi-precision matrix expected.\n",fname);
            return 1;
        }

        op = MpsGetMatrixFromAddr( arg2 );

        m1 = MPS_NUMROW(op);
        n1 = MPS_NUMCOL(op);

        if( m1 != m || n1 != n )
        {
            sciprint("%s: Wrong size for argument 2. Operand differ in size.\n",fname);
            return 1;
        }
    }

    if( arg3 != NULL )
    {
		int typearg3;
		getVarType( pvApiCtx, arg3, &typearg3 );
        if( typearg3 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 3. Multi-precision matrix expected.\n",fname);
            return 1;
        }

        op = MpsGetMatrixFromAddr( arg3 );

        m1 = MPS_NUMROW(op);
        n1 = MPS_NUMCOL(op);

        if( m1 != m || n1 != n )
        {
            sciprint("%s: Wrong size for argument 3. Operand differ in size.\n",fname);
            return 1;
        }
    }

    if( arg4 != NULL )
    {
		int typearg4;
		getVarType( pvApiCtx, arg4, &typearg4 );
        if( typearg4 != sci_mlist )
        {
            sciprint("%s: Wrong type for argument 4. Multi-precision matrix expected.\n",fname);
            return 1;
        }

        op = MpsGetMatrixFromAddr( arg4 );

        m1 = MPS_NUMROW(op);
        n1 = MPS_NUMCOL(op);

        if( m1 != m || n1 != n )
        {
            sciprint("%s: Wrong size for argument 4. Operand differ in size.\n",fname);
            return 1;
        }
    }

    return 0;
}

/* Check an rhs member for type and length. */
int MpsCheckStringSize( char* fname, int* arg1, int argnum, unsigned int minlen, unsigned int maxlen )
{
    int m1, n1;
    int pilen[1];

	int typearg1;
	getVarType( pvApiCtx, arg1, &typearg1 );
    if( typearg1 != sci_strings )
    {
        sciprint("%s: Wrong type for argument %d. String expected.\n", fname, argnum);
        return 1;
    }

    getMatrixOfString( pvApiCtx, arg1, &m1, &n1, NULL, NULL );

    if( m1 != 1 || n1 != 1 )
    {
        sciprint("%s: Wrong size for argument %d. String expected.\n", fname, argnum);
        return 1;
    }

    getMatrixOfString( pvApiCtx, arg1, &m1, &n1, pilen, NULL );

    if( (unsigned)pilen[0] > maxlen || (unsigned)pilen[0] < minlen )
    {
        if( minlen == 0 )
            sciprint("%s: Wrong length for argument %d. String longer than %ud characters.\n",
                fname, argnum, maxlen);
        else
            sciprint("%s: Wrong length for argument %d. String length is outside the range [%ud, %ud].\n",
                fname, argnum, minlen, maxlen);
        return 1;
    }

    return 0;
}

/*
 * Check if the argument given is of the right type and if it exist as a
 * properly allocated matrix.
 */
int MpsIsValid( char* fname, int *arg, int argnum )
{
    mps_ptr op;

    /* Operand must be a multi-precision matrix. */
	int typearg;
	getVarType( pvApiCtx, arg, &typearg );
    if( typearg != sci_mlist )
    {
      sciprint("%s: Wrong type for argument %d. Multi-precision matrix expected.\n",fname, argnum );
      return 1;
    }

    /* Get the matrix from the list. */
    op = MpsGetMatrixFromAddr( arg );

    if( mps_exist( op ) == 0 )
    {
      sciprint("%s: Warning argument one is an unallocated matrix.\n",fname);
      return 1;
    }

    return 0;
}

/* Validate the type and size of an input argument. */
int MpsCheckSize( char* fname, int *arg, int argnum, unsigned int m, unsigned int n )
{
    mps_ptr op;

    /* Operand must be a multi-precision matrix. */
	int typearg;
	getVarType( pvApiCtx, arg, &typearg );
    if( typearg != sci_mlist )
    {
      sciprint("%s: Wrong type for argument %d. Multi-precision matrix expected.\n",fname, argnum );
      return 1;
    }

    /* Get the matrix from the list. */
    op = MpsGetMatrixFromAddr( arg );

    if( MPS_NUMROW(op) != m || MPS_NUMCOL(op) != n )
    {
        sciprint("%s: Wrong size for argument %d. Multi-precision matrix of size %dx%d expected.\n",fname, argnum, m, n);
        return 1;
    }

    return 0;
}

/* Check that an argument is an mps matrix of size 1x1. */
int MpsCheckScalar( char* fname, int *arg, int argnum )
{
    mps_ptr op;

    /* Operand must be a multi-precision matrix. */
	int typearg;
	getVarType( pvApiCtx, arg, &typearg );
    if( typearg != sci_mlist )
    {
      sciprint("%s: Wrong type for argument %d. Multi-precision matrix expected.\n",fname, argnum );
      return 1;
    }

    /* Get the matrix from the list. */
    op = MpsGetMatrixFromAddr( arg );

    if( MPS_NUMROW(op) != 1 || MPS_NUMCOL(op) != 1 )
    {
        sciprint("%s: Wrong size for argument %d. Multi-precision matrix of size 1x1(scalar) expected. ",fname, argnum );
        return 1;
    }

    return 0;
}

int MpsGetRoundingArg( char* fname, mp_rnd_t *rnd, int* arg, int argnum )
{
    int m, n;
    int pilen[1];
    char rndchar[2];

    /* Check the rounding argument for the correct type. */
	int typearg;
	getVarType( pvApiCtx, arg, &typearg );
    if( typearg != sci_strings )
    {
        sciprint( "%s: Wrong type for argument %d. Single character string expected.\n", fname, argnum );
        return 1;
    }

    getMatrixOfString( pvApiCtx, arg, &m, &n, NULL, NULL );

    if( m != 1 || n != 1 )
    {
        sciprint( "%s: Wrong size for argument %d. Single character string expected.\n", fname, argnum );
        return 1;
    }

    getMatrixOfString( pvApiCtx, arg, &m, &n, pilen, NULL );

    if( pilen[0] != 1 )
    {
        sciprint( "%s: Wrong size for argument %d. Single character string expected.\n", fname, argnum );
        return 1;
    }

    GetRhsStringVar( argnum , &m, &n, pilen, rndchar );
    if( rndchar[0] == 'n' )
        *rnd = GMP_RNDN;
    else if( rndchar[0] == 'z' )
        *rnd = GMP_RNDZ;
    else if( rndchar[0] == 'u')
        *rnd = GMP_RNDU;
    else if( rndchar[0] == 'd' )
        *rnd = GMP_RNDD;
    else
    {
        sciprint("%s: Invalid rounding mode specified. Valid options : 'n', 'z', 'u', 'd'.\n",fname);
        return 1;
    }

    return 0;
}

