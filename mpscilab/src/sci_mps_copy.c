/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_copy( char *fname, unsigned long fname_len )
{
    int *arg1;
    int typearg1;
    mps_ptr op1;
    mps_t rop;
    int StkAdd;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(1,1);
    CheckLhs(1,1);

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

	getVarType( pvApiCtx, arg1, &typearg1 );
    if( typearg1 != sci_mlist )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision or double matrix expected.\n",fname);
      return 0;
    }

    op1 = MpsGetMatrix( 1 );

    mps_init( rop, MPS_NUMROW(op1), MPS_NUMCOL(op1), MPS_PREC(op1), 0 );
    mps_set_copy( rop, op1, GMP_RNDN );

    MpsCreateVarFrom( Rhs+1, rop, &StkAdd );
    LhsVar(1) = Rhs+1;

    return 0;
}
