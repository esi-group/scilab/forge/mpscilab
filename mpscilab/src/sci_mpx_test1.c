/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Internal check of signature functions.
 */

int sci_mpx_test1( char *fname, unsigned long fname_len )
{
	SciErr sciErr;
    double res[6] = {0.0};
    unsigned int sig;

    sig = MpsSig( sci_mlist );
    res[0] = (double)sig;

    sig = MpsSig( sci_matrix );
    res[1] = (double)sig;

    sig = MpsSig( sci_strings );
    res[2] = (double)sig;

    sig = MpsSig( sci_mlist, sci_mlist, sci_mlist, sci_mlist, sci_mlist, sci_mlist );
    res[3] = (double)sig;

    sig = MpsSig( sci_matrix, sci_poly, sci_boolean, sci_sparse, sci_ints, sci_pointer );
    res[4] = (double)sig;

    sig = MpsSig( sci_list, sci_tlist, sci_mlist );
    res[5] = (double)sig;

    sciErr =  createMatrixOfDouble( pvApiCtx, Rhs + 1, 6, 1, res );
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    LhsVar(1) = Rhs + 1;

    return 0;
}

