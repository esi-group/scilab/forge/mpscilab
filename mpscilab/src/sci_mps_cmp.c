/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"

#include "scimps.h"


/*
 * Compare two matrices element by element.
 * Scilab definition :
 * res = mps_cmp( op1, op2 );
 * res : Comparison result,
 * positive if op1 > op2,
 * zero if op1 > op2,
 * negative if op1 < op2.
 * op1, op2 : Matrices to be compared, double or multi-precision matrices.
 */
int sci_mps_cmp( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2;
    int m1, m2, n1, n2;
    int typearg1, typearg2;
    mps_ptr op1, op2;
/*    int *rop; */
    double *dptr1, *dptr2;
/*    SciErr sciErr; */

    CheckRhs(2,2);
    CheckLhs(1,1);

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );    

    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    /* Find which situation we are in and jump to the corresponding case. */
    switch (typearg1)
    {
        case sci_matrix:
            switch (typearg2)
            {
                case sci_matrix:
                    goto DOUBLE_DOUBLE;
                case sci_mlist:
                    goto DOUBLE_TLIST;
                default:
                    return 0;
    		}
        case sci_mlist:
            switch (typearg2)
            {
                case sci_matrix:
                    goto TLIST_DOUBLE;
                case sci_mlist:
                    goto TLIST_TLIST;
                default:
                    return 0;
            }
        default:
            return 0;
    }

    /* Never place something here...*/

    DOUBLE_DOUBLE: /* (double*, double*) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

/*    CreateVar( Rhs+1, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &stkadd ); */
/*        sciErr = createMatrixOfBoolean(pvApiCtx, Rhs+1, m1, n1, rop);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }*/

    goto END;

    DOUBLE_TLIST: /* (double*, mps_t) */

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 2 );

    goto END;

    TLIST_DOUBLE: /* (mps_t, double*) */

        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

    goto END;

    TLIST_TLIST: /* (mps_t, mps_t) */

        op1 = MpsGetMatrix( 1 );
        op2 = MpsGetMatrix( 2 );


    END:

    return 0;
}
