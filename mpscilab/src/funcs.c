/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
#include <stdarg.h>
#include <limits.h>

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

/*
 * The functions in this files are used to generate functions signature.
 * Each input argument is encoded as 4 bit :
 * sci_matrix              1
 * sci_poly                2
 * sci_boolean             3
 * sci_sparse              4
 * sci_boolean_sparse      5
 * sci_ints                6
 * sci_handles             7
 * sci_strings             8
 * sci_u_function          9
 * sci_c_function         10
 * sci_lib                11
 * sci_list               12
 * sci_tlist              13
 * sci_mlist              14
 * sci_pointer            15
 *
 * A signature of 0 means an error or an unhandled argument. A function without
 * any argument has the signature UINT_MAX.
 */


/* Translate the scilab type to the one used in the signature. */
static unsigned int trans( int sci_type )
{
    switch( sci_type )
    {
        case sci_matrix:
            return MPS_SCI_MATRIX;

        case sci_poly:
            return MPS_SCI_POLY;

        case sci_boolean:
            return MPS_SCI_BOOLEAN;

        case sci_sparse:
            return MPS_SCI_SPARSE;

        case sci_boolean_sparse:
            return MPS_SCI_BOOLEAN_SPARSE;

        case sci_matlab_sparse:
            return MPS_SCI_MATLAB_SPARSE;

        case sci_ints:
            return MPS_SCI_INTS;

        case sci_handles:
            return MPS_SCI_HANDLES;

        case sci_strings:
            return MPS_SCI_STRINGS;

        case sci_u_function:
            return MPS_SCI_U_FUNCTION;

        case sci_c_function:
            return MPS_SCI_C_FUNCTION;

        case sci_lib:
            return MPS_SCI_LIB;

        case sci_list:
            return MPS_SCI_LIST;

        case sci_tlist:
            return MPS_SCI_TLIST;
        
        case sci_mlist:
            return MPS_SCI_MLIST;

        case sci_pointer:
            return MPS_SCI_POINTER;

        case sci_implicit_poly:
            return MPS_SCI_IMPLICIT_POLY;

        case sci_intrinsic_function:
            return MPS_SCI_INTRINSIC_FUNCTION;

        default:
            return 0;
    }
}


/* 
 * Generate a signature from the given input types. Should be called from the
 * MPpsSig macro. 
 */
unsigned int _MpsSig( int arg1, ... )
{
    va_list vap;
    unsigned int i;
    unsigned int sig = 0;
    int t, arg;

    va_start( vap, arg1 );

    if( arg1 != INT_MAX )
    {
        t = trans(arg1);

        if( t == 0 )
            sig = 0;
        else
            sig = t;

        for( i = 1; i <= 5; i++ )
        {
            if( sig == 0 )
                break;

            arg = va_arg( vap, int);

            if( arg == INT_MAX )
                break;
            else
                t = trans(arg);

            if( t == 0 )
                sig = 0;
            else
                sig += t << (i*4);
        }
    }
    else
        sig = UINT_MAX;

    va_end( vap );

    return sig;
}


/* Return the signature of the current Scilab function call. */
unsigned int MpsGetSig()
{
    unsigned int i, t;
    unsigned int sig = 0;
    int *arg;
    int type;

    if( Rhs == 0 )
        return UINT_MAX;

    for( i = 0; i < (unsigned)Rhs; i++ )
    {
        getVarAddressFromPosition( pvApiCtx, i+1, &arg );
        getVarType( pvApiCtx, arg, &type );
        t = trans( type );

        if( t == 0 )
        {
            sig = 0;
            break;
        }
        else
            sig += t << (i*4);
    }

    return sig;
}

