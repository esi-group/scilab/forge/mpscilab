/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_col_max( char *fname, unsigned long fname_len )
{
    mps_ptr rop, op;
    mpfr_ptr maxptr;
    int *arg1, *arg2, *arg3;
    int typearg1, typearg2, typearg3 = 0;
    int m, n;
    unsigned int maxindx;
    double *dptr;

    if( Rhs > 3 || Rhs < 2 )
    {
        sciprint("%s: Wrong number of input argument(s): 2 to 3 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    if( Rhs == 3 )
    {
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );        
    }

    if( typearg1 == sci_mlist && typearg2 == sci_mlist && sci_matrix )
        goto MLIST_MLIST_DOUBLE;
    else if( typearg1 == sci_mlist && typearg2 == sci_matrix )
        goto MLIST_DOUBLE;

    MLIST_DOUBLE:
        op = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m, &n, &dptr );

        if( m != 1 || n != 1 )
        {
          sciprint("%s: Wrong size for row argument. Scalar expected.\n",fname);
          return 0;
        }

        if( dptr[0] > MPS_NUMCOL(op) )
        {
            sciprint("%s: Invalid index.\n",fname);
            return 0;
        }

        mps_col_max( op, &maxindx, dptr[0] );

        createScalarDouble( pvApiCtx, Rhs+1, maxindx );

        LhsVar(1) = Rhs + 1;

    goto END;

    MLIST_MLIST_DOUBLE:
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m, &n, &dptr );

        if( m != 1 || n != 1 )
        {
          sciprint("%s: Wrong size for row argument. Scalar expected.\n",fname);
          return 0;
        }

        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for result operand. Scalar expected.\n",fname);
            return 0;
        }

        if( dptr[0] > MPS_NUMCOL(op) )
        {
            sciprint("%s: Invalid index.\n",fname);
            return 0;
        }

        maxptr = mps_col_max( op, &maxindx, dptr[0] );
    
        createScalarDouble( pvApiCtx, Rhs+1, maxindx );

        mpfr_set( mps_get_ele(rop,1,1), maxptr, GMP_RNDN );

        LhsVar(1) = Rhs + 1;

    goto END;

    END:

    return 0;
}
