/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"


/*
 * Set an element of a multi-precision matrix from a scalar.
 * Scilab definition :
 * mps_set_ele( rop, op1, m, [o] )
 * rop : Result operand, multi-precision matrix.
 * op : Multi-precision or double scalar.
 * m : Sequential index
 * o : Optional character to specify the order
 *  'c' Column-wise (Default)
 *  'r' Row-wise
 */
int sci_mps_set_ele_seq( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4;
	int typearg;
    int m, n, m2, n2, m3, n3;
    int pilen[1];
    char* str;
    double *dptr2, *dptr3;
    mps_ptr rop, op;
    mpfr_ptr subrop, subop;
    char orderchar[2];
    int order = 0;

    CheckRhs(3,4);
    CheckLhs(1,1);

    /* Get address of input. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

    if( Rhs == 4)
    {
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg);
        if( typearg != sci_strings )
        {
            sciprint( "%s: Wrong type for argument 4. Single character string expected.\n", fname );
            return 1;
        }

        getMatrixOfString( pvApiCtx, arg4, &m, &n, NULL, NULL );

        if( m != 1 || n != 1 )
        {
            sciprint( "%s: Wrong size for argument 4. Single character string expected.\n", fname );
            return 1;
        }

        getMatrixOfString( pvApiCtx, arg4, &m, &n, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint( "%s: Wrong size for argument 4. Single character string expected.\n", fname );
            return 1;
        }

        GetRhsStringVar( 4 , &m, &n, pilen, orderchar );
        if( orderchar[0] == 'c' )
            order = 0;
        else if( orderchar[0] == 'r' )
            order = 1;
        else
        {
            sciprint("%s: Invalid order specified. Valid options : 'c', 'r'.\n",fname);
            return 1;
        }
    }

    if( MpsIsValid( fname, arg1, 1 ) != 0 )
        return 0;
	
	getVarType( pvApiCtx, arg3, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 3. Scalar double expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );

    if( m3 != 1 || n3 != 1 )
    {
      sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
      return 0;
    }

    rop = MpsGetMatrix( 1 );

    if( dptr3[0] > MPS_NUMROW(rop) * MPS_NUMCOL(rop) )
    {
      sciprint("%s: Requested index is out of bound.\n",fname);
      return 0;
    }

    if( order == 0 )
    {
        subrop = mps_get_ele_col( rop, dptr3[0] );
    }
    else
    {
        subrop = mps_get_ele_row( rop, dptr3[0] );
    }

	getVarType( pvApiCtx, arg2, &typearg );
    if( typearg == sci_matrix )
    {
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( m2 != 1 || n2 != 1 )
        {
          sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
          return 0;
        }
        mpfr_set_d( subrop, dptr2[0], GMP_RNDN );
    }
    else if( typearg == sci_mlist )
    {
        if( MpsCheckScalar( fname, arg2, 2 ) != 0 )
            return 0;

        op = MpsGetMatrix( 2 );

        subop = mps_get_ele( op, 1, 1 );
        mpfr_set( subrop, subop, GMP_RNDN );
    }
    else if( typearg == sci_strings )
    {
        getMatrixOfString( pvApiCtx, arg2, &m2, &n2, NULL, NULL );
        if( m2 != 1 || n2 != 1 )
        {
            sciprint( "%s: Wrong size for argument 2. String scalar expected.\n", fname );
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg2, &m2, &n2, pilen, NULL );
        str = malloc( pilen[0] );
        getMatrixOfString( pvApiCtx, arg2, &m2, &n2, pilen, &str );

        /* TODO: This is unsafe, upon error rop might be modified. */
        if( mpfr_set_str( subrop, str, 0, GMP_RNDN ) != 0 )
        {
            sciprint( "%s: String does not represent a valid number for argument 2.\n", fname );
        }
    }
    else
    {
        sciprint("%s: Unsupported type for argument 2.\n",fname);
        return 0;
    }

    return 0;
}

