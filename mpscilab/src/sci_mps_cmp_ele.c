/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"


/*
 * Compare two elements of a pair of multi-precision matrices.
 * Scilab definition :
 * res = mps_cmp_ele( op1, op2, m1, n1, m2, n2 );
 * res : Comparison result,
 * positive if op1[m1,n1] > op2[m2,n2],
 * zero if op1[m1,n1] > op2[m2,n2],
 * negative if op1[m1,n1] < op2[m2,n2].
 * op1, op2 : Matrices to be compared, multi-precision matrices.
 */
int sci_mps_cmp_ele( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4, *arg5, *arg6;
	int typearg;
    int m, n;
    double *m1, *n1, *m2, *n2;
    mps_ptr op1, op2;
    mpfr_ptr x1, x2;
    int stkadd;
    int cmpres;

    CheckRhs(6,6);
    CheckLhs(1,1);

    /* Get address of inputs. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
    getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
    getVarAddressFromPosition( pvApiCtx, 5, &arg5 );
    getVarAddressFromPosition( pvApiCtx, 6, &arg6 );

    if( MpsIsValid( fname, arg1, 1 ) != 0 )
        return 0;

    if( MpsIsValid( fname, arg2, 2 ) != 0 )
        return 0;
	
	getVarType( pvApiCtx, arg3, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 3. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType( pvApiCtx, arg4, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 4. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType( pvApiCtx, arg5, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 5. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType( pvApiCtx, arg6, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 6. Scalar double expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg3, &m, &n, &m1 );

    if( m != 1 || n != 1 )
    {
      sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg4, &m, &n, &n1 );

    if( m != 1 || n != 1 )
    {
      sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg5, &m, &n, &m2 );

    if( m != 1 || n != 1 )
    {
      sciprint("%s: Wrong size for argument 5. Scalar expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg6, &m, &n, &n2 );

    if( m != 1 || n != 1 )
    {
      sciprint("%s: Wrong size for argument 6. Scalar expected.\n",fname);
      return 0;
    }

    op1 = MpsGetMatrix( 1 );
    op2 = MpsGetMatrix( 2 );

    if( n1[0] > MPS_NUMROW(op1) || m1[0] > MPS_NUMCOL(op1) )
    {
        sciprint("%s: First element is out of bound.\n",fname);
        return 0;
    }

    if( n2[0] > MPS_NUMROW(op2) || m2[0] > MPS_NUMCOL(op2) )
    {
        sciprint("%s: First element is out of bound.\n",fname);
        return 0;
    }

    x1 = mps_get_ele( op1, m1[0], n1[0] );
    x2 = mps_get_ele( op2, m2[0], n2[0] );

    cmpres = mpfr_cmp( x1, x2 );

    m = 1;
    n = 1;

    CreateVar( Rhs+1, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &stkadd );

    stk(stkadd)[0] = cmpres;

    LhsVar(1) = Rhs+1;

    return 0;
}

