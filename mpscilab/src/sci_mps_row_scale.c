/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_row_scale( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3;
    int m2, n2, m3, n3;
    double *dptr2, *dptr3;
    int typearg;
    mps_ptr op1, op2;

    if( Rhs != 3 )
    {
        sciprint("%s: Wrong number of input arguments: 3 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

    if( MpsIsValid( fname, arg1, 1 ) != 0 )
        return 0;

    getVarType( pvApiCtx, arg3, &typearg );
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 3. Scalar double expected.\n",fname);
      return 0;
    }

    op1 = MpsGetMatrix( 1 );
    getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );

    if( m3 != 1 || n3 != 1 )
    {
      sciprint("%s: Wrong size for the coordinate argument. Scalar expected.\n",fname);
      return 0;
    }

    if( dptr3[0] > MPS_NUMROW(op1) || dptr3[0] < 1 )
    {
      sciprint("%s: Invalid index.\n",fname);
      return 0;
    }

    getVarType( pvApiCtx, arg2, &typearg );

    if( typearg == sci_matrix  )
    {
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );
        if( m2 != 1 || n2 != 1 )
        {
          sciprint("%s: Wrong size for scale factor. Scalar expected.\n",fname);
          return 0;
        }
        
        mps_row_scale_double( op1, dptr3[0], dptr2[0], GMP_RNDN );
    }
    else if( typearg == sci_mlist )
    {
        op2 = MpsGetMatrix( 2 );
        if( MPS_NUMROW(op2) != 1 || MPS_NUMCOL(op2) != 1 )
        {
          sciprint("%s: Wrong size for scale factor. Scalar expected.\n",fname);
          return 0;
        }
        
        mps_row_scale_mpfr( op1, dptr3[0], mps_get_ele( op2, 1, 1 ), GMP_RNDN );
    }
    else
    {
      sciprint("%s: Wrong type for argument 2. Double or multi precision scalar expected.\n",fname);
      return 0;
    }

    return 0;
}
