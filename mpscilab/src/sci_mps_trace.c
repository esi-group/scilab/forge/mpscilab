/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"


int sci_mps_trace( char *fname, unsigned long fname_len )
{
    int *arg;
    int m, n;
    mps_ptr rop, op;
    mps_t mps;
    double *dptr;
    unsigned int sig;
    int ret, StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist ) )
    {
        op = MpsGetMatrix( 1 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        ret = mps_init( mps, 1, 1, MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_trace( mps, op );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_matrix ) )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg );

        getMatrixOfDouble( pvApiCtx, arg, &m, &n, &dptr );

        if( m != n )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        ret = mps_init( mps, 1, 1, 53, 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_trace_double( mps, dptr, 0, m );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }


        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for output argument. Scalar expected.\n",fname);
            return 0;
        }

        mps_trace( rop, op );
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix ) )
    {
        rop = MpsGetMatrix( 1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg );

        getMatrixOfDouble( pvApiCtx, arg, &m, &n, &dptr );

        if( m != n )
        {
            sciprint("%s: Inconsistent matrix operation. Square matrix expected.\n",fname);
            return 0;
        }

        if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
        {
            sciprint("%s: Wrong size for output argument. Scalar expected.\n",fname);
            return 0;
        }

        mps_trace_double( rop, dptr, 0, m );
    }
    else
    {
        if( Rhs > 2 || Rhs < 1 )
        {
            sciprint("%s: Wrong number of input arguments: 1 or 2 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

