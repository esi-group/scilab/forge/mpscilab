/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* 
 * X-Macro for functions with the signature :
 * mps_func( rop, op [, rnd] )
 * rop = mps_func( op [, rnd] )
 *
 * Two "arguments" are required :
 * SCI_FUNCTION : Function name as it would be defined in a stand-alone c file.
 * MPS_FUNCTION : mp_lib function to map this function to.
 *
 * Functions using this macro :
 *
 * Trigonometric functions :
 * sci_mps_sin
 * sci_mps_cos
 * sci_mps_acosh
 * sci_mps_asin
 * sci_mps_asinh
 * sci_mps_atan
 * sci_mps_atanh
 * sci_mps_cosh
 * sci_mps_cot
 * sci_mps_coth
 * sci_mps_csc
 * sci_mps_csch
 * sci_mps_sec
 * sci_mps_sech
 * sci_mps_sinh
 * sci_mps_tan
 * sci_mps_tanh
 *
 * Other functions :
 * sci_mps_sqrt
 * sci_mps_abs
 * sci_mps_neg
 * sci_mps_erf
 * sci_mps_erfc
 * sci_mps_factorial
 * sci_mps_exp
 * sci_mps_exp2
 * sci_mps_exp10
 * sci_mps_log
 * sci_mps_log2
 * sci_mps_log10
 * sci_mps_gamma
 */

#ifndef SCI_FUNCTION
    #error "SCI_FUNCTION is not defined in invocation of x_macro_a.c"
#endif

#ifndef MPS_FUNCTION
    #error "MPS_FUNCTION is not defined in invocation of x_macro_a.c"
#endif

#define XMA_SCI_FUNC_DEF( FUNC ) int FUNC ( char *fname, unsigned long fname_len )
#define XMA_FUNC( FUNC ) FUNC

#define XMA_DOUBLE( FUNC ) FUNC##_double
#define XMA_FUNC_DOUBLE( FUNC ) XMA_DOUBLE( FUNC )
 
XMA_SCI_FUNC_DEF( SCI_FUNCTION )
{
    int *arg1, *arg2, *arg3;
    int m, n;
    int StkAdd;
    mps_ptr rop, op;
    mps_t mps;
    mp_rnd_t rnd = GMP_RNDN;
    double *dptr1;
    char* rndptr[1];
    char rndchar[2];
    int pilen[1];
    int ret;
    unsigned int sig;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_matrix, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_matrix, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, Rhs, &arg3 );

        getMatrixOfString( pvApiCtx, arg3, &m, &n, NULL, NULL );

        if( m != 1 || n != 1 )
        {
            sciprint("%s: Wrong size for rounding mode. Single character string expected.\n",fname);
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg3, &m, &n, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong length for rounding mode. Single character string expected.\n",fname);
            return 0;
        }

        rndptr[0] = rndchar;
        getMatrixOfString( pvApiCtx, arg3, &m, &n, pilen, rndptr );

        if( rndchar[0] == 'n' )
            rnd = GMP_RNDN;
        else if( rndchar[0] == 'z' )
            rnd = GMP_RNDZ;
        else if( rndchar[0] == 'u')
            rnd = GMP_RNDU;
        else if( rndchar[0] == 'd' )
            rnd = GMP_RNDD;
        else
        {
            sciprint("%s: Invalid rounding mode specified. Valid options : 'n', 'z', 'u', 'd'.\n",fname);
            return 0;
        }

    }

    if( sig == MpsSig( sci_mlist ) ||
        sig == MpsSig( sci_mlist, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

        op = MpsGetMatrix( 1 );

        MpsCollect();
        ret = mps_init( mps, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        XMA_FUNC( MPS_FUNCTION ) ( mps, op, rnd );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_matrix ) ||
             sig == MpsSig( sci_matrix, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

        getMatrixOfDouble( pvApiCtx, arg1, &m, &n, &dptr1 );

        if( m < 0 || n < 0 )
        {
            sciprint("%s: Null matrices are not supported.\n",fname);
            return 0;
        }

        MpsCollect();
        ret = mps_init( mps, m, n, 53, 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        XMA_FUNC_DOUBLE( MPS_FUNCTION ) ( mps, dptr1, 0, rnd );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( rop == NULL || op == NULL )
        {
            sciprint("%s: Invalid or un-initialized input arguments.\n",fname);
            return 0;
        }

        if( (MPS_NUMROW(rop) != MPS_NUMROW(op)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 0;
        }

        XMA_FUNC( MPS_FUNCTION ) ( rop, op, rnd );
        
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix ) ||
             sig == MpsSig( sci_mlist, sci_matrix, sci_strings ) )
    {
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        rop = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m, &n, &dptr1 );

        if( rop == NULL )
        {
            sciprint("%s: Invalid or un-initialized input arguments.\n",fname);
            return 0;
        }

        if( (MPS_NUMROW(rop) != (unsigned)m) || (MPS_NUMCOL(rop) != (unsigned)n) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 1;
        }

        XMA_FUNC_DOUBLE( MPS_FUNCTION ) ( rop, dptr1, 0, rnd );
    }
    else
    {
        if( Rhs > 3 || Rhs < 1 )
        {
            sciprint("%s: Wrong number of input arguments: 1 to 3 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}


#undef SCI_FUNCTION
#undef MPS_FUNCTION


#undef XMA_SCI_FUNC_DEF
#undef XMA_FUNC
#undef XMA_DOUBLE
#undef XMA_FUNC_DOUBLE

