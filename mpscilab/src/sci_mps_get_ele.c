/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"


/* 
 * Create a 1x1 matrix from an element of another matrix.
 * Scilab definition :
 * rop = mps_get_ele( rop, op, m, n )
 * rop : Result operand, mps matrix.
 * op : Input multi-precision matrix.
 * m : 1-indexed row of the requested element.
 * n : 1-indexed coloum of the requested element.
 */
int sci_mps_get_ele( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4;
	int typearg;
    int m3, m4, n3, n4;
    double *dptr3, *dptr4;
    mps_ptr rop, op;
    mpfr_ptr subrop, subop;

    CheckRhs(4,4);
    CheckLhs(1,1);

    /* Get address of inputs. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
    getVarAddressFromPosition( pvApiCtx, 4, &arg4 );

    if( MpsCheckScalar( fname, arg1, 1 ) != 0 )
        return 0;

    if( MpsIsValid( fname, arg2, 2 ) != 0 )
        return 0;

	getVarType( pvApiCtx, arg3, &typearg);
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 3. Scalar double expected.\n",fname);
      return 0;
    }

	getVarType( pvApiCtx, arg4, &typearg);
    if( typearg != sci_matrix  )
    {
      sciprint("%s: Wrong type for argument 4. Scalar double expected.\n",fname);
      return 0;
    }

    getMatrixOfDouble( pvApiCtx, arg3, &m3, &n3, &dptr3 );
    getMatrixOfDouble( pvApiCtx, arg4, &m4, &n4, &dptr4 );

    if( m3 != 1 || n3 != 1 )
    {
      sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
      return 0;
    }

    if( m4 != 1 || n4 != 1 )
    {
      sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
      return 0;
    }

    rop = MpsGetMatrix( 1 );
    op = MpsGetMatrix( 2 );

    if( dptr3[0] > MPS_NUMROW(op) || dptr4[0] > MPS_NUMCOL(op) )
    {
      sciprint("%s: Requested index is out of bound.\n",fname);
      return 0;
    }

    subrop = mps_get_ele( rop, 1, 1 );
    subop = mps_get_ele( op, dptr3[0], dptr4[0] );

    mpfr_set ( subrop, subop, GMP_RNDN );

    return 0;
}

