/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "scilines.h"

#include "scimps.h"

int g_scimps_format = 0; /* 'v' = 0; 'e' = 1; 'i' = 2 */
int g_scimps_width = 8;
int g_scimps_sformat = -1; /* 'v' = 0; 'e' = 1; 'n' = -1 */
int g_scimps_swidth = -1;

/* Display function for overloading the Scilab type display. */
int sci_mps_disp( char *fname, unsigned long fname_len )
{
    char *outputstr, *buf, *scistr, *charptr;
    mps_ptr op;
    mpfr_ptr subop;
    unsigned int i, j, k;
    size_t left, len;
    unsigned int *colslen;
    int colwidth;
    unsigned int col, row, nextbatch, batchlen;
    const int spacing = 1;
    double memsize;

    int precision;
    int format;
    int sprecision;
    int sformat;
    precision = g_scimps_width;
    format = g_scimps_format;
    sprecision = g_scimps_swidth;
    sformat = g_scimps_sformat;

    CheckRhs(1,1);
    CheckLhs(1,1);

/* 
 * Technically both way to get the column size are not part of the Scilab API.
 * The second method however works on windows since libshell has no .lib provided.
 */
//    colwidth = getColumnsSize();
    colwidth = C2F(iop).lct[4];

    op = MpsGetMatrix( 1 );

    if( sformat != -1 && MPS_NUMROW(op) == 1 && MPS_NUMCOL(op) == 1 )
    {
        format = sformat;
        if( sprecision != -1 )
            precision = sprecision;
    }

    /* Using the 'i' format so just print basic info about the matrix and exit. */
    if( format == 2 )
    {
        sciprint( "Size: %u x %u\n", MPS_NUMROW(op), MPS_NUMCOL(op)  );
        sciprint( "Precision: %u\n", MPS_PREC(op) );
        
        memsize = MPS_ALLOC_SIZE(op) + MPS_LIMBS_ALLOC_SIZE(op);
        if( memsize >= (double)1099511627776 )
            sciprint( "Memory: %.2f TiB\n", memsize/(double)1099511627776 );
        else if( memsize >= (double)1073741824 )
            sciprint( "Memory: %.2f GiB\n", memsize/(double)1073741824 );
        else if( memsize >= (double)1048576 )
            sciprint( "Memory: %.2f MiB\n", memsize/(double)1048576 );   
        else if( memsize >= (double)1024 )
            sciprint( "Memory: %.2f kiB\n", memsize/(double)1024 );
        else
            sciprint( "Memory: %.2f B\n", memsize );

        return 0;
    }

    buf = malloc( precision + 30 );
    if( buf == NULL )
    {
		sciprint( "%s: No more memory.\n", fname );
	    return 0;
    }

    left = MPS_NUMROW(op) * MPS_NUMCOL(op) * (precision+15);
    outputstr = malloc( MPS_NUMROW(op) * MPS_NUMCOL(op) * (precision+15) );
    scistr = malloc( MPS_NUMCOL(op) * (precision+15) );
    colslen = calloc( MPS_NUMCOL(op), sizeof(unsigned int) );
    if( outputstr == NULL || scistr == NULL || colslen == NULL  )
    {
		sciprint( "%s: No more memory.\n", fname );
		return 0;
    }

    outputstr[0] = '\0';
    scistr[0] = '\0';

    /* 
     * Formatted printing of multi-precision numbers is an extremely costly
     * operation especially for large precision. As such we minimize the amount
     * of processing done with multi-prec numbers by converting them all at
     * once, even if this will require more than twice the memory to store
     * the output string. 
     */
    for( i = 1; i <= MPS_NUMROW(op); i++ )
    {
        for( j = 1; j <= MPS_NUMCOL(op); j++ )
        {
            subop = mps_get_ele( op, i, j );
            
            if( !mpfr_regular_p(subop) )
            {
                /* Special case subop is either 0, +-inf or NaN. */
                if( mpfr_zero_p(subop) && !mpfr_signbit(subop) )
                    len = mpfr_snprintf( buf, precision+30," 0.\n" );
                else if( mpfr_zero_p(subop) )
                    len = mpfr_snprintf( buf, precision+30,"-0.\n" );
                else if( mpfr_nan_p(subop) )
                    len = mpfr_snprintf( buf, precision+30," nan\n" );
                else if( mpfr_inf_p(subop) && !mpfr_signbit(subop) )
                    len = mpfr_snprintf( buf, precision+30," inf\n" );
                else
                    len = mpfr_snprintf( buf, precision+30,"-inf\n" );
            }
            else if( format == 1 )
                len = mpfr_snprintf( buf, precision+30,"% #.*Re\n", precision-5, subop );
            else if( mpfr_get_exp(subop) >= (precision)*3.3219281 ||
                mpfr_get_exp(subop) <= (precision-2)*-3.3219281 )
                len = mpfr_snprintf( buf, precision+30,"% #.*Re\n", precision-5, subop );
            else if( mpfr_get_exp(subop) <= 0 )
            {
                len = mpfr_snprintf( buf, precision+30,"% #.*Rf", precision-1, subop );
                /* Potentially strip trailing 0s. */
                charptr = buf + len - 1;
                while( charptr[0] == '0' )
                {
                    charptr[0] = '\0';
                    charptr--;
                    len--;
                }
                strcat( buf, "\n" );
                len++;
            }
            else
            {
                len = mpfr_snprintf( buf, 100,"% #.*Rg", precision, subop );
                /* Potentially strip trailing 0s. */
                charptr = buf + len - 1;
                while( charptr[0] == '0' )
                {
                    charptr[0] = '\0';
                    charptr--;
                    len--;
                }
                strcat( buf, "\n" );
                len++;
            }

            left = left - len;
            if( len-1 > colslen[j-1] )
                colslen[j-1] = len-1;
            /* TODO possibly realloc here. */
            strcat( outputstr, buf );
        }
    }

    col = 1;
    row = 1;
    for(;;)
    {
        /* Figure out the size of the next batch of columns */
        nextbatch = 0;
        batchlen = 2;
        for(;;)
        {
            batchlen += colslen[col-1+nextbatch];
            if( batchlen >= (unsigned)colwidth )
                break;
            else
                nextbatch++;

            if( nextbatch + col > MPS_NUMCOL(op) )
                break;

            batchlen += spacing;
        }

        /* Means we are printing something with more digits per column than
        the Scilab console size. Eventually this should be taken care of 
        with a special printing routine. */
        if( nextbatch == 0 )
            nextbatch = 1;

        /* We can print it all at once. */
        if( col == 1 && nextbatch == MPS_NUMCOL(op) )
        {
            charptr = outputstr;
            for( i = 1; i <= MPS_NUMROW(op); i++ )
            {
                strcat( scistr, " " );
                    
                for( j = 1; j <= MPS_NUMCOL(op); j++ )
                {
                    len = strcspn( charptr, "\n" );
                    strncat( scistr, charptr, len );
                    charptr += len + 1;

                    /* Append the inter-column padding. */
                    if( j != MPS_NUMCOL(op) )
                        for(k = 1; k <= colslen[j-1] - len + spacing; k++ )
                            strcat( scistr, " " );
                }
                strcat( scistr, "\n" );
                sciprint( "%s", scistr );
                scistr[0] = '\0';
            }

            break;
        }

        /* Otherwise proceed to print the batch. */


        if( col != 1 )
            sciprint( "\n\n", col, col + nextbatch - 1 );

        sciprint( "Columns %u through %u\n\n", col, col + nextbatch - 1 );

        charptr = outputstr;
        for( i = 1; i <= MPS_NUMROW(op); i++ )
        {
            strcat( scistr, " " );
                
            for( j = 1; j <= MPS_NUMCOL(op); j++ )
            {
                if( j < col || j > col + nextbatch - 1 )
                    continue;

                len = strcspn( charptr, "\n" );
                strncat( scistr, charptr, len );
                charptr += len + 1;

                /* Append the inter-column padding. */
                if( j != col + nextbatch - 1 )
                    for(k = 1; k <= colslen[j-1] - len + spacing; k++ )
                        strcat( scistr, " " );
            }
            strcat( scistr, "\n" );
            sciprint( "%s", scistr );
            scistr[0] = '\0';
        }

        col = col + nextbatch;
        if( col > MPS_NUMCOL(op) )
            break;
    }

    free( buf );
    free( outputstr );
    free( colslen );
    free( scistr );

    return 0;
}

