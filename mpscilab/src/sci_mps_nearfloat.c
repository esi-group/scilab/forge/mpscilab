/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <string.h>

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_nearfloat( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2;
    int typearg1, typearg2;
    int ms, ns;
    mps_ptr op;
    int pilen[1];
    char dir[10] = "";

    if( Rhs > 2 || Rhs < 2 )
    {
        sciprint("%s: Wrong number of input argument: 2 expected.\n",fname);
        return 0;
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarType( pvApiCtx, arg1, &typearg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    if( typearg1 != sci_mlist )
    {
        sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
        return 0;
    }

    op = MpsGetMatrix( 1 );

    if( typearg2 != sci_strings )
    {
        sciprint("%s: Wrong type for argument 2. String expected.\n",fname);
        return 0;
    }

    getMatrixOfString( pvApiCtx, arg2, &ms, &ns, NULL, NULL );

    if( ms != 1 || ns != 1 )
    {
        sciprint( "%s: Wrong size for argument 2. Single string expected.\n", fname );
        return 0;
    }

    getMatrixOfString( pvApiCtx, arg2, &ms, &ns, pilen, NULL );

    if( pilen[0] > 9 )
    {
        sciprint( "%s: Invalid direction specified.\n", fname );
        return 0;
    }

    GetRhsStringVar( 2, &ms, &ns, pilen, dir );

    if( strcmp( dir, "succ" ) == 0 )
    {
        mps_nextabove( op );
    }
    else if( strcmp( dir, "pred" ) == 0 )
    {
        mps_nextbelow( op );
    }
    else
    {
        sciprint( "%s: Invalid direction specified.\n", fname );
        return 0;
    }

    return 0;
}

