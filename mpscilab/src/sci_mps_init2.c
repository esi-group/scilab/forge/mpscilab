/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"

#include "scimps.h"

/*
 * Init a multi-precision matrix from a scilab matrix.
 * Scilab definition :
 * rop = mps_init2( op, prec )
 * rop : Result operand,  multi-precision matrix.
 * op : Input matrix setting the size and value of rop.
 */
int sci_mps_init2( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2;
	int typearg1, typearg2;
    int m1, n1, m2, n2;
    double *dptr1, *dptr2;
    mps_ptr op1;
    mps_t mpsvar;
    int StkAdd;
    int rnum;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(2,2);
    CheckLhs(1,1);

    /* get address of input arguments. */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

    /* Check that the input arguments are of the correct type. */
	getVarType( pvApiCtx, arg1, &typearg1 );
    if( !(typearg1 == sci_matrix || typearg1 == sci_mlist)  )
    {
      sciprint("%s: Wrong type for argument 1. Multi-precision or double matrix expected.\n",fname);
      return 0;
    }

	getVarType( pvApiCtx, arg2, &typearg2 );
    if( typearg2 != sci_matrix )
    {
      sciprint("%s: Wrong type for argument 2. Scalar double expected.\n",fname);
      return 0;
    }

    if( typearg1 == sci_matrix )
    {
        /* Retrieve the rhs arguments. */
        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( m1 == 0 || n1 == 0 )
        {
          sciprint("%s: Wrong size for argument 1.\n",fname);
          return 0;            
        }

        /* Check that the second argument is of the correct size. */
        if( m2 != 1 || n2 != 1 )
        {
          sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
          return 0;
        }

        if( dptr2[0] < 2 )
        {
          sciprint("%s: Precision must be a minimum of 2.\n",fname);
          return 0;
        }

        /* Run garbage collection first. */
        MpsCollect();

        /* Create a new mps matrix. */
        mps_init( mpsvar, m1, n1, dptr2[0], 0 );

        /* Set the elements from the input matrix. */
        mps_double_input( mpsvar, dptr1, 0, GMP_RNDN );
    }
    else
    {
        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );
        
        if( m2 != 1 || n2 != 1 )
        {
          sciprint("%s: Wrong size for argument 2. Scalar expected.\n",fname);
          return 0;
        }        

        if( dptr2[0] < 2 )
        {
          sciprint("%s: Precision must be a minimum of 2.\n",fname);
          return 0;
        }

        /* Run garbage collection first. */
        MpsCollect();
        
        /* Create a new mps matrix. */
        mps_init( mpsvar, MPS_NUMROW(op1), MPS_NUMCOL(op1), dptr2[0], 0 );

        mps_set_copy( mpsvar, op1, GMP_RNDN );

    }

        /* Add the matrix to the Scilab stack. */
        rnum = MpsCreateVarFrom( 3, mpsvar, &StkAdd );

     /* Output the 3th stack position. */
    LhsVar(1) = 3;

    return 0;
}
