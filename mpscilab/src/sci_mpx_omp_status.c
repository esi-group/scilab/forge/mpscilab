/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Print the current value of various OpenMP ICVs.
 */

int sci_mpx_omp_status( char *fname, unsigned long fname_len )
{
    char *outputstr;
    mps_omp_struct omp;
    int count = 0;

    outputstr = malloc(2000);

    omp = mps_omp_status();

    count += sprintf( outputstr, "num_threads : %d\n", omp.num_threads );
    count += sprintf( &outputstr[count], "max_threads : %d\n", omp.max_threads );
    count += sprintf( &outputstr[count], "thread_num : %d\n", omp.thread_num );
    count += sprintf( &outputstr[count], "num_procs : %d\n", omp.num_procs );
    count += sprintf( &outputstr[count], "int_parallel : %d\n", omp.in_parallel );
    count += sprintf( &outputstr[count], "dynamic : %d\n", omp.dynamic );
    count += sprintf( &outputstr[count], "nested : %d\n", omp.nested );
    count += sprintf( &outputstr[count], "thread_limit : %d\n", omp.thread_limit );
    count += sprintf( &outputstr[count], "max_active_levels : %d\n", omp.max_active_levels );
    count += sprintf( &outputstr[count], "level : %d\n", omp.level );
    count += sprintf( &outputstr[count], "team_size : %d\n", omp.team_size );
    count += sprintf( &outputstr[count], "active_level : %d\n", omp.active_level );

    sciprint( "%s", outputstr );

    free(outputstr);

    return 0;
}
