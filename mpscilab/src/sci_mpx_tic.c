/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
#ifndef _MSC_VER

#include <time.h>
#include <sys/time.h>

#endif

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

#ifndef _MSC_VER

#ifdef HAVE_GETHRTIME
    hrtime_t hr_ts, hr_te, tic_time;
#elif HAVE_MACH_TIME
    uint64_t start, end, elapsed;
    Nanoseconds elapsedNano;
#else
    struct timeval start;
    struct timeval stop;
    /* struct timezone tz; */
    suseconds_t tic_time;
#endif

#endif

/*
 * ---Advanced MPX function---
 * High(er) precision timer function.
 */

int sci_mpx_tic( char *fname, unsigned long fname_len )
{
#ifndef _MSC_VER
    #ifdef HAVE_GETHRTIME
        hr_ts = gethrtime();
    #elif HAVE_MACH_TIME
        start = mach_absolute_time();
    #else
        gettimeofday( &start, NULL );
    #endif
#endif
    return 0;
}
