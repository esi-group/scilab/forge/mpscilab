/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"

#include "scimps.h"

/*
 * Return true for elements that are NaN.
 * Scilab definition :
 * res = mps_equal( op1 );
 * res : Comparison result,
 * True if op1(i) is NaN,
 * False otherwise
 * op1 : Multiprecision or double matrix.
 */
int sci_mps_isnan( char *fname, unsigned long fname_len )
{
    int *arg1;
    int m1, n1;
    int typearg1;
    mps_ptr op1;
    int *rop;
    double *dptr1;
    SciErr sciErr;

    CheckRhs(1,1);
    CheckLhs(1,1);

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );

    getVarType( pvApiCtx, arg1, &typearg1 );

    if( typearg1 == sci_mlist )
    {
        op1 = MpsGetMatrix( 1 );
        sciErr = allocMatrixOfBoolean(pvApiCtx, Rhs+1, MPS_NUMROW(op1), MPS_NUMCOL(op1), &rop);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        mps_nan_p( rop, MPS_COL_ORDER, op1 );

    }
    else if( typearg1 == sci_matrix )
    {
        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );

        sciErr = allocMatrixOfBoolean(pvApiCtx, Rhs+1, m1, n1, &rop);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        mps_nan_p_double( rop, MPS_COL_ORDER, dptr1, MPS_COL_ORDER, m1, n1 );

    }
    else
    {
        sciprint( "%s: Wrong type for argument 1. Multi-precision or matrix of double expected.\n", fname );
        return 0;        
    }

    LhsVar(1) = Rhs + 1;    

    return 0;
}
