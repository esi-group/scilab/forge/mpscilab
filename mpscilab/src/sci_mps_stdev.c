/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_stdev( char *fname, unsigned long fname_len )
{
    int *arg;
    int m, n;
    int pilen[1];
    char* opptr[1];
    char opchar[2] = "*";
    double *dptr;
    unsigned int sig;
    int norm = 1;
    mps_ptr rop, op;
    mps_t mps;
    int ret, StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_strings, sci_matrix ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings, sci_matrix ) )
    {
        if( sig == MpsSig( sci_mlist, sci_strings ) ||
            sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) )
            getVarAddressFromPosition( pvApiCtx, Rhs, &arg );
        else
            getVarAddressFromPosition( pvApiCtx, Rhs-1, &arg );

        getMatrixOfString( pvApiCtx, arg, &m, &n, NULL, NULL );

        if( m != 1 || n != 1 )
        {
            sciprint("%s: Wrong size for orientation. Single character string expected.\n",fname);
            return 0;
        }

        getMatrixOfString( pvApiCtx, arg, &m, &n, pilen, NULL );

        if( pilen[0] != 1 )
        {
            sciprint("%s: Wrong length for orientation. Single character string expected.\n",fname);
            return 0;
        }

        opptr[0] = opchar;
        getMatrixOfString( pvApiCtx, arg, &m, &n, pilen, opptr );

        if( !(opchar[0] == '*' || opchar[0] == 'r' || opchar[0] == 'c') )
        {
            sciprint("%s: Invalid operation specified. Valid options : 'r', 'c', '*'.\n",fname);
            return 0;
        }
    }

    
    if( sig == MpsSig( sci_mlist, sci_strings, sci_matrix ) ||
        sig == MpsSig( sci_mlist, sci_mlist, sci_strings, sci_matrix ) )
    {
        getVarAddressFromPosition( pvApiCtx, Rhs, &arg );

        getMatrixOfDouble( pvApiCtx, arg, &m, &n, &dptr );

        if( !(dptr[0] == 1 || dptr[0] == 0) )
        {
            sciprint("%s: Normalization option must be 0 or 1.\n",fname);
        }

        norm = dptr[0];
    }

    if( sig == MpsSig( sci_mlist ) ||
        sig == MpsSig( sci_mlist, sci_strings ) ||
        sig == MpsSig( sci_mlist, sci_strings, sci_matrix ) )
    {
        op = MpsGetMatrix( 1 );

        if( opchar[0] == '*' )
        {
            MpsCollect();
            ret = mps_init( mps, 1, 1, MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            mps_stdev( mps, op, norm );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
        else if( opchar[0] == 'r' )
        {
            MpsCollect();
            ret = mps_init( mps, MPS_NUMROW(op), 1, MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            mps_stdevr( mps, op, norm );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
        else if( opchar[0] == 'c' )
        {
            MpsCollect();
            ret = mps_init( mps, 1, MPS_NUMCOL(op), MPS_PREC(op), 0 );

            if( ret != 0 )
            {
                sciprint("%s: Error allocating a new MPS matrix.\n",fname);
                return 0;
            }

            mps_stdevc( mps, op, norm );

            MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
            LhsVar(1) = Rhs + 1;
        }
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_strings ) ||
             sig == MpsSig( sci_mlist, sci_mlist, sci_strings, sci_matrix ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( opchar[0] == '*' )
        {
            if( MPS_NUMROW(rop) != 1 || MPS_NUMCOL(rop) != 1 )
            {
                sciprint("%s: Wrong size for result operand. Scalar expected.\n",fname);
                return 0;
            }

            mps_stdev( rop, op, norm );
        }
        else if( opchar[0] == 'r' )
        {
            if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMROW(op) )
            {
                sciprint("%s: Wrong size for result operand.\n",fname);
                return 0;
            }

            mps_stdevr( rop, op, norm );
        }
        else if( opchar[0] == 'c' )
        {
            if( MPS_NUMROW(rop) * MPS_NUMCOL(rop) != MPS_NUMCOL(op) )
            {
                sciprint("%s: Wrong size for result operand.\n",fname);
                return 0;
            }

            mps_stdevc( rop, op, norm );
        }
    }
    else
    {
        if( Rhs > 1 || Rhs < 4 )
        {
            sciprint("%s: Wrong number of input arguments: 1 to 4 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

