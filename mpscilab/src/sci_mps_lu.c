/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

/*
 * LU decomposition.
 * Scilab definition :
 * mps_lu( ropU, ropL, op1, [algo] )
 * ropU : Result upper trianlge matrix.
 * ropL : Result lower triangle matrix.
 * op1 : Input matrix, Can be the same as one of the output matrix.
 * algo : Optional algorithm :
 * 'dool' : Doolitle's algorithm
 * 'crout' : Crout's algorithm
 */
int sci_mps_lu( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4;
    int m1, n1;
    mps_ptr ropU, ropL, op;
    int rnum;
    char algochar[6];
    int algotype = 0;
    int pilen[1];

    arg4 = NULL;

    /* Check the number of lhs and rhs arguments. */
    CheckRhs(3,4);
    CheckLhs(1,1);

    /* Get address of inputs */
    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

    /* Check the size and type of the arguments. */
    rnum = MpsCheckAllSameMps( fname, arg1, arg2, arg3, NULL );

    if( rnum != 0 )
        return 0;

    /* If there is a fourth argument, get it. */
    if( Rhs == 4 )
    {
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        rnum = MpsCheckStringSize( fname, arg4, 4, 0, 6 );
        if( rnum != 0 )
            return 0;
    }

    /* Get the algorithm type. */
    if( Rhs == 4 )
    {
        GetRhsStringVar( 4 , &m1, &n1, pilen, algochar );
        if( strncmp( algochar, "dool", 4 ) == 0 )
            algotype = 0;
        else if( strncmp( algochar, "crout", 5 ) == 0 )
            algotype = 1;
        else
        {
            sciprint("%s: Invalid algorithm specified. Valid options : 'crout', 'dool'.\n",fname);
            return 0;
        }
    }

    /* Fetch the result operands. */
    ropU = MpsGetMatrix( 1 );
    ropL = MpsGetMatrix( 2 );

    op = MpsGetMatrix( 3 );

    if( algotype == 0 )
    {
        if( mps_isalias( ropU, op ) == 0 )
            mps_copy( ropU, op );

        mps_lu_decomp( ropL, ropU );
    }
    else
    {
        if( mps_isalias( ropL, op ) == 0 )
            mps_copy( ropL, op );

        mps_lu_decomp2( ropU, ropL );
    }

    return 0;
}

