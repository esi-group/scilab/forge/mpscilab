/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

/* 
 * X-Macro for functions with the signature (mps only) :
 * mps_func( rop, op )
 * rop = mps_func( op )
 *
 * Functions using this macro :
 * sci_mps_ceil
 * sci_mps_floor
 * sci_mps_int
 * sci_mps_round
 */

#ifndef SCI_FUNCTION
    #error "SCI_FUNCTION is not defined in invocation of x_macro_d.c"
#endif

#ifndef MPS_FUNCTION
    #error "MPS_FUNCTION is not defined in invocation of x_macro_d.c"
#endif

#define XMD_SCI_FUNC_DEF( FUNC ) int FUNC ( char *fname, unsigned long fname_len )
#define XMD_FUNC( FUNC ) FUNC

XMD_SCI_FUNC_DEF( SCI_FUNCTION )
{
    mps_ptr rop, op;
    mps_t mps;
    unsigned int sig;
    int ret;
    int StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist ) )
    {
        op = MpsGetMatrix( 1 );

        MpsCollect();
        ret = mps_init( mps, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        XMD_FUNC( MPS_FUNCTION ) ( mps, op );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( (MPS_NUMROW(rop) != MPS_NUMROW(op)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 0;
        }

        XMD_FUNC( MPS_FUNCTION ) ( rop, op );
    }
    else
    {
        if( Rhs > 2 || Rhs < 1 )
        {
            sciprint("%s: Wrong number of input arguments: 1 or 2 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

#undef SCI_FUNCTION
#undef MPS_FUNCTION


#undef XMD_SCI_FUNC_DEF
#undef XMD_FUNC

