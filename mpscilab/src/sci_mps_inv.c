/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_inv( char *fname, unsigned long fname_len )
{
    int ret;
    unsigned int sig;
    mps_ptr rop, op;
    mps_t mps;
    int StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist ) )
    {
        op = MpsGetMatrix( 1 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Input matrix not square.\n",fname);
            return 0;
        }

        MpsCollect();
        ret = mps_init( mps, MPS_NUMROW(op), MPS_NUMCOL(op), MPS_PREC(op), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_copy( mps, op );
        ret = mps_inv_ipgjep( mps );

        if( ret == 1 )
            sciprint("%s: Input matrix is singular.\n",fname);

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        rop = MpsGetMatrix( 1 );
        op = MpsGetMatrix( 2 );

        if( MPS_NUMROW(op) != MPS_NUMCOL(op) )
        {
            sciprint("%s: Input matrix not square.\n",fname);
            return 0;
        }

        if( (MPS_NUMROW(rop) != MPS_NUMROW(op)) || (MPS_NUMCOL(rop) != MPS_NUMCOL(op)) )
        {
            sciprint("%s: Inconsistent matrix operation. Input and output operands differ in size.\n",fname);
            return 0;
        }

        if( mps_isalias( rop, op ) == 0 )
            mps_copy( rop, op );

        ret = mps_inv_ipgjep( rop );

        if( ret == 1 )
            sciprint("%s: Input matrix is singular.\n",fname);
    }
    else
    {
        if( Rhs > 2 || Rhs < 1 )
        {
            sciprint("%s: Wrong number of input arguments: 1 or 2 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

