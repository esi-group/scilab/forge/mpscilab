/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

/*
 * ---Advanced MPX function---
 * Returns true if mplib was built with openmp support.
 */

int sci_mpx_with_openmp( char *fname, unsigned long fname_len )
{

    CheckRhs(0,0);
    CheckLhs(1,1);

    createScalarBoolean( pvApiCtx, Rhs+1, mps_openmp_p() );

    LhsVar(1) = Rhs + 1;

    return 0;
}
