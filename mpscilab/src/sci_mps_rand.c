/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include <string.h>

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_rand( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3, *arg4;
    int typearg1, typearg2, typearg3, typearg4;
    int ms, ns;
    mps_ptr rop, a, b;
    int ma, mb, na, nb;
    double *dptra, *dptrb;
    int pilen[1];
    char dist[10] = "def";
    static gmp_randstate_t *rand_ptr = NULL;

    CheckRhs(2,4);
    CheckLhs(1,1);

    if( rand_ptr == NULL )
    {
        rand_ptr = malloc(sizeof(gmp_randstate_t));
        gmp_randinit_mt( *rand_ptr );
    }

    getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
    getVarType( pvApiCtx, arg1, &typearg1 );

    if( typearg1 != sci_mlist )
    {
        sciprint("%s: Wrong type for argument 1. Multi-precision matrix expected.\n",fname);
        return 0;
    }

    rop = MpsGetMatrix( 1 );

    getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
    getVarType( pvApiCtx, arg2, &typearg2 );

    if( typearg2 != sci_strings )
    {
        sciprint("%s: Wrong type for argument 2. String expected.\n",fname);
        return 0;
    }

    getMatrixOfString( pvApiCtx, arg2, &ms, &ns, NULL, NULL );

    if( ms != 1 || ns != 1 )
    {
        sciprint( "%s: Wrong size for argument 2. Single string expected.\n", fname );
        return 0;
    }

    getMatrixOfString( pvApiCtx, arg2, &ms, &ns, pilen, NULL );

    if( pilen[0] > 9 )
    {
        sciprint( "%s: Invalid distribution type specified.\n", fname );
        return 0;
    }

    GetRhsStringVar( 2 , &ms, &ns, pilen, dist );

    if( strcmp(dist, "def") == 0 )
    {
        mps_rand_def( rop, *rand_ptr );
    }
    else if( strcmp(dist, "unf") == 0 )
    {
        if( Rhs != 4 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );

        if( typearg3 == sci_matrix && typearg4 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );
            getMatrixOfDouble( pvApiCtx, arg4, &mb, &nb, &dptrb );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( mb != 1 || nb != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            mps_rand_unf_double( rop, dptra[0], dptrb[0], *rand_ptr );
        }
        else if( typearg3 == sci_mlist && typearg4 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );
            b = MpsGetMatrix( 4 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(b) != 1 || MPS_NUMCOL(b) != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            mps_rand_unf( rop, a, b, *rand_ptr );
        }
        else
        {
            sciprint("%s: Wrong type for arguments 3 and 4.\n",fname);
            return 0;
        }

    }
    else if( strcmp(dist, "nor") == 0 ||
             strcmp(dist, "norbm" ) == 0 ||
             strcmp(dist, "normp" ) == 0 )
    {
        if( Rhs != 4 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );

        if( typearg3 == sci_matrix && typearg4 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );
            getMatrixOfDouble( pvApiCtx, arg4, &mb, &nb, &dptrb );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( mb != 1 || nb != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( strcmp(dist, "nor") == 0 )
                mps_rand_nor1_double( rop, dptra[0], dptrb[0], *rand_ptr );
            else if( strcmp(dist, "norbm" ) == 0 )
                mps_rand_nor_double( rop, dptra[0], dptrb[0], *rand_ptr );
            else
                mps_rand_nor1_double( rop, dptra[0], dptrb[0], *rand_ptr );
        }
        else if( typearg3 == sci_mlist && typearg4 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );
            b = MpsGetMatrix( 4 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(b) != 1 || MPS_NUMCOL(b) != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( strcmp(dist, "nor") == 0 )
                mps_rand_nor1( rop, a, b, *rand_ptr );
            else if( strcmp(dist, "norbm" ) == 0 )
                mps_rand_nor( rop, a, b, *rand_ptr );
            else
                mps_rand_nor1( rop, a, b, *rand_ptr );
        }
        else
        {
            sciprint("%s: Wrong type for arguments 3 and 4.\n",fname);
            return 0;
        }
    }
    else if( strcmp(dist, "exp") == 0 )
    {
        if( Rhs != 3 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );

        if( typearg3 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            mps_rand_exp_double( rop, dptra[0], *rand_ptr );
        }
        else if( typearg3 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            mps_rand_exp( rop, a, *rand_ptr );
        }
        else
        {
            sciprint("%s: Wrong type for argument 3.\n",fname);
            return 0;
        }

    }
    else if( strcmp(dist, "gam") == 0 ||
             strcmp(dist, "gamgt") == 0 ||
             strcmp(dist, "gamgc") == 0 ||
             strcmp(dist, "gamgo") == 0 )
    {
        if( Rhs != 4 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );

        if( typearg3 == sci_matrix && typearg4 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );
            getMatrixOfDouble( pvApiCtx, arg4, &mb, &nb, &dptrb );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( mb != 1 || nb != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( dptrb[0] <= 0 )
            {
                sciprint("%s: Scale must be positive.\n",fname);
                return 0;
            }


            if( strcmp(dist, "gam") == 0 )
            {
                if( dptra[0] <= 0 )
                {
                    sciprint("%s: Shape must be positive.\n",fname);
                    return 0;
                }

                mps_rand_gam_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            else if( strcmp(dist, "gamgt") == 0 )
            {
                if( dptra[0] <= 0 )
                {
                    sciprint("%s: Shape must be positive.\n",fname);
                    return 0;
                }

                mps_rand_gam_gt_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            else if( strcmp(dist, "gamgc") == 0 )
            {
                if( dptra[0] <= 1 )
                {
                    sciprint("%s: Shape must be 1 or higher for algorithm gamgc.\n",fname);
                    return 0;
                }

                mps_rand_gam_gc_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            else if( strcmp(dist, "gamgo") == 0 )
            {
                if( dptra[0] <= 1 )
                {
                    sciprint("%s: Shape must be 3 or higher for algorithm gamgo.\n",fname);
                    return 0;
                }

                mps_rand_gam_go_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
        }
        else if( typearg3 == sci_mlist && typearg4 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );
            b = MpsGetMatrix( 4 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(b) != 1 || MPS_NUMCOL(b) != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( mpfr_sgn( mps_get_ele(b, 1, 1) ) <= 0 )
            {
                sciprint("%s: Scale must be positive.\n",fname);
                return 0;
            }

            if( strcmp(dist, "gam") == 0 )
            {
                if( mpfr_sgn( mps_get_ele(a, 1, 1) ) <= 0 )
                {
                    sciprint("%s: Shape must be positive.\n",fname);
                    return 0;
                }

                mps_rand_gam( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "gamgt") == 0 )
            {
                if( mpfr_sgn( mps_get_ele(a, 1, 1) ) <= 0 )
                {
                    sciprint("%s: Shape must be positive.\n",fname);
                    return 0;
                }

                mps_rand_gam_gt( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "gamgc") == 0 )
            {
                if( mpfr_cmp_ui( mps_get_ele(a, 1, 1), 1 ) <= 0 )
                {
                    sciprint("%s: Shape must be 1 or higher for algorithm gamgc.\n",fname);
                    return 0;
                }

                mps_rand_gam_gc( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "gamgo") == 0 )
            {
                if( mpfr_cmp_ui( mps_get_ele(a, 1, 1), 3 ) <= 0 )
                {
                    sciprint("%s: Shape must be 3 or higher for algorithm gamgo.\n",fname);
                    return 0;
                }

                mps_rand_gam_go( rop, a, b, *rand_ptr );
            }
        }
        else
        {
            sciprint("%s: Wrong type for arguments 3 and 4.\n",fname);
            return 0;
        }
    }
    else if( strcmp(dist, "beta") == 0 ||
             strcmp(dist, "betaba") == 0 ||
             strcmp(dist, "betabb") == 0 ||
             strcmp(dist, "betabn") == 0 )
    {
        if( Rhs != 4 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );

        if( typearg3 == sci_matrix && typearg4 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );
            getMatrixOfDouble( pvApiCtx, arg4, &mb, &nb, &dptrb );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( mb != 1 || nb != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( strcmp(dist, "beta") == 0 )
            {
                if( dptra[0] <= 0 || dptrb[0] <= 0 )
                {
                    sciprint("%s: Shape parameters must be positive.\n",fname);
                    return 0;
                }

                mps_rand_beta_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            if( strcmp(dist, "betaba") == 0 )
            {
                if( dptra[0] <= 0 || dptrb[0] <= 0 )
                {
                    sciprint("%s: Shape parameters must be positive.\n",fname);
                    return 0;
                }

                mps_rand_beta_ba_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            else if( strcmp(dist, "betabn") == 0 )
            {
                if( dptra[0] <= 1 || dptrb[0] <= 1 )
                {
                    sciprint("%s: Shape parameters must be higher than 1.\n",fname);
                    return 0;
                }

                mps_rand_beta_bn_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
            else if( strcmp(dist, "betabb") == 0 )
            {
                if( dptra[0] <= 1 || dptrb[0] <= 1 )
                {
                    sciprint("%s: Shape parameters must be higher than 1.\n",fname);
                    return 0;
                }

                mps_rand_beta_bb_double( rop, dptra[0], dptrb[0], *rand_ptr );
            }
        }
        else if( typearg3 == sci_mlist && typearg4 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );
            b = MpsGetMatrix( 4 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(b) != 1 || MPS_NUMCOL(b) != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( strcmp(dist, "beta") == 0 )
            {
                if( mpfr_sgn( mps_get_ele(a, 1, 1) ) <= 0 || 
                    mpfr_sgn( mps_get_ele(b, 1, 1) ) <= 0 )
                {
                    sciprint("%s: Shape parameters must be positive.\n",fname);
                    return 0;
                }

                mps_rand_beta( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "betaba") == 0 )
            {
                if( mpfr_sgn( mps_get_ele(a, 1, 1) ) <= 0 || 
                    mpfr_sgn( mps_get_ele(b, 1, 1) ) <= 0 )
                {
                    sciprint("%s: Shape parameters must be positive.\n",fname);
                    return 0;
                }

                mps_rand_beta_ba( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "betabb") == 0 )
            {
                if( mpfr_cmp_ui( mps_get_ele(a, 1, 1), 1 ) <= 0 || 
                    mpfr_cmp_ui( mps_get_ele(b, 1, 1), 1 ) <= 0 )
                {
                    sciprint("%s: Shape parameters must be higher than 1.\n",fname);
                    return 0;
                }

                mps_rand_beta_bb( rop, a, b, *rand_ptr );
            }
            else if( strcmp(dist, "betabn") == 0 )
            {
                if( mpfr_cmp_ui( mps_get_ele(a, 1, 1), 1 ) <= 0 || 
                    mpfr_cmp_ui( mps_get_ele(b, 1, 1), 1 ) <= 0 )
                {
                    sciprint("%s: Shape parameters must be higher than 1.\n",fname);
                    return 0;
                }

                mps_rand_beta_bn( rop, a, b, *rand_ptr );
            }
            
        }
        else
        {
            sciprint("%s: Wrong type for arguments 3 and 4.\n",fname);
            return 0;
        }

    }
    else if( strcmp(dist, "wei") == 0 )
    {
        if( Rhs != 4 )
        {
            sciprint( "%s: Wrong number of input arguments.\n", fname );
            return 0;
        }

        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );
        getVarType( pvApiCtx, arg3, &typearg3 );
        getVarAddressFromPosition( pvApiCtx, 4, &arg4 );
        getVarType( pvApiCtx, arg4, &typearg4 );

        if( typearg3 == sci_matrix && typearg4 == sci_matrix )
        {
            getMatrixOfDouble( pvApiCtx, arg3, &ma, &na, &dptra );
            getMatrixOfDouble( pvApiCtx, arg4, &mb, &nb, &dptrb );

            if( ma != 1 || na != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( mb != 1 || nb != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( dptra[0] <= 0 || 
                dptrb[0] <= 0 )
            {
                sciprint("%s: Shape and scale parameters must be positive.\n",fname);
                return 0;
            }

            mps_rand_wei_double( rop, dptra[0], dptrb[0], *rand_ptr );
        }
        else if( typearg3 == sci_mlist && typearg4 == sci_mlist )
        {
            a = MpsGetMatrix( 3 );
            b = MpsGetMatrix( 4 );

            if( MPS_NUMROW(a) != 1 || MPS_NUMCOL(a) != 1 )
            {
                sciprint("%s: Wrong size for argument 3. Scalar expected.\n",fname);
                return 0;
            }

            if( MPS_NUMROW(b) != 1 || MPS_NUMCOL(b) != 1 )
            {
                sciprint("%s: Wrong size for argument 4. Scalar expected.\n",fname);
                return 0;
            }

            if( mpfr_cmp_ui( mps_get_ele(a, 1, 1), 0 ) <= 0 || 
                mpfr_cmp_ui( mps_get_ele(b, 1, 1), 0 ) <= 0 )
            {
                sciprint("%s: Shape and scale parameters must be higher than 0.\n",fname);
                return 0;
            }

            mps_rand_wei( rop, a, b, *rand_ptr );
        }
        else
        {
            sciprint("%s: Wrong type for arguments 3 and 4.\n",fname);
            return 0;
        }
    }
    else
    {
        sciprint( "%s: Invalid distribution type specified.\n", fname );
        return 0;
    }

    return 0;
}

