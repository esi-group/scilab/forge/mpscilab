/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"

#include "scimps.h"

int sci_mps_mat_mul( char *fname, unsigned long fname_len )
{
    int *arg1, *arg2, *arg3;
    int m1, m2, n1, n2;
    int ret;
    unsigned int sig, prec;
    double *dptr1, *dptr2;
    mps_ptr rop, op1, op2;
    mps_t mps;
    int StkAdd;

    sig = MpsGetSig();

    if( sig == MpsSig( sci_mlist, sci_mlist ) )
    {
        /* MPS = MPS_FUNC( MPS, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        op1 = MpsGetMatrix( 1 );
        op2 = MpsGetMatrix( 2 );

        if( MPS_NUMCOL(op1) != MPS_NUMROW(op2) )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        prec = MPS_PREC(op1) > MPS_PREC(op2) ? MPS_PREC(op1) : MPS_PREC(op2);

        MpsCollect();
        ret = mps_init( mps, MPS_NUMROW(op1), MPS_NUMCOL(op2), prec, 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_mat_mul( mps, op1, op2 );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix ) )
    {
        /* MPS = MPS_FUNC( MPS, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        op1 = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m2, &n2, &dptr2 );

        if( MPS_NUMCOL(op1) != (unsigned)m2 )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        prec = MPS_PREC(op1) > 53 ? MPS_PREC(op1) : 53;

        MpsCollect();
        ret = mps_init( mps, MPS_NUMROW(op1), n2, MPS_PREC(op1), 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_mat_mul_double( mps, op1, dptr2, n2, 0 );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_matrix, sci_mlist ) )
    {
        /* MPS = MPS_FUNC( DOUBLE, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );

        getMatrixOfDouble( pvApiCtx, arg1, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 2 );

        if( (unsigned)n1 != MPS_NUMROW(op2) )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        prec = 53 > MPS_PREC(op2) ? 53 : MPS_PREC(op2);

        MpsCollect();
        ret = mps_init( mps, m1, MPS_NUMCOL(op2), prec, 0 );

        if( ret != 0 )
        {
            sciprint("%s: Error allocating a new MPS matrix.\n",fname);
            return 0;
        }

        mps_double_mat_mul( mps, dptr1, m1, 0, op2 );

        MpsCreateVarFrom( Rhs+1, mps, &StkAdd );
        LhsVar(1) = Rhs + 1;
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist, sci_mlist ) )
    {
        /* MPS_FUNC( MPS, MPS, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        op1 = MpsGetMatrix( 2 );
        op2 = MpsGetMatrix( 3 );

        if( MPS_NUMCOL(op1) != MPS_NUMROW(op2) )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
        {
            sciprint("%s: Inconsistent matrix operation, wrong size for the output operand.\n",fname);
            return 0;
        }

        if( MPS_MPFR_ARRAY(op1) == MPS_MPFR_ARRAY(rop) || MPS_MPFR_ARRAY(op2) == MPS_MPFR_ARRAY(rop) )
        {
            sciprint("%s: Result operand cannnot be one of the input operands.\n",fname);
            return 0;
        }

        mps_mat_mul( rop, op1, op2 );
    }
    else if( sig == MpsSig( sci_mlist, sci_mlist, sci_matrix ) )
    {
        /* MPS_FUNC( MPS, MPS, DOUBLE ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        op1 = MpsGetMatrix( 2 );
        getMatrixOfDouble( pvApiCtx, arg3, &m2, &n2, &dptr2 );

        if( MPS_NUMCOL(op1) != (unsigned)m2 )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        if( MPS_NUMROW(op1) != MPS_NUMROW(rop) || (unsigned)n2 != MPS_NUMCOL(rop) )
        {
            sciprint("%s: Inconsistent matrix operation, wrong size for the output operand.\n",fname);
            return 0;
        }

        if( MPS_MPFR_ARRAY(op1) == MPS_MPFR_ARRAY(rop) )
        {
            sciprint("%s: Result operand cannnot be one of the input operands.\n",fname);
            return 0;
        }

        mps_mat_mul_double( rop, op1, dptr2, n2, 0 );
    }
    else if( sig == MpsSig( sci_mlist, sci_matrix, sci_mlist ) )
    {
        /* MPS_FUNC( MPS, DOUBLE, MPS ) */
        getVarAddressFromPosition( pvApiCtx, 1, &arg1 );
        getVarAddressFromPosition( pvApiCtx, 2, &arg2 );
        getVarAddressFromPosition( pvApiCtx, 3, &arg3 );

        rop = MpsGetMatrix( 1 );
        getMatrixOfDouble( pvApiCtx, arg2, &m1, &n1, &dptr1 );
        op2 = MpsGetMatrix( 3 );

        if( (unsigned)n1 != MPS_NUMROW(op2) )
        {
            sciprint("%s: Inconsistent matrix operation.\n",fname);
            return 0;
        }

        if( (unsigned)m1 != MPS_NUMROW(rop) || MPS_NUMCOL(op2) != MPS_NUMCOL(rop) )
        {
            sciprint("%s: Inconsistent matrix operation, wrong size for the output operand.\n",fname);
            return 0;
        }

        if( MPS_MPFR_ARRAY(op2) == MPS_MPFR_ARRAY(rop) )
        {
            sciprint("%s: Result operand cannnot be one of the input operands.\n",fname);
            return 0;
        }

        mps_double_mat_mul( rop, dptr1, m1, 0, op2 );
    }
    else if( sig == MpsSig( sci_matrix, sci_matrix ) ||
             sig == MpsSig( sci_mlist, sci_matrix, sci_matrix ) )
    {
        sciprint("%s: Matrix multiplication not supported between two matrices of double.\n",fname);
        return 0;
    }
    else
    {
        if( Rhs > 3 || Rhs < 2 )
        {
            sciprint("%s: Wrong number of input arguments: 2 or 3 expected.\n",fname);
            return 0;
        }
        else
        {
            sciprint("%s: Invalid input arguments.\n",fname);
            return 0;            
        }
    }

    return 0;
}

