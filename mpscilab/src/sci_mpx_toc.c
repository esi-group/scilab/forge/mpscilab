/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2011 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#ifndef _MSC_VER

#include <time.h>
#include <sys/time.h>

#endif

#include "MALLOC.h"
#include "sciprint.h"
#include "stack-c.h"
#include "api_common.h"
#include "api_double.h"
#include "api_string.h"
#include "api_boolean.h"
#include "api_int.h"

#include "scimps.h"

#ifndef _MSC_VER
#ifdef HAVE_GETHRTIME
    extern hrtime_t hr_ts; 
    extern hrtime_t hr_te;
    extern hrtime_t tic_time;
#elif HAVE_MACH_TIME
    extern uint64_t start;
    extern uint64_t end;
    extern uint64_t elapsed;
    extern Nanoseconds elapsedNano;
#else
    extern struct timeval start;
    extern struct timeval stop;
    /* struct timezone tz; */
    extern suseconds_t tic_time;
#endif
#endif
/*
 * ---Advanced MPX function---
 * High(er) precision timer function.
 */

int sci_mpx_toc( char *fname, unsigned long fname_len )
{
#ifndef _MSC_VER
    double f_time = 0;

    #ifdef HAVE_GETHRTIME
        hr_te = gethrtime();
        tic_time = hr_te - hr_ts;
        f_time = (double)tic_time / 1000000000;
    #elif HAVE_MACH_TIME
        end = mach_absolute_time();
        elapsed = end - start;
        elapsedNano = AbsoluteToNanoseconds( *(AbsoluteTime *) &elapsed );
    #else
        /*gettimeofday( &stop, &tz );*/
        gettimeofday( &stop, NULL );

        if (start.tv_usec > stop.tv_usec)
        {
            stop.tv_usec += 1000000;
            stop.tv_sec--;
        }

        tic_time = stop.tv_usec - start.tv_usec;
        tic_time += (stop.tv_sec - start.tv_sec)*1000000;
        f_time = (double)tic_time / 1000000;
    #endif

    createScalarDouble( pvApiCtx, Rhs+1, f_time );
#else
    createScalarDouble( pvApiCtx, Rhs+1, 0 );
#endif

    LhsVar(1) = Rhs + 1;

    return 0;
}
