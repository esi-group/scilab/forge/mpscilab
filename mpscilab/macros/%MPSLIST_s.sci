// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Negation operator overloading.
function [rop] = %MPSLIST_s(op1)
    opsize = mps_size(op1);
    rop = mps_init(opsize(1), opsize(2), mps_prec(op1));
    mps_neg( rop, op1 );
endfunction
