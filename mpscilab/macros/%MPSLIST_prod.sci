// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Overloading function for prod().
function [rop] = %MPSLIST_prod(varargin)
    [lhs,rhs]=argn(0);
    op = varargin(1);
    opsize = mps_size(op);

    if( rhs == 1 )
        rop = mps_init( 1, 1, mps_prec(op) );
        mps_prod( rop, op );
    elseif( rhs >= 2 )
        opchar = varargin(2);
        if( opchar == '*' )
            rop = mps_init( 1, 1, mps_prec(op) );
            mps_prod( rop, op, '*' );
        elseif( opchar == 'r' )
            rop = mps_init( opsize(1), 1, mps_prec(op) );
            mps_prod( rop, op, 'r' );
        elseif( opchar == 'c' )
            rop = mps_init( 1, opsize(2), mps_prec(op) );
            mps_prod( rop, op, 'c' );
        end
    end
endfunction
