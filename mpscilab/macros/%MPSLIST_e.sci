// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Extraction operator overloading.
function [rop] = %MPSLIST_e(varargin)
    [lhs,rhs]=argn(0);
    op = varargin(rhs);
    sizeop = mps_size(op);

    if (rhs > 3) 
        error('Too many subscripts');
    end

    argi = varargin(1);
    sizei = size(argi, '*');

    if (rhs == 1)
        rop = mps_init2( op, mps_prec(op) );
        return;
    end

    if (rhs == 2)
        for k = 1:sizei
            if( argi(k) > sizeop(1)*sizeop(2) )
                error( 'Invalid index' );
            end
        end

        rop = mps_init( sizei, 1, mps_prec(op) );
        mpstmp = mps_init(1, 1, mps_prec(op));
        
        for k = 1:sizei
            mps_get_ele_seq( mpstmp, op, argi(k) );
            mps_set_ele( rop, mpstmp, k, 1 );
        end

        mps_clear(mpstmp);
    else
        argj = varargin(2);
        sizej = size( argj, '*' );
        for k = 1:sizei
            if( argi(k) > sizeop(1) )
                error( 'Invalid index' );
            end
        end

        for k = 1:sizej
            if( argj(k) > sizeop(2) )
                error('Invalid index');
            end
        end

        rop = mps_init( sizei, sizej, mps_prec(op) );
        mpstmp = mps_init(1, 1, mps_prec(op));

        for l = 1:sizej
            for k = 1:sizei
                mps_get_ele( mpstmp, op, argi(k), argj(l) );
                mps_set_ele( rop, mpstmp, k, l );
            end
        end

        mps_clear( mpstmp );

    end

endfunction
