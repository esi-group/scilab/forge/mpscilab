// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Overloading function for diag().
function [rop] = %MPSLIST_diag(op, varargin)
    [lhs,rhs]=argn(0);
    opsize = mps_size(op);

    if( rhs == 2 )
        k = varargin(1);
    else
        k = 0;
    end

    if( opsize(1) <= opsize(2) )
        dim = opsize(1);
    else
        dim = opsize(2);
    end

    if( opsize(1) == 1 | opsize(2) == 1 )
        m = opsize(1)*opsize(2) + abs(k);
        rop = mps_init( m, m, mps_prec(op) );
        mps_diag( rop, op, k );
    else
        if( k >= 0 )
            m = opsize(2) - k;
            if( m > dim )
                m = dim;
            end
        else
            m = opsize(1) + k;
            if( m > dim )
                m = dim;
            end
        end
        rop = mps_init( m, 1, mps_prec(op) );
        mps_diag( rop, op, k );
    end

endfunction
