// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Overloading function for min().
function [rop, k] = %MPSLIST_mini(varargin)
    [lhs,rhs]=argn(0);
    op = varargin(1);

    opsize = mps_size(op);
    prec = mps_prec(op);

    if( rhs == 1 )
        rop = mps_init( 1, 1, prec );
        k = mps_min( rop, op );
    else
        opchar = varargin(2);
        if( opchar == "c" )
            rop = mps_init( 1, opsize(2), prec );
            k = mps_min( rop, op, opchar );
        else
            rop = mps_init( 1, opsize(1), prec );
            k = mps_min( rop, op, opchar );
        end
    end

    varargout = list(rop, k);
endfunction
