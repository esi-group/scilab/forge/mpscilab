// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Greater or equal predicate overloading.
//double >= mps
function [rop] = %s_4_MPSLIST(op1, op2)
    rop = mps_greaterequal(op1, op2);
endfunction
