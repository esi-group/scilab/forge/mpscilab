// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Addition operator overloading.
//double + mps
function [rop] = %s_a_MPSLIST(op1, op2)
    opsize = mps_size(op2);
    rop = mps_init(opsize(1), opsize(2), mps_prec(op2));
    mps_add( rop, op1, op2 );
endfunction
