// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//Insertion operator overloading.
function [rop] = %s_i_MPSLIST(varargin)
    [lhs,rhs]=argn(0);
    rop = varargin(rhs);
    op = varargin(rhs-1);
    sizeop = size(op);
    sizerop = mps_size(rop);

    if ( rhs > 4 ) 
        error('Too many subscripts');
    end

    argi = varargin(1);
    sizei = size(argi);


    if ( rhs == 3 )
        if( sizei(1)*sizei(2) <> sizeop(1)*sizeop(2) )
            error("Submatrix incorrectly defined");
        end

        for k = 1:sizei(1)*sizei(2)
            if( argi(k) > sizerop(1)*sizerop(2) )
                error('Invalid index');
            end
        end

        for k = 1:sizei(1)*sizei(2)
            mps_set_ele_seq( rop, op(k), argi(k) );
        end

    else
        argj = varargin(2);
        sizej = size(argj);

        if( sizei(1)*sizei(2) <> sizeop(1) | sizej(1)*sizej(2) <> sizeop(2) )
            error("Submatrix incorrectly defined");
        end

        for l = 1:sizej(1)*sizej(2)
            for k = 1:sizei(1)*sizei(2)
                mps_set_ele( rop, op(k,l), argi(k), argj(l) ); 
            end
        end       

    end

endfunction
