// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////
//Matrix multiplication overloading.
//double * mps
function [rop] = %s_m_MPSLIST(op1, op2)
    opsize1 = size(op1);
    opsize2 = mps_size(op2);

    if ((opsize1(1) == 1) & (opsize1(2) == 1))
        rop = mps_init(opsize2(1), opsize2(2), mps_prec(op2));
        mps_mul(rop, op1, op2);
    elseif ((opsize2(1) == 1) & (opsize2(2) == 1))
        rop = mps_init(opsize1(1), opsize1(2), mps_prec(op2));
        mps_mul(rop, op1, op2);
    else
        rop = mps_init(opsize1(1), opsize2(2), mps_prec(op2));
        mps_mat_mul( rop, op1, op2 );
    end
endfunction
