// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Stress-ish test for repeated allocation and collection.

function D = func(A, B, C)
    D = ( ( A - B - C ) + 2 - (A + B - C) ) + A;
endfunction

A = mps_init2( 1, 256 );
B = mps_init2( 2, 256 );;
C = mps_init2( -3, 256 );;

for i = 1:1000
    D = func(A, B, C);
    A = A + B - C + D - A + B - C + D - A + B - C + D;
    B = A - D;
    C = C + 1;
end

rst = mps_isequal(D, 2976);
if rst <> %t  then pause, end
rst = %f;