// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_set_ele().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

mpsA = mps_init2(A, 100);

mpsB = mps_init2([25], 100);

mps_set_ele(mpsA, mpsB, 3, 2);
A(3,2) = 25;

rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_set_ele(mpsA, [45], 4, 6);
A(4,6) = 45;

rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


A = [4];

mpsA = mps_init2(A, 100);

mps_set_ele(mpsA, [5], 1, 1);

rst = mps_isequal(mpsA, [5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);