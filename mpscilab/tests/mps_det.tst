// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_det().

A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(4, 4, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 1  -2  3  4  5  6  3  2;
      5   6  7  8  9  8  7  6;
      9 10 -11 12  8  8  7  4;
     13 -14 15 16  1  2  3  4;
      9   8  7  6  5  4  3  2;
      2   1  4  8  7  6  4  1;
      1   1  4  4  4  7  8  6;
     13 -14 15 16 -1  2  3  4];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(8, 8, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);



////
A = [ 0 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 0, 0;
      0, 0 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(2, 2, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 1, 2;
      3, 4 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mpsC = mps_init(2, 2, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 1  -2  3  4  5  6  3  2;
      5   0  7  8  9  0  7  6;
      9 10 -11 12  8  8  7  4;
     13 -14 15 16  1  2  3  4;
      9   8  0  6  5  4  3  2;
      2   1  4  8  7  6  4  1;
      1   1  4  1024  4  7  0  6;
     13 -14 15 16 -1  2  3  4];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mps_det(mpsB, mpsA);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);

mpsB = mps_init(1, 1, 53);
mpsC = mps_init(8, 8, 53);

mps_det(mpsB, mpsA, mpsC);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsC);


mpsB = mps_init(1, 1, 53);

mps_det(mpsB, mpsA, mpsA);

rst = mps_isequal_margin(mpsB, B, 6, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
// Implicit rop.
////
A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = det(A);
mpsB = mps_det(mpsA);

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
// Large matrix.
////

A = rand(100,100);
     
mpsA = mps_init2(A, 53);

B = det(A);
mpsB = mps_det(mpsA);
rst = mps_isequal_margin(mpsB, B, 12, "b");
if rst <> %t  then pause, end
rst = %f;
