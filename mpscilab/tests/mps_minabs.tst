// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_minabs().

A = [ 1  -2  3  4;
      5  6  0  8;
      9 22 -11 12;
     13 -24 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [2 3]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [2 3]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 0);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1  -2  5  4 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 1);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 3;  -2;  5;  4; ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [2 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [2 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 4 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  4;
      5  6  7  8;
      9 22 -11 12;
     13 -14 15 %nan ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [1 2]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [1 2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1  -2  3  4;
      5  6  7  8;
      9 22 -11 12;
     13 -14 15 %nan ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 1);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ -%inf %nan %nan %inf 2 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA);

rst = isequal(k, [1 5]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA);

rst = isequal(k, [1 5]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

//// Row ////
A = [ 1  -2  3  4;
     50  6   7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(4,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [1; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [1; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [1; 6; 9; 13]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1  -2  3  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [1]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1; -2;  3;  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(4,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [1; 1; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [1; 1; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [1; -2;  3;  5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  4;
     50  6   %nan  %nan;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(4,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [2; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [2; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [-2; 6; 9; 13]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  4;
     50  6   %nan  %nan;
      9 %inf -11 12;
     13 -%inf 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(4,1,100);

k = mps_minabs(mpsA, 'r');

rst = isequal(k, [2; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'r');

rst = isequal(k, [2; 2; 1; 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [-2; 6; 9; 13]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

//// Column ////
A = [ 1  -2  3  4;
     50  1   7  8;
      9 22 -2 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,4,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [1 2 3 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [1 2 3 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [1 1 -2 4 ]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1  -2  3  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,4,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [ 1 1 1 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [ 1 1 1 1 ]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [ 1  -2  3  5 ]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1; -2;  0;  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [3]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [3]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [0]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  %nan;
     50  6   %nan  %nan;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,4,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [3 1 1 3]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [3 1 1 3]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [9 -2 3 12]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  4;
     50  6   %nan  %nan;
      9 %inf -11 12;
     13 -%inf 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,4,100);

k = mps_minabs(mpsA, 'c');

rst = isequal(k, [3 1 1 1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_minabs(mpsK, mpsA, 'c');

rst = isequal(k, [3 1 1 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, [9 -2 3 4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);