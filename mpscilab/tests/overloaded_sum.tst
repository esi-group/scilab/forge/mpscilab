// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded sum() function.

A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mpsB = sum(mpsA);
rst = mps_isequal(mpsB, 82);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'q');
rst = mps_isequal(mpsB, 82);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'a');
rst = mps_isequal(mpsB, 82);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*');
rst = mps_isequal(mpsB, 82);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r');
rst = mps_isequal(mpsB, [6; 26; 20; 30]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r', 'q');
rst = mps_isequal(mpsB, [6; 26; 20; 30]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c');
rst = mps_isequal(mpsB, [28 0 14 40]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c', 'q');
rst = mps_isequal(mpsB, [28 0 14 40]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);

////
A = [ 1  -2  3  4 ];
     
mpsA = mps_init2(A, 53);

mpsB = sum(mpsA);
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);

mpsB = sum(mpsA, '*');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'q');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r', 'q');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c');
rst = mps_isequal(mpsB, [1  -2  3  4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c', 'q');
rst = mps_isequal(mpsB, [1  -2  3  4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);

////
A = [ 1;  -2;  3;  4; ];
     
mpsA = mps_init2(A, 53);

mpsB = sum(mpsA);
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'q');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r');
rst = mps_isequal(mpsB, [1;  -2;  3;  4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r', 'q');
rst = mps_isequal(mpsB, [1;  -2;  3;  4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c', 'q');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 4 ];
     
mpsA = mps_init2(A, 53);

mpsB = sum(mpsA);
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'q');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r', 'q');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);

mpsB = sum(mpsA, 'c', 'q');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);

////
A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12; ];
     
mpsA = mps_init2(A, 53);

mpsB = sum(mpsA);
rst = mps_isequal(mpsB, 52);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*');
rst = mps_isequal(mpsB, 52);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, '*', 'q');
rst = mps_isequal(mpsB, 52);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r');
rst = mps_isequal(mpsB, [6; 26; 20]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'r', 'q');
rst = mps_isequal(mpsB, [6; 26; 20]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c');
rst = mps_isequal(mpsB, [15 14 -1 24]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = sum(mpsA, 'c', 'q');
rst = mps_isequal(mpsB, [15 14 -1 24]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);