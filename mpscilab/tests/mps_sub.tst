// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_sub().

/////
// Basic matrix subtraction.
/////
A = [ 2  6  6  8 10 12;
     14 16 18 20 22 24;
     26 28 30 32 34 36;
     38 40 42 44 46 48 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(4, 6, 100);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Row vector subtraction.
/////
A = [2 5 6 8 10 12];
B = [1 3 3 4 5 6];
ref = [1 2 3 4 5 6];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 6, 100);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Column vector subtraction.
/////
A = [2; 5; 6; 8; 10; 12;];
B = [1; 3; 3; 4; 5; 6;];
ref = [1; 2; 3; 4; 5; 6;];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(6, 1, 100);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Basic matrix subtraction with a scalar.
/////
A = [ 2  4  6  8 10 12;
     14 16 18 20 22 24;
     26 28 30 32 34 36;
     38 40 42 44 46 48 ];

B = [ 2 ];

ref = A - 2;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(4, 6, 100);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Subtraction with two scalars.
/////
A = [3];
B = [2];
ref = [1];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 1, 100);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


/////
// large matrix test.
/////
A = rand(100, 100);
B = rand(100, 100);

C = A - B;

mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);
mpsC = mps_init(100, 100, 53);

mps_sub(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_sub(mpsC, mpsA, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_sub(mpsC, A, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_sub(mpsC, A, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);
