// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded min() function.

A = [ 1  -2  3  4;
      5  6  7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA);

rst = isequal(k, [4 2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, -14);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 1  -2  5  4 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA);

rst = isequal(k, [1 2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 1;  -2;  5;  4; ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA);

rst = isequal(k, [2 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 5 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA);

rst = isequal(k, [1,1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, [5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


//// Column ////
A = [ 1  -2  3  4;
     50  6   7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA, "c");

rst = isequal(k, [1 4 3 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, [1 -14 -11 4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


//// Row ////
A = [ 1  -2  3  4;
     50  6   7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = min(mpsA, "r");

rst = isequal(k, [2; 2; 3; 2]);
if rst <> %t  then pause, end
rst = %f;

rst = isequal(k, [2; 2; 3; 2]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);