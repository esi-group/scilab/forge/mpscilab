// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_log2().

A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12;
      13 14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,4,100);

B = log2(A);
mps_log2(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,4,100);
mps_log2(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 2 3 4 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,4,100);

B = log2(A);
mps_log2(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,4,100);
mps_log2(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1;  2;  3;  4 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,1,100);

B = log2(A);
mps_log2(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,1,100);
mps_log2(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,1,100);

B = log2(A);
mps_log2(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,1,100);
mps_log2(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);