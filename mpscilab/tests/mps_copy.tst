// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the mps_copy() function.

ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
A = ref;

mpsA = mps_init2(ref, 100);

mpsB = mps_copy(mpsA);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


////
ref = [ 1  2  3  4  5  6 ];
A = ref;

mpsA = mps_init2(ref, 100);

mpsB = mps_copy(mpsA);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


////
ref = [ 1;  2;  3;  4;  5;  6; ];
A = ref;

mpsA = mps_init2(ref, 100);

mpsB = mps_copy(mpsA);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


////
ref = [ 6 ];
A = ref;

mpsA = mps_init2(ref, 100);

mpsB = mps_copy(mpsA);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);