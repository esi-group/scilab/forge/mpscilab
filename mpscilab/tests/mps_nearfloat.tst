// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_nearfloat().

A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);

B = nearfloat("succ", A);
mps_nearfloat(mpsA, "succ");
rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);

B = nearfloat("pred", A);
mps_nearfloat(mpsA, "pred");
rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
