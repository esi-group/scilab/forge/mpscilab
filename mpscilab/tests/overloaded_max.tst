// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded max() function.

A = [ 1  -2  3  4;
      5  6  7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = max(mpsA);

rst = isequal(k, [3 2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, 22);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 1;  -2;  5;  4; ];
     
mpsA = mps_init2(A, 100);

[rop, k] = max(mpsA);

rst = isequal(k, [3 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, 5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 4 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = max(mpsA);

rst = isequal(k, [1 1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


//// Row ////
A = [ 1  -2  3  4;
     50  6   7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = max(mpsA, "r");

rst = isequal(k, [4; 1; 2; 4]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, [4; 50; 22; 16]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


//// Column ////
A = [ 1  -2  3  4;
     50  6   7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);

[rop, k] = max(mpsA, "c");

rst = isequal(k, [2 3 4 4]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(rop, [50 22 15 16]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);