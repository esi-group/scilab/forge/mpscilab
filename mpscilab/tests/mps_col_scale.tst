// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_col_scale().

A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12;
      13 14 15 16 ];
      
B = [  2  2  3 12;
      10  6  7 24;
      18 10 11 36;
      26 14 15 48 ];

mpsA = mps_init2(A, 100);
mpsS = mps_init2(2, 100);

mps_col_scale( mpsA, mpsS, 1 );
mps_col_scale( mpsA, 3, 4 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsS);

////
A = [ 1 2 3 4 ];
B = [ 2 6 3 4 ];
     
mpsA = mps_init2(A, 100);
mpsS = mps_init2(2, 100);

mps_col_scale( mpsA, mpsS, 1 );
mps_col_scale( mpsA, 3, 2 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsS);

////
A = [ 1; 2; 3; 4 ];
B = [ 6; 12; 18; 24 ];
     
mpsA = mps_init2(A, 100);
mpsS = mps_init2(2, 100);

mps_col_scale( mpsA, mpsS, 1 );
mps_col_scale( mpsA, 3, 1 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsS);

////
A = [ 1 ];
B = [ 6 ];
     
mpsA = mps_init2(A, 100);
mpsS = mps_init2(2, 100);

mps_col_scale( mpsA, mpsS, 1 );
mps_col_scale( mpsA, 3, 1 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsS);