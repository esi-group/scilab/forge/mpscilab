// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded ones() function.

A = mps_init(2,2,100);
mpsA = ones(A);
rst = mps_isequal(mpsA, [1 1; 1 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);

A = mps_init(1,1,100);
mpsA = ones(A);
rst = mps_isequal(mpsA, [1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);

A = mps_init(1,2,100);
mpsA = ones(A);
rst = mps_isequal(mpsA, [1; 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);

A = mps_init(2,1,100);
mpsA = ones(A);
rst = mps_isequal(mpsA, [1, 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);