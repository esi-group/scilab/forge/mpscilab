// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_log2().
ln2 = 0.6931471805599453;

mpsA = mps_const_log2(53);
rst = mps_isequal(mpsA, ln2);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_const_log2(1,1,53);
rst = mps_isequal(mpsA, ln2);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_const_log2(1,2,53);
rst = mps_isequal(mpsA, [ln2; ln2]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_const_log2(2,1,53);
rst = mps_isequal(mpsA, [ln2, ln2]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(1,1,53);
mps_const_log2(mpsA);
rst = mps_isequal(mpsA, [ln2]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(1,2,53);
mps_const_log2(mpsA);
rst = mps_isequal(mpsA, [ln2; ln2]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(2,1,53);
mps_const_log2(mpsA);
rst = mps_isequal(mpsA, [ln2, ln2]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);
