// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_linspace().

s = mps_init2(2, 53);
e = mps_init2(6, 53);
mpsA = mps_init(1, 10, 53);

A = linspace(2,6,10);
mps_linspace(mpsA, s, e);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_init(1, 10, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(s, e, 10, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(2, 6, 10, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
s = mps_init2(2, 53);
e = mps_init2(6, 53);
mpsA = mps_init(1, 1, 53);

A = linspace(2,6,1);
mps_linspace(mpsA, s, e);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_init(1, 1, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(s, e, 1, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(2, 6, 1, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
s = mps_init2(2, 53);
e = mps_init2(6, 53);
mpsA = mps_init(1, 2, 53);

A = linspace(2,6,2);
mps_linspace(mpsA, s, e);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_init(1, 2, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(s, e, 2, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


mpsA = mps_linspace(2, 6, 2, 53);
mps_linspace(mpsA, 2, 6);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
