// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_mean().

A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA);
rst = mps_isequal(mpsB, 5.125);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, '*');
rst = mps_isequal(mpsB, 5.125);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init( 4, 1, 100 );

mps_mean( mpsB, mpsA, 'r');
rst = mps_isequal(mpsB, [1.5; 6.5; 5; 7.5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init( 4, 1, 100 );

mps_mean( mpsB, mpsA, 'c');
rst = mps_isequal(mpsB, [7; 0; 3.5; 10]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  -2  3  4 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA);
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, '*');
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init( 1, 1, 53 );

mps_mean( mpsB, mpsA, 'r' );
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init( 1, 4, 53 );

mps_mean( mpsB, mpsA, 'c' );
rst = mps_isequal(mpsB, [ 1  -2  3  4 ]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1;  -2;  3;  4; ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA);
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, '*');
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(4, 1, 53);

mps_mean(mpsB, mpsA, 'r');
rst = mps_isequal(mpsB, [1;  -2;  3;  4]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, 'c');
rst = mps_isequal(mpsB, 1.5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 4 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA);
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, '*');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, 'r');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, 'c');
rst = mps_isequal(mpsB, 4);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 6  -2  3  4;
      5  6  7  8;
      9 10 -11 12; ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA);
rst = mps_isequal(mpsB, 4.75);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);

mps_mean(mpsB, mpsA, '*');
rst = mps_isequal(mpsB, 4.75);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(3, 1, 53);
mps_mean(mpsB, mpsA, 'r');
rst = mps_isequal(mpsB, [2.75; 6.5; 5]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);


mpsB = mps_init(1, 4, 53);
mps_mean(mpsB, mpsA, 'c');
rst = mps_isequal(mpsB, [20/3 14/3 -1/3 24/3]);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
