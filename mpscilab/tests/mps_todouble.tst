// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_todouble().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

mpsA = mps_init2(A, 100);

B = mps_todouble(mpsA);

rst = isequal(A, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


A = [ 12 ];

mpsA = mps_init2(A, 100);

B = mps_todouble(mpsA);

rst = isequal(A, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


A = [ %nan ];

mpsA = mps_init2(A, 100);

B = mps_todouble(mpsA);

if isnan(B) <> %t then pause, end

mps_clear(mpsA);