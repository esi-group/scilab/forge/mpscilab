// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mpx_alloc_count().

res = mpx_alloc_count();
if res <> 0  then pause, end

mpsA = mps_init(10,10,10);

res = mpx_alloc_count();
if res <> 1  then pause, end

mpsB = mps_init(10,10,10);

res = mpx_alloc_count();
if res <> 2  then pause, end

mps_clear(mpsA);

res = mpx_alloc_count();
if res <> 1  then pause, end

mps_clear(mpsB);

res = mpx_alloc_count();
if res <> 0  then pause, end