// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

//Tests for mps_isequal().

// Test two equal matrices of zeroes.
mpsA = mps_init(2,3,10);
mpsB = mps_init(2,3,10);
ref = zeros(2,3);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, ref);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(ref, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

// Test two equal matrices.
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, B);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(A, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

// Test two different matrices.
A = [ 1  2  3  4  5  6;
      3  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  3  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
result = %t;

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

// Special case two equal scalars.

A = [1];
B = [1];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, B);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(A, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

// Test two different scalars.
A = [1];
B = [-1];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
result = %t;

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

// Special case two equal row vectors.

A = [1 2 3 4 5 6];
B = [1 2 3 4 5 6];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, B);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(A, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

// Test two different row vectors.
A = [1 2 3 4 5 6];
B = [1 -2 3 4 5 6];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
result = %t;

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

// Special case two equal column vectors.

A = [1; 2; 3; 4; 5; 6];
B = [1; 2; 3; 4; 5; 6];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, B);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(A, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

// Test two different column vectors.
A = [1; 2; 3; 4; 5; 6];
B = [1; -2; 3; 4; 5; 6];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
result = %t;

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

//Verify that %nan always result in %false.

A = [ %nan  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ %nan  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ %nan  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);
