// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_init().

mpsA = mps_init(2,3,10);
ref = zeros(2,3);

s = mps_size(mpsA);

if s(1) <> 2  then pause, end
if s(2) <> 3  then pause, end

p = mps_prec(mpsA);

if p <> 10 then pause, end

rst = mps_isequal(mpsA, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);



mpsA = mps_init(1,1,100);
ref = zeros(1,1);

s = mps_size(mpsA);

if s(1) <> 1  then pause, end
if s(2) <> 1  then pause, end

p = mps_prec(mpsA);

if p <> 100 then pause, end

rst = mps_isequal(mpsA, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);