// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_add().

/////
// Basic matrix addition.
/////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
ref = [ 2  6  6  8 10 12;
     14 16 18 20 22 24;
     26 28 30 32 34 36;
     38 40 42 44 46 48 ];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(4, 6, 100);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Row vector addition.
/////
A = [1 2 3 4 5 6];
B = [1 3 3 4 5 6];
ref = [2 5 6 8 10 12];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 6, 100);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 6, 100);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Column vector addition.
/////
A = [1; 2; 3; 4; 5; 6;];
B = [1; 3; 3; 4; 5; 6;];
ref = [2; 5; 6; 8; 10; 12;];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(6, 1, 100);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(6, 1, 100);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Basic matrix addition with a scalar.
/////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 2 ];
      
ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

ref = ref + 2;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(4, 6, 100);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, mpsB, mpsA);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);

mps_add(mpsC, mpsB, A);

rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, B, mpsA);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 6, 100);
mps_add(mpsC, B, A);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Additions with two scalars.
/////
A = [2];
B = [3];
ref = [5];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 1, 100);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(1, 1, 100);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


/////
// large matrix test.
/////
A = rand(100, 100);
B = rand(100, 100);

C = A + B;

mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);
mpsC = mps_init(100, 100, 53);

mps_add(mpsC, mpsA, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_add(mpsC, mpsA, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_add(mpsC, A, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_add(mpsC, A, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


/////
// Implicit rop test (x_macro_b.c).
/////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
C = A + B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mps_add(mpsA, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(mpsA, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 2 ];
      
C = A + B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mps_add(mpsA, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(mpsA, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


////
A = [ 4 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
C = A + B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mps_add(mpsA, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, mpsB);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(mpsA, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_add(A, B);
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);