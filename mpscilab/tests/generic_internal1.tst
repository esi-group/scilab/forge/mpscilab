// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Internal test 1. Checks generating various function signatures.

ref = [ 14; 1; 8; 15658734; 16139041; 3804 ];

res = mpx_test1();

rst = isequal(ref, res);
if rst <> %t  then pause, end
rst = %f;