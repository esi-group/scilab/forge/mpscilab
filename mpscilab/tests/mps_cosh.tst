// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_cosh().

A = [ -1  -2  2  1;
       5   0 -7  8;
       9 %pi 1 2;
       3  4  5 -%pi ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(4,4,53);

B = cosh(A);
mps_cosh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,4,53);
mps_cosh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1  -%pi  %pi  0 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1,4,53);

B = cosh(A);
mps_cosh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,4,53);
mps_cosh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1;  -%pi;  %pi;  0 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(4,1,53);

B = cosh(A);
mps_cosh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,1,53);
mps_cosh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1,1,53);

B = cosh(A);
mps_cosh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,1,53);
mps_cosh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 4, "b");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);