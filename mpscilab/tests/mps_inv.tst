// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_inv().

A = [ 1  -2  3  4;
      5  6  7  8;
      9 10 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(4, 4, 53);

B = inv(A);
mps_inv(mpsB, mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-15, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mpsB = mps_inv(mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-15, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
A = [ 6 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

B = inv(A);
mps_inv(mpsB, mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-15, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mpsB = mps_inv(mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-15, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


////
// Large matrix test.
////
A = rand(100,100);
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(100, 100, 53);

B = inv(A);
mps_inv(mpsB, mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-10, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsB);
mpsB = mps_inv(mpsA);
rst = mps_isequal_margin(mpsB, B, 1e-10, "a");
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);