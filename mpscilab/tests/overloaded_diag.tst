// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded diag() function.

A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12;
      13 14 15 16 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA );

rst = mps_isequal(mpsB, [1 6 11 16]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 0 );
rst = mps_isequal(mpsB, [1 6 11 16]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 1 );
rst = mps_isequal(mpsB, [2 7 12]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -1 );
rst = mps_isequal(mpsB, [5 10 15]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 2 );
rst = mps_isequal(mpsB, [3 8]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -2 );
rst = mps_isequal(mpsB, [9 14]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 3 );
rst = mps_isequal(mpsB, [4]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -3 );
rst = mps_isequal(mpsB, [13]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA );

rst = mps_isequal(mpsB, [1 6 11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 0 );
rst = mps_isequal(mpsB, [1 6 11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 1 );
rst = mps_isequal(mpsB, [2 7 12]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -1 );
rst = mps_isequal(mpsB, [5 10]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 2 );
rst = mps_isequal(mpsB, [3 8]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -2 );
rst = mps_isequal(mpsB, [9]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 3 );
rst = mps_isequal(mpsB, [4]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [  1  2  3;
       5  6  7;
       9 10 11;
      13 14 15 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA );

rst = mps_isequal(mpsB, [1 6 11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 0 );
rst = mps_isequal(mpsB, [1 6 11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 1 );
rst = mps_isequal(mpsB, [2 7]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -1 );
rst = mps_isequal(mpsB, [5 10 15]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, 2 );
rst = mps_isequal(mpsB, [3]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -2 );
rst = mps_isequal(mpsB, [9 14]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = diag( mpsA, -3 );
rst = mps_isequal(mpsB, [13]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  2  3  4 ];

B = [ 1 0 0 0;
      0 2 0 0;
      0 0 3 0;
      0 0 0 4 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  2  3 ];

B = [ 0 1 0 0;
      0 0 2 0;
      0 0 0 3;
      0 0 0 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, 1 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  2  3 ];

B = [ 0 0 0 0;
      1 0 0 0;
      0 2 0 0;
      0 0 3 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, -1 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  2 ];

B = [ 0 0 1 0;
      0 0 0 2;
      0 0 0 0;
      0 0 0 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, 2 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1  2 ];

B = [ 0 0 0 0;
      0 0 0 0;
      1 0 0 0;
      0 2 0 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, -2 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1 ];

B = [ 0 0 0 1;
      0 0 0 0;
      0 0 0 0;
      0 0 0 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, 3 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 1 ];

B = [ 0 0 0 0;
      0 0 0 0;
      0 0 0 0;
      1 0 0 0 ];
     
mpsA = mps_init2(A, 53);

mpsB = diag( mpsA, -3 );

rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);