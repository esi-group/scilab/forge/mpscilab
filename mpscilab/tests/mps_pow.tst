// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_pow().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init(4, 6, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mps_pow(mpsB, mpsA, mpsP);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mps_pow(mpsB, mpsA, 2);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 1  2  3  4  5  6 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init(1, 6, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mps_pow(mpsB, mpsA, mpsP);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mps_pow(mpsB, mpsA, 2);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 1;  2;  3;  4;  5;  6; ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init(6, 1, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mps_pow(mpsB, mpsA, mpsP);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mps_pow(mpsB, mpsA, 2);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 2 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init(1, 1, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mps_pow(mpsB, mpsA, mpsP);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mps_pow(mpsB, mpsA, 2);
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
C = A.^B;

mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

mpsC = mps_pow(mpsA, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(mpsA, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


////
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 5 ];
      
C = A.^B;

mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

mpsC = mps_pow(mpsA, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(mpsA, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


////
A = [ 6 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
C = A.^B;

mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

mpsC = mps_pow(mpsA, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(mpsA, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, mpsB);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear( mpsC)
mpsC = mps_pow(A, B);
rst = mps_isequal_margin(mpsC, C, 6, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);