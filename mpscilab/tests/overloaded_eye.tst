// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded eye() function.

A = eye(1,1);
mpsA = mps_init(1,1,100);

mpsB = eye(mpsA);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

A = eye(2,2);
mpsA = mps_init(2,2,100);

mpsB = eye(mpsA);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

A = eye(4,4);
mpsA = mps_init(4,4,100);

mpsB = eye(mpsA);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

A = eye(5,5);
mpsA = mps_init(5,5,100);

mpsB = eye(mpsA);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)