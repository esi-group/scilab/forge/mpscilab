// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded matrix multiplication operator (*).

A = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 1  2  1  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
C = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


A = [2; 3; 4; 5; 6; 7];
B = [1 2 3 4 5 6];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
C = A * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


A = [1 2 3 4 5 6];
B = [2; 3; 4; 5; 6; 7];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
C = A * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [2; 3; 4; 5; 6; 7];
B = [1 2 3 4 5 6];

C = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);


A = [2];
B = [3];

C = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 2 ];
     
C = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 2 ];
     
C = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA * B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A * mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);