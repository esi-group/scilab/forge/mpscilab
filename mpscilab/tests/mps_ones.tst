// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_ones().

mpsA = mps_ones(53);
rst = mps_isequal(mpsA, 1);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_ones(1,1,53);
rst = mps_isequal(mpsA, 1);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_ones(1,2,53);
rst = mps_isequal(mpsA, [1; 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_ones(2,1,53);
rst = mps_isequal(mpsA, [1, 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(1,1,53);
mps_ones(mpsA);
rst = mps_isequal(mpsA, [1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(1,2,53);
mps_ones(mpsA);
rst = mps_isequal(mpsA, [1; 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);


mpsA = mps_init(2,1,53);
mps_ones(mpsA);
rst = mps_isequal(mpsA, [1, 1]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsA);