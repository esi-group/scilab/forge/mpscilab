// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded insertion operator (a(ni,nj)=b).


//Scalar fun.
ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
A = ref;

mpsA = mps_init2(ref, 100);
mpsB = mps_init2([28], 100);

A(1) = 28;
mpsA(1) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(1) = 28;
mpsA(1) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(2) = 28;
mpsA(2) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(2) = 28;
mpsA(2) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(24) = 28;
mpsA(24) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(24) = 28;
mpsA(24) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(1,1) = 28;
mpsA(1,1) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(1,1) = 28;
mpsA(1,1) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(1,2) = 28;
mpsA(1,2) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(1,2) = 28;
mpsA(1,2) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(2,1) = 28;
mpsA(2,1) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(2,1) = 28;
mpsA(2,1) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref
mpsA = mps_init2(ref, 100);
A(4,6) = 28;
mpsA(4,6) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref
mpsA = mps_init2(ref, 100);
A(4,6) = 28;
mpsA(4,6) = 28;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

//Sequential insert.
ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(1:4) = B;
mpsA(1:4) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(1:4) = B;
mpsA(1:4) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(5:8) = B;
mpsA(5:8) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(5:8) = B;
mpsA(5:8) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([1 3 2 4]) = B;
mpsA([1 3 2 4]) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([1 3 2 4]) = B;
mpsA([1 3 2 4]) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([9 8 7 6]) = B;
mpsA([9 8 7 6]) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([9 8 7 6]) = B;
mpsA([9 8 7 6]) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

//Matrix insert.
ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
A = ref;
B = [1 2; 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(2:3,2:3) = B;
mpsA(2:3,2:3) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2; 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(2:3,2:3) = B;
mpsA(2:3,2:3) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2; 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([4 3],[4 3]) = B;
mpsA([4 3],[4 3]) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2; 3 4];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A([4 3],[4 3]) = B;
mpsA([4 3],[4 3]) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2 3 4 5 6];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(3,1:6) = B;
mpsA(3,1:6) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4 5 6];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(3,1:6) = B;
mpsA(3,1:6) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;


A = ref;
B = [1 2 3 4 5 6;
     6 5 4 3 2 1];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(2:3,1:6) = B;
mpsA(2:3,1:6) = mpsB;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
B = [1 2 3 4 5 6;
     6 5 4 3 2 1];

mpsA = mps_init2(ref, 100);
mpsB = mps_init2(B, 100);

A(2:3,1:6) = B;
mpsA(2:3,1:6) = B;
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;