// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the implicitly overloaded sec() function.

A = [ -1  -2  2  1;
       5   1 -7  8;
       9 1 11 12;
      13 14 15 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,4,100);

B = sec(A);
mpsB = sec(mpsA);
rst = mps_isequal_margin(mpsB, B, 1000*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1  -1  2  1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,4,100);

B = sec(A);
mpsB = sec(mpsA);
rst = mps_isequal_margin(mpsB, B, 1000*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1;  1;  1;  2 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,1,100);

B = sec(A);
mpsB = sec(mpsA);
rst = mps_isequal_margin(mpsB, B, 1000*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,1,100);

B = sec(A);
mpsB = sec(mpsA);
rst = mps_isequal_margin(mpsB, B, 1000*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);