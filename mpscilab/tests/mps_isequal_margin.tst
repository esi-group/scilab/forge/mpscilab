// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

//Tests for mps_isequal_margin().

///Check that it's working first
A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(mpsA, B);
if result <> %t  then pause, end
result = %f;

result = mps_isequal(A, mpsB);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 2  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

///Test that small variations are detected

A = [ 1+ %eps ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal(mpsA, mpsB);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(mpsA, B);
if result <> %f  then pause, end
result = %t;

result = mps_isequal(A, mpsB);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

//A margin case proper.
A = [ 1 + %eps ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal_margin(mpsA, mpsB, 10*%eps);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(mpsA, B, 10*%eps);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(A, mpsB, 10*%eps);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 1 - %eps ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal_margin(mpsA, mpsB, 10*%eps);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(mpsA, B, 10*%eps);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(A, mpsB, 10*%eps);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 1 - %eps ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
margin = mps_init2(10*%eps, 100);

result = mps_isequal_margin(mpsA, mpsB, margin);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(mpsA, B, margin);
if result <> %t  then pause, end
result = %f;

result = mps_isequal_margin(A, mpsB, margin);
if result <> %t  then pause, end
result = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [ 1 - 20*%eps ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
margin = mps_init2(10*%eps, 100);

result = mps_isequal_margin(mpsA, mpsB, margin);
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, margin);
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, margin);
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);

////
//Test the bits margin "b" options.
////

A = [ 1 ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 0, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 0, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 0, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 0, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 0, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 0, "b");
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-52 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %f  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);



A = [ 1 ];
      
B = [ 1 - 2**-51 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsA, B, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 54);

mps_add(mpsB, mpsB, %eps/2);

result = mps_isequal_margin(mpsA, mpsB, 2, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 2, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 2, "b");
if result <> %f  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 54);

mps_add(mpsB, mpsB, %eps/2);

result = mps_isequal_margin(mpsA, mpsB, 3, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 3, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 3, "b");
if result <> %t  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ 1 ];
      
B = [ 1 + 2**-51 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 54);

mps_add(mpsB, mpsB, 2**-53);

result = mps_isequal_margin(mpsA, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 2, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 2, "b");
if result <> %t  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ 0.6 ];
      
B = [ 0.6 + 2**-53 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ 0.6 ];
      
B = [ 0.6 + 2**-52 ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 1, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %f  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ %inf ];
      
B = [ %inf ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 1, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 1, "b");
if result <> %t  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ %inf ];
      
B = [ %inf ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 0, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 0, "b");
if result <> %t  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 0, "b");
if result <> %t  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);


A = [ -%inf ];
      
B = [ %inf ];
      
mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);

result = mps_isequal_margin(mpsA, mpsB, 0, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(mpsB, A, 0, "b");
if result <> %f  then pause, end
result = %t;

result = mps_isequal_margin(A, mpsB, 0, "b");
if result <> %f  then pause, end
result = %t;


mps_clear(mpsA);
mps_clear(mpsB);