// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_get_ele_seq().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

mpsA = mps_init2(A, 100);
mpsB = mps_init(1,1,100);

mps_get_ele_seq(mpsB, mpsA, 1);
rst = mps_isequal(mpsB, 1);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 1, 'c');
rst = mps_isequal(mpsB, 1);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 1, 'r');
rst = mps_isequal(mpsB, 1);
if rst <> %t  then pause, end
rst = %f;


mps_get_ele_seq(mpsB, mpsA, 2);
rst = mps_isequal(mpsB, 7);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 2, 'c');
rst = mps_isequal(mpsB, 7);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 2, 'r');
rst = mps_isequal(mpsB, 2);
if rst <> %t  then pause, end
rst = %f;


mps_get_ele_seq(mpsB, mpsA, 24);
rst = mps_isequal(mpsB, 24);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 24, 'c');
rst = mps_isequal(mpsB, 24);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 24, 'r');
rst = mps_isequal(mpsB, 24);
if rst <> %t  then pause, end
rst = %f;


mps_get_ele_seq(mpsB, mpsA, 6);
rst = mps_isequal(mpsB, 8);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 6, 'c');
rst = mps_isequal(mpsB, 8);
if rst <> %t  then pause, end
rst = %f;

mps_get_ele_seq(mpsB, mpsA, 6, 'r');
rst = mps_isequal(mpsB, 6);
if rst <> %t  then pause, end
rst = %f;