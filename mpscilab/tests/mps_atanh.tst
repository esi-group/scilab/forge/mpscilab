// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_acosh().

A = [ 0.1  0.1  0.2  0.1;
       0.5 0.1 0.1  0.8;
       0.9 0.1 0.1 0.2;
       0.3  0.4  0.5 0.1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,4,100);

B = atanh(A);
mps_atanh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10000*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,4,100);
mps_atanh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10000*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 0.1  0.3  0.2  0.1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,4,100);

B = atanh(A);
mps_atanh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,4,100);
mps_atanh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 0.1;  0.2;  0.3;  0.1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,1,100);

B = atanh(A);
mps_atanh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(4,1,100);
mps_atanh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 0.1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,1,100);

B = atanh(A);
mps_atanh(mpsB,mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsB);
mpsB = mps_init(1,1,100);
mps_atanh(mpsB,A);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);