// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_set_ele_seq().

ref = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
A = ref;

mpsA = mps_init2(ref, 100);
mpsB = mps_init2([28], 100);

A(1) = 28;
mps_set_ele_seq(mpsA, mpsB, 1);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(1) = 28;
mps_set_ele_seq(mpsA, mpsB, 1, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(1) = 28;
mps_set_ele_seq(mpsA, mpsB, 1, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;



A = ref;
mpsA = mps_init2(ref, 100);
A(1) = 28;
mps_set_ele_seq(mpsA, 28, 1);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(1) = 28;
mps_set_ele_seq(mpsA, 28, 1, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(1) = 28;
mps_set_ele_seq(mpsA, 28, 1, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;



A(2) = 28;
mps_set_ele_seq(mpsA, mpsB, 2);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(2) = 28;
mps_set_ele_seq(mpsA, mpsB, 2, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(5) = 28;
mps_set_ele_seq(mpsA, mpsB, 2, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;



A(2) = 28;
mps_set_ele_seq(mpsA, 28, 2);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(2) = 28;
mps_set_ele_seq(mpsA, 28, 2, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(5) = 28;
mps_set_ele_seq(mpsA, 28, 2, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;



A(24) = 28;
mps_set_ele_seq(mpsA, mpsB, 24);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(24) = 28;
mps_set_ele_seq(mpsA, mpsB, 24, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(24) = 28;
mps_set_ele_seq(mpsA, mpsB, 24, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;



A(24) = 28;
mps_set_ele_seq(mpsA, 28, 24);
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(24) = 28;
mps_set_ele_seq(mpsA, 28, 24, 'c');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

A = ref;
mpsA = mps_init2(ref, 100);
A(24) = 28;
mps_set_ele_seq(mpsA, 28, 24, 'r');
rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;