// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_row_min().

A = [ 1  -2  3  5;
      5  6  7  8;
      9 22 -11 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsA, 3);

rst = isequal(k, [3]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 3);

rst = isequal(k, [3]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -11);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1  -2  6  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 1;  -2;  6;  5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 2);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 2);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ 5 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 1);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 1);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 5);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -2  3  %nan;
      5  6  %nan  %nan;
      9 22 %nan 12;
     13 -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -2);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsA, 3);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 3);

rst = isequal(k, [1]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 9);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);

////
A = [ %nan  -%inf  3  %nan;
      5  6  %nan  %nan;
      %inf 22 %nan 12;
     %inf -14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsK = mps_init(1,1,100);

k = mps_row_min(mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 1);

rst = isequal(k, [2]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, -%inf);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsA, 3);

rst = isequal(k, [4]);
if rst <> %t  then pause, end
rst = %f;

k = mps_row_min(mpsK, mpsA, 3);

rst = isequal(k, [4]);
if rst <> %t  then pause, end
rst = %f;

rst = mps_isequal(mpsK, 12);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsK);