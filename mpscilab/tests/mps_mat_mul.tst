// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_mat_mul().

/////
// Basic matrix multiplication.
/////

A = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 1  2  1  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];

ref = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(4, 4, 100);

mps_mat_mul( mpsC, mpsA, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 4, 100);
mps_mat_mul(mpsC, mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsC);
mpsC = mps_init(4, 4, 100);
mps_mat_mul(mpsC, A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Matrix multiplication between a column and row vector.
/////

A = [2; 3; 4; 5; 6; 7];
B = [1 2 3 4 5 6];

ref = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(6, 6, 100);

mps_mat_mul( mpsC, mpsA, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, A, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, mpsA, B );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Matrix multiplication between a column and row vector(Reversed).
/////

A = [2; 3; 4; 5; 6; 7];
B = [1 2 3 4 5 6];

ref = B * A;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 1, 100);

mps_mat_mul( mpsC, mpsB, mpsA );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, B, mpsA );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, mpsB, A );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Matrix multiplication between a column and row vector of different length.
/////

A = [2; 3; 4; 5; 6; 7];
B = [1 2 3 4 5];

ref = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(6, 5, 100);

mps_mat_mul( mpsC, mpsA, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, A, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, mpsA, B );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);

/////
// Matrix multiplication between two scalars.
/////

A = [2];
B = [3];

ref = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);
mpsC = mps_init(1, 1, 100);

mps_mat_mul( mpsC, mpsA, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, A, mpsB );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_mat_mul( mpsC, mpsA, B );
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);


/////
// large matrix test.
/////
A = rand(100, 100);
B = rand(100, 100);


mpsA = mps_init2(A, 53);
mpsB = mps_init2(B, 53);
mpsC = mps_init(100, 100, 53);

C = A * B;

mps_mat_mul(mpsC, mpsA, mpsB);
rst = mps_isequal_margin(mpsC, C, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_mat_mul(mpsC, A, mpsB);
rst = mps_isequal_margin(mpsC, C, 4, "b");
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_init(100, 100, 53);
mps_mat_mul(mpsC, mpsA, B);
rst = mps_isequal_margin(mpsC, C, 4, "b");
if rst <> %t  then pause, end
rst = %f;


////
// Testing implicit rop.
////
A = [ 1  2  1  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16;
      1   2   3   4 ];
      
B = [ 1  2  3  4 5;
      5  6  7  8 9;
      9 10 11 12 13 ;
     13 14 15 16 17 ];
     
ref = A * B;

mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);


mpsC = mps_mat_mul(mpsA, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_mat_mul(A, mpsB);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsC);
mpsC = mps_mat_mul(mpsA, B);
rst = mps_isequal(mpsC, ref);
if rst <> %t  then pause, end
rst = %f;


mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsC);
