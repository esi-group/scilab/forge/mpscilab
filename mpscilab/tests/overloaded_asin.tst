// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded asin() function.

A = [ -1  -0.5  0.5  1;
       1   0 1  1;
       1 1 1 1;
      0.6 0.7 0.8 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,4,100);

B = asin(A);
mpsB = asin(mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1  -0.5  0.5  1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,4,100);

B = asin(A);
mpsB = asin(mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ -1;  -0.5;  0.5;  1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(4,1,100);

B = asin(A);
mpsB = asin(mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init(1,1,100);

B = asin(A);
mpsB = asin(mpsA);
rst = mps_isequal_margin(mpsB, B, 10*%eps);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
