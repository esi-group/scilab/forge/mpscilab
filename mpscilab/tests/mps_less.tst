// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_less().

A = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 1  2  1  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1  2  3  4 ];
     
B = [ 1  2  1  4 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1;  2;  3;  4; ];
     
B = [ 1;  2;  1;  4; ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
B = [ 1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1 ];
     
B = [ -1 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////%nan test cases
A = [ %nan  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ 1  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ %nan  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ %nan  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

////
A = [ 1  2  3  3;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
B = [ %nan  2  3  4;
      5  6  7  8;
      9 10 11 12;
     13 14 15 16 ];
     
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

ref = A < B;

res = mps_less(mpsA, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, mpsB);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(mpsA, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

res = mps_less(A, B);
rst = isequal(res, ref);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
