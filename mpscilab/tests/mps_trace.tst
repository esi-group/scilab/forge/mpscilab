// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_trace().

A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12;
      13 14 15 16 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_trace( mpsB, mpsA );
rst = mps_isequal(mpsB, [34]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mps_trace( mpsB, A );
rst = mps_isequal(mpsB, [34]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [  1  2  3;
       5  6  7;
       9 10 11 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_trace( mpsB, mpsA );
rst = mps_isequal(mpsB, [18]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mps_trace( mpsB, A );
rst = mps_isequal(mpsB, [18]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_trace( mpsA );
rst = mps_isequal(mpsB, [18]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_trace( A );
rst = mps_isequal(mpsB, [18]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);


////
A = [ 11 ];
     
mpsA = mps_init2(A, 53);
mpsB = mps_init(1, 1, 53);

mps_trace( mpsB, mpsA );
rst = mps_isequal(mpsB, [11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_init(1, 1, 53);
mps_trace( mpsB, A );
rst = mps_isequal(mpsB, [11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_trace( mpsA );
rst = mps_isequal(mpsB, [11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);


mpsB = mps_trace( A );
rst = mps_isequal(mpsB, [11]);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB);
mps_clear(mpsA);
