// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_init2().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

mpsA = mps_init2(A, 100);

s = mps_size(mpsA);

if s(1) <> 4  then pause, end
if s(2) <> 6  then pause, end

p = mps_prec(mpsA);

if p <> 100 then pause, end

rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

A = [9];

mpsA = mps_init2(A, 20);

s = mps_size(mpsA);

if s(1) <> 1  then pause, end
if s(2) <> 1  then pause, end

p = mps_prec(mpsA);

if p <> 20 then pause, end

rst = mps_isequal(mpsA, A);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

// Test initializing from another mps matrix.

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];

mpsA = mps_init2(A, 100);
mpsB = mps_init2(mpsA, 100);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);

A = [9];

mpsA = mps_init2(A, 20);
mpsB = mps_init2(mpsA, 100);

rst = mps_isequal(mpsA, mpsB);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);