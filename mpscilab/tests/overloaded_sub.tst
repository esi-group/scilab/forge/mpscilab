// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded subtraction operator (-).

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
B = [ 1  4  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsB = mps_init2(B, 100);

mpsC = mpsA - mpsB;
C = A - B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = A - mpsB;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA - B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA - mpsB - (A - mpsB) - mpsB;
C = A - B - (A - B) - B;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = mpsA - 2;
C = A - 2;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;

mpsC = 2 - mpsA;
C = 2 - A;
rst = mps_isequal(mpsC, C);
if rst <> %t  then pause, end
rst = %f;