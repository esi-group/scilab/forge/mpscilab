// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for the overloaded element-wise power operator (.^).

// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_pow().

A = [ 1  2  3  4  5  6;
      7  8  9  10 11 12;
      13 14 15 16 17 18;
      19 20 21 22 23 24 ];
      
mpsA = mps_init2(A, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mpsB = mpsA .^ mpsP;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mpsB = mpsA .^ 2;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 1  2  3  4  5  6 ];
      
mpsA = mps_init2(A, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mpsB = mpsA .^ mpsP;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mpsB = mpsA .^ 2;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 1;  2;  3;  4;  5;  6; ];
      
mpsA = mps_init2(A, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mpsB = mpsA .^ mpsP;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mpsB = mpsA .^ 2;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);


A = [ 2 ];
      
mpsA = mps_init2(A, 100);
mpsP = mps_init2([2], 100);

B = A .^ 2;
mpsB = mpsA .^ mpsP;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

B = A .^ 2;
mpsB = mpsA .^ 2;
rst = mps_isequal(mpsB, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);
mps_clear(mpsB);
mps_clear(mpsP);