// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Internal test 1. Checks reading various function signatures.


res = mpx_test2( 1 );
rst = isequal(res, 1);
if rst <> %t  then pause, end
rst = %f;


res = mpx_test2( 1, 2 );
rst = isequal(res, 17);
if rst <> %t  then pause, end
rst = %f;


res = mpx_test2( 1, 2, 3, 4, 5, 6 );
rst = isequal(res, 1118481);
if rst <> %t  then pause, end
rst = %f;


res = mpx_test2( "hi" );
rst = isequal(res, 8);
if rst <> %t  then pause, end
rst = %f;


mpsA = mps_init(1,1,100);

res = mpx_test2( mpsA );
rst = isequal(res, 14);
if rst <> %t  then pause, end
rst = %f;


res = mpx_test2( mpsA, mpsA );
rst = isequal(res, 238);
if rst <> %t  then pause, end
rst = %f;


res = mpx_test2( mpsA, 1, "test" );
rst = isequal(res, 2078);
if rst <> %t  then pause, end
rst = %f;
