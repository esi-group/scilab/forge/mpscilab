// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_eye().

A = eye(1,1);

mpsB = mps_eye(1,1,100);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

mpsB = mps_eye(100);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)


A = eye(2,2);

mpsB = mps_eye(2,2,100);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

mpsB = mps_init(2,2,100);
mps_eye(mpsB);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)


A = eye(4,4);

mpsB = mps_eye(4,4,100);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

mpsB = mps_init(4,4,100);
mps_eye(mpsB);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)


A = eye(5,5);

mpsB = mps_eye(5,5,100);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)

mpsB = mps_init(5,5,100);
mps_eye(mpsB);
rst = mps_isequal(A, mpsB);
if rst <> %t  then pause, end
rst = %f;
mps_clear(mpsB)