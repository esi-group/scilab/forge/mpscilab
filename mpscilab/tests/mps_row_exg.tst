// Multi precision toolbox for Scilab
// Copyright (C) 2011 - Jonathan Blanchard

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Tests for mps_row_exg().

A = [  1  2  3  4;
       5  6  7  8;
       9 10 11 12;
      13 14 15 16 ];
      
B=  [ 13 14 15 16;
       5  6  7  8;
       1  2  3  4;
       9 10 11 12 ];

mpsA = mps_init2(A, 100);

mps_row_exg(mpsA, 1, 4 );
mps_row_exg(mpsA, 4, 3 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);


////
A = [ 1 2 3 4 ];
B = [ 1 2 3 4 ];
     
mpsA = mps_init2(A, 100);

mps_row_exg(mpsA, 1, 1 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

////
A = [ 1; 2; 3; 4 ];
B = [ 1; 3; 2; 4 ];
     
mpsA = mps_init2(A, 100);

mps_row_exg(mpsA, 2, 3 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);

////
A = [ 1 ];
B = [ 1 ];
     
mpsA = mps_init2(A, 100);

mps_row_exg(mpsA, 1, 1 );

rst = mps_isequal(mpsA, B);
if rst <> %t  then pause, end
rst = %f;

mps_clear(mpsA);