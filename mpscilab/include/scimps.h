/*
 * Multi precision toolbox for Scilab
 * Copyright (C) 2009 - Jonathan Blanchard
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
#include <limits.h>

#include "mpscilab_config.h"
#include "mps.h"

#define SCI_MATRIX              1
#define SCI_POLY                2
#define SCI_BOOLEAN             4
#define SCI_SPARSE              5
#define SCI_BOOLEAN_SPARSE      6
#define SCI_MATLAB_SPARSE       7
#define SCI_INTS                8
#define SCI_HANDLES             9
#define SCI_STRINGS             10
#define SCI_U_FUNCTION          11
#define SCI_C_FUNCTION          13
#define SCI_LIB                 14
#define SCI_LIST                15
#define SCI_TLIST               16
#define SCI_MLIST               17
#define SCI_POINTER             128
#define SCI_IMPLICIT_POLY       129
#define SCI_INTRINSIC_FUNCTION  130


#define MPS_SCI_MATRIX              1
#define MPS_SCI_POLY                2
#define MPS_SCI_BOOLEAN             3
#define MPS_SCI_SPARSE              4
#define MPS_SCI_BOOLEAN_SPARSE      5
#define MPS_SCI_MATLAB_SPARSE       0
#define MPS_SCI_INTS                6
#define MPS_SCI_HANDLES             7
#define MPS_SCI_STRINGS             8
#define MPS_SCI_U_FUNCTION          9
#define MPS_SCI_C_FUNCTION          10
#define MPS_SCI_LIB                 11
#define MPS_SCI_LIST                12
#define MPS_SCI_TLIST               13
#define MPS_SCI_MLIST               14
#define MPS_SCI_POINTER             15
#define MPS_SCI_IMPLICIT_POLY       0
#define MPS_SCI_INTRINSIC_FUNCTION  0

/* memoryfuncs.c functions. */
int MpsCreateVarFrom( int, mps_t, int * );
mps_ptr MpsGetMatrix( int );
mps_ptr MpsGetMatrixFromAddr( int * );
int MpsCollect();

/* argscheck.c functions. */
int MpsCheck1( char*, int*, int*, int*, int* );
int MpsCheck2( char*, int*, int*, int* );
int MpsCheck3( char*, int*, int*, int* );
int MpsCheckSameSize( char*, int*, int*, int*, int* );
int MpsCheckSameSize2( char*, int*, int*, int* );
int MpsCheckAllSameMps( char *, int*, int*, int*, int* );
int MpsCheckStringSize( char*, int*, int, unsigned int, unsigned int );
int MpsCheckSize( char*, int*, int, unsigned int, unsigned int );
int MpsCheckScalar( char*, int*, int );
int MpsIsValid( char*, int *, int );
int MpsGetRoundingArg( char*, mp_rnd_t*, int*, int );

#define MpsSig(...)  _MpsSig( __VA_ARGS__, INT_MAX )

unsigned int _MpsSig( int, ... );
unsigned int MpsGetSig();

int sci_mps_init1( char*, unsigned long );
int sci_mps_init2( char*, unsigned long );
int sci_mps_clear( char*, unsigned long );
int sci_mps_disp( char*, unsigned long );

int sci_mps_add( char*, unsigned long );
int sci_mps_mul( char*, unsigned long );
int sci_mps_mat_mul( char*, unsigned long );

/* sci_mps_trig.c functions. */
int sci_mps_sin( char*, unsigned long );
int sci_mps_cos( char*, unsigned long );
int sci_mps_tan( char*, unsigned long );
int sci_mps_sec( char*, unsigned long );
int sci_mps_csc( char*, unsigned long );
int sci_mps_cot( char*, unsigned long );
int sci_mps_asin( char*, unsigned long );
int sci_mps_acos( char*, unsigned long );
int sci_mps_atan( char*, unsigned long );

int sci_mps_sinh( char*, unsigned long );
int sci_mps_cosh( char*, unsigned long );
int sci_mps_tanh( char*, unsigned long );
int sci_mps_sech( char*, unsigned long );
int sci_mps_csch( char*, unsigned long );
int sci_mps_coth( char*, unsigned long );
int sci_mps_asinh( char*, unsigned long );
int sci_mps_acosh( char*, unsigned long );
int sci_mps_atanh( char*, unsigned long );

/* sci_mps_lu.c */
int sci_mps_lu( char*, unsigned long );
